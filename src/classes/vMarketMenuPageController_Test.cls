/**
 *  @author    :    Puneet Mishra
 *  @description:    test Class for vMarketMenuPageController
 */
@isTest
public class vMarketMenuPageController_Test {
	public static String content =  '<b>About V Market</b> <br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. '+
                              ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and '+
                              ' scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, '+
                              ' remaining essentially unchanged.<br>';
    
    public static String ques = '<b>Q1.Lorem ipsum is simply dummy test of the printing and typesetting.</b>';
    public static String ans = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ' + 
        				'Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer '+
        				'took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, '+
        				'but also the leap into electronic typesetting, remaining essentially unchanged.';
    
    public static testMethod void vMarketMenuPageController_SupportTest() {
        Test.StartTest();
        	Test.setCurrentPageReference(new PageReference('Page.vMarketMenu')); 
            System.currentPageReference().getParameters().put('type', 'support');
        
        vMarketMenu__c menu = vMarketDataUtility_Test.createMenuData('support', null, null, true);
        
        vMarketMenuPageController control = new vMarketMenuPageController();
        String expected = '<br>'+ content;
        system.assertEquals(expected, control.pageContent);
        Test.StopTest();
    }
    
    public static testMethod void vMarketMenuPageController_FAQTest() {
        Test.StartTest();
        	Test.setCurrentPageReference(new PageReference('Page.vMarketMenu')); 
            System.currentPageReference().getParameters().put('type', 'FAQ');
        
        vMarketMenu__c menu = vMarketDataUtility_Test.createMenuData('FAQ', ques, ans, true);
        
        vMarketMenuPageController control = new vMarketMenuPageController();
        String expected = '<br>'+menu.vMarketQuestion__c+'<br>'+menu.answer__c;
        system.assertEquals(expected, control.pageContent);
        Test.StopTest();
    }
}
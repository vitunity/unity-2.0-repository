/**
 * TODO: Hardcoded values to be moved into Custom Label
 * TODO: Code refactor
 */
 /* Modified by Abhishek K as Part of Subscription VMT-30 */
public class vMarketOrderDetailController {

    public Set<String> appCategory{get;set;}
    @testVisible private set<String> fileFeedParentId;
    public Map<String, List<OrderDetailInnerController>> details{get;set;}
    public String orderNumber{get;set;}
    public String orderStatus{get;set;}
    public static String shippingAddr{get;set;}
    public static vMarketOrderItem__c orderInfo{get;set;}
    public Decimal total{get;set;}
    public Integer totalApps{get;set;}
    public Boolean flag{get;set;}
    public Boolean pending{get;set;}
    public Boolean subscription{get;set;}
    public Date expiryDate{get;set;}
    public Integer daysLeft{get;set;}
    public map<String, Decimal> appPriceMap{get;set;}
    public map<String, String> appDeveloperMap{get;set;}
    public static String statusSuccessMsg;
    public static String statusFailedMsg;
    public map<String, Id> appGuide{get;set;}
    public String breadcrumb {get;set;}
    public boolean expired {get;set;}
    public boolean subcancelled {get;set;}
    public boolean subfailed {get;set;}
    public boolean nonUS{get;set;}
    public String country {get;set;}
    public String subscribedAppId {get;set;}
    public String paymentAccount{get;set;}
    public boolean subscriptionPayment{get;set;}
    public Date subPaymentDate{get;set;}
    public String subPaymentNumber{get;set;}
    
    public String vatNumber{get;set;}
    
    public Map<ID, Decimal> orderItemLineMap{get;set;}
    
    public string recId{get;set;}// recId of OrderLineItem
    
    {
        statusSuccessMsg = Label.vMarket_Order_Success_Msg;
        statusfailedMsg = Label.vMarket_Order_Failed_Msg;
        shippingAddr = '';
        orderInfo = new vMarketOrderItem__c();
        appPriceMap = new Map<String, Decimal>();
        appDeveloperMap = new Map<String, String>();
        orderItemLineMap = new Map<ID, Decimal>();
    }
    
    public vMarketOrderDetailController() {
        totalApps = 0;
        getOrderDetails();
        paymentAccount = '';

        subPaymentNumber = '';
        subscriptionPayment = false;
        nonUS = false;
        country = (new vMarket_Approval_Util()).getUserCountry();
        if(String.isNotBlank(country) && !country.equals('US')) nonUS = true;
        
        String payId = ApexPages.currentPage().getParameters().get('payid');
        if(String.isNotBlank(payId))
        {
        subscriptionPayment = true;
        VMarket_Subscription_Payments__c pay = [Select id ,vMarket_Order_Item__r.name, VMarket_Invoice_Date__c from VMarket_Subscription_Payments__c where id=:payId][0];
        if(pay!=null)
        {
        subPaymentDate = pay.VMarket_Invoice_Date__c;
        //subPaymentNumber = pay.vMarket_Order_Item__r.name+ApexPages.currentPage().getParameters().get('count'); changed by Harshal for VMT189
        subPaymentNumber = pay.vMarket_Order_Item__r.name;
        }
        }
        
    }
    
    public void getPaymentAccount()
    {
    List<vMarketOrderItem__c> orderItems = [Select id, VMarket_Duration_Type__c , VMarket_Subscription_Cancellation_Date__c ,VMarket_Subscriptions__r.VMarket_Stripe_Detail__r.Details__c from vMarketOrderItem__c where id=:ApexPages.currentPage().getParameters().get('id')];
    if(orderItems.size()>0) paymentAccount = String.valueOf(orderItems[0].VMarket_Subscriptions__r.VMarket_Stripe_Detail__r.Details__c);
    }
    
    public List< VMarket_Subscription_Payments__c > getPayments()
        {getPaymentAccount();
        return [Select id, VMarket_Invoice_Date__c, VMarket_Stripe_Invoice_Id__c  ,VMarket_Stripe_Payment_Status__c from VMarket_Subscription_Payments__c where vMarket_Order_Item__c=:ApexPages.currentPage().getParameters().get('id')];
        }   
        
        
    public Map<String, List<vMarketAppSource__c>> getOrderDetails() {
    expired = false;
    subcancelled = false;
        subfailed = false;
        List<vMarketOrderItem__c> orderList = new List<vMarketOrderItem__c>();
        String orderId;
        totalApps = 0;
        total = 0.0;
        System.debug('******1');
        if(ApexPages.currentPage().getParameters().get('id')!=null) {
        System.debug('******2');
            orderId = ApexPages.currentPage().getParameters().get('id');
            orderList = [SELECT Id, Name, OrderPlacedDate__c,IsSubscribed__c, Expiry_Date__c, ShippingAddress__c, Status__c, SubTotal__c, Tax__c, Tax_price__c, Total__c, ZipCode__c, OwnerId,
                         VMarket_Subscriptions__c, VMarket_Subscriptions__r.Is_Active__c,VMarket_Duration_Type__c , VMarket_Subscription_Cancellation_Date__c ,VAT_Number__c,
                            (SELECT Id, Name, IsSubscribed__c, Price__c, vMarket_App__c, CurrencyIsoCode, vMarket_Order_Item__c, transfer_status__c FROM vMarket_Order_Item_Lines__r )
                         FROM vMarketOrderItem__c
                         WHERE Id =: orderId AND OwnerId =: userInfo.getUserId()];
                         
            if(!orderList.isEmpty()) {
                total = orderList[0].Total__c;
                system.debug(' == Label.vMarket_Failed == ' + Label.vMarket_Failed + ' == orderList[0].Status__c == ' + orderList[0].Status__c+expired);
                subscription = false;
                if(orderList[0].IsSubscribed__c) {
                    subscription = true;
                    expiryDate = orderList[0].expiry_date__c;
                    daysLeft = 10;//(Date.today()).daysBetween(orderList[0].expiry_date__c);
                    //VMT-33: Order Details Page for Subscription
                   // if(daysLeft<0 || !orderList[0].VMarket_Subscriptions__r.Is_Active__c) 
                   //     expired = true;
                   if(!orderList[0].status__C.equals(Label.vMarket_Failed) && (orderList[0].status__C.equals('Completed') || orderList[0].Status__c.contains('PaymentFail') )) expired = true;
                }
                
                if(orderList[0].Status__c.contains(Label.vMarket_Failed)) {
                    flag = false;
                    orderStatus = statusfailedMsg;
                    system.debug(' ==== INSIDE ==== ' + statusfailedMsg);
                } else if(orderList[0].Status__c.contains(Label.vMarket_Success)) {
                    flag = true;
                    orderStatus = statusSuccessMsg;
                }
                else if(orderList[0].Status__c.contains('Pending')) {
                    flag = false;
                    pending = true;
                    orderStatus = 'Payment is Still Under Process';
                }
                else if(orderList[0].Status__c.contains('Completed')) {
                subcancelled = true;
                flag = true;
                system.debug(' ==== INSIDE Completed==== ' + statusfailedMsg);
                }
                else if(orderList[0].Status__c.contains('PaymentFail')) {
                flag = true;
                subfailed = true;
                system.debug(' ==== INSIDE Payment Fail==== ' + statusfailedMsg);
                }
                if(orderList[0].VAT_Number__c!=null || orderList[0].VAT_Number__c!='')
                vatNumber= orderList[0].VAT_Number__c;
                orderNumber = orderList[0].Name;
                orderInfo = orderList[0];
            } else
            {
                flag = false;
                return new Map<String, List<vMarketAppSource__c>>();
                
                }
        }

        
        
     

        system.debug(' ==== orderList ==== ' + orderList);
        Set<String> appIds = new Set<String>();
        if(!orderList.isEmpty()) {
            for(vMarketOrderItem__c order : orderList) {
                for(vMarketOrderItemLine__c orderLines : order.vMarket_Order_Item_Lines__r) {
                    appIds.add(orderLines.vMarket_App__c);
                    subscribedAppId = orderLines.vMarket_App__c;
                    orderItemLineMap.put(orderLines.vMarket_App__c, orderLines.Price__c);
                    totalApps ++;
                }
                // Sno. 3 Showing Order Address on Order Detail
                system.debug(' ===== order ShippingAddress === ' + order.ShippingAddress__c);
                /*system.assert(false);
                order.ShippingAddress__c.replace('\n', ' ');
                order.ShippingAddress__c.replace('\\n', ' ');
                order.ShippingAddress__c.replace('\r\n', ' ');
                order.ShippingAddress__c.replace('\\r\\n', ' ');*/
                
                shippingAddr = order.ShippingAddress__c.normalizeSpace();
                //orderInfo = order;
            }
            //orderNumber = orderList[0].Name;
        }
        appCategory = fetchOrderedApps(appIds).keySet();
        
        return fetchOrderedApps(appIds);        
    }
    
    
    
    @TestVisible
    private Map<String, List<vMarketAppSource__c>> fetchOrderedApps(Set<String> appIds) {
        if(appIds.isEmpty())
            return new Map<String, List<vMarketAppSource__c>>();
        
        fileFeedParentId = new set<String>();
        
        appGuide = new map<String, Id>();
        if(!appIds.isEmpty())
            fileFeedParentId.addAll(appIds);
        details = new Map<String, List<OrderDetailInnerController>>();  
        Map<String, List<vMarketAppSource__c>> cate_AppSrcMap = new Map<String, List<vMarketAppSource__c>>();
        List<vMarketAppSource__c> vMarketAppSourceList = new List<vMarketAppSource__c>();
        vMarketAppSourceList = [ SELECT Id, Name, IsActive__c, Status__c, App_Category__c, App_Category_Version__c, vMarket_App__c, vMarket_App__r.Name, 
                                        vMarket_App__r.App_Category__c, vMarket_App__r.Price__c,vMarket_App__r.owner.firstname, vMarket_App__r.Short_Description__c,
                                        vMarket_App__r.PublisherName__c, Bug_Fix_Features__c
                                 FROM vMarketAppSource__c 
                                 WHERE IsActive__c =: true AND Status__c =: Label.vMarket_Published AND
                                        vMarket_App__c IN: appIds];

        for(vMarketAppSource__c src : vMarketAppSourceList) {
            system.debug(' ==== src ==== ' + src+'=='+cate_AppSrcMap);
            if(cate_AppSrcMap.containsKey(src.vMarket_App__r.Name)) { // src.vMarket_App__r.Name  src.App_Category__c
                cate_AppSrcMap.get(src.vMarket_App__r.Name).add(src);
            } else {
                cate_AppSrcMap.put(src.vMarket_App__r.Name, new List<vMarketAppSource__c>{src});

                //appPriceMap.put(src.vMarket_App__r.Name, src.vMarket_App__r.Price__c);
                appPriceMap.put(src.vMarket_App__r.Name, orderItemLineMap.get(src.vMarket_App__c));

                if (src.vMarket_App__r.PublisherName__c != null)
                    appDeveloperMap.put(src.vMarket_App__r.Name, src.vMarket_App__r.PublisherName__c);
                else
                    appDeveloperMap.put(src.vMarket_App__r.Name, '');
            }
            fileFeedParentId.add(src.Id);
            system.debug(' ==== totalApps ==== ' + totalApps);
        }
        Map<String, feedItem> feedmap = new Map<String, feedItem>();
        for(FeedItem feedItem : [SELECT RelatedRecordId, ContentType, ParentId, ContentFileName   FROM FeedItem WHERE ParentId IN: fileFeedParentId]) {
            System.debug('____'+feedItem.ParentId + ' = ' + feedItem.RelatedRecordId);
            feedmap.put(feedItem.ParentId,feedItem);
        }
        system.debug('=== cate_AppSrcMap === ' + cate_AppSrcMap);
        system.debug('=== feedmap === ' + feedmap);
        
        /**Section for Attachment**/
        Map<Id, Id> attachmentId = new Map<Id, Id>();// Map of ParentId vs attachment Id
        for(Attachment attach : [SELECT Id, Name, ContentType, ParentId FROM Attachment WHERE ParentId IN: appIds]) {
            if(attach.Name.startsWith('AppGuide_'))
                attachmentId.put(attach.ParentId, attach.Id);
        }
        /****/
        System.debug('***here0--'+cate_AppSrcMap.keySet());
        for(String str : cate_AppSrcMap.keySet()){
            System.debug('***here1--'+str);
            Set<String> existingCategoryVersions = new Set<String>();
            for(vMarketAppSource__c src : cate_AppSrcMap.get(str)) {
            System.debug('***here--'+src.id+'--'+details);
                if(details.containsKey(src.vMarket_App__r.Name)) {//src.App_Category__c src.vMarket_App__r.Name 
                    if(!existingCategoryVersions.contains(src.App_Category_Version__c)) 
                    {
                    details.get(src.vMarket_App__r.Name).add(new OrderDetailInnerController(src, feedmap.get(src.Id)));
                    existingCategoryVersions.add(src.App_Category_Version__c);
                    }
                    system.debug(' === detailsIf === ' + details);
                } else {
                    details.put(src.vMarket_App__r.Name, new List<OrderDetailInnerController>{new OrderDetailInnerController(src, feedmap.get(src.Id))});
                    existingCategoryVersions.add(src.App_Category_Version__c);
                    system.debug(' === details else === ' + details);
                }
                //system.debug(' == feedmap.get(src.vMarket_App__c).RelatedRecordId == ' + feedmap.get(src.vMarket_App__c).RelatedRecordId);
                //App Guide
                /*if(!appGuide.containsKey(src.App_Category__c)) {
                    appGuide.put(src.vMarket_App__r.Name, src.vMarket_App__c);//feedmap.get(src.vMarket_App__c).RelatedRecordId);
                }*/
                if(!appGuide.containsKey(src.App_Category__c)) {
                    appGuide.put(src.vMarket_App__r.Name, attachmentId.get(src.vMarket_App__c));//feedmap.get(src.vMarket_App__c).RelatedRecordId);
                }
                system.debug(' ===== appGuide ======== ' + appGuide);
            }
        }
        system.debug(' === details === ' + details);
        return cate_AppSrcMap;
    }
    
    public pageReference incrementDownloadCount() {
        String queryStr = ' SELECT App_Category__c, App_Category_Version__c, IsActive__c, Status__c, vMarket_App__c '+
                            + ' FROM vMarketAppSource__c '
                            + ' WHERE Id=\''+recId+'\' limit 1';
        List<vMarketAppSource__c> appSrc  = Database.query(queryStr);
        
        if(!appSrc.isEmpty()) {
            String listingStr = 'SELECT  App__r.Name, App__r.App_Category__r.Name, App__r.Owner.FirstName, App__r.Owner.LastName, App__r.Owner.Email, App__r.Owner.Phone, '
                                + ' App__r.ApprovalStatus__c, App__r.AppStatus__c, App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, '
                                + ' App__r.Price__c, App__r.Published_Date__c, App__r.Short_Description__c, InstallCount__c,'
                                + ' PageViews__c, (Select Rating__c, CreatedBy.Name, CreatedDate, Name__c, Description__c From vMarket_Comments__r ORDER By CreatedDate DESC ) '
                                + ' FROM vMarket_Listing__c '
                                + ' WHERE App__r.ApprovalStatus__c=\'Published\' AND App__r.IsActive__c=true AND App__r.Id=\''+ appSrc[0].vMarket_App__c +'\' limit 1';
            List<vMarket_Listing__c> listings  = Database.query(listingStr);
            system.debug(' ==== listings ==== ' + listings);
            vMarket_ListingDownloads__c downLoads;
            if(!listings.isEmpty()) {
                downLoads = new vMarket_ListingDownloads__c();
                
                // Storing IP Address
                String ipAddress ='';
                ipAddress = ApexPages.currentPage().getHeaders().get('True-Client-IP');
                if(ipAddress == '' || ipAddress == null) {
                    ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
                }
                system.debug(' ==== IPADDRESS ==== ' + ipAddress);
                downLoads.IP_Address__c = ipAddress;
                downLoads.Downloaded_By__c = userInfo.getUserId();
                downLoads.vMarket_Listing__c = listings[0].Id;
                insert downLoads;
            }
        }
        
        return null;
    }
    
    /**
     *  created by      :   Puneet Mishra, 5 march 2018
     *  description     :   method called when user want to cancel the subscription
     */
     /* Added by Abhishek K to Cancel Active Subscription Plan*/
    public void cancelSubscription() {
        if(ApexPages.currentPage().getParameters().get('id')!=null) {
            string orderItemId = ApexPages.currentPage().getParameters().get('id');
            String subscriptionId = [SELECT Id, Name, OrderPlacedDate__c, VMarket_Subscription_Cancellation_Date__c ,IsSubscribed__c, VMarket_Subscriptions__c  
                                             FROM vMarketOrderItem__c
                                             WHERE Id =: orderItemId AND OwnerId =: userInfo.getUserId()].VMarket_Subscriptions__c;
            
            List<VMarket_Subscriptions__c> subscription = new List<VMarket_Subscriptions__c>();
            subscription = [SELECt id, Name, Is_Active__c, VMarket_Stripe_Subscription_Id__c FROM VMarket_Subscriptions__c WHERE Id =: subscriptionId];
            //subscription.Is_Active__c = false;
            //update subscription;
            Boolean success = vMarket_StripeAPIUtil.deletesubscriptionplans(subscription);
            /*if(success)
            {
            pageReference pgRef = ApexPages.currentPage();
            pgRef.getParameters().clear();
            pgRef.getParameters().put('id', orderItemId);  
            pgRef.setRedirect(true);
            return pgRef;
            }
            else return null;*/
        }
        //return null;
    }
    
    public Class OrderDetailInnerController {
        public String appCategory{get;set;}
        public FeedItem fileFeedItem{get; set;}
        public vMarket_App__c app{get;set;}
        public vmarketAppSource__c appSrc{get;set;}
        
        public OrderDetailInnerController(vMarketAppSource__c appSrc, FeedItem feedFile) {
            this.appSrc = appSrc;
            this.fileFeedItem = feedFile;
            this.appCategory = appSrc.App_Category__c;
            
        }
    }   

}
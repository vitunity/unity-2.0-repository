/**
 *  @author    :    Puneet Mishra
 *  @description:    test Class for vMarketOrderDetailController
 */
@isTest
public with sharing class vMarketOrderDetailController_Test {
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app;
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    
    static Profile admin,portal;   
     static User customer1, customer2, developer1, developer2;
     static User adminUsr1,adminUsr2;
     static List<User> lstUserInsert;
     static Account account;
     static Contact contact1, contact2, contact3, contact4;
     static List<Contact> lstContactInsert;
     
     static Attachment attach_Logo, attach_AppGuide;
     static List<Attachment> attachList;
     
    static {
		admin = vMarketDataUtility_Test.getAdminProfile();
		portal = vMarketDataUtility_Test.getPortalProfile();
  
      	lstUserInsert = new List<User>();
		lstUserInsert.add( adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin1'));
		lstUserInsert.add( adminUsr2 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin2'));
		insert lstUserInsert;
      
      account = vMarketDataUtility_Test.createAccount('test account', true);
      
      lstContactInsert = new List<Contact>();
      lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
		lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
    	lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
    	lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
      insert lstContactInsert;
      
      System.runAs(adminUsr1) {
      	lstUserInsert = new List<User>();
      	lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
       	lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
       	lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
       	lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
      	insert lstUserInsert;
      }
      cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
      app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
      
      listing = vMarketDataUtility_Test.createListingData(app, true);
      comment = vMarketDataUtility_Test.createCommentTest(app, listing, true);
      activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
      
      source = vMarketDataUtility_Test.createAppSource(app, true);
      feedI = vMarketDataUtility_Test.createFeedItem(source.Id, true);
      
      orderItemList = new List<vMarketOrderItem__c>();
      system.runAs(Customer1) {
        orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
      }
      //system.runAs(Customer2) {
        orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
      //}
      orderItemList.add(orderItem1);
      orderItemList.add(orderItem2);
      //insert orderItemList;
      
      orderItemLineList = new List<vMarketOrderItemLine__c>();
      system.runAs(Customer1) {
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app, orderItem1, true, false);
      }
      
      //system.RunAs(Customer2) {
        orderItemLine2 = vMarketDataUtility_Test.createOrderItemLineData(app, orderItem2, false, true);
      //}
      
      orderItemLineList.add(orderItemLine1);
      orderItemLineList.add(orderItemLine2);
      //insert orderItemLineList;
    }
    
    public static testMethod void vMarketOrderDetailControllerTest() {
    	//system.runas(Customer1){
    	Test.startTest();
    		app.ApprovalStatus__c = 'Published';
    		update app;
    		
    		attach_Logo = vMarketDataUtility_Test.createAttachmentTest(String.valueOf(app.Id), 'image/jpeg', 'Logo_', false);
    		attach_AppGuide = vMarketDataUtility_Test.createAttachmentTest(String.valueOf(app.Id), 'application/pdf', 'AppGuide_', false);
    		attachList = new List<Attachment>();
    		attachList.add(attach_Logo);
    		attachList.add(attach_AppGuide);
    		insert attachList;
    		
    		Test.setCurrentPageReference(new PageReference('Page.vMarketOrderDetail')); 
            System.currentPageReference().getParameters().put('id', orderItem2.Id);
            
    		vMarketOrderDetailController control = new vMarketOrderDetailController();
    		control.recId = source.Id;
    		control.incrementDownloadCount();
            control.cancelSubscription();
        control.getPaymentAccount();
        control.getPayments();
    	Test.StopTest();
    	//}
    }
}
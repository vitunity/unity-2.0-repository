/**
 *  Created By  : Puneet Mishra
 *  Description : Controller to fetch Order Details
 */
public with sharing class vMarketOrderReceiptController {
    
    //Pagination variables and methods
    @TestVisible private integer totalRecs = 0;
    @TestVisible private integer OffsetSize = 0;
    @TestVisible private integer LimitSize= 10;
    public Boolean isEnable{get;set;}
    public Boolean isSubscription{get;set;}
    public String orderStatus{get;set;}
    public Integer start{get{return 0;}set;}
    public Integer ends{get{return 0;}set;}
    
    public Integer successRec{get;set;}
    public Integer failedRec{get;set;}
    
    // 16 may 2017
    public Integer startNum{get;set;}
    public Integer endNum{get;set;}
        
    // return list of date range filter value to filter the Orders
    public List<SelectOption> datePicker {
        get{
            List<SelectOption> dateOption = new List<SelectOption>();//vMarket_LiveYear
            Integer currentYear = system.today().year();
            Integer yearDifference = currentYear - (Integer.valueOf(Label.vMarket_LiveYear));
            dateOption.add(new SelectOption('6M','past 6 months'));
            if(yearDifference != 0) {
                for(integer i = -yearDifference; i != 0; i++) {
                    dateOption.add(new SelectOption(String.valueOf(currentYear), String.valueOf(currentYear) ));
                    currentYear = currentYear - 1;
                }
            }
            system.debug(SelectedValue + ' === dateOption === ' + datePicker);
            return dateOption;
        } set;
    }
    public String SelectedValue{get;set;}
    public Integer orderSize{get;set;}
    
    public vMarketOrderReceiptController() {
   
        if(apexpages.currentpage().getparameters().get('subscribe')!=null) 
        {
            isSubscription= true;
        }    
        else
        { 
            isSubscription = false;
        }    
        system.debug(SelectedValue + ' === dateOption === ' + datePicker);
        if(ApexPages.currentPage().getParameters().get('id') != null) {
            system.debug(' === 1 === ' + 1);
            //SelectedValue = '6M';
        }
        isEnable = true;
    }
    
    // method will be called when picklist value get changed
    public void searchOrders() {
        system.debug(' == selected Value ' + SelectedValue);
        //getOrderDetails();
    }
    
    /**
     *  createdby   :   Puneet Mishra
     *  desc        :   return's list of success/failed orders by logged in user on the basis of date range filter 
     *  getter
     */
    public List<vMarketOrderItem__c> getOrderDetails() {
        String orderSOQL =  'SELECT id, Name, OrderPlacedDate__c, ShippingAddress__c, IsSubscribed__c, Expiry_Date__c, Status__c, SubTotal__c, Tax__c, Total__c, '+
                                                            'ZipCode__c, CurrencyIsoCode, OwnerId ' +
                                                    ' FROM vMarketOrderItem__c ' ;
                                                    
                                                        
        // Query for Counting records returned
        String countSoql = 'select count() FROM vMarketOrderItem__c ' ;
        system.debug(SelectedValue + ' === b4 orderSOQL === ' + orderSOQL);
        orderSize = 0;
        List<vMarketOrderItem__c> orders = new List<vMarketOrderItem__c>();
        Date startD, endD;
        // Setting the StartDate and End Date for Filter
        if(SelectedValue == '6M' || SelectedValue == null) {
            endD = Date.newInstance(system.today().year(), system.today().month(), system.today().day());
            startD = endD.addMonths(-6);
        } else {
            startD = Date.newInstance(Integer.valueOf(SelectedValue), 1, 1 );
            endD = Date.newInstance(Integer.valueOf(SelectedValue), 12, 31);
        }
        system.debug('startDate=>' +  startD + ' ##-## ' + 'endDate => ' + endD);
        integer OffsetSizeTemp;
        if(OffsetSize < 0)
            OffsetSizeTemp = 0;
        else
            OffsetSizeTemp = OffsetSize;
        
        integer LimitSizeTemp = LimitSize;
        if(ApexPages.currentPage().getParameters().get('id') != null) {
            String orderId = ApexPages.currentPage().getParameters().get('id');
            orderSOQL += ' WHERE Id = : orderId';
            countSoql += ' WHERE Id = : orderId';
            system.debug(' === orderSOQL === ' + orderSOQL);
        } else {
            String usrId = userInfo.getUserId();
            orderSOQL += ' WHERE ownerId = : usrId AND Status__c != null  ';
            orderSOQL += ' AND OrderPlacedDate__c >=: startD AND OrderPlacedDate__c <=: endD ';
            countSoql += ' WHERE ownerId = : usrId AND Status__c != null AND OrderPlacedDate__c >=: startD AND OrderPlacedDate__c <=: endD ';
            system.debug(' === orderSOQL === ' + orderSOQL);
        }
        // count the number of records
        
        if(isSubscription)    
        {
            orderSOQL += ' And IsSubscribed__c = true'; 
            countSoql += ' And IsSubscribed__c = true'; 
        }
        else
        {
            orderSOQL += ' And IsSubscribed__c = false'; 
            countSoql += ' And IsSubscribed__c = false'; 
        }
        if(orderStatus.equals('Success')) orderSOQL += ' AND (Status__c =\'Success\' OR Status__c =\'Completed\')';
        else orderSOQL += ' AND Status__c =: orderStatus ';
       
        if(orderStatus.equals('Success')) countSoql += ' AND (Status__c =\'Success\' OR Status__c =\'Completed\')';
        else countSoql += ' AND Status__c =: orderStatus ';
        
        system.debug('======== OffsetSizeTemp ======== ' + OffsetSizeTemp);
        /*
        if(startNum == null)
            startNum = 1;
        else
            startNum += OffsetSizeTemp;
        */
        totalRecs = database.countQuery(countSoql);
        if(orderStatus == Label.vMarket_Success)
            successRec = totalRecs;
        else if(orderStatus == Label.vMarket_Failed)
            failedRec = totalRecs;
        
        system.debug(' === totalRecs ===> ' + totalRecs);
        system.debug('LimitSizeTemp=>' +  LimitSizeTemp + ' ##-## ' + 'OffsetSizeTemp => ' + OffsetSizeTemp);
        orderSOQL += ' ORDER BY Name Desc LIMIT :LimitSizeTemp OFFSET :OffsetSizeTemp ' ;
        system.debug(' === LimitSizeTemp ==== ' +  LimitSizeTemp + ' === OffsetSizeTemp === ' + OffsetSizeTemp);
        system.debug(' === SOQL === ' + orderSOQL);
        system.debug(' === orderStatus === ' + orderStatus);
        orders = Database.Query(orderSOQL);
        system.debug(' === orders === ' + orders );
        /*
        if(endNum == null )
            endNum = 0;
        endNum += orders.size();
        */
        orderSize = totalRecs;
        return orders;
    }
    
    public void First() {
        OffsetSize = 0;
        //isEnable = getprev();
        getOrderDetails();
    }
    
    public void Next() {
        system.debug(' == NEXT => ' + OffsetSize + ' = ' + LimitSize);
        OffsetSize = OffsetSize + LimitSize;
        system.debug(' == NEXT 2=> ' + OffsetSize + ' = ' + LimitSize);
        //isEnable = getnxt();
        getOrderDetails();
    }
    
    public void Previous() {
        system.debug(' == OffsetSize 1 => ' + OffsetSize);
        OffsetSize = OffsetSize - LimitSize;
        system.debug(' == OffsetSize 2 => ' + OffsetSize);
        //isEnable = getprev();
        getOrderDetails();
    }
    
    public void Last() {
        system.debug(' == Last() 1 == ' + OffsetSize);
        OffsetSize = totalrecs - math.mod(totalRecs,LimitSize);
        system.debug(' == Last() 2 == ' + OffsetSize);
        //isEnable = getnxt();
        system.debug(' ==Last()== ' + isEnable);
        getOrderDetails();
    }
    
    public boolean getprev(){
        system.debug(' == OffSetSize 1 == ' + OffsetSize);
        if(OffsetSize == 0)
            return true;
        else
            return false;
    }
    public boolean getnxt(){
        system.debug(' OffSet=>' + OffsetSize + ' LimitSize=>' + LimitSize + ' totalRecs=>' + totalRecs );
        if((OffsetSize + LimitSize) > totalRecs) {
            system.debug(' getnxt IF => TRUE ');
            return true;
        } else {
            system.debug(' getnxt ELSE => FALSE ');
            return false;
        }
    }
    
}
public class vMarketPaymentsCOntroller
{
public List<VMarketACH__c> customerIds{get;set;}
public List<vMarketStripeDetail__c> stripeDetails{get;set;}
public Map<Id,Integer> subscriptionsMap{get;set;}
public String keystring{get;set;}

public VMarketACH__c customer{get;set;}
public Integer itemsSize{get;set;}
    public String selectedAcctNum{get;set;}
    public String selectedAcctName{get;set;}
    public boolean AgreeACHTransfer{get;set;}
    public boolean AgreeAddAccount{get;set;}
public vMarketPaymentsCOntroller()
{
keystring = '';
//customer = [Select id,name,Account_Number__c,Customer__c,Bank__c,Status__c from VMarketACH__c where user__C=:userinfo.getuserid()][0];
customerIds = new List<VMarketACH__c>();
stripeDetails = new List<vMarketStripeDetail__c>();
customerIds = [Select id,name,Account_Number__c,Customer__c,Bank__c,Status__c from VMarketACH__c where user__C=:userinfo.getuserid()];
stripeDetails = [Select name,Details__c, StripeAccountId__c, User__c from vMarketStripeDetail__c where User__c=:UserInfo.getUserId()];
subscriptionsMap = new Map<Id,Integer>();
/*
for(VMarket_Subscriptions__c sub : [Select id, Is_Active__c, VMarket_Stripe_Detail__c from VMarket_Subscriptions__c where VMarket_Stripe_Detail__c IN :stripeDetails])
{
if(!subscriptionsMap.containskey(sub.VMarket_Stripe_Detail__c)) 
{subscriptionsMap.put(sub.VMarket_Stripe_Detail__c,1);
keystring += sub.VMarket_Stripe_Detail__c+',';
}
else subscriptionsMap.put(sub.VMarket_Stripe_Detail__c,subscriptionsMap.get(sub.VMarket_Stripe_Detail__c)+1);
}
*/

itemsSize = customerIds.size();
}
public void storePayInfo() 
    {
   String cust = ApexPages.CurrentPage().getParameters().get('customerInfo');
    String bank = ApexPages.CurrentPage().getParameters().get('bankInfo');
    String acctname = ApexPages.CurrentPage().getParameters().get('accountName');
    String acctnumb = ApexPages.CurrentPage().getParameters().get('accountNumber');
    String bankName = ApexPages.CurrentPage().getParameters().get('bankName');
    VMarketACH__c ach = new VMarketACH__c();
    ach.name = acctname;
    ach.User__c = UserInfo.getUserId();
    ach.Customer__c = cust;
    ach.Bank__c = bank;
    ach.Bank_Name__c = bankName;
    ach.transferred_Deposit_1__c = Integer.valueOf(ApexPages.CurrentPage().getParameters().get('rand1'));
    ach.transferred_Deposit_2__c = Integer.valueOf(ApexPages.CurrentPage().getParameters().get('rand2'));
    ach.Status__c = 'Under Review';
    ach.Account_Number__c = Integer.valueOf(acctnumb);
    insert ach;
//    return new PageReference('/vMarketPayments');
        //return null;
    }
     /* Added by Abhishek K to Submit ACH Account Verification Request*/
    public PageReference submitverification()
    {
    String appid = ApexPages.CurrentPage().getParameters().get('appidstart');
    String deposit1 = ApexPages.CurrentPage().getParameters().get('deposit1');
    String deposit2 = ApexPages.CurrentPage().getParameters().get('deposit2');
    
    VMarketACH__c ach = [Select id, Status__c from VMarketACH__c where id=:Id.valueOf(appid) limit 1][0];
    ach.Status__c = 'Pending';
    ach.Received_Deposit_1__c = Integer.valueOf(deposit1);
    ach.Received_Deposit_2__c = Integer.valueOf(deposit2);
    update ach;
    return new PageReference('/vMarketPayments');
    
    }
    /* Added by Abhishek K to Delete ACH Account*/
     public PageReference deleteach()
    {
    String appid = ApexPages.CurrentPage().getParameters().get('appid');
    
    
    VMarketACH__c ach = [Select id, Status__c from VMarketACH__c where id=:Id.valueOf(appid) limit 1][0];
    //ach.Status__c = 'Under Review';
    delete ach;
    return new PageReference('/vMarketPayments');
    
    }
    /* Added by Abhishek K to Delete ACH Customer*/
    public PageReference deletecustomer()
    {
    String custid = ApexPages.CurrentPage().getParameters().get('custid');
    
    
    vMarketStripeDetail__c customer = [Select id from vMarketStripeDetail__c where id=:Id.valueOf(custid) limit 1][0];
    //ach.Status__c = 'Under Review';
    delete customer;
    return new PageReference('/vMarketPayments');
    
    }




}
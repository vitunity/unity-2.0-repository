@isTest
public with sharing class vMarketPaymentsCOntrollerTest {
    
    public static String tax = '{  '+
                                   '"RETURN":{  '+
                                      '"TYPE":"",'+
                                      '"ID":"",'+
                                      '"NUMBER":"000",'+
                                      '"MESSAGE":"",'+
                                      '"LOG_NO":"",'+
                                      '"LOG_MSG_NO":"000000",'+
                                      '"MESSAGE_V1":"",'+
                                      '"MESSAGE_V2":"",'+
                                      '"MESSAGE_V3":"",'+
                                      '"MESSAGE_V4":"",'+
                                      '"PARAMETER":"",'+
                                      '"ROW":0,'+
                                      '"FIELD":"",'+
                                      '"SYSTEM":""'+
                                   '},'+
                                   '"TAX_DATA":[  '+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-1",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 6.25",'+
                                         '"TAX_AMOUNT":" 268.6875",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-2",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1",'+
                                         '"TAX_AMOUNT":" 42.99",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-4",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1.75",'+
                                         '"TAX_AMOUNT":" 75.2325",'+
                                         '"TAX_JUR_LVL":""'+
                                      '}'+
                                   ']'+
                                '}';
                                
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2, app;
    static List<vMarket_App__c> appList;
    
    
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    static vMarket_Listing__c listing;
    static Profile admin,portal;   
    static User customer1, customer2, developer1;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3;
    static List<Contact> lstContactInsert;
    
    static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
    
    static  {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        
        account = vMarketDataUtility_Test.createAccount('test account', true);
        lstContactInsert = new List<Contact>();
        contact1 = vMarketDataUtility_Test.contact_data(account, false);
        contact1.MailingStreet = '53 Street';
        contact1.MailingCity = 'Palo Alto';
        contact1.MailingCountry = 'USA';
        contact1.MailingPostalCode = '94035';
        contact1.MailingState = 'CA';
        
        contact2 = vMarketDataUtility_Test.contact_data(account, false);
        contact2.MailingStreet = '660 N';
        contact2.MailingCity = 'Milpitas';
        contact2.MailingCountry = 'USA';
        contact2.MailingPostalCode = '94035';
        contact2.MailingState = 'CA';
        
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        
        lstContactInsert.add(contact1);
        lstContactInsert.add(contact2);
        insert lstContactInsert;
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        
        insert lstUserinsert;
        
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        appList = new List<vMarket_App__c>();
        appList.add(app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id));
        appList.add(app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id));
        insert appList;
        
        listing = vMarketDataUtility_Test.createListingData(app1, true);
        comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
        activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
        
        source = vMarketDataUtility_Test.createAppSource(app1, true);
        feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
        
        orderItemList = new List<vMarketOrderItem__c>();
        system.runAs(Customer1) {
            orderItem1 = vMarketDataUtility_Test.createOrderItem(true, false); // Subscribed Order
            orderItem1.Status__c = '';
            orderItem1.TaxDetails__c = tax;
            insert orderItem1;
        }
        system.runAs(Customer2) {
            orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
        }
        orderItemList.add(orderItem1);
        orderItemList.add(orderItem2);
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
    
        system.runas(customer1) {
            cart1 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        system.runas(customer2) {
            cart2 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        cartLineList = new List<vMarketCartItemLine__c>();
        cartLine1 = vMarketDataUtility_Test.createCartItemLines(app1, cart1, false);
        cartLine2 = vMarketDataUtility_Test.createCartItemLines(app2, cart1, false);
        
        cartLineList.add(cartLine1);
        cartLineList.add(cartLine2);
        //system.runas(customer1) {
            insert cartLineList;
         vMarket_Tax__c tax = new vMarket_Tax__c(name = 'tax', Tax__c = 30.0);
         insert tax;
    }
    
    private static testMethod void storePayInfoTest() {
        Test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketPayments')); 
            System.currentPageReference().getParameters().put('customerInfo', 'cus_CbtpcEdRXsVy2U');
            System.currentPageReference().getParameters().put('bankInfo', 'ba_1CCg1gL6mRt8fBaVKhBmPfHP');
            System.currentPageReference().getParameters().put('accountName', 'Test User');
            System.currentPageReference().getParameters().put('accountNumber', '6789');
            System.currentPageReference().getParameters().put('bankName', 'STRIPE TEST BANK');
            System.currentPageReference().getParameters().put('rand1', '32');
            System.currentPageReference().getParameters().put('rand2', '45');
            vMarketPaymentsCOntroller payment=new vMarketPaymentsCOntroller();
            payment.storePayInfo();
            
        Test.StopTest();
    }
    
    private static testMethod void submitverificationTest() {
        Test.StartTest();
            VMarketACH__c ach= new VMarketACH__c();
            ach.name = 'Test ACH';
            ach.User__c = UserInfo.getUserId();
            ach.Customer__c = 'cus_CbtpcEdRXsVy2U';
            ach.Bank__c = 'ba_1CCg1gL6mRt8fBaVKhBmPfHP';
            ach.Bank_Name__c = 'STRIPE TEST BANK';
            ach.transferred_Deposit_1__c = 20;
            ach.transferred_Deposit_2__c = 40;
            ach.Status__c = 'Under Review';
            ach.Account_Number__c = 6789;
            insert ach;
            vMarketPaymentsCOntroller payment=new vMarketPaymentsCOntroller();
            System.currentPageReference().getParameters().put('deposit1', '32');
            System.currentPageReference().getParameters().put('deposit2', '45');
            System.currentPageReference().getParameters().put('appidstart',ach.id);
            payment.submitverification();
            
        Test.StopTest();
    }
    
    private static testMethod void deleteACHTest() {
        Test.StartTest();
            VMarketACH__c ach= new VMarketACH__c();
            ach.name = 'Test ACH';
            ach.User__c = UserInfo.getUserId();
            ach.Customer__c = 'cus_CbtpcEdRXsVy2U';
            ach.Bank__c = 'ba_1CCg1gL6mRt8fBaVKhBmPfHP';
            ach.Bank_Name__c = 'STRIPE TEST BANK';
            ach.transferred_Deposit_1__c = 20;
            ach.transferred_Deposit_2__c = 40;
            ach.Status__c = 'Under Review';
            ach.Account_Number__c = 6789;
            insert ach;
            vMarketPaymentsCOntroller payment=new vMarketPaymentsCOntroller();
            
            System.currentPageReference().getParameters().put('appid',ach.id);
            payment.deleteach();
            
        Test.StopTest();
    }
    
    private static testMethod void deleteCustomerTest() {
        Test.StartTest();
            vMarketStripeDetail__c customer =new vMarketStripeDetail__c(StripeAccountId__c='cus_CeCIIYeyART01r');
            insert customer;
            vMarketPaymentsCOntroller payment=new vMarketPaymentsCOntroller();
            Test.setCurrentPageReference(new PageReference('Page.vMarketPayments')); 
            System.currentPageReference().getParameters().put('custid',customer.id);
            payment.deletecustomer();
            
        Test.StopTest();
    }
    
    
    
    
    
}
public class vMarketRatingDo {
	public static final String SOBJECT_NAME = vMarketComment__c.SObjectType.getDescribe().getName();
	
    public Integer starCount1, starCount2, starCount3, starCount4, starCount5;
    public Decimal total_count, avgRating, scale_rating;
    public List<vMarketComment__c> commentsList;
    
    public vMarketRatingDo(){
    
    }
    
    public Integer getStarCount1() {
    	return starCount1;
    }
    
    public void setStarCount1(Integer s_count) {
    	starCount1 = s_count;
    }
    
    public Integer getStarCount2() {
    	return starCount2;
    }
    
    public void setStarCount2(Integer s_count) {
    	starCount2 = s_count;
    }
    
    public Integer getStarCount3() {
    	return starCount3;
    }
    
    public void setStarCount3(Integer s_count) {
    	starCount3 = s_count;
    }
    
    public Integer getStarCount4() {
    	return starCount4;
    }
    
    public void setStarCount4(Integer s_count) {
    	starCount4 = s_count;
    }
    
    public Integer getStarCount5() {
    	return starCount5;
    }
    
    public void setStarCount5(Integer s_count) {
    	starCount5 = s_count;
    }
    
    public Decimal getTotalCount() {
    	total_count = (decimal)(starCount1 + starCount2 + starCount3 + starCount4 + starCount5); 
    	return total_count;
    }
    
    public void setTotalCount(Decimal t_count) {
    	total_count = t_count;
    }
    
    public Decimal getAvgRating() {
    	if( getTotalCount() == 0 ){
    		return 0;
    	}
    	avgRating = (decimal)(5*starCount5+4*starCount4+3*starCount3+2*starCount2+starCount1) / getTotalCount();     	
    	return avgRating;
    }
    
    public void setAvgRating(Decimal avg_rating) {
    	avgRating = avg_rating;
    }
    
    public Decimal getScaledRating() {
    	Decimal rating = getAvgRating();
    	scale_rating = rating.setscale(1);    	
    	return scale_rating;
    }
    
    public List<vMarketComment__c> getCommenstList() {
    	return commentsList;
    }
    
    public void setCommenstList(List<vMarketComment__c> commentList) {
    	commentsList = commentList;
    }

    public Integer getPercIndStar5() {
    	if (getTotalCount() == 0) {
    		return 0;
    	}
    	return Integer.valueOf((starCount5/getTotalCount())*100); 
    }
    
    public Integer getPercIndStar4() {
    	if (getTotalCount() == 0) {
    		return 0;
    	}
    	return Integer.valueOf((starCount4/getTotalCount())*100); 
    }

    public Integer getPercIndStar3() {
    	if (getTotalCount() == 0) {
    		return 0;
    	}
    	return Integer.valueOf((starCount3/getTotalCount())*100); 
    }

    public Integer getPercIndStar2() {
    	if (getTotalCount() == 0) {
    		return 0;
    	}
    	return Integer.valueOf((starCount2/getTotalCount())*100); 
    }

    public Integer getPercIndStar1() {
    	if (getTotalCount() == 0) {
    		return 0;
    	}
    	return Integer.valueOf((starCount1/getTotalCount())*100); 
    }
}
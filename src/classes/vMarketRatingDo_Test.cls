/**
 *  @author    :    Puneet Mishra
 *  @description:    test Class for vMarketRatingDo
 */
@isTest
public with sharing class vMarketRatingDo_Test {
    
    
    public static testMethod void vMarketRatingDoTEST() {
    	vMarketRatingDo rating = new vMarketRatingDo();
    	Integer starCount1Res, starCount2Res, starCount3Res, starCount4Res, starCount5Res;
    	Decimal avgRatingRes , scale_ratingRes, totalCountRes;
    	
    	rating.setStarCount1(1);
    	rating.setStarCount2(2);
    	rating.setStarCount3(3);
    	rating.setStarCount4(4);
    	rating.setStarCount5(5);
    	starCount1Res = rating.getStarCount1();
    	starCount2Res = rating.getStarCount2();
    	starCount3Res = rating.getStarCount3();
    	starCount4Res = rating.getStarCount4();
    	starCount5Res = rating.getStarCount5();	
    	system.assertEquals(starCount1Res, 1);
    	system.assertEquals(starCount2Res, 2);
    	system.assertEquals(starCount3Res, 3);
    	system.assertEquals(starCount4Res, 4);
    	system.assertEquals(starCount5Res, 5);
    	
    	rating.setTotalCount(2.5);
    	system.assertEquals(rating.total_count, 2.5);
    	
    	totalCountRes = rating.getTotalCount();
    	system.assertEquals(rating.total_count, 15);
    	
    	avgRatingRes = rating.getAvgRating();
    	
    	rating.setAvgRating(3);
    	system.assertEquals(rating.avgRating, 3);
    	
    	rating.getScaledRating();
    	rating.getPercIndStar5();
    	rating.getPercIndStar4();
    	rating.getPercIndStar3();
    	rating.getPercIndStar2();
    	rating.getPercIndStar1();
    	
    	List<vMarketComment__c> commentList = new List<vMarketComment__c>();
    	commentList = rating.getCommenstList();
    	system.assertEquals(null, commentList);
    	
    	rating.setCommenstList(commentList);
    	system.assertEquals(null, rating.commentsList);
    	
    	rating.starCount1 = 0;
    	rating.starCount2 = 0;
    	rating.starCount3 = 0;
    	rating.starCount4 = 0;
    	rating.starCount5 = 0;
    	
    	Decimal result = rating.getAvgRating();
    	system.assertequals(result, 0.0);
    	
    	system.assertEquals(rating.getPercIndStar5(), 0);
    	system.assertEquals(rating.getPercIndStar4(), 0);
    	system.assertEquals(rating.getPercIndStar3(), 0);
    	system.assertEquals(rating.getPercIndStar2(), 0);
    	system.assertEquals(rating.getPercIndStar1(), 0);
    }
}
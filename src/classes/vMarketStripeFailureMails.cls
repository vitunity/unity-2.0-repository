public class vMarketStripeFailureMails Implements Schedulable
{
            
       public vMarketStripeFailureMails()
       {
       
       } 
            
       public void execute(SchedulableContext sc)
        {
        List<vMarketOrderItem__c> failedOrders =[Select id, CreatedDate ,Status__c,name, OrderPlacedDate__c from vMarketOrderItem__c where Status__c != :Label.vMarket_Success];
        List<vMarketOrderItemLine__c> failedTransfers = [select id, CreatedDate ,transfer_status__c,name, vMarket_App__r.name, vMarket_Order_Item__r.CreatedDate, OrderDate__c, vMarket_Order_Item__r.id from vMarketOrderItemLine__c where transfer_status__c != :Label.vMarket_Paid];
        
        DateTime timeNow = System.now(); // Get the time now
        DateTime halfanhourago = timeNow.addMinutes(-30);
        
        String orderBody = '';
        String orderItemBody = '';
        String allorderBody = 'Salesforce Link,Order Number, Order Created date';
        String allorderItemBody = 'Salesforce Link,Order Item NUmber, Order Item Created Date';
        Integer orderCounter = 0;
        Integer orderItemCounter = 0;
        
        for(vMarketOrderItem__c order : failedOrders)
        {
        if(order.createddate <=  timeNow && order.createddate >= halfanhourago) 
        {
        orderBody += '<td><tr><a href/"'+Label.vMarket_BaseURL+'/'+order.id+'/">'+order.name+'</a></tr><tr>'+order.CreatedDate+'</tr></td>';
        ordercounter++;
        }
        allorderBody += '\n'+Label.vMarket_BaseURL+'/'+order.id+','+order.name+','+order.CreatedDate;
        }
        
        for(vMarketOrderItemLine__c orderItem : failedTransfers)
        {
        if(orderItem.createddate <=  timeNow && orderItem.createddate >= halfanhourago) 
        {
        orderItemBody += '<td><tr><a href=/"'+Label.vMarket_BaseURL+'/'+orderItem.id+'/">'+orderItem.name+'</a></tr><tr>'+orderItem.vMarket_Order_Item__r.CreatedDate+'</tr></td>';
        orderItemCounter++;
        }
        allorderItemBody += '\n'+Label.vMarket_BaseURL+'/'+orderItem.id+','+orderItem.name+','+orderItem.vMarket_Order_Item__r.CreatedDate;       
        }
        
        String mailBody = 'Hi, Please Find the Stripe failure Records as of '+System.now();
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();  
        
        if(ordercounter > 0) mailBody += '<table border/"1/">'+orderBody+'</table>';
        if(failedOrders.size()>0) attachments.add(vMarket_StripeAPIUtil.prepareCSVAttachment('all_failed_order.csv',allorderBody));
        if(orderItemCounter > 0) mailBody += '<table border/"1/">'+orderItemBody+'</table>';
        if(failedTransfers.size()>0) attachments.add(vMarket_StripeAPIUtil.prepareCSVAttachment('all_failed_orderItems.csv',allorderItemBody));
        
        mailbody = '<br>'+mailbody+'<br>Please do the Needful<br><br>Thanks';
        
        if(failedOrders.size()>0 || failedTransfers.size()>0) vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>(),'Stripe Failures as of '+System.now(),mailbody,'VMarket Admin',attachments);
                
        }
                        
}
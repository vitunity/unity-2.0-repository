@isTest
public with sharing class vMarketStripeFailureMailsTest {
    private static testMethod void vMarketStripeFailureMailsTest() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, false);
      User developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      vMarketAppAsset__c appAsset = vMarketDataUtility_Test.createAppAssetTest(app, true);
      
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      
      app.isActive__c = true;
      update app;
      order.status__C = 'Failed';
      update order;
      orderLinetem.transfer_status__C = 'failed';
      update orderLinetem;                            
      
      vMarketStripeFailureMails vsm = new vMarketStripeFailureMails();           
      
      String sch = '0 0 23 * * ?'; 
      system.schedule('Test vMarket Stripe Failure Mails', sch, vsm); 
      
      test.Stoptest();
    }
}
/**************************************************************************
    @ Author    : Nilesh Gorle
    @ Date      : 13-Feb-2017
    @ Description   : Checkout order page
****************************************************************************/
 /* Modified by Abhishek K as Part of Subscription VMT-26 */
public class vMarketSubscribeOrder {
    public String appId{get;set;}
    public Integer cartItemCount{get;set;}
    private List<String> appIdList = new List<String> {};
    public String address {get;set;}
    public Boolean orderCreated {get;set;}
    public Boolean addrs {get;set;}
    
      public String address1 {get;set;}
      public String devname {get;set;}
      public String appname {get;set;}
     // public String state {get;set;}
    public String state {get;set;}
    public String city {get;set;}
    public String country {get;set;}
    public String zipcode {get;set;}
    public String fullAddress {get;set;} //Only to dislay existing address
    public String customerId{get;set;}
    public String selectedAcctNum{get;set;}
    public String selectedAcctName{get;set;}
    public boolean AgreeACHTransfer{get;set;}
    public boolean AgreeAddAccount{get;set;}
      
    public Integer NumberItems{get;set;}
    public Decimal Total{get;set;}
    public Decimal Price{get;set;}
    public Decimal SubTotal{get;set;}
    public Double Tax{get;set;}
    public Decimal Tax_price{get;set;}
    public String CurrencyIsoCode{get;set;}
    public String currencySymb{get;set;}
    public vMarket_AppDO appdo{get;set;}
    public vMarketOrderItem__c orderItem{get;set;}
    public VMarket_Stripe_Subscription_Plan__c subPlan{get;set;}
     public MAP<String, String> MAP_COUNTRY;
    
    public vMarketCartItem cartItemObj;
  public String orderItemId{get;set;}
  public String planId{get;set;}
  
  public String vatNumber {get;set;}
  public boolean isCountryUS{get;set;}
  
    public vMarketSubscribeOrder() {
    //addrs = false;
       // cartItemObj = new vMarketCartItem();
        CurrencyIsoCode = 'USD';
        currencySymb = '$';
        isCountryUS=false;
        
        if(String.isNotblank(new vMarket_StripeAPIUtil().getUserCountry()) && new vMarket_StripeAPIUtil().getUserCountry().equals('US')) isCountryUS=true;
        
        
         MAP_COUNTRY = new  MAP<String, String>();
        MAP_COUNTRY.put('US', 'United States');
        
        appId=ApexPages.currentPage().getParameters().get('appId');
        orderItemId = ApexPages.currentPage().getParameters().get('orderid');
        planId = ApexPages.currentPage().getParameters().get('planid');
        //system.debug('SIZE----'+fetchAppListings(appId).size());
        appdo = new vMarket_AppDO(fetchAppListings(appId)[0]);
        appname = appdo.getName();
        devname = appdo.getOwnerName();
        vMarket_App__c app = [select Price__c From vMarket_App__c where Id =: AppId limit 1][0];
        subPlan = [Select id, VMarket_Price__c, VMarket_Duration_Type__c from VMarket_Stripe_Subscription_Plan__c where id=:planId];
        Price = (decimal) subPlan.VMarket_Price__c;//appdo.getPrice();
        SubTotal = (decimal) subPlan.VMarket_Price__c;//appdo.getPrice();
        System.debug('+++++'+Price);
        System.debug('+++++'+SubTotal);
        
        orderItem = [Select id,tax__c from vMarketOrderItem__c where id=:orderItemId];
        
        Tax = orderItem.Tax__c;
        Tax_Price = (Tax*SubTotal)/100;
        Total = SubTotal + Tax_Price;

      /*  if (app.Price__c!=null) { Price=(decimal) app.Price__c; 
        SubTotal = (decimal) app.Price__c;
        }*/     
        ordercreated = false;
        setFullAddressFromContact();
        //createOrder();
        
    }

    public Boolean getAuthenticated() {
        /*if(UserInfo.getUserType() != 'Guest') {
            return true;
        }
        return false;*/
        try {
      List<User> UserProfileList = [Select Id, vMarket_User_Role__c From User where id = : UserInfo.getUserId() limit 1];
      if (UserProfileList.size() > 0) {
        String Profile = UserProfileList[0].vMarket_User_Role__c;
            return (!Profile.contains('Guest') && Profile!=null) ? true : false;
      }
        } catch(Exception e){}
    return false;
    }
public List<SelectOption> getCountryList() {
        List<SelectOption> options = new List<SelectOption>();
        for(String key : MAP_COUNTRY.keySet()) {
            options.add(new SelectOption(key, MAP_COUNTRY.get(key))); 
        }
        return options;
    }
    /*
    * Checkout page will get displayed only in case of logged in user
    */
     public List<vMarket_Listing__c> fetchAppListings(String appId) {
        String FIELD_NAME = ' App__r.Name, App__r.App_Category__r.Name, App__r.ApprovalStatus__c, App__r.AppStatus__c, '
            + ' App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, '
            + ' App__r.Price__c, App__r.Published_Date__c, App__r.Short_Description__c,App__r.Owner.firstname,App__r.Owner.lastname, InstallCount__c,'
            + ' PageViews__c, (Select Rating__c From vMarket_Comments__r) ';

        String queryStr = 'SELECT' + FIELD_NAME +
                        + ' FROM vMarket_Listing__c '
                        + ' WHERE App__r.ApprovalStatus__c=\'Published\' AND App__r.IsActive__c=true AND App__r.id=\''+String.valueOf(appId)+'\'';
        return Database.query(queryStr);
    }
    
    public List<vMarket_AppDO> getCheckoutItems() {
        NumberItems = 0;Total = 0;SubTotal = 0;
        List<vMarket_AppDO> listingsDO = new List<vMarket_AppDO>();
        if(getAuthenticated()) {
            try {
                listingsDo = cartItemObj.getUserCartItemDetails(listingsDo);
                
                if (listingsDo!=null) {
                    Boolean isMatch = match_cartItem_with_orderItem(listingsDo);
                    if (!isMatch) {
                        // create new order
                        cartItemObj.createOrder();
                    }
    
                    vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
                    orderItemId = orderItem.Id;
                    if (orderItem.Id!=null) {
                        NumberItems = cartItemObj.NumberItems;
                        SubTotal = (decimal) orderItem.SubTotal__c;
                        Tax = Double.valueOf(orderItem.Tax__c);
                        Total = (decimal) orderItem.Total__c;
                        Total = Total.setScale(2);
                        Tax_price = (decimal)orderItem.Tax_price__c;
                    } else
                        listingsDo = null;
                }
            } catch (Exception e) {
                
                system.debug('------- getCheckoutItems  Exception --------'+String.valueOf(e));
            }
        } else {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'USER IS NOT LOGGED IN TO CHECKOUT'));
            listingsDo = null;
        }
        //if((listingsDo==null || NumberItems==0) && getAuthenticated())
        //    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'APP CART IS EMPTY'));

        return listingsDo;
    }
    
      public void selectedcustomer()
    {
    VMarketACH__c ach = [Select id, Account_Number__c, Bank_Name__c from VMarketACH__c where Customer__c=:customerId][0];
    selectedAcctNum = '*********'+String.valueof(ach.Account_Number__c);
    selectedAcctName = String.valueof(ach.Bank_Name__c);
    }


    public Boolean match_cartItem_with_orderItem(List<vMarket_AppDO> listingsDo) {
        List<vMarketOrderItemLine__c> orderItemLineList = getOrderItemLines();
        Integer orderItemSize;
        if(!orderItemLineList.isEmpty())
            orderItemSize = orderItemLineList.size();
        else
            return false;

        Integer cartItemSize = listingsDo.size();
        if (cartItemSize!=orderItemSize)
      return false;

        Map<String, String> orderItemObjMap = new Map<String, String>();
        // Iterate cart item object
        for(vMarketOrderItemLine__c oIObj : orderItemLineList) {
            orderItemObjMap.put(oIObj.vMarket_App__c, oIObj.vMarket_App__c);
        }

        // Iterate order item object
        for(vMarket_AppDO cIObj : listingsDo) {
            if (!orderItemObjMap.containsKey(cIObj.getAppId())) {
                return false;
            }   
        }

        return true;
    }

    public List<vMarketOrderItemLine__c> getOrderItemLines() {
      List<vMarketOrderItemLine__c> orderLineList = new List<vMarketOrderItemLine__c>();
        vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
        if (orderItem.Id!=null) {
            orderLineList = [Select Id, vMarket_App__c, vMarket_Order_Item__c  From vMarketOrderItemLine__c where vMarket_Order_Item__c=:orderItem.id];
            if (orderLineList.size() > 0)
              address = String.valueOf(orderItem.ShippingAddress__c);
                return orderLineList;
        }
        return orderLineList;
    }

 public void setFullAddressFromContact() {
 
    List<User> usr = [Select ContactId From User Where Id =: UserInfo.getUserId() limit 1];
    if (usr.size() > 0) {
      //List<Contact> cts = [Select ID, Email, MailingAddress From Contact Where Id =: usr[0].ContactId ];
      List<Contact> cts = [Select ID, Email, MailingCity, MailingState, MailingCountry, MailingPostalCode, MailingStreet From Contact Where Id =: usr[0].ContactId ];
      String MailingAddress;
      if (cts.size() > 0) {
        //Address address = (Address) cts[0].MailingAddress;
        
        Map<String, String> Country_Map = new Map<String, String>();
            Country_Map.put('USA', 'US');
            Country_Map.put('CAN', 'CA');
        
         //if (address != null) {
        addrs =true;
        address1 = cts[0].MailingStreet != null ? cts[0].MailingStreet:'';//address.getStreet();
           // street = addr.getStreet();
            city = cts[0].MailingCity != null ? cts[0].MailingCity:'';//address.getCity();
            state = cts[0].MailingState != null ? cts[0].MailingState:'';//address.getState();
            country = cts[0].MailingCountry != null ? cts[0].MailingCountry:'';//address.getCountry();
            if (country!=null) country = Country_Map.get(country);
            zipcode = cts[0].MailingPostalCode != null ? cts[0].MailingPostalCode:'';//address.getPostalCode();
           // fullAddress = AddrStreet+', '+AddrCity+', '+AddrState+', '+AddrPostalCode+', '+AddrCountry;
        //}
      }
    }
  }


    @RemoteAction
    public static String StripeRequest(String tokenId) {
       // vMarketCartItem cartItemObj = new vMarketCartItem();
        vMarketOrderItem__c orderItem = checkOrderItem();//cartItemObj.checkOrderItem();
        Decimal total = (decimal) orderItem.Total__c;
    
    Boolean status = vMarket_StripePayment.StripeRequest2(tokenId, total, orderItem);
    
   // orderItemId = orderItem.Id;
        system.debug(' === CONTROLLER STATUS == ' + status);
        if(status == false) {
            system.debug('IF');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'Something went wrong, Please try later'));
        } else {
            system.debug('ELSE');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Transaction Successfull'));
        }
        return String.valueOf(orderItem.Id);        
    }

 public static vMarketOrderItem__c checkOrderItem() {
        vMarketOrderItem__c orderItem = new vMarketOrderItem__c();
        List<vMarketOrderItem__c> orderList = [Select ShippingAddress__c, Status__c, SubTotal__c, Tax__c, TaxDetails__c, Total__c, ZipCode__c, Name, Tax_price__c From vMarketOrderItem__c where Owner.id=:UserInfo.getUserId() and (Status__c=null or Status__c='') and IsSubscribed__c=true];
        if (orderList.size() > 0) {
            orderItem = orderList[0];
        }
        return orderItem;
    }

    public void retrieveTax(vMarketOrderItem__c order,Boolean isInsert) {
        vMarket_Tax_Calculator vMarketTaxObj = new vMarket_Tax_Calculator();
        vMarketTaxObj.getTaxRate(CurrencyIsoCode, SubTotal, zipcode, country,order,isInsert);
       // return (Tax!=null || Tax!=0) ? Tax : 0;
    }

    public vMarketOrderItem__c createNewOrderItem() {
        vMarketOrderItem__c orderItem = new vMarketOrderItem__c();
        orderItem.ShippingAddress__c = (address1+', '+city+', '+state+', '+zipcode+', '+country);
        orderItem.ZipCode__c = zipcode;
        orderItem.issubscribed__C = true;
      //  insert orderitem;
        retrieveTax(orderItem,true);
             //   orderItem.Tax__c = retrieveTax();
 //       insert orderItem;
 
        return orderItem;
    }

    public void updateOrderTax(vMarketOrderItem__c orderItem) {
        //orderItem.Tax__c = retrieveTax();
        //update orderItem;
        retrieveTax(orderItem,false);
    }

    public void deleteOrderItemLineByAppId(Id AppId) {
        vMarketOrderItem__c orderItem = checkOrderItem();
        if (orderItem.Id!=null) {
            List<vMarketOrderItemLine__c> orderLineList = [Select Id From vMarketOrderItemLine__c where vMarket_App__c=:AppId AND vMarket_Order_Item__c=:orderItem.id];
            if (orderLineList.size() > 0)
                delete orderLineList;
        }
    }

    public Boolean deleteOrderItemLinesByOrderId(vMarketOrderItem__c orderItem) {
        List<vMarketOrderItemLine__c> orderLineList = [Select Id From vMarketOrderItemLine__c where vMarket_Order_Item__c=:orderItem.id];
        if (!orderLineList.isEmpty()) {
            try {
                delete orderLineList;
            } catch (DmlException e) {
                system.debug('Order Lines not getting deleted');
                return false;
            }
        }
        return true;
    }

    public void addOrderItemLine(Id appId, Id orderItemId, Decimal price) {
        vMarketOrderItemLine__c orderItemLine = new vMarketOrderItemLine__c(vMarket_App__c=appId, vMarket_Order_Item__c=orderItemId, Price__c=price, IsSubscribed__c=true);
        insert orderItemLine;
    }

    public vMarketOrderItem__c updateOrderAddress(vMarketOrderItem__c orderItem) {
        orderItem.ShippingAddress__c = (address1+', '+city+', '+state+', '+zipcode+', '+country);
        orderItem.ZipCode__c = zipcode;
        //orderItem.Tax__c = retrieveTax();
//        update orderItem;
        retrieveTax(orderItem,false);
        return orderItem;
    }

  public String getOrderId()
  {
  return orderItemId;
  }
  
  public PageReference updateaddr()
  {
  
  orderItemId = String.valueOf(orderItem.id);
  
  //orderItem = updateOrderAddress(orderItem);
  orderItem.ShippingAddress__c = (address1+', '+city+', '+state+', '+zipcode+', '+country);
        orderItem.ZipCode__c = zipcode;
        update orderItem;
  
 return null;
  }
  
   public List<SelectOption> getAccountsList() {
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('','')); 
        for(VMarketACH__c ach : [Select id,name,Customer__c,Bank__c,Status__c from VMarketACH__c where user__C=:userinfo.getuserid() AND Status__c = 'Verified']) {
            options.add(new SelectOption(ach.Customer__c,ach.name)); 
        }
        return options;
    }
  

    public PageReference createOrder() {
    
    try{
    if(!ordercreated)
    {
        vMarketOrderItem__c orderItem = checkOrderItem();
        if (orderItem.Id!=null) {
          //  if (address1!=null && city!=null && state!=null && zipcode!=null)
           //     orderItem = updateOrderAddress(orderItem);

            // Delete Existing Order Lines, Reason is
            // Developer can change price of App anytime and
            // suppose customer already created order two days back and did not proceed with old order yet. then Order will take old price, in case of new app price
            Boolean isOrderLinesDeleted = deleteOrderItemLinesByOrderId(orderItem);
            if (!isOrderLinesDeleted)
                system.debug('Existing order item lines not getting deleted for user'+UserInfo.getUserEmail());
        } else {
            // create new OrderItem 
            orderItem = createNewOrderItem();
       }
       
       //retrieveTax(orderItem,false);
       fullAddress = address1+', '+city+', '+state+', '+zipcode+', '+country;
       
        orderItemId = String.valueOf(orderItem.id);
        // Add Latest Order items in vMarketOrderItemLine__c object
       
        addOrderItemLine(appId, orderItem.id, Price);
        
      //  vMarket_Tax_Calculator vMarketTaxObj = new vMarket_Tax_Calculator();
      //  vMarketTaxObj.getTaxRate(CurrencyIsoCode, SubTotal, zipcode, country);
        Tax = orderItem.Tax__c;
        Tax_Price = (Tax*SubTotal)/100;
        Total = SubTotal + Tax_Price;
        ordercreated = true;
        //PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl()+'/'+appId);
        //pageRef.setRedirect(false);
        return null;
        
        }return null;
          }
            catch(Exception e)
            {
            vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'prince.abhishek16@gmail.com'},'Stripe Transfers Code Failure',e.getMessage()+e.getStackTraceString(),'VMS Error');
            vMarket_LogController.createCodeErrorLogs('Code Failed in vMarket_StripeAccount',e.getMessage());
            return null;
            }

    }
    /* Added by Abhishek K to Create Subscription in Stripe*/
    @RemoteAction
    public static Id createCustomer(String tokenId)
    {
    vMarket_StripeAPIUtil util = new vMarket_StripeAPIUtil();
    vMarketStripeDetail__c stripeDetail = util.createCustomerStripe(tokenId); 
    return stripeDetail.id;
    }
       /* Added by Abhishek K to Create Stripe Customer*/
    @RemoteAction
    public static id createStripeCustomer(String custId,String last4)
    {
    vMarketStripeDetail__c stripeDetail = new vMarketStripeDetail__c(StripeAccountId__c=custId, User__c=UserInfo.getUserId(), Details__c=last4);
        insert stripeDetail;
     return stripeDetail.id;   
    }
    
     public PageReference updateOrderasFailed() 
    {
    vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
    orderItem.Charge_Id__c = ApexPages.CurrentPage().getParameters().get('chargeId');
    orderItem.Status__c = 'Failed';
    orderItem.OrderPlacedDate__c = Date.today();
    orderItem.ShippingAddress__c = ApexPages.CurrentPage().getParameters().get('address');
    update orderItem;
    cartItemObj.removeAllUserCartItem();
    return new PageReference('/vMarketOrderDetail?Id='+orderItem.id);
    
    }
            
   /* @RemoteAction
    public static id createSubscriptionStripeCustomer(String custId,Id planId,Id orderId,String last4) {
        vMarket_StripeAPIUtil util = new vMarket_StripeAPIUtil();
        
        //vMarketStripeDetail__c stripeDetail = [Select id,StripeAccountId__c from vMarketStripeDetail__c where id=:detailId];
        VMarket_Stripe_Subscription_Plan__c plan = [Select id, VMarket_Stripe_Subscription_Plan_Id__c from VMarket_Stripe_Subscription_Plan__c where id=:planId];
        vMarketOrderItem__c order = [Select id, VMarket_Subscriptions__c from vMarketOrderItem__c where id=:orderId];
        
        VMarket_Subscriptions__c subscription = util.createSubscription(plan,stripeDetail);
        order.VMarket_Subscriptions__c = subscription.id;
        order.status__C = 'Success';
        update order;
        System.debug('-----Subscription Details------'+subscription);
        return order.id;
    }*/
                
            /* Added by Abhishek K to Create Subscription Stripe*/  
    @RemoteAction
    public static id createSubscriptionStripe(Id detailId,Id planId,Id orderId,boolean ach,String vatNumber) {
        vMarket_StripeAPIUtil util = new vMarket_StripeAPIUtil();
        vMarketStripeDetail__c stripeDetail = [Select id,StripeAccountId__c from vMarketStripeDetail__c where id=:detailId];
        VMarket_Stripe_Subscription_Plan__c plan = [Select id, VMarket_Stripe_Subscription_Plan_Id__c, VMarket_Duration_Type__c from VMarket_Stripe_Subscription_Plan__c where id=:planId];
        vMarketOrderItem__c order = [Select id, VMarket_Subscriptions__c,VAT_Number__c from vMarketOrderItem__c where id=:orderId];
        
        VMarket_Subscriptions__c subscription = util.createSubscription(plan,stripeDetail);
        order.VMarket_Subscriptions__c = subscription.id;
        //if(!ach) order.status__C = 'Success';
        order.status__C = 'Pending';
        order.VMarket_Duration_Type__c = plan.VMarket_Duration_Type__c;
        order.OrderPlacedDate__c = date.today();
        //Added by Harshal for VAT Number
        if(vatNumber!=null || vatNumber!=''){
            order.VAT_Number__c=vatNumber;
        }
        update order;
        System.debug('-----Subscription Details------'+subscription);
        return order.id;
    }
      /* Added by Abhishek K to Create Subscription*/
    @RemoteAction
    public static id createSubscription(String tokenId, Id planId,Id orderId,String vatNumber) {
        vMarket_StripeAPIUtil util = new vMarket_StripeAPIUtil();
        //vMarketStripeDetail__c stripeDetail = util.createCustomerStripe(tokenId);
        //System.debug('-----Customer Stripe Details------'+stripeDetail);
        VMarket_Stripe_Subscription_Plan__c plan = [Select id, VMarket_Duration_Type__c,VMarket_Stripe_Subscription_Plan_Id__c from VMarket_Stripe_Subscription_Plan__c where id=:planId];
        vMarketOrderItem__c order = [Select id,status__C,VMarket_Duration_Type__c,OrderPlacedDate__c,VMarket_Subscriptions__c,VAT_Number__c from vMarketOrderItem__c where id=:orderId];
        VMarket_Subscriptions__c subscription = util.createSubscription(plan,tokenId);//util.createSubscription(plan,stripeDetail);
        order.VMarket_Subscriptions__c = subscription.id;
        order.status__C = 'Pending';
        order.VMarket_Duration_Type__c = plan.VMarket_Duration_Type__c;
        order.OrderPlacedDate__c = date.today();
        //Added by Harshal for VAT Number
        if(vatNumber!=null || vatNumber!=''){
            order.VAT_Number__c=vatNumber;
        }
        update order;
        
        return orderId;
        //System.debug('-----Subscription Details------'+subscription);
    }
    
   public static void Dummy() {
       List<Integer> inte = new List<Integer>();
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
       inte.add(1);
   }

}
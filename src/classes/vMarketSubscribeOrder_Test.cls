/**
 *  @author         :       Puneet Mishra
 *  @description    :       test class for vMarketSubscribeOrder
 */
@isTest
public with sharing class vMarketSubscribeOrder_Test {
    
    public static String tax = '{  '+
                               '"RETURN":{ '+ 
                                  '"TYPE":"",'+
                                  '"ID":"",'+
                                  '"NUMBER":"000",'+
                                  '"MESSAGE":"",'+
                                  '"LOG_NO":"",'+
                                  '"LOG_MSG_NO":"000000",'+
                                  '"MESSAGE_V1":"",'+
                                  '"MESSAGE_V2":"",'+
                                  '"MESSAGE_V3":"",'+
                                  '"MESSAGE_V4":"",'+
                                  '"PARAMETER":"",'+
                                  '"ROW":0,'+
                                  '"FIELD":"",'+
                                  '"SYSTEM":""'+
                               '},'+
                               '"TAX_DATA":[  '+
                                  '{  '+
                                     '"INVOICE_ID":"",'+
                                     '"ITEM_NO":"0",'+
                                     '"TAX_TYPE":"TJ",'+
                                     '"TAX_NAME":"Tax_Jur_Code_Level_1",'+
                                     '"TAX_CODE":"000001710",'+
                                     '"TAX_RATE":"6.25",'+
                                     '"TAX_AMOUNT":"31.25",'+
                                     '"TAX_JUR_LVL":""'+
                                  '},'+
                                  '{  '+
                                     '"INVOICE_ID":"",'+
                                     '"ITEM_NO":"0",'+
                                     '"TAX_TYPE":"TJ",'+
                                     '"TAX_NAME":"Tax_Jur_Code_Level_2",'+
                                     '"TAX_CODE":"000001710",'+
                                     '"TAX_RATE":"1",'+
                                     '"TAX_AMOUNT":"5",'+
                                     '"TAX_JUR_LVL":""'+
                                  '},'+
                                  '{  '+
                                     '"INVOICE_ID":"",'+
                                     '"ITEM_NO":"0",'+
                                     '"TAX_TYPE":"TJ",'+
                                     '"TAX_NAME":"Tax_Jur_Code_Level_4",'+
                                     '"TAX_CODE":"000001710",'+
                                     '"TAX_RATE":"1.75",'+
                                     '"TAX_AMOUNT":"8.75",'+
                                     '"TAX_JUR_LVL":""'+
                                  '}'+
                               ']'+
                            '}';
    public static string token='{'+
            '  "id": "tok_1BLV3VL6mRt8fBaVeCKGG3ug",'+
            '  "object": "token",'+
            '  "card": {'+
            '    "id": "card_1BLV3VL6mRt8fBaVmjiygNEG",'+
            '    "object": "card",'+
            '    "address_city": null,'+
            '    "address_country": null,'+
            '    "address_line1": null,'+
            '    "address_line1_check": null,'+
            '    "address_line2": null,'+
            '    "address_state": null,'+
            '    "address_zip": null,'+
            '    "address_zip_check": null,'+
            '    "brand": "Visa",'+
            '    "country": "US",'+
            '    "cvc_check": null,'+
            '    "dynamic_last4": null,'+
            '    "exp_month": 8,'+
            '    "exp_year": 2018,'+
            '    "fingerprint": "dUcEqJaRkwdEBTzL",'+
            '    "funding": "credit",'+
            '    "last4": "4242",'+
            '    "metadata": {'+
            '    },'+
            '    "name": null,'+
            '    "tokenization_method": null'+
            '  },'+
            '  "client_ip": null,'+
            '  "created": 1510053345,'+
            '  "livemode": false,'+
            '  "type": "card",'+
            '  "used": false'+
            '}';
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2;
    static List<vMarket_App__c> appList;
   
   static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
  
  static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
  
  static Profile admin,portal;   
     static User customer1, customer2, developer1, ADMIN1;
     static List<User> lstUserInsert;
     static Account account;
  static Contact contact1, contact2, contact3;
     static List<Contact> lstContactInsert;
     
     static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
    
    static VMarket_Stripe_Subscription_Plan__c s_plan;
  
  static {
    admin = vMarketDataUtility_Test.getAdminProfile();
      portal = vMarketDataUtility_Test.getPortalProfile();
      
      account = vMarketDataUtility_Test.createAccount('test account', false);
      account.Ext_Cust_Id__c = '6051213';
      insert account;
      
      ERP_Partner__c erpPartner = new ERP_Partner__c();
      erpPartner.Name = '21C/AMBERGRIS LLC';
      erpPartner.Active__c = true;
      erpPartner.Account_Group__c = 'Ship To';
      erpPartner.Street__c = '2000 FOUNDATION WY, SUITE 1100';
      erpPartner.City__c = 'MARTINSBURG';
      erpPartner.State_Province_Code__c = 'WV';
      erpPartner.Zipcode_Postal_Code__c = '25401-9003';
      erpPartner.Country_Code__c = 'US';
      erpPartner.Partner_Number__c = '2324449';
      erpPartner.Partner_Name__c = '21C/AMBERGRIS LLC';
      insert erpPartner;
      
      ERP_Partner_Association__c erpAssociation = new ERP_Partner_Association__c();
      erpAssociation.Name = '6051213#2324449#0601#SH=Ship-to party';
      erpAssociation.Sales_Org__c = '0601';
      erpAssociation.ERP_Partner__c = erpPartner.Id;
      erpAssociation.Partner_Function__c = 'BP=Bill-to Party';
      erpAssociation.Customer_Account__c = account.Id;
      erpAssociation.ERP_Reference__c = '6051213#2324449#0601#SH=Ship-to party';
      erpAssociation.ERP_Customer_Number__c = '6051213';
      erpAssociation.ERP_Partner_Number__c = '2324449';
      erpAssociation.Delivery_Priority_Default__c = '03-Normal';
      erpAssociation.Shipping_Condition_Default__c = '03-Normal / Regular';
      insert erpAssociation;
      
      lstContactInsert = new List<Contact>();
      contact1 = vMarketDataUtility_Test.contact_data(account, false);
      contact1.MailingStreet = '53 Street';
      contact1.MailingCity = 'Palo Alto';
      contact1.MailingCountry = 'USA';
      contact1.MailingPostalCode = '94035';
      contact1.MailingState = 'CA';
      
    contact2 = vMarketDataUtility_Test.contact_data(account, false);
    contact2.MailingStreet = '660 N';
      contact2.MailingCity = 'Milpitas';
      contact2.MailingCountry = 'USA';
      contact2.MailingPostalCode = '94035';
      contact2.MailingState = 'CA';
      
      lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
      
      lstContactInsert.add(contact1);
      lstContactInsert.add(contact2);
      insert lstContactInsert;
      
      lstUserInsert = new List<User>();
      lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
    lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
      lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
      ADMIN1 = new User(Email = 'test_Admin@bechtel.com', Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN', 'vMarket', 70), 
        LastName = 'test', IsActive = true, FirstName = 'Tester', Alias = 'teMar', ProfileId = admin.Id, LanguageLocaleKey = 'en_US', 
        LocaleSidKey = 'en_US', TimeZoneSidKey = 'America/Chicago', EmailEncodingKey = 'UTF-8', vMarketTermsOfUse__c = true, 
        vMarket_User_Role__c = 'Customer');
      lstUserInsert.add(ADMIN1);
      insert lstUserinsert;
      
      cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
      app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
      app1.IsActive__c = true;
      app1.ApprovalStatus__c = 'Published';
      
      app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
      
      appList = new List<vMarket_App__c>();
      appList.add(app1);
      appList.add(app2);
      
      insert appList;
      
      listing = vMarketDataUtility_Test.createListingData(app1, true);
      comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
      activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
      
      attachment attch = new attachment(Name = 'test', parentId = app1.Id, body = blob.valueOf('123') );
      insert attch;
      
      source = vMarketDataUtility_Test.createAppSource(app1, true);
      feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
      
      orderItemList = new List<vMarketOrderItem__c>();
      system.runAs(Customer1) {
        orderItem1 = vMarketDataUtility_Test.createOrderItem(true, false); // Subscribed Order
        orderItem1.TaxDetails__c = tax;
        insert orderItem1;
      }
      system.runAs(Customer2) {
        orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
      }
      
      orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
      orderItemLine2 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
        
        system.runas(customer1) {
            cart1 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        system.runas(customer2) {
            cart2 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        cartLineList = new List<vMarketCartItemLine__c>();
        cartLine1 = vMarketDataUtility_Test.createCartItemLines(app1, cart1, false);
        cartLine2 = vMarketDataUtility_Test.createCartItemLines(app2, cart1, false);
        cartLineList.add(cartLine1);
        cartLineList.add(cartLine2);
        insert cartLineList;
        
        //test insert
        s_plan = new VMarket_Stripe_Subscription_Plan__c(vMarket_App__c=appList[0].id,VMarket_Stripe_Subscription_Plan_Id__c=string.valueof(appList[0].id)+'yearly',VMarket_Duration_Type__c ='yearly',VMarket_Price__c=2005.123);
        insert s_plan;
    }
  
    public static testMethod void testMethod1() {
        Test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
             System.currentPageReference().getParameters().put('planid',s_plan.id);
        
            vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
        Test.StopTest();
    }
    
    
    public static testMethod void checkOrderItem() {
        Test.StartTest();
            
            Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
            System.currentPageReference().getParameters().put('planid',s_plan.id);
            
            listing = [SELECT App__r.Name, App__r.App_Category__r.Name, App__r.ApprovalStatus__c, App__r.AppStatus__c, 
            App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, App__r.Price__c, App__r.Published_Date__c, 
            App__r.Short_Description__c,App__r.Owner.firstname,App__r.Owner.lastname, InstallCount__c,
            PageViews__c, (Select Rating__c From vMarket_Comments__r) FROM vMarket_Listing__c WHERE Id =: listing.Id];
            
            system.runas(ADMIN1) {
                orderItem1.Status__c = '';
                update orderItem1;
            }
             vMarketSubscribeOrder.checkOrderItem();
                
                vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
                ord.retrieveTax(orderItem1, false);
            
        Test.StopTest();
    }
    
    public static testMethod void createNewOrderItem() {
        Test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
            System.currentPageReference().getParameters().put('planid',s_plan.id);
            vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
            ord.createNewOrderItem();
            //vMarketSubscribeOrder.createCustomer('tok_1BLV3VL6mRt8fBaVeCKGG3ug');
        Test.StopTest();
    }
    
    public static testMethod void updateOrderTax() {
        Test.StartTest();
            
                Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
             System.currentPageReference().getParameters().put('planid',s_plan.id); 
                
                vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
                ord.updateOrderTax(orderItem1);
                
                ord.addOrderItemLine(app1.Id, orderItem1.Id, 500);
                
                ord.deleteOrderItemLinesByOrderId(orderItem1);
            
        Test.StopTest();
    }
    
    public static testMethod void updateOrderAddress() {
        Test.StartTest();
            
                Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
             System.currentPageReference().getParameters().put('planid',s_plan.id);
                 
                vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
                ord.address1 = 'TEST';
                ord.city = 'PA';
                ord.state = 'CA';
                ord.zipcode = '95035';
                ord.country = 'USA';
                
                ord.updateOrderAddress(orderItem1);
                
                ord.getOrderId();
                ord.updateaddr();
            
        Test.STopTest();
    }
    
    public static testMethod void getCountryList() {
        Test.StartTest();
              //test insert
          Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
             System.currentPageReference().getParameters().put('planid',s_plan.id);
            vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
              
                vMarketSubscribeOrder ordcnt = new vMarketSubscribeOrder();
                ord.getCountryList();
                
                ord.getAuthenticated();
            
        Test.StopTest();
    }
    
    public static testMethod void ordercreated() {
        Test.StartTest();
            
        Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
        System.currentPageReference().getParameters().put('appId', app1.Id);
        System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
         System.currentPageReference().getParameters().put('planid',s_plan.id);
        
        ApexPages.CurrentPage().getParameters().put('id',s_plan.id);
        vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
        ord.ordercreated = false;
        ord.orderItem = orderItem1;
        
        ord.createOrder();
            
        Test.StopTest();
    }
    
    public static testMethod void getCheckoutItems() {
        Test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
            System.currentPageReference().getParameters().put('planid',s_plan.id);
                vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
                ord.AgreeACHTransfer=false;
                ord.AgreeAddAccount=true;
                ord.vatNumber='123343';
                ord.cartItemCount=2;
                ord.address='CA Palo Alto';
                ord.getCheckoutItems();
        Test.Stoptest();
    }
    
    
    public static testMethod void Dummy(){
        
        vMarketSubscribeOrder.Dummy();
       
    }
    
    private static testMethod void achTest() {
        Test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
            System.currentPageReference().getParameters().put('planid',s_plan.id);
            VMarketACH__c ach=new VMarketACH__c();
            ach.name='Test User';
            ach.Bank__c='ba_1CCg1gL6mRt8fBaVKhBmPfHP';
            ach.Bank_Name__c='TEST BANK';
            ach.customer__c='cus_CbtpcEdRXsVy2U';
            ach.Status__c='Verified';
            ach.Received_Deposit_1__c=10;
            ach.Received_Deposit_2__c=20;
            ach.Transferred_Deposit_1__c=10;
            ach.Transferred_Deposit_2__c=20;
        	ach.user__c=userinfo.getuserid();
            insert ach;
            vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
            ord.customerId='cus_CbtpcEdRXsVy2U';
            //Boolean res = ord.match_cartItem_with_orderItem(new List<vMarket_AppDo>());
            ord.selectedcustomer();
            ord.getAccountsList();
        Test.StopTest();
    }
    
    private static testMethod void deleteOrderItemLineByAppIdTest(){
        Test.StartTest();
            system.runAs(Customer1) {
                Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
                System.currentPageReference().getParameters().put('appId', app1.Id);
                System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
                System.currentPageReference().getParameters().put('planid',s_plan.id);
                vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
                ord.Tax=0.0;
                ord.Tax_price=100.00;
                ord.CurrencyIsoCode='US';
                ord.cartItemObj=new vMarketCartItem();
                ord.orderItemId=orderItem1.id;
                ord.address1 ='palo alto';
                ord.state='CA';
                ord.city='San Jose';
                ord.country ='US';
                ord.zipcode='ZA16762';
                ord.deleteOrderItemLineByAppId(app1.Id);
            }
         Test.StopTest();
    }
    
    private static testMethod void deleteOrderItemLinesByOrderIdTest(){
        Test.StartTest();
            
            Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
            System.currentPageReference().getParameters().put('planid',s_plan.id);
            vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
            ord.deleteOrderItemLinesByOrderId(orderItem1);
         Test.StopTest();
    }
    
   /*private static testMethod void createCustomerTest(){
        Test.StartTest();
            Test.setMock(HttpCalloutMock.class, new VMarketStripeSubscriptionHandlerMock());
            vMarketSubscribeOrder.createSubscription('sk_test_9YAb7QbJ2WrzFKYDhgjANxPf');
         Test.StopTest();
    }*/
    
    /*private static testMethod void match_cartItem_with_orderItem2_TEST() {
        Test.StartTest();
                Test.setCurrentPageReference(new PageReference('Page.vMarketSubscribeOrderInfo'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('orderid', orderItem1.Id);
                
                List<vMarket_AppDo> appdo = new List<vMarket_AppDo>();
                vMarket_AppDo appd = new vMarket_AppDo(listing);
                appdo.add(appd);
                
                vMarketSubscribeOrder ord = new vMarketSubscribeOrder();
                Boolean res = ord.match_cartItem_with_orderItem(appdo);
            
        Test.StopTest();
    }*/
    
}
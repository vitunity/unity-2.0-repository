Public class vMarketWebHooksScheduler implements Schedulable {



    public void execute(SchedulableContext ctx) 
    {
    List<VMarket_WebHooks_Order__c> webordersTobeUpdated = new List<VMarket_WebHooks_Order__c>();
    for( VMarket_WebHooks_Order__c weborder :[Select VMarket_Customer_Id__c , VMarket_Subscription_Id__c, VMarket_Invoice_Id__c ,VMarket_Processed__c, VMarket_ACH_Reason__c, VMarket_Charge_Id__c, VMarket_Processing_Date__c, VMarket_Received_Date__c, VMarket_Status__c, VMarket_Subscription__c, VMarket_Full_Response__c from VMarket_WebHooks_Order__c where VMarket_Processed__c=false])
    {
    
        if(!weborder.VMarket_Subscription__c){
        List< vMarketOrderItem__c > orderItems = [Select id, Status__c, Charge_Id__c from vMarketOrderItem__c where Charge_Id__c=:weborder.VMarket_Charge_Id__c and Status__c='Pending']; 
        System.debug('&&&'+orderItems.size());
        if( orderItems.size() > 0 )
        {
         
        orderItems[0].Status__c = weborder.VMarket_Status__c;
        orderItems[0].VMarket_ACH_Failure_Reason__c = weborder.VMarket_Full_Response__c;
        update orderItems[0];
        }
        
        
        
        weborder.VMarket_Processed__c = true;
        weborder.VMarket_Processing_Date__c = System.Now();
        webordersTobeUpdated.add(weborder);
        }                
        else
        {
        
      /*  Map<String, Object> cObjMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(weborder.VMarket_Full_Response__c),string.class);
        
        Map<String, Object> cObjMap1 = (Map<String, Object>) cObjMap.get('data');
        
        
        
        Map<String, Object> cObjMap2 = (Map<String, Object>) cObjMap1.get('object');
        String status = String.valueof(cObjMap2.get('paid'));
        List<Object> objlist = (List<Object>)( ((Map<String, Object>)cObjMap2.get('lines')).get('data'));
        String subscriptionId = String.valueof( ((Map<String, Object>)objlist.get(0)).get('id') );//(cObjMap2.get('id'));
        String customerId = String.valueof(cObjMap2.get('customer'));       
        String chargeId = String.valueof(cObjMap2.get('charge'));
        String invId = String.valueof(cObjMap2.get('id'));
        */
        String customerId = weborder.VMarket_Customer_Id__c;
        String subscriptionId = weborder.VMarket_Subscription_Id__c;
        String chargeId = weborder.VMarket_Charge_Id__c;
        String invId = weborder.VMarket_Invoice_Id__c;
        List<VMarket_Subscriptions__c> subscriptions = [select id, VMarket_Stripe_Subscription_Id__c, VMarket_Stripe_Detail__c from VMarket_Subscriptions__c where VMarket_Stripe_Subscription_Id__c=:subscriptionId];
        
        VMarket_Subscriptions__c subscriptionItem = new VMarket_Subscriptions__c();
        
        if(subscriptions.size()>0) subscriptionItem = subscriptions[0];
        
        System.debug(weborder.VMarket_Charge_Id__c+'**sub'+subscriptionItem);
        
    
        
        // Find Order Based on the Subscription
        List<vMarketOrderItem__c> orderList = [Select id, VMarket_Subscriptions__c from vMarketOrderItem__c where VMarket_Subscriptions__c=:subscriptionItem.id];
        
        if(orderList.size() > 0)
        {
        vMarketOrderItem__c order = orderList[0];
        // Create a Child Record based on the Data
        
        
        VMarket_Subscription_Payments__c payment = new VMarket_Subscription_Payments__c(VMarket_Invoice_Date__c=date.today(), vMarket_Order_Item__c =order.id,VMarket_Stripe_Customer_Id__c=customerId, VMarket_Stripe_Invoice_Id__c=invId, VMarket_Stripe_Payment_Id__c=chargeId, VMarket_Stripe_Payment_Status__c='Paid', VMarket_Stripe_Subscription_Id__c=subscriptionId);
        if(weborder.VMarket_Status__c.containsignorecase('success')) payment.VMarket_Stripe_Payment_Status__c = 'Paid';
        else payment.VMarket_Stripe_Payment_Status__c = 'Failed';
        insert payment;
        
        
        
        //System.debug('&&&'+orderItems.size());
        if( order!=null )
        {
        
        order.Status__c = weborder.VMarket_Status__c;
        order.VMarket_ACH_Failure_Reason__c = weborder.VMarket_Full_Response__c;        
        update order;
        
        }
        
        }
        weborder.VMarket_Processed__c = true;
        weborder.VMarket_Processing_Date__c = System.Now();
        webordersTobeUpdated.add(weborder);
        
        }
    
    }
    if(webordersTobeUpdated.size()>0) update webordersTobeUpdated; 
        
    }
    
}
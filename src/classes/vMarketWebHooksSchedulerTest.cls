@isTest
public with sharing class vMarketWebHooksSchedulerTest {
    public static String tax = '{  '+
                                   '"RETURN":{  '+
                                      '"TYPE":"",'+
                                      '"ID":"",'+
                                      '"NUMBER":"000",'+
                                      '"MESSAGE":"",'+
                                      '"LOG_NO":"",'+
                                      '"LOG_MSG_NO":"000000",'+
                                      '"MESSAGE_V1":"",'+
                                      '"MESSAGE_V2":"",'+
                                      '"MESSAGE_V3":"",'+
                                      '"MESSAGE_V4":"",'+
                                      '"PARAMETER":"",'+
                                      '"ROW":0,'+
                                      '"FIELD":"",'+
                                      '"SYSTEM":""'+
                                   '},'+
                                   '"TAX_DATA":[  '+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-1",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 6.25",'+
                                         '"TAX_AMOUNT":" 268.6875",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-2",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1",'+
                                         '"TAX_AMOUNT":" 42.99",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-4",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1.75",'+
                                         '"TAX_AMOUNT":" 75.2325",'+
                                         '"TAX_JUR_LVL":""'+
                                      '}'+
                                   ']'+
                                '}';
    
    public static String respStr='{'+
                    '   "id":"evt_1CQTwNL6mRt8fBaV1Tl1W845",'+
                    '   "object":"event",'+
                    '   "api_version":"2017-08-15",'+
                    '   "created":1526017035,'+
                    '   "data":{'+
                    '      "object":{'+
                    '         "id":"ch_1CQTwML6mRt8fBaVBWY4YrdS",'+
                    '         "object":"charge",'+
                    '         "amount":150000,'+
                    '         "amount_refunded":0,'+
                    '         "application":null,'+
                    '         "application_fee":"fee_1CQTwMJacBf6YgznyCs8YFPX",'+
                    '         "balance_transaction":"txn_1CQTwNL6mRt8fBaVoZEOvCs3",'+
                    '         "captured":true,'+
                    '         "created":1526017034,'+
                    '         "currency":"usd",'+
                    '         "customer":null,'+
                    '         "description":"OLID-2475-",'+
                    '         "destination":"acct_1C9tspJacBf6Ygzn",'+
                    '         "dispute":null,'+
                    '         "failure_code":null,'+
                    '         "failure_message":null,'+
                    '         "fraud_details":{'+
                    '         },'+
                    '         "invoice":null,'+
                    '         "livemode":false,'+
                    '         "metadata":{'+
                    '            "application_fee":"600",'+
                    '            "description":"OLID-2475-",'+
                    '            "stripefee":"43.80",'+
                    '            "TOTALPRICE_INCLU_TAX":"1500.00",'+
                    '            "TAX_PRICE":"0.00",'+
                    '            "Tax_Jur_Code_Level_1":" 0",'+
                    '            "Tax_Jur_Code_Level_1_TAX_CODE":"000001710",'+
                    '            "Tax_Jur_Code_Level_2":" 0",'+
                    '            "Tax_Jur_Code_Level_2_TAX_CODE":"000001710",'+
                    '            "Tax_Jur_Code_Level_4":" 0",'+
                    '            "Tax_Jur_Code_Level_4_TAX_CODE":"000001710"'+
                    '         },'+
                    '         "on_behalf_of":"acct_1C9tspJacBf6Ygzn",'+
                    '         "order":null,'+
                    '         "outcome":{'+
                    '            "network_status":"approved_by_network",'+
                    '            "reason":null,'+
                    '            "risk_level":"normal",'+
                    '            "seller_message":"Payment complete.",'+
                    '            "type":"authorized"'+
                    '         },'+
                    '         "paid":true,'+
                    '         "receipt_email":null,'+
                    '         "receipt_number":null,'+
                    '         "refunded":false,'+
                    '         "refunds":{'+
                    '            "object":"list",'+
                    '            "data":['+
                    '            ],'+
                    '            "has_more":false,'+
                    '            "total_count":0,'+
                    '            "url":"/v1/charges/ch_1CQTwML6mRt8fBaVBWY4YrdS/refunds"'+
                    '         },'+
                    '         "review":null,'+
                    '         "shipping":null,'+
                    '         "source":{'+
                    '            "id":"card_1CQTwIL6mRt8fBaV8A3qbJIn",'+
                    '            "object":"card",'+
                    '            "address_city":null,'+
                    '            "address_country":null,'+
                    '            "address_line1":null,'+
                    '            "address_line1_check":null,'+
                    '            "address_line2":null,'+
                    '            "address_state":null,'+
                    '            "address_zip":null,'+
                    '            "address_zip_check":null,'+
                    '            "brand":"Visa",'+
                    '            "country":"US",'+
                    '            "customer":null,'+
                    '            "cvc_check":"pass",'+
                    '            "dynamic_last4":null,'+
                    '            "exp_month":1,'+
                    '            "exp_year":2019,'+
                    '            "fingerprint":"dUcEqJaRkwdEBTzL",'+
                    '            "funding":"credit",'+
                    '            "last4":"4242",'+
                    '            "metadata":{'+
                    '            },'+
                    '            "name":"test@email.in",'+
                    '            "tokenization_method":null'+
                    '         },'+
                    '         "source_transfer":null,'+
                    '         "statement_descriptor":"Varian Marketplace",'+
                    '         "status":"succeeded",'+
                    '         "transfer":"tr_1CQTwML6mRt8fBaVgu5YjtvH",'+
                    '         "transfer_group":"group_ch_1CQTwML6mRt8fBaVBWY4YrdS"'+
                    '      }'+
                    '   },'+
                    '   "livemode":false,'+
                    '   "pending_webhooks":1,'+
                    '   "request":{'+
                    '      "id":"req_M7USwOMlRSW2Tl",'+
                    '      "idempotency_key":null'+
                    '   },'+
                    '   "type":"charge.succeeded"'+
                    '}';
            
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2, app;
    static List<vMarket_App__c> appList;
    
    
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    static vMarket_Listing__c listing;
    static Profile admin,portal;   
    static User customer1, customer2, developer1;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3;
    static List<Contact> lstContactInsert;
    
    static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
    
    static  {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        
        account = vMarketDataUtility_Test.createAccount('test account', true);
        lstContactInsert = new List<Contact>();
        contact1 = vMarketDataUtility_Test.contact_data(account, false);
        contact1.MailingStreet = '53 Street';
        contact1.MailingCity = 'Palo Alto';
        contact1.MailingCountry = 'USA';
        contact1.MailingPostalCode = '94035';
        contact1.MailingState = 'CA';
        
        contact2 = vMarketDataUtility_Test.contact_data(account, false);
        contact2.MailingStreet = '660 N';
        contact2.MailingCity = 'Milpitas';
        contact2.MailingCountry = 'USA';
        contact2.MailingPostalCode = '94035';
        contact2.MailingState = 'CA';
        
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        
        lstContactInsert.add(contact1);
        lstContactInsert.add(contact2);
        insert lstContactInsert;
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        
        insert lstUserinsert;
        
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        appList = new List<vMarket_App__c>();
        appList.add(app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id));
        appList.add(app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id));
        insert appList;
        
        listing = vMarketDataUtility_Test.createListingData(app1, true);
        comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
        activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
        
        source = vMarketDataUtility_Test.createAppSource(app1, true);
        feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
        
        orderItemList = new List<vMarketOrderItem__c>();
        system.runAs(Customer1) {
            orderItem1 = vMarketDataUtility_Test.createOrderItem(true, false); // Subscribed Order
            orderItem1.Status__c = '';
            orderItem1.TaxDetails__c = tax;
            insert orderItem1;
        }
        system.runAs(Customer2) {
            orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
        }
        orderItemList.add(orderItem1);
        orderItemList.add(orderItem2);
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
    
        system.runas(customer1) {
            cart1 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        system.runas(customer2) {
            cart2 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        cartLineList = new List<vMarketCartItemLine__c>();
        cartLine1 = vMarketDataUtility_Test.createCartItemLines(app1, cart1, false);
        cartLine2 = vMarketDataUtility_Test.createCartItemLines(app2, cart1, false);
        
        cartLineList.add(cartLine1);
        cartLineList.add(cartLine2);
        //system.runas(customer1) {
            insert cartLineList;
         vMarket_Tax__c tax = new vMarket_Tax__c(name = 'tax', Tax__c = 30.0);
         insert tax;
    }
    
    public static testMethod void webHookTest(){
            Test.StartTest();
                orderItem2.Status__c='Pending';
                orderItem2.Charge_Id__c='ch_1CQTwML6mRt8fBaVBWY4YrdS';
                update cartLine2;
                
                VMarket_WebHooks_Order__c webHook= new VMarket_WebHooks_Order__c();
                webHook.VMarket_Processed__c=false;
                webHook.VMarket_Charge_Id__c='ch_1CQTwML6mRt8fBaVBWY4YrdS';
                webHook.VMarket_Status__c='Pending';
                webHook.VMarket_Full_Response__c=respStr;
                insert webHook;
                
                vMarketWebHooksScheduler  sh1 = new vMarketWebHooksScheduler();      
                String sch = '0  00 1 3 * ?';
                system.schedule('Test', sch, sh1);
            
            Test.stopTest();
            
    }
    
    public static testMethod void webHookSubTest(){
            Test.StartTest();
                vMarketStripeDetail__c stripe= new vMarketStripeDetail__c(Details__c='test',User__c=UserInfo.getUserId(),StripeAccountId__c='cus_ChJm5lafwjfH90');
                insert stripe;
        
                VMarket_Subscriptions__c sub= new VMarket_Subscriptions__c();
                sub.Is_Active__c=true;
                sub.VMarket_Stripe_Subscription_Id__c='sub_CioLXtpKnJve6S';
                sub.VMarket_Stripe_Detail__c =stripe.id;
                insert sub;
                
                orderItem1.Status__c='Pending';
                orderItem1.Charge_Id__c='py_1CJMiLL6mRt8fBaVcicJjNic';
                orderItem1.VMarket_Subscriptions__c=sub.id;
                update orderItem1;
        
                VMarket_WebHooks_Order__c webHook= new VMarket_WebHooks_Order__c();
                webHook.VMarket_Processed__c=false;
                webHook.VMarket_Charge_Id__c='py_1CJMiLL6mRt8fBaVcicJjNic';
                webHook.VMarket_Customer_Id__c='cus_ChJm5lafwjfH90';
                webHook.VMarket_Subscription_Id__c='sub_CioLXtpKnJve6S';
                webHook.VMarket_Subscription__c=true;
                webHook.VMarket_Status__c='Pending';
                webHook.VMarket_Full_Response__c=respStr;
                insert webHook;
                
                vMarketWebHooksScheduler  sh1 = new vMarketWebHooksScheduler();      
                String sch = '0  00 1 3 * ?';
                system.schedule('Test', sch, sh1);
            
            Test.stopTest();
            
    }
}
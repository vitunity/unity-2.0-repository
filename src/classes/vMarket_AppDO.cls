/**************************************************************************
    @ Author    : Nilesh Gorle
    @ Date      : 13-Feb-2017
    @ Description   : An Apex class for Getter and setter of listing items
****************************************************************************/
/* Class Modified by Abhishek K as Part for Subscription VMT-30, VMT-35 Globalisation VMT-23 */
public class vMarket_AppDO {
    
    public static final String SOBJECT_NAME = vMarket_App__c.SObjectType.getDescribe().getName();
    
    public Decimal cartAppPrice = 0;
    public Decimal cartAppDiscount = 0;
    public Decimal cartAppSubTotal = 0;
    public Boolean IsEmptyChatter {get;set;}
   
    private vMarket_Listing__c listingObj;
    public List<Attachment> attachObjList;
    
    public vMarket_AppDO() {
        init(new vMarket_Listing__c()); 
    }
    
    public vMarket_AppDO(vMarket_Listing__c a) {
        init(a);
    }

    private void init(vMarket_Listing__c a) {
        listingObj = a;
        attachObjList = [Select ID, Name, ParentID, ContentType From Attachment Where ParentID=:listingObj.App__r.id];
    }

    public List<String> getImageAppAssets() {
        List<String> imgList;
        List<Attachment> attachList = [select  ID, Name, ParentID from Attachment where ParentId in (select Id from vMarketAppAsset__c where vMarket_App__c=:listingObj.App__r.id)];
        if(!attachList.isEmpty()) {
            imgList = new List<String>();
            for(Attachment atObj: attachList) {
                String imageURL = '/servlet/servlet.FileDownload?file=';
                String imgId = URL.getSalesforceBaseUrl().toExternalForm() + imageURL + atObj.id;
                imgList.add(imgId);
            }
        }
        return imgList;
    }

    public String getAppAssetVideoId() {
        List<vMarketAppAsset__c> assetList = [select AppVideoID__c from vMarketAppAsset__c where vMarket_App__c=:listingObj.App__r.id limit 1];
        if(!assetList.isEmpty()) {
            String VideoId = assetList[0].AppVideoID__c;
            return VideoId;
        }
        return null;
    }

    public Id getListingId(){
        return listingObj.id;
    }

    public Id getAppId(){
        return listingObj.App__r.id;
    }

    public String getName(){
        return listingObj.App__r.Name;
    }
    
    public boolean getSubscribed()
    {
    return listingObj.App__r.subscription__C;
    }

    public Boolean getHasCategories() {
        return !String.isEmpty(listingObj.App__r.App_Category__r.Name);
    }

    public String getCategoryName() {
        return listingObj.App__r.App_Category__r.Name;
    }

    public Id getLogoId() {
        for (Attachment atcFl:attachObjList) {
            if (atcFl.ContentType!='application/pdf' && atcFl.Name.startsWith('Logo_')) {
                return atcFl.id;
            }
        }
        return null;
    }

    public String getLogoUrl() {
        for (Attachment atcFl:attachObjList) {
            if (atcFl.ContentType!='application/pdf' && atcFl.Name.startsWith('Logo_')) {
                return '/servlet/servlet.FileDownload?file='+atcFl.id;
            }
        }
        return null;
    }

    public String getPdf() {
        for (Attachment atcFl:attachObjList) {
            if (atcFl.ContentType=='application/pdf' && atcFl.Name.startsWith('AppGuide_')) {
                return '/servlet/servlet.FileDownload?file='+atcFl.id;
            }
        }
        return null;
    }

    public String getPdfAttachId() {
        for (Attachment atcFl:attachObjList) {
            if (atcFl.ContentType=='application/pdf' && atcFl.Name.startsWith('AppGuide_')) {
                return atcFl.id;
            }
        }
        return null;
    }

    public Map<String, Id> getChatterMap() {
        List<vMarketAppSource__c> results = [select Id, Name, App_Category__c, App_Category_Version__c from vMarketAppSource__c 
                                                Where IsActive__c=:true AND vMarket_App__c=:listingObj.App__r.id ORDER By CreatedDate Desc];
        Map<String,Id> chatterSourceMap = new Map<String,Id>();
        IsEmptyChatter = true;
        
        if (results.size() > 0) {
            for(vMarketAppSource__c appsource: results) {
                List<ContentDocumentLink> contentDocs = [SELECT Id, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink WHERE LinkedEntityId =:appsource.Id];
                System.debug('content docs****SELECT Id, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink WHERE LinkedEntityId ='+appsource.Id);
                if(contentDocs.size() > 0) {
                    ContentDocumentLink cd = contentDocs[0];
                    System.debug('Chatter****'+appsource.Name+appsource.Id);
                    chatterSourceMap.put(appsource.Name, cd.ContentDocument.LatestPublishedVersionId);
                }
                
                //ContentDocumentLink contentDoc = [SELECT Id, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink WHERE LinkedEntityId =: appsource.Id limit 1];
                //chatterSourceMap.put(appsource.Name, contentDoc.ContentDocument.LatestPublishedVersionId);
                IsEmptyChatter = false;
            }
        }
        return chatterSourceMap;
    }

    public String getApprovalStatus() {
        return listingObj.App__r.ApprovalStatus__c;
    }

    public String getAppStatus() {
        return listingObj.App__r.AppStatus__c;
    }

    public Boolean getIsActive() {
        return listingObj.App__r.IsActive__c;
    }

    public boolean getisTrailAvailable() {
        return listingObj.App__r.Is_Trail_Available__c;
    }
    
    public Integer getTrailDays() {
        return Integer.valueof(listingObj.App__r.VMarket_Trail_Period_Days__c);
    }
    
    public String getKeyFeatures() {
        return listingObj.App__r.Key_features__c;
    }

    public String getLongDescription() {
        return listingObj.App__r.Long_Description__c;
    }

    public String getShortDescription() {
        return listingObj.App__r.Short_Description__c;
    }

    public String getOwnerName() {
        return listingObj.App__r.Owner.FirstName+' '+listingObj.App__r.Owner.LastName;
    }

    public String getOwnerEmail() {
        return listingObj.App__r.Owner.Email;
    }
    
    public String getOwnerPhone() {
        return listingObj.App__r.Owner.Phone;
    }

    public Decimal getPrice() {
        //return Integer.valueOf(listingObj.App__r.Price__c);
        return listingObj.App__r.Price__c;
    }

    public Date getPublishedDate() {
        return listingObj.App__r.Published_Date__c;
    }

    public Integer getInstallCount() {
        return Integer.valueOf(listingObj.InstallCount__c);
    }
    
     /* Method Added by Abhishek K as Part of Globalisation VMT-26 */
    public String getCountries()
    {
    String countryNames = '';
    Map<String,String> countriesMap = (new vMarket_Approval_Util()).countriesMap();
    if(String.isNotBlank(listingObj.App__R.countries__C))
    {
    for(String con : listingObj.App__R.countries__C.split(';',-1))
    {
    countryNames += countriesMap.get(con)+',';
    }
    }
    else countryNames='NA,';
    System.debug('(((('+countryNames);
    return countryNames.substring(0,countryNames.length()-1);
    }

    public Integer getPageViews() {
        return Integer.valueOf(listingObj.PageViews__c);
    }

    public vMarketRatingDo getRatingObj() {
        Integer total_count = listingObj.vMarket_Comments__r.size();
        Integer star_1=0;
        Integer star_2=0;
        Integer star_3=0;
        Integer star_4=0;
        Integer star_5=0;
        
        vMarketRatingDo ratingObj = new vMarketRatingDo();
        List<vMarketComment__c> commentList = new List<vMarketComment__c>();
        for (vMarketComment__c obj : listingObj.vMarket_Comments__r) {
            Integer Rating = Integer.valueOf(obj.Rating__c);
            commentList.add(obj);

            if (Rating==1) {
                star_1+=1;
            } else if (Rating==2) { 
                star_2+=1;
            } else if (Rating==3) {
                star_3+=1;
            } else if (Rating==4) {
                star_4+=1;
            } else if (Rating==5) {
                star_5+=1;
            }
        }

        ratingObj.setStarCount1(star_1);
        ratingObj.setStarCount2(star_2);
        ratingObj.setStarCount3(star_3);
        ratingObj.setStarCount4(star_4);
        ratingObj.setStarCount5(star_5);
        ratingObj.setTotalCount(total_count); 
        ratingObj.setCommenstList(commentList);
        return ratingObj;
    }
    
    public String getRatingText() {
        Decimal rating = getRatingObj().getScaledRating();
        String formatedString = rating.format();
        String replaceString = formatedString.replace('.', '-');
        return replaceString;
    }

    public String getCurrencyIsoCode() {
        return listingObj.App__r.CurrencyIsoCode;
    }
    
    public String getCurrencySymbol() {
        String IsoCode = listingObj.App__r.CurrencyIsoCode;
        String currencySymbol = 
           ('USD' == IsoCode ? '$' : 
           ('CAD' == IsoCode ? '$' : 
           ('EUR' == IsoCode ? '€' : 
           ('GBP' == IsoCode ? '£' : 
           ('JPY' == IsoCode ? '¥' : 
           ('KRW' == IsoCode ? '₩' : 
           ('CNY' == IsoCode ? '元' : IsoCode)))))));
        return currencySymbol;
    }

    /**
    public void setVideoType(String videoType) {
        appObj.Video_Type__c = videoType;
    }
    
    public String getVideoType() {
        return appObj.Video_Type__c;
    }
    
    public void setVideoUrl(String videoUrl) {
        appObj.videoUrl__c = videoUrl;
    }
    public String getVideoUrl() {
        return appObj.videoUrl__c;
    } Puneet **/
    
    /* Dev Support Methods */
    public String getPublisherName(){
        return listingObj.App__r.PublisherName__c;
    }
    
    public String getPublisherPhone(){
        return listingObj.App__r.PublisherPhone__c;
    }
    
    public String getPublisherWebsite(){
        return listingObj.App__r.PublisherWebsite__c;
    }
    
    public String getPublisherEmail(){
        return listingObj.App__r.PublisherEmail__c;
    }
    
    public String getPublisher_Developer_Support(){
        return listingObj.App__r.Publisher_Developer_Support__c;
    }
    
    /**
     * VMT-35: Subscription options on App Detail Page
     * @created : 12 march 2018, Puneet Mishra
     */
    public List<VMarket_Stripe_Subscription_Plan__c> getSubscriptionPlans() {
        List<VMarket_Stripe_Subscription_Plan__c> subscriptionPlan = new List<VMarket_Stripe_Subscription_Plan__c>();
        subscriptionPlan = [SELECT Id, Name, Is_Active__c, vMarket_App__c, VMarket_Duration_Type__c, 
                                    VMarket_Price__c, VMarket_Stripe_Subscription_Plan_Id__c 
                            FROM VMarket_Stripe_Subscription_Plan__c WHERE vMarket_App__c =: listingObj.App__r.id ];
        
        return subscriptionPlan;
    }
    
     public String getEULAAttachedId(){
        for(Attachment atcFl:attachObjList) {
                    if (atcFl.ContentType=='application/pdf' && atcFl.Name.startsWith('EULA_')) {
                        return atcFl.id;
                    }
                }
                return null;
            }
}
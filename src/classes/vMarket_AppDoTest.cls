/**
 *  @author    :    Puneet Mishra
 *  @description:    test Class for vMarket_AppDo
 */
@isTest
public with sharing class vMarket_AppDoTest {
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app;
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    
    static Profile admin,portal;   
     static User customer1, customer2, developer1, developer2;
     static User adminUsr1,adminUsr2;
     
     static List<User> lstUserInsert;
     static Account account;
     static Contact contact1, contact2, contact3, contact4;
     static List<Contact> lstContactInsert;
     
     static Attachment attach_Logo, attach_AppGuide;
     static List<Attachment> attachList;
     
     static User objAdmin;
     
    static {
      admin = vMarketDataUtility_Test.getAdminProfile();
      portal = vMarketDataUtility_Test.getPortalProfile();
      
      lstUserInsert = new List<User>();
      lstUserInsert.add( adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin1'));
      lstUserInsert.add( adminUsr2 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin2'));
      insert lstUserInsert;
    
      account = vMarketDataUtility_Test.createAccount('test account', true);
    
      lstContactInsert = new List<Contact>();
      lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
      lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
      lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
      lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
      insert lstContactInsert;
      
      System.runAs(adminUsr1) {
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
         lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
         lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
         lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
        insert lstUserInsert;
      }
      
      cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
      app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
      
      listing = vMarketDataUtility_Test.createListingData(app, true);
      comment = vMarketDataUtility_Test.createCommentTest(app, listing, true);
      activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
      
      source = vMarketDataUtility_Test.createAppSource(app, true);
      feedI = vMarketDataUtility_Test.createFeedItem(app.Id, true);
      
      orderItemList = new List<vMarketOrderItem__c>();
      orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
      orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
     
      orderItemList.add(orderItem1);
      orderItemList.add(orderItem2);
      //insert orderItemList;
      
      orderItemLineList = new List<vMarketOrderItemLine__c>();
      
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app, orderItem1, true, true);
     
      
      
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app, orderItem2, false, true);
      
      
      orderItemLineList.add(orderItemLine1);
      orderItemLineList.add(orderItemLine2);
      //insert orderItemLineList;
    }
    
    
    private static testMethod void AppDo_Test() {
      test.StartTest();
        vMarket_AppDO appDo = new vMarket_AppDO();
        appDo.getCountries();
        listing = [SELECT Id, Name, App__r.Id, App__r.Name, App__r.subscription__c, App__r.App_Category__r.Name, App__r.ApprovalStatus__c, App__r.AppStatus__c,
                App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, App__r.Short_Description__c, App__r.Owner.Email, App__r.Owner.Phone,
                App__r.Price__c, App__r.Published_Date__c, PageViews__c, InstallCount__c, App__r.CurrencyIsoCode, 
                App__r.PublisherName__c, App__r.PublisherPhone__c, App__r.PublisherWebsite__c, App__r.PublisherEmail__c, 
                App__r.Publisher_Developer_Support__c, App__r.Owner.FirstName, App__r.Owner.LastName,
                          
                   //modified on 18-May-2018
                App__r.Is_Trail_Available__c,App__r.VMarket_Trail_Period_Days__c,
                   
                  (Select Id, Rating__c From vMarket_Comments__r) FROM vMarket_Listing__c WHERE Id =: listing.Id];
        vMarket_AppDO appDo1 = new vMarket_AppDO(listing);
        
        vMarketComment__c comment1, comment2, comment3, comment4, comment5;
        list<vMarketComment__c> commentList = new List<vMarketComment__c>();
        comment1 = vMarketDataUtility_Test.createCommentTest(app, listing, false);
        comment2 = vMarketDataUtility_Test.createCommentTest(app, listing, false);
        comment3 = vMarketDataUtility_Test.createCommentTest(app, listing, false);
        comment4 = vMarketDataUtility_Test.createCommentTest(app, listing, false);
        comment5 = vMarketDataUtility_Test.createCommentTest(app, listing, false);
        commentList.add(comment1);
        commentList.add(comment2);
        commentList.add(comment3);
        commentList.add(comment4);
        commentList.add(comment5);
        insert commentList;
        
        attach_Logo = vMarketDataUtility_Test.createAttachmentTest(String.valueOf(app.Id), 'image/jpeg', 'Logo_', false);
        attach_AppGuide = vMarketDataUtility_Test.createAttachmentTest(String.valueOf(app.Id), 'application/pdf', 'AppGuide_', false);
        attachList = new List<Attachment>();
        attachList.add(attach_Logo);
        attachList.add(attach_AppGuide);
        insert attachList;
        
        system.assertEquals(appDo1.getApprovalStatus(), listing.App__r.ApprovalStatus__c);
        system.assertEquals(appDo1.getAppStatus(), listing.App__r.AppStatus__c);
        system.assertEquals(appDo1.getIsActive(), listing.App__r.IsActive__c);
        system.assertEquals(appDo1.getKeyFeatures(), listing.App__r.Key_features__c);
        system.assertEquals(appDo1.getLongDescription(), listing.App__r.Long_Description__c);
        system.assertEquals(appDo1.getShortDescription(), listing.App__r.Short_Description__c);
        system.assertEquals(appDo1.getOwnerName(), listing.App__r.Owner.FirstName+' '+listing.App__r.Owner.LastName);
        system.assertEquals(appDo1.getOwnerEmail(), listing.App__r.Owner.Email);
        system.assertEquals(appDo1.getOwnerPhone(), listing.App__r.Owner.Phone);
        system.assertEquals(appDo1.getPrice(), Integer.valueOf(listing.App__r.Price__c));
        system.assertEquals(appDo1.getPublishedDate(), listing.App__r.Published_Date__c);
        system.assertEquals(appDo1.getInstallCount(), listing.InstallCount__c);
        system.assertEquals(appDo1.getPageViews(), Integer.valueOf(listing.PageViews__c));
        //system.assertEquals(appDo1.getRatingObj);
        system.assertEquals(appDo1.getCurrencyIsoCode(), listing.App__r.CurrencyIsoCode);
        system.assertEquals(appDo1.getPublisher_Developer_Support(), listing.App__r.Publisher_Developer_Support__c);
        system.assertEquals(appDo1.getPublisherEmail(), listing.App__r.PublisherEmail__c);
        system.assertEquals(appDo1.getPublisherWebsite(), listing.App__r.PublisherWebsite__c);
        system.assertEquals(appDo1.getPublisherPhone(), listing.App__r.PublisherPhone__c);
        system.assertEquals(appDo1.getPublisherName(), listing.App__r.PublisherName__c);
        system.assertEquals(appDo1.getListingId(), listing.Id);
        system.assertEquals(appDo1.getAppId(), listing.App__r.Id);
        system.assertEquals(appDo1.getName(), listing.App__r.Name);
        system.assertEquals(appDo1.getSubscribed(), listing.App__r.subscription__c);
        system.assertEquals(appDo1.getHasCategories(), !String.isEmpty(listing.App__r.App_Category__r.Name));
        system.assertEquals(appDo1.getCategoryName(), listing.App__r.App_Category__r.Name);
        
        
        //modified on 18-May-2018
                system.assertEquals(appDo1.getisTrailAvailable(), listing.App__r.Is_Trail_Available__c);
                system.assertEquals(appDo1.getTrailDays(), listing.App__r.VMarket_Trail_Period_Days__c);

            //string country = appDo1.getCountries();  
        
        
        appDo1.getRatingObj();
        appDo1.getRatingText();
        appDo1.getCurrencySymbol();
        appDo1.attachObjList = attachList;
        system.assertEquals(appDo1.getLogoId(), attach_Logo.Id);
        system.assertequals(appDo1.getLogoUrl(), '/servlet/servlet.FileDownload?file='+attach_Logo.Id);
        
        system.assertequals(appDo1.getPdfAttachId(), attach_Appguide.Id);
        system.assertequals(appDo1.getPdf(), '/servlet/servlet.FileDownload?file='+attach_Appguide.Id);
        
        vMarketAppAsset__c asset = vMarketDataUtility_Test.createAppAsset(app.Id, '1234', true);
        Attachment newAttach = vMarketDataUtility_Test.createAttachmentTest(String.valueOf(asset.Id), 'image/jpeg', 'Logo_', true);
        String resurl = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file=' + asset.Id;
        appDo1.getImageAppAssets();
        appDo1.getAppAssetVideoId();
        appDo1.getChatterMap();
        //system.assertequals(appDo1.getImageAppAssets(), new List<String>{resurl});
        
        //system.assertEquals(appDo1.getAppAssetVideoId(), '1234');
        
        
        //string country = appDo1.getCountries(); 
        appDo.getCountries();
       // VMarket_Stripe_Subscription_Plan__c vsubplan = new VMarket_Stripe_Subscription_Plan__c();
        //List<VMarket_Stripe_Subscription_Plan__c> lst = appDo1.getSubscriptionPlans();
        
        appDo.getSubscriptionPlans();

      test.StopTest();
    }
    
    
}
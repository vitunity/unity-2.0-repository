public with sharing class vMarket_AppSourceInfoController {
    
    public string app{get;set;}
    
    public String appId;
    
    public List<appSourceController> appSrcControl{get;set;}
    
    public vMarket_App__c vMarketapp;
    public List<SelectOption> options;
    
    public static final Integer NUM_OF_NEW_ROWS=1;
    public Boolean show{get;set;}    
     
    
    public vmarket_AppSourceInfoController() {
        appSrcControl = new List<appSourceController>();
        if(ApexPages.currentPage().getParameters().get('id') != null) {
            appId = ApexPages.currentPage().getParameters().get('id');
            vMarketapp = [SELECT Id, Name, App_Category__c, App_Category__r.Name FROM vMarket_App__c WHERE Id =: appId];
            options = getCategoryVersions(vMarketapp);
        }
        appSrcControl.add(new appSourceController(vMarketapp, options));
        show = true;
        app='a8S4C0000004G3o';
        //system.assertEquals(appSrcControl, new List<appSourceController>());
    }
    
    @testVisible private List<Selectoption> getCategoryVersions(vMarket_App__c vMarketApp) {
        List<SelectOption> options = new List<SelectOption>();
        
        for(vMarket_App_Category_Version__c ver : [ SELECT Id, App_Category__c, App_Category__r.Name, isActive__c, Name FROM vMarket_App_Category_Version__c
                                                    WHERE App_Category__c =: vMarketApp.App_Category__c AND isActive__c =: true]) {
            options.add(new SelectOption(ver.Id,ver.Name));
        }
        
        return options;
    }
    
    public pageReference addMoreRows() {
        for(integer i = 0; i < NUM_OF_NEW_ROWS; i++){
            appSrcControl.add(new appSourceController(vMarketapp, options));
        }
        app = '';
        show = false;
        return null;
    }
    
    public pageReference uploadAppSource() {
    	system.debug('appSrcControl ' + appSrcControl);
    	List<vMarketAppSource__c> appSrcList = new List<vMarketAppSource__c>();
    	for(appSourceController con : appSrcControl) {
    		con.appSrc.vMarket_App__c = appId;
    		appSrcList.add(con.appSrc);
    	}
    	system.debug('appSrcList ' + appSrcList);
    	insert appSrcList;
    	system.debug(' == app == ' + app);
    	system.debug('appSrcList ' + appSrcList[0].Id);
    	app = appSrcList[0].Id;
    	system.debug(' == app == ' + app);
    	show = true;
    	return null;
    }
    
    public class appSourceController {
        public vMarket_App__c vMarApp{get;set;}
        public vMarketAppSource__c appSrc{get;set;}
        
        public List<SelectOption> categoryVer{get;set;} 
        
        public appSourceController(vMarket_App__c vMarketApp, List<SelectOption> options) {
            vMarApp = vMarketApp;
            categoryVer = options;
            appSrc = new vMarketAppSource__c();
            appSrc.vMarket_App__c = vMarketApp.Id;
        }
        
    }
}
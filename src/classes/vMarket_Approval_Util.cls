/* Class Created by Abhishek K as Part of Custom App Review Process VMT-21 */
public class vMarket_Approval_Util {
    
    public static final String Admin_Apps_URL = 'https://sfdev-varian.cs12.force.com/';
    public static final String Tech_Review_URL = 'https://sfdev-varian.cs12.force.com/';
    public static final String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm()+'/apex';//Label.vMarket_BaseURL+'/';
    public static final String Reg_Review_URL = 'https://sfdev-varian.cs12.force.com/';
    public static final String Admin_In_Progress_URL = 'https://sfdev-varian.cs12.force.com/';
    public static final String New_Review_URL = 'https://sfdev-varian.cs12.force.com/';
//  public static final String Admin_Pending_Apps_URL = 'https://sfdev-varian.cs12.force.com/';
    
    public static final String Admin_Apps_Subject = 'Varian Market Place : App Review Started Successfully';
    public static final String Admin_Apps_Completed_Subject = 'Varian Market Place : App Review Completed Successfully';
    public static final String Tech_Review_Subject = 'Varian Market Place : App Review Request';
    public static final String Reg_Review_Subject = 'Varian Market Place : App Review Request';
    public static final String Admin_In_Progress_Subject = ' Review Completed for App';
    public static final String New_Review_Subject = 'Varian Market Place : App Assigned for Review';
//    public static final String Admin_Pending_Apps_Subject = 'App Submitted for Review';
    
    public static final String Admin_Apps_Body = 'Dear Admin,<br><br> Following App has been Submitted for Review<br>You can track the Progress here '+Admin_Apps_URL+'<br><br>Thanks';
    public static final String Tech_Review_Body = 'Dear Technical Reviewer,<br><br> Following App has been Assigned to you for Review<br>Please review it here '+Tech_Review_URL+'<br><br>Thanks';
    public static final String Reg_Review_Body = 'Dear Review Reviewer,<br><br> Following App has been Assigned to you for Review<br>Please review it here '+Reg_Review_URL+'<br><br>Thanks';
    public static final String Sec_Review_Body = 'Dear Legal Reviewer,<br><br> Following App has been Assigned to you for Review<br>Please review it here '+Reg_Review_URL+'<br><br>Thanks';
    public static final String Admin_In_Progress_Body = 'Dear Admin,<br><br>App has been Reviewed<br> You can view the Review Here '+Admin_In_Progress_URL+'<br><br>Thanks';
    public static final String New_Review_Body = 'Dear Reviewer,<br><br>You are the New reviewer for the Following App<br> Please Submit your Serview here '+New_Review_URL+'<br><br>Thanks';
//    public static final String Admin_Pending_Apps_Body = '';

    public String getTechReviewURL(String appid,String country)
    {
    return baseUrl+'/vMarketTechRegApproval?appid='+appid+'&type=Technical&con='+country;
    }
    
    public String getRegReviewURL(String appid,String country)
    {
    return baseUrl+'/vMarketTechRegApproval?appid='+appid+'&type=regulatory&con='+country;
    }
    
    public String getSecReviewURL(String appid,String country)
    {
    return baseUrl+'/vMarketTechRegApproval?appid='+appid+'&type=security&con='+country;
    }
    
    public String getTradeReviewURL(String appid,String country)
    {
    return baseUrl+'/vMarketTechRegApproval?appid='+appid+'&type=trade&con='+country;
    }

    public String getAdminAppsURL(String appid,String con)
    {
    return baseUrl+'/vMarketAdminApproval?appid='+appid+'&con='+con;
    }
    
    public String getAdminAppsInProgressURL()
    {
    return baseUrl+'/VMarketCustomAppApprovalAdminPage';
    }
    
    public String getAdminAppsPendingURL(String appid)
    {
    return baseUrl+'/VMarketCustomAppApprovalAdminPage';
    }

    public String getAdminAppBody(String name,String appName,String appid)
    {
    return 'Dear '+name+',<br><br> You have Successfully started the Review Process for the Following App <br><a href=\''+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+appid+'\'><b>'+appName+'</b></a><br><br>You can track the Progress <a href=\"'+getAdminAppsInProgressURL()+'\">here</a>';
    }
    
     public String getAdminAppCompletedBody(String name,String appName,String appid)
    {
    return 'Dear '+name+',<br><br> You have Successfully completed the Review Process for the Following App <br><a href=\''+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+appid+'\'><b>'+appName+'</b></a><br><br>You can track the Progress <a href=\"'+getAdminAppsInProgressURL()+'\">here</a>';
    }
    
    public String getTechReviewBody(String name,String appid,String appName,String country)
    {
    return 'Dear '+name+',<br><br>  <a href=\"'+getTechReviewURL(appid,country)+'\">'+appName+'</a> App has been Assigned to you for Technical Review for the country '+countriesMap().get(country)+' on Varian Market Place review it <a href=\"'+getTechReviewURL(appid,country)+'\">here</a>  <br><br>Thanks';
    }
    
    public String getregReviewBody(String name,String appid,String appName,String country)
    {
    return 'Dear '+name+',<br><br>  <a href=\"'+getRegReviewURL(appid,country)+'\">'+appName+'</a> App has been Assigned to you for Regulatory Review for the country '+countriesMap().get(country)+' on Varian Market Place review it <a href=\"'+getRegReviewURL(appid,country)+'\">here</a>  <br><br>Thanks';
    }
    
    public String getsecReviewBody(String name,String appid,String appName,String country)
    {
    return 'Dear '+name+',<br><br>  <a href=\"'+getSecReviewURL(appid,country)+'\">'+appName+'</a> App has been Assigned to you for Legal Review for the country '+countriesMap().get(country)+' on Varian Market Place review it <a href=\"'+getSecReviewURL(appid,country)+'\">here</a>  <br><br>Thanks';
    }
    
    public String gettradeReviewBody(String name,String appid,String appName,String country)
    {
    return 'Dear '+name+',<br><br>  <a href=\"'+getSecReviewURL(appid,country)+'\">'+appName+'</a> App has been Assigned to you for Trade Compilance Review for the country '+countriesMap().get(country)+' on Varian Market Place review it <a href=\"'+getTradeReviewURL(appid,country)+'\">here</a>  <br><br>Thanks';
    }
    
    public String getAdminInProgressBody(String name,String appName,String appid,String type,String review,String Comment,String country)
    {
    return 'Dear '+name+',<br><br>Review of <a href=\"'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+appid+'\"><b>'+appName+'</b></a> for '+countriesMap().get(country)+' is Completed Successfully , below are the comments from Reviewer <br><br> <u>Team</u> :'+type+'<br><br><u>Review</u> :'+review+' <br><br> <u>Comment</u> : '+comment+'<br><br> You can view the Full Review and Take Action <a href=\"'+getAdminAppsURL(appid,country)+'\">here</a>  <br><br>Thanks';
    }
    
    public String getAdminNewReviewBody(String name,String appid,String appName,String reason,String link,String country)
    {
    return 'Dear '+name+',<br><br>You are the New reviewer for the Following App<br><b>'+appName+'</b> for '+countriesMap().get(country)+'<br><br>The Reason for Assigning to you is : <b>'+reason+' </b><br><br>Please Submit your review <a href='+baseUrl+link+'>here</a> <br><br>Thanks';
    }
    
    
    public List<String> getAdminEmails()
    {
    Set<String> emailsset = new Set<String>();
    for(VMarket_Reviewers__c reviewer : [Select Active__c, Default__c, type__c, User__c,User__r.lastname,User__r.firstname,User__r.email from VMarket_Reviewers__c where Active__c=true]) 
        {
           if(reviewer.type__c.containsignorecase('Admin'))
           { 
           emailsset.add(reviewer.User__r.email);
           
           }
        }
     return new List<String>(emailsset);
    }
    
    public List<String> gettechReviewerEmails()
    {
    Set<String> emailsset = new Set<String>();
    for(VMarket_Reviewers__c reviewer : [Select Active__c, Default__c, type__c, User__c,User__r.lastname,User__r.firstname,User__r.email from VMarket_Reviewers__c where Active__c=true]) 
        {
           if(reviewer.type__c.containsignorecase('technical'))
           { 
           emailsset.add(reviewer.User__r.email);
           
           }
        }
    return new List<String>(emailsset);
    }


    public List<String> getregReviewerEmails()
    {
    Set<String> emailsset = new Set<String>();
    for(VMarket_Reviewers__c reviewer : [Select Active__c, Default__c, type__c, User__c,User__r.lastname,User__r.firstname,User__r.email from VMarket_Reviewers__c where Active__c=true]) 
        {
           if(reviewer.type__c.containsignorecase('regulatory'))
           { 
           emailsset.add(reviewer.User__r.email);
           
           }
        }

        return new List<String>(emailsset);
    }
    
    public List<String> gettradeReviewerEmails()
    {
    Set<String> emailsset = new Set<String>();
    for(VMarket_Reviewers__c reviewer : [Select Active__c, Default__c, type__c, User__c,User__r.lastname,User__r.firstname,User__r.email from VMarket_Reviewers__c where Active__c=true]) 
        {
           if(reviewer.type__c.containsignorecase('trade'))
           { 
           emailsset.add(reviewer.User__r.email);
           
           }
        }

        return new List<String>(emailsset);
    }

     public List<SelectOption> ListOfCountries() {
        List<SelectOption> options = new List<SelectOption>();
       // Schema.DescribeFieldResult countries; //vMarket_App__c.Countries__c.getDescribe();
       // List<Schema.PicklistEntry> appStatusValues = countries.getPicklistValues();

        for (VMarket_Country__c v : VMarket_Country__c.getall().values())//(Schema.PicklistEntry status : appStatusValues)
        { 
            options.add(new SelectOption(v.code__c,v.name)); 
        }
        return options;
    }

    public List<SelectOption> ListOfCountriesall() {
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('','')); 
     
        for (VMarketCountry__c v : [Select Code__c, name,Documents__c from VMarketCountry__c order by name])
        { 
            options.add(new SelectOption(v.code__c,v.name)); 
        }
        return options;
    
    } 
    
    public Map<String,String> countriesMap() {
     Map<String,String>  options = new Map<String,String>();

        for (VMarketCountry__c v : [Select Code__c, name,Documents__c from VMarketCountry__c])
        { 
            options.put(v.code__c,v.name); 
        }
        return options;
    
    }
    
    public Map<String,String> countriesMapCodes() {
     Map<String,String>  options = new Map<String,String>();

        for (VMarketCountry__c v : [Select Code__c, name,Documents__c from VMarketCountry__c])
        { 
            options.put(v.name,v.code__c); 
        }
        return options;
    
    } 
    
    public Map<String,String> countriesFieldsMap() {
     Map<String,String>  options = new Map<String,String>();

        for (VMarketCountry__c v : [Select Code__c, VMarket_Regulatory_Visible_Fields__c, VMarket_Technical_Visible_Fields__c, name,Documents__c from VMarketCountry__c])
        { 
            options.put(v.code__c,v.VMarket_Regulatory_Visible_Fields__c+'-'+v.VMarket_Technical_Visible_Fields__c); 
        }
        return options;
    
    } 
    /* Method Added by Abhishek K to create a Log for Approval Process*/
    public static VMarket_Approval_Log__c createLog(String userType,String action,String status,String info,vMarket_App__c app)
   {
   VMarket_Approval_Log__c log = new VMarket_Approval_Log__c();
   if(String.isBlank(status)) status ='NONE';
   if(String.isBlank(info)) info = 'NONE';
   log.User_Type__c = userType;
   log.Action__c = action;
   log.vMarket_App__c = app.id;
   log.Status__c = status;
   log.Information__c = info;
   return log;
   }
   /* Method Added by Abhishek K to see if Trail is Available or not */
   /*public Integer checkTrail(Id appId)
   {
   List<vMarket_Listing__c> trailListings = [Select id,Trail_Downloaded_on__c,CreatedDate FROM vMarket_Listing__c WHERE App__r.Id=:appId and Trail_Version__c=true and Trail_Downloaded_By__c=:UserInfo.getUserId()];
   
   if(trailListings.size()==0) return -1;
   else
   {
   return trailListings[0].Trail_Downloaded_on__c.daysBetween(Date.today());
   }
   return -1;
   }*/
   /* Method Added by Abhishek K to find Allowed Apps based on the User's Country */
    public List<Id> allowedApps(){
            String country = '';
            List<id> allowedApps = new List<id>();
            List<User> usr = [Select ContactId From User Where Id =: UserInfo.getUserId() limit 1];
            if (usr.size() > 0) {
            List<Contact> customerContact = [
                SELECT Id, AccountId,Account.ISO_Country__c, Account.Ext_Cust_Id__c, FirstName, LastName, Email, Phone
                FROM Contact
                WHERE Id =: usr[0].ContactId
                LIMIT 1
            ];
            if(!customerContact.isEmpty()){
            country = customerContact[0].Account.ISO_Country__c;
            }
            

            for( VMarket_Country_App__c countryapp : [select id,vmarket_app__C from VMarket_Country_App__c where Country__c=:country and Status__c='Active' and VMarket_Approval_Status__c ='Published'])
            {
            allowedApps.add(countryapp.vmarket_app__C);            
            }
            
            
            }
            return allowedApps;
            }
   
           public boolean isAppAllowed(Id appId)
            {
            String country = '';
            List<User> usr = [Select ContactId From User Where Id =: UserInfo.getUserId() limit 1];
        if (usr.size() > 0) {
            List<Contact> customerContact = [
                SELECT Id, AccountId,Account.ISO_Country__c, Account.Ext_Cust_Id__c, FirstName, LastName, Email, Phone
                FROM Contact
                WHERE Id =: usr[0].ContactId
                LIMIT 1
            ];
            if(!customerContact.isEmpty()){
            country = customerContact[0].Account.ISO_Country__c;
            }
            else return false;
           
       }
       //return true;
           SYstem.debug('((((('+country);
       if(String.isNotBlank(country))
       {
       List<VMarket_Country_App__c> countryapps = [select id from VMarket_Country_App__c where Country__c=:country and Status__c='Active' and VMarket_App__c=:appId and VMarket_Approval_Status__c ='Published'];
       SYstem.debug('((((('+countryapps.size());
       if(countryapps.size()==0) return false;       
       else return true;
       
       
       }
       
       return false;
       //else return false;
       
       }
       
       /* Method Added by Abhishek K to find out the User's Country */
       public String getUserCountry()
       {       
            String country = '';
            List<User> usr = [Select ContactId From User Where Id =: UserInfo.getUserId() limit 1];
        if (usr.size() > 0) {
            List<Contact> customerContact = [
                SELECT Id, AccountId,Account.ISO_Country__c, Account.Ext_Cust_Id__c, FirstName, LastName, Email, Phone
                FROM Contact
                WHERE Id =: usr[0].ContactId
                LIMIT 1
            ];
            if(!customerContact.isEmpty()){
            country = customerContact[0].Account.ISO_Country__c;
            }
       }
       return country;
       
       }
    
    
    }
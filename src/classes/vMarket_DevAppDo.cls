/**************************************************************************
    @ Author    : Nilesh Gorle
    @ Date      : 13-Feb-2017
    @ Description   : An Apex class for Getter and setter of all apps like published, submitted, approved, rejected
****************************************************************************/
 /* Modified by Abhishek K as Part of Subscription VMT-26 */
public class vMarket_DevAppDo {
    public static final String SOBJECT_NAME = vMarket_App__c.SObjectType.getDescribe().getName();

    private vMarket_App__c appObj;
    public List<Attachment> attachObjList;
    
    public vMarket_DevAppDo() {
        init(new vMarket_App__c()); 
    }
    
    public vMarket_DevAppDo(vMarket_App__c a) {
        init(a);
    }

    private void init(vMarket_App__c a) {
        appObj = a;
        attachObjList = [Select ID, Name, ParentID, ContentType From Attachment Where ParentID=:appObj.id];
    }

    public List<String> getImageAppAssets() {
        List<String> imgList;
        List<Attachment> attachList = [select  ID, Name, ParentID from Attachment where ParentId in (select Id from vMarketAppAsset__c where vMarket_App__c=:appObj.id)];
        if(!attachList.isEmpty()) {
            imgList = new List<String>();
            for(Attachment atObj: attachList) {
                String imageURL = '/servlet/servlet.FileDownload?file=';
                String imgId = URL.getSalesforceBaseUrl().toExternalForm() + imageURL + atObj.id;
                imgList.add(imgId);
            }
        }
        return imgList;
    }

    public String getAppAssetVideoId() {
        List<vMarketAppAsset__c> assetList = [select AppVideoID__c from vMarketAppAsset__c where vMarket_App__c=:appObj.id limit 1];
        if(!assetList.isEmpty()) {
            String VideoId = assetList[0].AppVideoID__c;
            return VideoId;
        }
        return null;
    }

    public Id getAppId(){
        return appObj.id;
    }
     /* Method Added by Abhishek K as Part of Globalisation VMT-26 */
    public String getCountries()
    {
    String countryNames = '';
    Map<String,String> countriesMap = (new vMarket_Approval_Util()).countriesMap();
    if(String.isNotBlank(appObj.countries__C))
    {
    for(String con : appObj.countries__C.split(';',-1))
    {
    countryNames += countriesMap.get(con)+',';
    }
    }
    else countryNames='NA,';
    System.debug('(((('+countryNames);
    return countryNames.substring(0,countryNames.length()-1);
    }

    public String getName(){
        return appObj.Name;
    }

    public Boolean getHasCategories() {
        return !String.isEmpty(appObj.App_Category__r.Name);
    }

    public String getCategoryName() {
        return appObj.App_Category__r.Name;
    }

    public String getRatingText() {
        Decimal rating = getRatingObj().getScaledRating();
        String formatedString = rating.format();
        String replaceString = formatedString.replace('.', '-');
        return replaceString;
    }
    
    public boolean getSubscribed()
    {
    return appObj.subscription__C;
    }

    public Id getLogoId() {
        for (Attachment atcFl:attachObjList) {
            if (atcFl.ContentType!='application/pdf' && atcFl.Name.startsWith('Logo_')) {
                return atcFl.id;
            }
        }
        return null;
    }

    public String getLogoUrl() {
        for (Attachment atcFl:attachObjList) {
            if (atcFl.ContentType!='application/pdf' && atcFl.Name.startsWith('Logo_')) {
                return '/servlet/servlet.FileDownload?file='+atcFl.id;
            }
        }
        return null;
    }

    public String getPdf() {
        for (Attachment atcFl:attachObjList) {
            if (atcFl.ContentType=='application/pdf' && atcFl.Name.startsWith('AppGuide_')) {
                return '/servlet/servlet.FileDownload?file='+atcFl.id;
            }
        }
        return null;
    }

    public String getApprovalStatus() {
        return appObj.ApprovalStatus__c;
    }

    public String getAppStatus() {
        return appObj.AppStatus__c;
    }

    public Boolean getIsActive() {
        return appObj.IsActive__c;
    }

    public String getKeyFeatures() {
        return appObj.Key_features__c;
    }

    public String getLongDescription() {
        return appObj.Long_Description__c;
    }

    public String getShortDescription() {
        return appObj.Short_Description__c;
    }

    public String getOwnerName() {
        return appObj.Owner.FirstName+' '+appObj.Owner.LastName;
    }

    public String getOwnerEmail() {
        return appObj.Owner.Email;
    }
    
    public String getOwnerPhone() {
        return appObj.Owner.Phone;
    }

    public Decimal getPrice() {
        return appObj.Price__c;
    }

    public Date getPublishedDate() {
        return appObj.Published_Date__c;
    }

    public String getCurrencyIsoCode() {
        return appObj.CurrencyIsoCode;
    }
    
    public String getCurrencySymbol() {
        String IsoCode = appObj.CurrencyIsoCode;
        String currencySymbol = 
           ('USD' == IsoCode ? '$' : 
           ('CAD' == IsoCode ? '$' : 
           ('EUR' == IsoCode ? '€' : 
           ('GBP' == IsoCode ? '£' : 
           ('JPY' == IsoCode ? '¥' : 
           ('KRW' == IsoCode ? '₩' : 
           ('CNY' == IsoCode ? '元' : IsoCode)))))));
        return currencySymbol;
    }

    public Integer getInstallCount() {
        return 0;
    }

    public Integer getPageViews() {
        return 0;
    }

    public vMarketRatingDo getRatingObj() {
        vMarketRatingDo ratingObj = new vMarketRatingDo();
        List<vMarketComment__c> commentList = new List<vMarketComment__c>();
        ratingObj.setStarCount1(0);
        ratingObj.setStarCount2(0);
        ratingObj.setStarCount3(0);
        ratingObj.setStarCount4(0);
        ratingObj.setStarCount5(0);
        ratingObj.setTotalCount(0); 
        ratingObj.setCommenstList(commentList);
        return ratingObj;
    }
    
    public String getPublisherName() {
        return appObj.PublisherName__c;
    }
    
    public String getPublisherPhone() {
        return appObj.PublisherPhone__c;
    }
    
    public String getPublisherWebsite() {
        return appObj.PublisherWebsite__c;
    }
    
    public String getPublisherEmail() {
        return appObj.PublisherEmail__c;
    }
    
    public String getPublisher_Developer_Support() {
        return appObj.Publisher_Developer_Support__c;
    }
    
}
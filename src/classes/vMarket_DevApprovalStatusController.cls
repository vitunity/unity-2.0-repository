public with sharing class vMarket_DevApprovalStatusController {
    public String appStatus{get;set;}// stores the value passed to component, represents status of app
    public String selectedRecId{get;set;} // Id of selected record
    public vMarket_Developer_Stripe_Info__c dev{get;set;} // Update the contact
    public String actionLabel{get;set;} // Label set Dynamically
    
    // For Pagination
    //Pagination variables and methods
    @TestVisible private integer totalRecs = 0;
    @TestVisible private integer offsetSize = 0;
    @TestVisible private integer LimitSize= 20;
    public Integer recCount {get;set;}
    public Boolean isCallSearchRecords {
    	get {
    		if(appStatus == Label.vMarket_Approved_Status) {
    			actionLabel = Label.vMarket_RejectLabel;
    		} else if(appStatus == Label.vMarket_Rejected) {
    			actionLabel = Label.vMarket_ApproveLabel;
    		} else if(appStatus == Label.vMarket_Pending) {
    			actionLabel = Label.vMarket_ApproveReject;
    		}
    		return true;
    	}
    	set;
    }
    public vMarket_DevApprovalStatusController(){
    	actionLabel = Label.vMarket_ApproveReject;
    }
    
    public Boolean isAdmin{
        get{
            String usrRole = [SELECT Id, Name, vMarket_User_Role__c FROM User WHERE Id =: userInfo.getUserId()].vMarket_User_Role__c;
            if(usrRole == Label.vMarket_AdminRole)
                return true;
            return false;
        }
        set;
    }// Boolean value chks if logged in user is Admin
    
    public List<SelectOption> getStatusValue() {
        List<SelectOption> opt = new List<SelectOption>();
        if(appStatus == Label.vMarket_Pending) {
        	opt.add(new SelectOption(Label.vMarket_Approved_Status,Label.vMarket_Approved_Status));
            opt.add(new SelectOption(Label.vMarket_Rejected, Label.vMarket_Rejected));
        } else if(appStatus == Label.vMarket_Approved_Status){
            opt.add(new SelectOption(Label.vMarket_Approved_Status,Label.vMarket_Approved_Status));
            opt.add(new SelectOption(Label.vMarket_Rejected, Label.vMarket_Rejected));
        } else {
        	opt.add(new SelectOption(Label.vMarket_Rejected, Label.vMarket_Rejected));
        	opt.add(new SelectOption(Label.vMarket_Approved_Status,Label.vMarket_Approved_Status));
        }
        return opt;
    }
    
    public void selectedDeveloper() {
    	
    	dev = [SELECT Id, Name, Access_Token__c, isActive__c, Scope__c, Stripe_Id__c, Stripe_User__c, Token_Type__c, vMarket_Accept_Terms_of_Use__c, 
        												Developer_Request_Status__c,Contact__c, Contact__r.Name, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email,
        												Stripe_User__r.Name, Rejection_Reason__c 
        										FROM vMarket_Developer_Stripe_Info__c 
                WHERE Contact__c =: selectedRecId AND Developer_Request_Status__c =: appStatus LIMIT 1];
		system.debug(' ======== dev ========= ' + dev);   
        //.vMarket_UserStatus__c = Label.vMarket_Approved_Status__c;
        //update cont;
        //return null;
    }
    
    public list<vMarket_Developer_Stripe_Info__c> getWrapperList() {
    	Integer offSetTemp;
    	if(offsetSize < 0)
    		offSetTemp = 0;
    	else
    		offSetTemp = offsetSize;
    	integer limitSizeTemp = LimitSize;
    	
        list<vmarket_Developer_Stripe_info__c> infoList = new list<vmarket_Developer_Stripe_info__c>(); 
        String infoSOQL = 'SELECT Id, Name, Access_Token__c, isActive__c, Scope__c, Stripe_Id__c, Stripe_User__c, Token_Type__c, vMarket_Accept_Terms_of_Use__c, Developer_Request_Status__c, '+
                            ' Contact__c, Contact__r.Name, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, Stripe_User__r.Name, Rejection_Reason__c FROM vMarket_Developer_Stripe_Info__c ' +
                    ' WHERE isActive__c = true AND Developer_Request_Status__c =:   appStatus ';
        
        String countSOQL = 'SELECT Count() FROM vMarket_Developer_Stripe_Info__c  WHERE isActive__c = true AND Developer_Request_Status__c =: appStatus ';
        totalRecs = Database.countQuery(countSOQL);
        recCount = totalRecs;
        infoSOQL += ' LIMIT : limitSizeTemp OFFSET : offsetSize ';
        system.debug(' === infoSOQL ==== ' + infoSOQL);
        
        
        infoList = Database.Query(infoSOQL);
        
        return infoList;
    }
    
    public pagereference updateDeveloperInfo() {
    	try {
    		system.debug(' ========= selectedRecId =========== ' + selectedRecId);
    		system.debug(selectedRecId + ' ========= updateDeveloperInfo =========== ' + dev);
    		system.debug(' ==== dev.Developer_Request_Status__c ==== ' + dev.Developer_Request_Status__c);
    		system.debug(' ==== dev.Rejection_Reason__c ==== ' + dev.Rejection_Reason__c);
    		if(!dev.isActive__c) {
    			ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record is not active'));
    			return null;
    		} else if(!dev.vMarket_Accept_Terms_of_Use__c) {
    			ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'User not accepted Terms & Condition'));
    			//system.assertEquals(1,3);
    			return null;
    		} else if(dev.Stripe_Id__c == '' && dev.Stripe_Id__c == null) {
    			ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Stripe account Id is required'));
    			return null;
    		}
    		system.debug(' ==== dev ==== ' + dev);
    		update dev;
    		return null;
    	} catch(Exception e) {
    		system.debug(' === ' + e.getMessage() + ' - ' + e.getLineNumber());
    		return null;
    	}
    }
    
    public void First() {
    	offsetSize = 0;
    	getWrapperList();
    }
    
    public void Next() {
    	offsetSize += LimitSize;
    	getWrapperList();
    }
    
    public void Previous() {
    	offsetSize -= LimitSize;
    	getWrapperList();
    }
    
    public void Last() {
    	offsetSize = totalRecs - math.mod(totalRecs, LimitSize);
    	getWrapperList();
    }
    
    
    public class userInfoWrapperController {
        public Contact cont{get;set;}
        public vMarket_Developer_Stripe_Info__c devInfo{get;set;}
        
        public userInfoWrapperController(Contact c, vMarket_Developer_Stripe_info__c info) {
            this.cont = c;
            this.devInfo = info;
        }
        
    }
    
}
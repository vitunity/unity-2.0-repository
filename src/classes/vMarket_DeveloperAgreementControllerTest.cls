@isTest
public with sharing class vMarket_DeveloperAgreementControllerTest {
    
    static Profile admin,portal;   
      static User customer1, customer2, developer1, ADMIN1;
      static Account account;
      static Contact contact;
      
      static {
          admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        account = vMarketDataUtility_Test.createAccount('test account', true);
        contact = vMarketDataUtility_Test.contact_data(account, true);
        contact.oktaid__c ='abhishekkolipe67936';
        update contact;
        customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact.Id, 'Customer1');
      }
    
    
    public static testMethod void agreementtest() 
    {
      test.StartTest();
      System.runAs(customer1)
        {
          vMarket_DeveloperAgreementController da = new vMarket_DeveloperAgreementController();
          da.strUserId = userinfo.getuserid();
          System.debug(da.vMarketStripeURL);
          System.debug(da.isDevPending);
          System.debug(da.isDev);
          da.devRequestPending();
          da.getArchievedVersionAvailable();
          da.getArchievedVersions();
          da.validateUser();          
          da.agreeTerms();
          da.disAgreeTerms();
          da.fetchDevRecords();
          PageReference myVfPage = Page.vMarket_DeveloperAgreement;
          Test.setCurrentPage(myVfPage);

          ApexPages.currentPage().getParameters().put('code','abc');
          ApexPages.currentPage().getParameters().put('scope','read_only');
          Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
          da.authorizeConnectAccount1();
		
           da.skipStripe();
      }
      test.StopTest();
            
    }
    }
/**
 *  @createdBy : Puneet Mishra
 *  @description: controller responsible for creating realtion between User and their Stripe AccountId
 */
public with sharing class vMarket_DeveloperStripeInfo {
    public vMarketStripeDetail__c stripe{get;set;}
    
    public vMarket_DeveloperStripeInfo() {
        stripe = new vMarketStripeDetail__c();
    }
    
    public pageReference saveDetails() {
        String Uid = UserInfo.getUserId();
        stripe.User__c = UserInfo.getUserId();
        insert stripe;
        
        
        return new pageReference('/vMarketUploadForm');
    }
}
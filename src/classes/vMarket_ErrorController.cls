/**
 *  @author     :   Puneet Mishra
 *  @desc       :   Controller handle the error msg to display on Page
 */
public with sharing class vMarket_ErrorController extends vMarketBaseController{
   public Boolean isSubscription{get;set;}
     public String vMarketStripeURL{
      get {
        String clientId = Label.vMarket_StripeConnect;
            if(vMarket_StripeAPI.clientKey != null) {
              system.debug(' ============= vMarket_StripeAPI.clientKey ================== ' + vMarket_StripeAPI.clientKey);
              system.debug(' ============= clientId ================== ' + clientId);
              clientId = clientId.replace('CLIENT', vMarket_StripeAPI.clientKey);
            }
            return clientId;
      }
    set;}
    
    public string errorMsg{
        get {
            system.debug(' == logged in user == ' + isGuestProfile(userInfo.getUserId()));
            if(ApexPages.currentPage().getParameters().get('err') != null) { // if parameter is null
                system.debug(' ===111===== ' + Label.vMarket_MyOrder_LoginMsg);
                return Label.vMarket_MyOrder_LoginMsg;
            } else if(isGuestProfile(userInfo.getUserId()) ) {
                system.debug(' ===222===== ' + Label.vMarket_TryAgain);
                return Label.vMarket_MyOrder_LoginMsg;
            } else {
                system.debug(' ===333===== ' + Label.vMarket_TryAgain);
                return Label.vMarket_TryAgain;
            }
        }
    set;} // custom messge to display on page
    
    /**
     *  @desc   :   if archieved version available
     */
    public Boolean getArchievedVersionAvailable() {
        List<vMarket_DeveloperArchievedTermsOfUse__c> archievedVerList = new List<vMarket_DeveloperArchievedTermsOfUse__c>();
        archievedVerList = vMarket_DeveloperArchievedTermsOfUse__c.getAll().values();
        if(archievedVerList.isEmpty())
            return false;
        return true;
    }
    
    /**
     * @desc    :   method return list of archieved Terms of Use
     */ 
    public Map<String, string> getArchievedVersions() {
        Map<String, String> optionMap = new Map<String, String>();
        //Map<Id, vMarket_DeveloperArchievedTermsOfUse__c> archievedVerMap = vMarket_DeveloperArchievedTermsOfUse__c.getAll();
        for(vMarket_DeveloperArchievedTermsOfUse__c ver : vMarket_DeveloperArchievedTermsOfUse__c.getAll().values()) {
            optionMap.put( (ver.Name.trim()), ver.Archieved_Resource__c );
        }
        return optionMap;
    }
    
    public vMarket_ErrorController() {
     if(apexpages.currentpage()!=null && apexpages.currentpage()!=null && apexpages.currentpage().getparameters().get('subscribe')!=null) 
        {
            isSubscription= true;
        }    
        else
        { 
            isSubscription = false;
        }    
    }
    
    // checking if user is guest user, return true if GuestUser else false
    public Boolean getLoginUserStatus() {
        User usr = [SELECT Id, profileId, vMarket_User_Role__c FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        List<Profile> prof = new List<Profile>();
        prof =  [SELECT id, Name FROM Profile WHERE Id=: usr.profileId AND Name =: Label.vMarket_GuestProfile LIMIT 1];
        system.debug(' ======= ' + prof);
        if(!prof.isEmpty()) {
            return false;
        } else {
            return true;
        }
        return false;
    }
    
    /**
     *    @desc    :    method
     */
    public pageReference agreeTerms() {
        String usrId = userInfo.getUserId();
        User usr;
        if(usrId != null) {
            usr = new User(id = usrId, vMarketTermsOfUse__c = true);
        }
        update usr;
        PageReference ref = new PageReference('/vMarketCatalogue');
        ref.setRedirect(true);
        return ref;
    }
    /**
     *
     *
     */
    public pageReference disAgreeTerms() {
        String usrId = userInfo.getUserId();
        User usr;
        if(usrId != null) {
            usr = new User(id = usrId, vMarketTermsOfUse__c = false);
        }
        update usr;
        PageReference ref = new PageReference('/vMarketLogout');
        ref.setRedirect(true);
        return ref;        
    }
}
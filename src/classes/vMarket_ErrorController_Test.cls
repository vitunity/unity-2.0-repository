/**
 *  @author    :    Puneet Mishra
 *  @description:    test Class for vMarket_ErrorController
 */
@isTest
public with sharing class vMarket_ErrorController_Test {
 	
 	static Profile admin,portal;   
    static User customer1, customer2, developer1, developer2;
    static User adminUsr1,adminUsr2;
 	static List<User> lstUserInsert;
 	static Account account;
	static Contact contact1, contact2, contact3, contact4;
	static List<Contact> lstContactInsert;
     
 	static {
 		admin = vMarketDataUtility_Test.getAdminProfile();
    	portal = vMarketDataUtility_Test.getPortalProfile();
 		
 		lstUserInsert = new List<User>();
    	lstUserInsert.add( adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin1'));
		lstUserInsert.add( adminUsr2 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin2'));
		insert lstUserInsert; 
 		
 		account = vMarketDataUtility_Test.createAccount('test account', true);
      
		lstContactInsert = new List<Contact>();
      	lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
    	lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
    	lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
    	lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
      	insert lstContactInsert;
 		
 		System.runAs(adminUsr1) {
			lstUserInsert = new List<User>();
	      	lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
	       	lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(false, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
	       	lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
	       	lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
	      	insert lstUserInsert;
 		}
 	}
 	
	public static testMethod void testErrorMsgs_LoginMsg() {
		System.currentPageReference().getParameters().put('err', 'SOMEERROR');
		vMarket_ErrorController error = new vMarket_ErrorController();
		system.assertEquals(error.errorMsg, 'PLEASE LOGIN TO VIEW YOUR ORDERS');
	}
	
	public static testMethod void testErrorMsgs_GuestUser() {
		Test.StartTest();
			system.runAs(customer1) {
				vMarket_ErrorController error1 = new vMarket_ErrorController();
				//system.assertEquals(error1.errorMsg, 'PLEASE LOGIN TO VIEW YOUR ORDERS');
			}
		Test.StopTest();
	}
	
	public static testMethod void testErrorMsgs_TryAgain() {
		Test.StartTest();
			vMarket_ErrorController error1 = new vMarket_ErrorController();
			system.assertEquals(error1.errorMsg, 'Please try again later !');
		Test.StopTest();
	}
	
	public static testMethod void test_getArchievedVersionAvailable_false() {
		test.StartTest();
		vMarket_ErrorController error1 = new vMarket_ErrorController();
		system.assertEquals(false, error1.getArchievedVersionAvailable());
		test.StopTest();
	}
	
	public static testMethod void test_getArchievedVersionAvailable_true() {
		test.StartTest();
		vMarketDataUtility_Test.createArchievedData();
		
		vMarket_ErrorController error1 = new vMarket_ErrorController();
		system.assertEquals(false, error1.getArchievedVersionAvailable());
		test.StopTest();
	}
	
	
	public static testMethod void getLoginUserStatus_Test() {
		Test.StartTest();
			vMarket_ErrorController error = new vMarket_ErrorController();
			system.assertEquals(true, error.getLoginUserStatus());
			
			system.runAs(customer1) {
				system.assertEquals(true, error.getLoginUserStatus());
			}
		Test.StopTest();
	}
	
	public static testMethod void agreeTerms_Test() {
		Test.StartTest();
			system.runAs(customer2) {
				vMarket_ErrorController error = new vMarket_ErrorController();
				PageReference pageRef = Page.vMarketCatalogue;
				PageReference resultRef = error.agreeTerms();
				
				//system.assertEquals(pageRef, resultRef);
			}
		Test.StopTest();
	}
	
	public static testMethod void disAgreeTerms_Test() {
		Test.StartTest();
			vMarketDataUtility_Test.createDEVArchievedData();
			
			vMarket_ErrorController error = new vMarket_ErrorController();
			error.disAgreeTerms();
		Test.StopTest();
	}
	
	public static testMethod void getArchievedVersions_Developers_test() {
		Test.Starttest();
			
			List<vMarket_DeveloperArchievedTermsOfUse__c> termsList = new List<vMarket_DeveloperArchievedTermsOfUse__c>();
			
			vMarket_DeveloperArchievedTermsOfUse__c terms1 = new vMarket_DeveloperArchievedTermsOfUse__c(Name = 'Privacy', Active__c = true, Archieved_Resource__c ='/resource/12345', 
															Year__c = '2015');
			vMarket_DeveloperArchievedTermsOfUse__c terms2 = new vMarket_DeveloperArchievedTermsOfUse__c(Name = 'Privacy2', Active__c = false, Archieved_Resource__c ='/resource/123456', 
															Year__c = '2016');
			
			termsList.add(terms1);
			termsList.add(terms2);
			insert termsList;
			
			vMarket_ErrorController control = new vMarket_ErrorController();
			String str = control.vMarketStripeURL;
    		control.getArchievedVersions();
			
		Test.Stoptest();
	}
}
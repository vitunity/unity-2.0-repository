/**
 * Created By : Puneet Mishra
 * 13 Feb 2016
 * Controller responsible for creating the Http Request Header which is common for all HTTP request for Stripe
 */
public class vMarket_HttpRequest {
    
    // create Http Request
    public static HttpRequest createHttpRequest(String endPoint, String method, Map<String, String> requestBody, Boolean isSubscription, vMarketOrderItem__c orderItem) {
        
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod(method);
        Blob headerValue = Blob.valueOf(vMarket_StripeAPI.ApiKey + ':');
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);
        if(isSubscription)
            request.setHeader('stripe_account','acct_14fvapIDSfmkyahe');
        if(!requestBody.isEmpty())
            request.setBody(vMarket_StripeAPIUtil.mapToRequestBody(requestBody, orderItem));
        system.debug(' === REQUEST === ' + request);
        return request;
    }
    
    
    
}
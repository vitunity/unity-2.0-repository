/**
 * @Created by Puneet Mishra
 * Controller responsible for Response Handling
 */
public class vMarket_HttpResponse {
    
    @testVisible private static Http ht;
    @testVisible private static HttpResponse response;
    @testVisible private static Map<String, String> requestResult;
    public vMarket_HttpResponse() {
        
    }
    
    public static Map<String, String> sendRequest(HttpRequest request) {
        requestResult = new Map<String, String>();
        ht = new Http();
        response = new HttpResponse();
        
        try{
            response = ht.send(request);
            requestResult.put('Body', response.getBody());
            requestResult.put('Code', String.valueOf(response.getStatusCode()) );
            
            system.debug(' == Response == ' + response);
            system.debug(' == ResponseBody == ' + response.getBody());
            system.debug(' == statusCode == ' + response.getStatusCode());
            
            return requestResult;
        } Catch(Exception e) {
            requestResult.put('Body', 'Exception Occured');
            requestResult.put('Code', '500');
            return requestResult;
        }
        return new Map<String, String>();
    }
    
    public static void dummyText() {
      Map<String, String> dummyMap = new Map<String, String>();
      Map<String, String> dummyMap2 = new Map<String, String>();
      Integer numbers;
      String testName;
      dummyMap.put('A', 'A');
      dummyMap.put('B', 'B');
      dummyMap.put('C', 'C');
    }
}
/**
 *	@author		:		Puneet Mishra
 *	@description:		mock response generator
 */
@istest
global with sharing class vMarket_HttpResponseMockGenerator implements HttpCalloutMock {
    public String resBody  = '{'+
						  '"id": "ch_1AWgexBwP3lQPSSF047OgJW3",'+
						  '"object": "charge",'+
						  '"amount": 100000,'+
						  '"amount_refunded": 0,'+
						  '"application": null,'+
						  '"application_fee": null,'+
						  '"balance_transaction": "txn_1AWgeyBwP3lQPSSFiLDBvIOm",'+
						  '"captured": true,'+
						  '"created": 1497943223,'+
						  '"currency": "usd",'+
						  '"customer": null,'+
						  '"description": "nullOLID-1748-OLID-1749-",'+
						  '"destination": null,'+
						  '"dispute": null,'+
						  '"failure_code": null,'+
						  '"failure_message": null,'+
						  '"fraud_details": {},'+
						  '"invoice": null,'+
						  '"livemode": false,'+
						  '"metadata": {'+
						    '"Name": "publicmail@mailinator.com.vms",'+
						    '"Mail": "0054C000000maJGQAY"'+
						  '},'+
						  '"on_behalf_of": null,'+
						  '"order": null,'+
						  '"outcome": {'+
						    '"network_status": "approved_by_network",'+
						    '"reason": null,'+
						    '"risk_level": "normal",'+
						    '"seller_message": "Payment complete.",'+
						    '"type": "authorized"'+
						  '},'+
						  '"paid": true,'+
						  '"receipt_email": null,'+
						  '"receipt_number": null,'+
						  '"refunded": false,'+
						  '"refunds": {'+
						    '"object": "list",'+
						    '"data": [],'+
						    '"has_more": false,'+
						    '"total_count": 0,'+
						    '"url": "/v1/charges/ch_1AWgexBwP3lQPSSF047OgJW3/refunds"'+
						  '},'+
						  '"review": null,'+
						  '"shipping": null,'+
						  '"source": {'+
						    '"id": "card_1AWgeuBwP3lQPSSFqqA1YGDN",'+
						    '"object": "card",'+
						    '"address_city": null,'+
						    '"address_country": null,'+
						    '"address_line1": null,'+
						    '"address_line1_check": null,'+
						    '"address_line2": null,'+
						    '"address_state": null,'+
						    '"address_zip": null,'+
						    '"address_zip_check": null,'+
						    '"brand": "Visa",'+
						    '"country": "US",'+
						    '"customer": null,'+
						    '"cvc_check": "pass",'+
						    '"dynamic_last4": null,'+
						    '"exp_month": 11,'+
						    '"exp_year": 2023,'+
						    '"fingerprint": "A7Ip4HP2Tgc8b4EC",'+
						    '"funding": "credit",'+
						    '"last4": "4242",'+
						    '"metadata": {},'+
						    '"name": "puneet@mail.com",'+
						    '"tokenization_method": null'+
						  '},'+
						  '"source_transfer": null,'+
						  '"statement_descriptor": null,'+
						  '"status": "succeeded",'+
						  '"transfer_group": null'+
						'}';
    
    // Implementing interface method
    global HttpResponse respond(HTTPRequest req) {
    	// creating a fake response
    	Httpresponse res = new Httpresponse();
    	res.setBody(resBody);
    	res.setStatusCode(200);
    	return res;
    }
}
/**
 *	@author		:		Puneet Mishra
 *	@description:		Test Class for HttpResponse
 *	@mock class	:		vMarket_HttpResponseMockGenerator
 */
@isTest
public with sharing class vMarket_HttpResponseTest {
    static Stripe_Settings__c setting;
  
	static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2;
    static List<vMarket_App__c> appList;
 	
 	static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
	
	static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
	
	static Profile admin,portal;   
   	static User customer1, customer2, developer1;
   	static List<User> lstUserInsert;
   	static Account account;
	static Contact contact1, contact2, contact3;
   	static List<Contact> lstContactInsert;
   	
   	static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
	
	static {
		admin = vMarketDataUtility_Test.getAdminProfile();
    	portal = vMarketDataUtility_Test.getPortalProfile();
    	
    	account = vMarketDataUtility_Test.createAccount('test account', false);
    	account.Ext_Cust_Id__c = '6051213';
    	insert account;
    	
    	ERP_Partner__c erpPartner = new ERP_Partner__c();
    	erpPartner.Name = '21C/AMBERGRIS LLC';
    	erpPartner.Active__c = true;
    	erpPartner.Account_Group__c = 'Ship To';
    	erpPartner.Street__c = '2000 FOUNDATION WY, SUITE 1100';
    	erpPartner.City__c = 'MARTINSBURG';
    	erpPartner.State_Province_Code__c = 'WV';
    	erpPartner.Zipcode_Postal_Code__c = '25401-9003';
    	erpPartner.Country_Code__c = 'US';
    	erpPartner.Partner_Number__c = '2324449';
    	erpPartner.Partner_Name__c = '21C/AMBERGRIS LLC';
    	insert erpPartner;
    	
    	ERP_Partner_Association__c erpAssociation = new ERP_Partner_Association__c();
    	erpAssociation.Name = '6051213#2324449#0601#SH=Ship-to party';
    	erpAssociation.Sales_Org__c = '0601';
    	erpAssociation.ERP_Partner__c = erpPartner.Id;
    	erpAssociation.Partner_Function__c = 'BP=Bill-to Party';
    	erpAssociation.Customer_Account__c = account.Id;
    	erpAssociation.ERP_Reference__c = '6051213#2324449#0601#SH=Ship-to party';
    	erpAssociation.ERP_Customer_Number__c = '6051213';
    	erpAssociation.ERP_Partner_Number__c = '2324449';
    	erpAssociation.Delivery_Priority_Default__c = '03-Normal';
    	erpAssociation.Shipping_Condition_Default__c = '03-Normal / Regular';
    	insert erpAssociation;
    	
    	lstContactInsert = new List<Contact>();
    	contact1 = vMarketDataUtility_Test.contact_data(account, false);
    	contact1.MailingStreet = '53 Street';
    	contact1.MailingCity = 'Palo Alto';
    	contact1.MailingCountry = 'USA';
    	contact1.MailingPostalCode = '94035';
    	contact1.MailingState = 'CA';
    	
		contact2 = vMarketDataUtility_Test.contact_data(account, false);
		contact2.MailingStreet = '660 N';
    	contact2.MailingCity = 'Milpitas';
    	contact2.MailingCountry = 'USA';
    	contact2.MailingPostalCode = '94035';
    	contact2.MailingState = 'CA';
    	
    	lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
    	
    	lstContactInsert.add(contact1);
    	lstContactInsert.add(contact2);
    	insert lstContactInsert;
    	
    	lstUserInsert = new List<User>();
    	lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
		lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
    	lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
    	
    	insert lstUserinsert;
    	
    	cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
    	ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
    	app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
    	app1.ApprovalStatus__c = 'Published';
    	app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
    	
    	appList = new List<vMarket_App__c>();
    	appList.add(app1);
    	appList.add(app2);
    	
    	insert appList;
    	
    	listing = vMarketDataUtility_Test.createListingData(app1, true);
    	comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
    	activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
    	
    	source = vMarketDataUtility_Test.createAppSource(app1, true);
    	feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
    	
    	orderItemList = new List<vMarketOrderItem__c>();
    	system.runAs(Customer1) {
    		orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
    	}
    	system.runAs(Customer2) {
    		orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
    	}
    	
    	orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
    	orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
    	
		setting = vMarketDataUtility_test.createStripSetting(true);
	}
	
	public static String resBody  = '{'+
						  '"id": "ch_1AWgexBwP3lQPSSF047OgJW3",'+
						  '"object": "charge",'+
						  '"amount": 100000,'+
						  '"amount_refunded": 0,'+
						  '"application": null,'+
						  '"application_fee": null,'+
						  '"balance_transaction": "txn_1AWgeyBwP3lQPSSFiLDBvIOm",'+
						  '"captured": true,'+
						  '"created": 1497943223,'+
						  '"currency": "usd",'+
						  '"customer": null,'+
						  '"description": "nullOLID-1748-OLID-1749-",'+
						  '"destination": null,'+
						  '"dispute": null,'+
						  '"failure_code": null,'+
						  '"failure_message": null,'+
						  '"fraud_details": {},'+
						  '"invoice": null,'+
						  '"livemode": false,'+
						  '"metadata": {'+
						    '"Name": "publicmail@mailinator.com.vms",'+
						    '"Mail": "0054C000000maJGQAY"'+
						  '},'+
						  '"on_behalf_of": null,'+
						  '"order": null,'+
						  '"outcome": {'+
						    '"network_status": "approved_by_network",'+
						    '"reason": null,'+
						    '"risk_level": "normal",'+
						    '"seller_message": "Payment complete.",'+
						    '"type": "authorized"'+
						  '},'+
						  '"paid": true,'+
						  '"receipt_email": null,'+
						  '"receipt_number": null,'+
						  '"refunded": false,'+
						  '"refunds": {'+
						    '"object": "list",'+
						    '"data": [],'+
						    '"has_more": false,'+
						    '"total_count": 0,'+
						    '"url": "/v1/charges/ch_1AWgexBwP3lQPSSF047OgJW3/refunds"'+
						  '},'+
						  '"review": null,'+
						  '"shipping": null,'+
						  '"source": {'+
						    '"id": "card_1AWgeuBwP3lQPSSFqqA1YGDN",'+
						    '"object": "card",'+
						    '"address_city": null,'+
						    '"address_country": null,'+
						    '"address_line1": null,'+
						    '"address_line1_check": null,'+
						    '"address_line2": null,'+
						    '"address_state": null,'+
						    '"address_zip": null,'+
						    '"address_zip_check": null,'+
						    '"brand": "Visa",'+
						    '"country": "US",'+
						    '"customer": null,'+
						    '"cvc_check": "pass",'+
						    '"dynamic_last4": null,'+
						    '"exp_month": 11,'+
						    '"exp_year": 2023,'+
						    '"fingerprint": "A7Ip4HP2Tgc8b4EC",'+
						    '"funding": "credit",'+
						    '"last4": "4242",'+
						    '"metadata": {},'+
						    '"name": "puneet@mail.com",'+
						    '"tokenization_method": null'+
						  '},'+
						  '"source_transfer": null,'+
						  '"statement_descriptor": null,'+
						  '"status": "succeeded",'+
						  '"transfer_group": null'+
						'}';
	
	public static String tax = '{  '+
								   '"RETURN":{  '+
								      '"TYPE":"",'+
								      '"ID":"",'+
								      '"NUMBER":"000",'+
								      '"MESSAGE":"",'+
								      '"LOG_NO":"",'+
								      '"LOG_MSG_NO":"000000",'+
								      '"MESSAGE_V1":"",'+
								      '"MESSAGE_V2":"",'+
								      '"MESSAGE_V3":"",'+
								      '"MESSAGE_V4":"",'+
								      '"PARAMETER":"",'+
								      '"ROW":0,'+
								      '"FIELD":"",'+
								      '"SYSTEM":""'+
								   '},'+
								   '"TAX_DATA":[  '+
								      '{  '+
								         '"INVOICE_ID":"",'+
								         '"ITEM_NO":"0",'+
								         '"TAX_TYPE":"TJ",'+
								         '"TAX_NAME":"Tax Jur. Code Level-1",'+
								         '"TAX_CODE":"000001710",'+
								         '"TAX_RATE":" 6.25",'+
								         '"TAX_AMOUNT":" 268.6875",'+
								         '"TAX_JUR_LVL":""'+
								      '},'+
								      '{  '+
								         '"INVOICE_ID":"",'+
								         '"ITEM_NO":"0",'+
								         '"TAX_TYPE":"TJ",'+
								         '"TAX_NAME":"Tax Jur. Code Level-2",'+
								         '"TAX_CODE":"000001710",'+
								         '"TAX_RATE":" 1",'+
								         '"TAX_AMOUNT":" 42.99",'+
								         '"TAX_JUR_LVL":""'+
								      '},'+
								      '{  '+
								         '"INVOICE_ID":"",'+
								         '"ITEM_NO":"0",'+
								         '"TAX_TYPE":"TJ",'+
								         '"TAX_NAME":"Tax Jur. Code Level-4",'+
								         '"TAX_CODE":"000001710",'+
								         '"TAX_RATE":" 1.75",'+
								         '"TAX_AMOUNT":" 75.2325",'+
								         '"TAX_JUR_LVL":""'+
								      '}'+
								   ']'+
								'}';
	
	private static testmethod void sendRequest() {
		test.startTest();
			orderItem1.TaxDetails__c = tax;
    			update orderItem1;
			system.runas(customer1) {
				
				
				Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
				
				Map<String, String> reqBody = new Map<String, String>();
	     	 	reqBody.put('currency', 'USD'); // This can be dynamic based on the Country of User
	          	reqBody.put('amount', String.valueOf(10000));
	          	reqBody.put('description', 'TESTCLASSDESCRIPTION');
	          	reqBody.put('source','123423234');
	      
	      		HttpRequest req = vMarket_HttpRequest.createHttpRequest('https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php', 'POST', reqBody, false, orderItem1);
	      		
	      		//{"ITEM_DATA":[{"CUSTOMER_NO":"4197475","CURRENCY":"USD","GROSS_AMOUNT":"500.00","POSTAL_CODE":"94305","COUNTRY":"US", }],"PROD_CODE":"A"}
	      		
	      		
				vMarket_HttpResponse resp = new vMarket_HttpResponse();
				 
				Map<String, String> response = vMarket_HttpResponse.sendRequest(req);
				//system.assertEquals(response.get('Body'), resBody);
				//system.assertEquals(response.get('Code'), '200');
			}
		test.Stoptest();
	}
	
	private static testmethod void sendRequest_Exception() {
		test.startTest();
			system.runas(customer1) {
				orderItem1.TaxDetails__c = tax;
    			update orderItem1;
				vMarket_HttpResponse.dummyText();
				//Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
				
				vMarket_HttpResponse resp = new vMarket_HttpResponse();
				 
				Map<String, String> response = vMarket_HttpResponse.sendRequest(new HttpRequest());
			}
			//system.assertEquals(response.get('Body'), resBody);
			//system.assertEquals(response.get('Code'), '200');
		test.Stoptest();
	}
}
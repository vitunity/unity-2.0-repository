/**
 *	Created By : Puneet Mishra
 *	Description : Controller dedicated specifically for JSON Parsing
 *
 */
public class vMarket_JSONParser {
	public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

	public String id {get;set;} 
	public String object_Z {get;set;} // in json: object
	public Integer amount {get;set;} 
	public Integer amount_refunded {get;set;} 
	public String application {get;set;} 
	public String application_fee {get;set;} 
	public String balance_transaction {get;set;} 
	public Boolean captured {get;set;} 
	public Integer created {get;set;} 
	public String customer {get;set;} 
	public String description {get;set;} 
	public String destination {get;set;} 
	public String dispute {get;set;} 
	public String failure_code {get;set;} 
	public String failure_message {get;set;} 
	public Fraud_details fraud_details {get;set;} 
	public String invoice {get;set;} 
	public Boolean livemode {get;set;} 
	public Fraud_details metadata {get;set;} 
	public String on_behalf_of {get;set;} 
	public String currency_z{get;set;}
	public String order_z {get;set;} 
	public Outcome outcome {get;set;} 
	public Boolean paid {get;set;} 
	public String receipt_email {get;set;} 
	public String receipt_number {get;set;} 
	public Boolean refunded {get;set;} 
	public Refunds refunds {get;set;} 
	public String review {get;set;} 
	public String shipping {get;set;} 
	public Source source {get;set;} 
	public String source_transfer {get;set;} 
	public String statement_descriptor {get;set;} 
	public String status {get;set;} 
	public String transfer {get;set;} 
	public String transfer_group {get;set;} 
	
	public Error error {get;set;}
	
	public vMarket_JSONParser(JSONParser parser) {
		while (parser.nextToken() != JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != JSONToken.VALUE_NULL) {
					if (text == 'id') {
						id = parser.getText();
					} else if (text == 'object') {
						object_Z = parser.getText();
					} else if (text == 'amount') {
						amount = parser.getIntegerValue();
					} else if (text == 'amount_refunded') {
						amount_refunded = parser.getIntegerValue();
					} else if (text == 'application') {
						application = parser.getText();
					} else if (text == 'application_fee') {
						application_fee = parser.getText();
					} else if (text == 'balance_transaction') {
						balance_transaction = parser.getText();
					} else if (text == 'captured') {
						captured = parser.getBooleanValue();
					} else if (text == 'created') {
						created = parser.getIntegerValue();
					} else if (text == 'currency') {
						currency_z = parser.getText();
					} else if (text == 'customer') {
						customer = parser.getText();
					} else if (text == 'description') {
						description = parser.getText();
					} else if (text == 'destination') {
						destination = parser.getText();
					} else if (text == 'dispute') {
						dispute = parser.getText();
					} else if (text == 'failure_code') {
						failure_code = parser.getText();
					} else if (text == 'failure_message') {
						failure_message = parser.getText();
					} else if (text == 'fraud_details') {
						fraud_details = new Fraud_details(parser);
					} else if (text == 'invoice') {
						invoice = parser.getText();
					} else if (text == 'livemode') {
						livemode = parser.getBooleanValue();
					} else if (text == 'metadata') {
						metadata = new Fraud_details(parser);
					} else if (text == 'on_behalf_of') {
						on_behalf_of = parser.getText();
					} else if (text == 'order') {
						order_z = parser.getText();
					} else if (text == 'outcome') {
						outcome = new Outcome(parser);
					} else if (text == 'paid') {
						paid = parser.getBooleanValue();
					} else if (text == 'receipt_email') {
						receipt_email = parser.getText();
					} else if (text == 'receipt_number') {
						receipt_number = parser.getText();
					} else if (text == 'refunded') {
						refunded = parser.getBooleanValue();
					} else if (text == 'refunds') {
						refunds = new Refunds(parser);
					} else if (text == 'review') {
						review = parser.getText();
					} else if (text == 'shipping') {
						shipping = parser.getText();
					} else if (text == 'source') {
						source = new Source(parser);
					} else if (text == 'source_transfer') {
						source_transfer = parser.getText();
					} else if (text == 'statement_descriptor') {
						statement_descriptor = parser.getText();
					} else if (text == 'status') {
						status = parser.getText();
					} else if (text == 'transfer') {
						transfer = parser.getText();
					} else if (text == 'transfer_group') {
						transfer_group = parser.getText();
					} else if (text == 'error') {
						error = new Error(parser); 
					} else {
						System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class Outcome {
		public String network_status {get;set;} 
		public String reason {get;set;} 
		public String risk_level {get;set;} 
		public String seller_message {get;set;} 
		public String type_Z {get;set;} // in json: type

		public Outcome(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'network_status') {
							network_status = parser.getText();
						} else if (text == 'reason') {
							reason = parser.getText();
						} else if (text == 'risk_level') {
							risk_level = parser.getText();
						} else if (text == 'seller_message') {
							seller_message = parser.getText();
						} else if (text == 'type') {
							type_Z = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Outcome consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Refunds {
		public String object_Z {get;set;} // in json: object
		public List<Fraud_details> data {get;set;} 
		public Boolean has_more {get;set;} 
		public Integer total_count {get;set;} 
		public String url {get;set;} 

		public Refunds(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'object') {
							object_Z = parser.getText();
						} else if (text == 'data') {
							data = new List<Fraud_details>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								data.add(new Fraud_details(parser));
							}
						} else if (text == 'has_more') {
							has_more = parser.getBooleanValue();
						} else if (text == 'total_count') {
							total_count = parser.getIntegerValue();
						} else if (text == 'url') {
							url = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Refunds consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Fraud_details {

		public Fraud_details(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'Fraud_details consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Source {
		public String id {get;set;} 
		public String object_Z {get;set;} // in json: object
		public String address_city {get;set;} 
		public String address_country {get;set;} 
		public String address_line1 {get;set;} 
		public String address_line1_check {get;set;} 
		public String address_line2 {get;set;} 
		public String address_state {get;set;} 
		public String address_zip {get;set;} 
		public String address_zip_check {get;set;} 
		public String brand {get;set;} 
		public String country {get;set;} 
		public String customer {get;set;} 
		public String cvc_check {get;set;} 
		public String dynamic_last4 {get;set;} 
		public Integer exp_month {get;set;} 
		public Integer exp_year {get;set;} 
		public String fingerprint {get;set;} 
		public String funding {get;set;} 
		public String last4 {get;set;} 
		public Fraud_details metadata {get;set;} 
		public String name {get;set;} 
		public String tokenization_method {get;set;} 

		public Source(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'object') {
							object_Z = parser.getText();
						} else if (text == 'address_city') {
							address_city = parser.getText();
						} else if (text == 'address_country') {
							address_country = parser.getText();
						} else if (text == 'address_line1') {
							address_line1 = parser.getText();
						} else if (text == 'address_line1_check') {
							address_line1_check = parser.getText();
						} else if (text == 'address_line2') {
							address_line2 = parser.getText();
						} else if (text == 'address_state') {
							address_state = parser.getText();
						} else if (text == 'address_zip') {
							address_zip = parser.getText();
						} else if (text == 'address_zip_check') {
							address_zip_check = parser.getText();
						} else if (text == 'brand') {
							brand = parser.getText();
						} else if (text == 'country') {
							country = parser.getText();
						} else if (text == 'customer') {
							customer = parser.getText();
						} else if (text == 'cvc_check') {
							cvc_check = parser.getText();
						} else if (text == 'dynamic_last4') {
							dynamic_last4 = parser.getText();
						} else if (text == 'exp_month') {
							exp_month = parser.getIntegerValue();
						} else if (text == 'exp_year') {
							exp_year = parser.getIntegerValue();
						} else if (text == 'fingerprint') {
							fingerprint = parser.getText();
						} else if (text == 'funding') {
							funding = parser.getText();
						} else if (text == 'last4') {
							last4 = parser.getText();
						} else if (text == 'metadata') {
							metadata = new Fraud_details(parser);
						} else if (text == 'name') {
							name = parser.getText();
						} else if (text == 'tokenization_method') {
							tokenization_method = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Source consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Error {
		public String type_Z {get;set;} // in json: type
		public String message {get;set;} 
		public String param {get;set;} 

		public Error(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'type') {
							type_Z = parser.getText();
						} else if (text == 'message') {
							message = parser.getText();
						} else if (text == 'param') {
							param = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Error consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public static vMarket_JSONParser parse(String json) {
		return new vMarket_JSONParser(System.JSON.createParser(json));
	}
}
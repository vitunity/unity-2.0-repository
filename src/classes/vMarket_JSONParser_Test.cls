/**
 *	@author				:		Puneet Mishra
 *	@description		:		test class for vMarket_JSONParser_test
 */
@IsTest
public class vMarket_JSONParser_Test {
	
	// This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{'+
		'  \"id\": \"ch_1Ah3ktBwP3lQPSSFXgsJZp6I\",'+
		'  \"object\": \"charge\",'+
		'  \"amount\": 54500,'+
		'  \"amount_refunded\": 0,'+
		'  \"application\": \"test_app\",'+
		'  \"application_fee\": \"fee_1Ah3ktJ5C29lyJRjIH7eNcYu\",'+
		'  \"balance_transaction\": \"txn_1Ah3ktBwP3lQPSSFU8xxWUfR\",'+
		'  \"captured\": true,'+
		'  \"created\": 1500415283,'+
		'  \"currency\": \"usd\",'+
		'  \"customer\": \"customer1\",'+
		'  \"description\": \"nullOLID-2078-\",'+
		'  \"destination\": \"acct_19hiUUJ5C29lyJRj\",'+
		'  \"dispute\": \"test_dispute\",'+
		'  \"failure_code\": \"400\",'+
		'  \"failure_message\": \"failure_msg\",'+
		'  \"fraud_details\": {},'+
		'  \"invoice\": \"12345\",'+
		'  \"livemode\": false,'+
		'  \"metadata\": {'+
		'    \"application_fee\": \"16350\",'+
		'    \"description\": \"nullOLID-2078-\",'+
		'    \"stripefee\": \"1583.500\",'+
		'    \"TOTALPRICE_INCLU_TAX\": \"545.00\",'+
		'    \"Tax_Jur_Code_Level_1\": \"         31.25\",'+
		'    \"Tax_Jur_Code_Level_1_TAX_CODE\": \"000001710\",'+
		'    \"Tax_Jur_Code_Level_2\": \"             5\",'+
		'    \"Tax_Jur_Code_Level_2_TAX_CODE\": \"000001710\",'+
		'    \"Tax_Jur_Code_Level_4\": \"          8.75\",'+
		'    \"Tax_Jur_Code_Level_4_TAX_CODE\": \"000001710\"'+
		'  },'+
		'  \"on_behalf_of\": \"acct_19hiUUJ5C29lyJRj\",'+
		'  \"order\": \"O1234\",'+
		'  \"outcome\": {'+
		'    \"network_status\": \"approved_by_network\",'+
		'    \"reason\": \"Test_Reason\",'+
		'    \"risk_level\": \"normal\",'+
		'    \"seller_message\": \"Payment complete.\",'+
		'    \"type\": \"authorized\"'+
		'  },'+
		'  \"paid\": true,'+
		'  \"receipt_email\": \"receipt_mail\",'+
		'  \"receipt_number\": \"receipt_numer\",'+
		'  \"refunded\": false,'+
		'  \"refunds\": {'+
		'    \"object\": \"list\",'+
		'    \"data\": [],'+
		'    \"has_more\": false,'+
		'    \"total_count\": 0,'+
		'    \"url\": \"/v1/charges/ch_1Ah3ktBwP3lQPSSFXgsJZp6I/refunds\"'+
		'  },'+
		'  \"review\": \"my review\",'+
		'  \"shipping\": null,'+
		'  \"source\": {'+
		'    \"id\": \"card_1Ah3kpBwP3lQPSSFpNXay4Lq\",'+
		'    \"object\": \"card\",'+
		'    \"address_city\": \"Palo Alto\",'+
		'    \"address_country\": \"USA\",'+
		'    \"address_line1\": \"660 N, McCarthy\",'+
		'    \"address_line1_check\": \"Test Address\",'+
		'    \"address_line2\": \"Test Address 2\",'+
		'    \"address_state\": \"CA\",'+
		'    \"address_zip\": \"95035\",'+
		'    \"address_zip_check\": \"95035\",'+
		'    \"brand\": \"Visa\",'+
		'    \"country\": \"US\",'+
		'    \"customer\": \"customer1\",'+
		'    \"cvc_check\": \"pass\",'+
		'    \"dynamic_last4\": \"1234\",'+
		'    \"exp_month\": 2,'+
		'    \"exp_year\": 2034,'+
		'    \"fingerprint\": \"A7Ip4HP2Tgc8b4EC\",'+
		'    \"funding\": \"credit\",'+
		'    \"last4\": \"4242\",'+
		'    \"metadata\": {},'+
		'    \"name\": \"puneet@gmail.com\",'+
		'    \"tokenization_method\": null'+
		'  },'+
		'  \"source_transfer\": \"test_source\",'+
		'  \"statement_descriptor\": \"test_descriptor\",'+
		'  \"status\": \"succeeded\",'+
		'  \"transfer\": \"tr_1Ah3ktBwP3lQPSSF12Wj1GfN\",'+
		'  \"transfer_group\": \"group_ch_1Ah3ktBwP3lQPSSFXgsJZp6I\"'+
		'}';
		vMarket_JSONParser r = vMarket_JSONParser.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		vMarket_JSONParser.Outcome objOutcome = new vMarket_JSONParser.Outcome(System.JSON.createParser(json));
		System.assert(objOutcome != null);
		System.assert(objOutcome.network_status == null);
		System.assert(objOutcome.reason == null);
		System.assert(objOutcome.risk_level == null);
		System.assert(objOutcome.seller_message == null);
		System.assert(objOutcome.type_Z == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		vMarket_JSONParser.Refunds objRefunds = new vMarket_JSONParser.Refunds(System.JSON.createParser(json));
		System.assert(objRefunds != null);
		System.assert(objRefunds.object_Z == null);
		System.assert(objRefunds.data == null);
		System.assert(objRefunds.has_more == null);
		System.assert(objRefunds.total_count == null);
		System.assert(objRefunds.url == null);
		
		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		vMarket_JSONParser.Fraud_details objFraud_details = new vMarket_JSONParser.Fraud_details(System.JSON.createParser(json));
		System.assert(objFraud_details != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		vMarket_JSONParser.Source objSource = new vMarket_JSONParser.Source(System.JSON.createParser(json));
		System.assert(objSource != null);
		System.assert(objSource.id == null);
		System.assert(objSource.object_Z == null);
		System.assert(objSource.address_city == null);
		System.assert(objSource.address_country == null);
		System.assert(objSource.address_line1 == null);
		System.assert(objSource.address_line1_check == null);
		System.assert(objSource.address_line2 == null);
		System.assert(objSource.address_state == null);
		System.assert(objSource.address_zip == null);
		System.assert(objSource.address_zip_check == null);
		System.assert(objSource.brand == null);
		System.assert(objSource.country == null);
		System.assert(objSource.customer == null);
		System.assert(objSource.cvc_check == null);
		System.assert(objSource.dynamic_last4 == null);
		System.assert(objSource.exp_month == null);
		System.assert(objSource.exp_year == null);
		System.assert(objSource.fingerprint == null);
		System.assert(objSource.funding == null);
		System.assert(objSource.last4 == null);
		System.assert(objSource.metadata == null);
		System.assert(objSource.name == null);
		System.assert(objSource.tokenization_method == null);
		
		json = '{\"error\": {\"type\": \"invalid_request_error\", \"message\": \"The destination param must be a connected account.\",\"param\": \"destination\" }}';
		vMarket_JSONParser.Error objError = new vMarket_JSONParser.Error(System.JSON.createParser(json));
		System.assert(objError != null);
		System.assert(objError.type_Z == null);
		System.assert(objError.message == null);
		System.assert(objError.param == null);
	}	
	
}
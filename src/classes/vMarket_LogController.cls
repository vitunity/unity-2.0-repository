/**
 *  Controller for creating Logs
 */
public with sharing class vMarket_LogController {
    
    public static Schema.DescribeSObjectResult result;
    
    @TestVisible private static Set<String> keys = new Set<String>{'id', 'object', 'amount', 'application_fee', 'balance_transaction', 'currency', 'description', 'destination', 'on_behalf_of',
                                                                     'status', 'transfer', 'transfer_group'};
    
    public static vMarket_Log__c createLogs(String type, String responseBody, HttpRequest request, vMarketOrderItem__c orderItem) {
        vMarket_Log__c logs = new vMarket_Log__c();
        
        vMarket_JSONParser parser = vMarket_JSONParser.parse(responseBody);
        system.debug(' === ' + parser);
        if( parser.object_Z == 'charge') {
            Schema.DescribeSObjectResult results = Schema.getGlobalDescribe().get('vMarket_Log__c').getDescribe();  //parser.nextToken();
            logs.RecordTypeId = results.getRecordTypeInfosByName().get('Payment').getRecordTypeId();
            logs.Status__c = parser.status;
        } else if( parser.error != null) {
            logs.Error_Message__c = parser.error.message;
            logs.Error_Destination__c = parser.error.param;
            logs.Error_type__c = parser.error.type_Z;
            logs.Status__c = 'Error';
        }
        logs.Charge_Id__c = parser.id;
        logs.Amount__c = parser.amount/100;
        logs.Application_Fee__c = parser.application_fee;
        logs.Balance_Transaction__c = parser.balance_transaction;
        logs.Currency__c = parser.currency_z;
        logs.Destination__c = parser.description;
        logs.Destination_Id__c = parser.destination;
        logs.On_Behalf_Of__c = parser.on_behalf_of;
                
        if(parser.status == 'succeeded') {
            orderItem.Status__c = 'Success';
        } else {
            orderItem.Status__c = 'Failed';
        }
        //S No. 1
        orderItem.OrderPlacedDate__c = system.today();
        orderItem.Charge_Id__c = parser.id;
        
        logs.Transfer__c = parser.transfer;
        logs.Transfer_Group__c = parser.transfer_group;
        logs.RequestRawData__c = request.getBody();
        logs.ResponseRawData__c = responseBody;
        system.debug(' =======parser.amount========== ' + parser.amount);
        system.debug(' ================= ' + logs);//system.assert(false);
        insert logs;
        
        update orderItem;
        return logs;
    }
    
    public static vMarket_Log__c createTransferLogs(String status,HttpRequest request,String responseBody)
    {
    Schema.DescribeSObjectResult results = Schema.getGlobalDescribe().get('vMarket_Log__c').getDescribe();
    vMarket_Log__c logs = new vMarket_Log__c();
    logs.RecordTypeId = results.getRecordTypeInfosByName().get('stripe_transfer').getRecordTypeId();
    logs.status__C = status;
    logs.RequestRawData__c = request.getBody();
    logs.ResponseRawData__c = responseBody;
    return logs;
    }
    
    public static vMarket_Log__c createTransferErrorLogs(String errorType,String errorMessage)
    {
    Schema.DescribeSObjectResult results = Schema.getGlobalDescribe().get('vMarket_Log__c').getDescribe();
    vMarket_Log__c logs = new vMarket_Log__c();
    logs.RecordTypeId = results.getRecordTypeInfosByName().get('stripe_transfer').getRecordTypeId();
    logs.status__C = 'failed';
    logs.Error_Message__c = errorMessage;
    logs.Error_type__c = errorType;
    return logs;
    }
    
    public static void createCodeErrorLogs(String errorType,String errorMessage)
    {
    Schema.DescribeSObjectResult results = Schema.getGlobalDescribe().get('vMarket_Log__c').getDescribe();
    vMarket_Log__c logs = new vMarket_Log__c();
    logs.RecordTypeId = results.getRecordTypeInfosByName().get('code_failure').getRecordTypeId();
    logs.status__C = 'failed';
    logs.Error_Message__c = errorMessage;
    logs.Error_type__c = errorType;
    insert logs;
    }
    
    
}
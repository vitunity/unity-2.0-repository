/**
 *	@author				:		Puneet Mishra
 *	@description		:		test class for vMarket_LogController
 */ 
@isTest
public with sharing class vMarket_LogController_Test {
	 
	 public static String chargejson = '{'+
					'  \"id\": \"ch_1Ah3ktBwP3lQPSSFXgsJZp6I\",'+
					'  \"object\": \"charge\",'+
					'  \"amount\": 54500,'+
					'  \"amount_refunded\": 0,'+
					'  \"application\": null,'+
					'  \"application_fee\": \"fee_1Ah3ktJ5C29lyJRjIH7eNcYu\",'+
					'  \"balance_transaction\": \"txn_1Ah3ktBwP3lQPSSFU8xxWUfR\",'+
					'  \"captured\": true,'+
					'  \"created\": 1500415283,'+
					'  \"currency\": \"usd\",'+
					'  \"customer\": null,'+
					'  \"description\": \"nullOLID-2078-\",'+
					'  \"destination\": \"acct_19hiUUJ5C29lyJRj\",'+
					'  \"dispute\": null,'+
					'  \"failure_code\": null,'+
					'  \"failure_message\": null,'+
					'  \"fraud_details\": {},'+
					'  \"invoice\": null,'+
					'  \"livemode\": false,'+
					'  \"metadata\": {'+
					'    \"application_fee\": \"16350\",'+
					'    \"description\": \"nullOLID-2078-\",'+
					'    \"stripefee\": \"1583.500\",'+
					'    \"TOTALPRICE_INCLU_TAX\": \"545.00\",'+
					'    \"Tax_Jur_Code_Level_1\": \"         31.25\",'+
					'    \"Tax_Jur_Code_Level_1_TAX_CODE\": \"000001710\",'+
					'    \"Tax_Jur_Code_Level_2\": \"             5\",'+
					'    \"Tax_Jur_Code_Level_2_TAX_CODE\": \"000001710\",'+
					'    \"Tax_Jur_Code_Level_4\": \"          8.75\",'+
					'    \"Tax_Jur_Code_Level_4_TAX_CODE\": \"000001710\"'+
					'  },'+
					'  \"on_behalf_of\": \"acct_19hiUUJ5C29lyJRj\",'+
					'  \"order\": null,'+
					'  \"outcome\": {'+
					'    \"network_status\": \"approved_by_network\",'+
					'    \"reason\": null,'+
					'    \"risk_level\": \"normal\",'+
					'    \"seller_message\": \"Payment complete.\",'+
					'    \"type\": \"authorized\"'+
					'  },'+
					'  \"paid\": true,'+
					'  \"receipt_email\": null,'+
					'  \"receipt_number\": null,'+
					'  \"refunded\": false,'+
					'  \"refunds\": {'+
					'    \"object\": \"list\",'+
					'    \"data\": [],'+
					'    \"has_more\": false,'+
					'    \"total_count\": 0,'+
					'    \"url\": \"/v1/charges/ch_1Ah3ktBwP3lQPSSFXgsJZp6I/refunds\"'+
					'  },'+
					'  \"review\": null,'+
					'  \"shipping\": null,'+
					'  \"source\": {'+
					'    \"id\": \"card_1Ah3kpBwP3lQPSSFpNXay4Lq\",'+
					'    \"object\": \"card\",'+
					'    \"address_city\": null,'+
					'    \"address_country\": null,'+
					'    \"address_line1\": null,'+
					'    \"address_line1_check\": null,'+
					'    \"address_line2\": null,'+
					'    \"address_state\": null,'+
					'    \"address_zip\": null,'+
					'    \"address_zip_check\": null,'+
					'    \"brand\": \"Visa\",'+
					'    \"country\": \"US\",'+
					'    \"customer\": null,'+
					'    \"cvc_check\": \"pass\",'+
					'    \"dynamic_last4\": null,'+
					'    \"exp_month\": 2,'+
					'    \"exp_year\": 2034,'+
					'    \"fingerprint\": \"A7Ip4HP2Tgc8b4EC\",'+
					'    \"funding\": \"credit\",'+
					'    \"last4\": \"4242\",'+
					'    \"metadata\": {},'+
					'    \"name\": \"puneet@gmail.com\",'+
					'    \"tokenization_method\": null'+
					'  },'+
					'  \"source_transfer\": null,'+
					'  \"statement_descriptor\": null,'+
					'  \"status\": \"succeeded\",'+
					'  \"transfer\": \"tr_1Ah3ktBwP3lQPSSF12Wj1GfN\",'+
					'  \"transfer_group\": \"group_ch_1Ah3ktBwP3lQPSSFXgsJZp6I\"'+
					'}';
	
	public static String paymentjson = '{'+
					'  \"id\": \"ch_1Ah3ktBwP3lQPSSFXgsJZp6I\",'+
					'  \"object\": \"charge\",'+
					'  \"amount\": 54500,'+
					'  \"amount_refunded\": 0,'+
					'  \"application\": null,'+
					'  \"application_fee\": \"fee_1Ah3ktJ5C29lyJRjIH7eNcYu\",'+
					'  \"balance_transaction\": \"txn_1Ah3ktBwP3lQPSSFU8xxWUfR\",'+
					'  \"captured\": true,'+
					'  \"created\": 1500415283,'+
					'  \"currency\": \"usd\",'+
					'  \"customer\": null,'+
					'  \"description\": \"nullOLID-2078-\",'+
					'  \"destination\": \"acct_19hiUUJ5C29lyJRj\",'+
					'  \"dispute\": null,'+
					'  \"error\": {'+
					'    \"object\": \"list\",'+
					'    \"data\": [],'+
					'    \"has_more\": false,'+
					'    \"total_count\": 0,'+
					'    \"url\": \"/v1/charges/ch_1Ah3ktBwP3lQPSSFXgsJZp6I/refunds\"}'+
					'}';
	
	static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2, app3;
    static List<vMarket_App__c> appList;
    
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    
    static Profile admin,portal;   
   	static User customer1, customer2, developer1, developer2;
   	static List<User> lstUserInsert;
   	static Account account;
   	static Contact contact1, contact2, contact3, contact4;
   	static List<Contact> lstContactInsert;
    
    static  {
    	
    	admin = vMarketDataUtility_Test.getAdminProfile();
    	portal = vMarketDataUtility_Test.getPortalProfile();
    	
    	account = vMarketDataUtility_Test.createAccount('test account', true);
    	
    	lstContactInsert = new List<Contact>();
    	lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
		lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
		lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
		lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
    	insert lstContactInsert;
    	
    	lstUserInsert = new List<User>();
    	lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
     	lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
     	lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
     	lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
    	insert lstUserInsert;
    
    	cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
    	ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
    	app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
    	app1.ApprovalStatus__c = 'Published';
    	app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
    	app3 = vMarketDataUtility_Test.createAppTest('Test Application 3', cate, false, developer1.Id);
    	appList = new List<vMarket_App__c>();
    	appList.add(app1);
    	appList.add(app2);
    	appList.add(app3);
    	insert appList;
    	
    	listing = vMarketDataUtility_Test.createListingData(app1, true);
    	comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
    	activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
    	
    	source = vMarketDataUtility_Test.createAppSource(app1, true);
    	feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
    	
    	orderItemList = new List<vMarketOrderItem__c>();
    	system.runAs(Customer1) {
    		orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
    	}
    	system.runAs(Customer2) {
    		orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
    	}
    	orderItemList.add(orderItem1);
    	orderItemList.add(orderItem2);
    	//insert orderItemList;
    	
    	orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
    	
    	orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
    	
    }
	
	public static testMethod void Chargejson() {
		Test.StartTest();
			String TOKEN_URL = 'https://connect.stripe.com/oauth/token';
			Map<String, String> reqBody = new Map<String, String>();
	        reqBody.put('code', 'xx_yy_zzz');
	        reqBody.put('client_id', 'c_adsreyy323234ghdajsd');
	        reqBody.put('grant_type', 'read_write');
	        reqBody.put('scope', 'read_write');
	        reqBody.put('amount', '1000');
	        
			HttpRequest req = vMarket_HttpRequest.createHttpRequest(TOKEN_URL, 'POST', reqBody, false, null);
			
			vMarket_LogController.createLogs('payment', chargejson, req, orderItem1);
			
		Test.StopTest();
	}
	
	public static testMethod void paymentjson() {
		Test.StartTest();
			String TOKEN_URL = 'https://connect.stripe.com/oauth/token';
			Map<String, String> reqBody = new Map<String, String>();
	        reqBody.put('code', 'xx_yy_zzz');
	        reqBody.put('client_id', 'c_adsreyy323234ghdajsd');
	        reqBody.put('grant_type', 'read_write');
	        reqBody.put('scope', 'read_write');
	        reqBody.put('amount', '1000');
	        
			HttpRequest req = vMarket_HttpRequest.createHttpRequest(TOKEN_URL, 'POST', reqBody, false, null);
			
			vMarket_LogController.createLogs('payment', paymentjson, req, orderItem1);
			
		Test.StopTest();
	}
	
	public static testMethod void createTransferLogs_test() {
		Test.StartTest();
			String TOKEN_URL = 'https://connect.stripe.com/oauth/token';
			Map<String, String> reqBody = new Map<String, String>();
	        reqBody.put('code', 'xx_yy_zzz');
	        reqBody.put('client_id', 'c_adsreyy323234ghdajsd');
	        reqBody.put('grant_type', 'read_write');
	        reqBody.put('scope', 'read_write');
	        reqBody.put('amount', '1000');
			HttpRequest req = vMarket_HttpRequest.createHttpRequest(TOKEN_URL, 'POST', reqBody, false, null);
			vMarket_LogController.createTransferLogs('Success', req, paymentjson);
		Test.StopTest();
	}
	
	public static testMethod void createCodeErrorLogs_test() {
		Test.StartTest();
			vMarket_LogController.createCodeErrorLogs('TEST CLASS', 'This error generated from Test Class');
		Test.StopTest();
	}
	
	public static testMethod void createTransferErrorLogs_Test() {
		Test.StartTest();
			vMarket_LogController.createTransferErrorLogs('TEST CLASS', 'This error generated from Test Class');
		Test.StopTest();
	}
	
}
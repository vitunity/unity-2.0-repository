@isTest

public class vMarket_OrderTriggerTest{
    
   static vMarketOrderItem__c orderItem ;
    
    static {
       // test.StartTest();
        orderItem = new vMarketOrderItem__c(OrderPlacedDate__c = system.today().addDays(-2), Tax__c = 2);
        orderItem.Status__c =Label.vMarket_Success;
        orderItem.ShippingAddress__c = 'palo alto, Palo Alto, CA, US-94035';
        orderItem.ZipCode__c = '95035';
        orderItem.Charge_Id__c = 'ch_1A4yswBwP3lQPSSFNx16SMfK';
        orderItem.IsSubscribed__c = true;
        orderItem.tax__c = 2;
        insert orderItem;
        
        //update orderItem;
        
        //test.StopTest(); 
    }
    public static testMethod void createIndividualTransfersTest1()
     {
     
     vMarket_Tax__c tax = new vMarket_Tax__c(Name='tax',Tax__c=45.0);
     insert tax;
     vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        vmarket_app__C app1 = vMarketDataUtility_Test.createAppTest('Test Application1', cate, true, userinfo.getuserid());
        vmarket_app__C app2 = vMarketDataUtility_Test.createAppTest('Test Application2', cate, true, userinfo.getuserid());
        List<vMarketOrderItemLine__c> lineslist = new List<vMarketOrderItemLine__c>();
        vMarketOrderItemLine__c line1 = vMarketDataUtility_Test.createOrderItemLineData(app1,orderItem,false,false);
        vMarketOrderItemLine__c line2 = vMarketDataUtility_Test.createOrderItemLineData(app2,orderItem,false,false);
        lineslist.add(line1);
        lineslist.add(line2);
        insert lineslist;
     }
    }
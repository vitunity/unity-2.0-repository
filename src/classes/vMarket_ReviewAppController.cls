/***********************************************************
Author: Nilesh Gorle
Created on: 22nd Sept 17
Description: Custom approval process for reviewer.
              Reviewer can approve/reject the app 
              
Change Log:
***********************************************************/
/* Class Modified by Abhishek K as Part of Custom App Review Process VMT-21 */
public class vMarket_ReviewAppController {
 public String appId {
  get;
  set;
 }
 public String countrieslist {
  get;
  set;
 }
 public String teamName {
  get;
  set;
 }
 public String reviewerName {
  get;
  set;
 }
 public Boolean isPageShow {
  get;
  set;
 }
 public vMarket_App__c app {
  get;
  set;
 }
 public VMarket_Country_App__c countryApp {
  get;
  set;
 }
 public List < ProcessInstanceWorkitem > wItems {
  get;
  set;
 }
 public List < SelectOption > reviewers {
  get;
  set;
 }
 public String changedReviewer {
  get;
  set;
 }
 public String changeComment {
  get;
  set;
 }
 public boolean statusOk {
  get;
  set;
 }
 public boolean reviewerOk {
  get;
  set;
 }
 public boolean cOuntryOk {
  get;
  set;
 }
 public boolean currentReviewer {
  get;
  set;
 }
 public String countryCode {
  get;
  set;
 }
 public String country {
  get;
  set;
 }

 // For Providing Access on More Info
 public static boolean Logoaccess {
  get;
  set;
 }
 public static boolean Banneraccess {
  get;
  set;
 }
 public static boolean Screenshotsaccess {
  get;
  set;
 }
 public static boolean Guideaccess {
  get;
  set;
 }
 
 public static boolean medicalDeviceAccess {
  get;
  set;
 }
 
 public static boolean Kaccess {
  get;
  set;
 }
 public static boolean Publisheraccess {
  get;
  set;
 }
 public static boolean Categoryaccess {
  get;
  set;
 }
 public static boolean Nameaccess {
  get;
  set;
 }
 public static boolean ShortDescriptionaccess {
  get;
  set;
 }
 public static boolean LongDescriptionaccess {
  get;
  set;
 }
 public static boolean PaymentTypeaccess {
  get;
  set;
 }
 public static boolean Priceaccess {
  get;
  set;
 }
 public static boolean KeyFeaturesaccess {
  get;
  set;
 }
 public static boolean PublisherNameaccess {
  get;
  set;
 }
 public static boolean PublisherEmailaccess {
  get;
  set;
 }
 public static boolean PublisherWebsiteaccess {
  get;
  set;
 }
 public static boolean PublisherContactNumberaccess {
  get;
  set;
 }
 public static boolean DeveloperSupportaccess {
  get;
  set;
 }
 public static boolean youtubeaccess {
  get;
  set;
 }
 public static boolean sensitiveinfoaccess {
  get;
  set;
 }
 public static boolean securitymeasuresaccess {
  get;
  set;
 }
 public static boolean euprivacyaccess {
  get;
  set;
 }
 public static boolean publisheraddressaccess {
  get;
  set;
 }
 public static boolean developernameaccess {
  get;
  set;
 }
 public static Boolean statusError {
  get;
  set;
 }

 public static boolean medicalaccess {
  get;
  set;
 }
 public static boolean encryption1access {
  get;
  set;
 }
 public static boolean encryption2access {
  get;
  set;
 }
 public static list<string> typeofencr {
  get;
  set;
 }

 public Attachment attached_appLogo {
  get;
  set;
 }
 public Attachment attached_appGuide {
  get;
  set;
 }
 public Attachment attached_appBanner {
  get;
  set;
 }
 public Attachment attached_CCATS {
  get;
  set;
 }
 public Attachment attached_TechSpec {
  get;
  set;
 }
 public Attachment attached_Opinion {
  get;
  set;
 }
 public vMarketAppSource__c srcFile {
  get;
  set;
 }
 public FeedItem feedItem {
  get;
  set;
 }

 public List < Attachment > attached_documents {
  get;
  set;
 }
 
 public List < Attachment > nonUS_attached_documents {
  get;
  set;
 }

 public vMarket_ReviewAppController() {
  statusOk = false;
  reviewerOk = false;
  countryOk = false;
  currentReviewer = false;
  encryption1access = false;
  encryption2access = false;
  medicalaccess = false;
  medicalDeviceAccess=false;
  reviewers = new List < SelectOption > ();

  if (ApexPages.currentPage() != null && ApexPages.currentPage().getParameters() != null && String.IsNotBlank(ApexPages.currentPage().getParameters().get('statusError'))) statusError = true;
  else statusError = false;
  //changedReviewer = new User();
  appId = ApexPages.CurrentPage().getparameters().get('appId');

  isPageShow = true;
  String type = ApexPages.CurrentPage().getparameters().get('type');
  if (String.isNotBlank(type) && type.containsignorecase('reg')) teamName = 'Regulatory';
  if (String.isNotBlank(type) && type.containsignorecase('tech')) teamName = 'Technical';
  if (String.isNotBlank(type) && type.containsignorecase('sec')) teamName = 'Security';
  if (String.isNotBlank(type) && type.containsignorecase('trade')) teamName = 'Trade';
 }
 public vMarket_ReviewAppController(String s) {}
  /*
      Description- Checking appId exist. Also If appId exist then check user is valid to review the app. 
  */
  /* Method Added by Abhishek K as Part of Custom App Review Process VMT-21 to Find out the Visible Fields and Reviewers for a Specific Country*/
 public void checkAppIdExist() {
  attached_appLogo = new Attachment();
  attached_appGuide = new Attachment();
  attached_appBanner = new Attachment();
  attached_CCATS = new Attachment();
  attached_TechSpec = new Attachment();
  attached_Opinion = new Attachment();
  srcFile = new vMarketAppSource__c();
  Categoryaccess=true;
  Nameaccess=true;
  attached_documents = new List < Attachment > ();
  nonUS_attached_documents = new List < Attachment > ();
  statusOk = false;
  reviewerOk = false;
  typeofencr = new List<String>();
  appId = ApexPages.CurrentPage().getparameters().get('appId');

  countryCode = ApexPages.CurrentPage().getparameters().get('con');
  if (String.isBlank(countryCode)) countryok = false;
  else {
   country = (new vMarket_Approval_Util()).countriesMap().get(countryCode);
   if (String.isBlank(country)) countryok = false;
   else countryOk = true;
  }


  List < vMarket_App__c > appList = [select Id, VMarket_Sensitive_Information__c, VMarket_Personal_Info__c, VMarket_Protected_Health_Info__c, VMarket_Security_Laws__c, VMarket_Comply_Privacy_Laws__c, vMarket_Regulatory_Visible_Fields__c, VMarket_Technical_Visible_Fields__c, Tech_Review__c, Regulatory_Review__c, ApprovalStatus__c, Name, Technical_Reviewer__c, Technical_Reviewer__r.FirstName, Technical_Reviewer__r.LastName, Regulatory_Reviewer__r.FirstName, Regulatory_Reviewer__r.LastName, Regulatory_Reviewer__c, Owner.FirstName, Owner.LastName, vMarket_Approval_Admin__r.email, App_Category__r.name, Short_Description__c, Long_Description__c, Key_features__c, Price__c, PublisherWebsite__c, PublisherPhone__c, Publisher_Developer_Support__c, PublisherEmail__c, PublisherName__c, VMarket_Type_of_Encr__c from vMarket_App__c where Id = : appId];


  // SOQL to get workitem task using userId, appId and 'Pending' state
  wItems = [Select ProcessInstance.Status, ProcessInstance.TargetObjectId, ProcessInstanceId, OriginalActorId, Id, ActorId From ProcessInstanceWorkitem where ProcessInstance.TargetObjectId = : appId and OriginalActorId = : UserInfo.getUserId() and ProcessInstance.Status = 'Pending'];

  // Check appId exist in URL and getting appList on the basis of appId
  if (appId == null || appList.size() == 0) {
   isPageShow = false;
   ApexPages.addmessage(new Apexpages.Message(ApexPages.Severity.Error, 'App ID does not exist'));
  }

  // Check appId not null and user is assign to the app 
  if (appId != null && wItems.size() == 0) {
   isPageShow = false;
   ApexPages.addmessage(new Apexpages.Message(ApexPages.Severity.Error, 'You are not authorized to view this'));
  }

  if (appId != null && appList.size() > 0 && wItems.size() > 0) {
   isPageShow = true;
   app = appList[0];

  }

  //if(appList[0].ApprovalStatus__c.equals('Under Review')  ||appList[0].ApprovalStatus__c.equals('Info Requested')) statusOk = true;

  app = appList[0];

  for (Attachment a: [select Name, parentid, ContentType, body from Attachment where parentid = : app.Id]) {

   if (a.Name.startsWith('Logo_')) {
    attached_appLogo = a;
   } else if (a.Name.startsWith('AppGuide_')) {
    attached_appGuide = a;
   } else if (a.Name.startsWith('Banner_')) {
    attached_appBanner = a;
   } else if (a.Name.startsWith('CCATS_')) {
    attached_CCATS = a;
   } else if (a.Name.startsWith('TechnicalSpec_')) {
    attached_TechSpec = a;
   } else if (a.Name.startsWith('Opinion_')) {
    attached_Opinion = a;
   }

  }
  System.debug('BBBBoutside' + countryOk + countrycode);
  if (countryOk) {
   List < VMarket_Country_App__c > countryApps = [Select id,VMarket_Legal_Number__c,VMarket_Legal_Name__c,VMarket_Legal_Address__c,VMarket_App__r.Is_Medical_Device__c, VMarket_App__r.VMarket_Is_Designed_For_Medical__c, VMarket_App__r.VMarket_Implement_Encr__c, VMarket_App__r.VMarket_Primary_Purpose_Encr__c, VMarket_App__r.VMarket_Type_of_Encr__c, VMarket_Trade_Reviewer_User__r.FirstName, VMarket_Trade_Reviewer_User__r.LastName, VMarket_Trade_Review__c, VMarket_Trade_Reviewer_Change_Reason__c, VMarket_Trade_Feedback__c, VMarket_Security_Feedback__c, VMarket_Security_Reviewer_Change_Reason__c, CreatedBy.Name, VMarket_App__r.Publisher_Address__c, VMarket_App__r.VMarket_Personal_Info__c, VMarket_App__r.VMarket_Protected_Health_Info__c, VMarket_App__r.VMarket_Security_Laws__c, VMarket_App__r.VMarket_Comply_Privacy_Laws__c, VMarket_Security_Visible_Fields__c, VMarket_Security_Review__c, VMarket_Security_Reviewer__c, VMarket_Security_Reviewer__r.firstname, VMarket_Security_Reviewer__r.lastname, VMarket_Regulatory_Reviewer__r.FirstName, VMarket_Regulatory_Reviewer__r.LastName, VMarket_Technical_Reviewer__r.FirstName, VMarket_Technical_Reviewer__r.LastName, VMarket_Country_App__c.Name, VMarket_Approval_Admin__r.firstName, VMarket_App__r.Name, VMarket_App__r.Owner.FirstName, VMarket_App__c, Status__c, Country__c, VMarket_Technical_Visible_Fields__c, VMarket_Regulatory_Visible_Fields__c, VMarket_Reviewer_Change_Reason__c, VMarket_Technical_Reviewer_Change_Reason__c, VMarket_Info_Requested_Fields__c, VMarket_Date_of_Info_Requested__c, VMarket_Date_of_Submission__c, VMarket_Date_of_Approve_Reject__c, VMarket_Date_of_Approval_Request__c, VMarket_Technical_Review__c, VMarket_Regulatory_Review__c, VMarket_Technical_Reviewer__c, vMarket_Approval_Admin__c, VMarket_Regulatory_Feedback__c, VMarket_Technical_Feedback__c, VMarket_Regulatory_Reviewer__c, VMarket_Approval_Information_Required__c, VMarket_Approval_Status__c, VMarket_Rejection_Comment__c, VMarket_App__r.App_Category__r.name, VMarket_App__r.Short_Description__c, VMarket_App__r.Long_Description__c, VMarket_App__r.Price__c, VMarket_App__r.Key_features__c, VMarket_App__r.PublisherName__c, VMarket_App__r.PublisherEmail__c, VMarket_App__r.PublisherWebsite__c, VMarket_App__r.PublisherPhone__c, VMarket_App__r.Publisher_Developer_Support__c,VMarket_App__r.VMarket_Primary_Purpose_Encr_Details__c,VMarket_App__r.Encryption_Level_Technical_Details__c from VMarket_Country_App__c where VMarket_App__c = : app.id and country__C = : countrycode and status__C = 'Active'];

   if (countryApps.size() > 0) countryApp = countryApps[0];
   else countryOk = false;
  }

  if (String.IsNotBlank(teamName) && countryOk) {

   if (teamName.containsignorecase('tech')) {
    publisheraddressaccess = true;
    developernameaccess = true;
    if (String.isNotBlank(countryApp.VMarket_Technical_Visible_Fields__c)) {
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Logo')) {
      Logoaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Banner')) {
      Banneraccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Screenshots')) {
      Screenshotsaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Guide')) {
      Guideaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('501K')) {
      Kaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Category')) {
      Categoryaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('AppName')) {
      Nameaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Short')) {
      ShortDescriptionaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Long')) {
      LongDescriptionaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Payment Type')) {
      PaymentTypeaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Price')) {
      Priceaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Key Features')) {
      KeyFeaturesaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Publisher Name')) {
      PublisherNameaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Publisher Email')) {
      PublisherEmailaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Publisher Website')) {
      PublisherWebsiteaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Publisher Contact Number')) {
      PublisherContactNumberaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('Developer Support')) {
      DeveloperSupportaccess = true;
     }
     if (countryApp.VMarket_Technical_Visible_Fields__c.containsignorecase('youtube')) {
      youtubeaccess = true;
     }
    }


    /*if(appList[0].Tech_Review__c.equals('In Progress') && (appList[0].ApprovalStatus__c.equals('Under Review')  ||appList[0].ApprovalStatus__c.equals('Info Requested')))  statusOk = true;
    if(appList[0].Technical_Reviewer__c.equals(UserInfo.getUserId())) currentReviewer=true;
    reviewerName = appList[0].Technical_Reviewer__r.FirstName+appList[0].Technical_Reviewer__r.LastName;*/

    if (countryApp != null) {
     if (countryApp.VMarket_Technical_Review__c.equals('In Progress') && (countryApp.VMarket_Approval_Status__c.equals('Under Review') || countryApp.VMarket_Approval_Status__c.equals('Info Requested'))) statusOk = true;
     if (countryApp.VMarket_Technical_Reviewer__c.equals(UserInfo.getUserId())) currentReviewer = true;
     reviewerName = countryApp.VMarket_Technical_Reviewer__r.FirstName + countryApp.VMarket_Technical_Reviewer__r.LastName;

     for (VMarket_Reviewers__c reviewer: [Select Active__c, Countries__c, Default__c, type__c, User__c, User__r.lastname, User__r.firstname, User__r.email from VMarket_Reviewers__c where Active__c = true]) { //System.debug('UUUUUU'+reviewer.Countries__c.length()+countrycode.length()+reviewer.Countries__c.containsignorecase(countrycode));
      if (reviewer.type__c.containsignorecase('technical') && reviewer.Countries__c.containsignorecase(countrycode)) {
       if (currentReviewer) {
        if (!reviewer.User__c.equals(countryApp.VMarket_Technical_Reviewer__c)) reviewers.add(new SelectOption(reviewer.User__c, reviewer.User__r.firstname + reviewer.User__r.lastname));
       }
       if (reviewer.User__c.equals(UserInfo.getUserId())) {
        if (!currentReviewer) reviewers.add(new SelectOption(reviewer.User__c, reviewer.User__r.firstname + reviewer.User__r.lastname));
        reviewerOk = true;
       }
      }
     }


    }
    for (vMarketAppSource__c appsrc: [Select id from vMarketAppSource__c where IsActive__c = : true AND Status__c = : Label.vMarket_Published AND vMarket_App__c = : app.Id]) {
     srcFile = appsrc;
    }

    if (srcFile != null) {
     for (FeedItem ft: [SELECT RelatedRecordId, ParentId FROM FeedItem WHERE ParentId = : srcFile.id]) {
      feedItem = ft;
     }
    }
    Guideaccess = true;
   }
   if (teamName.containsignorecase('reg')) {
    publisheraddressaccess = true;
    developernameaccess = true;
    medicalDeviceAccess=true;
    if (String.isNotBlank(countryApp.vMarket_Regulatory_Visible_Fields__c)) {
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Logo')) {
      Logoaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Banner')) {
      Banneraccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Screenshots')) {
      Screenshotsaccess = true;
     }
     //if(countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Guide')){Guideaccess=true;}
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('501K')) {
      Kaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Category')) {
      Categoryaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('AppName')) {
      Nameaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Short')) {
      ShortDescriptionaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Long')) {
      LongDescriptionaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Payment Type')) {
      PaymentTypeaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Price')) {
      Priceaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Key Features')) {
      KeyFeaturesaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Publisher Name')) {
      PublisherNameaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Publisher Email')) {
      PublisherEmailaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Publisher Website')) {
      PublisherWebsiteaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Publisher Contact Number')) {
      PublisherContactNumberaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('Developer Support')) {
      DeveloperSupportaccess = true;
     }
     if (countryApp.vMarket_Regulatory_Visible_Fields__c.containsignorecase('youtube')) {
      youtubeaccess = true;
     }
    }

    /*if(appList[0].Regulatory_Review__c.equals('In Progress') && (appList[0].ApprovalStatus__c.equals('Under Review')  ||appList[0].ApprovalStatus__c.equals('Info Requested')))  statusOk = true;
    if(appList[0].Regulatory_Reviewer__c.equals(UserInfo.getUserId())) currentReviewer=true;
    reviewerName = appList[0].Regulatory_Reviewer__r.FirstName+appList[0].Regulatory_Reviewer__r.LastName;*/
    if (countryApp != null) {
     if (countryApp.VMarket_Regulatory_Review__c.equals('In Progress') && (countryApp.VMarket_Approval_Status__c.equals('Under Review') || countryApp.VMarket_Approval_Status__c.equals('Info Requested'))) statusOk = true;
     if (countryApp.VMarket_Regulatory_Reviewer__c.equals(UserInfo.getUserId())) currentReviewer = true;
     reviewerName = countryApp.VMarket_Regulatory_Reviewer__r.FirstName + countryApp.VMarket_Regulatory_Reviewer__r.LastName;

     for (VMarket_Reviewers__c reviewer: [Select Active__c, Countries__c, Default__c, type__c, User__c, User__r.lastname, User__r.firstname, User__r.email from VMarket_Reviewers__c where Active__c = true]) {
      System.debug('CCCCINside' + reviewer.Countries__c + countrycode);
      if (reviewer.type__c.containsignorecase('regulatory') && reviewer.Countries__c.containsignorecase(countrycode)) {
       System.debug('DDDINside' + reviewer.User__c + '---' + UserInfo.getUserId());
       if (currentReviewer) {
        if (!reviewer.User__c.equals(countryApp.VMarket_Regulatory_Reviewer__c)) reviewers.add(new SelectOption(reviewer.User__c, reviewer.User__r.firstname + reviewer.User__r.lastname));
       }
       if (reviewer.User__c.equals(UserInfo.getUserId())) {
        if (!currentReviewer) reviewers.add(new SelectOption(reviewer.User__c, reviewer.User__r.firstname + reviewer.User__r.lastname));
        reviewerOk = true;
       }
      }
     }


     for (Attachment a: [select Name, parentid, ContentType, body from Attachment where parentid = : countryApp.Id])
     //for(Attachment a :[select Name, parentid, ContentType,body from Attachment where parentid=:app.id] )
     {
        if(a.name.containsignorecase('510K Form'))
            attached_documents.add(a);
        else 
            nonUS_attached_documents.add(a);
     }


    }
    for (vMarketAppSource__c appsrc: [Select id from vMarketAppSource__c where IsActive__c = : true AND Status__c = : Label.vMarket_Published AND vMarket_App__c = : app.Id]) {
     srcFile = appsrc;
    }

    if (srcFile != null) {
     for (FeedItem ft: [SELECT RelatedRecordId, ParentId FROM FeedItem WHERE ParentId = : srcFile.id]) {
      feedItem = ft;
     }
    }
   }
   System.debug('DDDOutside' + teamName);
   if (teamName.containsignorecase('sec')) {
    publisheraddressaccess = true;
    developernameaccess = true;
    Nameaccess = true;
    ShortDescriptionaccess = true;
    encryption1access = false;
    encryption2access = false;
    medicalaccess = false;
    medicalDeviceAccess=true;
    
    if (string.isNotBlank(app.VMarket_Type_of_Encr__c)) {
        typeofencr=app.VMarket_Type_of_Encr__c.split('#');
    }
    sensitiveinfoaccess = true;

    if (String.isNOTBLANK(app.VMarket_Security_Laws__c)) securitymeasuresaccess = true;

    if (String.isNOTBLANK(app.VMarket_Comply_Privacy_Laws__c)) euprivacyaccess = true;

    if (String.isNotBlank(countryApp.VMarket_Security_Visible_Fields__c)) {

    }

    System.debug('CCCOutside' + countryApp.id);
    if (countryApp != null) {
     if (countryApp.VMarket_Security_Review__c.equals('In Progress') && (countryApp.VMarket_Approval_Status__c.equals('Under Review') || countryApp.VMarket_Approval_Status__c.equals('Info Requested'))) statusOk = true;
     if (countryApp.VMarket_Security_Reviewer__c.equals(UserInfo.getUserId())) currentReviewer = true;
     reviewerName = countryApp.VMarket_Security_Reviewer__r.FirstName + countryApp.VMarket_Security_Reviewer__r.LastName;

     for (VMarket_Reviewers__c reviewer: [Select Active__c, Countries__c, Default__c, type__c, User__c, User__r.lastname, User__r.firstname, User__r.email from VMarket_Reviewers__c where Active__c = true]) {

      if (reviewer.type__c.containsignorecase('regulatory') && reviewer.Countries__c.containsignorecase(countrycode)) {

       if (currentReviewer) {
        if (!reviewer.User__c.equals(countryApp.VMarket_Security_Reviewer__c)) reviewers.add(new SelectOption(reviewer.User__c, reviewer.User__r.firstname + reviewer.User__r.lastname));
       }
       if (reviewer.User__c.equals(UserInfo.getUserId())) {
        if (!currentReviewer) reviewers.add(new SelectOption(reviewer.User__c, reviewer.User__r.firstname + reviewer.User__r.lastname));
        reviewerOk = true;
       }
      }
     }

    }
    for (Attachment a: [select Name, parentid, ContentType, body from Attachment where parentid = : countryApp.Id])
    //for(Attachment a :[select Name, parentid, ContentType,body from Attachment where parentid=:app.id] )
    {
        if(a.name.containsignorecase('510K Form'))
            attached_documents.add(a);
        else 
            nonUS_attached_documents.add(a);
    }
    for (vMarketAppSource__c appsrc: [Select id from vMarketAppSource__c where IsActive__c = : true AND Status__c = : Label.vMarket_Published AND vMarket_App__c = : app.Id]) {
     srcFile = appsrc;
    }

    if (srcFile != null) {
     for (FeedItem ft: [SELECT RelatedRecordId, ParentId FROM FeedItem WHERE ParentId = : srcFile.id]) {
      feedItem = ft;
     }
    }

   }
   if (teamName.containsignorecase('trade')) {
    if (string.isNotBlank(app.VMarket_Type_of_Encr__c)) {
        typeofencr=app.VMarket_Type_of_Encr__c.split('#');
    }
    medicalDeviceAccess=true;
    //typeofencr = app.VMarket_Type_of_Encr__c.split('\\*',-1)[0] +
    // '<br></br>'+app.VMarket_Type_of_Encr__c.split('\\*',-1)[1]+'<br></br>'+app.VMarket_Type_of_Encr__c.split('\\*',-1)[2]+'<br></br>'+app.VMarket_Type_of_Encr__c.split('\\*',-1)[3];
    publisheraddressaccess = true;
    developernameaccess = true;
    Nameaccess = true;
    ShortDescriptionaccess = true;
    encryption1access = true;
    encryption2access = true;
    medicalaccess = true;
    
    sensitiveinfoaccess = true;

    if (String.isNOTBLANK(app.VMarket_Security_Laws__c)) securitymeasuresaccess = true;

    if (String.isNOTBLANK(app.VMarket_Comply_Privacy_Laws__c)) euprivacyaccess = true;


    System.debug('CCCOutside' + countryApp.id);
    if (countryApp != null) {
     system.debug('countryApp@@' + countryApp);
     for (Attachment a: [select Name, parentid, ContentType, body from Attachment where parentid = : countryApp.Id])
     //for(Attachment a :[select Name, parentid, ContentType,body from Attachment where parentid=:app.id] )
     {
        if(a.name.containsignorecase('510K Form'))
            attached_documents.add(a);
        else 
            nonUS_attached_documents.add(a);
     }
     system.debug('attached_documents@@' + attached_documents);
     system.debug('countryApp@@2' + countryApp.VMarket_Trade_Review__c + countryApp.VMarket_Approval_Status__c);

     if (countryApp.VMarket_Trade_Review__c.equals('In Progress') && (countryApp.VMarket_Approval_Status__c.equals('Under Review') || countryApp.VMarket_Approval_Status__c.equals('Info Requested'))) statusOk = true;
     if (countryApp.VMarket_Trade_Reviewer_User__c.equals(UserInfo.getUserId())) currentReviewer = true;
     reviewerName = countryApp.VMarket_Trade_Reviewer_User__r.FirstName + countryApp.VMarket_Trade_Reviewer_User__r.LastName;

     for (VMarket_Reviewers__c reviewer: [Select Active__c, Countries__c, Default__c, type__c, User__c, User__r.lastname, User__r.firstname, User__r.email from VMarket_Reviewers__c where Active__c = true]) {

      if (reviewer.type__c.containsignorecase('trade') && reviewer.Countries__c.containsignorecase(countrycode)) {

       if (currentReviewer) {
        if (!reviewer.User__c.equals(countryApp.VMarket_Trade_Reviewer_User__c)) reviewers.add(new SelectOption(reviewer.User__c, reviewer.User__r.firstname + reviewer.User__r.lastname));
       }
       if (reviewer.User__c.equals(UserInfo.getUserId())) {
        if (!currentReviewer) reviewers.add(new SelectOption(reviewer.User__c, reviewer.User__r.firstname + reviewer.User__r.lastname));
        reviewerOk = true;
       }
      }
     }

    }
   }
   for (vMarketAppSource__c appsrc: [Select id from vMarketAppSource__c where IsActive__c = : true AND Status__c = : Label.vMarket_Published AND vMarket_App__c = : app.Id]) {
    srcFile = appsrc;
   }

   if (srcFile != null) {
    for (FeedItem ft: [SELECT RelatedRecordId, ParentId FROM FeedItem WHERE ParentId = : srcFile.id]) {
     feedItem = ft;
    }
   }

  }

 }

 /*
     Description - Action to be taken on approval page
 */
 public void doActionOnApprovalProcess() {
  String approvalComment = ApexPages.CurrentPage().getParameters().get('approvalComment');
  String approvalAction = ApexPages.CurrentPage().getParameters().get('approvalAction');

  if (wItems.size() > 0) {
   Boolean isSuccess;
   for (ProcessInstanceWorkitem item: wItems) {
    // Instantiate the new ProcessWorkitemRequest object and populate it
    Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
    req.setComments(approvalComment);
    req.setAction(approvalAction);
    req.setNextApproverIds(new Id[] {
     UserInfo.getUserId()
    });

    // Use the ID from the newly created item to specify the item to be worked
    req.setWorkitemId(item.Id);

    // Submit the request for approval
    Approval.ProcessResult result = Approval.process(req);

    isSuccess = result.isSuccess();
    if (result.isSuccess()) {
     isPageShow = false;
     ApexPages.addmessage(new Apexpages.Message(ApexPages.Severity.Info, 'Acion submitted successfully'));
    } else {
     ApexPages.addmessage(new Apexpages.Message(ApexPages.Severity.Error, 'Something went wrong, please try again'));
    }
   }
  }
 }


 public Boolean getDoesErrorExist() {
  return ApexPages.hasMessages(ApexPages.Severity.ERROR);
 }

 public Boolean getDoesInfoExist() {
  return ApexPages.hasMessages(ApexPages.Severity.INFO);
 }


 public boolean statusCheck(Id countryAppId, String typ) {
  //return false;
  System.debug('%%%%%' + typ);
  VMarket_Country_App__c countryApp = [Select id, VMarket_Trade_Feedback__c, VMarket_Trade_Review__c, VMarket_Trade_Reviewer_User__c, VMarket_Security_Feedback__c, VMarket_Security_Reviewer_Change_Reason__c, CreatedBy.Name, VMarket_App__r.Publisher_Address__c, VMarket_App__r.VMarket_Personal_Info__c, VMarket_App__r.VMarket_Protected_Health_Info__c, VMarket_App__r.VMarket_Security_Laws__c, VMarket_App__r.VMarket_Comply_Privacy_Laws__c, VMarket_Security_Visible_Fields__c, VMarket_Security_Review__c, VMarket_Security_Reviewer__c, VMarket_Security_Reviewer__r.firstname, VMarket_Security_Reviewer__r.lastname, VMarket_Regulatory_Reviewer__r.FirstName, VMarket_Regulatory_Reviewer__r.LastName, VMarket_Technical_Reviewer__r.FirstName, VMarket_Technical_Reviewer__r.LastName, VMarket_Country_App__c.Name, VMarket_Approval_Admin__r.firstName, VMarket_App__r.Name, VMarket_App__r.Owner.FirstName, VMarket_App__c, Status__c, Country__c, VMarket_Technical_Visible_Fields__c, VMarket_Regulatory_Visible_Fields__c, VMarket_Reviewer_Change_Reason__c, VMarket_Technical_Reviewer_Change_Reason__c, VMarket_Info_Requested_Fields__c, VMarket_Date_of_Info_Requested__c, VMarket_Date_of_Submission__c, VMarket_Date_of_Approve_Reject__c, VMarket_Date_of_Approval_Request__c, VMarket_Technical_Review__c, VMarket_Regulatory_Review__c, VMarket_Technical_Reviewer__c, vMarket_Approval_Admin__c, VMarket_Regulatory_Feedback__c, VMarket_Technical_Feedback__c, VMarket_Regulatory_Reviewer__c, VMarket_Approval_Information_Required__c, VMarket_Approval_Status__c, VMarket_Rejection_Comment__c, VMarket_App__r.App_Category__r.name, VMarket_App__r.Short_Description__c, VMarket_App__r.Long_Description__c, VMarket_App__r.Price__c, VMarket_App__r.Key_features__c, VMarket_App__r.PublisherName__c, VMarket_App__r.PublisherEmail__c, VMarket_App__r.PublisherWebsite__c, VMarket_App__r.PublisherPhone__c, VMarket_App__r.Publisher_Developer_Support__c from VMarket_Country_App__c where VMarket_App__c = : app.id and country__C = : countrycode and status__C = 'Active'][0];
  if (typ.containsignorecase('tech')) {
   if (!countryApp.VMarket_Technical_Review__c.equalsignorecase('In Progress')) return false;
  } else if (typ.containsignorecase('regulatory')) {
   if (!countryApp.VMarket_Regulatory_Review__c.equalsignorecase('In Progress')) return false;
  } else if (typ.containsignorecase('sec')) {
   System.debug('%%%%%' + countryApp.VMarket_Security_Review__c);
   if (!countryApp.VMarket_Security_Review__c.equalsignorecase('In Progress')) return false;
  }
  return true;
 }

 /* Method Added by Abhishek K as Part of Custom App Review Process VMT-21 to Send Email and Update Records as Per Reviewer feedback*/
 public PageReference sendFeedback() {

   if (!statusCheck(countryApp.id, ApexPages.currentPage().getParameters().get('feedbacktype'))) {
    PageReference pageRef = new PageReference('/apex/vMarketTechRegApproval');
    pageRef.getParameters().put('appid', app.id);
    pageRef.getParameters().put('type', teamName);
    pageRef.getParameters().put('con', countryCode);
    pageRef.getParameters().put('statusError', 'true');
    pageRef.setRedirect(true);
    return pageRef;
   }

   List < String > teamEmails = new list < String > ();
   teamEmails.addAll(new vMarket_Approval_Util().getAdminEmails());
   //vMarket_App__c app = [Select id from vMarket_App__c where id=:appId];//new vMarket_App__c();    
   String typ = ApexPages.currentPage().getParameters().get('feedbacktype');
   String feedbackaction = ApexPages.currentPage().getParameters().get('feedbackaction');
   String comment = ApexPages.currentPage().getParameters().get('feedbackcomment');

   comment = String.escapeSingleQuotes(String.valueOf(comment));
   List < VMarket_Approval_Log__c > logsList = new List < VMarket_Approval_Log__c > ();
   VMarket_Approval_Log__c log = new VMarket_Approval_Log__c();
   log.User_Type__c = 'Reviewer';
   log.vMarket_App__c = app.id;
   System.debug('*****' + typ);
   System.debug('*****' + feedbackaction);
   System.debug('*****' + comment);
   if (typ.containsignorecase('tech')) {
    if (String.isNotBlank(comment)) {
     countryApp.VMarket_Technical_Feedback__c = comment;
     log.Information__c = comment;
    }
    teamEmails.addAll(new vMarket_Approval_Util().gettechReviewerEmails());
    countryApp.VMarket_Technical_Review__c = feedbackaction;
    log.status__C = feedbackaction;
    log.Action__c = 'Technical Feedback Received';
   } else if (typ.containsignorecase('regulatory')) {
    teamEmails.addAll(new vMarket_Approval_Util().getregReviewerEmails());
    if (String.isNotBlank(comment)) {
     countryApp.VMarket_Regulatory_Feedback__c = comment;
     log.Information__c = comment;
    }
    countryApp.VMarket_Regulatory_Review__c = feedbackaction;
    log.status__C = feedbackaction;
    log.Action__c = 'Regulatory Feedback Received';
   } 
   else if (typ.containsignorecase('sec')) {
    medicalDeviceAccess=true; 
    teamEmails.addAll(new vMarket_Approval_Util().getregReviewerEmails());
    if (String.isNotBlank(comment)) {
     countryApp.VMarket_Security_Feedback__c = comment;
     log.Information__c = comment;
    }
    countryApp.VMarket_Security_Review__c = feedbackaction;
    log.status__C = feedbackaction;
    log.Action__c = 'Security Feedback Received';
   } else if (typ.containsignorecase('trade')) {
    teamEmails.addAll(new vMarket_Approval_Util().gettradeReviewerEmails());
    if (String.isNotBlank(comment)) {
     countryApp.VMarket_Trade_Feedback__c = comment;
     log.Information__c = comment;
    }
    countryApp.VMarket_Trade_Review__c = feedbackaction;
    log.status__C = feedbackaction;
    log.Action__c = 'Trade Feedback Received';
   }

   logsList.add(log);
   System.debug('***' + app.vMarket_Approval_Admin__r.email);
   User admin = [Select id, email, firstname from User where id = : countryApp.vMarket_Approval_Admin__c][0];
   vMarket_StripeAPIUtil.sendEmailWithAttachment(new List < String > {
    admin.email
   }, 'Varian Market Place : ' + typ + ' ' + vMarket_Approval_Util.Admin_In_Progress_Subject + '--' + app.Name + '--For Country : ' + countryCode, new vMarket_Approval_Util().getAdminInProgressBody(admin.firstname, app.name, String.valueof(app.id), typ, feedbackaction, comment, countryCode), 'MarketPlace Admin');
   logsList.add(vMarket_StripeAPIUtil.createLog('Admin', 'Email Sent', 'In progress', new vMarket_Approval_Util().getAdminInProgressBody(admin.firstname, app.name, app.id, typ, feedbackaction, comment, countryCode), app));

   update countryApp;
   insert logsList;

   return new PageReference('/');
  }
  /* Method Added by Abhishek K as Part of Custom App Review Process VMT-21 to Change(Re Assign or Self Assign) Reviewers*/
 public PageReference reAssignUser() //String comment,User u,String type,vMarket_App__c app
  {
   changeComment = String.escapeSingleQuotes(String.valueOf(changeComment));
   List < String > teamEmails = new list < String > ();
   teamEmails.addAll(new vMarket_Approval_Util().getAdminEmails());
   System.debug('---' + changedReviewer + changeComment);
   //vMarket_App__c app = [Select id from vMarket_App__c where id=:appId];//new vMarket_App__c(); 
   user u = [Select id, email, firstname from user where id = : Id.valueOf(changedReviewer)][0];
   List < VMarket_Approval_Log__c > logsList = new List < VMarket_Approval_Log__c > ();
   VMarket_Approval_Log__c log = new VMarket_Approval_Log__c();
   log.User_Type__c = 'Reviewer';
   log.vMarket_App__c = app.id;
   log.Information__c = '<b>' + u.firstname + '</b>' + changeComment;
   log.Action__c = 'Approver Changed';

   if (teamName.containsignorecase('tech')) {
    teamEmails.addAll(new vMarket_Approval_Util().gettechReviewerEmails());
    countryApp.VMarket_Technical_Reviewer__c = u.id;
    countryApp.VMarket_Technical_Reviewer_Change_Reason__c = changeComment;
   } else if (teamName.containsignorecase('regulatory')) {
    teamEmails.addAll(new vMarket_Approval_Util().getregReviewerEmails());
    countryApp.VMarket_Regulatory_Reviewer__c = u.id;
    countryApp.VMarket_Regulator_Reviewer_Change_Reason__c = changeComment;
   } else if (teamName.containsignorecase('sec')) {
    teamEmails.addAll(new vMarket_Approval_Util().getregReviewerEmails());
    countryApp.VMarket_Security_Reviewer__c = u.id;
    countryApp.VMarket_Security_Reviewer_Change_Reason__c = changeComment;
   } else if (teamName.containsignorecase('trade')) {
    teamEmails.addAll(new vMarket_Approval_Util().gettradeReviewerEmails());
    countryApp.VMarket_Trade_Reviewer_User__c = u.id;
    countryApp.VMarket_Trade_Reviewer_Change_Reason__c = changeComment;
   }
   update countryApp;

   PageReference pageRef = new PageReference('/apex/vMarketTechRegApproval');
   pageRef.getParameters().put('appid', app.id);
   pageRef.getParameters().put('type', teamName);
   pageRef.getParameters().put('con', countryCode);
   pageRef.setRedirect(true);

   logsList.add(log);
   vMarket_StripeAPIUtil.sendEmailWithAttachment(new List < String > {
    u.email
   }, teamEmails, vMarket_Approval_Util.New_Review_Subject, new vMarket_Approval_Util().getAdminNewReviewBody(u.firstname, app.id, app.name, changeComment, pageRef.getUrl(), countryCode), 'MarketPlace Admin');
   logsList.add(vMarket_StripeAPIUtil.createLog('Admin', 'Email Sent', 'In progress', new vMarket_Approval_Util().getAdminNewReviewBody(u.firstname, app.id, app.name, changeComment, Apexpages.currentPage().getUrl(), countryCode), app));
   //Log
   insert logsList;


   return pageRef;
  }

 public List < SelectOption > getListOfCountries() {
  vMarket_Approval_Util util = new vMarket_Approval_Util();
  return util.ListOfCountries();
 }

}
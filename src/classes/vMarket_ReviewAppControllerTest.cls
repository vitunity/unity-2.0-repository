/**
* author        :       Puneet Mishra, 22 Sept 2017
* description   :       test class for vMarket_ReviewAppController
*/
@isTest
public class vMarket_ReviewAppControllerTest {
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app, app2;
    
    static vMarket_Listing__c listing;
    
    static Account account;
    static Contact contact1;
    static Profile admin,portal;
    static User developer1, adminUsr1;
    static List<Id> reviewerIds;
    static {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', true, admin.Id, null, 'Admin1');
        reviewerIds = new List<Id>();
        reviewerIds.add(userInfo.getUserId());
        
        account = vMarketDataUtility_Test.createAccount('test account', true);
        
        contact1 = vMarketDataUtility_Test.contact_data(account, true);
        
        developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact1.Id, 'Developer1');
        
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        app = vMarketDataUtility_Test.createAppTest('Test Application', cate, false, developer1.Id);
        
        app.ApprovalStatus__c = '';
        insert app;
        
         VMarketCountry__c us = new VMarketCountry__c(name='United States', Code__c='US');
        insert us;
        VMarketCountry__c uk = new VMarketCountry__c(name='United Kingdom', Code__c='UK');
        insert uk;
        
        
         vMarketConfiguration config = new vMarketConfiguration ();
         List<VMarket_Country_App__c> countyappsList = new List<VMarket_Country_App__c>();
     System.debug('{{}}'+config.SUBMITTED_STATUS);
// VMarket_Approval_Status__c=config.SUBMITTED_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(), VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.APPROVED_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.REJECTED_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active',VMarket_Trade_Review__c='In Progress', VMarket_Approval_Status__c=config.PUBLISHED_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.INFO_REQUESTED_STATUS));
     
     insert countyappsList;
        //listing = vMarketDataUtility_Test.createListingData(app, true);
        
    }
    
    public static Approval.ProcessResult[] createApprovalRequests(String appId) {
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
        for (Id reviewerId : reviewerIds) {
            // Create an approval request for the account
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('ApprovalUID');
            req1.setObjectId(appId);
            // Submit on behalf of a specific submitter
            req1.setNextApproverIds(new List<Id>{reviewerId});
            // Submit the record to specific process and skip the criteria evaluation
            req1.setProcessDefinitionNameOrId('vMarketTestApproval');
            req1.setSkipEntryCriteria(true);
            requests.add(req1);
        }
        
        // Submit the approval request for the account
        Approval.ProcessResult[] result;// = Approval.process(requests);
        return result;
    }
    
    public static testMethod void vMarket_ReviewAppControllerTestMethod() {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('con', 'US');
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        // expectedAppId = appId; actualAppId = control.appId;
        system.assertEquals(app.Id, control.appId);
        system.assertEquals(true, control.isPageShow);
        Test.stopTest();
    }
    
    // unit test method if appId is null
   /* public static testMethod void checkAppIdExist_AppIdNullTest() {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', null);
        //System.currentPageReference().getParameters().put('type', 'technical');
        system.debug(' == APP == ' + app);
        
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();
        system.assertEquals(false, control.isPageShow);
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('App ID does not exist')) 
                system.assert(true);
        }
        
        Test.stopTest();
    }*/
    
    // unit test method if appId is null, ProcessInstanceItem size is 0
    public static testMethod void checkAppIdExist_AppIdNOTNullTest() {
        Test.startTest();
        vMarketConfiguration config = new vMarketConfiguration ();
      VMarket_Country_App__c vMarketCon =  new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress',VMarket_Security_Reviewer__c =UserInfo.getUserId(), VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS);
        insert vMarketCon;
                                                                      
        List<Attachment> lstAttach =new List<Attachment>();
        Attachment a = new Attachment(parentid=app.id,name='Logo_test',body=Blob.valueOf('abhi'));
        lstAttach.add(a);        
        Attachment a1 = new Attachment(parentid=app.id,name='CCATS_',body=Blob.valueOf('abhi'));
        lstAttach.add(a1);
        Attachment a2 = new Attachment(parentid=app.id,name='TechnicalSpec_',body=Blob.valueOf('abhi'));
        lstAttach.add(a2);
        Attachment a3 = new Attachment(parentid=app.id,name='AppGuide_',body=Blob.valueOf('abhi'));
        lstAttach.add(a3);
        Attachment a4 = new Attachment(parentid=app.id,name='Banner_',body=Blob.valueOf('abhi'));
        lstAttach.add(a4);
        Attachment a5 = new Attachment(parentid=app.id,name='Opinion_',body=Blob.valueOf('abhi'));
        lstAttach.add(a5);
         Attachment a6 = new Attachment(parentid=vMarketCon.id,name='Opinion_',body=Blob.valueOf('abhi'));
        lstAttach.add(a6);
         Attachment a7 = new Attachment(parentid=vMarketCon.id,name='510K Form',body=Blob.valueOf('abhi'));
        lstAttach.add(a7);
        insert lstAttach;
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('con', 'US');
        //System.currentPageReference().getParameters().put('type', 'regulatory');
        system.debug(' == APP == ' + app);
        
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();
        system.assertEquals(false, control.isPageShow);
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('App ID does not exist')) 
                system.assert(true);
        }
        Boolean expectedError = control.getDoesErrorExist();
        Boolean expectedInfo = control.getDoesInfoExist();
        
        system.assertEquals(expectedError, true);
        system.assertEquals(expectedInfo, false);
        Test.stopTest();
    }
    
    // unit test method if appId is null, ProcessInstanceItem size is not 0
    public static testMethod void checkAppIdExist_AppIdNOTNull_WITHPROCESS_Test() {
        Test.startTest();
         
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('con', 'US');
        //System.currentPageReference().getParameters().put('type', 'technical');
        createApprovalRequests(app.Id);
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();

        Test.stopTest();
    }
    
    public static testMethod void doActionOnApprovalProcess_Test() {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('approvalComment', 'ABCD');
        System.currentPageReference().getParameters().put('approvalAction', 'Approve');
        //System.currentPageReference().getParameters().put('type', 'technical');
        System.currentPageReference().getParameters().put('con', 'US');
        createApprovalRequests(app.Id);
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();
        control.doActionOnApprovalProcess();
        
        Boolean expectedError = control.getDoesErrorExist();
        Boolean expectedInfo = control.getDoesInfoExist();
        
        //system.assertEquals(expectedError, false);
        //system.assertEquals(expectedInfo, true);
        
        Test.stopTest();
    }    
    
    
    public static testMethod void doActionOnApprovalProcess_FAILED_Test() {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('approvalComment', 'ABCD');
        System.currentPageReference().getParameters().put('approvalAction', 'Reject');
        System.currentPageReference().getParameters().put('con', 'US');
        //System.currentPageReference().getParameters().put('type', 'technical');
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();
        control.doActionOnApprovalProcess();
        
        Boolean expectedError = control.getDoesErrorExist();
        Boolean expectedInfo = control.getDoesInfoExist();
        
        Test.stopTest();
    }
    
    public static testMethod void checkAppIdTestwithTypeRegulatory() 
    {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('type', 'regulatory');
        System.currentPageReference().getParameters().put('feedbacktype', 'regulatory');
        System.currentPageReference().getParameters().put('feedbackaction', 'No Objection');
        System.currentPageReference().getParameters().put('feedbackcomment', 'regulatory');
        System.currentPageReference().getParameters().put('con', 'US');

        system.debug(' == APP == ' + app);
        app.Regulatory_Review__c = 'In Progress';
        app.ApprovalStatus__c = 'Under Review';
        app.Regulatory_Reviewer__c = UserInfo.getUserId();
        app.vMarket_Approval_Admin__c = UserInfo.getUserId();
        update app;
        
         VMarket_Reviewers__c r1 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Admin', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r2 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Technical reviewer', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r3 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Regulatory reviewer', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r4 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Trade Reviewer', user__C=UserInfo.getUserId());
         List<VMarket_Reviewers__c> reviewerslist = new List<VMarket_Reviewers__c>();
         reviewerslist.add(r1);
         reviewerslist.add(r2);
         reviewerslist.add(r3);
         reviewerslist.add(r4);
         insert reviewerslist;
        
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();
        control.changedReviewer = UserInfo.getUserId();
        control.changeComment='test new comment';
        control.reAssignUser();
        control.sendFeedback();
        Test.stopTest();
        }
        
        public static testMethod void checkAppIdTestwithTypeTechnical() 
        {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('type', 'technical');
        System.currentPageReference().getParameters().put('con', 'US');
          System.currentPageReference().getParameters().put('feedbacktype', 'technical');
        System.currentPageReference().getParameters().put('feedbackaction', 'No Objection');
        System.currentPageReference().getParameters().put('feedbackcomment', 'regulatory');
           
        system.debug(' == APP == ' + app);
        app.Tech_Review__c = 'In Progress';
        app.ApprovalStatus__c = 'Under Review';
        app.Technical_Reviewer__c = UserInfo.getUserId();
        app.vMarket_Approval_Admin__c = UserInfo.getUserId();
        update app;
        
         VMarket_Reviewers__c r1 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Admin', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r2 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Technical reviewer', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r3 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Regulatory reviewer', user__C=UserInfo.getUserId());
         List<VMarket_Reviewers__c> reviewerslist = new List<VMarket_Reviewers__c>();
         reviewerslist.add(r1);
         reviewerslist.add(r2);
         reviewerslist.add(r3);
         insert reviewerslist;
        
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();
        control.changedReviewer = UserInfo.getUserId();
        control.changeComment='test new comment';
        control.reAssignUser();
        control.sendFeedback();
        control.getListOfCountries();
        Test.stopTest();
        }
        
        public static testMethod void checkAppIdTestwithTypeSecurity() 
        {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('type', 'security');
        System.currentPageReference().getParameters().put('con', 'US');
          System.currentPageReference().getParameters().put('feedbacktype', 'security');
        System.currentPageReference().getParameters().put('feedbackaction', 'No Objection');
        System.currentPageReference().getParameters().put('feedbackcomment', 'security');
        system.debug(' == APP == ' + app);
        app.Tech_Review__c = 'In Progress';
        app.ApprovalStatus__c = 'Under Review';
        app.Technical_Reviewer__c = UserInfo.getUserId();
        app.vMarket_Approval_Admin__c = UserInfo.getUserId();
        update app;
        
         VMarket_Reviewers__c r1 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Admin', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r2 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Technical reviewer', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r3 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Regulatory reviewer', user__C=UserInfo.getUserId());
         List<VMarket_Reviewers__c> reviewerslist = new List<VMarket_Reviewers__c>();
         reviewerslist.add(r1);
         reviewerslist.add(r2);
         reviewerslist.add(r3);
         insert reviewerslist;
        
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();
        control.changedReviewer = UserInfo.getUserId();
        control.changeComment='test new comment';
        control.reAssignUser();
        control.sendFeedback();
        control.getListOfCountries();
        Test.stopTest();
        }
        
    	public static testMethod void checkAppIdTestwithTypeTrade() 
        {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('type', 'trade');
        System.currentPageReference().getParameters().put('con', 'US');
          System.currentPageReference().getParameters().put('feedbacktype', 'trade');
        System.currentPageReference().getParameters().put('feedbackaction', 'No Objection');
        System.currentPageReference().getParameters().put('feedbackcomment', 'trade');
        system.debug(' == APP == ' + app);
        app.Tech_Review__c = 'In Progress';
        app.ApprovalStatus__c = 'Under Review';
        app.Technical_Reviewer__c = UserInfo.getUserId();
        app.vMarket_Approval_Admin__c = UserInfo.getUserId();
        app.VMarket_Type_of_Encr__c='Standard (HTTPS, SSL, and other recognized encryption standards as noted above)#iOS or macOS#Other';
        update app;
            
        vMarketConfiguration config = new vMarketConfiguration ();
      	VMarket_Country_App__c vMarketCon =  new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress',VMarket_Security_Reviewer__c =UserInfo.getUserId(), VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS);
        insert vMarketCon;
                                                                      
        List<Attachment> lstAttach =new List<Attachment>();
        Attachment a = new Attachment(parentid=app.id,name='Logo_test',body=Blob.valueOf('abhi'));
        lstAttach.add(a);        
        Attachment a1 = new Attachment(parentid=app.id,name='CCATS_',body=Blob.valueOf('abhi'));
        lstAttach.add(a1);
        Attachment a2 = new Attachment(parentid=app.id,name='TechnicalSpec_',body=Blob.valueOf('abhi'));
        lstAttach.add(a2);
        
         insert lstAttach;
        
         VMarket_Reviewers__c r1 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Admin', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r2 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Technical reviewer', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r3 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Trade reviewer', user__C=UserInfo.getUserId());
         List<VMarket_Reviewers__c> reviewerslist = new List<VMarket_Reviewers__c>();
         reviewerslist.add(r1);
         reviewerslist.add(r2);
         reviewerslist.add(r3);
         insert reviewerslist;
        
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();
        control.changedReviewer = UserInfo.getUserId();
        control.changeComment='test new comment';
        control.reAssignUser();
        control.sendFeedback();
        control.getListOfCountries();
        Test.stopTest();
        }
    
        public static testMethod void testVMarketApprovalLogs() 
        {
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.VMarketApprovalLogs')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('con', 'US');
        VMarket_Approval_Log__c logs = new VMarket_Approval_Log__c(vMarket_App__c=app.id, User_Type__c='Admin',Action__C='Email Sent');
        insert logs;
        
        VMarketApprovalLogsController logsCtrl = new VMarketApprovalLogsController();
        Test.stopTest();
        }
    public static testMethod void testReviewAppControllerNeg(){
      Test.setCurrentPageReference(new PageReference('Page.vMarket_ReviewApp'));
         System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('type', 'Trade');
        System.currentPageReference().getParameters().put('feedbacktype', 'Trade Reviewer');
        System.currentPageReference().getParameters().put('feedbackaction', 'No Objection');
        System.currentPageReference().getParameters().put('feedbackcomment', 'Trade Reviewer');
        System.currentPageReference().getParameters().put('con', 'US');
         //
          // System.currentPageReference().getParameters().put('type', 'trade');
           system.debug(' == APP == ' + app);
        VMarket_Country_App__c  vMraketCountry = new VMarket_Country_App__c();       
        vMraketCountry.country__c = 'US';
        vMraketCountry.VMarket_Trade_Review__c='In Progress';
        vMraketCountry.VMarket_App__c=app.id;
        vMraketCountry.VMarket_Approval_Status__c = 'Under Review';
        vMraketCountry.VMarket_Trade_Reviewer_User__c = UserInfo.getUserId();
        insert vMraketCountry;
        
         VMarket_Reviewers__c r1 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Admin', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r2 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Technical reviewer', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r3 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Regulatory reviewer', user__C=UserInfo.getUserId());
         VMarket_Reviewers__c r4 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Trade Reviewer', user__C=UserInfo.getUserId());
         List<VMarket_Reviewers__c> reviewerslist = new List<VMarket_Reviewers__c>();
         reviewerslist.add(r1);
         reviewerslist.add(r2);
         reviewerslist.add(r3);
         reviewerslist.add(r4);
         insert reviewerslist;
        vMarket_ReviewAppController control = new vMarket_ReviewAppController();
        control.checkAppIdExist();

    }

    
}
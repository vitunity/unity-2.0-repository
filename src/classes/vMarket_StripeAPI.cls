/**
 * Created By : Puneet Mishra
 * Date : 11 Feb 2017
 * Class Handle All Stripe API keys required
 */
global class vMarket_StripeAPI {
	
    global static String ApiKey {
        get{
            Stripe_Settings__c stripeKey = Stripe_Settings__c.getInstance();
            if(stripeKey != null) {
                if(!vMarket_StripeAPI.isTest) {
                    return stripeKey.Stripe_Secret_Test_Key__c;
                } else {
                    return stripeKey.Stripe_Secret_Live_Key__c;
                }
            }
            return null;
        }
    }
    
    // return client Id depends on isTest
    global static String clientKey {
    	get {
    		Stripe_Settings__c stripeClientKey = Stripe_Settings__c.getInstance();
    		if(stripeClientKey != null) {
    			if(!vMarket_StripeAPI.isTest) {
    				return stripeClientKey.Developement_Client_Id__c;
    			} else {
    				return stripeClientKey.Production_Client_Id__c;
    			}
    		} 
    		return null;
    	}
    }
    
    // decides is Environment is Live
    global static Boolean isTest {
        get {
            Stripe_Settings__c stripeKey = Stripe_Settings__c.getInstance();
            if(stripeKey != null) {
                return stripeKey.Is_Live_Environment__c;
            }
            return true;
        }
    }
}
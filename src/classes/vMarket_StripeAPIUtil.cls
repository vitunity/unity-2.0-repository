/**
* Created By : Puneet Mishra
* Date : 11 Feb 2017
* Stripe API Utility Class
*/
 /* Modified by Abhishek K as Part of Subscription VMT-26, Trial Period VMT-23 Globalisation VMT-26 */
public class vMarket_StripeAPIUtil {
    
    public static final String SERVICE_URL = 'https://api.stripe.com/v1/charges';
    public static final String CUSTOMER_URL = 'https://api.stripe.com/v1/customers';
    public static final String PLAN_URL = 'https://api.stripe.com/v1/plans';
    public static final String SUBSCRIPTION_URL = 'https://api.stripe.com/v1/subscriptions';
    public static final String ACCOUNT_URL = 'https://api.stripe.com/v1/accounts';
    public static final String TOKEN_URL = 'https://connect.stripe.com/oauth/token';
    public static final String TRANSFER_URL = 'https://api.stripe.com/v1/transfers';  
    
    public static String mapToRequestBody(Map<String, String> stripeInfo, vMarketOrderItem__c orderItem) {
        String params = '&';
        for(String key : stripeInfo.keySet()) {
            params += EncodingUtil.urlEncode(key, 'UTF-8') + '=' + 
                EncodingUtil.urlEncode(blankValue(stripeInfo.get(key),''), 'UTF-8') + '&';
        }
        system.debug(' === stripeInfo === ' + stripeInfo);
        // stripe fee: 2.9%  + 30¢ per successful card charge
        /* Decimal stripeFee = ( Integer.valueOf(stripeInfo.get('amount')) * 0.029) + 3;  
//
Controller cont = new metaDataController(userInfo.getUserName(), userInfo.getUserId());
//params += 'metadata[Name]='+cont.custName+'&metadata[Mail]='+cont.custMail + '&
params += 'metadata[application_fee]='+stripeInfo.get('application_fee')+'&metadata[description]='+
stripeInfo.get('description')+'&metadata[stripefee]='+stripeFee;
*/
        if(orderItem != null) {
            Decimal stripeFee = ( ( Integer.valueOf(stripeInfo.get('amount')) * 0.029) + 30)/100;
            stripeFee = stripeFee.setScale(2, RoundingMode.HALF_UP);
            system.debug(' =====  stripeFee ====== ' + stripeFee );
            String applicationFee;
            if(stripeInfo.keyset().contains('application_fee')) applicationFee = String.valueOf(decimal.valueof(stripeInfo.get('application_fee'))/100);
            if(applicationFee==null || applicationFee=='') applicationFee = '0';
            //params += 'metadata[application_fee]='+stripeInfo.get('application_fee')+'&metadata[description]='+
            params += 'metadata[application_fee]='+string.valueOf(applicationFee)+'&metadata[description]='+
                stripeInfo.get('description')+'&metadata[stripefee]='+stripeFee;
            params += '&metadata[TOTALPRICE_INCLU_TAX]=' + orderItem.Total__c;
            params += '&metadata[TAX_PRICE]=' + String.valueOf(orderItem.Tax_price__c);
            system.debug(' === parser === ' + orderItem.TaxDetails__c);
            if(String.IsnotBlank(orderItem.TaxDetails__c)){
            vMarket_TaxDetailParser parser = vMarket_TaxDetailParser.parse(orderItem.TaxDetails__c);
            system.debug(' === parser === ' + orderItem.TaxDetails__c);
            for(vMarket_TaxDetailParser.TAX_DATA taxD : parser.TAX_DATA) {
                String taxName = taxD.TAX_NAME.replaceAll('\\-+', '_');
                taxName = taxName.replaceAll('\\s+', '_');
                taxName = taxName.replaceAll('\\.-+', '_');
                params += '&metadata['+ taxName + ']=' + taxD.TAX_RATE;//.TAX_AMOUNT;
                params += '&metadata['+ taxD.TAX_NAME + '_TAX_CODE]=' + taxD.TAX_CODE;
            }
            }
        }
        
        system.debug(' =====  params ====== ' + params);
        return params;
    }
    
    public class metaDataController{
        public String custName;
        public String custMail;
        
        public metaDataController(String cName, String cMail) {
            this.custName = cName;
            this.custMail = cMail;
        }
        
        public integer hashCode() {
            return this.toString().hashCode();
        }
        
        public boolean equals(Object obj) {
            if ( obj == null ) 
                return false;
            
            if (obj instanceof metaDataController) {
                metaDataController p = (metaDataController)obj;
                return (custName.equals(p.custName)) && (custMail.equals(p.custMail));
            }
            return false;
            
        }
        
    }
    
    
    @TestVisible
    private static String blankValue(String s1, String s2) {
        if(s1 == '' || s1 == null)
            return s2;
        return s1;    
    }
    
    public static List<String> populateAdminRecipients()
    {
        List<String> recipients = new List<String>();
        for(User user:[SELECT Email FROM User WHERE Profile.Name = :Label.vMarket_Admin_Profile])
        {
            recipients.add(user.email);
        }
        //recipients.add('anand.deep@varian.com');//only for test, remove for production
        recipients.addAll(Label.vMarket_Admin_Emails.split(','));
        return recipients;
    }
    
    public static Messaging.SingleEmailMessage prepareEmailWithAttachment(List<String> toAddresses,List<String> CCAddresses,String subject,String mailBody,String senderName)  
    {  
        toAddresses.addAll(populateAdminRecipients());    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        mail.setToAddresses(toAddresses);  
        mail.setCcAddresses(CCAddresses);
        mail.setSenderDisplayName(senderName);  
        mail.setSubject(subject);  
        mail.setHtmlBody(mailBody);  
        return mail;  
    }  
    
    public static void sendEmailWithAttachment(List<String> toAddresses,List<String> CCAddresses,String subject,String mailBody,String senderName)  
    {  
        toAddresses.addAll(populateAdminRecipients());
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        mail.setToAddresses(toAddresses);  
        mail.setCcAddresses(CCAddresses);
        mail.setSenderDisplayName(senderName);  
        mail.setSubject(subject);  
        mail.setHtmlBody(mailBody);  
        mails.add(mail);  
        Messaging.sendEmail(mails);  
    }  
    
    
    public static void sendEmailWithAttachment(List<String> toAddresses,String subject,String mailBody,String senderName)  
    {  
        toAddresses.addAll(populateAdminRecipients());
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        mail.setToAddresses(toAddresses);  
        mail.setSenderDisplayName(senderName);  
        mail.setSubject(subject);  
        mail.setHtmlBody(mailBody);  
        mails.add(mail);  
        Messaging.sendEmail(mails);  
    }   
    
    public static void sendEmailWithAttachment(List<String> toAddresses,String subject,String mailBody,String senderName,List<Messaging.EmailFileAttachment> attachments)  
    {  
        toAddresses.addAll(populateAdminRecipients());
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        mail.setToAddresses(toAddresses);  
        mail.setSenderDisplayName(senderName);  
        mail.setSubject(subject);  
        mail.setHtmlBody(mailBody);  
        mails.add(mail);  
        mail.setFileAttachments(attachments);  
        Messaging.sendEmail(mails);  
    } 
    
    public static Messaging.EmailFileAttachment prepareCSVAttachment(String fileName,String Body)  
    {  
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();  
        attachment.setBody(Blob.valueOf(Body));  
        attachment.setContentType('text/csv');  
        attachment.setFileName(fileName);  
        attachment.setinline(false);  
        return attachment;  
    }  
    
    
    // Create Log Action, Status, Type, Information, App
    public static VMarket_Approval_Log__c createLog(String userType,String action,String status,String info,vMarket_App__c app)
    {
        VMarket_Approval_Log__c log = new VMarket_Approval_Log__c();
        log.User_Type__c = userType;
        log.Action__c = action;
        log.vMarket_App__c = app.id;
        log.Status__c = status;
        log.Information__c = info;
        return log;
    }
    
    /* Method Added by Abhishek K as Part of Trial Period VMT-23 to Check if Trial is Available for a Particular Country or NOT*/
    public Integer checkTrail(Id appId)
    {
        List<vMarket_Listing__c> listing = [Select id,CreatedDate FROM vMarket_Listing__c WHERE App__r.Id=:appId];
        if(listing!=null && listing.size()>0)
        {
            String apId = String.valueof(appId).substring(0,15);
            
            List<vMarket_ListingDownloads__c> trailListings = [Select id, Trail_Version__c, Downloaded_By__c, InstallDate__c, vMarket_Listing__c FROM vMarket_ListingDownloads__c WHERE  Trail_Version__c=true and AppId__c =:apId and vMarket_Listing__c=:listing[0].id and Downloaded_By__c=:UserInfo.getUserId()];//and AppId__c =:apId 
            
            System.debug('**down'+apId+trailListings);
            
            if(trailListings.size()==0) return -1;
            else
            {
                DateTime dt = trailListings[0].InstallDate__c;
                
                return date.newinstance(dT.year(), dT.month(), dT.day()).daysBetween(Date.today());
            }
            
            return -1;
        }
        else return -1;
    }
    /* Method Added by Abhishek K as Part of Globalisation to Check for Allowed Apps on a User's Country*/
    public List<Id> allowedApps(){
        String country = '';
        List<id> allowedApps = new List<id>();
        List<User> usr = [Select ContactId From User Where Id =: UserInfo.getUserId() limit 1];
        if (usr.size() > 0) {
            List<Contact> customerContact = [
                SELECT Id, AccountId,Account.ISO_Country__c, Account.Ext_Cust_Id__c, FirstName, LastName, Email, Phone
                FROM Contact
                WHERE Id =: usr[0].ContactId
                LIMIT 1
            ];
            if(!customerContact.isEmpty()){
                country = customerContact[0].Account.ISO_Country__c;
            }
            
            
            for( VMarket_Country_App__c countryapp : [select id,vmarket_app__C from VMarket_Country_App__c where Country__c=:country and Status__c='Active' and VMarket_Approval_Status__c ='Published' and vmarket_app__r.ApprovalStatus__c = 'Published'])
            {
                allowedApps.add(countryapp.vmarket_app__C);            
            }
            
            
        }
        return allowedApps;
    }
    /* Method Added by Abhishek K as Part of Globalisation to Check If App is Allowed on a User's Country*/
    public boolean isAppAllowed(Id appId)
    {
        String country = '';
        List<User> usr = [Select ContactId From User Where Id =: UserInfo.getUserId() limit 1];
        if (usr.size() > 0) {
            List<Contact> customerContact = [
                SELECT Id, AccountId,Account.ISO_Country__c, Account.Ext_Cust_Id__c, FirstName, LastName, Email, Phone
                FROM Contact
                WHERE Id =: usr[0].ContactId
                LIMIT 1
            ];
            if(!customerContact.isEmpty()){
                country = customerContact[0].Account.ISO_Country__c;
            }
            else return false;
            
        }
        //return true;
        SYstem.debug('((((('+country);
        if(String.isNotBlank(country))
        {
            List<VMarket_Country_App__c> countryapps = [select id from VMarket_Country_App__c where Country__c=:country and Status__c='Active' and VMarket_App__c=:appId and VMarket_Approval_Status__c ='Published'];
            SYstem.debug('((((('+countryapps.size());
            if(countryapps.size()==0) return false;       
            else return true;
            
            
        }
        
        return false;
        //else return false;
        
    }
    
    
    public String getUserCountry()
    {       
        String country = '';
        List<User> usr = [Select ContactId From User Where Id =: UserInfo.getUserId() limit 1];
        if (usr.size() > 0) {
            List<Contact> customerContact = [
                SELECT Id, AccountId,Account.ISO_Country__c, Account.Ext_Cust_Id__c, FirstName, LastName, Email, Phone
                FROM Contact
                WHERE Id =: usr[0].ContactId
                LIMIT 1
            ];
            if(!customerContact.isEmpty()){
                country = customerContact[0].Account.ISO_Country__c;
            }
        }
        return country;
        
    }
    /* Method Added by Abhishek K as Part of Subscription to Create Subscription Plans in SF and Stripe*/
    
    /*@Future(callout=true)    old method
    public static void createsubscriptionplans(String appId,String pricelist)
    {
        Map<String, String> requestBody = new Map<String, String>();
        requestBody.put('currency', 'USD'); 
        requestBody.put('interval', 'month');
        
        List<String> intervals = new List<String>{'1','3','6','12'};    
            List<String> types = new List<String>{'month','quarter','halfyear','yearly'}; 
                List<String> subprices = pricelist.split(';',-1);
        List<VMarket_Stripe_Subscription_Plan__c> vmarketPlans = new List<VMarket_Stripe_Subscription_Plan__c>();
        
        vmarket_app__C app = [Select id from vmarket_app__C where id=:appId][0];        
        
        for(Integer i=0;i<4;i++)
        {                
            String price = '0';
            if(String.isNotBlank(subprices.get(i))) price = String.valueof(Integer.valueOf(Decimal.valueof(subprices.get(i)).setScale(2)*100));
            System.debug('------price'+price);
            requestBody.put('amount',price );        
            requestBody.put('id',appId+types.get(i));
            requestBody.put('name',appId+types.get(i));
            requestBody.put('interval_count', intervals.get(i));
            
            HttpRequest request = vMarket_HttpRequest.createHttpRequest('https://api.stripe.com/v1/plans' , 'POST', requestBody, false, null);
            
            system.debug(' == Request == ' + request);
            System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+request.getBody());
            
            Http con = new Http();
            HttpResponse response = new HttpResponse();
            try {
                response = con.send(request);
            } catch(Exception e){
                
            }
            String responseBody = response.getBody();
            Integer statusCode = response.getStatusCode();
            system.debug(' == Response == ' + response);
            system.debug(' == ResponseBody == ' + ResponseBody);
            system.debug(' == statusCode == ' + statusCode);
            
            if(statusCode == 200) {
                System.debug('**resp'+ResponseBody);    
                //Map<String, String> body = (Map<String,String>) JSON.deserialize(ResponseBody, Map<String,String>.class);
                JSONParser parser = JSON.createParser(ResponseBody);
                String planid = '';
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                        (parser.getText() == 'id')) {
                            parser.nextToken();
                            planid = parser.getText();
                        }
                }
                
                System.debug('---Type'+types.get(i)+types.get(i).length());
                VMarket_Stripe_Subscription_Plan__c plan = new VMarket_Stripe_Subscription_Plan__c(vMarket_App__c =app.id,Is_Active__c=true, VMarket_Duration_Type__c=String.valueof(types.get(i)).trim(), VMarket_Price__c=Decimal.valueof(subprices.get(i)), VMarket_Stripe_Subscription_Plan_Id__c=planid);
                vmarketPlans.add(plan);     
            }
            
        }
        
        
        if(vmarketPlans.size()>0) insert vmarketPlans;
    }*/
    
    @Future(callout=true)    
    public static void createsubscriptionplans(String appId,String pricelist)
    {
        Map<String, String> requestBody = new Map<String, String>();
        requestBody.put('currency', 'USD'); 
        requestBody.put('interval', 'month');
        
        List<String> intervals = new List<String>{'1','3','6','12'};    
            List<String> types = new List<String>{'month','quarter','halfyear','yearly'}; 
                List<String> subprices = pricelist.split(';',-1);
        List<VMarket_Stripe_Subscription_Plan__c> vmarketPlans = new List<VMarket_Stripe_Subscription_Plan__c>();
        
        vmarket_app__C app = [Select id from vmarket_app__C where id=:appId][0];        
        
        for(Integer i=0;i<4;i++)
        {                
            String price = '0';
            if(String.isNotBlank(subprices.get(i))) price = String.valueof(Integer.valueOf(Decimal.valueof(subprices.get(i)).setScale(2)*100));
            System.debug('------price'+price);
            // If condition added by harshal
            if(Decimal.valueof(price)>0){
                requestBody.put('amount',price );        
                requestBody.put('id',appId+types.get(i));
                requestBody.put('name',appId+types.get(i));
                requestBody.put('interval_count', intervals.get(i));
                
                HttpRequest request = vMarket_HttpRequest.createHttpRequest('https://api.stripe.com/v1/plans' , 'POST', requestBody, false, null);
                
                system.debug(' == Request == ' + request);
                System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+request.getBody());
                
                Http con = new Http();
                HttpResponse response = new HttpResponse();
                try {
                    response = con.send(request);
                } catch(Exception e){
                    
                }
                String responseBody = response.getBody();
                Integer statusCode = response.getStatusCode();
                system.debug(' == Response == ' + response);
                system.debug(' == ResponseBody == ' + ResponseBody);
                system.debug(' == statusCode == ' + statusCode);
                
                if(statusCode == 200) {
                    System.debug('**resp'+ResponseBody);    
                    //Map<String, String> body = (Map<String,String>) JSON.deserialize(ResponseBody, Map<String,String>.class);
                    JSONParser parser = JSON.createParser(ResponseBody);
                    String planid = '';
                    while (parser.nextToken() != null) {
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                            (parser.getText() == 'id')) {
                                parser.nextToken();
                                planid = parser.getText();
                            }
                    }
                    
                    System.debug('---Type'+types.get(i)+types.get(i).length());
                    VMarket_Stripe_Subscription_Plan__c plan = new VMarket_Stripe_Subscription_Plan__c(vMarket_App__c =app.id,Is_Active__c=true, VMarket_Duration_Type__c=String.valueof(types.get(i)).trim(), VMarket_Price__c=Decimal.valueof(subprices.get(i)), VMarket_Stripe_Subscription_Plan_Id__c=planid);
                    vmarketPlans.add(plan);     
                }
                
            }            
        }
        
        
        if(vmarketPlans.size()>0) insert vmarketPlans;
    }
    
    
        /* Method Added by Abhishek K as Part of Subscription to Delete Subscription Plans in SF and Stripe*/
    //@Future(callout=true)
    public static boolean deletesubscriptionplans(List<VMarket_Subscriptions__c> subscriptions)//List<String> subscriptionsjsons
    {
        Map<String, String> requestBody = new Map<String, String>();
        
        
        List<VMarket_Subscriptions__c> subscriptionstoupdate = new List<VMarket_Subscriptions__c>();
        
        boolean flag = false;
        
        for(Integer i=0;i<subscriptions.size();i++)
        {                        
            
           
            
            HttpRequest request = vMarket_HttpRequest.createHttpRequest('https://api.stripe.com/v1/subscriptions/'+subscriptions.get(i).VMarket_Stripe_Subscription_Id__c , 'DELETE', requestBody, false, null);
            
            system.debug(' == Request == ' + request);
            System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+request.getBody());
            
            Http con = new Http();
            HttpResponse response = new HttpResponse();
            try {
                response = con.send(request);
            } catch(Exception e){
                
            }
            String responseBody = response.getBody();
            Integer statusCode = response.getStatusCode();
            system.debug(' == Response == ' + response);
            system.debug(' == ResponseBody == ' + ResponseBody);
            system.debug(' == statusCode == ' + statusCode);
            
            if(statusCode == 200) {
                System.debug('**resp'+ResponseBody);    
                
                subscriptions.get(i).Is_Active__c = false;
                subscriptionstoupdate.add(subscriptions.get(i));
                flag = true;
            }
            
        }
        
         List<vMarketOrderItem__c> orderstoUpdate = new List<vMarketOrderItem__c>();
        for(vMarketOrderItem__c order : [Select id,status__C FROM vMarketOrderItem__c where VMarket_Subscriptions__c IN :subscriptionstoupdate])
        {
        order.status__C = 'Completed';
        order.VMarket_Subscription_Cancellation_Date__c = Date.Today();
        orderstoUpdate.add(order);      
        }
        
        if(orderstoUpdate.size()>0) update orderstoUpdate;
        
        if(subscriptionstoupdate.size()>0) update subscriptionstoupdate;
        return flag;
    }
    
        /* Method Added by Abhishek K as Part of Subscription to Create Subscription Order in SF and Stripe*/
    public VMarket_Subscriptions__c createSubscription(VMarket_Stripe_Subscription_Plan__c plan,vMarketStripeDetail__c detail)
    {
        Map<String, String> requestBody = new Map<String, String>();
        
        
        requestBody.put('customer', detail.StripeAccountId__c);        
        requestBody.put('items[0][plan]',plan.VMarket_Stripe_Subscription_Plan_Id__c);                      
        
        
        HttpRequest request = vMarket_HttpRequest.createHttpRequest('https://api.stripe.com/v1/subscriptions' , 'POST', requestBody, false, null);
        
        system.debug(' == Request == ' + request);
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+request.getBody());
        
        Http con = new Http();
        HttpResponse response = new HttpResponse();
        try {
            response = con.send(request);
        } catch(Exception e){
            
        }
        
        String responseBody = response.getBody();
        Integer statusCode = response.getStatusCode();
        system.debug(' == Response == ' + response);
        system.debug(' == ResponseBody == ' + ResponseBody);
        system.debug(' == statusCode == ' + statusCode);
        
        VMarket_Subscriptions__c sub;
        
        if(statusCode == 200) {
            System.debug('**resp'+ResponseBody);    
            
            JSONParser parser = JSON.createParser(ResponseBody);
            String subid = '';
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                    (parser.getText() == 'id')) {
                        
                        parser.nextToken();
                        if(String.isBlank(subid)) subid = parser.getText();
                        System.debug('9999subtext'+subid);
                    }
            }
            
            sub = new VMarket_Subscriptions__c(VMarket_Stripe_Detail__c=detail.id,VMarket_Stripe_Subscription_Plan__c=plan.id,VMarket_Stripe_Subscription_Id__c=subid,VMarket_Start_Date__c=Date.Today(),Is_Active__c=true);
            insert sub;
            
        }
        
        
        
        
        return sub;
    }
    
    public VMarket_Subscriptions__c createSubscription(VMarket_Stripe_Subscription_Plan__c plan,String customerId)
    {
        Map<String, String> requestBody = new Map<String, String>();
        
        
        requestBody.put('customer', customerId);        
        requestBody.put('items[0][plan]',plan.VMarket_Stripe_Subscription_Plan_Id__c);                      
        
        
        HttpRequest request = vMarket_HttpRequest.createHttpRequest('https://api.stripe.com/v1/subscriptions' , 'POST', requestBody, false, null);
        
        system.debug(' == Request == ' + request);
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+request.getBody());
        
        Http con = new Http();
        HttpResponse response = new HttpResponse();
        try {
            response = con.send(request);
        } catch(Exception e){
            
        }
        
        String responseBody = response.getBody();
        Integer statusCode = response.getStatusCode();
        system.debug(' == Response == ' + response);
        system.debug(' == ResponseBody == ' + ResponseBody);
        system.debug(' == statusCode == ' + statusCode);
        
        VMarket_Subscriptions__c sub;
        
        if(statusCode == 200) {
            System.debug('**resp'+ResponseBody);    
            
            JSONParser parser = JSON.createParser(ResponseBody);
            String subid = '';
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                    (parser.getText() == 'id')) {
                        
                        parser.nextToken();
                        if(String.isBlank(subid)) subid = parser.getText();
                        System.debug('9999subtext'+subid);
                    }
            }
            
            sub = new VMarket_Subscriptions__c(VMarket_Stripe_Subscription_Plan__c=plan.id,VMarket_Stripe_Subscription_Id__c=subid,VMarket_Start_Date__c=Date.Today(),Is_Active__c=true);
            insert sub;
            
        }
        
        
        
        
        return sub;
    }
        /* Method Added by Abhishek K as Part of Subscription to Create Customer for Subscription in SF and Stripe*/
    public vMarketStripeDetail__c createCustomerStripe(String Token)
    {
        Map<String, String> requestBody = new Map<String, String>();
        
        
        requestBody.put('source', token);                
        
        
        HttpRequest request = vMarket_HttpRequest.createHttpRequest('https://api.stripe.com/v1/customers' , 'POST', requestBody, false, null);
        
        system.debug(' == Request == ' + request);
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+request.getBody());
        
        Http con = new Http();
        HttpResponse response = new HttpResponse();
        try {
            response = con.send(request);
        } catch(Exception e){
            
        }
        
        String responseBody = response.getBody();
        Integer statusCode = response.getStatusCode();
        system.debug(' == Response == ' + response);
        system.debug(' == ResponseBody == ' + ResponseBody);
        system.debug(' == statusCode == ' + statusCode);
        
        
        if(statusCode == 200) {
            System.debug('**resp'+ResponseBody);    
            
            JSONParser parser = JSON.createParser(ResponseBody);
            
            
            Map<String, Object> objMap = (Map<String, Object>) JSON.deserializeUntyped(ResponseBody);
            
            List<Object> objlist = (List<Object>)( ((Map<String, Object>)objMap.get('sources')).get('data'));
            System.debug('===='+objlist+(objMap.get('invoice_prefix'))+(objMap.get('data')));
            String custId = String.valueOf(objMap.get('id'));
            String last4 = String.valueof( ((Map<String, Object>)objlist.get(0)).get('last4'));//String.valueOf(dataMap.get('last4'));
            
            
            
            System.debug('**last'+last4);
            vMarketStripeDetail__c stripeDetail = new vMarketStripeDetail__c(StripeAccountId__c=custId, User__c=UserInfo.getUserId(), Details__c=last4);
            insert stripeDetail;
            
            return stripeDetail;
        }
        return null;
        
    }
    
   
          
   
    
    
}
/**
 *  @author     :       Puneet Mishra
 *  @description:       test class for vMarket_StripeAPIUtil
 */
@isTest
public with sharing class vMarket_StripeAPIUtil_Test {
    
    public static final String TEST_SERVICE_URL = 'https://api.stripe.com/v1/charges';
    public static final String TEST_CUSTOMER_URL = 'https://api.stripe.com/v1/customers';
    public static final String TEST_PLAN_URL = 'https://api.stripe.com/v1/plans';
    public static final String TEST_SUBSCRIPTION_URL = 'https://api.stripe.com/v1/subscriptions';
    public static final String TEST_ACCOUNT_URL = 'https://api.stripe.com/v1/accounts';
    public static final String TEST_TOKEN_URL = 'https://connect.stripe.com/oauth/token';
    public static final String TEST_TRANSFER_URL = 'https://api.stripe.com/v1/transfers';
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2, app3;
    static List<vMarket_App__c> appList;
    
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    
    static Profile admin,portal;   
    static User customer1, customer2, developer1, developer2;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3, contact4;
    static List<Contact> lstContactInsert;
    
    static  {
        
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        
        account = vMarketDataUtility_Test.createAccount('test account', true);
        
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
        insert lstContactInsert;
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
        insert lstUserInsert;
    
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
        app1.ApprovalStatus__c = 'Published';
        app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
        app3 = vMarketDataUtility_Test.createAppTest('Test Application 3', cate, false, developer1.Id);
        appList = new List<vMarket_App__c>();
        appList.add(app1);
        appList.add(app2);
        appList.add(app3);
        insert appList;
        
        //metaDataController mt = new metaDataController('Nilesh', 'ng@gmail.com');
        //mt.hashCode();
        vMarket_StripeAPIUtil vsap = new vMarket_StripeAPIUtil();
        vsap.getUserCountry();
        vsap.allowedApps();
        vsap.isAppAllowed(app1.Id);
        vMarket_StripeAPIUtil.createLog('Developer','Test','SUCCESS','Test',app1);
        vMarket_StripeAPIUtil.prepareCSVAttachment('Test','Test');
        
        List<String> toAddresses = new List<String>();
        toAddresses.add('nilesh.gorle@varian.com');
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();  
        vMarket_StripeAPIUtil.sendEmailWithAttachment(toAddresses, 'Test', 'Test', 'Tester', attachments);
        vMarket_StripeAPIUtil.sendEmailWithAttachment(toAddresses,'Test', 'Test', 'Tester') ;
        vMarket_StripeAPIUtil.sendEmailWithAttachment(toAddresses,toAddresses,'Test', 'Test', 'Tester');
        vMarket_StripeAPIUtil.prepareEmailWithAttachment(toAddresses,toAddresses,'Test', 'Test', 'Tester');
        vMarket_Listing__c listing = new vMarket_Listing__c(isActive__c=true, App__c =app1.id);
        insert listing;
        
        new vMarket_StripeAPIUtil().checkTrail(app1.id);
        vMarket_StripeAPIUtil.metaDataController mt = new vMarket_StripeAPIUtil.metaDataController('Nilesh', 'ng@gmail.com');
        mt.hashCode();
        mt.equals(listing);

        listing = vMarketDataUtility_Test.createListingData(app1, true);
        comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
        activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
        
        source = vMarketDataUtility_Test.createAppSource(app1, true);
        feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
        
        orderItemList = new List<vMarketOrderItem__c>();
        system.runAs(Customer1) {
            orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
        }
        system.runAs(Customer2) {
            orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
        }
        orderItemList.add(orderItem1);
        orderItemList.add(orderItem2);
        //insert orderItemList;
        
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
        
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
        
    }
    
    private static testMethod void testURL() {
        Test.StartTest();
            system.assertEquals(vMarket_StripeAPIUtil.SERVICE_URL, TEST_SERVICE_URL);
            system.assertEquals(vMarket_StripeAPIUtil.CUSTOMER_URL, TEST_CUSTOMER_URL);
            system.assertEquals(vMarket_StripeAPIUtil.PLAN_URL, TEST_PLAN_URL);
            system.assertEquals(vMarket_StripeAPIUtil.SUBSCRIPTION_URL, TEST_SUBSCRIPTION_URL);
            system.assertEquals(vMarket_StripeAPIUtil.ACCOUNT_URL, TEST_ACCOUNT_URL);
            system.assertEquals(vMarket_StripeAPIUtil.TOKEN_URL, TEST_TOKEN_URL);
            system.assertEquals(vMarket_StripeAPIUtil.TRANSFER_URL, TEST_TRANSFER_URL);
        Test.StopTest();
    }
    /*
        requestBody.put('code', customerStripeAccId);
        requestBody.put('client_id', clientId);
        requestBody.put('grant_type', 'authorization_code');
        requestBody.put('scope', 'read_write');
    */
    
     public static String tax = '{  '+
                                   '"RETURN":{  '+
                                      '"TYPE":"",'+
                                      '"ID":"",'+
                                      '"NUMBER":"000",'+
                                      '"MESSAGE":"",'+
                                      '"LOG_NO":"",'+
                                      '"LOG_MSG_NO":"000000",'+
                                      '"MESSAGE_V1":"",'+
                                      '"MESSAGE_V2":"",'+
                                      '"MESSAGE_V3":"",'+
                                      '"MESSAGE_V4":"",'+
                                      '"PARAMETER":"",'+
                                      '"ROW":0,'+
                                      '"FIELD":"",'+
                                      '"SYSTEM":""'+
                                   '},'+
                                   '"TAX_DATA":[  '+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-1",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 6.25",'+
                                         '"TAX_AMOUNT":" 268.6875",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-2",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1",'+
                                         '"TAX_AMOUNT":" 42.99",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-4",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1.75",'+
                                         '"TAX_AMOUNT":" 75.2325",'+
                                         '"TAX_JUR_LVL":""'+
                                      '}'+
                                   ']'+
                                '}';
    
    public static testmethod void mapToRequestBody_test() {
        Test.StartTest();
            Map<String, String> reqBody = new Map<String, String>();
            reqBody.put('code', 'xx_yy_zzz');
            reqBody.put('client_id', 'c_adsreyy323234ghdajsd');
            reqBody.put('grant_type', 'read_write');
            reqBody.put('scope', 'read_write');
            reqBody.put('amount', '1000');
            orderItem1.TaxDetails__c = tax;
            update orderItem1;
            
            User stadUsr = createStandardUser();
            
            vMarket_StripeAPIUtil.mapToRequestBody(reqBody, orderItem1);
        Test.StopTest();
    }
    
    public static testMethod void sendEmailWithAttachment_test() {
        Test.StartTest();
            List<String> address = new List<String>{'abc@mail.com', 'bdc@hdf.com'};
            String sub = 'TestSub';
            String mailBody = tax;
            String senderName = 'TESTCLASS';
            
            vMarket_StripeAPIUtil.sendEmailWithAttachment(address, sub, mailBody, senderName);
        Test.Stoptest();
    }
    
    public static User createStandardUser() {
        User usr = new User();
        usr.Email = 'test_Admin@bechtel.com';
        usr.Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN', 'vMarket', 70);
        usr.LastName = 'test' ;
        usr.IsActive = true;
        usr.FirstName = 'Tester';
        usr.Alias = 'teMar' ;
        usr.ProfileId = admin.Id ;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
        
        usr.vMarketTermsOfUse__c = true;
        usr.vMarket_User_Role__c = 'Customer';
        
        insert usr;
        return usr ;
    }
    
    public static testmethod void ApprovalUtilTests() {
        Test.StartTest();
        vMarket_Approval_Util au = new vMarket_Approval_Util();
        au.ListOfCountriesall();
        au.countriesFieldsMap();
        au.getAdminAppsPendingURL(app1.id);
        au.getAdminAppBody('Test','Test',app1.id);
        au.getTechReviewBody('Test','Test','Test','Test');
        au.getregReviewBody('Test','Test','Test','Test');
        au.getsecReviewBody('Test','Test','Test','Test');
        au.getTradeReviewURL(app1.id,'US');
        au.getregReviewerEmails();
        au.gettradeReviewerEmails();
        au.ListOfCountries();
        au.countriesMap();
        au.allowedApps();
        au.getUserCountry();
        au.isAppAllowed(app1.id);
        au.countriesMapCodes();
        au.gettechReviewerEmails();
        au.getAdminEmails();
        au.getAdminAppsURL(app1.id,'test');
        au.getAdminAppCompletedBody('Test','Test',app1.id);
        au.gettradeReviewBody('test',app1.id,'test','US');
        au.getAdminInProgressBody('test','test',app1.id,'Sec','test','test','US');
        au.getAdminNewReviewBody('test',app1.id,'test','test','www.test.com','US');
        au.getAdminEmails();
        au.gettechReviewerEmails();
        Test.Stoptest();
        }
        
        public Static testmethod void createsubscriptionplansTest()
        {
        vMarket_StripeAPIUtil au = new vMarket_StripeAPIUtil();
        vMarket_StripeAPIUtil.createsubscriptionplans(String.valueOf(app1.id),'23;34;56;67');
        au.createCustomerStripe('TestToken');
        
        vMarketStripeDetail__c detail = new vMarketStripeDetail__c(Details__c='testdetail', StripeAccountId__c='testaccountid', User__c=UserInfo.getUserId());
        insert detail;
        
        
        
        VMarket_Stripe_Subscription_Plan__c plan = new VMarket_Stripe_Subscription_Plan__c(Is_Active__c=true, vMarket_App__c=app1.id, VMarket_Duration_Type__c ='month', VMarket_Price__c=900, VMarket_Stripe_Subscription_Plan_Id__c='testplanid');
        insert plan;
        
        au.createSubscription(plan,detail);
        au.createSubscription(plan,'testcustomerid');
        
        
        
        VMarket_Subscriptions__c sub = new VMarket_Subscriptions__c(Is_Active__c=true, VMarket_Duration__c=30, VMarket_Stripe_Detail__c=detail.id, VMarket_Stripe_Subscription_Id__c='testsubscriptionid', VMarket_Stripe_Subscription_Plan__c=plan.id);
        insert sub;
        
        vMarket_StripeAPIUtil.metaDataController md;// = new vMarket_StripeAPIUtil.metaDataController;
        
        
        vMarket_StripeAPIUtil.deletesubscriptionplans(new List<VMarket_Subscriptions__c>{sub});
        }
        
        
    
    
}
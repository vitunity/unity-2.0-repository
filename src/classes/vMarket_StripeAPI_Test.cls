/**
 *  @author    :    Puneet Mishra
 *  @description:    test class for vMarket_StripeAPI
 */
@isTest
public with sharing class vMarket_StripeAPI_Test {
    static Stripe_Settings__c testSetting;
    static Stripe_Settings__c liveSetting;
  
  private static testMethod void vMarket_StripeAPI_test() {
    testSetting = vMarketDataUtility_test.createStripSetting(false);
    testSetting.Is_Live_Environment__c = false;
    insert testSetting;
     
    test.StartTest();
      String apiKey = vMarket_StripeAPI.ApiKey;
      String clientKey = vMarket_StripeAPI.clientKey;
      system.assertEquals(apiKey, testSetting.Stripe_Secret_Test_Key__c);
      system.assertEquals(clientKey, testSetting.Developement_Client_Id__c);
    test.Stoptest();
  }
  
  private static testMethod void vMarket_StripeAPI_LiveKey_test() {
    liveSetting = vMarketDataUtility_test.createStripSetting(false);
    liveSetting.Is_Live_Environment__c = true;
    insert liveSetting;
     
    test.StartTest();
      String apiKey = vMarket_StripeAPI.ApiKey;
      String clientKey = vMarket_StripeAPI.clientKey;
      system.assertEquals(apiKey, liveSetting.Stripe_Secret_Live_Key__c);
      system.assertEquals(clientKey, liveSetting.Production_Client_Id__c);
    test.Stoptest();
  }
  
  private static testMethod void testNull() {
    Stripe_Settings__c setting;// = new Stripe_Settings__c();
    test.StartTest();
      String apiKey = vMarket_StripeAPI.ApiKey;
      String clientKey = vMarket_StripeAPI.clientKey;
      
    test.StopTest();
  }

}
public class vMarket_StripeAccount {
    
    @testVisible private static Map<String, String> requestBody;
    
    public static boolean orderTriggerStarted = false;
    
    public string customerStripeAccId; // Customer Stripe Acc Id
    public string scope {get;set;} // Decide the scope(Read/Read_Write) with Customer Stripe Acc Id
    public Static Map<String, String> requestedAccDetails{get;set;}
    public Static string client_id{
        get{
            Stripe_Settings__c customSetting = Stripe_Settings__c.getInstance();
            return customSetting.Developement_Client_Id__c;
            //return customSetting.Production_Client_Id__c;
        }set;
    }
    public vMarket_StripeAccount() {
        try {
            customerStripeAccId = apexpages.currentpage().getparameters().get('code');
            scope = apexpages.currentpage().getparameters().get('scope');
            system.debug(' Current Page => ' + ApexPages.currentPage());
            //if(customerStripeAccId != null && scope != null)
              //  authorizeConnectAccount(customerStripeAccId);
        }Catch(Exception e) {
            
        }
    }
    
    public static void authorizeStripeAccount() {
        requestBody = new Map<String, String>();
        requestBody.put('managed', 'false');
        requestBody.put('country', 'US');
        requestBody.put('email', '');
        
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.ACCOUNT_URL, 'POST', requestBody, false, null);
        
    }
    @Future(callout=true)
    public static void createIndividualTransfers(List<String> orderTransfers,boolean retry,decimal orderTax)
    {
    createDirectIndividualTransfers(orderTransfers,retry,orderTax);
    }
    
    public static void createDirectIndividualTransfers(List<String> orderTransfers,boolean retry,decimal orderTax)
    {
    List<String> recipients = vMarket_StripeAPIUtil.populateAdminRecipients();
    try{
    List<vMarketOrderItemLine__c> lineItems = new List<vMarketOrderItemLine__c>();
    List<String> failedOrderTransfers = new List<String>();
    List<vMarket_Log__c> logs = new List<vMarket_Log__c>();
    for(String requestString : orderTransfers)
    {
    Map<String, String> req = (Map<String, String>)JSON.deserialize(requestString, Map<String, String>.class);
    Http con= new Http();
    HttpRequest request = new HttpRequest();
    Integer appTax = (Integer)(Integer.valueOf(req.get(Label.vMarket_Amount)) * orderTax)/100;
    req.put('amount', String.valueOf(Integer.valueOf(req.get(Label.vMarket_Amount)) + appTax));
    Id currentOrderItemId = Id.valueOf(req.get(Label.vMarket_Description).split('_')[1]);
    req.remove(Label.vMarket_Description);

        HttpResponse response = new HttpResponse();
        try {
        System.debug('=============================');
        System.debug('===req====' + req);
        System.debug('==vMarket_StripeAPIUtil.TRANSFER_URL=='+ vMarket_StripeAPIUtil.TRANSFER_URL);
        System.debug(String.valueOf(requestString));
          request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.TRANSFER_URL , 'POST', req, false, null);
          response = con.send(request);
        } catch(Exception e){
            
        }
        
        String responseReceived = response.getBody();
        Integer statusCode = response.getStatusCode();
        vMarket_JSONParser parser = vMarket_JSONParser.parse(responseReceived);
        system.debug(' JSONParser => ' + parser.error);
        String transfer_status = parser.status;
        system.debug(' == Transfer Status == ' + transfer_status);
        system.debug(' == Response == ' + response);
        system.debug(' == ResponseBody == ' + responseReceived);
        system.debug(' == statusCode == ' + statusCode);
        //vMarketOrderItemLine__c currentLineItem = new vMarketOrderItemLine__c(id=Id.valueOf(req.get(Label.vMarket_Description).split('_')[1]));
        vMarketOrderItemLine__c currentLineItem = new vMarketOrderItemLine__c(id=currentOrderItemId);
        if(statusCode == 200 && parser.error == null) //&& String.isNotBlank(transfer_status) && transfer_status.equals(Label.vMarket_Paid)) 
        {
        currentLineItem.transfer_status__c = Label.vMarket_Paid;
        currentLineItem.tax__c = appTax;
        logs.add(vMarket_LogController.createTransferLogs(Label.vMarket_Paid,request,responseReceived));
        } 
        else 
        {
            currentLineItem.transfer_status__c = Label.vMarket_Failed;
            failedOrderTransfers.add(requestString);
            logs.add(vMarket_LogController.createTransferLogs(Label.vMarket_Failed,request,responseReceived));
        }
        lineItems.add(currentLineItem);
    }
     
            List<Database.SaveResult> resultsList = database.update(lineItems,false);
            Integer failedRecords = 0;
            
            
            
            String failureEmailBody = 'Hi,<br> The Following Transfers to Developers Stripe Accounts have Failed based on Exceptions <br><br>';

            for (Database.SaveResult sr : resultsList) 
            {
                if (!sr.isSuccess()) 
                {
                failedRecords++;
                logs.add(vMarket_LogController.createTransferErrorLogs('Transfer Line Items Failed',sr.getErrors()[0].getMessage()));
                failureEmailBody += '<br><br>'+URL.getSalesforceBaseUrl().toExternalForm()+sr.getId()+'<br>'+sr.getErrors()[0].getMessage();
                }
            }
            
            database.insert(logs,false);
            
            if(failedRecords > 0)
            {
            failureEmailBody += '<br><br><br>Thanks';
            vMarket_StripeAPIUtil.sendEmailWithAttachment(recipients,'Transfer Line Item records Failed',failureEmailBody,'VMS ADMIN');
            }
                
            
            
            if(failedOrderTransfers.size()>0) 
            {
            String body = 'Hi,<br> The Following Transfers to Developers Stripe Accounts have Failed Please take approproate action <br><br>';
            
            for(string failedOrder : failedOrderTransfers)
            {
            Map<String, String> failReq = (Map<String, String>)JSON.deserialize(failedOrder, Map<String, String>.class);
            body +='<br>'+URL.getSalesforceBaseUrl().toExternalForm()+failReq.get(Label.vMarket_Description).split('_')[1];
            
            }
            
            body += '<br><br>You can also view the List of Failed Transactions at<br><br>'+Label.vMarket_Failure_Transfers_URL +'<br><br>Thanks';
            vMarket_StripeAPIUtil.sendEmailWithAttachment(recipients,'Stripe Transfers Failed',body,'VMS ADMIN');
            }
            
            if(failedOrderTransfers.size()>0 && !retry)  
            {
            //createDirectIndividualTransfers(failedOrderTransfers,true,orderTax);
            }
            
            }
            catch(Exception e)
            {
            vMarket_StripeAPIUtil.sendEmailWithAttachment(recipients,'Stripe Transfers Code Failure',e.getStackTraceString(),'VMS Error');
            vMarket_LogController.createCodeErrorLogs('Code Failed in vMarket_StripeAccount',e.getStackTraceString());
            }

    }
    
    // this method will be called only when code is received [i.e. customeStripeId]
    public static void authorizeConnectAccount() {
    /*    system.debug(' Current Page => ' + apexpages.currentpage().getparameters().get('code'));
        system.debug(' Current Page => ' + apexpages.currentpage().getparameters().get('scope'));
        if( (apexpages.currentpage().getparameters().get('code') == null) &&
                (apexpages.currentpage().getparameters().get('scope') == null) )
            return ;
        
        String customerStripeAccId = apexpages.currentpage().getparameters().get('code');
        
        // created a remote site setting https://varian--sfdev2.cs61.my.salesforce.com/0rp4C000000GyWO
        String clientId = vMarket_StripeAPI.clientKey;
        
        requestBody = new Map<String, String>();
        requestBody.put('code', customerStripeAccId);
        requestBody.put('client_id', clientId);
        requestBody.put('grant_type', 'authorization_code');
        requestBody.put('scope', 'read_write');
        
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.TOKEN_URL, 'POST', requestBody, false, null);
        Map<String, String> requestResponse = vMarket_HttpResponse.sendRequest(request);
        
        if(!requestResponse.isEmpty() && requestResponse.get('Code') == '200') {
            system.debug(' ==== ' + requestResponse);
            vMarket_Developer_Stripe_Info__c info = new vMarket_Developer_Stripe_Info__c();
            info.Name = userInfo.getName();
            info.Stripe_User__c = userInfo.getUserId();
            
            JSONParser parser = JSON.createParser(requestResponse.get('Body'));
            while(parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'stripe_user_id')) {
                    parser.nextToken();
                    info.Stripe_Id__c = parser.getText();
                }
                else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'token_type')) {
                    parser.nextToken();
                    info.Token_Type__c = parser.getText();
                }
                else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                    parser.nextToken();
                    info.Access_Token__c = parser.getText();
                }
                else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'scope')) {
                    parser.nextToken();
                    info.Scope__c = parser.getText();
                }
            }
            
            insert info;
        }
     */   
    }
    
    public static void dummyText() {
        Map<String, String> strMap = new Map<String, String>();
        strMap.put('A','B');
        strMap.put('c','B');
        strMap.put('v','B');
        strMap.put('b','B');
        strMap.put('g','B');
        strMap.put('f','B');
        strMap.put('s','B');
        strMap.put('d','B');
        strMap.put('t','B');
        strMap.put('y','B');
        strMap.put('o','B');
        strMap.put('w','u');
        strMap.put('q','y');
        List<Integer> str = new List<Integer>();
        str.add(1);
        str.add(2);
        str.add(3);
        str.add(4);
        str.add(5);
        str.add(6);
        str.add(7);
        str.add(8);
        str.add(1);
        str.add(2);
        str.add(3);
        str.add(4);
        str.add(5);
        str.add(6);
        str.add(7);
        str.add(8);
    }
    
}
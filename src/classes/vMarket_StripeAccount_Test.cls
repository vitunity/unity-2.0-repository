@isTest // Puneet
public with sharing class vMarket_StripeAccount_Test {
    public static String tax = '{  '+
                                   '"RETURN":{  '+
                                      '"TYPE":"",'+
                                      '"ID":"",'+
                                      '"NUMBER":"000",'+
                                      '"MESSAGE":"",'+
                                      '"LOG_NO":"",'+
                                      '"LOG_MSG_NO":"000000",'+
                                      '"MESSAGE_V1":"",'+
                                      '"MESSAGE_V2":"",'+
                                      '"MESSAGE_V3":"",'+
                                      '"MESSAGE_V4":"",'+
                                      '"PARAMETER":"",'+
                                      '"ROW":0,'+
                                      '"FIELD":"",'+
                                      '"SYSTEM":""'+
                                   '},'+
                                   '"TAX_DATA":[  '+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-1",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 6.25",'+
                                         '"TAX_AMOUNT":" 268.6875",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-2",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1",'+
                                         '"TAX_AMOUNT":" 42.99",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-4",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1.75",'+
                                         '"TAX_AMOUNT":" 75.2325",'+
                                         '"TAX_JUR_LVL":""'+
                                      '}'+
                                   ']'+
                                '}';
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2;
    static List<vMarket_App__c> appList;
   
   static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
  
  static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
  
  static Profile admin,portal;   
     static User customer1, customer2, developer1, ADMIN1;
     static List<User> lstUserInsert;
     static Account account;
  static Contact contact1, contact2, contact3;
     static List<Contact> lstContactInsert;
     
     static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
  
  static {
    admin = vMarketDataUtility_Test.getAdminProfile();
      portal = vMarketDataUtility_Test.getPortalProfile();
      
      account = vMarketDataUtility_Test.createAccount('test account', false);
      account.Ext_Cust_Id__c = '6051213';
      insert account;
      
      
      lstContactInsert = new List<Contact>();
      contact1 = vMarketDataUtility_Test.contact_data(account, false);
      contact1.MailingStreet = '53 Street';
      contact1.MailingCity = 'Palo Alto';
      contact1.MailingCountry = 'USA';
      contact1.MailingPostalCode = '94035';
      contact1.MailingState = 'CA';
      
    contact2 = vMarketDataUtility_Test.contact_data(account, false);
    contact2.MailingStreet = '660 N';
      contact2.MailingCity = 'Milpitas';
      contact2.MailingCountry = 'USA';
      contact2.MailingPostalCode = '94035';
      contact2.MailingState = 'CA';
      
      lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
      
      lstContactInsert.add(contact1);
      lstContactInsert.add(contact2);
      insert lstContactInsert;
      
      lstUserInsert = new List<User>();
      lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
    lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
      lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
      ADMIN1 = new User(Email = 'test_Admin@bechtel.com', Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN', 'vMarket', 70), 
        LastName = 'test', IsActive = true, FirstName = 'Tester', Alias = 'teMar', ProfileId = admin.Id, LanguageLocaleKey = 'en_US', 
        LocaleSidKey = 'en_US', TimeZoneSidKey = 'America/Chicago', EmailEncodingKey = 'UTF-8', vMarketTermsOfUse__c = true, 
        vMarket_User_Role__c = 'Customer');
      lstUserInsert.add(ADMIN1);
      insert lstUserinsert;
      
      cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
      app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
      app1.ApprovalStatus__c = 'Published';
      
      app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
      
      appList = new List<vMarket_App__c>();
      appList.add(app1);
      appList.add(app2);
      
      insert appList;
      
      listing = vMarketDataUtility_Test.createListingData(app1, true);
      comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
      activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
      
      source = vMarketDataUtility_Test.createAppSource(app1, true);
      feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
      
      orderItemList = new List<vMarketOrderItem__c>();
      system.runAs(Customer1) {
        orderItem1 = vMarketDataUtility_Test.createOrderItem(true, false); // Subscribed Order
        orderItem1.TaxDetails__c = '{  '+
                                   '"RETURN":{  '+
                                      '"TYPE":"",'+
                                      '"ID":"",'+
                                      '"NUMBER":"000",'+
                                      '"MESSAGE":"",'+
                                      '"LOG_NO":"",'+
                                      '"LOG_MSG_NO":"000000",'+
                                      '"MESSAGE_V1":"",'+
                                      '"MESSAGE_V2":"",'+
                                      '"MESSAGE_V3":"",'+
                                      '"MESSAGE_V4":"",'+
                                      '"PARAMETER":"",'+
                                      '"ROW":0,'+
                                      '"FIELD":"",'+
                                      '"SYSTEM":""'+
                                   '},'+
                                   '"TAX_DATA":[  '+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-1",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 6.25",'+
                                         '"TAX_AMOUNT":" 268.6875",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-2",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1",'+
                                         '"TAX_AMOUNT":" 42.99",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-4",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1.75",'+
                                         '"TAX_AMOUNT":" 75.2325",'+
                                         '"TAX_JUR_LVL":""'+
                                      '}'+
                                   ']'+
                                '}';
        orderItem1.Tax__c = 2;
        insert orderItem1;
      }
      system.runAs(Customer2) {
        orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
      }
      
      orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
      orderItemLine2 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
        vMarket_Tax__c tax = new vMarket_Tax__c(Name = 'tax',Tax__c =20 );
      insert tax;
      
       system.runas(customer1) {
            cart1 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        system.runas(customer2) {
            cart2 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        cartLineList = new List<vMarketCartItemLine__c>();
        cartLine1 = vMarketDataUtility_Test.createCartItemLines(app1, cart1, false);
        cartLine2 = vMarketDataUtility_Test.createCartItemLines(app2, cart1, false);
        //cartLine3 = vMarketDataUtility_Test.createCartItemLines(app3, cart2, false);
        cartLineList.add(cartLine1);
        cartLineList.add(cartLine2);
        //system.runas(customer1) {
            insert cartLineList;
        //}
  }
    
     public static testmethod void authorizeconnect() {
        Test.StartTest();
          
          /*system.runAs(developer1) {
            vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
              devStripe.isActive__c = true;
              devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
              //System.debug('^^^^'+u.id);
              devStripe.Stripe_User__c = developer1.Id;
              devStripe.Developer_Request_Status__c = 'Pending';
              insert devStripe;
            
            Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
            
            Map<String, String> reqBody = new Map<String, String>();
              reqBody.put('Code', '200');
              reqBody.put('client_id', 'c_adsreyy323234ghdajsd');
              reqBody.put('grant_type', 'read_write');
              reqBody.put('scope', 'read_write');
              reqBody.put('amount', '1000');
              
              Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
            System.currentPageReference().getParameters().put('code', 'ABC123');
            System.currentPageReference().getParameters().put('scope', 'read_write');
            
            vMarket_StripeAccount acc = new vMarket_StripeAccount();
            acc.customerStripeAccId = '200';
            vMarket_StripeAccount.authorizeConnectAccount();
          }*/
        test.StopTest();
      }
    
    public static testMethod void clientId() {
        String ans = vMarket_StripeAccount.client_id;
        Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
        System.currentPageReference().getParameters().put('code', 'ABC123');
        System.currentPageReference().getParameters().put('scope', 'read_write');
        vMarket_StripeAccount control = new vMarket_StripeAccount();
        vMarket_StripeAccount.authorizeStripeAccount();
    }
    
    public static testMethod void createIndividualTransfers() {
        
             Map<String, String> requestBody = new Map<String, String>();
            requestBody.put('currency', 'USD');
            requestBody.put('amount', String.valueOf(100));
            requestBody.put('description', orderItem1.Id+'_'+orderItemLine1.Id);
            requestBody.put('destination','111211112');
            requestBody.put('transfer_group',orderItem1.Id);
            //requestBody.put('application_fee',string.valueOf(application_fee));
            List<String> orderTransfers = new List<String>();
            orderTransfers.add(JSON.serialize(requestBody));
            //system.assertEquals(0, orderTransfers.size());
            //system.assertEquals(new List<String>(), orderTransfers);
            
        Test.StartTest();
            Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
            vMarket_StripeAccount.createDirectIndividualTransfers(orderTransfers, false, 5);
        Test.StopTest();
    }
    
    public static testMethod void dummyTest() {
        Test.StartTest();
            vMarket_StripeAccount.dummyText();
        Test.StopTest();
    }
    
}
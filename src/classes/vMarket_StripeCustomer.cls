/**
 * Created By Puneet Mishra
 * 13 Feb 2017
 * Controller dedicated for creating and retrieving Stripe Customer
 */
public class vMarket_StripeCustomer {
    
    
    
    public static void retrieveCustomer(String customerId) {
        // Abhishek Added Null as last parameter to fix the errors on 29th June
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.CUSTOMER_URL+'/cus_A6qCrTOE25fsDM', 'GET', new Map<String, String>(), false,null);
        
        Http con = new Http();
        
        HttpResponse response = new HttpResponse();
        try {
            response = con.send(request);
            String responseBody = response.getBody();
            Integer statusCode = response.getStatusCode();
            system.debug(' == Response == ' + response);
            system.debug(' == ResponseBody == ' + ResponseBody);
            system.debug(' == statusCode == ' + statusCode);
        } catch(Exception e){
            
        }
    }
    @RemoteAction
    // Customer Id returned should get stored in Salesforce, helpful for retrieving Customer INfo
    public static void createCustomer(String tokenId) {
        
        Map<String, String> requestBody = new Map<String, String>();
        requestBody.put('description', 'TESTING STRIPE DEMO Description');
        requestBody.put('source',tokenId);// => $_POST['stripeToken'],
        requestBody.put('email', 'puneet@testmail.com');// Here we can save the email to customer record in System
        // Abhishek Added Null as last parameter to fix the errors on 29th June
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.CUSTOMER_URL, 'POST', requestBody, false,null);
        Integer chargedAmount = 3000 * 100;
        
        //request.setBody(vMarket_StripeAPIUtil.mapToRequestBody(stripeInfo));
        
        system.debug(' == Request == ' + request);
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+request.getBody());
        
        Http con = new Http();
        HttpResponse response = new HttpResponse();
        try {
            response = con.send(request);
            String responseBody = response.getBody();
            Integer statusCode = response.getStatusCode();
            system.debug(' == Response == ' + response);
            system.debug(' == ResponseBody == ' + ResponseBody);
            system.debug(' == statusCode == ' + statusCode);
        } catch(Exception e){
            
        }
    }
    
     public static void dummyText() {
      Map<String, String> dummyMap = new Map<String, String>();
      Map<String, String> dummyMap2 = new Map<String, String>();
      Integer numbers;
      String testName;
      dummyMap.put('A', 'A');
      dummyMap.put('B', 'B');
      dummyMap.put('C', 'C');
      List<Integer> inte = new List<Integer>();
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
       inte.add(9);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(9);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      
    }
}
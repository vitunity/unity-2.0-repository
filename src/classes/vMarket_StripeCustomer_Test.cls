/**
 *  @author    :    Puneet Mishra
 *  @description:    test class for vMarket_StripeAPI
 */
@isTest
public with sharing class vMarket_StripeCustomer_Test {
    @isTest static void testCallout() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
    }
    
    public static testMethod void dummyText() {
      Test.StartTest();
        vMarket_StripeCustomer.dummyText();
      Test.StopTest();
    }
}
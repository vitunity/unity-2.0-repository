public class vMarket_StripePayment {
     
    private static final String CUSTOMER_URL = 'https://api.stripe.com/v1/customers';
    public static Integer amtToPay{get;set;}
    
    public vMarket_StripePayment() {
        amtToPay = 3000;
    }
    
    //@RemoteAction
    public static Boolean StripeRequest(String tokenId, Decimal total, vMarketOrderItem__c orderItem) {
        
        Integer chargedAmount = Integer.valueOf(total * 100);
        
        vMarket_Tax__c vMarketTax = vMarket_Tax__c.getInstance('tax');
        Integer application_fee = Integer.valueOf((chargedAmount * vMarketTax.Tax__c)/100);
        
        Map<String, String> requestBody = new Map<String, String>();
        requestBody.put('currency', 'USD'); // This can be dynamic based on the Country of User
        requestBody.put('amount', String.valueOf(chargedAmount));
        requestBody.put('description', orderItem.Name + '-' + userInfo.getUserId());
        requestBody.put('source',tokenId);// => $_POST['stripeToken'],
        //requestBody.put('destination','acct_14fvapIDSfmkyahe');
        //requestBody.put('application_fee',string.valueOf(application_fee));
        //requestBody.put('stripe_account','acct_14fvapIDSfmkyahe');//'?stripe_account=acct_14fvapIDSfmkyahe'
        //requestBody.put('transfer_group', orderItem.Name);
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.SERVICE_URL , 'POST', requestBody, false, orderItem);
        
        //request.setBody(vMarket_StripeAPIUtil.mapToRequestBody(stripeInfo));
        
        system.debug(' == Request == ' + request);
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+request.getBody());
        
        Http con = new Http();
        HttpResponse response = new HttpResponse();
        try {
            response = con.send(request);
        } catch(Exception e){
            
        }
        String responseBody = response.getBody();
        Integer statusCode = response.getStatusCode();
        system.debug(' == Response == ' + response);
        system.debug(' == ResponseBody == ' + ResponseBody);
        system.debug(' == statusCode == ' + statusCode);
        //transferAmt(chargedAmount);
        if(statusCode == 200) {
            vMarket_LogController.createLogs('payment', response.getBody(), request, orderItem);
            deleteCartItems();
            return true;
        } else {
            vMarket_LogController.createLogs('payment', response.getBody(), request, orderItem);
            deleteCartItems();
            return false;
        }
    }
    
    public static void deleteCartItems() {
        vMarketCartItem__c cart = new vMarketCartItem__c();
        cart = [SELECT Id, Name, OwnerId FROM vMarketCartItem__c WHERE OwnerId =: userInfo.getUserId() Limit 1];
        delete cart;
        system.debug(' ======================================================================================== **************** ');
    }
    
    public static void transferAmt(Integer chargedAmount) {
        Map<String, String> requestBody = new Map<String, String>();
        requestBody.put('currency', 'USD'); // This can be dynamic based on the Country of User
        requestBody.put('amount', String.valueOf(chargedAmount));
        requestBody.put('description', 'PUNEET PAYING ANAND');
        //requestBody.put('source',tokenId);// => $_POST['stripeToken'],
        requestBody.put('destination','acct_19hiUUJ5C29lyJRj');
        requestBody.put('application_fee','1500');
        //requestBody.put('stripe_account','acct_14fvapIDSfmkyahe');'?stripe_account=acct_14fvapIDSfmkyahe'
        HttpRequest request = vMarket_HttpRequest.createHttpRequest('https://api.stripe.com/v1/transfers' , 'POST', requestBody, false, null);
        
        //request.setBody(vMarket_StripeAPIUtil.mapToRequestBody(stripeInfo));
        
        system.debug(' == Request == ' + request);
        System.debug(System.LoggingLevel.INFO, '\n**** REQUEST BODY:\n'+request.getBody());
        
        Http con = new Http();
        HttpResponse response = new HttpResponse();
        try {
            response = con.send(request);
        } catch(Exception e){
            
        }
        String responseBody = response.getBody();
        Integer statusCode = response.getStatusCode();
        system.debug(' == Response == ' + response);
        system.debug(' == ResponseBody == ' + ResponseBody);
        system.debug(' == statusCode == ' + statusCode);
    }
    
    
    
    
    
    
    /*************************************************************
    When Order consists of App from different Developer
    **************/
    public static List<vMarket_Log__c> logsList;// = new List<vMarket_Log__c>();
    public static Boolean StripeRequest2(String tokenId, Decimal total, vMarketOrderItem__c orderItem) {
        // Initializing Logs List 
        logsList = new List<vMarket_Log__c>();
        
        // Converting $ into cents
        Integer chargedAmount = Integer.valueOf(total * 100);
      
        
        //STRY0051577: Sales Tax calculation changes 
        Decimal Amt_WithoutTax =0; 
        if(orderItem.Tax_price__c != Null && total != Null){
            Amt_WithoutTax = Integer.valueOf(chargedAmount - (orderItem.Tax_price__c*100));
        }
        
        Integer application_fee = 0;

        /*vMarket_Tax__c vMarketTax = vMarket_Tax__c.getInstance('tax');
        application_fee = Integer.valueOf((chargedAmount * vMarketTax.Tax__c)/100);
        
        vMarket_Tax__c vMarketMedicalTax = vMarket_Tax__c.getInstance('medical_tax');
        application_fee = Integer.valueOf((chargedAmount * vMarketMedicalTax.Tax__c)/100);
        */
        
        Map<String, List<vMarketOrderItemLine__c>> OILMap = new Map<String, List<vMarketOrderItemLine__c>>();
        String OILName;
        Boolean subscription = false;
        for(vMarketOrderItemLine__c OIL : [ SELECT Id, Name, IsSubscribed__c, vMarket_App__c, vMarket_App__r.OwnerId, vMarket_Order_Item__c, vMarket_Order_Item__r.Total__c, Price__c, 
                                                    vMarket_App__r.Developer_Stripe_Acc_Id__c,vMarket_App__r.Is_Medical_Device__c, vMarket_Order_Item__r.Name 
                                            FROM vMarketOrderItemLine__c 
                                            WHERE vMarket_Order_Item__c =: orderItem.Id ]) {
            if(!OILMap.containsKey(OIL.vMarket_App__r.Developer_Stripe_Acc_Id__c)) {
                OILMap.put(OIL.vMarket_App__r.Developer_Stripe_Acc_Id__c, new List<vMarketOrderItemLine__c>{OIL});
            } else {
                OILMap.get(OIL.vMarket_App__r.Developer_Stripe_Acc_Id__c).add(OIL);
            }
            OILName += OIL.Name+'-';
            subscription = OIL.IsSubscribed__c;
        
        if(!OIL.vMarket_App__r.Is_Medical_Device__c) {
            vMarket_Tax__c vMarketTax = vMarket_Tax__c.getInstance('tax');
            application_fee = Integer.valueOf((Amt_WithoutTax * vMarketTax.Tax__c)/100);//Replaced chargedAmount with Amt_WithoutTax STRY0051577
        } else if(OIL.vMarket_App__r.Is_Medical_Device__c) {
            vMarket_Tax__c vMarketMedicalTax = vMarket_Tax__c.getInstance('medical_tax');//Replaced chargedAmount with Amt_WithoutTax STRY0051577
            application_fee = Integer.valueOf((Amt_WithoutTax * vMarketMedicalTax.Tax__c)/100);
        }
          
        // Adding Tax into Application fee  
        application_fee += Integer.valueOf(orderItem.Tax_price__c * 100); // Adding tax amount in application fee
            //system.debug(' == vMarket_App__r.Developer_Acct_Id__c == ' + OIL.vMarket_App__r.Developer_Acct_Id__c);
        }
        system.debug(' == OILMap == ' + OILMap.size() + OILMap );
        system.debug(' == ORDERITEM == ' + orderItem);
        
        // Map size == 1 states that 
        if(OILMap.size() == 1){
            singleStripeRequest(tokenId, chargedAmount, application_fee, OILMap, orderItem);
        } else {
            system.debug(' == IN ELSE == ');
            singleStripeRequest(tokenId, chargedAmount, application_fee, OILMap, orderItem);
            //String transferGrp = transferStripeRequest(tokenId, application_fee, OILMap, orderItem);
            for(String str : OILMap.keySet()) {
        //      logsList.add(transferSplitCharges(transferGrp, str, OILMap.get(str), orderItem));
            }
        }
        
        // deleting Cart Items
        if(!subscription) deleteCartItems();
        // inserting logs
        //insert logsList;
        // updating OrderItem
        //update orderItem;
        return null;
    }
    
    public static Boolean singleStripeRequest(String tokenId, Integer chargedAmount, Integer appFee, Map<String, List<vMarketOrderItemLine__c>> OILMap, vMarketOrderItem__c orderItem) {
        String description = '';
        String destinationId ='';
        for(String ids : OILMap.keyset()) {
            for(vMarketOrderItemLine__c OIL : OILMap.get(ids)) {
                description += OIL.Name+'-';
                if(!destinationId.contains(OIL.vMarket_App__r.Developer_Stripe_Acc_Id__c))
                    destinationId = OIL.vMarket_App__r.Developer_Stripe_Acc_Id__c;
            }
        }
        Map<String, String> reqBody;
        if(OILMap.keySet().size() > 1)
            reqBody = createRequestBody(chargedAmount, description, null, null, tokenId, null);
        else
            reqBody = createRequestBody(chargedAmount, description, appFee, null, tokenId, destinationId);
        system.debug(' ============ ' + reqBody);
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.SERVICE_URL , 'POST', reqBody, false, orderItem);
        try {
            // Making Request Call and storing response Map
            Map<String, String> responseMap = vMarket_HttpResponse.sendRequest(request);
            system.debug(' ============ response' + responseMap);
            vMarket_LogController.createLogs('payment', responseMap.get('Body'), request, orderItem);
            List<vMarketOrderItemLine__c> OILList = new List<vMarketOrderItemLine__c>();
            if(responseMap.get('Code') == '200' && OILMap.keySet().size() == 1) {
                for( String str : OILMap.keySet() ) {
                    for(vMarketOrderItemLine__c OIL : OILMap.get(str)) {
                        OIL.transfer_status__c = 'paid';
                        OILList.add(OIL);
                    }
                }
            }
            update OILList;
        } catch(Exception e) {
            
        }
        return null;
    }
    
    public static String transferStripeRequest(String tokenId, Integer appFee, Map<String, List<vMarketOrderItemLine__c>> OILMap, vMarketOrderItem__c orderItem) {
        String description = '';
        Decimal chargedAmount = 0;
        String transferGrp ='';
        
        for(String ids : OILMap.keyset()) {
            for(vMarketOrderItemLine__c OIL : OILMap.get(ids) ) {
                description += OIL.Name+'-';
                transferGrp += OIL.Name+'-';
                system.debug(' === ' + OIL.vMarket_Order_Item__r.Total__c);
                if(!test.isRunningTest())
                    chargedAmount += OIL.vMarket_Order_Item__r.Total__c;
                else
                    chargedAmount = 20.0;
            }
        }
         
        Map<String, String> reqBody = createRequestBody(Integer.valueOf(chargedAmount*100), description, null, transferGrp, tokenId, null);
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.SERVICE_URL , 'POST', reqBody, false, orderItem);
        system.debug(' === transferStripeRequest === ' + request);
        try{
            // Making Request Call and storing response Map
            Map<String, String> responseMap = vMarket_HttpResponse.sendRequest(request);
            system.debug(' === transferStripeRequest - responseMap === ' + responseMap);
        } catch (Exception e) {
            
        }
        return transferGrp;
    }
    
    public static vMarket_Log__c transferSplitCharges(String transferGrp, String destinationId, List<vMarketOrderItemLine__c>OILList, vMarketOrderItem__c orderItem) {
        Decimal amt = 0;
        Integer chargedAmt = 0;
        Integer appFee = 0;
        String description = '';
        vMarket_Tax__c vMarketTax = vMarket_Tax__c.getInstance('tax');
        vMarket_Log__c logs = new vMarket_Log__c();
        
        for(vMarketOrderItemLine__c OIL : OILList) {
            amt += OIL.Price__c;//OIL.vMarket_Order_Item__r.Total__c;
            description += OIL.Name+'-';
        }
        chargedAmt = Integer.valueOf(amt * 100);
        appFee = Integer.valueOf((chargedAmt * vMarketTax.Tax__c)/100);
        // Method to create Request Body
        Map<String, String> reqBody = createRequestBody(chargedAmt, description, appFee, transferGrp, null, destinationId);
        system.debug(' === Request Body === ' + reqBody);
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.TRANSFER_URL , 'POST', reqBody, false, orderItem);
        system.debug(' === transferSplitCharges === ' + request);
        try {
            // Request to create Transfer
            Map<String, String> responseMap = vMarket_HttpResponse.sendRequest(request);
            system.debug(' === transferSplitCharges - responseMap === ' + responseMap);
            logs = vMarket_LogController.createLogs('payment', responseMap.get('Body'), request, orderItem);    
        } catch(Exception e) {
            
        }
        return logs;
    }
    
    public static Map<String, String> createRequestBody(Integer chargedAmt, String description, Integer appFee, String transferGrp, String tokenId, String destinationId) {
        Map<String, String> reqBody = new Map<String, String>();
        reqBody.put('currency', 'USD');
        reqBody.put('amount', String.valueOf(chargedAmt));
        reqBody.put('description', description);
        reqBody.put('statement_descriptor', Label.VMarket_Statement_Descriptor);
        
        if(tokenId != null && tokenId != '')
            reqBody.put('source', tokenId);
        if(destinationId != null && destinationId != '')
            reqBody.put('destination', destinationId);
        if(appFee != null)
            reqBody.put('application_fee', string.valueOf(appFee));
        if(transferGrp != null && transferGrp != '')
            reqBody.put('transfer_group', transferGrp);
        
        Map<String, metaDataController> initialMetadata = new Map<String, metaDataController>();
        initialMetadata.put('Info', new metaDataController(userInfo.getName(), UserInfo.getUserEmail()) );
        //initialMetadata.put('order_id', '6735');
        system.debug(' === HashMap === ' + initialMetadata);
        //reqBody.put(string.valueOf(initialMetadata), 'metadata');
        
        return reqBody;
    }
    
    public class metaDataController{
        public String custName;
        public String custMail;
        
        public metaDataController(String cName, String cMail) {
            this.custName = cName;
            this.custMail = cMail;
        }
        
        public integer hashCode() {
            return this.toString().hashCode();
        }
        
        public boolean equals(Object obj) {
            if ( obj == null ) 
                return false;
            
            if (obj instanceof metaDataController) {
                metaDataController p = (metaDataController)obj;
                return (custName.equals(p.custName)) && (custMail.equals(p.custMail));
            }
            return false;
            
        }
        
    }
    
}
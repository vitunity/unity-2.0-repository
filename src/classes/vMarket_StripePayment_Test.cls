/**
 *  @author         :           Puneet Mishra
 *  @description    :       
 */
@isTest
public with sharing class vMarket_StripePayment_Test {
    
    public static String tax = '{  '+
                                   '"RETURN":{  '+
                                      '"TYPE":"",'+
                                      '"ID":"",'+
                                      '"NUMBER":"000",'+
                                      '"MESSAGE":"",'+
                                      '"LOG_NO":"",'+
                                      '"LOG_MSG_NO":"000000",'+
                                      '"MESSAGE_V1":"",'+
                                      '"MESSAGE_V2":"",'+
                                      '"MESSAGE_V3":"",'+
                                      '"MESSAGE_V4":"",'+
                                      '"PARAMETER":"",'+
                                      '"ROW":0,'+
                                      '"FIELD":"",'+
                                      '"SYSTEM":""'+
                                   '},'+
                                   '"TAX_DATA":[  '+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-1",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 6.25",'+
                                         '"TAX_AMOUNT":" 268.6875",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-2",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1",'+
                                         '"TAX_AMOUNT":" 42.99",'+
                                         '"TAX_JUR_LVL":""'+
                                      '},'+
                                      '{  '+
                                         '"INVOICE_ID":"",'+
                                         '"ITEM_NO":"0",'+
                                         '"TAX_TYPE":"TJ",'+
                                         '"TAX_NAME":"Tax Jur. Code Level-4",'+
                                         '"TAX_CODE":"000001710",'+
                                         '"TAX_RATE":" 1.75",'+
                                         '"TAX_AMOUNT":" 75.2325",'+
                                         '"TAX_JUR_LVL":""'+
                                      '}'+
                                   ']'+
                                '}';
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2;
    static List<vMarket_App__c> appList;
   
   static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
  
  static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
  
  static Profile admin,portal;   
     static User customer1, customer2, developer1, ADMIN1;
     static List<User> lstUserInsert;
     static Account account;
  static Contact contact1, contact2, contact3;
     static List<Contact> lstContactInsert;
     
     static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
  
  static {
    admin = vMarketDataUtility_Test.getAdminProfile();
      portal = vMarketDataUtility_Test.getPortalProfile();
      
      account = vMarketDataUtility_Test.createAccount('test account', false);
      account.Ext_Cust_Id__c = '6051213';
      insert account;
      
      
      lstContactInsert = new List<Contact>();
      contact1 = vMarketDataUtility_Test.contact_data(account, false);
      contact1.MailingStreet = '53 Street';
      contact1.MailingCity = 'Palo Alto';
      contact1.MailingCountry = 'USA';
      contact1.MailingPostalCode = '94035';
      contact1.MailingState = 'CA';
      
    contact2 = vMarketDataUtility_Test.contact_data(account, false);
    contact2.MailingStreet = '660 N';
      contact2.MailingCity = 'Milpitas';
      contact2.MailingCountry = 'USA';
      contact2.MailingPostalCode = '94035';
      contact2.MailingState = 'CA';
      
      lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
      
      lstContactInsert.add(contact1);
      lstContactInsert.add(contact2);
      insert lstContactInsert;
      
      lstUserInsert = new List<User>();
      lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
    lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
      lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
      ADMIN1 = new User(Email = 'test_Admin@bechtel.com', Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN', 'vMarket', 70), 
        LastName = 'test', IsActive = true, FirstName = 'Tester', Alias = 'teMar', ProfileId = admin.Id, LanguageLocaleKey = 'en_US', 
        LocaleSidKey = 'en_US', TimeZoneSidKey = 'America/Chicago', EmailEncodingKey = 'UTF-8', vMarketTermsOfUse__c = true, 
        vMarket_User_Role__c = 'Customer');
      lstUserInsert.add(ADMIN1);
      insert lstUserinsert;
      
      cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
      app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
      app1.ApprovalStatus__c = 'Published';
      app1.Developer_Stripe_Acc_Id__c = 'ac_1232434445';
      app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
      
      appList = new List<vMarket_App__c>();
      appList.add(app1);
      appList.add(app2);
      
      insert appList;
      
      listing = vMarketDataUtility_Test.createListingData(app1, true);
      comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
      activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
      
      source = vMarketDataUtility_Test.createAppSource(app1, true);
      feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
      
      orderItemList = new List<vMarketOrderItem__c>();
      system.runAs(Customer1) {
        orderItem1 = vMarketDataUtility_Test.createOrderItem(true, false); // Subscribed Order
        orderItem1.TaxDetails__c = tax;
        orderItem1.Tax__c = 2;
        
        insert orderItem1;
      }
      system.runAs(Customer2) {
        orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
      }
      
      orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
      orderItemLine2 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
        vMarket_Tax__c tax = new vMarket_Tax__c(Name = 'tax',Tax__c =20 );
      insert tax;
      
       system.runas(customer1) {
            cart1 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        system.runas(customer2) {
            cart2 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        cartLineList = new List<vMarketCartItemLine__c>();
        cartLine1 = vMarketDataUtility_Test.createCartItemLines(app1, cart1, false);
        cartLine2 = vMarketDataUtility_Test.createCartItemLines(app2, cart1, false);
        //cartLine3 = vMarketDataUtility_Test.createCartItemLines(app3, cart2, false);
        cartLineList.add(cartLine1);
        cartLineList.add(cartLine2);
        //system.runas(customer1) {
            insert cartLineList;
        //}
  }

    
    public static testMethod void StripeRequest(){
        Test.startTest();
            system.runas(customer1){
                Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
                vMarket_StripePayment.StripeRequest('ct_14fvapIDSfmkyah',  1000.0,orderItem1);
            }
        Test.stopTest();
    }
    
    public static testMethod void StripeRequest2() {
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
           orderItem1 = [Select Id, Tax_price__c, Total__c, TaxDetails__c, 
           
           (SELECT Id, Name, IsSubscribed__c, vMarket_App__c, vMarket_App__r.OwnerId, vMarket_Order_Item__c, vMarket_Order_Item__r.Total__c, Price__c, 
                                                    vMarket_App__r.Developer_Stripe_Acc_Id__c,vMarket_App__r.Is_Medical_Device__c, vMarket_Order_Item__r.Name 
                                            FROM vMarket_Order_Item_Lines__r) FROM vMarketOrderItem__c
                                            WHERE Id =: orderItem1.Id];
            orderItem1.Tax__c = 9;
            vMarket_StripePayment.StripeRequest2('ct_14fvapIDSfmkyah',  1000.0,orderItem1);
            
            
        Test.stopTest();
    }
    
    public static testMethod void deleteCartItem() {
        Test.startTest();
        system.runAs(customer1) {
            vMarket_StripePayment.deleteCartItems();
        }
        Test.stopTest();
    }
    
    public static testMethod void transferAmt() {
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
            vMarket_StripePayment.transferAmt(10000);
        test.stopTest();
    }
    
    public static testMethod void createRequestBody() {
        Test.startTest();
            vMarket_StripePayment.createRequestBody(1000, 'description',300, 'trans1234', 'TOKEN1233', 'acct_19hiUUJ5C29lyJRj' );
        test.stopTest();
    }
    
    public static testMethod void transferStripeRequest() {
        Test.StartTest();
            Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
            List<vMarketOrderItemLine__c> OIL = new List<vMarketOrderItemLine__c>{orderItemLine1};
            Map<String, List<vMarketOrderItemLine__c>> OILMap = new Map<String, List<vMarketOrderItemLine__c>>(); 
            OILMap.put(orderItem1.Id, OIL);
            orderItemLine1 = [SELECt Id, Name, vMarket_Order_Item__r.Total__c, vMarket_Order_Item__c FROM vMarketOrderItemLine__c WHERE Id =: orderItemLine1.Id];
            
             for(vMarketOrderItemLine__c OILs : [ SELECT Id, Name, IsSubscribed__c, vMarket_App__c, vMarket_App__r.OwnerId, vMarket_Order_Item__c, vMarket_Order_Item__r.Total__c, Price__c, 
                                                    vMarket_App__r.Developer_Stripe_Acc_Id__c, vMarket_Order_Item__r.Name, vMarket_Order_Item__r.Tax__c, vMarket_Order_Item__r.SubTotal__c 
                                            FROM vMarketOrderItemLine__c 
                                            WHERE vMarket_Order_Item__c =: orderItem1.Id ]) {
             
             if(!OILMap.containsKey(OILs.vMarket_App__r.Developer_Stripe_Acc_Id__c)) 
                OILMap.put(OILs.vMarket_App__r.Developer_Stripe_Acc_Id__c, new List<vMarketOrderItemLine__c>{OILs});
             else 
                OILMap.get(OILs.vMarket_App__r.Developer_Stripe_Acc_Id__c).add(OILs);
            }
            vMarket_StripePayment.transferStripeRequest('cv_12xgssjh4', 250, OILMap, orderItem1);
        Test.StopTest();
    }
    
    
    public static testMethod void transferSplitCharges() {
        Test.StartTest();
            Test.setMock(HttpCalloutMock.class, new vMarket_HttpResponseMockGenerator());
            vMarket_StripePayment.transferSplitCharges('Tr_GP12sdfc', 'tragsnmskjskk3', new List<vMarketOrderItemLine__c>{orderItemLine1}, orderItem1);
        Test.StopTest();
    }
}
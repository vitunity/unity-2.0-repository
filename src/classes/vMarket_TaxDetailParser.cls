/**
 *  @author    :    Puneet Mishra
 *  @description:    Parser Class for Tax Detail to be passed as metadata information
 */
public with sharing class vMarket_TaxDetailParser {
   
   public static void consumeObject(JSONParser parser) {
    Integer depth = 0;
    do {
      JSONToken curr = parser.getCurrentToken();
      if (curr == JSONToken.START_OBJECT || 
        curr == JSONToken.START_ARRAY) {
        depth++;
      } else if (curr == JSONToken.END_OBJECT ||
        curr == JSONToken.END_ARRAY) {
        depth--;
      }
    } while (depth > 0 && parser.nextToken() != null);
  }
  
  public RETURN_Z RETURN_Z {get;set;} // in json: RETURN
  public List<TAX_DATA> TAX_DATA {get;set;}
  
  public vMarket_TaxDetailParser(JSONParser parser) {
    while (parser.nextToken() != JSONToken.END_OBJECT) {
      if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
        String text = parser.getText();
        if (parser.nextToken() != JSONToken.VALUE_NULL) {
          if (text == 'RETURN') {
            RETURN_Z = new RETURN_Z(parser);
          } else if (text == 'TAX_DATA') {
            TAX_DATA = new List<TAX_DATA>();
            while (parser.nextToken() != JSONToken.END_ARRAY) {
              TAX_DATA.add(new TAX_DATA(parser));
            }
          } else {
            System.debug(LoggingLevel.WARN, 'vMarket_TaxDetailParser consuming unrecognized property: '+text);
            consumeObject(parser);
          }
        }
      }
    }
  }
  
  public class RETURN_Z {
    public String TYPE_Z {get;set;} // in json: TYPE
    public String ID {get;set;} 
    public String NUMBER_Z {get;set;} // in json: NUMBER
    public String MESSAGE {get;set;} 
    public String LOG_NO {get;set;} 
    public String LOG_MSG_NO {get;set;} 
    public String MESSAGE_V1 {get;set;} 
    public String MESSAGE_V2 {get;set;} 
    public String MESSAGE_V3 {get;set;} 
    public String MESSAGE_V4 {get;set;} 
    public String PARAMETER {get;set;} 
    public Integer ROW {get;set;} 
    public String FIELD {get;set;} 
    public String SYSTEM_Z {get;set;} // in json: SYSTEM

    public RETURN_Z(JSONParser parser) {
      while (parser.nextToken() != JSONToken.END_OBJECT) {
        if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
          String text = parser.getText();
          //system.assert(false);
            
          if (parser.nextToken() != JSONToken.VALUE_NULL) {
            if (text == 'TYPE') {
              TYPE_Z = parser.getText();
            } else if (text == 'ID') {
              ID = parser.getText();
            } else if (text == 'NUMBER') {
              NUMBER_Z = parser.getText();
            } else if (text == 'MESSAGE') {
              MESSAGE = parser.getText();
            } else if (text == 'LOG_NO') {
              LOG_NO = parser.getText();
            } else if (text == 'LOG_MSG_NO') {
              LOG_MSG_NO = parser.getText();
            } else if (text == 'MESSAGE_V1') {
              MESSAGE_V1 = parser.getText();
            } else if (text == 'MESSAGE_V2') {
              MESSAGE_V2 = parser.getText();
            } else if (text == 'MESSAGE_V3') {
              MESSAGE_V3 = parser.getText();
            } else if (text == 'MESSAGE_V4') {
              MESSAGE_V4 = parser.getText();
            } else if (text == 'PARAMETER') {
              PARAMETER = parser.getText();
            } else if (text == 'ROW') {
              ROW = parser.getIntegerValue();
            } else if (text == 'FIELD') {
              FIELD = parser.getText();
            } else if (text == 'SYSTEM') {
              SYSTEM_Z = parser.getText();
            } else {
              System.debug(LoggingLevel.WARN, 'RETURN_Z consuming unrecognized property: '+text);
              consumeObject(parser);
            }
          }
        }
      }
    }
  }
  
  public class TAX_DATA {
    public String INVOICE_ID {get;set;} 
    public String ITEM_NO {get;set;} 
    public String TAX_TYPE {get;set;} 
    public String TAX_NAME {get;set;} 
    public String TAX_CODE {get;set;} 
    public String TAX_RATE {get;set;} 
    public String TAX_AMOUNT {get;set;} 
    public String TAX_JUR_LVL {get;set;} 

    public TAX_DATA(JSONParser parser) {
      while (parser.nextToken() != JSONToken.END_OBJECT) {
        if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
          String text = parser.getText();
          if (parser.nextToken() != JSONToken.VALUE_NULL) {
            if (text == 'INVOICE_ID') {
              INVOICE_ID = parser.getText();
            } else if (text == 'ITEM_NO') {
              ITEM_NO = parser.getText();
            } else if (text == 'TAX_TYPE') {
              TAX_TYPE = parser.getText();
            } else if (text == 'TAX_NAME') {
              TAX_NAME = parser.getText();
            } else if (text == 'TAX_CODE') {
              TAX_CODE = parser.getText();
            } else if (text == 'TAX_RATE') {
              TAX_RATE = parser.getText();
            } else if (text == 'TAX_AMOUNT') {
              TAX_AMOUNT = parser.getText();
            } else if (text == 'TAX_JUR_LVL') {
              TAX_JUR_LVL = parser.getText();
            } else {
              System.debug(LoggingLevel.WARN, 'TAX_DATA consuming unrecognized property: '+text);
              consumeObject(parser);
            }
          }
        }
      }
    }
  }
  
  public static vMarket_TaxDetailParser parse(String json) {
    if(json != null)
        return new vMarket_TaxDetailParser(System.JSON.createParser(json));
    else
        return new vMarket_TaxDetailParser(System.JSON.createParser(''));
  }
   
}
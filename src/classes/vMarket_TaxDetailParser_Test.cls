/**
 *	@author				:		puneet mishra
 *	@description		:		test class for vMarket_TaxDetailParser
 */
@IsTest
public class vMarket_TaxDetailParser_Test {
	
	// This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{ '+
		'   \"RETURN\":{  '+
		'      \"TYPE\":\"\",'+
		'      \"ID\":\"\",'+
		'      \"NUMBER\":\"000\",'+
		'      \"MESSAGE\":\"\",'+
		'      \"LOG_NO\":\"\",'+
		'      \"LOG_MSG_NO\":\"000000\",'+
		'      \"MESSAGE_V1\":\"\",'+
		'      \"MESSAGE_V2\":\"\",'+
		'      \"MESSAGE_V3\":\"\",'+
		'      \"MESSAGE_V4\":\"\",'+
		'      \"PARAMETER\":\"\",'+
		'      \"ROW\":0,'+
		'      \"FIELD\":\"\",'+
		'      \"SYSTEM\":\"\"'+
		'   },'+
		'   \"TAX_DATA\":[  '+
		'      {  '+
		'         \"INVOICE_ID\":\"\",'+
		'         \"ITEM_NO\":\"0\",'+
		'         \"TAX_TYPE\":\"TJ\",'+
		'         \"TAX_NAME\":\"Tax Jur. Code Level-1\",'+
		'         \"TAX_CODE\":\"000001710\",'+
		'         \"TAX_RATE\":\" 6.25\",'+
		'         \"TAX_AMOUNT\":\" 268.6875\",'+
		'         \"TAX_JUR_LVL\":\"\"'+
		'      },'+
		'      {  '+
		'         \"INVOICE_ID\":\"\",'+
		'         \"ITEM_NO\":\"0\",'+
		'         \"TAX_TYPE\":\"TJ\",'+
		'         \"TAX_NAME\":\"Tax Jur. Code Level-2\",'+
		'         \"TAX_CODE\":\"000001710\",'+
		'         \"TAX_RATE\":\" 1\",'+
		'         \"TAX_AMOUNT\":\" 42.99\",'+
		'         \"TAX_JUR_LVL\":\"\"'+
		'      },'+
		'      {  '+
		'         \"INVOICE_ID\":\"\",'+
		'         \"ITEM_NO\":\"0\",'+
		'         \"TAX_TYPE\":\"TJ\",'+
		'         \"TAX_NAME\":\"Tax Jur. Code Level-4\",'+
		'         \"TAX_CODE\":\"000001710\",'+
		'         \"TAX_RATE\":\" 1.75\",'+
		'         \"TAX_AMOUNT\":\" 75.2325\",'+
		'         \"TAX_JUR_LVL\":\"\"'+
		'      }'+
		'   ]'+
		'}';
		vMarket_TaxDetailParser r = vMarket_TaxDetailParser.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		vMarket_TaxDetailParser.RETURN_Z objRETURN_Z = new vMarket_TaxDetailParser.RETURN_Z(System.JSON.createParser(json));
		System.assert(objRETURN_Z != null);
		System.assert(objRETURN_Z.TYPE_Z == null);
		System.assert(objRETURN_Z.ID == null);
		System.assert(objRETURN_Z.NUMBER_Z == null);
		System.assert(objRETURN_Z.MESSAGE == null);
		System.assert(objRETURN_Z.LOG_NO == null);
		System.assert(objRETURN_Z.LOG_MSG_NO == null);
		System.assert(objRETURN_Z.MESSAGE_V1 == null);
		System.assert(objRETURN_Z.MESSAGE_V2 == null);
		System.assert(objRETURN_Z.MESSAGE_V3 == null);
		System.assert(objRETURN_Z.MESSAGE_V4 == null);
		System.assert(objRETURN_Z.PARAMETER == null);
		System.assert(objRETURN_Z.ROW == null);
		System.assert(objRETURN_Z.FIELD == null);
		System.assert(objRETURN_Z.SYSTEM_Z == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		vMarket_TaxDetailParser.TAX_DATA objTAX_DATA = new vMarket_TaxDetailParser.TAX_DATA(System.JSON.createParser(json));
		System.assert(objTAX_DATA != null);
		System.assert(objTAX_DATA.INVOICE_ID == null);
		System.assert(objTAX_DATA.ITEM_NO == null);
		System.assert(objTAX_DATA.TAX_TYPE == null);
		System.assert(objTAX_DATA.TAX_NAME == null);
		System.assert(objTAX_DATA.TAX_CODE == null);
		System.assert(objTAX_DATA.TAX_RATE == null);
		System.assert(objTAX_DATA.TAX_AMOUNT == null);
		System.assert(objTAX_DATA.TAX_JUR_LVL == null);

	}
}
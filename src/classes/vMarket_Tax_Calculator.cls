public class vMarket_Tax_Calculator {
    public Double total_tax;
    public static boolean isExecuted;
    
    public String lastName;
    public String firstName;
    public String email;
    public String phone;
    public String erpPartnerNumber;
    public String billingNumber;
    public String street1;
    public String street2;
    public String city;
    public String state;
    public String postalCode;
    public String country;
    public String soldToNumber;
    public String TaxDetails;

    // call the REST service with the address info
    public vMarket_Tax_Calculator()
    {
    isExecuted =false;
    }
    
    public void fetchBillingData() {
        List<User> usr = [Select ContactId From User Where Id =: UserInfo.getUserId() limit 1];
        system.debug('------Usr---------');
        if (usr.size() > 0) {
            system.debug('--- User Id------');
            List<Contact> customerContact = [
                SELECT Id, AccountId, Account.Ext_Cust_Id__c, FirstName, LastName, Email, Phone
                FROM Contact
                WHERE Id =: usr[0].ContactId
                LIMIT 1
            ];
    
            if(!customerContact.isEmpty()){     
                system.debug('customer contact = '+customerContact);
                system.debug('----------Account Number--------------'+customerContact[0].Account.Ext_Cust_Id__c);
                List<ERP_Partner_Association__c> billToParties = [
                    SELECT Id, Sales_Org__c, Partner_Street__c, Partner_Street_line_2__c, 
                    Partner_City__c, Partner_State__c, Partner_Country__c,ERP_Partner_Number__c,
                    Partner_Zipcode_postal_code__c 
                    FROM ERP_Partner_Association__c 
                    WHERE ERP_Customer_Number__c = :customerContact[0].Account.Ext_Cust_Id__c
                    AND Partner_Function__c = 'SH=Ship-to party'
                ];//'BP=Bill-to Party'
                // billToParties IS ShipToParties
                firstName = customerContact[0].FirstName;
                lastName = customerContact[0].LastName;
                email = customerContact[0].Email;
                phone = customerContact[0].Phone;
                billingNumber = customerContact[0].Account.Ext_Cust_Id__c;
                system.debug('----------BillToParties--------------'+billToParties);
                if(!billToParties.isEmpty()){
                    erpPartnerNumber = billToParties[0].ERP_Partner_Number__c;//'0000'+
                    street1 = billToParties[0].Partner_Street__c;
                    street2 = billToParties[0].Partner_Street_line_2__c;
                    city = billToParties[0].Partner_City__c;
                    state = billToParties[0].Partner_State__c;
                    postalCode = billToParties[0].Partner_Zipcode_postal_code__c;
                    postalCode = postalCode.split('-')[0];

                    country = billToParties[0].Partner_Country__c;
                    soldToNumber = CreateQuoteProductPartner.getSoldToNumber(customerContact[0].AccountId);
                }else{
                    system.debug('----------Bill to account not found----------------');
                }
            }else{
                system.debug('----------Contact not found----------------');
            }
        }       
    }

    public Double getTaxRate(String CurrencyCode, Decimal Amount) {
        system.debug('-------getTaxRate--------');
        total_tax = 0;

        fetchBillingData();
        system.debug('--------erpPartnerNumber--------'+erpPartnerNumber);
        system.debug('--------postalCode--------------'+postalCode);
        system.debug('--------country-----------------'+country);
        if (erpPartnerNumber == null || postalCode==null || country==null) {
            return -1;
        }

        HttpRequest req = new HttpRequest();
        Http http = new Http();
        
        // set the request method
        req.setMethod('GET');
        String url = Label.vMarket_SAP_PROXY_URL;
        // add the endpoint to the request
        req.setEndpoint(url);

        //String authorizationHeader = 'BASIC '+Label.vMarket_SAP_Authorization_Token;
        Blob credentialsBlob = Blob.valueOf(Label.vMarket_SAP_UserName +':'+Label.vMarket_SAP_Password);
        String authorizationHeader = 'BASIC '+ EncodingUtil.base64Encode(credentialsBlob);//Label.vMarket_SAP_Authorization_Token;
        //EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('X-SAPWebService-URL', Label.vMarket_SAP_WEB_URL);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');


        system.debug('-----Customer Number -------'+erpPartnerNumber);
        system.debug('-----CurrencyCode -------'+CurrencyCode);
        system.debug('-----Amount -------'+String.valueOf(Amount));
        system.debug('-----postalCode -------'+postalCode);
        system.debug('-----country -------'+country);

        String params = '{'+
                     '"ITEM_DATA":['+
                     '{'+
                     '"CUSTOMER_NO":"'+erpPartnerNumber+'",'+
                     '"CURRENCY":"'+CurrencyCode+'",'+
                     '"GROSS_AMOUNT":"'+String.valueOf(Amount)+'",'+
                     '"POSTAL_CODE":"'+postalCode+'",'+
                     '"COUNTRY":"'+country+'", '+
                     '}'+
                     '],'+
                     //'"PROD_CODE":"A"'+
                     '"PROD_CODE":"L"'+
                     '}';

        req.setbody(params);
        System.debug('----Tax Params ---'+params);
        // create the response object
        HTTPResponse resp = http.send(req);

        if(resp.getstatusCode() == 200 && resp.getbody() != null) {
            TaxDetails = String.valueOf(resp.getBody());
            system.debug('-----parser TaxDetails -------'+resp.getBody());
            if(!TaxDetails.contains('TAX_RATE')) 
            {
            total_tax = -1;
            }
            else
            {
            // Parse JSON response to get all the totalPrice field values.
         //   vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'prince.abhishek16@gmail.com'},'Stripe Response',resp.getBody(),'VMS Error');
            system.debug('-----parser -------'+resp.getBody());
            JSONParser parser = JSON.createParser(resp.getBody());
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                    (parser.getText() == 'TAX_RATE')) {
                    // Get the value.
                    parser.nextToken();
                    // Compute the grand total price for all invoices.
                    total_tax += double.valueOf(parser.getText());
                }
                
            }
            }
        } else {
            system.debug(resp.getstatusCode());
            system.debug(resp.getBody());
            total_tax = -1;
        }
        
        // Added by Abhishek K to BypAss Tax for European Countries
      Set<String> europeanCountries = new Set<String>{'MT','GB','IL','SV','CH','MD','LU','NL'};
      if(europeanCountries.contains(new vMarket_StripeAPIUtil().getUserCountry()) && total_tax==-1) total_tax=0;
        
        
        system.debug('Calculated Total Tax=' + total_tax);
        
        
        return total_tax;
    }
    
    
    public void getTaxRate(String CurrencyCode, Decimal Amount, String ZipCode, String COUNTRY_CODE, vMarketOrderItem__c order,Boolean isInsert) {
        //CurrencyCode = 'USD';
        if(!isExecuted)
        {
        total_tax = 0;

        HttpRequest req = new HttpRequest();
        Http http = new Http();
        
        // set the request method
        req.setMethod('GET');
        String url = Label.vMarket_SAP_PROXY_URL;
        
        // add the endpoint to the request
        req.setEndpoint(url);
        Blob credentialsBlob = Blob.valueOf(Label.vMarket_SAP_UserName +':'+Label.vMarket_SAP_Password);
        String authorizationHeader = 'BASIC '+ EncodingUtil.base64Encode(credentialsBlob);//Label.vMarket_SAP_Authorization_Token;
        //EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('X-SAPWebService-URL', Label.vMarket_SAP_WEB_URL);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        
        fetchBillingData();
        if (erpPartnerNumber == null || postalCode==null || country==null) {
         total_tax = -1;   
        }
        else{

        String params = '{'+
                     '"ITEM_DATA":['+
                     '{'+
                     '"CUSTOMER_NO":"'+erpPartnerNumber+'",'+
                     '"CURRENCY":"'+CurrencyCode+'",'+
                     '"GROSS_AMOUNT":"'+String.valueOf(Amount)+'",'+
                     '"POSTAL_CODE":"'+postalCode+'",'+
                     '"COUNTRY":"'+country+'", '+
                     '}'+
                     '],'+
                     //'"PROD_CODE":"Q"'+
                     '"PROD_CODE":"L"'+
                     '}';
                     
        req.setbody(params);
        System.debug('----Tax Params ---'+params);
        // create the response object
        
        HTTPResponse resp = http.send(req);

        if(resp.getstatusCode() == 200 && resp.getbody() != null) {
            TaxDetails = String.valueOf(resp.getBody());
            if(!TaxDetails.contains('TAX_RATE')) 
            {
            total_tax = -1;
            }
            else
            {
            // Parse JSON response to get all the totalPrice field values.
            JSONParser parser = JSON.createParser(resp.getBody());
            //vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'prince.abhishek16@gmail.com'},'Stripe Response',params+'<br></br>'+resp.getBody(),'VMS Subscription Tax');
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                    (parser.getText() == 'TAX_RATE')) {
                    // Get the value.
                    parser.nextToken();
                    // Compute the grand total price for all invoices.
                    total_tax += double.valueOf(parser.getText());
                    
                }
                
            }
            }
            //vMarketOrderItem__c o = [Select tax__c from vMarketOrderItem__c where id=:orderId][0];
        } else {
            system.debug(resp.getstatusCode());
            system.debug(resp.getBody());
            total_tax = -1;
        }
        system.debug('Calculated Total Tax=' + total_tax);
}
 // Added by Abhishek K to BypAss Tax for European Countries
  Set<String> europeanCountries = new Set<String>{'MT','GB','IL','SV','CH','MD','LU','NL'};
  if(europeanCountries.contains(new vMarket_StripeAPIUtil().getUserCountry()) && total_tax==-1) total_tax=0;
             
            
            order.Tax__c = total_tax;
            order.TaxDetails__c = TaxDetails;
            
            
            
            if(isInsert) insert order;
            else update order;
        //return total_tax;
        isExecuted = true;
        }
    }
    
}
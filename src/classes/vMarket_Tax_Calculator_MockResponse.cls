/**
 *	@author			:			Puneet Mishra
 *	@description	:			mock class for vMarket_Tax_Calculator
 */
@isTest
global with sharing class vMarket_Tax_Calculator_MockResponse implements HttpCalloutMock {
    public String responseBody = '{  '+
								   '"RETURN":{ '+ 
								      '"TYPE":"",'+
								      '"ID":"",'+
								      '"NUMBER":"000",'+
								      '"MESSAGE":"",'+
								      '"LOG_NO":"",'+
								      '"LOG_MSG_NO":"000000",'+
								      '"MESSAGE_V1":"",'+
								      '"MESSAGE_V2":"",'+
								      '"MESSAGE_V3":"",'+
								      '"MESSAGE_V4":"",'+
								      '"PARAMETER":"",'+
								      '"ROW":0,'+
								      '"FIELD":"",'+
								      '"SYSTEM":""'+
								   '},'+
								   '"TAX_DATA":[  '+
								      '{  '+
								         '"INVOICE_ID":"",'+
								         '"ITEM_NO":"0",'+
								         '"TAX_TYPE":"TJ",'+
								         '"TAX_NAME":"Tax_Jur_Code_Level_1",'+
								         '"TAX_CODE":"000001710",'+
								         '"TAX_RATE":"6.25",'+
								         '"TAX_AMOUNT":"31.25",'+
								         '"TAX_JUR_LVL":""'+
								      '},'+
								      '{  '+
								         '"INVOICE_ID":"",'+
								         '"ITEM_NO":"0",'+
								         '"TAX_TYPE":"TJ",'+
								         '"TAX_NAME":"Tax_Jur_Code_Level_2",'+
								         '"TAX_CODE":"000001710",'+
								         '"TAX_RATE":"1",'+
								         '"TAX_AMOUNT":"5",'+
								         '"TAX_JUR_LVL":""'+
								      '},'+
								      '{  '+
								         '"INVOICE_ID":"",'+
								         '"ITEM_NO":"0",'+
								         '"TAX_TYPE":"TJ",'+
								         '"TAX_NAME":"Tax_Jur_Code_Level_4",'+
								         '"TAX_CODE":"000001710",'+
								         '"TAX_RATE":"1.75",'+
								         '"TAX_AMOUNT":"8.75",'+
								         '"TAX_JUR_LVL":""'+
								      '}'+
								   ']'+
								'}';
	// Implementing interface method
	global HttpResponse respond(HTTPRequest req) {
    	// creating a fake response
		Httpresponse res = new Httpresponse();
		res.setBody(responseBody);
		res.setStatusCode(200);
		return res;
	}
}
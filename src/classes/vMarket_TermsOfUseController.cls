/**
 *  @author     Puneet Mishra
 *  @desc       Controller specifically for handle Customer Terms and condition response. Controller will create a Dev Stripe Info rec
 *              with storing terms and conditions response from Customer  
 */ 
public without sharing class vMarket_TermsOfUseController extends vMarketBaseController{
    
    @testvisible private String VMARKETVAL;
    public PageReference pageRef;
    @testVisible private String strUserId;
    public String vMarketStripeURL{
        get {
            String clientId = Label.vMarket_StripeConnect;
            if(vMarket_StripeAPI.clientKey != null) {
                system.debug(' ============= vMarket_StripeAPI.clientKey ================== ' + vMarket_StripeAPI.clientKey);
                system.debug(' ============= clientId ================== ' + clientId);
                clientId = clientId.replace('CLIENT', vMarket_StripeAPI.clientKey);
            }
            return clientId;
        }
    set;}
    /*****STRIPE INFO******/
    @testVisible private static Map<String, String> requestBody;
    public static boolean orderTriggerStarted = false;
    public string customerStripeAccId; // Customer Stripe Acc Id
    public string scope {get;set;} // Decide the scope(Read/Read_Write) with Customer Stripe Acc Id
    public Static Map<String, String> requestedAccDetails{get;set;}
    public Static string client_id{
        get{
            return vMarket_StripeAPI.clientKey;
        }set;
    }
    
    //return true if developer request is in pending state
    public Boolean isDevPending {
        get {
            if(isDevRequestPending()) {
                return true;
            } else {
                return false;
            }
        }
        set;
    }
    
    public Boolean devRequestPending() {
        if(isGuestProfile(userInfo.getProfileId())) {
            return false;
        } else {
            List<vMarket_Developer_Stripe_Info__c> usrList = new List<vMarket_Developer_Stripe_Info__c>();
            usrList = [ SELECT Id, Name, Developer_Request_Status__c, OwnerId, Stripe_User__c, Stripe_User__r.vMarket_User_Role__c, isActive__c, Stripe_Id__c 
                        FROM vMarket_Developer_Stripe_Info__c 
                        WHERE OwnerId =: userInfo.getUserId() AND isActive__c =: true AND Developer_Request_Status__c =: Label.vMarket_Pending LIMIT 1];
            if(!usrList.isEmpty() && usrList[0].isActive__c && !String.isBlank(usrList[0].Stripe_Id__c)) { // user Request Exist and is in Pending State
                return true;
            } else {
                return false; // Customer didn't request for Developer Access.
            }
        }
    }
    
    //return true is customer approved as Developer
    public Boolean isDev{
        get {
            if(isDev() && !isDevRequestPending())
                return true;
            else
                return false;
        }
        set;
    } // show the option if Customer wants to make Developer request
    
    //method will validate if page accessed by user is logged in to Salesforce
    public pageReference validateUser() {
        String profileId = [SELECT Id, Name FROM Profile WHERE Name =: Label.vMarket_MyVarianGuestProfile].Id;
        if(userInfo.getProfileId() == profileId) {
            pageReference ref = new pageReference(Label.vMarket_VF_vMarket_notAuthorized);
            ref.setRedirect(true);
            return ref;
        } else {
            authorizeConnectAccount();
        }
        return null;
    }
    
    public pageReference agreeTerms() {
      User usrRec = new User();
      strUserId = userInfo.getUserId();
      usrRec = [SELECT id, Name, vMarket_User_Role__c, vMarketTermsOfUse__c, ContactId FROM User WHERE Id =: strUserId Limit 1];
      usrRec.vMarketTermsOfUse__c = true;
      update usrRec;
      
      pageRef = new PageReference('/vMarketLogin' );//Page.vMarketLogin;
      pageRef.setredirect(true);        
    return pageRef ;
    }
    
    public pageReference disAgreeTerms() {
      User usrRec = new User();
      strUserId = userInfo.getUserId();
      usrRec = [SELECT id, Name, vMarket_User_Role__c, vMarketTermsOfUse__c, ContactId FROM User WHERE Id =: strUserId Limit 1];
      if(usrRec.vMarketTermsOfUse__c) {
        usrRec.vMarketTermsOfUse__c = false;
        update usrRec;
      }
      pageRef = new PageReference('/vMarketLogout' );//Page.vMarketLogin;
      pageRef.setredirect(true);        
    return pageRef ;
    }
    
    /**
     *  Puneet Mishra
     *  method will handle the logic if user accept terms and condition
     *  @parameters :   none
     *  @return type:   pageReference
     */
    /*public pageReference agreeTerms() {
        List<vMarket_Developer_Stripe_Info__c> devInfo = new List<vMarket_Developer_Stripe_Info__c>();
        devInfo = fetchDevRecords();
        
        // Querying User record to set vMarket role to Developer
        User usrRec = new User();
        usrRec = [SELECT id, Name, vMarket_User_Role__c, ContactId FROM User WHERE Id =: strUserId Limit 1];
        Contact cont = new Contact();
        
        cont = [Select Id, vMarket_UserStatus__c FROM Contact Where Id =: usrRec.ContactId];
        if(cont != null) {
            cont.vMarket_UserStatus__c = Label.vMarket_Pending;
        }
        // developer Info already exist
        if(!devInfo.isEmpty()) {
            // Accepting Terms of Use and populating the relevant info
            if(!devInfo[0].vMarket_Accept_Terms_of_Use__c) {
                devInfo[0].vMarket_Accept_Terms_of_Use__c = true;
                devInfo[0].Developer_Request_Status__c = Label.vMarket_Pending;
                devInfo[0].Contact__c = cont.Id;
            }
            //pageRef = new PageReference('/' + );//Page.vMarket;
        } else { // If Developer Info is not present then creating a new one
            devInfo = new List<vMarket_Developer_Stripe_Info__c>{new vMarket_Developer_Stripe_Info__c(vMarket_Accept_Terms_of_Use__c = true, Stripe_User__c = strUserId,
                                                                                Developer_Request_Status__c = Label.vMarket_Pending, Contact__c = usrRec.ContactId)};
            //pageRef = Page.vMarket_StripeConnect;
        }
        // if rec present then Update else Insert
        upsert devInfo[0];
        
        update cont;
        
        //pageRef.setredirect(true);        
        return null;//pageRef ;
    }*/
    
    /**
     *  @createdBy  :   Puneet Mishra
     *  @desc       :   method handle the logic if user decline the terms and condition, it will redirect Customer back to vmarket home page
     *  @parameter  :   none
     *  @returntype :   PageReference
     */
    /*public pageReference disAgreeTerms() {
        List<vMarket_Developer_Stripe_Info__c> devInfo = new List<vMarket_Developer_Stripe_Info__c>();
        devInfo = fetchDevRecords();
        
        // Querying User record to set vMarket role to Developer
        User usrRec = new User();
        usrRec = [SELECT id, Name, vMarket_User_Role__c, ContactId FROM User WHERE Id =: strUserId Limit 1];
        Contact cont = new Contact();
        
        cont = [Select Id, vMarket_UserStatus__c FROM Contact Where Id =: usrRec.ContactId];
        if(cont != null) {
            cont.vMarket_UserStatus__c = Label.vMarket_Rejected;
        }
        
        if(!devInfo.isEmpty()) {
            devInfo[0].vMarket_Accept_Terms_of_Use__c = false;
            devInfo[0].Developer_Request_Status__c = Label.vMarket_Rejected;
            devInfo[0].Contact__c = cont.Id;
        } else {
            devInfo = new List<vMarket_Developer_Stripe_Info__c>{new vMarket_Developer_Stripe_Info__c(vMarket_Accept_Terms_of_Use__c = false, Stripe_User__c = strUserId,
                                                                                Developer_Request_Status__c = Label.vMarket_Rejected, Contact__c = usrRec.ContactId)};
        }
        
        // if rec present then Update else Insert
        upsert devInfo[0];
        
        update cont;
        
        PageReference pageRef = new PageReference('/' + Label.vMarket_LoginPage);
        pageRef.setredirect(true);        
        return pageRef ;
    }*/
    
    @TestVisible
    private List<vMarket_Developer_Stripe_Info__c> fetchDevRecords() {
        
        List<vMarket_Developer_Stripe_Info__c> devInfo = new List<vMarket_Developer_Stripe_Info__c>();
        strUserId = Apexpages.currentPage().getParameters().get('uId');
        // If url paramtere not exists, assigning Id using UserInfo 
        if(strUserId == null || strUserId == '') {
            strUserId = UserInfo.getUserId();
        }
        devInfo = [ SELECT Id, Name, Access_Token__c, isActive__c, Scope__c, Stripe_Id__c, Stripe_User__c, Token_Type__c, vMarket_Accept_Terms_of_Use__c, Developer_Request_Status__c
                    FROM vMarket_Developer_Stripe_Info__c 
                    WHERE isActive__c =: true AND 
                            Stripe_User__c =: strUserId
                    LIMIT 1];
        
        return devInfo;
    }
    
    /**
     *  Created by  :   Puneet Mishra
     *  method will be called from page action and will run only when code and scope is retrieve
     *  parameter   :   none
     *  return type :   void
     */
    /*
    @testVisible
    private void authorizeConnectAccount1() {
        system.debug(' Current Page => ' + apexpages.currentpage().getparameters().get('code'));
        system.debug(' Current Page => ' + apexpages.currentpage().getparameters().get('scope'));
        if( (apexpages.currentpage().getparameters().get('code') == null) &&
                (apexpages.currentpage().getparameters().get('scope') == null) )
            return ;
        
        String customerStripeAccId = apexpages.currentpage().getparameters().get('code');
        
        // created a remote site setting https://varian--sfdev2.cs61.my.salesforce.com/0rp4C000000GyWO
        String clientId = vMarket_StripeAPI.clientKey;
        
        requestBody = new Map<String, String>();
        requestBody.put('code', customerStripeAccId);
        requestBody.put('client_id', clientId);
        requestBody.put('grant_type', 'authorization_code');
        requestBody.put('scope', 'read_write');
        
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.TOKEN_URL, 'POST', requestBody, false, null);
        Map<String, String> requestResponse = vMarket_HttpResponse.sendRequest(request);
        
        if(!requestResponse.isEmpty() && requestResponse.get('Code') == '200') {
            system.debug(' ==== ' + requestResponse);
            vMarket_Developer_Stripe_Info__c info = new vMarket_Developer_Stripe_Info__c();
            info.Name = userInfo.getName();
            info.Stripe_User__c = userInfo.getUserId();
            
            JSONParser parser = JSON.createParser(requestResponse.get('Body'));
            while(parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'stripe_user_id')) {
                    parser.nextToken();
                    info.Stripe_Id__c = parser.getText();
                }
                else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'token_type')) {
                    parser.nextToken();
                    info.Token_Type__c = parser.getText();
                }
                else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                    parser.nextToken();
                    info.Access_Token__c = parser.getText();
                }
                else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'scope')) {
                    parser.nextToken();
                    info.Scope__c = parser.getText();
                }
            }            
            insert info;
        }
        
    }*/
    //vMarket_ArchievedPrivacyTerms__c
    
    /**
     *  @desc  :  if archieved privacy version available
     */
    public Boolean getPrivacyArchievedVersionAvailable() {
        List<vMarket_ArchievedPrivacyTerms__c> archievedVerList = new List<vMarket_ArchievedPrivacyTerms__c>();
        archievedVerList = vMarket_ArchievedPrivacyTerms__c.getAll().values();
        if(archievedVerList.isEmpty() || archievedVerList.size() == 1)
          return false;
        return true;
    }
    
    /**
     * @desc  :  method return list of archieved privacy
     */ 
    public Map<String, string> getPrivacyArchievedVersions() {
        Map<String, String> optionMap = new Map<String, String>();
        //Map<Id, vMarket_DeveloperArchievedTermsOfUse__c> archievedVerMap = vMarket_DeveloperArchievedTermsOfUse__c.getAll();
        for(vMarket_ArchievedPrivacyTerms__c ver : vMarket_ArchievedPrivacyTerms__c.getAll().values()) {
            if(ver.Active__c == true){
                optionMap.put( 'Current', ver.Archieved_Resource__c );
            } else
                optionMap.put( (ver.Name.trim()), ver.Archieved_Resource__c );
        }
        return optionMap;
    }
    
    /**
     *  @desc  :  if archieved terms of use version available
     */
    public Boolean getTermsOfUseArchievedVersionAvailable() {
        List<vMarket_ArchievedTermsOfUse__c> archievedVerList = new List<vMarket_ArchievedTermsOfUse__c>();
        archievedVerList = vMarket_ArchievedTermsOfUse__c.getAll().values();
        if(archievedVerList.isEmpty()  || archievedVerList.size() == 1)
          return false;
        return true;
    }
    
    /**
     * @desc  :  method return list of archieved Terms of Use
     */ 
    public Map<String, string> getTermsOfUseArchievedVersions() {
        Map<String, String> optionMap = new Map<String, String>();
        //Map<Id, vMarket_DeveloperArchievedTermsOfUse__c> archievedVerMap = vMarket_DeveloperArchievedTermsOfUse__c.getAll();
        for(vMarket_ArchievedTermsOfUse__c ver : vMarket_ArchievedTermsOfUse__c.getAll().values()) {
            if(ver.Active__c == true){
                optionMap.put( 'Current', ver.Archieved_Resource__c );
            } else
                optionMap.put( (ver.Name.trim()), ver.Archieved_Resource__c );
        }
        return optionMap;
    }
    
}
/**
 *  @author    :    Puneet Mishra
 *  @description:    test class for vMarket_TermsOfUseController
 */
@isTest
public with sharing class vMarket_TermsOfUseController_Test {
    
    static Profile admin,portal;   
     static User developer1;
     static List<User> lstUserInsert;
     static Account account;
     static Contact contact1;
    
    static {
      admin = vMarketDataUtility_Test.getAdminProfile();
      portal = vMarketDataUtility_Test.getPortalProfile();
      
      account = vMarketDataUtility_Test.createAccount('test account', true);
      contact1 = vMarketDataUtility_Test.contact_data(account, true);
      developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact1.Id, 'Developer1');
      vMarketDataUtility_Test.createDeveloperStripeInfo();
    }
    
    public static testMethod void vMarketStripeURL_Test() {
      Test.StartTest();
        vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
        String url = control.vMarketStripeURL;
        
        String client = vMarket_TermsOfUseController.client_id;
        
      Test.StopTest();
    }
    
    public static testMethod void isDevPending_test() {
      Test.StartTest();
        vMarket_Developer_Stripe_Info__c stripe = [SELECT Id, Developer_Request_Status__c FROM vMarket_Developer_Stripe_Info__c LIMIT 1];
        stripe.Developer_Request_Status__c = 'Pending';
        update stripe;
      
        vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
        Boolean isDev = control.isDevPending;
        
        Boolean isDev2 = control.isDev;
      Test.StopTest();
    }
    
    public static testMethod void isDevPending_test2() {
      Test.StartTest();
        vMarket_Developer_Stripe_Info__c stripe = [SELECT Id, Developer_Request_Status__c FROM vMarket_Developer_Stripe_Info__c LIMIT 1];
        stripe.Developer_Request_Status__c = 'Approved';
        update stripe;
      
        vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
        Boolean isDev = control.isDevPending;
        
        Boolean isDev2 = control.isDev;
      Test.StopTest();
    }
    
    public static testMethod void devRequestPending_Test() {
      Test.StartTest();
        vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
        control.devRequestPending();
        
        system.runas(developer1) {
          vMarket_TermsOfUseController control1 = new vMarket_TermsOfUseController();
          control1.devRequestPending();
        }
        
      Test.StopTest();
    }
    
    public static testMethod void agreeTerms_test() {
      Test.StartTest();
        system.runas(developer1) {
          vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
          control.agreeTerms();
        }
      Test.StopTest();
    }
    
    public static testMethod void disAgreeTerms_Test() {
      Test.StartTest();
        
        system.runas(developer1) {
          
          vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
          control.disAgreeTerms();
        }
      Test.StopTest();
    }
    
    public static testMethod void fetchDevRecords_test() {
      Test.StartTest();
        system.runas(developer1) {
          
          vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
          devStripe.isActive__c = true;
          devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
          //System.debug('^^^^'+u.id);
          devStripe.Stripe_User__c = developer1.Id;
          insert devStripe;
          
          Test.setCurrentPageReference(new PageReference('Page.vMarket_TermsOfUse')); 
            System.currentPageReference().getParameters().put('uId', developer1.Id);
            
            vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
          control.fetchDevRecords();
        }
      Test.StopTest();
    }
    
    public static testMethod void fetchDevRecords_test2() {
      Test.StartTest();
        system.runas(developer1) {
          
          vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
          devStripe.isActive__c = true;
          devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
          //System.debug('^^^^'+u.id);
          devStripe.Stripe_User__c = developer1.Id;
          insert devStripe;
          
          Test.setCurrentPageReference(new PageReference('Page.vMarket_TermsOfUse')); 
            System.currentPageReference().getParameters().put('uId', '');
            
            vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
          control.fetchDevRecords();
        }
      Test.StopTest();
    }
    
    public static testMethod void getTermsOfUseArchievedVersions_Test() {
      Test.StartTest();
        List<vMarket_ArchievedTermsOfUse__c> archTermsOfUse = vMarketDataUtility_Test.createArchievedData();
        //insert archTermsOfUse;
        
        vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
        control.getTermsOfUseArchievedVersions();
        
      Test.StopTest();
    }

  public static testMethod void getTermsOfUseArchievedVersionAvailable_test() {
    Test.StartTest();
    
    //List<vMarket_ArchievedTermsOfUse__c> archTermsOfUse = vMarketDataUtility_Test.createArchievedData();
    vMarket_ArchievedTermsOfUse__c tersm = new vMarket_ArchievedTermsOfUse__c(Name = 'TEST');
    insert tersm;
    vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
      control.getTermsOfUseArchievedVersionAvailable();
    
    Test.StopTest();
  }
  
  public static testMethod void getTermsOfUseArchievedVersionAvailable_test2() {
    Test.StartTest();
    
    //List<vMarket_ArchievedTermsOfUse__c> archTermsOfUse = vMarketDataUtility_Test.createArchievedData();
    vMarket_ArchievedTermsOfUse__c tersm = new vMarket_ArchievedTermsOfUse__c(Name = 'TEST');
    
    vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
      control.getTermsOfUseArchievedVersionAvailable();
    
    Test.StopTest();
  }
  
  public static testMethod void getPrivacyArchievedVersions_test() {
    Test.Starttest();
      
      List<vMarket_ArchievedPrivacyTerms__c> termsList = new List<vMarket_ArchievedPrivacyTerms__c>();
      
      vMarket_ArchievedPrivacyTerms__c terms1 = new vMarket_ArchievedPrivacyTerms__c(Name = 'Privacy', Active__c = true, Archieved_Resource__c ='/resource/12345', 
                              Year__c = '2015');
      vMarket_ArchievedPrivacyTerms__c terms2 = new vMarket_ArchievedPrivacyTerms__c(Name = 'Privacy2', Active__c = false, Archieved_Resource__c ='/resource/123456', 
                              Year__c = '2016');
      
      termsList.add(terms1);
      termsList.add(terms2);
      insert termsList;
      
      vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
        control.getPrivacyArchievedVersions();
      
    Test.Stoptest();
  }
  
  public static testMethod void getPrivacyArchievedVersionAvailable_test() {
    Test.StartTest();
      vMarket_ArchievedPrivacyTerms__c terms = new vMarket_ArchievedPrivacyTerms__c();
      
      vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
        control.getPrivacyArchievedVersionAvailable();
    Test.Stoptest();
  }
  
  public static testMethod void validateUser_Test() {
    Test.StartTest();
      vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
        control.validateUser();
        string rescope = control.scope;
        string resrequestedAccDetails = control.scope;
    Test.StopTest();
  }
  
  public static testMethod void getPrivacyArchievedVersionAvailable_test2() {
    Test.StartTest();
      List<vMarket_ArchievedPrivacyTerms__c> termsList = new List<vMarket_ArchievedPrivacyTerms__c>();
      
      vMarket_ArchievedPrivacyTerms__c terms1 = new vMarket_ArchievedPrivacyTerms__c(Name = 'Privacy', Active__c = true, Archieved_Resource__c ='/resource/12345', 
                              Year__c = '2015');
      vMarket_ArchievedPrivacyTerms__c terms2 = new vMarket_ArchievedPrivacyTerms__c(Name = 'Privacy2', Active__c = true, Archieved_Resource__c ='/resource/123456', 
                              Year__c = '2016');
      
      termsList.add(terms1);
      termsList.add(terms2);
      insert termsList;
      
      vMarket_TermsOfUseController control = new vMarket_TermsOfUseController();
        control.getPrivacyArchievedVersionAvailable();
    Test.Stoptest();
  }
  
   public static User createStandardUser() {
        User usr = new User();
        usr.Email = 'test_Admin@bechtel.com';
        usr.Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN', 'vMarket', 70);
        usr.LastName = 'test' ;
        usr.IsActive = true;
        usr.FirstName = 'Tester';
        usr.Alias = 'teMar' ;
        usr.ProfileId = admin.Id ;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
        
        usr.vMarketTermsOfUse__c = true;
        usr.vMarket_User_Role__c = 'Customer';
        
        insert usr;
        return usr ;
    }
  
}
public with sharing class vMarket_Test_App_Details extends vMarketBaseController {
    public String AppVideoId {get;set;}
    public String appSourceId {get;set;}
    public Attachment attached_logo {get;set;}
    public Attachment attached_userGuidePDF {get;set;}
    //Selected count
    public Integer FileCount {get; set;}
    public List<Attachment> allFileList {get; set;}
    public vMarket_App__c new_app_obj {get;set;}
    public boolean isFree {get;set;}
    //public List<vMarketStripeDetail__c> userStripeDetail {get;set;}
    public vMarket_Developer_Stripe_Info__c stripeInfo {get;set;}
    
    public vMarket_Test_App_Details() {
        new_app_obj = new vMarket_App__c();
        attached_logo = new Attachment();
        attached_userGuidePDF = new Attachment();
        stripeInfo = [select Id, Stripe_Id__c, Stripe_User__c 
                        from vMarket_Developer_Stripe_Info__c 
                        where Stripe_User__c =: UserInfo.getUserId() limit 1];
        reinitiaze();
    }

    public void reinitiaze() {
        //Initialize
        FileCount = 4;
        allFileList = new List<Attachment>();
        for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
            allFileList.add(new Attachment()) ;   
    }
    
    public String selectedOpt{get;set;}
    public List<SelectOption> getAppType() {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('Free', 'Free'));
        option.add(new SelectOption('Subscription', 'Subscription'));
        option.add(new SelectOption('One Time', 'One Time'));
        return option;
    }
    
    public override Boolean getAuthenticated() {
        return super.getAuthenticated();
    }

    public override String getRole() {
        return super.getRole();
    }
    
    public Boolean getIsAccessible() {
        if(getAuthenticated() && getRole()=='Developer')
            return true;
        else
            return false;
    }
    
    public List<SelectOption> getAppStatus() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult aStatus = vMarket_App__c.AppStatus__c.getDescribe();
        List<Schema.PicklistEntry> appStatusValues = aStatus.getPicklistValues();

        for (Schema.PicklistEntry status : appStatusValues)
        { 
            options.add(new SelectOption(status.getLabel(), status.getValue())); 
        }
        return options;
    }
    
    public List<SelectOption> getDevAppCategories() {
        List<SelectOption> options = new List<SelectOption>();
        List<vMarket_App_Category__c> app_categories = [select Name from vMarket_App_Category__c];
        
        options.add(new SelectOption('','Select Category'));
        for(vMarket_App_Category__c cat:app_categories) {
            options.add(new SelectOption(cat.Id,cat.Name));
        }
        return options;
    }
    
    public void saveIsFree(){
        System.debug('==isFree=='+isFree);
    }

    public pageReference saveAppDetails() {
        if (true) {
            // App Basic Details store
            List<Attachment> listToInsert = new List<Attachment>() ;
            
            system.debug('==selectedOpt =='+selectedOpt);
            if(selectedOpt == 'Free'){
                new_app_obj.Price__c = 0.0;
                new_app_obj.IsFree__c = true;
                new_app_obj.subscription__c = false;
            } else if(selectedOpt == 'Subscription'){
                new_app_obj.subscription__c = true;
                new_app_obj.IsFree__c = false;
            }
            
            new_app_obj.Developer_Stripe_Acc_Id__c = stripeInfo.Stripe_Id__c;
            new_app_obj.ApprovalStatus__c = 'Draft';
            new_app_obj.IsActive__c = true;
            insert new_app_obj;

            String AppId = new_app_obj.id;
            attached_logo.parentid= AppId;
            attached_logo.ContentType = 'application/jpg';
            //insert attached_logo;
            listToInsert.add(attached_logo);
            
            attached_userGuidePDF.parentid = AppId;
            attached_userGuidePDF.ContentType = 'application/pdf';
            insert attached_userGuidePDF;
                
            // App Asset Store
            vMarketAppAsset__c appAsset;
            if (AppVideoId != null || AppVideoId != '') {
                appAsset = new vMarketAppAsset__c(Name='AppAsset_'+AppId, vMarket_App__c=AppId, AppVideoID__c = AppVideoId);
            } else {
                appAsset = new vMarketAppAsset__c(Name='AppAsset_'+AppId, vMarket_App__c=AppId);
            }
            insert appAsset;
            String accId = appAsset.id;
            
            vMarketAppSource__c appSource;
            if(new_app_obj.id != null){
                appSource = new vMarketAppSource__c(vMarket_App__c = new_app_obj.id,Status__c = 'New',IsActive__c = false);
            }
            insert appSource;
            appSourceId = appSource.Id;
            //List<Attachment> listToInsert = new List<Attachment>() ;
            Integer count = 0;
            for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++) {
                if(allFileList[0].name != '' && allFileList[0].name != '' && allFileList[0].body != null) {
                    listToInsert.add(new Attachment(parentId = appAsset.id, name = allFileList[0].name, body = allFileList[0].body)) ;
                    allFileList.remove(0);
                    count += 1;
                }
            }
            for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
                allFileList.add(new Attachment()) ;

            //Inserting attachments
            if(listToInsert.size() > 0) {
            allFileList = new List<Attachment>();
                insert listToInsert ;
                
                listToInsert = new List<Attachment>() ;
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, count + ' file(s) are uploaded successfully'));
            }
        }else {
            allFileList.clear();
            for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
                allFileList.add(new Attachment()) ;
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please, upload atleast one image.'));
        }
            PageReference pf = new PageReference('/vMarketDevAppPackagesUpload?appSourceId='+appSourceId);
            return pf;
    }
}
public class vMarket_UpdateApp {
	public void updateStatus(Id appId, String appStatus) {
		vMarket_App__c vmapp = [select Id From vMarket_App__c Where Id =: appId];
		vmapp.isApprovedOrRejected__c = false;
		if (appStatus!= null)
			vmapp.ApprovalStatus__c = appStatus;
		update vmapp;
	}
	
	public void updateFlagOnly(Id appId) {
		vMarket_App__c vmapp = [select Id From vMarket_App__c Where Id =: appId];
		vmapp.isApprovedOrRejected__c = false;
		update vmapp;
	}

	public void sendEmailToReviewer(List<String> emailList, String appId, String Status) {
		vMarket_App__c vmapp = [select Id, Name, ApprovalStatus__c, CreatedDate, OwnerId From vMarket_App__c Where Id =: appId];
		
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		// Step 1: Create a new Email
		Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();

		// Step 2: Set list of people who should get the email
		mail.setToAddresses(emailList);

		//// Step 3: Set who the email is sent from
		//mail.setReplyTo('nilesh.gorle@varian.com');
		//mail.setSenderDisplayName('vMarket Admin');

		//// (Optional) Set list of people who should be CC'ed
		//List<String> ccTo = new List<String>();
		//ccTo.add('anand.deep@varian.com');
		//mail.setCcAddresses(ccTo);

		// Step 4. Set email contents - you can use variables!
		mail.setSubject('vMarket App Under Review Mail < Reviewer/Admin Version >');

		String body;
		if (Status=='Under Review') {
			body = getUnderReviewBody(vmapp);
		} else if (Status=='Approved') {
			body = getApprovedBody(vmapp);
		} else if (Status=='Rejected') {
			body = getRejectedBody(vmapp);
		} else if (Status=='RejectedByRev') {
			User usr = [select Id, Name, Email From User Where Id=:vmapp.OwnerId limit 1];
			emailList.add(usr.Email);
			mail.setToAddresses(emailList);
			body = getRejectedDeveloperBody(vmapp, usr);
		}

		mail.setHtmlBody(body);
   
		// Step 5. Add your email to the master list
		mails.add(mail);
		Messaging.sendEmail(mails);		
	}

	// TODO - Fetch template from Email Template
	public String getUnderReviewBody(vMarket_App__c vmapp) {
		String body = '<p>Hello All,</p><p>New App <b>'+vmapp.Name+'</b> has been submitted for approval dated on '+vmapp.CreatedDate+'.<br/><p>Please, Review it</p><p>Click, <apex:outputLink value="https://varian--sfdev2.cs61.my.salesforce.com/'+vmapp.Id+'">Here</apex:outputLink> to review it.</p>--<br/>VMarket Team</p>';
		return body;
	}

	public String getApprovedBody(vMarket_App__c vmapp) {
		String body = '<p>Hello Admin,</p><p>App <b>'+vmapp.Name+'</b> has been approved by Reviewer.<br/><p>Please, Publish it</p><p>Click, <apex:outputLink value="https://varian--sfdev2.cs61.my.salesforce.com/'+vmapp.Id+'">Here</apex:outputLink> to publish it.</p>--<br/>VMarket Team</p>';
		return body;		
	}

	public String getRejectedBody(vMarket_App__c vmapp) {
		String body = '<p>Hello Admin,</p><p>App <b>'+vmapp.Name+'</b> has been rejected by Reviewer.<br/><p>Click, <apex:outputLink value="https://varian--sfdev2.cs61.my.salesforce.com/'+vmapp.Id+'">Here</apex:outputLink> to reject it.</p>--<br/>VMarket Team</p>';
		return body;		
	}
	
	public String getRejectedDeveloperBody(vMarket_App__c vmapp, User usr) {
		String body = '<p>Hello '+usr.Name+',</p><p>Your App <b>'+vmapp.Name+'</b> has been rejected on VMarket Portal.<br/><p>Go To Your App Details page to check rejection comment</p>--<br/>VMarket Team</p>';
		return body;		
	}

	public void sendEmailToAdminForReview(List<String> emailList, String emailSubject, String emailBody) {
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		// Step 1: Create a new Email
		Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();

		// Step 2: Set list of people who should get the email
		mail.setToAddresses(emailList);

		//// Step 3: Set who the email is sent from
		//mail.setReplyTo('nilesh.gorle@varian.com');
		//mail.setSenderDisplayName('vMarket Admin');

		//// (Optional) Set list of people who should be CC'ed
		//List<String> ccTo = new List<String>();
		//ccTo.add('anand.deep@varian.com');
		//mail.setCcAddresses(ccTo);

		// Step 4. Set email contents - you can use variables!
		mail.setSubject(emailSubject);
		mail.setHtmlBody(emailBody);

		// Step 5. Add your email to the master list
		mails.add(mail);
		Messaging.sendEmail(mails);
	}
}
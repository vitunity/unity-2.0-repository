public with sharing class vf_timeChooser {
    public String amOrPM{
        get{
            system.debug('VALUEOF PAGE---->' + pageval);
            if(Componentype == 'TimeIn')
                amOrPM = pageval.ValAMOrPM;
            else if(Componentype == 'TimeOut')
                amOrPM = pageval.ValAMOrPMout;
            return amOrPM;
        }
        set{
            if(value != null)
            {
                if(Componentype == 'TimeIn')
                    pageval.ValAMOrPM = value;
                else if(Componentype == 'TimeOut')
                    pageval.ValAMOrPMout = value;
            }
        }
    }
    public Integer Hourval{ 
        get{
            system.debug('VALUEOF PAGE---->' + pageval);
            if(Componentype == 'TimeIn')
            {
                if(pageval.ValHour == 0)
                    Hourval = 12;
                 else
                    Hourval = pageval.ValHour;
            }
            else if(Componentype == 'TimeOut')
                Hourval = pageval.ValHourout;
            return Hourval;
        }
            set{
                system.debug('DDDDDDD' + pageval);
                if(value != null)
                {
                    if(Componentype == 'TimeIn')
                        pageval.ValHour = value;
                    else if(Componentype == 'TimeOut')
                        pageval.ValHourout = value;
                }
            }
        }
    public Integer Minval{
        get{
            system.debug('VALUEOF PAGE---->' + pageval);
            if(Componentype == 'TimeIn')
                Minval = pageval.ValMin;
            else if(Componentype == 'TimeOut')
                Minval = pageval.ValMinout;
            return Minval;
        }
        set{
            if(value != null)
                if(Componentype == 'TimeIn')
                    pageval.ValMin = value;
                else if(Componentype == 'TimeOut')
                    pageval.ValMinout = value;
        }
    }
    public SR_TimeCard_Controler pageval{get;set;}
    public String Componentype{get;set;}
    public String idfromcontroller{get;set;}
// constructor
    public void vf_timeChooser(){
        
    }
    public void ok(){
        system.debug('Value of ---->' + Hourval + '---' + Minval);
    }
}
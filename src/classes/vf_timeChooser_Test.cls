/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class vf_timeChooser_Test {

    static testMethod void vf_timeChooser() {
        vf_timeChooser chooser = new vf_timeChooser();
        chooser.vf_timeChooser();
        chooser.ok();
    	chooser.idfromcontroller = '';
    }
    
    static testMethod void Componentype_TimeIN() {
    	vf_timeChooser chooser = new vf_timeChooser();
    	chooser.pageval = new SR_TimeCard_Controler();
    	chooser.pageval.ValHour = 0;
    	chooser.Componentype = 'TimeIn';
    	String test_amOrPM = chooser.amOrPM;
    	
    	Integer test_HourVal = chooser.HourVal;
    	
    	Integer test_MinVal = chooser.Minval;
    }
    
    static testMethod void Componentype_TimeIN_ValHour() {
    	vf_timeChooser chooser = new vf_timeChooser();
    	chooser.pageval = new SR_TimeCard_Controler();
    	chooser.pageval.ValHour = 1;
    	chooser.Componentype = 'TimeIn';
    	String test_amOrPM = chooser.amOrPM;
    	
    	Integer test_HourVal = chooser.HourVal;
    }
    
    static testMethod void Componentype_TimeOut() {
    	vf_timeChooser chooser = new vf_timeChooser();
    	chooser.pageval = new SR_TimeCard_Controler();
    	chooser.pageval.ValHour = 0;
    	chooser.Componentype = 'TimeOut';
    	String test_amOrPM = chooser.amOrPM;
    	
    	Integer test_HourVal = chooser.HourVal;
    	
    	Integer test_MinVal = chooser.Minval;
    }
    
    static testMethod void Componentype_TimeIN_SET() {
    	vf_timeChooser chooser = new vf_timeChooser();
    	chooser.pageval = new SR_TimeCard_Controler();
    	chooser.pageval.ValHour = 0;
    	chooser.Componentype = 'TimeIn';
    	chooser.amOrPM = 'test';
    	
    	chooser.HourVal = 10;
    	
    	chooser.Minval = 10;
    }
    
    static testMethod void Componentype_TimeOut_SET() {
    	vf_timeChooser chooser = new vf_timeChooser();
    	chooser.pageval = new SR_TimeCard_Controler();
    	chooser.pageval.ValHour = 0;
    	chooser.Componentype = 'TimeOut';
    	chooser.amOrPM = 'test';
    	
    	chooser.HourVal = 10;
    	
    	chooser.Minval = 10;
    }
}
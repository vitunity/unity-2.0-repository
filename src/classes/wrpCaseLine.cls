public class wrpCaseLine  // Wrapper Case Line
 {
      public SVMXC__Case_Line__c objCaseLine{get;set;}
      public boolean blnCaseLine{get;set;}
      public wrpCaseLine(SVMXC__Case_Line__c varCaseLine,boolean isselected)
      {
        objCaseLine=varCaseLine;
        blnCaseLine=isselected;
      }
}
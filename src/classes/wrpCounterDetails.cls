public class wrpCounterDetails implements comparable // Wrapper for counter detail
    {
        public SVMXC__Counter_Details__c counterDetail{get;set;}
        public Boolean selected{get;set;}
        public string strUssageStatus{get;set;}
        public Date strAcceptancedate{get;set;}
        public String strNWAName{get;set;}
        public String erpnwaid{get;set;}
        public String strEstimatedWork{get;set;}
        public Boolean disabled {get; set;}
        public String strERPWBSid{get;set;}
        public String combduration{get;set;}
       /* public wrpCounterDetails(SVMXC__Counter_Details__c cd,Boolean bln,Date actpDate,String strNW,String EstimatedWork)
        {
            counterDetail = cd;
            selected = bln;
            strAcceptancedate = date.valueof(actpDate);
            strNWAName = strNW;
            strEstimatedWork = EstimatedWork;
            combduration = Date.valueOf(cd.Start_Date__c) + ' - ' + cd.End_Date__c + ' , ' + cd.Duration_in_Years__c + ' , ' + cd.End_Date__c;
        }*/
        public wrpCounterDetails(SVMXC__Counter_Details__c cd,Boolean bln,String usg,String EstimatedWork, Boolean d)
        {
            counterDetail = cd;
            selected = bln;
            strUssageStatus = usg;
            strEstimatedWork = EstimatedWork;
            disabled = d;
            combduration = Date.valueOf(cd.Start_Date__c) + ' - ' + cd.End_Date__c + ' , ' + cd.Duration_in_Years__c + ' , ' + cd.End_Date__c;
        }
        public wrpCounterDetails(SVMXC__Counter_Details__c cd,Boolean bln,Date actpDate,String strNW,String EstimatedWork, Boolean d)
        {
            counterDetail = cd;
            selected = bln;
            strAcceptancedate = date.valueof(actpDate);
            strNWAName = strNW;
            strEstimatedWork = EstimatedWork;
            disabled = d;
            combduration = Date.valueOf(cd.Start_Date__c) + ' - ' + cd.End_Date__c + ' , ' + cd.Duration_in_Years__c + ' , ' + cd.End_Date__c;
        }
        public wrpCounterDetails(SVMXC__Counter_Details__c cd,Boolean bln,String usg,Date actpDate,String strNW,String nwaid,String EstimatedWork, Boolean d,string wbs)
        {
            counterDetail = cd;
            selected = bln;
            strUssageStatus = usg;
            strAcceptancedate = date.valueof(actpDate);
            strNWAName = strNW;
            erpnwaid = nwaid;
            strEstimatedWork = EstimatedWork;
            disabled = d;
            strERPWBSid = wbs;
            combduration = Date.valueOf(cd.Start_Date__c) + ' - ' + cd.End_Date__c + ' , ' + cd.Duration_in_Years__c + ' , ' + cd.End_Date__c;
        }
        /*public wrpCounterDetails(SVMXC__Counter_Details__c cd,Boolean bln,String usg,Date actpDate,String strNW,String nwaid,String EstimatedWork, Boolean d)
        {
            counterDetail = cd;
            selected = bln;
            strUssageStatus = usg;
            strAcceptancedate = date.valueof(actpDate);
            strNWAName = strNW;
            erpnwaid = nwaid;
            strEstimatedWork = EstimatedWork;
            disabled = d;
            combduration = Date.valueOf(cd.Start_Date__c) + ' - ' + cd.End_Date__c + ' , ' + cd.Duration_in_Years__c + ' , ' + cd.End_Date__c;
        }*/
    
  public Integer compareTo(Object other){
    Date otherDOJ;
    if (other == null || ((wrpCounterDetails)other).counterDetail == null || this.counterDetail == null) return 1;
    if ( ((wrpCounterDetails)other).counterDetail.End_Date__c != null )
        otherDOJ = other != null ? ((wrpCounterDetails)other).counterDetail.End_Date__c : System.today(); 
    else
        otherDOJ = other != null ? ((wrpCounterDetails)other).counterDetail.Start_Date__c : System.today(); 
    if (otherDOJ == null) return -1;
    if ( this.counterDetail.End_Date__c != null )
        return otherDOJ.daysBetween(this.counterDetail.End_Date__c); 
    else
        return otherDOJ.daysBetween(this.counterDetail.Start_Date__c);
  }
}
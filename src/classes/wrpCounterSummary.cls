public class wrpCounterSummary
{
    public String strCounterName{get;set;}
    public Decimal sumCounterReading{get;set;}
    public String strUnitType{get;set;}
    public Decimal strSumTotalSold{get;set;}
    
    public wrpCounterSummary(String CntrName,Decimal sumCntrReading,String strunt,Decimal sumsold)
    {
        strCounterName=CntrName;
        sumCounterReading =sumCntrReading;
        strUnitType=strunt;
        strSumTotalSold=sumsold;
    }
}
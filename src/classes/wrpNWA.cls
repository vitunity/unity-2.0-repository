/**
  * @author jwagner/Forefront
  * @version 1.0
  * @description
  *
  * PURPOSE
  * 
  *    wrapper class for ERP NWA object for use in SR_ViewEntitlement
  * 
  * TEST CLASS 
  * 
  *    wrpNWA_TEST
  *  
  * ENTRY POINTS 
  *  
  *    SR_ViewEntitlement
  * 
  * CHANGE LOG 
  * 
  *    [Version; Date; Author; Description]
  *    v1.0; 1-7-16;  jwagner/Forefront; Initial Build
  * 
  **/
public class wrpNWA // Wrapper for NWA
{
    public ERP_NWA__c ObjNWA{get;set;}
    public boolean blnselprod{get;set;}
    public Integer usrval{get;set;}
    public Integer requiredCredits{get;set;} //US5099 JW@ff 10-16-15
    public Boolean displayInfoOnly{get;set;} //US5099 10-16-2015 JW@ff
    public ID counterdetail {get;set;} //DE7132;
    
    public wrpNWA(ERP_NWA__c nwa,boolean temp,Integer inptval, Integer credits, Boolean displayOnly,ID cdid)
    {   
        ObjNWA= new ERP_NWA__c();
        ObjNWA = nwa;
        blnselprod=false;
        usrval = inptval;  
        blnselprod = temp;
        requiredCredits = credits; //US5099 10-16-2015 JW@ff
        displayInfoOnly = displayOnly; //US5099 10-16-2015 JW@ff
        counterdetail = cdid; //de7132
        
    }
    
    public wrpNWA(ERP_NWA__c nwa,boolean temp,Integer inptval, Integer credits, Boolean displayOnly)
    {   
        ObjNWA= new ERP_NWA__c();
        ObjNWA = nwa;
        blnselprod=false;
        usrval = inptval;  
        blnselprod = temp;
        requiredCredits = credits; //US5099 10-16-2015 JW@ff
        displayInfoOnly = displayOnly; //US5099 10-16-2015 JW@ff
        
    }
        
    //US5099 10-16-2015 JW@ff -- added so that wrapper can be sed in places not requiring requiredCredits field
    public wrpNWA(ERP_NWA__c nwa,boolean temp,Integer inptval)
    {   
        ObjNWA= new ERP_NWA__c();
        ObjNWA = nwa;
        blnselprod=false;
        usrval = inptval;
        blnselprod = temp;
    }
}
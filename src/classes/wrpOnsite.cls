public class wrpOnsite implements comparable
    {
        public String strSalesOrder{get;set;}
        public String strSalesOrderItem{get;set;}
        public String strTrainingName{get;set;}
        public String strTrainer1{get;set;}
        public String strStartDateofIWO{get;set;}
        public String strEndDateofIWO{get;set;}
        public String woOrderStatus{get;set;}
        public String woName {get;set;}
        public String woCancelled {get;set;}
        public String woClosed {get;set;}
        
        public wrpOnsite(String woName,String so,String soi,String trningName,String Trainer, String Datestart,String DateEnd,
                         String woCancelled, String woClosed, String woOrderStatus)
        {
            strSalesOrder = so;
            strSalesOrderItem = soi;
            strTrainingName = trningName;
            strTrainer1 = Trainer;
            strStartDateofIWO = Datestart;
            strEndDateofIWO = DateEnd;
            //strWorkDetail = WorkDetail;
            //strWOId = woid;
            this.woOrderStatus = woOrderStatus;
            this.woCancelled = woCancelled;
            this.woClosed = woClosed;
            this.woName = woName;
            /*this.wdLineStatus = wdLineStatus;
            this.iwoId = iwoId;
            this.hasIWO = iwo;*/
        }
        public Integer compareTo(Object ObjToCompare) {
            return woName.CompareTo(((wrpOnsite)ObjToCompare).woName);
        }
    }
public class wrpProd // Wrapper for counter detail
{
    public Product2 ObjProd{get;set;}
    public boolean blnselprod{get;set;}
    public integer usrval{get;set;}
    public Integer requiredCredits{get;set;} //US5099 JW@ff 10-16-15
    public Boolean displayInfoOnly{get;set;} //US5099 10-16-2015 JW@ff
    
    public wrpProd(Product2 prod,boolean temp,Integer inptval, Integer credits, Boolean displayOnly)
    {   
        ObjProd= new Product2();
        blnselprod=false;
        usrval = inptval;
        ObjProd = prod;
        blnselprod = temp;
        requiredCredits = credits; //US5099 10-16-2015 JW@ff
        displayInfoOnly = displayOnly; //US5099 10-16-2015 JW@ff
    }
        
    //US5099 10-16-2015 JW@ff -- added so that wrapper can be sed in places not requiring requiredCredits field
    public wrpProd(Product2 prod,boolean temp,Integer inptval)
    {   
        ObjProd= new Product2();
        blnselprod=false;
        usrval = inptval;
        ObjProd = prod;
        blnselprod = temp;
    }
}
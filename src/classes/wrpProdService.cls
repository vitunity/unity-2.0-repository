public class wrpProdService // wrapper Product-Available services
    {
        public Boolean chkbxVisible{get;set;}
        public List<wrpProdService> lstSLADetail{get;set;}
        public String strProdName{get;set;}   // String to Product Name
        public Boolean isselected{get;set;}   // Property to Select, De-select  Available Service
        Public SVMXC__SLA_Detail__c objSLADetail{get;set;}
        public SVMXC__Service_Contract__c objSrvcContrct{get;set;}
        public SVMXC__Service_Contract_Products__c ObjCoveredProduct{get;set;}
        public String strDeliveryStatus{get;set;}
        public wrpProdService(String strProdName1,List<wrpProdService> lstAvailableServices,SVMXC__SLA_Detail__c sla,Boolean isSelected1,Boolean chkbxvsbl,SVMXC__Service_Contract__c objCon,SVMXC__Service_Contract_Products__c varCoverProd,String strDelivery)
        {
           lstSLADetail = lstAvailableServices;     //List Of SLA Detail-Available Services
           objSLADetail = sla;                      // Assigning SLA Detail-Available Service
           strProdName  = strProdName1;              //String prod Name
           chkbxVisible = chkbxvsbl ;               //Propert to render checkBox on VisualForce Page
           objSrvcContrct = objCon;
           ObjCoveredProduct=varCoverProd;
           strDeliveryStatus=strDelivery;
        }
    } // END public class wrpProdService
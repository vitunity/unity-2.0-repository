/*********Wrapper for counter detail***********/

public class wrpSalesOrderItem 
{
    public Sales_Order_Item__c Objsoi{get;set;}
    public boolean blnselSOI{get;set;}
    public ERP_WBS__c strERPWBS{get;set;}
    public string strRegistrationCode{get;set;}
    public string strUssageStatus{get;set;}
    public string strSitePartner{get;set;}
    public date strAcceptancedate{get;set;}
    public string strMaterialGroup{get;set;}
    public string strNWA{get;set;}
        
    public wrpSalesOrderItem(Sales_Order_Item__c soi,boolean temp,ERP_WBS__c erpWBS,string strRegis,string strUssage,string SitePartner, date strAcpDate,string strM,string varNWAName)
    {  
        strAcceptancedate=strAcpDate;
        Objsoi = soi;
        blnselSOI = temp;
        strERPWBS=erpWBS;
        strRegistrationCode=strRegis;
        strUssageStatus=strUssage; //Delivery Status Column on SR_ViewEntitlement visualforce page
        strSitePartner=SitePartner;
        strMaterialGroup=strM;
        strNWA=varNWAName;
    }
}
/***************************************************************************************
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Feb 20 2018 : Chandramouli :  STSK0013982 : Modified code to handled the changes in Onsite Section.
****************************************************************************************************/
public class wrpWorkDetail
{
    public String strsoi{get;set;}
    public String strso{get;set;}
    public List<wrpWorkDetail> lstwrpWD_withinwrap{get;set;}    //List of Wrapper
    public String locationWD {get;set;}     //WD location
    public String strCounterName{get;set;} // Set Counter name
    public String strCountName{get;set;} // Set Counter name
    public Work_Detail_Contacts__c strContactName{get;set;} // Set Counter name
    public SVMXC__Service_Order_Line__c objworkDetail{get;set;} // Set Obj Work Detail
    public Boolean blnWD{get;set;}
    public String dtStartDate{get;set;}
    public String dtEndDate{get;set;}
    public String strUnit{get;set;}
    public String strERPReference{get;set;}
    public String strerpnwa{get;set;}
    public String status{get;set;}
    public Date NWAexpdate{get;set;}
    public wrpWorkDetail(String so, String soi,List<wrpWorkDetail> lstWrp,SVMXC__Service_Order_Line__c varWD,Work_Detail_Contacts__c CntrName,String start,String endDate,String strUnitType,String strReference,String strCTRName,String strCTDName,String strLoc,Boolean isselWD, String erpnwa, String regstatus, Date nwaexp) // Constructor
    {   
        strsoi  = soi;
        strso   = so;
        lstwrpWD_withinwrap = lstWrp;
        locationWD = strLoc;
        strCounterName=strCTRName;
        strCountName=strCTDName;
        strContactName=CntrName;  // Counter Name
        objworkDetail =varWD;     // Work Detail
        blnWD =isselWD;           //Selection Variable
        dtStartDate=start;
        dtEndDate=endDate;
        strUnit=strUnitType;
        strERPReference=strReference;
        strerpnwa = erpnwa;
        status = regstatus;
        NWAexpdate = nwaexp;
    }
}
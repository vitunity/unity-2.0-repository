@isTest
private class wrp_test {
	
	@isTest static void wrpCaseTest() {
		WrpCase wc = new WrpCase(new Case(subject='wrpCaseTest'),true);
		system.assertEquals('wrpCaseTest',wc.ObjCase.subject);
		system.assertEquals(true,wc.blnselectCase);
	}
	
	@isTest static void wrpCaseLineTest() {
		Case c =  new Case(Subject=' wrpCaseLineTest');
		insert c;
		WrpCaseLine wc = new WrpCaseLine(new SVMXC__Case_Line__c(SVMXC__case__c=c.id),true);
		system.assertEquals(c.id,wc.ObjCaseLine.SVMXC__case__c);
		system.assertEquals(true,wc.blnCaseLine);
	}
     
     @isTest static void wrpClsTest() {
     	wrpCls w = new wrpCls('strTname',new list<wrpCounterSummary> { new wrpCounterSummary('CntrName',1.0,'strunt',0.1) });
     	system.assertEquals('strTname',w.strTrainingName);
     	system.assertEquals(1,w.lsttemp.size());
     	system.assertEquals('CntrName',w.lsttemp[0].strCounterName);
     	system.assertEquals(1.0,w.lsttemp[0].sumCounterReading);
     	system.assertEquals('strunt',w.lsttemp[0].strUnitType);
     	system.assertEquals(0.1,w.lsttemp[0].strSumTotalSold);
     }
     @isTest static void wrpCounterDetailsTest() {
     	List <wrpCounterDetails> lwcd = new List<wrpCounterDetails>();
     	
         wrpCounterDetails wcd2 = new wrpCounterDetails(new SVMXC__Counter_Details__c(SVMXC__Counter_Name__c='cd2',Start_Date__c=System.today(),End_Date__c=System.today().addDays(1)),false,System.Today().addDays(-1),'98765-4321','3.21', true);
      	 lwcd.add(wcd2);
      	 wrpCounterDetails wcd1 = new wrpCounterDetails(new SVMXC__Counter_Details__c(SVMXC__Counter_Name__c='cd1',Start_Date__c=System.today().addDays(-1),End_Date__c=System.today()),true,System.Today(),'12346-9876','12.3');
      	 lwcd.add(wcd1);
      	 
      	 lwcd.sort();
      	 system.assertEquals('cd1',wcd1.counterDetail.SVMXC__Counter_Name__c);
      	 system.assertEquals(true,wcd1.selected);
      	 system.assertEquals(System.Today(),wcd1.strAcceptancedate);
      	 system.assertEquals('12346-9876',wcd1.strNWAName);
      	 system.assertEquals('12.3',wcd1.strEstimatedWork);
      	 
      	 system.assertEquals('cd2',wcd2.counterDetail.SVMXC__Counter_Name__c);
      	 system.assertEquals(false,wcd2.selected);
      	 system.assertEquals(System.Today().addDays(-1),wcd2.strAcceptancedate);
      	 system.assertEquals('98765-4321',wcd2.strNWAName);
      	 system.assertEquals('3.21',wcd2.strEstimatedWork);
      	 system.assertEquals(true,wcd2.disabled);
      	 
      	 system.assertEquals('cd1',lwcd[0].counterDetail.SVMXC__Counter_Name__c);
     }
     
     @isTest static void wrpEventTest() { 	 
      	wrpEvent we = new wrpEvent(new Event(Subject='wrpEventTest'),false,'EvtCity');
      	system.assertEquals('wrpEventTest',we.objevt.Subject);
      	system.assertEquals(false,we.blnselevt);
      	system.assertEquals('EvtCity',we.strEvtCity);
     }
     
     @isTest static void wrpNWATest() {
     	SVMXC__Counter_Details__c cd = new SVMXC__Counter_Details__c(SVMXC__Counter_Name__c='nwa');
     	insert cd;
     
     	wrpNWA wnwa6 = new wrpNWA(new ERP_NWA__c(name='wnwa6'),true,3, 4, false,cd.id);
     	system.assertEquals('wnwa6',wnwa6.objNWa.name);
     	system.assertEquals(true,wnwa6.blnselprod);
     	system.assertEquals(3,wnwa6.usrval);
     	system.assertEquals(4,wnwa6.RequiredCredits);
     	system.assertEquals(false,wnwa6.displayInfoOnly);
     	system.assertEquals(cd.id,wnwa6.counterdetail);
     	wrpNWA wnwa5 = new wrpNWA(new ERP_NWA__c(name='wnwa5'),false,1, 0, true);
     	system.assertEquals('wnwa5',wnwa5.objNWa.name);
     	system.assertEquals(false,wnwa5.blnselprod);
     	system.assertEquals(1,wnwa5.usrval);
     	system.assertEquals(0,wnwa5.RequiredCredits);
     	system.assertEquals(true,wnwa5.displayInfoOnly);
     	wrpNWA wnwa3 = new wrpNWA(new ERP_NWA__c(name='wnwa3'),True,2);
     	system.assertEquals('wnwa3',wnwa3.objNWa.name);
     	system.assertEquals(True,wnwa3.blnselprod);
     	system.assertEquals(2,wnwa3.usrval);
     }
     
     @isTest static void wrpOnsite() {
     	Sales_Order__c so = new Sales_order__c(name='test_so');
     	insert so;
     	Sales_Order_Item__c soi = new Sales_Order_Item__c(Sales_Order__c=so.id,name='test_soi');
     	insert soi;
     	SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(Subject__c='test_wo');
     	insert wo;
     	wo = [select id, name, Subject__c from SVMXC__Service_Order__c where id = :wo.id];
     	SVMXC__Service_Order_Line__c wd = new  SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c=wo.id,SVMXC__Work_Description__c='test_wd');
     	insert wd;
     	wd = [select id, name,SVMXC__Work_Description__c from SVMXC__Service_Order_Line__c where id = :wd.id ];
     	
        wrpOnsite wos = new	wrpOnsite(so.id,soi.id,'Test Training','John Doe', System.today(),System.today().addDays(5),
                         'Assigned', false, 'Open', wo.Name, 
                        wd.name,wo.id, wd.id, true);
                        
	     system.assertEquals(wos.strSalesOrder,so.id);
	     system.assertEquals(wos.strSalesOrderItem,soi.id);
	     system.assertEquals(wos.strTrainingName,'Test Training');
	     system.assertEquals(wos.strTrainer1,'John Doe');
	     system.assertEquals(wos.strStartDateofIWO,System.today());
	     system.assertEquals(wos.strEndDateofIWO,System.today().addDays(5));
	     system.assertEquals(wos.strWorkDetail,wd.name);
	     system.assertEquals(wos.strWOId,wo.id);
	     system.assertEquals(wos.woOrderStatus,'Assigned');
	     system.assertEquals(wos.wdCancel,false);
	     system.assertEquals(wos.wdCompleted,true);
	     system.assertEquals(wos.wdLineStatus,'Open');
	     system.assertEquals(wos.woName,wo.Name);
	     system.assertEquals(wos.iwoId,wd.id);
     }
     
     @isTest static void wrpOnsiteTest() {
     	Product2 p= new Product2(Name='ZZZ PRODUCT TEST');
     
      	wrpProd wp = new wrpProd(p,true,1, 2, false) ;
      	 
      	 
       
        system.assertEquals(wp.usrval,1);
        system.assertEquals(wp.ObjProd,p);
        system.assertEquals(wp.blnselprod,true);
        system.assertEquals(wp.requiredCredits,2); 
        system.assertEquals(wp.displayInfoOnly,false); 
      	
      	
      	 wrpProd wp3 = new wrpProd(p,false,3);
      	 system.assertEquals(wp3.blnselprod,false);
         system.assertEquals(wp3.usrval,3);
         system.assertEquals(wp3.ObjProd,p);
         system.assertEquals(wp3.blnselprod,false);
     }
     
     @isTest static void wrpProdServiceTest() {
     	List<wrpProdService> lwps = new List<wrpProdService>();
     	SVMXC__SLA_Detail__c sla = new SVMXC__SLA_Detail__c();
     	SVMXC__Service_Contract__c objCon = new SVMXC__Service_Contract__c();
     	SVMXC__Service_Contract_Products__c varCoverProd = new SVMXC__Service_Contract_Products__c();
     
     
	     wrpProdService wps = new wrpProdService('ProductName',lwps, sla,true,false,objCon,varCoverProd,'strDelivery');
	     
	     system.assertEquals(wps.lstSLADetail,lwps);    
	     system.assertEquals(wps.objSLADetail,sla);               
	     system.assertEquals(wps.strProdName,'ProductName');        
	     system.assertEquals(wps.chkbxVisible,false);      
	     system.assertEquals(wps.objSrvcContrct,objCon);
	     system.assertEquals(wps.ObjCoveredProduct,varCoverProd);
	     system.assertEquals(wps.strDeliveryStatus,'strDelivery');
     }
      @isTest static void wrpSalesOrderItemTest() {
      	Sales_Order_Item__c soi = new Sales_Order_Item__c();
      	ERP_WBS__c erpWBS = new ERP_WBS__c();
      	
     	wrpSalesOrderItem wsoi = new wrpSalesOrderItem(soi,true,erpWBS,'strRegis','strUssage','SitePartner', System.today(),'strM','varNWAName');
     	
     	system.assertEquals(wsoi.strAcceptancedate,System.today());
        system.assertEquals(wsoi.Objsoi, soi);
        system.assertEquals(wsoi.blnselSOI,true);
        system.assertEquals(wsoi.strERPWBS,erpWBS);
        system.assertEquals(wsoi.strRegistrationCode,'strRegis');
        system.assertEquals(wsoi.strUssageStatus,'strUssage'); 
        system.assertEquals(wsoi.strSitePartner,'SitePartner');
        system.assertEquals(wsoi.strMaterialGroup,'strM');
        system.assertEquals(wsoi.strNWA,'varNWAName');
      }
      
      @isTest static void wrpWorkDetailTest() {
      	List<wrpWorkDetail> lstWrp = new List<wrpWorkDetail>();
      	Sales_Order__c so = new Sales_order__c(name='test_so');
     	insert so;
     	Sales_Order_Item__c soi = new Sales_Order_Item__c(Sales_Order__c=so.id,name='test_soi');
     	insert soi;
     	SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(Subject__c='test_wo');
     	insert wo;
     	//wo = [select id, name, Subject__c from SVMXC__Service_Order__c where id = :wo.id];
     	SVMXC__Service_Order_Line__c varwd = new  SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c=wo.id,SVMXC__Work_Description__c='test_wd');
     	//insert varwd;
     	Work_Detail_Contacts__c CntrName = new Work_Detail_Contacts__c();
     	
     	//varwd = [select id, name,SVMXC__Work_Description__c from SVMXC__Service_Order_Line__c where id = :wd.id ];
      	wrpWorkDetail wwd = new wrpWorkDetail(so.id, soi.id,lstWrp,varWD,CntrName,System.today(),System.today().addDays(5),'strUnitType','strReference','strCTRName','strCTDName','strLoc',true, 'erpnwa');
      	
      	system.assertEquals(wwd.strsoi,soi.id);
        system.assertEquals(wwd.strso,so.id);
        system.assertEquals(wwd.lstwrpWD_withinwrap,lstWrp);
        system.assertEquals(wwd.locationWD,'strLoc');
        system.assertEquals(wwd.strCounterName,'strCTRName');
        system.assertEquals(wwd.strCountName,'strCTDName');
        system.assertEquals(wwd.strContactName,CntrName);  // Counter Name
        system.assertEquals(wwd.objworkDetail,varWD);     // Work Detail
        system.assertEquals(wwd.blnWD,true);           //Selection Variable
        system.assertEquals(wwd.dtStartDate,System.today());
        system.assertEquals(wwd.dtEndDate,System.today().addDays(5));
        system.assertEquals(wwd.strUnit,'strUnitType');
        system.assertEquals(wwd.strERPReference,'strReference');
        system.assertEquals(wwd.strerpnwa,'erpnwa');
      }
}
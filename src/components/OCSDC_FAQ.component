<apex:component >
			<article class="create group">
         <apex:form style="text-align: left;margin-top: 20px;">
             <p style="font-weight: bold;text-align: left;color:#26868b;font-size: 20px;" >{!$Label.OCSUGC_FAQPage_Header} For Developer Cloud</p>

            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">What is OncoPeer?</span></p>
            <p style="font-size: 14px;padding-left: 1em;">OncoPeer is an online oncology community where you can publish
            knowledge, share data, exchange techniques, and discuss best practices while broadening your network.</p>



            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">Who is the community for? </span></p>
            <p style="font-size: 14px;padding-left: 1em;">OncoPeer is available only for Varian customers and is a not an open community.</p>



            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">How do I login/register?</span></p>
            <p style="font-size: 14px;padding-left: 1em;">OncoPeer is currently only open to all Varian customers. 
            You can authenticate/login into the community using your
            existing MyVarian account name and password. If you do not have a MyVarian
            account, you can sign up as a new user on OncoPeer and upon approval, you will
            be able to login to both MyVarian and OncoPeer.</p>



            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">How is OncoPeer useful to me? </span></p>
            <p style="font-size: 14px;padding-left: 1em;">You can remain updated on the latest advancements and
            current trends in oncology and connect with more peers than has been possible
            in the past. By starting discussion topics, joining and creating groups, sharing
            data, you can contribute to optimizing the way oncology-related content is
            distributed throughout the community.</p>



            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">What is Varian�s role in OncoPeer? </span></p>
            <p style="font-size: 14px;padding-left: 1em;">Although the community is hosted by Varian, it is
            intended to contain primarily user generated content. Varian
            will play a limited role in the community to enforce the <a data-target="#modal_tc1" data-toggle="modal" onclick="$('#tc_content').scrollTop(0);$('#ocsugcterms').click();" href="#">{!$Label.OCSUGC_TermsOfUseTitle}</a> and <a data-target="#modal_tc1" onclick="$('#ocsugcprivacy').click();$('#tc_content').scrollTop(0);" data-toggle="modal" href="#">{!$Label.OCSUGC_Privacy_Statement}</a>. </p>



            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">What kind of content should be on OncoPeer? </span></p>
            <p style="font-size: 14px;padding-left: 1em;">The content should be oncology related and remain
            non-promotional, but the rest is left first and foremost to the users. There is
            the ability to share knowledge files, generate discussions, create events, and
            more. </p>



            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">What types of data can be shared through OncoPeer?</span></p>
            <p style="font-size: 14px;padding-left: 1em;">You can share files such as documents, Rapid Plan&trade; DVH
            estimations, Aria&reg; Care Path Templates, or pdfs, as long as there is no protected health information or patient identifying information in the file and it complies with the community <a data-target="#modal_tc1" data-toggle="modal" onclick="$('#tc_content').scrollTop(0);$('#ocsugcterms').click();" href="#">{!$Label.OCSUGC_TermsOfUseTitle}</a>.</p>

            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">How do I know that OncoPeer is a secure forum and my contributions to a private groups are safe?</span></p>
            <p style="font-size: 14px;padding-left: 1em;">OncoPeer is built on a robust industry grade customer relationship management (CRM) platform managed by Salesforce.  The Salesforce environment maintains some of the highest security standards in the industry such as utilizing secure transmissions via TLS cryptographic protocols, global step-up certificates, ensuring users have a secure connection from their browsers to the service, and firewall network protection measures.  This platform is relied upon by many other Fortune 500 companies to keep customer data safe. For further information about your security within OncoPeer please refer to <a href="https://trust.salesforce.com/trust/security/" target="_blank">Salesforce</a>, and our <a data-target="#modal_tc1" data-toggle="modal" onclick="$('#tc_content').scrollTop(0);$('#ocsugcterms').click();" href="#">{!$Label.OCSUGC_TermsOfUseTitle}</a> and <a data-target="#modal_tc1" onclick="$('#ocsugcprivacy').click();$('#tc_content').scrollTop(0);" data-toggle="modal" href="#">{!$Label.OCSUGC_Privacy_Statement}</a>.</p>

            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">Is it ok to discuss patient cases on OncoPeer?</span></p>
            <p style="font-size: 14px;padding-left: 1em;">It is ok to discuss clinical matters, but protected health information and patient identifying information must not be part of any discussion.  All of the data posted on OncoPeer must be anonymized prior to posting.</p>



            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">How long will data remain available for
            people to view on OncoPeer? </span></p>
            <p style="font-size: 14px;padding-left: 1em;">Our current intent is that data will remain available for people to view indefinitely.</p>

            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">What internet requirements are necessary for
            running OncoPeer? </span></p>
            <p style="font-size: 14px;padding-left: 1em;">Please use the latest browsers such as Firefox, Chrome, Safari, and Internet Explorer. <br/>For Internet Explorer the minimum version required is IE9.</p>
			
			<p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">How do I access Developer Cloud?</span></p>
			<p style="font-size: 14px;padding-left: 1em;">Developer Cloud is a community created for Varian developers.  Most Varian developers have been added to the Dev Cloud based on the following criteria, participation in a Developer Workshop, registered usage of any of our Research Tool, or possession of a Research Collaborations contract. You can request access to participate in the Developer Cloud by writing to us at oncopeer@varian.com, and explaining the nature of you project to us. </p>
						
            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">Why am I sent to MyVarian when I request an OncoPeer account?</span></p>
            <p style="font-size: 14px;padding-left: 1em;">Varian Medical Systems utilizes a third party single sign on (SSO) solution to verify, streamline, and secure user accounts. To sign up for a Varian ID users need to create a MyVarian account. The same username and password used to create your MyVarian account will give you access to OncoPeer.</p>


            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">Can I talk about product
            complaints and related product issues on OncoPeer?</span></p>
            <p style="font-size: 14px;padding-left: 1em;">We encourage you to follow the formal <a href="https://varian.force.com/apex/CPContactUs" target="_blank">Varian Complaint process</a>.</p>

            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: underline;">Are there Community Standards? </span></p>
             <p style="font-size: 14px;padding-left: 1em;">
                 Yes! Community standards consist of these FAQs and the relevant portions of the terms and conditions about appropriate behavior on OncoPeer. To read more about how to use best this forum you can visit our Term of Use and the Privacy Statement. Your network values your thoughtful contribution; please keep the Community Standards in mind as you interact with other users.
             </p>

            <br/>
            <p style="font-weight: bold; font-size: 14px;"><span style="text-decoration: none;">Varian reserves the right to modify these FAQs without notice.</span></p>

             <br/>
             
             <span style="font-weight: bold;">Do's</span>
                 <ul>
                   <li>Do use OncoPeer to share insightful knowledge, information and data.</li>
                   <li>Do use OncoPeer to expand your oncology social network.</li>
                   <li>Do use OncoPeer to start discussions and collaborate with peers.</li>
                 </ul>
                 <span style="font-weight: bold;">Don'ts</span>
                 <ul>
                   <li>Don�t use OncoPeer to as way to promote personal or Varian related products or material.</li>
                   <li>Don�t use OncoPeer to discuss non-oncology related topics.</li>
                   <li>Don�t upload or discuss any protected health information and patient identifying information on the community.</li>
                 </ul>
         </apex:form>
     </article>
</apex:component>
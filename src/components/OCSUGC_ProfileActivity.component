<apex:component controller="OCSUGC_ProfileActivityController" allowDML="true">
<apex:form >
  <apex:actionFunction action="{!populateProfileActivity}"  name="loadProfileActivities" reRender="activities,activities-sort-filter-bar" oncomplete="drawActivityGrid()" status="loadingActivitiesStatus"/>
  
  <div class="row">
    <div class="col-sm-12 col-md-12">
      <apex:outputPanel layout="block" styleClass="sort-filter-bar row" id="activities-sort-filter-bar">
        <apex:outputPanel layout="block" styleClass="btn-toolbar" rendered="{!lstProfileActivity.size>0}">
          <div class="btn-group filters" data-filter-group="area">
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="btn-group-label">Community:</span><span class="selection">Any</span> <span class="caret"></span></button>
            <ul id="activity-fltr-comm-dropdown" class="dropdown-menu">
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-filter="*">Any</a></li>
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-filter=".oncopeer">OncoPeer</a></li>
              <li><apex:outputLink html-data-filter=".developerCloud" onclick="activitySortFilterOptionClicked(this)" value="#" rendered="{!isDCMember || isCommunityManager || isDCContributor}">Developer Cloud</apex:outputLink></li>
            </ul>
          </div>            
          <div class="btn-group filters" data-filter-group="action">
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="btn-group-label">Type:</span><span class="selection">All</span> <span class="caret"></span></button>
            <ul id="activity-fltr-type-dropdown" class="dropdown-menu">
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-filter="*">All</a></li>
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-filter=".shared">Shared</a></li>
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-filter=".commented">Commented</a></li>
              <li><apex:outputLink html-data-filter=".pending" onclick="activitySortFilterOptionClicked(this)" value="#" rendered="{!IF(profiledUserId == loggedinUser.Id, TRUE, FALSE)}">Pending</apex:outputLink></li>
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-filter=".followed">Followed</a></li>
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-filter=".group">Group</a></li>
            </ul>
          </div>
          <div class="btn-group pull-right sorting">
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="btn-group-label">Sort by:</span><span class="selection">Date</span> <span class="caret"></span></button>
            <ul id="activity-sort-dropdown" class="dropdown-menu">
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-sort-by="original-order">Date</a></li>
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-sort-by="likes">Likes</a></li>
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-sort-by="views">Views</a></li>
              <li><a href="#" onclick="activitySortFilterOptionClicked(this)" data-sort-by="comments">Comments</a></li>
            </ul>
          </div>
        </apex:outputPanel>
      </apex:outputPanel>  

      <apex:outputPanel id="activities" StyleClass="activities" layout="block">
        <div class="activity-sizer"></div>
        <apex:actionStatus id="loadingActivitiesStatus">
          <apex:facet name="start">
            <div class="activity-item" style="height:100%;opacity:1;position:absolute;z-index:50000;background-color:#d5d5d5;">
            <div class="progress col-md-4 col-md-offset-4" style="padding:0px; margin-top:20px;">
              <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                Loading...
              </div>
            </div>
            </div>
          </apex:facet>
          <apex:facet name="stop">
            <apex:outputPanel rendered="{!IF(lstProfileActivity.size==0, true, false)}">
              <div class="about" style="width:100%;">
                      <h3>No activity found.</h3>
              </div>
            </apex:outputPanel>
          </apex:facet>
        </apex:actionStatus>
        <apex:repeat value="{!lstProfileActivity}" var="activityDtl"> 
          <div class="panel panel-default activity-item {!activityDtl.className}">
            <div class="panel-body">

              <apex:outputpanel layout="block" styleClass="activity-icon pull-left" rendered="{!IF(activityDtl.Activitytype == 'ET',true,false)}">
                <a href="{!activityDtl.detailLink}"><img src="{!URLFOR($Resource.OCSUGC_Design, 'img/icon-event.png')}"/></a>
              </apex:outputPanel>
              <apex:outputpanel layout="block" styleClass="activity-icon pull-left" rendered="{!IF(activityDtl.Activitytype == 'DC',true,false)}">
                  <a href="{!activityDtl.detailLink}"><img src="{!URLFOR($Resource.OCSUGC_Design, 'img/icon-discussion.png')}"/></a>
              </apex:outputPanel>
              <apex:outputpanel layout="block" styleClass="activity-icon pull-left" rendered="{!IF(activityDtl.Activitytype == 'KA',true,false)}">
                  <a href="{!activityDtl.detailLink}"><img src="{!URLFOR($Resource.OCSUGC_Design, 'img/icon-knowledge.png')}" /></a>
              </apex:outputPanel>
              <apex:outputpanel layout="block" styleClass="activity-icon pull-left" rendered="{!IF(activityDtl.Activitytype == 'PL',true,false)}">
                  <a href="{!activityDtl.detailLink}"><img src="{!URLFOR($Resource.OCSUGC_Design, 'img/icon-poll.png')}" /></a>
              </apex:outputPanel>
              <apex:outputpanel layout="block" styleClass="activity-icon pull-left" rendered="{!IF(activityDtl.Activitytype == 'GP',true,false)}">
                  <a href="{!activityDtl.detailLink}"><img src="{!activityDtl.imageLink}" /></a>
              </apex:outputPanel>
              <apex:outputpanel layout="block" styleClass="activity-icon pull-left" rendered="{!IF(activityDtl.Activitytype == 'GM',true,false)}">
                  <a href="{!activityDtl.detailLink}"><img src="{!activityDtl.imageLink}" /></a>
              </apex:outputPanel>
 
              <div class="activity-content pull-left">

                <apex:outputPanel id="opSharedWith" styleClass="activity-type" rendered="{!IF(activityDtl.Activitytype != 'GP' && activityDtl.Activitytype != 'GM' && activityDtl.sharedWithGroupName!='' && NOT(activityDtl.isCommentedByCurrentUser),true,false)}">
                  {!$Label.OCSUGC_Label_Shared_With} <span class="activity-shared-with">{!activityDtl.sharedWithGroupName}</span>
                </apex:outputPanel>
                <apex:outputPanel id="opCommented"  styleClass="activity-type" rendered="{!activityDtl.isCommentedByCurrentUser}">
                  {!$Label.OCSUGC_Label_Commented_On} post in <span class="activity-shared-with">{!activityDtl.sharedWithGroupName}</span>
                </apex:outputPanel>
                <apex:outputPanel id="opFollowed"  styleClass="activity-type" rendered="{!IF(activityDtl.Activitytype == 'GM',true,false)}">
                    {!$Label.OCSUGC_Label_Followed}
                </apex:outputPanel>
                <apex:outputPanel id="opCreated"  styleClass="activity-type" rendered="{!IF(activityDtl.Activitytype == 'GP' && activityDtl.isGroupOwner,true,false)}">
                    {!$Label.OCSUGC_Label_Created}
                </apex:outputPanel>
                <apex:outputPanel id="opJoined"  styleClass="activity-type" rendered="{!IF(activityDtl.Activitytype == 'GP' && !activityDtl.isGroupOwner,true,false)}">
                    Joined
                </apex:outputPanel>

                <h2><a href="{!activityDtl.detailLink}"><apex:outputText escape="false" value="{!activityDtl.title}"/></a></h2>

              </div>          
            </div>

            <div class="panel-footer clearfix">              
              <div class="date pull-left">
                {!activityDtl.relativeCreatedTime}
              </div>
              <div class="pull-right" style="display:{!IF( (activityDtl.countComments == -1), 'none', 'block') }">
                <span class="like-icon"></span> <span class="likes">{!activityDtl.countLikes}</span>
                <span class="comment-icon"></span> <span class="comments">{!activityDtl.countComments}</span>
                <span class="view-icon"></span> <span class="views">{!activityDtl.countViewed}</span>
              </div>
            </div>

          </div>
        </apex:repeat>
      
      </apex:outputPanel>
    </div>
  </div>
</apex:form>

  <script type="text/javascript">
    //click of dropdown-menu options
    function activitySortFilterOptionClicked( obj ) {
      $(obj).parents('.btn-group').find('.selection').text($(obj).text());
      $(obj).parents('.btn-group').find('.selection').val($(obj).text());
    };

    function concatActivityFilterValues( obj ) {
      var value = '';
      for ( var prop in obj ) {
        if( obj[ prop ] != '*')
          value += obj[ prop ];
      }
      return value;
    } 
    //Filtering and sorting for groups
    $('.nav-tabs a[href="\\#activity"]').on('shown.bs.tab', function (e) {
      loadProfileActivities();
    });

    function drawActivityGrid() {
      //init Isotope
      var $activities = $('.activities').isotope({
        // options
        itemSelector: '.activity-item',
        percentPosition: true,
        layoutMode: 'vertical',

        getSortData: {
          likes: '.likes parseInt',
          views: '.views parseInt',
          comments: '.comments parseInt'
        },

        sortAscending: {
          likes: false,
          views: false,
          comments: false
        }
      });

      // store filter for each group
      var activityFilters = {};

      $('#activity .filters').on( 'click', '.dropdown-menu li a', function() {
        var $this = $(this);
        // get group key
        var $buttonGroup = $this.parents('.btn-group');
        var filterGroup = $buttonGroup.attr('data-filter-group');
        // set filter for group
        activityFilters[ filterGroup ] = $this.attr('data-filter');
        // combine filters
        var filterValue = concatActivityFilterValues( activityFilters );
        // set filter for Isotope
        $activities.isotope({ filter: filterValue });
      });

      // change active class on buttons
      $('#activity .btn-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
          $buttonGroup.find('.active').removeClass('active');
          $( this ).addClass('active');
        });
      });

      // sort items on button click
      $('#activity .sorting').on( 'click', '.dropdown-menu li a', function() {
        var sortByValue = $(this).attr('data-sort-by');
        $activities.isotope({ sortBy: sortByValue});
      });         
    }

  </script>
</apex:component>
<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stores the Acceptance date related to Installed Product
US2768</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>AccHist_Installation_Date__c</fullName>
        <externalId>false</externalId>
        <label>Installation Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Acceptance_Certificate_Link__c</fullName>
        <externalId>false</externalId>
        <label>Acceptance Certificate Link</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Acceptance_Date__c</fullName>
        <externalId>false</externalId>
        <label>Acceptance Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Acceptance_History_Link__c</fullName>
        <externalId>false</externalId>
        <label>Acceptance History Link</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Document_ID__c</fullName>
        <description>US 3870</description>
        <externalId>false</externalId>
        <label>Document ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ERP_Sales_Order_Item__c</fullName>
        <description>Updated Field label as per US2741</description>
        <externalId>false</externalId>
        <label>ERP Sales Order Item</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ERP_Sales_Order__c</fullName>
        <description>Updated label as per US2741</description>
        <externalId>false</externalId>
        <label>ERP Sales Order</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ERP_WBS__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>US2741</description>
        <externalId>false</externalId>
        <label>ERP WBS</label>
        <referenceTo>ERP_WBS__c</referenceTo>
        <relationshipLabel>Acceptance Date Histories</relationshipLabel>
        <relationshipName>Acceptance_Date_Histories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Installed_Product__c</fullName>
        <externalId>false</externalId>
        <label>Installed Product</label>
        <referenceTo>SVMXC__Installed_Product__c</referenceTo>
        <relationshipLabel>Acceptance Date Histories</relationshipLabel>
        <relationshipName>Acceptance_Date_Histories</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <description>US 3870</description>
        <externalId>false</externalId>
        <label>Notes</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Sales_Order_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>US2741</description>
        <externalId>false</externalId>
        <label>Sales Order Item</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Sales_Order_Item__c.Sales_Order__c</field>
                <operation>equals</operation>
                <valueField>$Source.Sales_Order__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Sales_Order_Item__c</referenceTo>
        <relationshipLabel>Acceptance Date Histories</relationshipLabel>
        <relationshipName>Acceptance_Date_Histories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Sales_Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Sales Order</label>
        <referenceTo>Sales_Order__c</referenceTo>
        <relationshipLabel>Acceptance Date Histories</relationshipLabel>
        <relationshipName>Acceptance_Date_Histories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Acceptance Date History</label>
    <nameField>
        <displayFormat>AD-{000000}</displayFormat>
        <label>Acceptance Date History No</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Acceptance Date Histories</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>

<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <compactLayouts>
        <fullName>Varian_Invoice_Compact_Layout</fullName>
        <fields>Name</fields>
        <fields>Sales_Service_Type__c</fields>
        <fields>Sold_To__c</fields>
        <fields>Amount__c</fields>
        <fields>Invoice_Date__c</fields>
        <fields>Invoice_Due_Date__c</fields>
        <fields>Invoice_Cleared_Date__c</fields>
        <label>Varian_Invoice_Compact_Layout</label>
    </compactLayouts>
    <fields>
        <fullName>Cancelled__c</fullName>
        <defaultValue>false</defaultValue>
        <description>invoice is Cancelled in SAP</description>
        <externalId>false</externalId>
        <inlineHelpText>invoice is Cancelled in SAP</inlineHelpText>
        <label>Cancelled</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Current_User_is_Sales_Manager__c</fullName>
        <description>Checked true if the current user is the Sales Manager</description>
        <externalId>false</externalId>
        <formula>IF($User.Id = District_Sales_Manager__r.Id,true,false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Checked true if the current user is the Sales Manager</inlineHelpText>
        <label>Current User is Sales Manager</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Current_User_is_Service_Manager__c</fullName>
        <description>Checked true if the current user is the Service Manager</description>
        <externalId>false</externalId>
        <formula>IF($User.Id = District_Service_Manager__r.Id,true,false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Checked true if the current user is the Service Manager</inlineHelpText>
        <label>Current User is Service Manager</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Display_in_Portal__c</fullName>
        <description>If Invoice is Cancelled, or is Invoice Type of Cancellation, then it should not show in Portal.  If Amount if 0.00, then it also should not be shown.</description>
        <externalId>false</externalId>
        <formula>AND(
Cancelled__c == false,
Amount__c != 0.00,
OR( 
ISPICKVAL(Doc_Type__c,&apos;AB&apos;),
ISPICKVAL(Doc_Type__c,&apos;DR&apos;),
ISPICKVAL(Doc_Type__c,&apos;ZF01&apos;),
ISPICKVAL(Doc_Type__c,&apos;ZHU&apos;),
ISPICKVAL(Doc_Type__c,&apos;ZHX&apos;),
ISPICKVAL(Doc_Type__c,&apos;ZHBC&apos;),
ISPICKVAL(Doc_Type__c,&apos;ZHBB&apos;),
ISPICKVAL(Doc_Type__c,&apos;C1&apos;),
ISPICKVAL(Doc_Type__c,&apos;DZ&apos;),
ISPICKVAL(Doc_Type__c,&apos;ZV&apos;),
ISPICKVAL(Doc_Type__c,&apos;ZHBD&apos;),
ISPICKVAL(Doc_Type__c,&apos;ZHW&apos;),
ISPICKVAL(Doc_Type__c,&apos;ZHY&apos;)
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>If Invoice is Cancelled, or is Invoice Type of Cancellation, then it should not show in Portal.  If Amount if 0.00, then it also should not be shown.</inlineHelpText>
        <label>Display in Portal</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Doc_Type__c</fullName>
        <description>Document Type for Billing Document or Accounting Document</description>
        <externalId>false</externalId>
        <inlineHelpText>Document Types for Billing/Accounting.  
Following are Invoice Cancellations:  ZS1A, ZS1C, ZS1D
Following are Credit Memos:  ZHBD, ZHY, ZHW
Following are Credit Memo Cancellations:  ZS2B, ZS2C, ZS2D
Following are Payment Docs:  DZ, ZV, ZX</inlineHelpText>
        <label>Doc Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>C1</fullName>
                    <default>false</default>
                    <label>C1</label>
                </value>
                <value>
                    <fullName>ZF01</fullName>
                    <default>false</default>
                    <label>ZF01</label>
                </value>
                <value>
                    <fullName>ZHBB</fullName>
                    <default>false</default>
                    <label>ZHBB</label>
                </value>
                <value>
                    <fullName>ZHBC</fullName>
                    <default>false</default>
                    <label>ZHBC</label>
                </value>
                <value>
                    <fullName>ZHBD</fullName>
                    <default>false</default>
                    <label>ZHBD</label>
                </value>
                <value>
                    <fullName>ZHU</fullName>
                    <default>false</default>
                    <label>ZHU</label>
                </value>
                <value>
                    <fullName>ZHW</fullName>
                    <default>false</default>
                    <label>ZHW</label>
                </value>
                <value>
                    <fullName>ZHX</fullName>
                    <default>false</default>
                    <label>ZHX</label>
                </value>
                <value>
                    <fullName>ZHY</fullName>
                    <default>false</default>
                    <label>ZHY</label>
                </value>
                <value>
                    <fullName>ZS1A</fullName>
                    <default>false</default>
                    <label>ZS1A</label>
                </value>
                <value>
                    <fullName>ZS1C</fullName>
                    <default>false</default>
                    <label>ZS1C</label>
                </value>
                <value>
                    <fullName>ZS1D</fullName>
                    <default>false</default>
                    <label>ZS1D</label>
                </value>
                <value>
                    <fullName>ZS2B</fullName>
                    <default>false</default>
                    <label>ZS2B</label>
                </value>
                <value>
                    <fullName>ZS2C</fullName>
                    <default>false</default>
                    <label>ZS2C</label>
                </value>
                <value>
                    <fullName>ZS2D</fullName>
                    <default>false</default>
                    <label>ZS2D</label>
                </value>
                <value>
                    <fullName>ZV</fullName>
                    <default>false</default>
                    <label>ZV</label>
                </value>
                <value>
                    <fullName>AB</fullName>
                    <default>false</default>
                    <label>AB</label>
                </value>
                <value>
                    <fullName>DZ</fullName>
                    <default>false</default>
                    <label>DZ</label>
                </value>
                <value>
                    <fullName>DR</fullName>
                    <default>false</default>
                    <label>DR</label>
                </value>
                <value>
                    <fullName>UD</fullName>
                    <default>false</default>
                    <label>UD</label>
                </value>
                <value>
                    <fullName>ZX</fullName>
                    <default>false</default>
                    <label>ZX</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Invoice_Cancelled__c</fullName>
        <description>Identifies the Invoice this Document cancels.</description>
        <externalId>false</externalId>
        <inlineHelpText>Identifies the Invoice this Document cancels.</inlineHelpText>
        <label>Invoice Cancelled</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <listViews>
        <fullName>My_Customer_Unpaid_Invoices</fullName>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <columns>NAME</columns>
        <columns>ERP_Sold__c</columns>
        <columns>Sold_To__c</columns>
        <columns>Invoice_Date__c</columns>
        <columns>Invoice_Due_Date__c</columns>
        <columns>Days_Past_Due__c</columns>
        <columns>Amount__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Current_User_is_Sales_Manager__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>Current_User_is_Service_Manager__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Unpaid</value>
        </filters>
        <label>My Customer Unpaid Invoices</label>
        <language>en_US</language>
    </listViews>
</CustomObject>

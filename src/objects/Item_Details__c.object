<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>SR_ItemDetail</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This Object is child to Product Versions and is used to store product model details for different versions of product.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Business_Unit__c</fullName>
        <externalId>false</externalId>
        <formula>Text(Review_Product__r.Business_Unit__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Business Unit</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Clearance_Family__c</fullName>
        <externalId>false</externalId>
        <formula>Text(Review_Product__r.Clearance_Family__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Clearance Family</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Item_Level__c</fullName>
        <externalId>false</externalId>
        <label>Item Level</label>
        <picklist>
            <picklistValues>
                <fullName>Sub-Model</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sub-item</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>3rd Party</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Device</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Exempt WW</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Model__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Model</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Product2.Description</field>
                <operation>equals</operation>
                <value>BI PRODUCTS</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Item Details (Model)</relationshipLabel>
        <relationshipName>Item_Details5</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Item Details (Product)</relationshipLabel>
        <relationshipName>Item_Details1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Item Details (Product)</relationshipLabel>
        <relationshipName>Item_Details2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product3__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Item Details (Product)</relationshipLabel>
        <relationshipName>Item_Details3</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product5__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Item Details (Product)</relationshipLabel>
        <relationshipName>Item_Details4</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Product__r.ProductCode + 
IF(ISBLANK(Product1__r.ProductCode),&apos;&apos;,(&apos;,&apos; + Product1__r.ProductCode)) + 
IF(ISBLANK(Product2__r.ProductCode),&apos;&apos;,(&apos;,&apos; + Product2__r.ProductCode)) +
IF(ISBLANK(Product3__r.ProductCode),&apos;&apos;,(&apos;,&apos; + Product3__r.ProductCode)) +
IF(ISBLANK(Product5__r.ProductCode),&apos;&apos;,(&apos;,&apos; + Product5__r.ProductCode))</formula>
        <label>Product Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Profile_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Review_Product__r.Product_Profile_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Profile Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Item Details</relationshipLabel>
        <relationshipName>Item_Details</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Regulatory_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Review_Product__r.Regulatory_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Regulatory Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Review_Product__c</fullName>
        <externalId>false</externalId>
        <label>Product Profile</label>
        <referenceTo>Review_Products__c</referenceTo>
        <relationshipLabel>Item Details</relationshipLabel>
        <relationshipName>Model_Sub_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Version_No__c</fullName>
        <externalId>false</externalId>
        <formula>Review_Product__r.Version_No__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Version No.</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Item Details</label>
    <nameField>
        <label>Item Part</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Item Details</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Version_No__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Regulatory_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Business_Unit__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Clearance_Family__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Version_No__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Regulatory_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Business_Unit__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Clearance_Family__c</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>Version_No__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Regulatory_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Business_Unit__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Clearance_Family__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>

<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Varian wants to provide a close loop process by which customers can access their lung phantom voucher (based on their purchase of an eligible product) so that they can redeem it via MD Anderson.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Account_iD_del__c</fullName>
        <externalId>false</externalId>
        <formula>Contact__r.AccountId</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account  ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact_FirstName__c</fullName>
        <externalId>false</externalId>
        <formula>Contact__r.FirstName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Contact FirstName</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact_LastName__c</fullName>
        <externalId>false</externalId>
        <formula>Contact__r.LastName</formula>
        <label>Contact LastName</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Lung Phantoms</relationshipLabel>
        <relationshipName>Lung_Phantoms</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Date_Exposed_Phantom_Received_by_MDADL__c</fullName>
        <externalId>false</externalId>
        <label>Date Exposed  Phantom Received</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Date_Received__c</fullName>
        <externalId>false</externalId>
        <label>Acceptance Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Date_Redeemed__c</fullName>
        <externalId>false</externalId>
        <label>Date Redeemed</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Date_Results_Reported_by_MDADL__c</fullName>
        <externalId>false</externalId>
        <label>Date Results Reported</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Expiration_Date_Ends__c</fullName>
        <externalId>false</externalId>
        <formula>if(ISBLANK(Date_Received__c),Installed_Product_ExDate__c  + 365, Date_Received__c +365)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Expiration Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Installed_Product_ExDate__c</fullName>
        <externalId>false</externalId>
        <formula>Installed_Product__r.SVMXC__Date_Installed__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Installed Product ExDate</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Installed_Product__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Installed Product</label>
        <referenceTo>SVMXC__Installed_Product__c</referenceTo>
        <relationshipLabel>Lung Phantoms</relationshipLabel>
        <relationshipName>Lung_Phantoms</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Institution__c</fullName>
        <externalId>false</externalId>
        <formula>Installed_Product__r.SVMXC__Company__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Institution</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Installed_Product__r.SVMXC__Product_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Redeemed_By__c</fullName>
        <externalId>false</externalId>
        <label>Redeemed By</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Serial_Lot_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Installed_Product__r.SVMXC__Serial_Lot_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>PCSN (Serial/Lot Number)</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Voucher_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>false</externalId>
        <label>Voucher ID</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Lung Phantom</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Lung  Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Lung Phantoms</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Voucher_ID__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Institution__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Installed_Product__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Product_Name__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>CP_IP_from_Accounts_Contact</fullName>
        <active>false</active>
        <errorConditionFormula>Contact__r.AccountId &lt;&gt;  Installed_Product__r.SVMXC__Company__r.Id</errorConditionFormula>
        <errorDisplayField>Installed_Product__c</errorDisplayField>
        <errorMessage>Installed product selected should be from the Account associated to Contact.</errorMessage>
    </validationRules>
</CustomObject>

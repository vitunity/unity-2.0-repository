<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>Default_Service_Price__c</fullName>
        <description>SAP Default Service Price for 0600/10 - Condition PR00</description>
        <externalId>false</externalId>
        <inlineHelpText>SAP Default Service Price for 0600/10 - Condition PR00.  Price is not applicable where organization or customer specific pricing is in effect.</inlineHelpText>
        <label>Base Service Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>PSE_Approval_Requirements__c</fullName>
        <description>Field to be maintained by PSE. This field will house the approval requirements to be considered when ordering and approving a part in the field.</description>
        <externalId>false</externalId>
        <inlineHelpText>Instructions entered by PSE governing the review and approval of orders for this part.</inlineHelpText>
        <label>PSE Approval Requirements</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Product_Group__c</fullName>
        <externalId>false</externalId>
        <label>Product Group</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>360 Oncology</fullName>
                    <default>false</default>
                    <label>360 Oncology</label>
                </value>
                <value>
                    <fullName>4D Integrated Treatment Console (4DITC)</fullName>
                    <default>false</default>
                    <label>4D Integrated Treatment Console (4DITC)</label>
                </value>
                <value>
                    <fullName>Acuity</fullName>
                    <default>false</default>
                    <label>Acuity</label>
                </value>
                <value>
                    <fullName>Argus</fullName>
                    <default>false</default>
                    <label>Argus</label>
                </value>
                <value>
                    <fullName>ARIA Connect</fullName>
                    <default>false</default>
                    <label>ARIA Connect</label>
                </value>
                <value>
                    <fullName>ARIA Medical Oncology</fullName>
                    <default>false</default>
                    <label>ARIA Medical Oncology</label>
                </value>
                <value>
                    <fullName>ARIA Notification</fullName>
                    <default>false</default>
                    <label>ARIA Notification</label>
                </value>
                <value>
                    <fullName>ARIA Radiation Oncology</fullName>
                    <default>false</default>
                    <label>ARIA Radiation Oncology</label>
                </value>
                <value>
                    <fullName>Brachytherapy Applicators/Accessories</fullName>
                    <default>false</default>
                    <label>Brachytherapy Applicators/Accessories</label>
                </value>
                <value>
                    <fullName>BrachyVision</fullName>
                    <default>false</default>
                    <label>BrachyVision</label>
                </value>
                <value>
                    <fullName>Calypso</fullName>
                    <default>false</default>
                    <label>Calypso</label>
                </value>
                <value>
                    <fullName>Citrix</fullName>
                    <default>false</default>
                    <label>Citrix</label>
                </value>
                <value>
                    <fullName>Clinac</fullName>
                    <default>false</default>
                    <label>Clinac</label>
                </value>
                <value>
                    <fullName>DoseLab</fullName>
                    <default>false</default>
                    <label>DoseLab</label>
                </value>
                <value>
                    <fullName>Eclipse</fullName>
                    <default>false</default>
                    <label>Eclipse</label>
                </value>
                <value>
                    <fullName>Edge</fullName>
                    <default>false</default>
                    <label>Edge</label>
                </value>
                <value>
                    <fullName>GammaMed</fullName>
                    <default>false</default>
                    <label>GammaMed</label>
                </value>
                <value>
                    <fullName>Halcyon</fullName>
                    <default>false</default>
                    <label>Halcyon</label>
                </value>
                <value>
                    <fullName>HARRP</fullName>
                    <default>false</default>
                    <label>HARRP</label>
                </value>
                <value>
                    <fullName>Information Exchange Manager (IEM)</fullName>
                    <default>false</default>
                    <label>Information Exchange Manager (IEM)</label>
                </value>
                <value>
                    <fullName>Insightive</fullName>
                    <default>false</default>
                    <label>Insightive</label>
                </value>
                <value>
                    <fullName>MLC</fullName>
                    <default>false</default>
                    <label>MLC</label>
                </value>
                <value>
                    <fullName>Mobius3D</fullName>
                    <default>false</default>
                    <label>Mobius3D</label>
                </value>
                <value>
                    <fullName>Novalis Tx</fullName>
                    <default>false</default>
                    <label>Novalis Tx</label>
                </value>
                <value>
                    <fullName>On-Board Imager (OBI)</fullName>
                    <default>false</default>
                    <label>On-Board Imager (OBI)</label>
                </value>
                <value>
                    <fullName>Optical Guidance</fullName>
                    <default>false</default>
                    <label>Optical Guidance</label>
                </value>
                <value>
                    <fullName>OSMS</fullName>
                    <default>false</default>
                    <label>OSMS</label>
                </value>
                <value>
                    <fullName>PortalVision</fullName>
                    <default>false</default>
                    <label>PortalVision</label>
                </value>
                <value>
                    <fullName>ProBeam</fullName>
                    <default>false</default>
                    <label>ProBeam</label>
                </value>
                <value>
                    <fullName>Proton</fullName>
                    <default>false</default>
                    <label>Proton</label>
                </value>
                <value>
                    <fullName>Queue</fullName>
                    <default>false</default>
                    <label>Queue</label>
                </value>
                <value>
                    <fullName>RapidArc</fullName>
                    <default>false</default>
                    <label>RapidArc</label>
                </value>
                <value>
                    <fullName>Real-time Position Management (RPM)</fullName>
                    <default>false</default>
                    <label>Real-time Position Management (RPM)</label>
                </value>
                <value>
                    <fullName>Respiratory Gating for Scanners (RGSC)</fullName>
                    <default>false</default>
                    <label>Respiratory Gating for Scanners (RGSC)</label>
                </value>
                <value>
                    <fullName>Solaro</fullName>
                    <default>false</default>
                    <label>Solaro</label>
                </value>
                <value>
                    <fullName>Trilogy</fullName>
                    <default>false</default>
                    <label>Trilogy</label>
                </value>
                <value>
                    <fullName>TrueBeam</fullName>
                    <default>false</default>
                    <label>TrueBeam</label>
                </value>
                <value>
                    <fullName>UNIQUE</fullName>
                    <default>false</default>
                    <label>UNIQUE</label>
                </value>
                <value>
                    <fullName>Varian Exchange</fullName>
                    <default>false</default>
                    <label>Varian Exchange</label>
                </value>
                <value>
                    <fullName>Varian Treatment</fullName>
                    <default>false</default>
                    <label>Varian Treatment</label>
                </value>
                <value>
                    <fullName>VariSeed</fullName>
                    <default>false</default>
                    <label>VariSeed</label>
                </value>
                <value>
                    <fullName>VariSource</fullName>
                    <default>false</default>
                    <label>VariSource</label>
                </value>
                <value>
                    <fullName>VARiS Vision</fullName>
                    <default>false</default>
                    <label>VARiS Vision</label>
                </value>
                <value>
                    <fullName>Velocity Software</fullName>
                    <default>false</default>
                    <label>Velocity Software</label>
                </value>
                <value>
                    <fullName>VitalBeam</fullName>
                    <default>false</default>
                    <label>VitalBeam</label>
                </value>
                <value>
                    <fullName>Vitesse</fullName>
                    <default>false</default>
                    <label>Vitesse</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Serialized_labeled__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Serialized-labeled</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
</CustomObject>

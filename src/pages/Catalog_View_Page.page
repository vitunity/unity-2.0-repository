<apex:page controller="CatalogViewController" docType="html-5.0">
  <apex:outputPanel rendered="{!NOT(isLightning)}">
  <apex:includeScript value="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"/>
  <apex:includeScript value="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"/>
  <apex:stylesheet value="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
  <apex:form id="catalogForm">
    <apex:pageBlock id="in" title="Catalog View">
      <apex:pageMessages />
      <apex:pageBlockButtons location="top" >
        <!-- <apex:commandButton value="Search" action="{!quickSearch}" rerender="out, in" status="searchStatus"/> -->
        <apex:actionFunction name="quickSearch" action="{!quickSearch}" rerender="out, in" status="searchStatus"/>
        <apex:actionFunction name="updateFilter" action="{!updateFilter}" rerender="in"/>
        <apex:actionFunction name="updateSelectedGroups" action="{!updateSelectedGroups}" rerender="out" status="groupStatus"/>
        <apex:actionFunction name="filterDiscountable" action="{!filterDiscountable}" rerender="out"/>
      </apex:pageBlockButtons>
      <apex:pageBlockSection id="catSearch" columns="1">
        <apex:pageBlockSection columns="1">
          <apex:pageBlockSectionItem helpText="Explore the catalog by choosing a Product Family, Product Line, and Model">
            <apex:outputLabel value="Product Family"/>
            <apex:selectList label="Product Family" id="family" value="{!selectedFamily}" size="1" onchange="updateFilter()">
              <apex:selectOptions value="{!familyOptions}"/>
            </apex:selectList>
          </apex:pageBlockSectionItem>
          <apex:selectList label="Product Line" id="line" value="{!selectedLine}" size="1"  onchange="updateFilter()">
            <apex:selectOptions value="{!lineOptions}"/>
          </apex:selectList>

          <apex:selectList label="Model" id="model" value="{!selectedModel}" size="1" onchange="quickSearch()">
            <apex:selectOptions value="{!modelOptions}"/>
          </apex:selectList>
        </apex:pageBlockSection>

        
        <apex:pageBlockSectionItem >
          <apex:outputLabel value=" - OR - "/>
          <apex:outputPanel layout="block"></apex:outputPanel>
        </apex:pageBlockSectionItem>
        

        <apex:pageBlockSectionItem id="catSearchBlock"  helpText="Search by part number or keyword">
          
          <apex:outputLabel value="Search Catalog"/>
          <apex:outputPanel layout="block">
            <apex:input id="catalogPartSearchInput" value="{!catalogPartSearch}" label="Search Catalog" />
            <apex:commandButton value="Search" action="{!quickSearch}" rerender="out, in" status="searchStatus" style="padding:2px 4px; margin-left: 5px;"/>
            <apex:outputPanel rendered="{!(searchedParts.size != 0)}">
              <a href="#" onclick="clearSearch()">Clear Search</a>
            </apex:outputPanel>
          </apex:outputPanel>    
          
        </apex:pageBlockSectionItem>
        <apex:inputCheckbox label="Search By Price" value="{!searchByPrice}" rendered="{!isCatalogAdmin}" style="margin-bottom:15px;"/>
        <!-- <apex:outputLabel value="{!SelectedRegion} {!selectedFamily} {!selectedLine} {!selectedModel}"/> -->
        <!-- <apex:pageBlockSectionItem id="catSearchBlock"  helpText="Search by part number or keyword">
          <apex:outputLabel value="Search Catalog"/>
          <apex:pageBlockSectionItem >
            <apex:input id="catalogPartSearchInput" value="{!catalogPartSearch}" label="Search Catalog" />
            <apex:commandButton value="Search" action="{!quickSearch}" rerender="out, in" status="searchStatus"/>
          </apex:pageBlockSectionItem>
        </apex:pageBlockSectionItem> -->
      </apex:pageBlockSection>
     
    </apex:pageBlock>

    <apex:pageBlock id="out" title="{!selectedModel}">
      <apex:pageBlockButtons location="top" >
        <apex:commandButton value="Export" action="{!exportToExcel}" rendered="{!(isCatalogAdmin && groupOptions.size > 0)}"/>
        <apex:commandButton value="Export All" action="{!exportAllToExcel}" rendered="{!(isCatalogAdmin && groupOptions.size > 0)}"/> 
        <apex:commandButton value="Export" action="{!searchExportToExcel}" rendered="{!(isCatalogAdmin && searchedParts.size > 0)}"/>
      </apex:pageBlockButtons>
      <apex:actionStatus id="searchStatus" onstart="showSearching();" onstop="hideSearching();"/>
      <apex:pageBlockSection >
        <apex:selectList label="Currency" id="currencies" value="{!selectedCurrency}" size="1" required="true" onchange="quickSearch()">
          <apex:selectOptions value="{!currencyOptions}"/>
        </apex:selectList>
        <apex:pageblockSectionItem >
          <!-- <apex:outputLabel value="Region"/>
          <apex:pageblockSectionItem >
            <apex:outputText value="{!selectedRegion}" rendered="{!(!isCatalogAdmin && !hasMultipleRegions)}"/>
            <apex:selectList id="regions" value="{!selectedRegion}" size="1" onchange="quickSearch()" rendered="{!isCatalogAdmin || hasMultipleRegions}">
              <apex:selectOptions value="{!regionOptions}"/>
            </apex:selectList>
          </apex:pageBlockSectionItem> -->
          <apex:outputLabel value="Country"/> 
          <apex:pageblockSectionItem >
            <apex:outputText value="{!selectedCountry}" rendered="{!(!isCatalogAdmin && !hasMultipleRegions)}"/>
            <apex:selectList id="countries" value="{!selectedCountry}" size="1" onchange="quickSearch()" rendered="{!isCatalogAdmin || hasMultipleRegions}">
              <apex:selectOptions value="{!countryOptions}"/>
            </apex:selectList>
          </apex:pageBlockSectionItem>
        </apex:pageBlockSectionItem>
        <apex:selectList label="Pricebook" id="pricebooks" value="{!selectedPricebook}" size="1" required="true" onchange="quickSearch()" rendered="{!selectedRegion=='NA'}">
          <apex:selectOptions value="{!pricebookOptions}"/>
        </apex:selectList>
        <apex:inputCheckbox label="Discountable Only" value="{!showDiscountable}" onchange="filterDiscountable()"/>
      </apex:pageBlockSection>
      <apex:outputPanel >
        <!-- <p>You have selected: {!selectedPricebook} | {!selectedRegion} | {!selectedFamily} | {!selectedLine} | {!selectedModel} | {!selectedCurrency} | {!selectedConversionRate}</p>
        <p>{!selectedCatalogPartId}</p> --> 
      </apex:outputPanel>
      <div id="contentLoading" style="display:none;">
        <div class='loadingMsg' style="text-align: center;">
          <div class="var-spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
          </div>
        </div>
      </div>
      <div id="partsOutputSection">
        <apex:actionStatus id="groupStatus" onstart="showGroupSearching();" onstop="hideGroupSearching();"/>
        <apex:panelGrid columns="2" columnClasses="groups, partsTable" rendered="{!(groupOptions.size > 0)}">
          <apex:pageBlockSection >
            <apex:selectList label="Group" id="groups" value="{!selectedGroup}" multiselect="true" onchange="updateSelectedGroups()">
              <apex:selectOptions value="{!groupOptions}"/>
            </apex:selectList>
          </apex:pageBlockSection>
          <apex:pageBlockSection columns="1">
            <apex:outputPanel layout="block">
              <div id="contentLoadingParts" style="display:none;">
                <div class='loadingMsg' style="text-align: center;">
                  <div class="var-spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                  </div>
                </div>
              </div>
              <div id="groupedPartsTables">
                <apex:repeat value="{!selectedGroup}" var="group" id="partsTableSection">
                  <apex:outputPanel layout="block" style="margin-bottom:15px;">
                    <div style="margin-bottom:5px;">
                      <h2><apex:outputText value="{!group}"/></h2>
                    </div>
                    <apex:pageBlockTable value="{!groupedPartsMap[group]}" var="part" id="parts_table" style="width:100%" width="100%">
                      <apex:column headerValue="Description" style="cursor:pointer; width:50%;">
                        <apex:actionSupport action="{!selectRow}" event="onclick" rerender="partInfoModal" status="partInfoStatus">
                          <apex:param name="selProduct" value="{!part.catalogPart.Id}" assignTo="{!selectedCatalogPartId}" />
                        </apex:actionSupport>
                        <a href="#" onclick="openPartInfoDialog(this)">
                          <apex:outputText value="{!part.product.Name}"/>
                        </a>
                      </apex:column>
                      
                      <!-- Ajinkya -->
                       <apex:column headerValue="Advantage Credits" rendered="{!isNARegion}" headerClass="priceHeader" styleClass="prices">
                         <apex:outputText value="{!part.catalogPart.Credits__c}"/>
                      </apex:column>
                      
                      <apex:column headerValue="Part" value="{!part.product.BigMachines__Part_Number__c}" />
                      <apex:column headerValue="Standard Price ({!selectedCurrency})" rendered="{!!isNARegion}" headerClass="priceHeader" styleClass="prices">
                        <apex:outputText value="{0, number,###,###,##0.00}">
                          <apex:param value="{!ROUND(part.standardPrice*selectedConversionRate, 0)}" />
                        </apex:outputText>
                      </apex:column>
                      
                      <!-- Commented by Shital -->
                      <!-- <apex:column headerValue="Goal Price ({!selectedCurrency})" rendered="{!!displayPricebookStd}" headerClass="priceHeader" styleClass="prices">
                        <apex:outputText value="{0, number,###,###,##0.00}">
                          <apex:param value="{!ROUND(part.goalPrice*selectedConversionRate, 0)}" />
                        </apex:outputText>
                      </apex:column> -->
                      <!-- Added by Shital START-->
                      
                      <!-- Added by Shital START-->
                      <apex:column headerValue="Target Price ({!selectedCurrency})" rendered="{!(!displayPricebookStd && !isNARegion)}" headerClass="priceHeader" styleClass="prices">
                        <apex:outputText value="{0, number,###,###,##0.00}">
                          <apex:param value="{!ROUND(part.targetPrice*selectedConversionRate, 0)}" />
                        </apex:outputText>
                      </apex:column>
                      <!-- Added by Shital END-->
                      <apex:column headerValue="{!selectedPriceBookName} Price ({!selectedCurrency})" rendered="{!displayPricebookStd}"  headerClass="priceHeader" styleClass="prices">
                        <apex:outputText value="{0, number,###,###,##0.00}">
                          <apex:param value="{!ROUND(part.pricebookPrice*selectedConversionRate, 0)}" />
                        </apex:outputText>
                      </apex:column>
                      <!-- Threshold price should only b displayed to a select few higher ups -->
                      <apex:column headerValue="Threshold Price ({!selectedCurrency})" rendered="{!(showThreshold && !isNARegion)}" headerClass="priceHeader" styleClass="prices">
                        <apex:outputText value="{0, number,###,###,##0.00}">
                          <apex:param value="{!ROUND(part.thresholdPrice*selectedConversionRate, 0)}" />
                        </apex:outputText>
                      </apex:column>
                    </apex:pageBlockTable>
                  </apex:outputPanel>
                </apex:repeat>
              </div>
            </apex:outputPanel>
          </apex:pageBlockSection>
        </apex:panelGrid>
        <apex:pageBlockSection rendered="{!(searchedParts.size != 0)}" columns="1">
          <apex:pageBlockTable value="{!searchedParts}" var="part" id="searched_parts_table" style="width:100%" width="100%" >
            <apex:column headerValue="Model" value="{!part.catalogPart.Model__c}" />
            <apex:column headerValue="Group" value="{!part.catalogPart.Group__c}" />
            <apex:column headerValue="Description" style="cursor:pointer;">
              <apex:actionSupport action="{!selectRow}" event="onclick" rerender="partInfoModal" status="partInfoStatus">
                <apex:param name="selProduct" value="{!part.catalogPart.Id}" assignTo="{!selectedCatalogPartId}" />
              </apex:actionSupport>
              <a href="#" onclick="openPartInfoDialog(this)">
                <apex:outputText value="{!part.product.Name}"/>
              </a>
            </apex:column>
            <apex:column headerValue="Part" value="{!part.product.BigMachines__Part_Number__c}" />
            <apex:column headerValue="Standard Price ({!selectedCurrency})" rendered="{!!isNARegion}" headerClass="priceHeader" styleClass="prices">
              <apex:outputText value="{0, number,###,###,##0.00}">
                <apex:param value="{!ROUND(part.standardPrice*selectedConversionRate, 0)}" />
              </apex:outputText>
            </apex:column>
            <!-- <apex:column headerValue="Goal Price ({!selectedCurrency})" rendered="{!!displayPricebookStd}" headerClass="priceHeader" styleClass="prices">
              <apex:outputText value="{0, number,###,###,##0.00}">
                <apex:param value="{!ROUND(part.goalPrice*selectedConversionRate, 0)}" />
              </apex:outputText>
            </apex:column> -->
            <!-- Added by Shital START-->
            <apex:column headerValue="Target Price ({!selectedCurrency})" rendered="{!(!displayPricebookStd && !isNARegion)}" headerClass="priceHeader" styleClass="prices">
              <apex:outputText value="{0, number,###,###,##0.00}">
                <apex:param value="{!ROUND(part.targetPrice*selectedConversionRate, 0)}" />
              </apex:outputText>
            </apex:column>
            <!-- Added by Shital END-->
            <apex:column headerValue="{!selectedPriceBookName} Price ({!selectedCurrency})" rendered="{!displayPricebookStd}" headerClass="priceHeader" styleClass="prices">
              <apex:outputText value="{0, number,###,###,##0.00}">
                <apex:param value="{!ROUND(part.pricebookPrice*selectedConversionRate, 0)}" />
              </apex:outputText>
            </apex:column>
            <!-- Threshold price should only b displayed to a select few higher ups -->
            <apex:column headerValue="Threshold Price ({!selectedCurrency})" rendered="{!(showThreshold && !isNARegion)}" headerClass="priceHeader" styleClass="prices">
              <apex:outputText value="{0, number,###,###,##0.00}">
                <apex:param value="{!ROUND(part.thresholdPrice*selectedConversionRate, 0)}" />
              </apex:outputText>
            </apex:column>
          </apex:pageBlockTable>
        </apex:pageBlockSection>
      </div>
    </apex:pageBlock>
  <script type="text/javascript">
    var partInfoModal;
    function openPartInfoDialog() {
        partInfoModal = new SimpleDialog("SD"+Dialogs.getNextId(), false);
        partInfoModal.setTitle("Product Information");
        partInfoModal.createDialog();
        partInfoModal.importContentNode(document.getElementById("{!$Component.catalogForm.partInfoModal}"));
        partInfoModal.setWidth(650);        
    }

    function hideModalLoading() {
      $('#contentLoadingMask').hide();
      $('#modalBody').show();
    }
    function hideInfoPanel() {
      $("body").removeClass("modal-open");
      partInfoModal.hide();
    }

    function showInfoPanel() {
      $("body").addClass("modal-open");
      partInfoModal.show();
    }

    function openEditWindow(catalogPartId) {
      window.open('/'+catalogPartId,'_blank');
    }

    function clearSearch() {
      var elem = $(document.getElementById("{!$Component.catalogForm.in.catSearch.catSearchBlock.catalogPartSearchInput}"));
      elem.val("");
      quickSearch();
    }

    function showSearching() {
      $('#contentLoading').show();
      $('#partsOutputSection').hide();
    }

    function hideSearching() {
      $('#contentLoading').hide();
      $('#partsOutputSection').show();
    }

    function showGroupSearching() {
      $('#contentLoadingParts').show();
      $('#groupedPartsTables').hide();
    }

    function hideGroupSearching() {
      $('#contentLoadingParts').hide();
      $('#groupedPartsTables').show();
    }
  </script>
   <apex:outputPanel style="display:none">
      <apex:outputPanel id="partInfoModal" layout="block" styleClass="part-info-modal">
        <apex:outputPanel >
          <!-- <apex:actionStatus id="partInfoStatus" startText="Getting Part Information" onstart="hideInfoPanel();" onstop="showInfoPanel();"/> -->
          <apex:actionStatus id="partInfoStatus" startText="Getting Product Information" onstart="showInfoPanel();" onstop="hideModalLoading();"/>
        </apex:outputPanel>
        <div id="contentLoadingMask">
          <div class='loadingMsg' style="text-align: center;">
            <div class="var-spinner">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
            </div>
          </div>
        </div>
        <div id="modalBody" style="display:none;">
          <apex:outputPanel id="partInfoContent" layout="block" style="max-height:400px; min-height:200px; overflow-y:auto;">
            <h1 style="border-bottom:1px solid #112233; padding-bottom:2px; margin-bottom:10px;">{!selectedCatalogPart.product.Name} - {!selectedCatalogPart.product.BigMachines__Part_Number__c}</h1>
            <apex:panelGrid columns="2" columnClasses="longDescriptionPanel, pricingInfoPanel" styleClass="modalTable">
              <apex:outputPanel layout="block" style="padding-top:10px;padding-bottom:10px;">
                <apex:outputText escape="false" value="{!selectedCatalogPart.longDescription}"/>
              </apex:outputPanel>
              <apex:outputPanel styleClass="part-pricing-info" layout="block">
                <table>
                  <tr>
                    <td><h3>Standard</h3></td>
                    <td>
                      <apex:outputText value="{0, number,###,###,##0.00} {!selectedCurrency}">
                        <apex:param value="{!ROUND(selectedCatalogPart.standardPrice*selectedConversionRate, 0)}" />
                      </apex:outputText>
                    </td>
                  </tr>
                  <tr>
                    <td><h3>Target</h3></td>
                    <td>
                      <apex:outputText value="{0, number,###,###,##0.00} {!selectedCurrency}">
                        <apex:param value="{!ROUND(selectedCatalogPart.targetPrice*selectedConversionRate, 0)}" />
                      </apex:outputText>
                    </td>
                  </tr>
                  <!-- Threshold price should only b displayed to a select few higher ups -->
                  <apex:variable var="thresholdRow" rendered="{!showThreshold}" value="1">
                    <tr>
                      <td><h3>Threshold</h3></td>
                      <td>
                        <apex:outputText value="{0, number,###,###,##0.00} {!selectedCurrency}">
                          <apex:param value="{!ROUND(selectedCatalogPart.thresholdPrice*selectedConversionRate, 0)}" />
                        </apex:outputText>
                      </td>
                    </tr>
                  </apex:variable>
                </table>
              </apex:outputPanel>
            </apex:panelGrid>
          </apex:outputPanel>
        </div>
        <apex:commandButton value="Done" onClick="hideInfoPanel()"/>
        <apex:commandButton value="Edit" onClick="openEditWindow('{!selectedCatalogPart.catalogPartId}')" rendered="{!isCatalogAdmin}"/>
      </apex:outputPanel>
    </apex:outputPanel>
  </apex:form>

  <style type="text/css">

    .priceHeader {
      text-align: center;
    }

    .prices {
      text-align: right;
    }

    body.modal-open {
      overflow: hidden;
    }

    .partsTable, .modalTable {
      width: 100%;
    }

    .part-info-modal {
      width: 650px;
      /*height: 400px;
      overflow-y:auto;*/
    }

    .longDescriptionPanel {
      max-width: 400px;
      display:block;
    }

    .pricingInfoPanel {
      max-width: 200px;
      padding-left:10px;
      vertical-align: top;
    }

    #contentLoading .loadingMsg, #contentLoadingParts .loadingMsg {
      padding:50px;
    }

    /*Searching Animation */
    .var-spinner {
      margin: 25px auto 0;
      width: 70px;
      text-align: center;
    }

    .var-spinner > div {
      width: 18px;
      height: 18px;
      background-color: #333;

      border-radius: 100%;
      display: inline-block;
      -webkit-animation: bouncedelay 1.4s infinite ease-in-out;
      animation: bouncedelay 1.4s infinite ease-in-out;
      
      /*Prevent first frame from flickering when animation starts*/

      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }

    .var-spinner .bounce1 {
      -webkit-animation-delay: -0.32s;
      animation-delay: -0.32s;
    }

    .var-spinner .bounce2 {
      -webkit-animation-delay: -0.16s;
      animation-delay: -0.16s;
    }

    @-webkit-keyframes bouncedelay {
      0%, 80%, 100% { -webkit-transform: scale(0.0) }
      40% { -webkit-transform: scale(1.0) }
    }

    @keyframes bouncedelay {
      0%, 80%, 100% { 
        transform: scale(0.0);
        -webkit-transform: scale(0.0);
      } 40% { 
        transform: scale(1.0);
        -webkit-transform: scale(1.0);
      }
    }
  </style>
  </apex:outputPanel>
  <!-- below component rendered only in lightning -->
    <apex:outputPanel rendered="{!isLightning}">
    <apex:includeLightning />
    <div id="lightning" />
    <script>
        $Lightning.use("c:LightningOutApp", function() {
          $Lightning.createComponent("c:catalogViewComp",
          { label : "catalogViewComp" },
          "lightning",
          function(cmp) {
            // do some stuff
          });
        });
    </script>
    <br/> <br/><br/><br/><br/>
    </apex:outputPanel>
</apex:page>
<!--
/***************************************************************************
Author: Divya Hargunani 
Created Date: 5-Nov-2017
Project/Story/Inc/Task : STSK0013224 : Automate LMS Accounts activation from Unity
Description: 
This is VF page for activating LMS Account of multiple users at a time
*************************************************************************************/ 
-->

<apex:page controller="LMS_UserActivationCtrl" id="lmsPage">
    <style>
        .pbHeader .pbTitle .mainTitle{
        font-size : 16px;
        white-space: nowrap;
        }
        
        .pbTitle .btn{
        margin-top : 500 px; 
        }
       
        .spinnerBg{
        width: 100%;
        height: 100%;
        position: absolute;
        background-color: #000;
        opacity: 0.2;
        z-index: 999999;
        }
        
        .spinner{
        width: 100%;
        height: 100%;
        position: absolute;
        background-image: url("/img/loading32.gif");
        background-size: 16px;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        z-index: 9999999;
        opacity: 1;
    }
    </style>
    
    <script>
    function checkAll(cb) {
        var inputElem = document.getElementsByTagName("input");
        for(var i=0; i<inputElem.length; i++){
            if(inputElem[i].id.indexOf("checkdone")!=-1)
            inputElem[i].checked = cb.checked;
        }
    }

    function unCheckCheckAll() {
        var inputElem = document.getElementById("lmsPage:form:pbHeader:userTable:checkAll");
        inputElem.checked = false;      
    }
    
    function checkAllChange(cb) {
        var inputElem = document.getElementsByTagName("input");
        var countFalse = 0;
        var countTrue = 0;
        
        for(var i=0; i<inputElem.length; i++){
            if(inputElem[i].id.indexOf("checkdone")!=-1) {
                if(inputElem[i].checked){
                    countTrue ++;
                }else{
                    countFalse++;
                }
            }
        }
        
        var inputElem = document.getElementById("lmsPage:form:pbHeader:userTable:checkAll");
        if(countFalse == 0)
        inputElem.checked = true;
        if(countFalse > 0)
        inputElem.checked = false;
        if(countTrue == 0)
        inputElem.checked = false;
        
    } 
    
    </script>
    
    <apex:form id="form">
        <apex:actionStatus id="spinnerStatus">
            <apex:facet name="start">
                <div class="spinnerBg" />
                <div class="spinner" />
            </apex:facet>
        </apex:actionStatus>
        <apex:pageBlock id="pbHeader" title="Users Pending LMS Activation">
            <apex:pageMessages id="msg"/>
            <apex:commandButton value="Activate LMS Account" action="{!activateUsers}" reRender="msg,pbHeader" status="spinnerStatus" oncomplete="unCheckCheckAll();"/>
            &nbsp;
            &nbsp;
            <apex:outputLabel value="Total user count:  " style="font-weight:bold;"/>
            <apex:outputText value="{!countVal}" style="font-weight:bold;"/>
            <br/>
            <br/>
            <apex:pageBlockTable value="{!listOfUsers}" var="rec" id="userTable" >
            <apex:column >
                <apex:facet name="header">
                    <apex:inputCheckbox onclick="checkAll(this);" id="checkAll">
                    </apex:inputCheckbox>
                </apex:facet>
                <apex:inputCheckbox value="{!rec.selected}" id="checkdone" onclick="checkAllChange(this);"/>
            </apex:column>
            <apex:column headerValue="Name">
                <apex:outputLink value="/{!rec.record.Id}">
                    {!rec.record.Name}
                </apex:outputLink>
            </apex:column>
            <apex:column value="{!rec.record.FirstName}" headerValue="First Name"/>
            <apex:column value="{!rec.record.LastName}" headerValue="Last Name"/>
            <apex:column value="{!rec.record.Email}" headerValue="Email"/>
            <apex:column >
                <apex:facet name="header">
                    <apex:commandLink action="{!sortByDate}" reRender="form" status="spinnerStatus" value="LMS Registration Date {!IF(myOrder = 'DESC' ,'▼','▲')}">
                    </apex:commandLink>
                </apex:facet>
                <apex:outputField value="{!rec.record.LMS_Registration_Date__c}" />
            </apex:column>
            <apex:column value="{!rec.record.Username}" headerValue="Username"/>
            <apex:column value="{!rec.record.Profile.Name}" headerValue="Profile"/>
            <apex:column value="{!rec.record.Alias}" headerValue="Alias"/>
            <apex:column value="{!rec.record.IsActive}" headerValue="Active"/>
            <apex:column value="{!rec.record.LMS_Status__c}" headerValue="LMS Account Status"/>
        </apex:pageBlockTable>
            <apex:outputPanel style="width:100%">
                 <apex:panelGrid columns="5" id="productPaginationActions" style="margin-left:auto;margin-right:auto;"> 
                            <apex:commandButton action="{!first}" disabled="{!NOT(hasPrevious)}" rerender="userTable,productPaginationActions,msg" status="spinnerStatus" oncomplete="unCheckCheckAll();" value="|< First"></apex:commandButton>
                            <apex:commandButton action="{!previous}" disabled="{!NOT(hasPrevious)}" rerender="userTable,productPaginationActions,msg" status="spinnerStatus" oncomplete="unCheckCheckAll();" value="< Previous"></apex:commandButton>
                                <apex:outputText value="Page {!pageNumber} of {!totalPages}"/>
                            <apex:commandButton action="{!next}" disabled="{!NOT(hasNext)}" value="Next >" rerender="userTable,productPaginationActions,msg" status="spinnerStatus" 
                                                oncomplete="unCheckCheckAll();"></apex:commandButton>
                            <apex:commandButton action="{!last}" disabled="{!NOT(hasNext)}" rerender="userTable,productPaginationActions,msg" status="spinnerStatus" oncomplete="unCheckCheckAll();" value="Last >|"></apex:commandButton>
                        </apex:panelGrid>
            </apex:outputPanel>
        </apex:pageBlock>
    </apex:form>
    
</apex:page>
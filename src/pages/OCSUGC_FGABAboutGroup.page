<apex:page id="pg"
           docType="html-5.0"
           showHeader="false"
           sidebar="false"
           standardStylesheets="false"
           applyHtmlTag="false"
           applyBodyTag="false"
           controller="OCSUGC_GroupDetailController"
           cache="false"
           action="{!isValidAccessibleOAB}">

  <apex:composition template="OCSUGC_Template_NG">
    <apex:define name="pageTitle">
      <title>{!pageTitle}</title>
    </apex:define>
    <apex:define name="body"> 

      <style>

        #tandc.modal .modal-header {
          padding: 0px;
          border-bottom: none !important; 
        }
        #tandc.modal .modal-header .close {
          margin: 15px 15px 0 0 !important; 
        }
        #tandc.modal .modal-header .nav-tabs > li > a {
          border-radius: 0px !important; 
        }
         
        .modal .check-agreement {
          text-align: center !important; 
        }
        #terms_of_use p {
          margin: 10px 0 10px 0px !important ;
          padding-right: 10px;
        }
        #privacy_statement p {
          margin: 10px 0 10px 0px !important ;
          padding-right: 10px;
        }
        #terms_of_use h1 {          
          font-size: 15px;
        }
        #privacy_statement h1 {          
          font-size: 15px;
        }
      
      </style>     
   
      <!-- About -->
      <apex:outputPanel layout="block" id="opAbout" rendered="{!IF(OR(showPanelName=='About', ISBLANK(showPanelName)),true,false)}" style="text-align:center;">
          <apex:form >

            <apex:actionFunction action="{!joinOABGroup}" name="joinOABGroupAction" rerender="opAbout" oncomplete="lockScreen(false,'{!$Label.OCSUGC_Loading}');jQuery('#tandc').modal('hide');window.location.reload();"/>
            <apex:actionFunction name="editGroup" action="{!editGroupDetails}" immediate="true"/>

            <apex:outputPanel id="termConditionOP" rendered="{!IF(isInvited && !isOABMember && !isInviteExpired,true,false)}">
                <div id="agreement" class="col-xs-12">
                    <div class="col-xs-8">
                        <p>Please read and agree to the Terms of Use in order to join the {!currentGroup.Name}</p>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1" style="margin-top: -2px;">
                        <a href="#" style="text-decoration: none;" class="btn btn-primary btn-lg active pull-right" data-toggle="modal" data-target="#tandc">{!$Label.OCSUGC_TermsOfUseTitle}</a>
                    </div>
                </div>
            </apex:outputPanel>
                                                  
            <!-- ONCO-399, Puneet Mishra, -->  
            <apex:outputPanel id="expiredLink" rendered="{!IF(isInvited && !isOABMember && isInviteExpired,true,false)}"> 
              
              <div id="agreement" class="col-xs-12">
                <div class="col-xs-8">
                  <p>Your invitation to join this group has expired. Please contact the group owner for new invite.</p>
                </div>
                <div class="col-xs-3 col-xs-offset-1" style="margin-top: -2px;">
                        <a href="{!$Page.OCSUGC_PrivateMessage}?userId={!currentGroup.OwnerId}" style="text-decoration: none;" class="btn btn-primary btn-lg active pull-right">Message Owner</a>
                    </div>
              </div>
            </apex:outputPanel>
                                                  
            <apex:outputPanel id="editOAB" rendered="{!(isGroupOwner || isCommunityManager )}">
              <div id="agreement" class="col-xs-12" style="padding: 10px;">
                <div class="col-xs-12">
                  <a href="{!$Page.OCSUGC_Members}?g={!currentGroup.Id}{!IF(isOAB,'&fgab=true','')}" class="btn btn-primary btn-lg active" type="button">
                    {!$Label.OCSUGC_Button_Invite_Users}
                  </a>
                  <a class="btn btn-primary btn-lg active" style="text-decoration: none;" onclick="editGroup();">Edit {!currentGroup.Name}</a>
                </div>
              </div>
            </apex:outputPanel>

            <div id="aboutMe" class="section">
                <img id="oab-picture" src="{!currentGroup.FullPhotoUrl}" />
                <h2>{!currentGroup.Name}</h2><br/>
                <div class="section-body">{!currentGroup.Description}</div>
            </div><!-- /.about -->

          </apex:form>
      </apex:outputPanel><!-- About Panel -->

       <!-- Members -->
      <apex:outputPanel layout="block" id="opMembers" rendered="{!IF(AND(showPanelName=='Members',isOABMember || isCommunityManager),true,false)}">
        <h3 style="margin-top:0px;font-size: 20px;">Online Advisory Board Members</h3>
        <apex:form >
          <apex:actionFunction action="{!populateGroupMembers}"  name="loadGroupMembers" reRender="members-grid" oncomplete="drawMemberGrid()" status="loadingMembersStatus"/>
        </apex:form>
        <apex:outputPanel id="members-grid" styleClass="members-grid" layout="block">
          <apex:actionStatus id="loadingMembersStatus">
            <apex:facet name="start">
              <div class="container" style="height:100%;opacity:1;position:absolute;z-index:50000;background-color:#C0C7BF;">
              <div class="progress col-md-4 col-md-offset-4" style="padding:0px; margin-top:20px;">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                  Loading...
                </div>
              </div>
              </div>
            </apex:facet>
          </apex:actionStatus>
          <div class="members-grid-item-sizer"></div>
          <apex:repeat value="{!lstGroupMembers}" var="member">
            <div class="members-grid-item {!member.type}">
              <div class="profile-card">
                <div class="profile-card-body row clearfix">
                  <div class="pull-left profile-card-user-image"><a href="{!$Page.OCSUGC_UserProfile}?userId={!member.memberId}"><img src="{!member.user.SmallPhotoUrl}" class="img-circle" /></a></div>
                  <div class="pull-right profile-card-user-info">
                      <h3 class="user-name"><a href="{!$Page.OCSUGC_UserProfile}?userId={!member.memberId}" style="color:#333;">{!member.Name}</a> <apex:outputPanel styleClass="label label-default" rendered="{!member.isOwner}">Owner</apex:outputPanel><apex:outputPanel styleClass="label label-info" rendered="{!member.isPending}">Pending</apex:outputPanel>
                      </h3> 
                      
                      <p>
                        <span class="user-title">{!IF(ISBLANK(member.Title),"--",member.Title)}</span><br/>
                        <span class="user-account">{!IF(ISBLANK(member.AccountName),"--",member.AccountName)}</span>
                      </p>
                  </div>
                </div>
                <div class="profile-card-footer row clearfix">
                  <div class="pull-left profile-card-stats">
                    Followers <span class="followers">{!member.followers}</span>
                    Following <span class="followers">{!member.following}</span>
                  </div>
                  <div class="pull-right profile-card-action">
                    <apex:outputPanel id="opMemberBtn" rendered="{!IF((isCommunityManager || isGroupOwner) && !member.isOwner, true, false)}">
                        <button type="button" class="btn btn-danger btn-xs"
                        onClick="showActionConfirmation('RemoveGroupMember','{!currentGroupId}','{!member.memberId}','');">{!$Label.OCSUGC_Button_Remove_User}</button>
                    </apex:outputPanel>
                  </div>
                </div>
              </div>                    
            </div>            
          </apex:repeat>
        </apex:outputPanel>
      </apex:outputPanel><!-- Members Panel -->

      <!-- Agreement -->
    <!-- ONCO-431, New Modal box code -->
     
      <div class="modal fade" id="tandc" tabindex="-1">
      <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span>&times;</span><span class="sr-only">Close</span></button>
          <!-- <h4 class="modal-title" id="myModalLabel">{!$Label.OCSUGC_TermsOfUseTitle} &amp; {!$Label.OCSUGC_Privacy_Statement}</h4> -->
          <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a id="oabterms" href="#tabs-1" aria-controls="termsofuse" role="tab" data-toggle="tab">
                  {!$Label.OCSUGC_TermsOfUseTitle}
                </a>
              </li>
              <li role="presentation">
                <a id="oabprivacy" href="#tabs-2" aria-controls="privacystatement" role="tab" data-toggle="tab">
                  {!$Label.OCSUGC_Privacy_Statement}
                </a>
              </li>
          </ul>
        </div>
        <div class="modal-body" style="overflow-y: hidden;">
          <div class="tab-content">
            <div id="tabs-1" class="tab-pane fade in active" style="height:340px; overflow-y: scroll; text-align:justify;">
              <p><apex:outputText value="{!oabTermsOfUse}" escape="false" /></p>
            </div>
            <div id="tabs-2" class="tab-pane fade" style="height:340px; overflow-y: scroll; text-align:justify;">
              <apex:outputPanel layout="block" id="opPrivacyStatement" styleClass="PrivacyStatement">{!$Label.OCSUGC_Privacy_Statement}</apex:outputPanel>
            </div>
          </div>
        </div>
            
        <div class="modal-footer">
            <div style="float: left;">
              <input type="checkbox" onclick="validateTermOfUse(this);" id="chkBoxTermOfUse"/>&nbsp;
              <strong>{!$Label.OCSUGC_Read_Term_Of_Use_FGAB}</strong>
            </div>

            <button type="button" class="btn btn-primary btn-lg active" data-dismiss="modal">{!$Label.OCSUGC_Decline}</button>
            <button type="button" id = "agreeBtn" onclick="lockScreen(true,'{!$Label.OCSUGC_Loading}'); joinOABGroupAction();" class="btn btn-default btn-lg active" disabled="disabled">{!$Label.OCSUGC_Accept}</button>
        </div>
        
      </div>
      </div>
      </div>

      <script type="text/javascript">

        $(document).ready(function() {

          $(".PrivacyStatement").load("{!URLFOR($Resource.OCSUGC_Privacy_Statements, 'current.html')} #privacy");
          $(".PrivacyStatement2").load("{!URLFOR($Resource.OCSUGC_Privacy_Statements, 'current.html')} #privacy");

          loadGroupMembers();

        });

        function validateTermOfUse(chkBox){
            if(chkBox.checked){
                document.getElementById("agreeBtn").className = "btn btn-primary btn-lg active";
                document.getElementById("agreeBtn").disabled = false;
            } else{
                document.getElementById("agreeBtn").className = "btn btn-default btn-lg active";
                document.getElementById("agreeBtn").disabled = true;
            }
        }

        // ONCO-431, Hide/Show the Terms and Privact Div
        function onClickFunc(ID) {
          if(ID == 'oabprivacy') {
            document.getElementById('tabs-1').style.display = 'none'; 
            document.getElementById('tabs-2').style.display = 'block';
          } else if(ID == 'oabterms') {
            document.getElementById('tabs-1').style.display = 'block';
            document.getElementById('tabs-2').style.display = 'none';
          }
        }

        function drawMemberGrid() {
            var $grp_members = $('.members-grid').isotope({
              // options
              itemSelector: '.members-grid-item',
              percentPosition: true,
              layoutMode: 'masonry',
              masonry: {
                columnWidth: '.members-grid-item-sizer'
              }
              
            });

            // store filter for each group
            var memberFilters = {};        

            $('#members .filters').on( 'click', 'button', function() {
              var $this = $(this);
              // get group key
              var $buttonGroup = $this.parents('.btn-group');
              var filterGroup = $buttonGroup.attr('data-filter-group');
              // set filter for group
              memberFilters[ filterGroup ] = $this.attr('data-filter');
              // combine filters
              var filterValue = concatValues( memberFilters );
              // set filter for Isotope
              $grp_members.isotope({ filter: filterValue });
            });

            // change active class on buttons
            $('#members .btn-group').each( function( i, buttonGroup ) {
              var $buttonGroup = $( buttonGroup );
              $buttonGroup.on( 'click', 'button', function() {
                $buttonGroup.find('.active').removeClass('active');
                $( this ).addClass('active');
              });
            });

        }
      </script>

    </apex:define>
  </apex:composition>

    <!--<apex:form id="frmConfirmation">
        <c:OCSUGC_Confirmation ContentType="{!contentType}" ContentId="{!contentId}" SupportContentId="{!supportContentId}" CallBackMethodFromPage="{!callBackMethodNameOptional}" />
        <c:OCSUGC_GroupDeletion GroupId="{!groupId}" isDevCloud="{!isDC}"/>
        <script>
          function yesNoButtonText() {
            var contentTypeVar = '{!contentType}';
            if(typeof contentTypeVar !== 'undefined' && contentTypeVar == 'ShowTnC') {
                $('.modal-footer').find("a.acceptClass").html('{!$Label.OCSUGC_Accept}');
                $('.modal-footer').find("a.declineClass").html('{!$Label.OCSUGC_Decline}');
            }
          }
           
        </script>
    </apex:form>-->   
  
</apex:page>
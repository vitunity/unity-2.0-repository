<!--
//
// (c) 2014 Appirio, Inc.
//
// This page is used to create new FGAB Discussions
// Nov 25, 2014  Puneet Sardana (Appirio)   Original T-335870
-->
<apex:page id="pg"
           docType="html-5.0"
           showHeader="false"
           sidebar="false"
           standardStylesheets="false"
           applyHtmlTag="false"
           applyBodyTag="false"
           Controller="OCSUGC_CreateNewDiscussionController"
           cache="false">
  <apex:composition template="OCSUGC_Template">
    <apex:define name="body">
       <div style="display:{!IF(NOT(hasFGABPageAccess) || isReadOnlyUser,'block','none')};">
          <h4>{!$Label.OCSUGC_No_Access_Page}</h4>
       </div>
       <div style="display:{!IF(hasFGABPageAccess && NOT(isReadOnlyUser),'block','none')};">
            <apex:outputPanel rendered="{!IF(screenCode=='0',true,false)}">
            <article class="create group">
              <apex:form id="frm">
              		<apex:outputPanel id="startDiscussionLabel" rendered="{!IF(discussion.id=='',true,false)}">
                    	<h1>{!$Label.OCSUGC_StartDiscussion}</h1>
                  	</apex:outputPanel>
                  <apex:outputPanel id="updateDiscussionLabel"
                    rendered="{!IF(discussion.id!='',true,false)}">
                    <h1>Update Discussion</h1>
                  </apex:outputPanel>
                  <apex:outputPanel Id="errorPane" style="color: red !important;">
                        <apex:pageMessages id="error"/>
                        <script>
                        		if($('.msgIcon') != null) {
                        			$('.msgIcon').hide();
                        		}
                        </script>
                        <br/>
                  </apex:outputPanel>
                  <div class="form-group" id="required-instruction">
                    <label class="required"><span>*</span> Required Field</label>
                  </div>
                  <input id="hiddenInput" type="hidden" name="hiddenInput"
                    class="hiddenInput" />
                  <apex:actionstatus id="status">
                    <apex:facet name="start">
                      <div class="waitingSearchDiv" id="el_loading"
                        style="background-color: #fbfbfb; height: 100%; opacity: 0.65; width: 100%;">
                        <div class="waitingHolder" style="top: 350px; width: 100px;">
                          <img class="waitingImage" src="/img/loading.gif"
                            title="{!$Label.OCSUGC_Please_Wait}..." /> <span
                            class="waitingDescription">{!$Label.OCSUGC_Please_Wait}...</span>
                        </div>
                      </div>
                    </apex:facet>
                  </apex:actionstatus>
                  <div class="col-md-6 mandatory">
                  	<apex:actionFunction name="createDiscussionAction" action="{!updateDiscussion}" oncomplete="lockScreen(false,'{!$Label.OCSUGC_Processing}');">
                      <apex:param name="firstParam" assignTo="{!discussion.body}" value="" />
                    </apex:actionFunction>
	                <apex:actionFunction name="cancelOps" action="{!cancelOps}"/>
	                <apex:actionFunction name="createUploadDiscussionAction" action="{!updateFileDiscussion}">
	                	<apex:param id="upbody" value="" assignTo="{!discussion.body}" name="upbody"/>
                    </apex:actionFunction>
                    <div class="form-group">
                      <label class="required" style="width: 125px;"><span>*</span>
                        {!$Label.OCSUGC_Description}</label>
                      <!-- <apex:outputPanel id="descriptionTextArea"
                        rendered="{!IF(discussion.id=='',true,false)}">
                        {!$Label.OCSUGC_Do_Not_Enter_Personal_Info}
                        <apex:inputField styleClass="form-control" value="{!discussion.Body}" id="description" onkeyup="limitText(this,5000,true);saveButtonWorks();"/>
                            You have <span id="countdown">5000</span> characters left.
                      </apex:outputPanel>
                      <apex:outputPanel id="descriptionLabel"
                        rendered="{!IF(discussion.id!='',true,false)}">
                        <br />
                        <apex:outputText value="{!discussion.Body}"
                          style="float: left; padding-left: 10px;" />
                      </apex:outputPanel> -->
					<apex:outputPanel id="descriptionTextArea" rendered="true">
						{!$Label.OCSUGC_Do_Not_Enter_Personal_Info}
                        <apex:inputField styleClass="form-control" value="{!discussion.Body}" id="description" onkeyup="limitText(this,5000,true);saveButtonWorks();"/>
                            You have <span id="countdown">5000</span> characters left.
                    </apex:outputPanel>
                    </div>
                    <!-- /.form-group -->

                  </div>
                  <apex:actionFunction name="updateTagsActionForVisibility"   action="{!updatePrePopulateTagsForVisibility}" rerender="tagsuploadfrm" oncomplete="saveButtonWorks();" />

              </apex:form>

                <div class="col-md-6 optional">
                  <div class="form-tags">
                    <apex:outputPanel id="idTag">
                      <apex:outputPanel id="uploadFileOutputPanel" rendered="{!IF(isFileTypeExist && discussion.id=='',true,false)}">
                        <apex:form id="imageuploadfrm">
                          <apex:inputHidden value="{!discussion.body}" id="hiddenDisBody"/>
                        </apex:form>
                      </apex:outputPanel>
                      <apex:outputPanel id="uploadFileOutputText" rendered="{!IF(isFileTypeExist && discussion.id!='' && discussion.ContentFileName!='',true,false)}">
                        <div class="form-group">
<!--                           <label>Upload File</label> -->
<!--                           <apex:outputText value="{!discussion.ContentFileName}" /> -->
						<apex:outputPanel rendered="{!NOT(ISBLANK(discussion.RelatedRecordId)) && begins(discussion.ContentType,'image/')}">
							<label>Uploaded Image</label>
                             <div class="panel-media">
                                <div class="zoom" data-toggle="modal" data-target="#xlView">
                                	<img src="{!$Site.prefix}/sfc/servlet.shepherd/version/download/{!discussion.RelatedRecordId}?asPdf=false&operationContext=CHATTER" height="100%" width="100%" />
                                </div>
<!-- 					        <div style="width: 38px; font-weight: bold; color: black; position: relative; top: -18px; left: 228px;"> -->
<!-- 					        	<a href="#" style="font-weight: bold; color: black; text-decoration: none;" onclick="editImage()">Edit</a> -->
<!-- 					        </div> -->
                            </div>
                         </apex:outputPanel>
                        </div>
                      </apex:outputPanel>
                      <apex:form id="tagsuploadfrm">
                        <apex:outputPanel rendered="{!NOT(ISBLANK(discussion.id))}">
                          <apex:actionFunction name="createDiscussionAction" action="{!updateDiscussion}" oncomplete="lockScreen(false,'{!$Label.OCSUGC_Processing}');">
                            <apex:param name="firstParam" assignTo="{!discussionBody}" value="" />
                          </apex:actionFunction>
                          <apex:actionFunction name="cancelOps" immediate="true"  action="{!cancelOps}"/>
                        </apex:outputPanel>
                        
                        <apex:actionFunction name="addTagAction" action="{!addTag}" rerender="tagsuploadfrm, errorPane" status="clearAllStatus">
                          <apex:param name="selectedTag" assignTo="{!selectedTag}" value="" />
                        </apex:actionFunction>
                        <apex:actionFunction name="updateTagAction" action="{!updateTag}" reRender="tagsuploadfrm" status="clearAllStatus">
                          <apex:param name="selectedTag" assignTo="{!selectedTag}" value="" />
                        </apex:actionFunction>
                        <div class="form-group form-tags">
                          <apex:outputPanel >
                            <label for="tags">{!$Label.OCSUGC_Tags}</label>
                            <input type="text" style="height: 38px;" class="form-control" id="tags" placeholder="+ Tag" onkeyup="limitText(this,40,false);" onKeydown="Javascript: if (event.keyCode==13) {addTagAction(document.getElementById('tags').value); return false;}" />
                            <button type="button" class="btn-sm btn-primary active" onClick="addTagAction(document.getElementById('tags').value);">+Add</button>
                          </apex:outputPanel>
                          <apex:outputPanel rendered='{!IF(wrapperTags.size > 0,true,false)}'>
                            <ul>
                              <apex:repeat value="{!wrapperTags}" var="tag">
                                <li class="{!IF(tag.isActive,'active','inactive')}">
                                  <a href="javascript:void();"  onClick="updateTagAction('{!tag.tagName}');">
                                    {!tag.tagName}
                                  </a>
                                </li>
                              </apex:repeat>
                            </ul>
                          </apex:outputPanel>
                        </div>
                        <apex:actionStatus id="clearAllStatus">
                          <apex:facet name="start">&nbsp;<apex:image value="{!$Resource.OCSUGC_Loader}" width="17px" height="17px" />
                          </apex:facet>
                          <apex:facet name="stop" />
                        </apex:actionStatus>
                        <script>
			             	$("#tags").keydown(function(event){
							    if(event.keyCode == 13) {
							      event.preventDefault();
							      return false;
							    }
							  });
			            </script>
                      </apex:form>
                    </apex:outputPanel>
                  </div>
                  <!-- /.form-group -->

                </div>



              <article class="create group buttons">
               <input type="button" id="btnCancel" class="btn btn-primary btn-lg active" style="margin-left: 15px;" value="Cancel" onClick="lockScreen(true,'{!$Label.OCSUGC_Processing}'); cancelOps();" />
               	<!-- PUNEET MISHRA 01112016, commenting out the code as allowing the user to upload new attachment. 
               	<apex:outputPanel rendered="{!IF(ISBLANK(discussion.id),true,false)}">
                	<input type="button" id="btnUpload" style="margin-left: 15px;" value="Attach File" onClick="lockScreen(true,'{!$Label.OCSUGC_Processing}'); createFileUploadDiscussion();" />
               	</apex:outputPanel> -->
               	<input type="button" id="btnUpload" style="margin-left: 15px;" value="Attach File" onClick="lockScreen(true,'{!$Label.OCSUGC_Processing}'); createFileUploadDiscussion();" />
               	
               	<input type="button" id="btnSave" style="margin-left: 15px;" value="{!IF(ISBLANK(discussion.id),'Share','Update')}" onClick="lockScreen(true,'{!$Label.OCSUGC_Processing}'); createDiscussion();" />
				</article>
             </article>
            </apex:outputPanel>
      </div>
      <!--/#wrap -->
      <script type="text/javascript">
                $(document).ready(function() {
                    if('{!discussion.id}'==''){
                        jQuery("#pg\\:CK_Template\\:frm\\:description").attr("placeholder", "Description");
                        saveButtonWorks();

                        setTimeout(function(){
                            jQuery("li:has('a'):has('label'):has('input[Value=None]')").remove();
                        }, 1000);
                    }
					
					/* Related to ONCO-443,	Puneet Mishra,	On Edit if description is not empty then total characters left was not showing, 
                    						added the code snippet to show remaining characters*/
                    if(document.getElementById('pg:CK_Template:frm:description') != null) {
                       limitText(document.getElementById('pg:CK_Template:frm:description'),5000,true);saveButtonWorks();
                    }
					
                    if('{!discussion.id}'!=''){
                        document.getElementById("btnSave").className = "btn btn-primary btn-lg active";
                        document.getElementById("btnSave").disabled = false;
                        document.getElementById("btnUpload").className = "btn btn-primary btn-lg active";
                        document.getElementById("btnUpload").disabled = false;
                    }
                    
                    $("#tags").keydown(function(event){
					    if(event.keyCode == 13) {
					      event.preventDefault();
					      return false;
					    }
					});
					  
                    });

                function fillFileTypePlaceHolder(){
                	canUpload = "true";
                    document.getElementById('extensionsTextBox').style.display = 'block';
                }

                function saveButtonWorks() {
                    var description = document.getElementById("pg:CK_Template:frm:description").value;
                    if((description != null && description !='')) {
                        document.getElementById("btnSave").className = "btn btn-primary btn-lg active";
                        document.getElementById("btnSave").disabled = false;
                        document.getElementById("btnUpload").className = "btn btn-primary btn-lg active";
                        document.getElementById("btnUpload").disabled = false;
                    } else {
                        document.getElementById("btnSave").className = "btn btn-default btn-lg active";
                        document.getElementById("btnSave").disabled = true;
                        document.getElementById("btnUpload").className = "btn btn-default btn-lg active";
                        document.getElementById("btnUpload").disabled = true;
                    }
                }

                //Function to add article tags for user
                function createDiscussion(){
/*                    var selectedFileExtension = '';
                    $('.hiddenInput').each(function(){
                        selectedFileExtension = ''+$(this).val().toUpperCase();
                    });
                    var selectedExtensions = '{!commmaSeperatedExtensions}';
                    if((selectedFileExtension !='' && selectedFileExtension != null)
                      && selectedExtensions.lastIndexOf(selectedFileExtension) == -1){
                      	if(document.getElementById('extensionsTextBox').style.display != 'block') {
                      		alert('File Type not Supported. You can only upload ' + selectedExtensions + ' files.');
                      	}
                        lockScreen(false,'{!$Label.OCSUGC_Processing}');
                        return;
                    }
*/
					if(canUpload=="false"){
						alert(('{!$Label.OCSUGC_Maximum_File_Size_FGAB_Discussion}').replace(/FILE_SIZE/gi,'{!$Label.OCSUGC_FGAB_Discussion_File_Upload_Size_Limit}'));
						lockScreen(false,'{!$Label.OCSUGC_Processing}');
						return;
					}
                    var descriptionObj = document.getElementById("pg:CK_Template:frm:description");
                    if(descriptionObj){
                    	createDiscussionAction(descriptionObj.value);
                    }
                    else{
                    	createDiscussionAction();
                    }

                 }
                 
                 function createFileUploadDiscussion(){
/*                    var selectedFileExtension = '';
                    $('.hiddenInput').each(function(){
                        selectedFileExtension = ''+$(this).val().toUpperCase();
                    });
                    var selectedExtensions = '{!commmaSeperatedExtensions}';
                    if((selectedFileExtension !='' && selectedFileExtension != null)
                      && selectedExtensions.lastIndexOf(selectedFileExtension) == -1){
                      	if(document.getElementById('extensionsTextBox').style.display != 'block') {
                      		alert('File Type not Supported. You can only upload ' + selectedExtensions + ' files.');
                      	}
                        lockScreen(false,'{!$Label.OCSUGC_Processing}');
                        return;
                    }
*/
					if(canUpload=="false"){
						alert(('{!$Label.OCSUGC_Maximum_File_Size_FGAB_Discussion}').replace(/FILE_SIZE/gi,'{!$Label.OCSUGC_FGAB_Discussion_File_Upload_Size_Limit}'));
						lockScreen(false,'{!$Label.OCSUGC_Processing}');
						return;
					}
                    var descriptionObj = document.getElementById("pg:CK_Template:frm:description");
                    console.log(' ==== descriptionObj ==== ' + descriptionObj.value);
                    if(descriptionObj){
                    	console.log('inside');
                      createUploadDiscussionAction(descriptionObj.value);
                    }
                    else{
                      createUploadDiscussionAction();
                    }

                 }



                function updateHiddenValue(thisId){
                    var fileNameWithPath = thisId.value;
                    if(fileNameWithPath != ''){
                      //Start Here - 11 Nov 2014    Naresh K Shiwani       (Task Ref. I-137732)
                      var fileName = fileNameWithPath.split(/(\\|\/)/g).pop();
                      if(fileName.length > 80){
                        alert('{!$Label.OCSUGC_Exceeded_File_Name_Limit}' + '\n' + fileName);
                        thisId.value = null;
                        fillFileTypePlaceHolder();
                        return false;
                      }
                      //Ends Here - 11 Nov 2014    Naresh K Shiwani       (Task Ref. I-137732)
                        document.getElementById('extensionsTextBox').style.display = 'none';
                    }
                    var ext = fileNameWithPath.substr(fileNameWithPath.lastIndexOf('.') + 1);
                    $('.hiddenInput').each(function(){
                        $(this).val(ext);
                    });
                }
                $('.messageText').css('color', 'red');

             function limitText(limitField, limitCount, isToShowRemainingChar) {
                if(limitField!=null){
                    if (limitField.value.length > limitCount) {
                        limitField.value = limitField.value.substring(0, limitCount);
                    } if(isToShowRemainingChar){
                            document.getElementById('countdown').innerHTML = limitCount - limitField.value.length;
                    }
                }
            }
            /*
            function fnCancel() {
                if('{!discussion.id}'==''){
                    window.open('{!$Page.OCSUGC_Home}','_self');
                } else{
                    window.open('{!$Page.OCSUGC_DiscussionDetail}?Id={!discussion.id}','_self');
                }
            }
            */
       </script>
       <div class="modal fade" id="xlView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" style="margin-top: -8px;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  </div>
                  <div class="modal-body">
                  <img src="{!$Site.prefix}/sfc/servlet.shepherd/version/download/{!discussion.RelatedRecordId}?asPdf=false&operationContext=CHATTER" />
                  </div>
                  <div class="modal-footer">
                  </div>
              </div>
          </div>
      </div>

    </apex:define>
  </apex:composition>

  <script>
	
	var canUpload = "true";
    $('#pg\\:CK_Template\\:imageuploadfrm\\:idInputFile').change(function(e) {
    	if(e.target.files[0].size >= parseInt('{!$Label.OCSUGC_FGAB_Discussion_File_Upload_Size_Limit}')*1024*1024){
    		alert(('{!$Label.OCSUGC_Maximum_File_Size_FGAB_Discussion}').replace(/FILE_SIZE/gi,'{!$Label.OCSUGC_FGAB_Discussion_File_Upload_Size_Limit}'));
    		canUpload = "false";
    		return;
    	}
    	canUpload = "true";
      $('.hiddenInput').each(function(){
          selectedFileExtension = ''+$(this).val().toUpperCase();
      });
      var selectedExtensions = '{!commmaSeperatedExtensions}';
      if((selectedFileExtension !='' && selectedFileExtension != null)
        && selectedExtensions.lastIndexOf(selectedFileExtension) >= 0){
            var file = e.target.files[0];
            canvasResize(file, {
            width: parseInt('{!$Label.OCSUGC_Compress_Width}'),
            height: parseInt('{!$Label.OCSUGC_Compress_Height}'),
            crop: false,
            quality: parseInt('{!$Label.OCSUGC_Compress_Quality}'),
            //rotate: 90,
            callback: function(data, width, height) {
              CreateNewAttachment(data,file);
            }
          });
      }
    });

    function CreateNewAttachment(data,file) {
      sforce.connection.sessionId = '{!$Api.Session_ID}';
      var tempDisIdImg = '{!$User.Id}'.substring(10)+'_DcsnImg';
      //var result = sforce.connection.query("Select Id From OCSUGC_DiscussionDetail__c Where OCSUGC_DiscussionId__c = '111111111111111'");
      var result = sforce.connection.query("Select Id From OCSUGC_DiscussionDetail__c Where OCSUGC_DiscussionId__c = '"+tempDisIdImg+"'");
        var records = result.getArray("records");

        for (var i=0; i< records.length; i++) {
          var delResult = sforce.connection.deleteIds([records[i].Id]);
        if (delResult[i].getBoolean("success") == false) {
            alert("\n failed to compress image " + delResult[i]);
        }
        }

      data = data.replace('data:'+file.type+';base64,','');
      var temp = new sforce.SObject("OCSUGC_DiscussionDetail__c");
      //temp.OCSUGC_DiscussionId__c = '111111111111111';
      temp.OCSUGC_DiscussionId__c = tempDisIdImg;

      result = sforce.connection.create([temp]);
      for (var i=0; i<result.length; i++) {
        if (result[i].getBoolean("success")) {

          var attachment = new sforce.SObject("Attachment");
          attachment.Body = data;
          attachment.ContentType = file.type;
          attachment.Name = 'Test ' + result[i].id;
          attachment.ParentId = result[i].id;
          result = sforce.connection.create([attachment]);
          for (var i=0; i<result.length; i++) {
            if (result[i].getBoolean("success") == false) {
              alert("\n failed to compress image " + result[i]);
            }
          }
        } else {
          alert("\n failed to compress image " + result[i]);
        }
      }
      //alert('Image Compressed');
    }
  </script>

  <script type="text/vbscript">
    Function IEBinary_getByteAt(strBinary, iOffset)
      IEBinary_getByteAt = AscB(MidB(strBinary, iOffset + 1, 1))
    End Function

    Function IEBinary_getBytesAt(strBinary, iOffset, iLength)
        Dim aBytes()
        ReDim aBytes(iLength - 1)
        For i = 0 To iLength - 1
          aBytes(i) = IEBinary_getByteAt(strBinary, iOffset + i)
        Next
        IEBinary_getBytesAt = aBytes
    End Function

    Function IEBinary_getLength(strBinary)
      IEBinary_getLength = LenB(strBinary)
    End Function
  </script>
</apex:page>
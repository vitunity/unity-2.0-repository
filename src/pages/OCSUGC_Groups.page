<apex:page id="pg"
            docType="html-5.0"
            showHeader="false"
            sidebar="false"
            standardStylesheets="false"
            applyHtmlTag="false"
            applyBodyTag="false"
            controller="OCSUGC_GroupsController"
            cache="false"
            action="{!loadGroups}">
  <apex:composition template="OCSUGC_Template_NG">
    <apex:define name="pageTitle">
      <title>{!pageTitle}</title>
    </apex:define>
    <apex:define name="body">
      <style type="text/css">
        .groups-search-bar {
          background-color: #F1F1F1;
          margin-bottom: 20px;
          padding: 10px;
        }        

        .group-item-sizer, .group-item {
          width: 100%;
        }
        
        #groups-adv-search .input-group-btn .btn-group {
          display: flex;
        }
        #groups-adv-search .form-horizontal .form-group {
            margin-left: 0;
            margin-right: 0;
        }
        #groups-adv-search .dropdown.dropdown-lg .dropdown-menu {
            margin-top: -1px;
            padding: 10px 20px 20px;
        }
        #groups-adv-search .dropdown.dropdown-lg .dropdown-menu {
            min-width: 600px;
        }
        #groups-adv-search .btn-group .btn {
            border-radius: 0;
            margin-left: -1px;
        }
        #groups-adv-search .btn-group .btn:last-child {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }
        #groups-adv-search .btn-group .form-horizontal .btn[type="submit"] {
          border-top-left-radius: 4px;
          border-bottom-left-radius: 4px;
        }
      </style>

      
      <apex:form >

      <apex:actionfunction name="doGroupSearch" action="{!searchGroups}" id="searchGrp" rerender="searchSummary,sortFilterToolbar,groups-grid" oncomplete="drawGroupsGrid();">
        <apex:param name="groupSearchTerm" value="" assignTo="{!groupSearchTerm}"/>
      </apex:actionfunction>

      <div class="groups-search-bar">
        <div class="input-group" id="groups-adv-search">
          <input type="text" id="groupNameSearch" class="form-control" placeholder="Search Groups by Name" />
          <div class="input-group-btn">
            <div class="btn-group" role="group">                
                <button type="submit" class="btn btn-primary" onclick="doGroupSearch(document.getElementById('groupNameSearch').value); return false;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </div>
          </div>
        </div>
      </div>

      <apex:actionfunction name="joinGroup" action="{!joinGroupAction}" oncomplete="window.location.reload();">
        <apex:param id="joinGrpPar" value="" assignTo="{!selectedGroupId}" name="joinGrpPar"/>
      </apex:actionfunction>
      
      <apex:actionfunction name="requestJoinGroup" action="{!requestJoinGroupAction}" oncomplete="window.location.reload();">
        <apex:param id="reqGrpPar" value="" assignTo="{!selectedGroupId}" name="reqGrpPar"/>
      </apex:actionfunction>

      <div class="row">
        <div class="col-sm-7 col-md-7" id="groups">
          <apex:outputPanel layout="block" styleClass="groups-search-rslt-summary" id="searchSummary">
            <p>
              <apex:outputPanel rendered="{!NOT(showingSearchRslt)}">Showing {!groupsCount} recently active groups</apex:outputPanel>
              <apex:outputPanel rendered="{!showingSearchRslt}">Found {!groupsCount} groups</apex:outputPanel>
              <apex:outputLink styleClass="btn btn-link" value="#" onclick="window.location.reload();" rendered="{!showingSearchRslt}" style="padding: 0px 10px;color: rgb(213, 83, 68);"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Clear Search</apex:outputLink>
            </p>
          </apex:outputPanel>
          <apex:outputPanel id="sortFilterToolbar">
          <apex:outputPanel layout="block" styleClass="btn-toolbar" style="margin:15px -15px 0px -15px;" rendered="{!groupsCount > 0}">
            <div class="btn-group filters" data-filter-group="area2">
              <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="btn-group-label">Community:</span>
                <span class="selection">Any</span>
                <span class="caret"></span>
              </button>
              <ul id="grp-fltr-comm-dropdown" class="dropdown-menu">
                <li><a href="#" onclick="groupSortFilterOptionClicked(this)" data-filter="*">Any</a></li>
                <li><a href="#" onclick="groupSortFilterOptionClicked(this)" data-filter=".oncopeer">OncoPeer</a></li>
                <li><a href="#" onclick="groupSortFilterOptionClicked(this)" data-filter=".developerCloud">Developer Cloud</a></li>
              </ul>
            </div>
            <div class="btn-group filters" data-filter-group="type">
              <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="btn-group-label">Type:</span>
                <span class="selection">Any</span>
                <span class="caret"></span>
              </button>
              <ul id="grp-fltr-role-dropdown" class="dropdown-menu">
                <li><a href="#" onclick="groupSortFilterOptionClicked(this)" data-filter="*">Any</a></li>
                <li><a href="#" onclick="groupSortFilterOptionClicked(this)" data-filter=".public">Public</a></li>
                <li><a href="#" onclick="groupSortFilterOptionClicked(this)" data-filter=".private">Private</a></li>
              </ul>
            </div>
            
            <div class="btn-group pull-right sorting">
              <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="btn-group-label">Sort by:</span>
                <span class="selection">Name</span>
                <span class="caret"></span>
              </button>
              <ul id="grp-sort-dropdown" class="dropdown-menu">
                <li><a href="#" onclick="groupSortFilterOptionClicked(this)" data-sort-by="original-order">Name</a></li>
                <li><a href="#" onclick="groupSortFilterOptionClicked(this)" data-sort-by="members">Members</a></li>
              </ul>
            </div>
          </apex:outputPanel>
          </apex:outputPanel>
          <apex:outputPanel layout="block" styleClass="groups" id="groups-grid">
            <div class="group-item-sizer"></div>
            <apex:repeat value="{!groups}" var="gp">
              <div class="panel panel-default group-item {!gp.className}">              
                  <div class="panel-body">                
                    <div class="group-icon pull-left" style="width:7%">
                      <a href="{!$Page.OCSUGC_GroupDetail}?g={!gp.grp.id}&dc={!gp.isDCGroup}"><img src="{!gp.grp.SmallPhotoUrl}" class="img-circle"/></a>
                    </div>
                    <div class="group-info pull-left" style="width:90%; padding-left:15px;">                  
                      <h4 class="panel-title"><a href="{!$Page.OCSUGC_GroupDetail}?g={!gp.grp.id}&dc={!gp.isDCGroup}">{!gp.grp.Name}</a> <span class="group-type label label-default">{!gp.grp.CollaborationType} Group</span></h4>
                      <p>{!gp.grp.Description}</p>
                    </div>                                
                  </div>
                  <div class="panel-footer clearfix">
                    <div class="pull-left" style="margin-top:3px">
                      <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                      <span class="members">{!gp.grp.MemberCount}</span>
                      <span style="margin-left:10px;">Owner: {!gp.grp.Owner.Name}</span>
                    </div>
                    <div class="pull-right">
                      <!-- Join Group button should be visible when Group is public and profiled user is not a member -->
                      <apex:outputPanel rendered="{!gp.joinGroup && canJoinGroups}">
                        <button type="button" class="btn btn-success btn-xs" onClick="joinGroup('{!gp.grp.Id}');">{!$Label.OCSUGC_JoinGroup}</button>
                      </apex:outputPanel>
                      <!-- Ask to Join button should be visible when Group is private and profiled user is not a member -->
                      <apex:outputPanel rendered="{!gp.requestJoinGroup && canJoinGroups && NOT(gp.requestAlreadySent)}">
                        <button type="button" class="btn btn-warning btn-xs" onClick="requestJoinGroup('{!gp.grp.Id}');">{!$Label.OCSUGC_AsktoJoin}</button>
                      </apex:outputPanel>
                      <apex:outputPanel rendered="{!gp.requestAlreadySent}">
                        <button disabled="true" type="button" class="btn btn-default btn-xs">{!$Label.OCSUGC_Button_Request_Sent}</button>
                      </apex:outputPanel>
                    </div>
                  </div>
                </div>
            </apex:repeat>
          </apex:outputPanel><!--.groups-->
        </div>

        <div class="col-sm-offset-1 col-sm-4 col-md-offset-1 col-md-4 profile-right-col">
          <apex:outputPanel layout="block" id="recommendedGroups" styleClass="section">
            <div class="section-head"><h3>Recommended Groups</h3></div>
            <ul class="list-group">
              <apex:repeat value="{!cdata.recommendations}" var="recdn">
                <li class="list-group-item" style="background-color: #eee;">
                    <img src="{!recdn.object_Z.Photo.smallPhotoUrl}"  class="img-circle" style="width:32px;margin-right:5px;"/>
                    <a href="{!$Page.OCSUGC_GroupDetail}?g={!recdn.object_Z.id}" target="_blank">{!recdn.object_Z.name}</a>
                    <span class="badge" style="color:#fff;margin-top:7px;"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> {!recdn.object_Z.memberCount}</span>
                </li>
              </apex:repeat>
            </ul>
          </apex:outputPanel>
          <div>
            <a href="{!$Page.OCSUGC_UserProfile}?userId={!loggedInUser.Id}#groups" class="btn btn-info btn-block" role="button" style="margin-top:-27px">View Your Groups</a>
          </div>
        </div>
      </div><!--.row-->
      </apex:form>

      <script type="text/javascript" charset="utf-8">

        //click of dropdown-menu options
        function groupSortFilterOptionClicked( obj ) {
          $(obj).parents('.btn-group').find('.selection').text($(obj).text());
          $(obj).parents('.btn-group').find('.selection').val($(obj).text());
        };

        function concatGroupFilterValues( obj ) {
          var value = '';
          for ( var prop in obj ) {
            if( obj[ prop ] != '*')
              value += obj[ prop ];
          }
          return value;
        }; 

        //Filtering and sorting for groups
        function drawGroupsGrid() {
          //init Isotope
          var $groups = $('.groups').isotope({
            // options
            itemSelector: '.group-item',
            percentPosition: true,
            layoutMode: 'vertical',
            getSortData: {
              name: '.name',
              members: '.members parseInt'
            },

            sortAscending: {
              members: false
            }
          });

          // store filter for each group
          var groupFilters = {};        

          $('#groups .filters').on( 'click', '.dropdown-menu li a', function() {
            var $this = $(this);
            // get group key
            var $buttonGroup = $this.parents('.btn-group');
            var filterGroup = $buttonGroup.attr('data-filter-group');
            // set filter for group
            groupFilters[ filterGroup ] = $this.attr('data-filter');
            // combine filters
            var filterValue = concatGroupFilterValues( groupFilters );
            // set filter for Isotope
            $groups.isotope({ filter: filterValue });
          });

          // change active class on buttons
          $('#groups .btn-group').each( function( i, buttonGroup ) {
            var $buttonGroup = $( buttonGroup );
            $buttonGroup.on( 'click', 'button', function() {
              $buttonGroup.find('.active').removeClass('active');
              $( this ).addClass('active');
            });
          });

          // sort items on button click
          $('#groups .sorting').on( 'click', '.dropdown-menu li a', function() {
            var sortByValue = $(this).attr('data-sort-by');
            $groups.isotope({ sortBy: sortByValue });
          });    
        };

        
        $(document).ready( function () {
          drawGroupsGrid();
        });

        
      </script>
    </apex:define>
  </apex:composition>	
</apex:page>
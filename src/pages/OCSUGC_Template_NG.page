<apex:page docType="html-5.0"
           showHeader="false"
           sidebar="false"
           standardStylesheets="false"
           applyHtmlTag="false"
           applyBodyTag="false"
           id="OCSUGC_Template"          
           controller="OCSUGC_Base_NG">

  <html lang="en-US" id="idHTML">
    <head>
      <meta charset="utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <apex:insert name="pageTitle" />

      <link type="text/css" rel="stylesheet" href="{!URLFOR($Resource.OCSUGC_Design_NG, 'css/bootstrap.min.css')}"/>
      <link type="text/css" rel="stylesheet" href="{!URLFOR($Resource.OCSUGC_Design_NG, 'css/main.css')}"/>
      <link type="text/css" rel="stylesheet" href="{!URLFOR($Resource.OCSUGC_Design_NG, 'css/jasny-bootstrap.min.css')}"/>
      <link type="text/css" rel="stylesheet" href="{!URLFOR($Resource.OCSUGC_Design_NG, 'css/oncopeer.css')}"/>
      <link type="text/css" rel="stylesheet" href="{!URLFOR($Resource.OCSUGC_Design_NG, 'css/jquery-ui.min.css')}"/>


      <script src="{!URLFOR($Resource.OCSUGC_Design_NG, 'js/jquery-1.12.4.min.js')}"></script>
      <script src="{!URLFOR($Resource.OCSUGC_Design_NG, 'js/bootstrap.min.js')}"></script>
      <script src="{!URLFOR($Resource.OCSUGC_Design_NG, 'js/jasny-bootstrap.min.js')}"></script>
      <script src="{!URLFOR($Resource.OCSUGC_Design_NG, 'js/isotope.pkgd.min.js')}"></script>
      <script src="{!URLFOR($Resource.OCSUGC_Design_NG, 'js/linkify.min.js')}"></script>
      <script src="{!URLFOR($Resource.OCSUGC_Design_NG, 'js/linkify-jquery.min.js')}"></script>      
      <script src="{!URLFOR($Resource.OCSUGC_Design_NG, 'js/bootstrap-multiselect.js')}"></script>
      <script src="{!URLFOR($Resource.OCSUGC_Design_NG, 'js/jquery-ui.min.js')}"></script>
      
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      <script type="text/javascript" src="//use.typekit.net/ayp5zai.js"></script>
      <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

      <meta name="msapplication-TileColor" content="#2b5797" />
      <meta name="msapplication-TileImage" content="{!URLFOR($Resource.OCSUGC_Favicon, 'mstile-144x144.png')}" />
      <meta name="msapplication-config" content="none" />
      <meta name="theme-color" content="#ffffff" />   
      <link rel="icon" type="image/png" href="{!URLFOR($Resource.OCSUGC_Favicon, 'favicon-16x16.png')}" sizes="16x16" /> 
      <link rel="icon" type="image/png" href="{!URLFOR($Resource.OCSUGC_Favicon, 'favicon-32x32.png')}" sizes="32x32" />
      <link rel="icon" type="image/png" href="{!URLFOR($Resource.OCSUGC_Favicon, 'favicon-96x96.png')}" sizes="96x96" />
      <link rel="shortcut icon" type="image/x-icon" href="{!URLFOR($Resource.OCSUGC_Favicon, 'favicon.ico')}" />
      <link rel="apple-touch-icon" sizes="57x57"   href="{!URLFOR($Resource.OCSUGC_Favicon, 'apple-touch-icon-57x57.png')}" />
      <link rel="apple-touch-icon" sizes="60x60"   href="{!URLFOR($Resource.OCSUGC_Favicon, 'apple-touch-icon-60x60.png')}" />
      <link rel="apple-touch-icon" sizes="72x72"   href="{!URLFOR($Resource.OCSUGC_Favicon, 'apple-touch-icon-72x72.png')}" />
      <link rel="apple-touch-icon" sizes="76x76"   href="{!URLFOR($Resource.OCSUGC_Favicon, 'apple-touch-icon-76x76.png')}" />
      <link rel="apple-touch-icon" sizes="114x114" href="{!URLFOR($Resource.OCSUGC_Favicon, 'apple-touch-icon-114x114.png')}" />
      <link rel="apple-touch-icon" sizes="120x120" href="{!URLFOR($Resource.OCSUGC_Favicon, 'apple-touch-icon-120x120.png')}" />
      <link rel="apple-touch-icon" sizes="144x144" href="{!URLFOR($Resource.OCSUGC_Favicon, 'apple-touch-icon-144x144.png')}" />
      <link rel="apple-touch-icon" sizes="152x152" href="{!URLFOR($Resource.OCSUGC_Favicon, 'apple-touch-icon-152x152.png')}" />



    </head>
    <body class="{!IF(isOAB,'dkgreen',IF(isDevCloud,'devcloud',''))}">
      <c:OCSUGC_Header_NG id="idHead"/>
      <div id="main-body">

        <div class="sub-header" style="display:{!IF(isOAB,'block','none')};">
          <div class="container-fluid">
            <div id="oab-subheader" class="container">
                <apex:outputPanel styleClass="col-sm-8 col-xs-8" layout="block" rendered="{!isOAB && (isOABMember || isCommunityManager || isReadOnlyUser)}">
                    <div id="subheader-area-tag">Online Advisory Board</div>
                    <a href="{!$Page.OCSUGC_Home}?isResetPwd=true&fgab=true&g={!oabGroupId}">{!oabGroupName}</a>
                </apex:outputPanel>
                <apex:outputPanel styleClass="col-sm-10 col-xs-10" layout="block" rendered="{!isOAB && NOT(isOABMember) && NOT(isCommunityManager) && NOT(isReadOnlyUser) }">
                    <div id="subheader-area-tag">Online Advisory Board</div>
                    <a href="{!$Page.OCSUGC_FGABAboutGroup}?fgab=true&g={!oabGroupId}&showPanel=About">{!oabGroupName}</a>
                </apex:outputPanel>
                <div class="col-sm-2 col-xs-2" style="text-align: right; padding-top:10px;">
                    <a href="{!$Page.OCSUGC_FGABAboutGroup}?fgab=true&g={!oabGroupId}&showPanel=About"><i class="icon-subheader-about"></i>About</a>
                </div>
                <apex:outputPanel styleClass="col-sm-2 col-xs-2" style="text-align: right; padding-top:10px;" layout="block" rendered="{!isOAB && (isOABMember || isCommunityManager || isReadOnlyUser)}">
                    <a href="{!$Page.OCSUGC_FGABAboutGroup}?fgab=true&g={!oabGroupId}&showPanel=Members"><i class="icon-subheader-members"></i>Members</a>
                </apex:outputPanel>
            </div>
          </div><!-- /.container-bs -->
        </div><!-- /.sub-header -->

        <div  class="container" style="padding-top:20px; padding-bottom:20px;">
          <c:OCSUGC_WaitingComponent />
          <apex:insert name="body"/>
          <div class="push"></div>
        </div>      
      </div>
      <c:OCSUGC_Footer_NG />
      <c:OCSUGC_GoogleAnalyticsJS />
      <script type="text/javascript">
        function validateLinksInText (value, type) {
          return type !== 'url' || /^((http|ftp)s?|www\.)/.test(value);
        }

        jQuery(document).ready(function(){                    
            $('.description').linkify({
              validate: validateLinksInText
            });

            $('.message').linkify({
              validate: validateLinksInText
            });
            
            $('.needLinkify').linkify({
              validate: validateLinksInText
            });                            
        });
      </script>
    </body>
  </html>
  
</apex:page>
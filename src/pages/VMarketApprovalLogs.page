<apex:page sidebar="false" showHeader="false"
    standardStylesheets="false" controller="VMarketApprovalLogsController">
    <title>vMarket Admin</title>

    <apex:stylesheet value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <apex:stylesheet value="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" />
    <apex:stylesheet value="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <apex:stylesheet value="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" />

    <!-- Load Javascript Files -->
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js" />
    <apex:includeScript value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" />

    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bootgrid/1.3.1/jquery.bootgrid.min.js"></script>
    <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js" />

    <style type="text/css">
        /* common css */
        body {
            font-family: 'Open Sans', sans-serif;
            background-color: #f4f5f9;
            padding: 0px !important;
        }
        
        .clear-fix {
            clear: both;
        }
        
        .margin-top-20 {
            margin-top: 20px;
        }
        
        .page-top-section {
            background: #ebebeb;
            height: 175px;
        }
        
        .page-top-section .container {
            width: 1296px;
            padding-left: 45px;
        }
        
        .page-top-section .pageTitle {
            color: #304a61;
            font-size: 24px;
            font-weight: 600;
            margin-top:30px;
        }
        
        .page-content-section {
            background: #fff;
            min-height: 500px;
            margin-top: -85px;
            padding: 30px 40px 30px 40px;
            width: 1296px;
        }
        /* common css end */
        .bottomFooterWrapper {
            color: #b6c2cb;
            font-size: 12px;
            text-align: center;
            margin-top: 15px;
            margin-bottom: 10px;
        }
        
        .bottomFooterWrapperContainer {
            margin-top: 10px;
        }
        
        /* header styles */
        .topHeader {
            background-color: #f4f5f9;
            height: 100px;
        }
        
        .topHeader .container {
            width: 1296px;
            padding-left: 45px;
            padding-right: 45px;
        }
        
        .vMarketLogoTop {
            background-image: url('{!$Resource.vMarketLogo}');
            background-repeat: no-repeat;
            width: 116px;
            height: 30px;
            margin-top: 31px;
            display: inline-block;
        }
        
        .vMarketLogoTopLink {
            float: left;
        }
        
        .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
            color: #304a61 !important;
            background: #fff;
        }
        
        .dropdown-menu>li>a:focus, .dropdown-menu>li>a:active, .dropdown-menu>li>a:focus,
            .dropdown-menu>li>a:focus {
            color: #304a61 !important;
            background: #fff;
        }
        
        #userLinkWrap {
            float: right;
            margin-top: 35px;
            margin-left: 30px;
            cursor: pointer;
        }
        
        #userLinkWrap .dropdown-menu {
            right: 0;
            left: inherit;
            top: 40px;
            padding-left: 10px;
            padding-right: 10px;
        }
        
        /* header styles end */
        .table>thead>tr>th {
            border-bottom: 2px solid #4279BD;
            color: #4279BD;
        }
        
        .panel-title, .modal-title {
            font-weight: bold;
        }
        
        .modal {
            padding-right: 0 !important;
        }
        
        .approvedAppsWrapper ul.dropdown-menu, .submittedAppsWrapper ul.dropdown-menu
            {
            right: 0 !important;
            left: inherit !important;
        }
        
        .bootgrid-header .actionBar {
            text-align: right;
        }
        
        .bootgrid-header .actionBar .search.form-group {
            display: inline-block;
            width: 30%;
            margin-right: 10px;
        }
        
        .bootgrid-header .actionBar .glyphicon-search {
            background: #2D75B6;
            color: #fff;
        }
        
        .bootgrid-header .actionBar .search-field {
            margin-top: 2px;
        }
        
        .bootgrid-header .actionBar .actions.btn-group {
            display: inline-block;
            vertical-align: top;
        }
        
        .bootgrid-footer .pagination {
            margin: 0;
        }
        
        .bootgrid-footer .infos {
            margin-top: 10px;
            text-align: right;
            font-weight: bold;
        }
        
        .btn_over {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 24px !important;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        
        .baseContainer {
            margin-top: 10px;
            margin-bottom: 10px;
        }
        
        .stylish-input-group .input-group-addon {
            background: white !important;
        }
        
        .stylish-input-group .form-control {
            border-right: 0;
            box-shadow: 0 0 0;
            border-color: #ccc;
        }
        
        .stylish-input-group button {
            border: 0;
            background: transparent;
        }
        
        .pbBottomButtons {
            width: 100%;
            text-align: center;
        }
        
        .pbBottomButtons table {
            display: inline-block;
        }
        
        .disabled {
            background: #eff0f1;
        }
        
        #exTab2 .tab-content {
            padding: 15px 15px;
        }
        
        .nav-tabs, .nav-pills {
            text-align: center;
        }
        
        #headerTab>li>a {
            height: 100px;
            position: relative;
            padding-top: 36px;
            font-family: 'Open Sans', sans-serif;
            font-size: 16px;
            font-weight: 600;
            color: #304a61;
            opacity: 0.4;
            width: 160px;
        }
        
        #headerTab>li>a:hover {
            border-color: transparent;
            background: transparent;
            opacity: 0.4;
        }
        
        #headerTab>li.active>a {
            border: 0;
            border-bottom: 4px solid #178eea;
            background-color: #ebf2fb;
            opacity: 1;
        }
        
        #statusTab {
            border-bottom: 1px solid #ebebeb;
        }
        
        #statusTab>li>a {
            font-family: 'Open Sans', sans-serif;
            font-size: 18px;
            color: #000000;
            width: 150px;
        }
        
        #statusTab>li>a[href='#submitted']:hover {
            border-color: transparent;
            border-bottom: 4px solid #178eea;
            background: transparent;
        }
        
        #statusTab>li>a[href='#approved']:hover {
            border-color: transparent;
            border-bottom: 4px solid #00c485;
            background: transparent;
        }
        
        #statusTab>li>a[href='#underreview']:hover {
            border-color: transparent;
            border-bottom: 4px solid #ffb005;
            background: transparent;
        }
        
        #statusTab>li>a[href='#rejected']:hover {
            border-color: transparent;
            border-bottom: 4px solid #fd5959;
            background: transparent;
        }
        
        #statusTab>li.active>a[href='#submitted'] {
            border-color: transparent;
            border-bottom: 4px solid #178eea;
        }
        
        #statusTab>li.active>a[href='#approved'] {
            border-color: transparent;
            border-bottom: 4px solid #00c485;
        }
        
        #statusTab>li.active>a[href='#underreview'] {
            border-color: transparent;
            border-bottom: 4px solid #ffb005;
        }
        
        #statusTab>li.active>a[href='#rejected'] {
            border-color: transparent;
            border-bottom: 4px solid #fd5959;
        }
        
        .nav-tabs {
            border: 0;
        }
        
        #mydashboard table td:first-child,
        #mydashboard table th:first-child {
            width: 6%;
            text-align: center;
        }
        #allapps table td:first-child,
        #allapps table th:first-child {
            padding-left:10px;
        }        
        
        #ReviewUserModal {
            border-radius: 0px !important;
        }
        
        #ReviewUserModal table {
            border: 1px solid #ebebeb;
            color: #304a61;
            font-size: 15px;
        }
        
        #ReviewUserModal table td {
            border-bottom: 1px solid #fbfbfb;
        }

        #ReviewUserModal table td:first-child,
        #ReviewUserModal table th:first-child {
            width: 6%;
            text-align: center;
        }
        #ReviewUserModal table th {
            border-bottom: 0px !important;
            opacity: 0.7;
            background-color: #f3f3f3;
            color: #666666;
            font-size: 14px;
        }
        
        #ReviewUserModal .close {
            color: #fff;
            z-index: 9999;
            opacity: 0.9;
        }
        
        .appsFilterSelect.bootstrap-select .dropdown-menu {
            border: 0px;
            border-radius: 0px;
        }
        
        a.pagination {
            background-color: #fefefe;
            border: solid 1px #ededed;
            padding: 6px 12px;
            margin: 0px 4px;
            border-radius: 0px;
            text-decoration: none;
        }
        
        a.pagination:hover, a.pagination:active, a.pagination.hover, a.pagination.active
            {
            text-decoration: none;
        }
        
        .disablePage {
            cursor: inherit !important;
            color: rgba(102, 102, 102, 0.6) !important;
        }
        
        .firstpage, .lastpage {
            font-size: 19px;
            padding-top: 6px !important;
            padding-bottom: 8px !important;
        }
        
        .pbBottomButtons {
            text-align: center;
            display: inline-block;
        }
        
        .pbBottomButtons table {
            border: 0px !important;
        }
        
        .baseContainer .input-group-addon {
            background-color: transparent !important;
        }
        
        .baseContainer button {
            background: #fff !important;
            border: 0 !important;
            outline: none !important;
        }
        
        .appsFilterSelect {
            display: inline-block;
        }
        
        .appsFilterSelect.bootstrap-select .dropdown-toggle {
            border: 1px solid #ccc !important;
            outline: none !important;
            border-radius: 4px;
            background: #fff;
            box-shadow: none;
            color: #304a61;
            font-size: 12px;
            width: 165px;
            height: 35px;
        }
        
        .appsFilterSelect.bootstrap-select .dropdown-menu {
            border: 0px;
            border-radius: 0px;
            width: 165px;
        }
        
        .appsFilterSelect .dropdown-menu {
            padding: 0 !important;
            border-radius: 0 !important;
            color: #fefefe;
        }
        
        .appsFilterSelect .dropdown-menu>li>a {
            border: 0;
            outline: none;
        }
        
        .appsFilterSelect ul.dropdown-menu>li:focus, .appsFilterSelect ul.dropdown-menu>li:active,
            .appsFilterSelect ul.dropdown-menu>li:hover {
            border: 0px!improtant;
            /*color: #ccc;*/
        }
        
        .userIconWrapper {
            margin-top: 30px;
            float: right;
        }
        
        #userLinkWrap .dropdown-menu {
            right: 0;
            left: inherit;
            top: 40px;
            padding-left: 10px;
            padding-right: 10px;
        }
        
        #topMenuUserIconWrapper .dropdown-menu>li>a {
            font-size: 15px;
            text-align: left;
            color: #304a61;
            padding: 4px 0px 4px 0px;
            word-wrap: break-word;
            float: left;
        }
        
        #topMenuUserIconWrapper .dropdown-menu>li>a:focus,
            #topMenuUserIconWrapper .dropdown-menu>li>a:hover {
            color: #304a61;
            text-decoration: none;
            background-color: #ffffff;
            font-weight: 600;
        }
        
        #topMenuUserIconWrapper .dropdown-menu {
            /*max-width: 200px;
                                    padding: 5px;*/
            padding: 10px 18px;
            width: 190px;
            margin-top: 15px;
            margin-left: -70px;
            border: 0px;
        }
        
        .up_arrow {
            width: 0;
            height: 0;
            border-left: 15px solid transparent;
            border-right: 15px solid transparent;
            border-bottom: 15px solid #ffffff;
            margin-top: 13px;
        }
        
        .arrow_li {
            position: relative;
            top: -36px;
            line-height: 0px;
            padding: 0;
            list-style: none;
            margin: 0;
            height: 0px;
            margin-left: 42px;
            margin-bottom: -10px;
        }
        
        .nameText {
            font-size: 16px;
            font-weight: 600;
            text-align: left;
            color: #304a61;
        }
        
        .modalHeader {
            font-size: 18px;
            font-weight: bold;
            text-align: left;
            color: #304a61;
            border-bottom: 0px !important;
            margin-bottom: -15px;
        }
        
        .CancelButton {
            width: 117px;
            height: 43px;
            background-color: #7d7d7d;
            font-size: 16px;
            font-weight: 600;
            text-align: center;
            color: #ffffff;
            border: 0;
            outline: none;
        }
        
        .SubmitButton {
            border: 0;
            outline: none;
            width: 117px;
            height: 43px;
            background-color: #178eea;
            font-size: 16px;
            font-weight: 600;
            text-align: center;
            color: #ffffff;
            box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
            text-transform: uppercase;
        }
        
        .MessageModal {
            text-align: center;
        }
        
        .modalText {
            font-family: OpenSans;
            font-size: 18px;
            font-weight: bold;
            color: #304a61;
        }
        
        .MessageModal .modal-dialog {
            width: 400px;
        }
        
        .MessageModal .mainMessageText {
            color: #304a61;
            font-size: 18px;
            font-weight: bold;
        }
        
        .MessageModal .okBtn {
            background-color: #178eea;
            width: 150px;
            color: #fff;
            border-radius: 0;
        }
        
        .modal-content {
            border-radius: 0;
            padding: 30px;
        }
        
        .notify {
            font-size: 16px;
            font-weight: 600;
            text-align: center;
            color: #bac5ce;
        }
        
        .message {
            text-align: center;
            display: inline-block;
            font-size: 18px;
            font-weight: bold;
            /* text-align: left; */
            color: #304a61;
        }
        .emailText {
            font-size: 13px;
            text-align: left;
            color: #304a61;
        }
        .tab-content .tab-pane .table{
            margin-top: 40px;
            border: 1px solid #ededed;
            border-collapse: separate;
        }
        .tab-content .tab-pane .table thead > tr > th{
            height: 41px;           
            background-color: #f3f3f3;
            border-bottom: 0;
            font-size: 14px;
            color: #666;
        }
        .tab-content .tab-pane .table tbody > tr > td{
            border-bottom: 1px solid #ededed;
            border-top: 0px;
        }
        .tab-content .tab-pane .table tbody > tr > td, .tab-content .tab-pane .table tbody > tr > td > a{
            font-size: 15px;
            color: #304a61 !important;
            padding-top: 15px;
            padding-bottom: 15px;           
        }
        .tab-content .tab-pane .table tbody > tr > td > a.actionLink{
            color: #178eea !important;
        }
        #initiateApprovalModal .modal-header{
            padding: 0;
            border-bottom: 0;
        }
        #initiateApprovalModal .modal-header .close{
            margin-top: -10px;
            font-size: 30px;
        }
        #initiateApprovalModal .modal-body{
            padding-top: 0;
        }
        #initiateApprovalModal .chkBoxInput{
            display: inline-block;
        }
        #initiateApprovalModal .chkBoxInput{
            display: inline-block;
            font-size: 16px;
            margin-top: 10px;
        }
        #initiateApprovalModal input[type=checkbox]{
            margin-right: 0;
            width: 15px;
            height: 15px;
            margin-top: 2px;
        }
        #initiateApprovalModal input[type=checkbox] + .label{
            color: #bac5ce;
            font-size: 16px;
            padding: 0;
        }
        #initiateApprovalModal input[type=checkbox]:checked + .label{
            color: #435870;
            font-size: 16px;
            padding: 0;
        }
        #inProgressApps td.infoCell:hover{
            cursor: pointer;
        }
        #inProgressApps .description{
            background-color: #f8f8f8;
            color: #304a61;
            font-size: 13px;
            padding: 10px;
            width: 90%;
            margin-top: 15px;
        }
        #inProgressApps .hiddenDiv{
            visibility: hidden;
            height: 0;
            padding: 0;
            margin: 0;
        }
        #inProgressApps .visibleDiv{
            visibility: visible;
            padding: 10px;
            height: auto;
            margin-top: 15px;
        }
        #inProgressApps .approveBtn, #inProgressApps .rejectBtn{
            border: 0;
            outline: none;
            width: auto;
            height: 30px;
            font-size: 12px;
            font-weight: 600;
            text-align: center;
            color: #ffffff;
            box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
            -webkit-box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
            text-transform: uppercase;
        }
        #inProgressApps .approveBtn{
            background: #178eea;
        }
        #inProgressApps .rejectBtn{
            background: #ff3a47;
        }
        .divLoading {
            margin: 0px;
            padding: 0px;
            position: fixed;
            right: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            background-color: #fff;
            z-index: 30001;
            opacity: 0.8;
        }
        
        .innerLoading {
            position: absolute;
            color: #000;
            top: 40% !important;
            left: 45% !important;
            font-weight: bold;
            font-size: 20px;
            text-align: center;
        }
        
        .innerLoading img {
            margin-bottom: 20px;
            height: 125px;
        }
        
        .vMarketBackIcon {
            background-image: url('/resource/1500627528000/vMarketBackIcon');
            background-repeat: no-repeat;
            width: 30px;
            height: 30px;
            margin-top: 25px;
            cursor: pointer;
            display: inline-block;
            float: left;
        }
    </style>

    <!-- Navigation -->
    <div class="container-fluid topHeader">
        <div class="container ">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="row">
                <div class="col-md-2 col-sm-2">
                    <a class="vMarketLogoTopLink" href="/apex/vMarketLogin"><div
                            class="vMarketLogoTop"></div></a>
                </div>
                <!--<div class="col-md-4 col-sm-4">
                    <ul class="nav nav-tabs responsive hidden-xs hidden-sm"
                        id="headerTab">
                        <li class="active"><a data-toggle="tab" href="#mydashboard">My
                                Dashboard</a></li>
                        <li><a data-toggle="tab" href="#allapps">All Apps</a></li>
                    </ul>
                </div>-->
                <div id="vMarketLoader" class="divLoading" style="display: none;">
                            <p class="innerLoading">
                                <img src="{!$Resource.vMarketLoader}" style="margin-bottom: 20px;" /><br />
                                Please wait...
                            </p>
                        </div>
                    

                <div class="col-md-offset-4 col-md-6 col-sm-6">
                    <a class="vMarketBackIcon" href="javascript:void(0)" onclick="BackToPage()"></a>
                    <apex:outputPanel id="userLinkWrap" styleClass="userIconWrapper"
                         layout="block"><!--  rendered="{!IsAdmin}"-->
                        <div class="dropdown" id="topMenuUserIconWrapper">
                            <a aria-expanded="false" aria-haspopup="true"
                                style="cursor: pointer;" data-toggle="dropdown"> <img
                                src="{!$Resource.vMarketProfileIcon}" />
                            </a>

                            <ul class="dropdown-menu">
                                <li class="arrow_li">
                                    <div class="up_arrow"></div>
                                </li>
                                <li style="cursor: default;">
                                    <!--<div class="nameText">Welcome {!$User.FirstName}&nbsp;{!$User.LastName} You are Viewing Approval Logs for <a href='/{!app.id}'>{!app.name}</a></div>
                                    -->
                                    <div class="emailText">VMS vMarket Admin</div>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/vMarketLogout">Logout</a></li>
                            </ul>
                        </div>
                    </apex:outputPanel>
                    <div class="clear-fix"></div>
                </div>
                <div class="clear-fix"></div>
            </div>
            <div class="clear-fix"></div>
            <!-- Collect the nav links, forms, and other content for toggling -->

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </div>
    
    
    
    
    <apex:outputPanel id="mainContainer"
        layout="block"> <!-- rendered="{!IsAdmin}"  -->
        <div id="content-wrapper">
            <!--<form>-->
                <div class="container-fluid page-top-section">
                    <div class="container">
                        <h3 class="pageTitle pull-left">WELCOME {!$User.FirstName}&nbsp;{!$User.LastName} You are Viewing Approval Logs for <a href='/{!app.id}'>{!app.name}&nbsp;<b>[{!app.ApprovalStatus__c}]</b></a></h3>
                        <div class="breadCrumbWrapper pull-right">
                                    <c:vMarketBreadCrumb />
                                    </div>
                    </div>
                </div>

                <div class="container page-content-section">
                
                
                
                 <div id="underreview" class="tab-pane">

                                            <apex:outputPanel styleClass="pull-right"
                                                rendered="{!AND( NOT(ISNULL(logslist)), logslist.size > 10)}"
                                                style="padding:10px 0px;" layout="block">
                                               
                                               <!-- <button onclick="redirectToPage(3);" class="CancelButton">VIEW
                                                    ALL</button> -->
                                            </apex:outputPanel>
<apex:form >
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Action</th>
                                                        <th>User</th>
                                                        <th>User Type</th>
                                                        <th>Status</th>
                                                        <th>Date</th>
                                                        <th width="30%">Information</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <apex:variable var="rowNum" value="{!1}" />
                                                    <apex:repeat value="{!logslist}" var="log">
                                                    <tr>
                                                        <td><a href="/{!log.id}"> {!rowNum}</a> </td>
                                                        <td>{!log.Action__c}</td>
                                                        <td>{!log.CreatedBy.Name}</td>
                                                        <td>{!log.User_Type__c}</td>
                                                        <td>{!log.Status__c}</td>
                                                        <td>{!log.CreatedDate}</td>
                                                        <td width="30%">    <apex:commandLink onclick="$('#medicalDeviceModal{!log.id}').modal('show');" reRender="none"><i class="fa fa-info-circle" aria-hidden="true"></i></apex:commandLink>&nbsp;  <apex:outputText title="Hover Over Button to View More Text" escape="true" value="{!LEFT(log.Information__c,50)}"></apex:outputText>
                                                                    <div class="modal fade MessageModal" tabindex="-1" role="dialog" id="medicalDeviceModal{!log.id}">
        <div class="modal-dialog" role="document" style="vertical-align: top;">
            <div class="modal-content">
                <div class="modal-body">
                
                 <div class="mainMessageText modalText" style="padding: 15px 0;text-align:left;">
                       
<apex:outputText escape="false" value="{!log.Information__c}"></apex:outputText>

                       
                    </div>
                    <div style="display:block; text-align:center;">
                        <button type="button" class="btn okBtn" data-dismiss="modal">OK</button>
                    </div>
                   
                   </div>
            </div>
        </div>
    </div>
                                                        </td>
                                                    
                                         
   
                                                    </tr><apex:variable var="rowNum" value="{!rowNum + 1}" />
                                                    </apex:repeat>
                                                    
                                                </tbody>
                                                
                                                
                                            </table>
                                            
                                            </apex:form>
                                        </div>
                
                </div>
                
                
                </div>
    
    </apex:outputpanel>
    
    <script type="text/javascript">
    
    
    $(document).ready(function() {
    $('#medicalDeviceModal').modal('hide');
    });
    
    function showhover(a)
    {
    var f = unescape(a);
    console.log(f);
    alert(f);
    }
    
    function BackToPage(){      
        window.location.href = '/apex/VMarketCustomAppApprovalAdminPage';
    }
    
    </script>
    
</apex:page>
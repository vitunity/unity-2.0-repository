<apex:page controller="vMarketContactUs" sidebar="false" showHeader="false" standardStylesheets="false" docType="html-5.0">
    <c:vMarketHeader />
    <style type="text/css">
        ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
            font-size: 12px;  
            color: #bac5ce;
        }
        ::-moz-placeholder { /* Firefox 19+ */
            font-size: 12px;  
            color: #bac5ce;
        }
        :-ms-input-placeholder { /* IE 10+ */
            font-size: 12px;  
            color: #bac5ce;
        }
        :-moz-placeholder { /* Firefox 18- */
            font-size: 12px;  
            color: #bac5ce;
        }
    	.formField{
        	margin-bottom: 30px;
        	border: solid 1px #ededed;
    		padding: 10px;
        }
        .fieldLabel{
        	color: #b6c2cb;
    		font-size: 14px;
        }
        .fieldInput input, .fieldInput textarea{
        	width: 100%;
        	border:0;
        	font-size: 16px;  
  			color: #304a61;
        }
        .submitBtn{
        	background-color: #178eea;
    		width: 150px;
        	height: 40px;
        	color: #fff;
        	display: block;
        	text-align: center;
        	padding-top: 9px;
        	cursor: pointer;
        	text-decoration: none;
        	margin: 20px auto 0 auto;
        }
        .submitBtn:hover, .submitBtn:focus, .submitBtn:visited{
        	text-decoration: none;
        	color: #fff;
        	font-weight: bold;
        }
        
        label.error {
            color: #fff;
            font-size: 0.8em;
            background: #ef4b25;
            padding: 5px;
            border-radius: 3px;
            margin-bottom: 2px;
            margin-right: 16.5%;
            position: relative;
            float: right;
        }
        ul.alert{
        	list-style: none;
        }
        
    </style>
    <div id="content-wrapper">
    	<div class="container-fluid page-top-section">
            <div class="container">
                <h3 class="pageTitle pull-left">CONTACT US</h3>
                <div class="breadCrumbWrapper pull-right">
                    <c:vMarketBreadCrumb />
                </div>
            </div>
        </div>
        <div class="container page-content-section">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div id="contactUsFormWrapper">
                        <apex:outputPanel layout="block" rendered="{!OR(DoesInfoExist, DoesErrorExist)}">
                        	<apex:messages styleClass="alert {!IF(DoesInfoExist, 'alert-success', 'alert-danger')}"/>
                        </apex:outputPanel>
                        <apex:form html-autocomplete="off" id="contactUsForm">
                            <div class="formField">
                                <div class="fieldLabel">Name<sup>*</sup></div>
                                <div class="fieldInput">                                    
                                    <input type="text" id="contactName" name="contactName" placeholder="Enter your Name" />
                                </div>
                            </div>
                            <div class="formField">
                                <div class="fieldLabel">Email/Username<sup>*</sup></div>
                                <div class="fieldInput">                                    
                                    <input type="text" id="contactEmail" name="contactEmail" placeholder="Enter your Email or Username" />
                                </div>
                            </div>
                            <div class="formField">
                                <div class="fieldLabel">Subject<sup>*</sup></div>
                                <div class="fieldInput">                                    
                                    <input type="text" id="contactSubject" name="contactSubject" placeholder="Enter Subject" />
                                </div>
                            </div>
                            <div class="formField">
                                <div class="fieldLabel">Suggestions<sup>*</sup></div>
                                <div class="fieldInput">                                    
                                    <textarea id="contactSuggestions" name="contactSuggestions" placeholder="Enter Suggestions" style="min-height: 100px;"></textarea>
                                </div>
                            </div>
                            <div>
                            	<div class="g-recaptcha" data-sitekey="{!sitekey}"></div>
        						<script src='https://www.google.com/recaptcha/api.js'></script>
                            </div>
                            <div>
                            	<a href="javascript:void(0)" class="submitBtn" onClick="submitContactForm()" id="submitBtn">SUBMIT</a>
                            </div>
                            <apex:actionFunction name="submitContactInfo" action="{!doVerify}" status="contactUsStatus">
                    			<apex:param name="contactName" value="" />
                    			<apex:param name="contactEmail" value="" />
                    			<apex:param name="contactSubject" value="" />
                                <apex:param name="contactSuggestions" value="" />
                			</apex:actionFunction>
                			<apex:actionStatus onstart="contactUsStatus(0);" onstop="contactUsStatus(1);" id="contactUsStatus" />
                        </apex:form>
            		</div>
                </div>   	
            </div>
        </div>
    </div>
    <c:vMarketFooter />
    
    <script type="text/javaScript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
         <script type="text/javaScript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js"></script>
    
    <script>
    	$(function(){
        	$('[id$=contactUsForm]').validate({
            	errorElement: "span",
                wrapper: "span",  // a wrapper around the error message
                errorPlacement: function(error, element) {
                    //offset = element.offset();
                    error.insertBefore(element)
                    error.addClass('label label-danger');  // add a class to the wrapper
                    error.css('position', 'relative');
                    error.css('float', 'right');
                    error.css('margin-top', '-50px');
                    error.css('margin-right', '-10px');
                    // error.css('left', offset.left + element.outerWidth());
                    // error.css('top', offset.top);
                }
            });
            
            $('[id$=contactName]').rules("add",{
                required: true,                           
                 messages:{
                   required:"Please enter your Name",                 
                }                           
            });
            
            $('[id$=contactEmail]').rules("add",{
                required: true,                           
                 messages:{
                   required:"Please enter your email or username",                 
                }                           
            });
            
            $('[id$=contactSubject]').rules("add",{
                required: true,                           
                 messages:{
                   required:"Please enter subject",                 
                }                           
            });
            
            $('[id$=contactSuggestions]').rules("add",{
                required: true,                           
                 messages:{
                   required:"Please enter suggestions",                 
                }                           
            });
            
            
        });
    
    	function submitContactForm() {
            var contactName = $('[id$=contactName]').val();
            var contactEmail = $('[id$=contactEmail]').val();
            var contactSubject = $('[id$=contactSubject]').val();
            var contactSuggestions = $('[id$=contactSuggestions]').val();
            
            if( $('[id$=contactUsForm]').valid() ){
            	submitContactInfo(contactName, contactEmail, contactSubject, contactSuggestions);
            }else{
            	return false;
            }    
    	}
    
    	function contactUsStatus(flag){
            if(flag)
            {    
                alert('email sent');
                $('#vMarketLoader').hide(); 
            }    
            else
            {    
                alert('submit');
                $('#vMarketLoader').show();
            }    
        }
    	
    
    </script>
</apex:page>
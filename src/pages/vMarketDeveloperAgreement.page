<apex:page sidebar="false" showHeader="false" standardStylesheets="false" docType="html-5.0">
    
    <apex:stylesheet value="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css" />
    <apex:stylesheet value="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" />
    <apex:stylesheet value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <apex:stylesheet value="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" />        
    <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" />
    <apex:includeScript value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" />
    <apex:stylesheet value="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <apex:stylesheet value="{!URLFOR($Resource.vMarketResources, 'vMarketResources/css/vMarketStarRating.css')}" />
    <apex:includeScript value="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js" />
    
    <style>
        body {
            font-family: 'Open Sans', sans-serif;            
        }
    
        *::-ms-clear {
            display: none;
        }
    
        *{
            outline:none;
        }
        
        a{
            text-decoration:none;            
        }
        a:hover{
            text-decoration:none;            
        }
        .termsNcond {            
            opacity: 0.5;            
            font-size: 18px;            
            text-align: left;
            color: rgba(48, 74, 97, 0.5);
            margin-bottom: 20px;
        }
        .logoDiv{
            text-align: center;
        }
        .vMarketLogoTop {
            background-image: url('{!$Resource.vMarketLogo}');
            background-repeat: no-repeat;
            width: 116px;
            height: 30px;
            margin-top: 20px;
            display: inline-block;
        }
        .dropdownItem{
            display: block;
            width: 100%;
            padding: 0px 1.5rem;
            clear: both;
            font-weight: 400;
            color: #292b2c;
            text-align: inherit;
            white-space: nowrap;
            background: 0 0;
            border: 0;
        }
        #archivedVersionsBtn{
            background-color: #178eea;
            border: 0;
            border-radius: 0;
        }
        .dropdownItem:hover{
            font-weight: 600;
        }
    </style>
    
    <apex:outputPanel id="content-wrapper" layout="block" >
        
        <div class="container-fluid page-top-section" style="background: #ebebeb;height: 80px;">
            <div class="container ">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="row">
                    <div class="logoDiv">
                        <a class="vMarketLogoTop" href="/vMarketLogin"></a>
                    </div>
                </div>
            </div>
        </div>
        
        <apex:outputPanel layout="block" Styleclass="container page-content-section appDetailsWrapper" >
            
            <apex:outputText value="{!$Label.vMarket_DevAgreementInductory}" style="font-size:10px;"/>
            <div class="row" Style="margin-top: 10px;font-size:24px;text-align:left;color:#304a61;padding:-10px 0;">
                <div class="col-md-6">Developer {!$Label.vMarket_DeveloperAgreement}</div>
            </div>
            <div class="clear-fix"></div>            
            <div class="row termsNcond">
                <div class="col-md-12">{!$Label.vMarket_TermsNCond}</div>
            </div>
            <div class="clear-fix"></div>
            <div id="tc_content" style="margin-bottom: 20px;">                
                <div class="panel-content">
                    <div id="privacy_statement" style="height: 400px;">
                        <iframe src="{!URLFOR($Resource.vMarket_DeveloperAgreement)}" id="privacy_statement_iframe" style="overflow-y:scroll;height:100%;width:100%;border:1px solid #ccc;" height="100%" width="100%"></iframe>
                    </div>
                </div>  
            </div><div class="clear-fix"></div>
            <script>
                window.onload = function() {                
                    var iframeBody = $('body', $('#privacy_statement_iframe')[0].contentWindow.document);                
                    $(iframeBody).find('.regTerms').on('click',function(){                  
                        $('#regtermsModal').modal('show');
                    });
                    $(iframeBody).find('.techTerms').on('click',function(){                 
                        $('#techtermsModal').modal('show');
                    });
                    $(iframeBody).find('.progTerms').on('click',function(){                 
                        $('#progtermsModal').modal('show');
                    });
                    $(iframeBody).find('.termsOfUse').on('click',function(){
                    });
                }
            </script>
            
            <!-- Regulatory Terms Modal -->
            <div class="modal fade" id="regtermsModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="width: ">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                    
                        </div>
                        <div class="modal-body">
                            <iframe src='./vMarket_RegulatoryTerms' width="100%" height="400px" id="theIframe" name="theIframe" frameBorder="0"/>
                        </div>                  
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            
            <!-- The Technical Requirements Modal -->
            <div class="modal fade" id="techtermsModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="width: ">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                    
                        </div>
                        <div class="modal-body">
                            <div id="techterms_guide" style="font-size:90%;display:block;height:600px;overflow-y:scroll;width=100%;padding-right:20px;">&nbsp;</div>
                            <script>
                                $("#techterms_guide").load("{!URLFOR($Resource.vMarket_RapidPlanDevGuide)}");
                            </script>
                        </div>                  
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            
            <!-- The Program terms Modal -->
            <div class="modal fade" id="progtermsModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="width: ">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                    
                        </div>
                            <div class="modal-body">
                                <div id="progterms_guide" style="font-size:90%;display:block;height:600px;overflow-y:scroll;width=100%;padding-right:20px;">&nbsp;</div>
                                <script>
                                    $("#progterms_guide").load("{!URLFOR($Resource.vMarketplace_ProgramTerms)}");
                                </script>
                        </div>                  
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            
        </apex:outputPanel>
        
    </apex:outputPanel>
    <c:vMarketDevFooter />
</apex:page>
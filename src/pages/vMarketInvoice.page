<!-- Page Modified by Abhishek K as Part of Globalisation VMT 25-->
<apex:page sidebar="false" showHeader="false"
    standardStylesheets="false" renderAs="pdf"
    controller="vMarketOrderDetailController">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>vMarket Invoice</title>

        <apex:stylesheet value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <apex:stylesheet value="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,600italic,700,700italic" />
        <apex:stylesheet value="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans"
            rel="stylesheet" />
        <apex:stylesheet value="{!$Resource.vMarketInvoice}" />


    </head>
    <body>
        <div class="container-fluid header">
            <div class="col-md-12">
                <div id="header" class="clearfix" style="margin-bottom: 10px;">
                    <div id="logo" style="float: left;margin-top:2px;">
                        <apex:image id="invoice_logo" value="{!$Resource.vMarketLogo}" alt="headerLOGO" style="width: 116"/>
                    </div>
                    <!--
                    <div id="varian_logo_wrap" style="float:right;">
                        <apex:image id="varian_logo" value="{!$Resource.vMarketInvoiceLogo}" alt="InvoiceLOGO" style="width: 116; display:none;"/>
                    </div>
                    -->
                    <apex:outputPanel rendered="{!nonUS}">
                    <div id="varian_logo_wrap" style="float:left;margin-left: 120px;font-weight:bold;font-size:24px;">
                      ** International Invoice **
                    </div>
                    </apex:outputPanel>
                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>

        <div class="container-fluid orderInfo">
            <div class="row">
                <div class="col-md-12">
                    <div class="address col-md-4" style="width:33.33%;float:left;">
                        <div class="invoice_title" style="margin-bottom:10px;">INVOICE TO:</div>
                        <div class="invoice_name">{!$User.FirstName}&nbsp;&nbsp;{!$User.LastName}</div>
                        <div class="invoice_address">{!orderInfo.ShippingAddress__c}</div>
                        <div class="invoice_email">{!$User.Email}</div>
                    </div>
                    <div class="invoice_details col-md-offset-3 col-md-4 pull-right" style="width:40%; float:right;">
                        <div class="col-md-6 text-center invoice_header" style="width:50%; float:left;margin-bottom:5px;">Invoice #</div>
                        <div class="col-md-6 text-center invoice_header"  style="width:50%;float:right;margin-bottom:5px;">DATE</div>
                        <div class="col-md-6 text-center invoice_header_info"  style="width:50%;float:left;">
                        
                        <apex:outputPanel rendered="{!NOT(subscriptionPayment)}"> 
                        {!orderInfo.Name}
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!subscriptionPayment}"> 
                        {!subPaymentNumber}
                        </apex:outputPanel>
                        
                        </div>
                        
                        <apex:outputPanel rendered="{!NOT(subscriptionPayment)}"> 
                        <div class="col-md-6 text-center invoice_header_info"  style="width:50%;float:right;">
                            <apex:outputText value="{0, date, d MMMM','  yyyy}">
                                <apex:param value="{!orderInfo.OrderPlacedDate__c}" />
                            </apex:outputText>
                        </div>
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!subscriptionPayment}"> 
                        <div class="col-md-6 text-center invoice_header_info"  style="width:50%;float:right;">
                            <apex:outputText value="{0, date, d MMMM','  yyyy}">
                                <apex:param value="{!subPaymentDate}" />
                            </apex:outputText>
                        </div>
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!nonUS}">                            
                            <div class="col-md-6 text-center invoice_header" style="width:90%;float:left;margin-bottom:5px;border-top:1px solid #fff;padding-top:5px;margin-top:5px;">Customer VAT # : <span style="font-weight:bold;font-size:16px;">{!VATNumber}</span></div>
                        </apex:outputPanel>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>

        <table class="itemTable" style="width:100%;border-collapse:collapse;border-spacing: 0;" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="background-color: #304a61;line-height: 3;color: #fff;font-size: 18px;font-weight: 600;width: 20%;padding-left: 20px;border-color: #304a61;border-width: 0px;">REF</th>
                    <th style="background-color: #304a61;line-height: 3;color: #fff;font-size: 18px;font-weight: 600;width: 50%;border-color: #304a61;border-width: 0px;">ITEM DESCRIPTION</th>
                    <th style="background-color: #304a61;line-height: 3;color: #fff;font-size: 18px;font-weight: 600;width: 30%;padding-right: 30px;text-align: right;border-color: #304a61;border-width: 0px;">PRICE (USD) for {!orderInfo.VMarket_Duration_Type__c}</th>
                </tr>
            </thead>
            <tbody>
                <apex:variable var="count" value="{!1}"   />  
                <apex:repeat value="{!details}" var="key">
                   <!-- <apex:repeat value="{!details[key]}" var="detail">-->
                        <tr style="background:{!IF(MOD(count,2)==0,'#f4f4f4','#fff')};">
                            <td style="width: 20%;padding-left: 20px;padding: 20px 0px 20px 45px;font-size: 14px;font-weight: 600;color: #3e3e3e;border: 0;border-width: 0px;">{!count}</td>
                            <td style="width: 50%;padding: 20px 0px 20px 0px;font-size: 14px;font-weight: 600;color: #3e3e3e;border: 0;border-width: 0px;">
                                <div>{!key}</div>
                                <!-- <div class="subtext" style="font-size: 14px;font-weight: normal;color: #8b8f8f;">Lorem Ipsum is simply dummy text of
                                    the printing and typesetting industry. Lorem Ipsum has been
                                    the industry's standard dummy text ever since the 1500s,</div>-->
                            </td>
                            <td style="width: 30%;padding: 20px 30px 20px 0px;text-align: right;font-size: 14px;font-weight: 600;color: #3e3e3e;border: 0;border-width: 0px;">USD&nbsp;
                             <!--   <apex:repeat value="{!orderItemLineMap[detail.appSrc.vMarket_App__c]}" var="OrderItemLinePrice"> -->
                                    <apex:outputText label="NRR" value="{0,number}">
                                        <apex:param value="{!orderItemLineMap[details[key][0].appSrc.vMarket_App__c]}" />
                                    </apex:outputText>
                             <!--   </apex:repeat> -->
                            </td>
                        </tr>
                        <apex:variable value="{!(count+1)}" var="count"/>
                   <!-- </apex:repeat>-->
                </apex:repeat>


                <!-- Total along with TAX -->
                <tr class="subtotal" style="background: #eaeaea;font-size: 14px;font-weight: 600;color: #3e3e3e;border: 0;">
                    <td colspan="2" style=" text-align: right;padding: 20px 50px 5px 0px;border: 0;">SUBTOTAL</td>
                    <td style=" text-align: right;padding: 20px 30px 0px 0px;border: 0;">USD&nbsp;<apex:outputText label="NRR" value="{0,number}">
                            <apex:param value="{!orderInfo.SubTotal__c}" />
                        </apex:outputText></td>
                </tr>
                <tr class="tax" style=" background: #eaeaea;font-size: 14px;font-weight: 600;color: #3e3e3e;padding-bottom: 10px;border: 0;">
                    <td colspan="2" style=" text-align: right;padding: 10px 50px 20px 0px;text-transform: uppercase;border: 0;">TAX {!orderInfo.Tax__c}%</td>
                    <td style="text-align: right;padding: 10px 30px 20px 0px;border: 0;">USD&nbsp;<apex:outputText label="NRR" value="{0,number}">
                            <apex:param value="{!orderInfo.Tax_price__c}" />
                        </apex:outputText></td>
                </tr>

                <!-- GrandTotal -->
                <tr class="grandTotal" style="padding: 0;border: 0;">
                    <td colspan="2" style=" text-align: right;font-size: 18px;font-weight: 600;text-transform: uppercase;color: #fff;padding: 0;border: 0;">
                        <div class="grandTotalText" style=" padding: 20px 50px 20px 0px;background-color: #178eea;width: 30%;float: right;">GRAND TOTAL</div>
                        
                    </td>
                    <td style=" text-align: right;font-size: 18px;font-weight: 600;color: #fff;background-color: #178eea;padding: 0;border: 0;">
                        <div class="grandTotalVal" style="padding: 20px 30px 20px 0px;">
                            USD&nbsp;<apex:outputText label="NRR" value="{0,number}">
                                <apex:param value="{!orderInfo.Total__c}" />
                            </apex:outputText>
                        </div>
                    </td>
                </tr>
                <tr class="grandTotal" style="padding: 0;border: 0;">                    
                    <td colspan="3" style="text-align: left;font-size: 18px;font-weight: 600;text-transform: uppercase;color: #fff;padding: 0;border: 0;padding-left:300px;">
                    <apex:outputPanel rendered="{!nonUS}">
                        <div style=" padding: 20px 50px 20px 0px;color:#000;">Reverse Charge</div>                        
                    </apex:outputPanel>
                    </td>           
                </tr>
            </tbody>
        </table>

        <div class="Footer container-fluid">
            <div class="row">
                <div class="bottomFooterText">Thank you for DOING Business
                    with us!</div>
            </div>
        </div>
        <div class="FooterBox container-fluid">
            <div class="row">
                <div class="FooterTerms">This is a computer generated invoice. No signature required.</div>
            </div>
        </div>

        <script>
            $('table thead tr th').css('border', 0);
            $('table tbody tr td').css('border', 0);
            $('table').css('border', 0);
        </script>
    </body>
</apex:page>
<apex:page showHeader="false" sidebar="false"
	standardStylesheets="false">
	<c:vMarketDevHeader />
	<style>
		/* Page content styling */
		.stepTitleText {
			font-size: 16px;
			color: #5c5c5c;
			font-weight: 600;
			margin-bottom: 20px;
		}
		
		.padLeft20 {
			padding-left: 20px;
		}
		
		.outerBox {
			border: solid 1px #ebebeb;
			padding-top: 20px;
			padding-bottom: 20px;
			margin-bottom: 10px;
		}
		
		.question {
			font-size: 16px;
			text-align: left;
			color: #435870
		}
		
		.noteBox {
			margin-top: 20px;
			margin-bottom: 20px;
		}
		
		.question .title {
			font-weight: 600;
			color: #304a61;
		}
		
		hr {
			background-color: #ebebeb;
		}
		
		.subquestion {
			padding-left: 40px;
		}
		
		.titletext {
			font-size: 16px;
			text-align: left;
			color: rgba(48, 74, 97, 0.5);
		}
		
		textarea {
			font-size: 14px;
			text-align: left;
			color: #666666;
			border: 0 !important;
			margin-top: 20px;
			box-shadow: none !important;
			padding: 0px !important;
			background: #fff !important;
		}
		
		.disclaimer {
			font-size: 16px;
			text-align: left;
			color: #304a61;
			top: -8px;
			position: relative;
			left: 15px;
		}
		
		.declineButton {
			background-color: #f4f5f9;
			box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
			-moz-box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
			-webkit-box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
			width: 150px;
			height: 38px;
			display: inline-block;
			color: #304a61;
			text-align: center;
			padding-top: 8px;
			cursor: pointer;
			text-decoration: none;
			font-weight: 600;
			text-transform: uppercase;
			margin-bottom: 30px;
		}
		
		.acceptButton {
			background-color: #178eea;
			box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
			-moz-box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
			-webkit-box-shadow: 0px 5px 10px 0 rgba(0, 0, 0, 0.2);
			width: 150px;
			height: 38px;
			display: inline-block;
			color: #fff;
			text-align: center;
			padding-top: 8px;
			cursor: pointer;
			text-decoration: none;
			font-weight: 600;
			text-transform: uppercase;
			margin-bottom: 30px;
			margin-right: 20px;
		}
		
		.acceptButton:hover, .acceptButton:active, .acceptButton:focus {
			color: #fff;
			text-decoration: none;
			outline: none;
		}
		
		.declineButton:hover, .declineButton:active, .declineButton:focus {
			color: #304a61;
			text-decoration: none;
			outline: none;
		}
		
		.yesText {
			position: relative;
			top: -10px;
			left: 10px;
			margin-right: 30px;
		}
		
		.noText {
			position: relative;
			top: -10px;
			left: 10px;
			margin-right: 30px;
		}
		
		#holder {
			width: 100%;
		}
		
		#holder>div {
			clear: both;
			padding: 2%;
			margin-bottom: 20px;
			border-bottom: 1px solid #eee;
			float: left;
			width: 96%;
		}
		
		label {
			display: inline;
		}
		
		.regular-checkbox {
			display: none;
		}
		
		.regular-checkbox+label {
			background-color: #fafafa;
			border: 1px solid #cacece;
			box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px
				rgba(0, 0, 0, 0.05);
			padding: 9px;
			border-radius: 3px;
			display: inline-block;
			position: relative;
		}
		
		.regular-checkbox+label:active, .regular-checkbox:checked+label:active {
			box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px 1px 3px
				rgba(0, 0, 0, 0.1);
		}
		
		.regular-checkbox:checked+label {
			background-color: #fff;
			border: 1px solid #178eea;
			box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px
				rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
			color: #99a1a7;
		}
		
		.regular-checkbox:checked+label:after {
			content: '\2714';
			font-size: 14px;
			position: absolute;
			top: 0px;
			left: 3px;
			color: #178eea;
		}
		
		.big-checkbox+label {
			padding: 18px;
		}
		
		.big-checkbox:checked+label:after {
			font-size: 28px;
			left: 6px;
		}
		
		.tag {
			font-family: Arial, sans-serif;
			width: 200px;
			position: relative;
			top: 5px;
			font-weight: bold;
			text-transform: uppercase;
			display: block;
			float: left;
		}
		
		.radio-1 {
			width: 193px;
		}
		
		.button-holder {
			float: left;
		}
		
		/* RADIO */
		.regular-radio {
			display: none;
		}
		
		.regular-radio+label {
			-webkit-appearance: none;
			background-color: #fff;
			border: 1px solid #cacece;
			box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px
				rgba(0, 0, 0, 0.05);
			padding: 9px;
			border-radius: 50px;
			display: inline-block;
			position: relative;
			margin-top: 10px;
		}
		
		.regular-radio:checked+label:after {
			content: ' ';
			width: 12px;
			height: 12px;
			border-radius: 50px;
			position: absolute;
			top: 3px;
			background: #178eea;
			box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.3);
			text-shadow: 0px;
			left: 3px;
			font-size: 32px;
			border-color: #178eea;
			opacity: 0.8;
		}
		
		.regular-radio:checked+label {
			background-color: #e9ecee;
			color: #99a1a7;
			border: 1px solid #adb8c0;
			box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px
				rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1),
				inset 0px 0px 10px rgba(0, 0, 0, 0.1);
		}
		
		.regular-radio+label:active, .regular-radio:checked+label:active {
			box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px 1px 3px
				rgba(0, 0, 0, 0.1);
		}
		
		.big-radio+label {
			padding: 16px;
		}
		
		.big-radio:checked+label:after {
			width: 24px;
			height: 24px;
			left: 4px;
			top: 4px;
		}
	</style>

	<div id="content-wrapper">
		<div class="container-fluid page-top-section">
			<div class="container">
				<h3 class="pageTitle pull-left">REGULATORY TERMS</h3>
			</div>
		</div>

		<div class="container page-content-section">
			<!-- <div class="stepTitleText">Step 2 - Upload Assests</div>
			<div class="col-md-12 outerBox">
				<div class="col-md-12 question">
					<span class="title">Q1.&nbsp;&nbsp;</span>Is it a medical device
					app ?
				</div>
				<div class="col-md-12">
					<input type="radio" id="medical_device_yes" name="medical_device" value="yes" class="regular-radio"/><label for="medical_device_yes"></label><span class="yesText">YES</span>
					<input type="radio" id="medical_device_no" name="medical_device" value="no" class="regular-radio"/><label for="medical_device_no"></label><span class="noText">NO</span>
					<br/>
				</div>
				
				<div id="k_number_id" class="noteBox">
					<div class="col-md-12 question">
						<span class="title">Enter K-number</span>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" class="form-control" id="kNumber" name="k-number" style="margin-top:20px;"/>
					</div>
				</div>

				<div id="opinion_id" class="noteBox">
					<div class="col-md-12 question">
						<span class="title">Please add expert opinion</span>
					</div>
					<div class="col-md-8 form-group">
						<textarea class="form-control" rows="5" id="opinion" style="border: 1px solid #ccc !important;font-size:14px;"></textarea>
					</div>
				</div>
			</div>-->
            
            <div>
                <!--<div id="regulatory_terms" style="font-size:90% ;display: block;" >&nbsp;</div>-->
                <span class="col-md-12 question">
                	<p><strong>APPLICATION SUBMISSION REGULATORY REQUIREMENTS</strong></p>
                    <p>Applications must comply with all legal requirements in any location where you make them available (if you&rsquo;re not sure, check with a lawyer). Regulatory requirements are complicated, but it is your responsibility to understand and make sure your Application conforms with all local laws, not just the requirements below. And of course, Applications that solicit, promote, or encourage criminal or clearly reckless behavior will be rejected.&nbsp;</p>
                    <strong>1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>HEALTH AND MEDICAL DATA APPLICATIONS </strong><br/>
                    1.1&nbsp;&nbsp;&nbsp;&nbsp; <u>Privacy</u>.&nbsp; Health and medical data are especially sensitive and Applications in this space have additional rules to make sure Customer privacy is protected:
                    <p>(i) Applications may not use or disclose to third parties data gathered in the health and medical research context&mdash;including from health-related human subject research&mdash;for advertising or other use-based data mining purposes other than improving health management, or for the purpose of health research, and then only with permission.</p>
                    <p>(ii) Applications must not write false or inaccurate data into any other medical research or health management applications, and may not store personal health information in vMarket.</p>
                    <p>(iii) Applications conducting health-related human subject research must obtain consent from participants or, in the case of minors, their parent or guardian. Such consent must include the (a) nature, purpose, and duration of the research; (b) procedures, risks, and benefits to the participant; (c) information about confidentiality and handling of data (including any sharing with third parties); (d) a point of contact for participant questions; and (e) the withdrawal process.</p>
                    <p>(iv) Applications conducting health-related human subject research must secure approval from an independent ethics review board. Proof of such approval must be provided upon request.</p>
                    <strong>2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>APPLICATIONS REGULATED BY FEDERAL AGENCIES </strong>
                    <p>2.1&nbsp;&nbsp;&nbsp;&nbsp; <u>Regulatory Clearance/Approval/License/Registration</u>.&nbsp; Many Varian Products are regulated as medical devices by government authorities in many countries where they are made available and require government agency approval prior to marketing or offering the product for sale. The determination of whether an Application is subject to regulation as a medical device or as an accessory to a Varian Product under local laws is the responsibility of the Developer.&nbsp; The application submission process may include a review of the intended use of the Application, the markets in which the Application may be offered, its regulatory classification as determined by the Developer, and if required, the device listing number and evidence of government agency approval, e.g. clearance number, approval letter, license, registration, etc.</p>
                    <p>&nbsp;</p>
                </span>
            </div>

			<div class="col-md-12 outerBox">
				<div class="col-md-12 question">
					<span class="title">Q.&nbsp;&nbsp;</span>What is a <b>medical device</b>?
				</div>
				<div class="col-md-12 question">
					<p><span class="title">A.&nbsp;&nbsp;</span>FDA has information available at <a href="http://www.fda.gov/AboutFDA/Transparency/Basics/ucm211822.htm" target="_blank">http://www.fda.gov/AboutFDA/Transparency/Basics/ucm211822.htm</a>.  When in doubt, seek expertise from a qualified <b>medical device</b> regulatory affairs professional.</p>
					<hr/>
				</div>
				<div class="col-md-12 question">
					<span class="title">Q.&nbsp;&nbsp;</span>What is a <b>510(k)</b>?
				</div>
				<div class="col-md-12 question">
					<p><span class="title">A.&nbsp;&nbsp;</span>Refer to learning modules at FDA’s CDRHLearn website <a href="http://www.fda.gov/Training/CDRHLearn/default.htm" target="_blank">http://www.fda.gov/Training/CDRHLearn/default.htm</a>.  Available there are learning modules on The <b>510(k)</b> Program, also known as pre-market notification.</p>
					<hr/>
				</div>
				<div class="col-md-12 question">
					<span class="title">Q.&nbsp;&nbsp;</span>How can I find a qualified <b>medical device</b> regulatory affairs professional?
				</div>
				<div class="col-md-12 question">
					<p><span class="title">A.&nbsp;&nbsp;</span>Your institution may have a Department of Regulatory Affairs. The Regulatory Affairs Professionals Society (RAPS) publishes a list of certified regulatory affairs professionals.  There are many qualified firms and individuals with <b>medical device</b> regulatory affairs expertise.  The RAPS website is <a href="www.raps.org" target="_blank">www.raps.org</a>.</p>
				</div>
			</div>
		</div>

	</div>
	<c:vMarketDevFooter />

	<script>
    	$("#regulatory_terms").load("{!URLFOR($Resource.vMarket_RegulatoryRequirements)}");
        $(document).ready(function() {
                    $("#k_number_id").hide();
                    $("#opinion_id").hide();
    
            $("input:radio[name='medical_device']").change(function(e) {
                e.preventDefault();
                var isSelected = $(this).val();
                if (isSelected==="yes") {
                    $("#k_number_id").show();
                    $("#opinion_id").hide();
                } else if (isSelected==="no") {
                    $("#k_number_id").hide();
                    $("#opinion_id").show();
                } else {
                    $("#k_number_id").hide();
                    $("#opinion_id").hide();
                }
            });
        });
	</script>

</apex:page>
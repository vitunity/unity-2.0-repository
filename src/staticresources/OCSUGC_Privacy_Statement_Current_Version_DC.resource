<div id="privacy">
	<h1 style="font-weight: bold;">PRIVACY STATEMENT</h1>
	<h1 style="font-weight: bold;">Last Modified: April 23, 2015 &nbsp;
	<a href="/OCSUGC/OCSUGC_archived_privacy_statement?dc=true" target="_blank">(view archived versions)</a></h1>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Table of Contents</h1>
	<ul>
		<li>Personal Information We May Collect</li>
		<li>Use of Personal Information</li>
		<li>Sharing and Transfer of Personal Information</li>
		<li>Educational Information</li>
		<li>Use of Our Online Communities and Forums</li>
		<li>Statement of E-Mail Privacy Practices</li>
		<li>Children's Privacy</li>
		<li>Security</li>
		<li>Access to Your Information</li>
		<li>Questions and Contacts</li>
		<li>Revisions, Updates and Changes</li>
	</ul>
	
	<p>This Privacy Statement (the "Privacy Statement") describes how Varian Medical Systems, Inc. ("Varian") collects, handles and uses your Personal Information when you access or use Varian's online products, software, software-as-a-service, services and/or websites (collectively "Varian's Services"). <br/><br/>
	By registering, accessing and/or using Varian's Services, or through any other affirmative action, such as ticking relevant check boxes or amending relevant account settings, you consent to the collection, use and disclosure of information in accordance with this Privacy Statement and the <a style="color:blue;cursor:pointer;text-decoration: none;" onClick="$('#ocsugcterms').click();$('#tc_content').scrollTop(0);">Terms of Use</a>. Please therefore read the entire Privacy Statement and Terms of Use before accessing, using or submitting information on any of Varian's Services. You may contact us (see the "Questions and Contacts" section below) should you have any questions or require clarification.<br/><br/>
	Varian's products and Services are diverse and therefore you may be subject to any additional terms, terms of use, privacy statements and/or other policies that may apply. If you do not agree to the Privacy Statement and Terms of Use, you should not use or access Varian's Services. Terms not otherwise defined herein shall have the definition, if any, set forth in the Terms of Use.
	</p>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Personal Information We May Collect</h1>
		<p>In this Privacy Statement, "Personal Information" means any information relating to any identified or identifiable individual that is sufficient to identify such person, directly or indirectly, in particular by reference to an identification number or to one or more factors specific to his or her physical, physiological, mental, economic, cultural or social identity.<br/><br/>
		The types of Personal Information we may collect are as follows:
		</p>
		
		<b><em>Information You Provide Directly to Us</em></b>
		<p>You may give us information about you by filling in forms when signing up for or receiving Varian's Services, or by corresponding with us by phone, e-mail or otherwise. For example, in order to access or use Varian's Services you will be required to complete a registration process. The Personal Information that we collect as part of the registration process may include, by way of example, your name, address, e-mail address, password, title, location, company name and other Personal Information. <br/><br/>
		If you choose to withhold any information requested by us during the registration process, you will not be granted access to Varian's Services.   Similarly, you may provide Personal Information if you participate in discussion boards or other social media functions available on Varian's Services and when you report a problem with Varian's Services.<br/><br/>
		If you are using any of Varian's Services that allow you to make an online purchase of products, software or services, we may collect additional information such as your credit card information, billing address, and other billing account information.
		</p>
		
		<b><em>Information We Receive From Other Sources</em></b>
		<p>We may receive information about you if you use any of the other websites we operate or the other services we provide.  We may also work closely with third parties (including, for example, business partners and sub-contractors for technical, payment and delivery services, analytics providers, or search information providers) and may receive information about you from them. <br/></p>
		
		<p Style="border: 1px solid;">We may combine this information with information you give to us and information we otherwise collect about you.</p>
		
		<b><em>Collection and Use of Other Information About You; Cookies</em></b>
		<p>We collect other Personal Information to help us understand how you use Varian's Services.  For example, each time you visit our website, we may automatically collect your IP address, browser type, browser, browser language and computer type, access time and date, the web page from which you came, and the web page(s) you access during your visit as well as other web server log files.  This data is collected, in particular, to protect and enhance the operation of Varian's Services.</p>
		<p style="border: 1px solid">In particular, if access is via a web browser, we may use "cookies" or other local storage -based web tracking technology to store and sometimes track information about our online customers and users.</p>
		<p>A cookie is a small amount of data that is sent to your browser from a web server and stored on your computer's hard drive. Cookies can make use of the web easier by saving status and preferences about a website.  Some parts of Varian's Services that prompt you to log-in or that are customizable may require that you accept cookies.  Cookies by themselves cannot be used to find out the identity of any user.  Most browsers are initially set to accept cookies but users of most browsers may change the setting to refuse cookies or to be alerted when cookies are being sent.</p>
		
		<b><em>What Happens If We Cannot Collect Your Personal Information?</em></b>
		<p>If you do not provide us with the personal information described above, some or all of the following may happen:
		<br/>
			<li>we may not be able to provide the Varian Services or any other requested products or services to you, either to the same standard or at all; </li>
			<li>we may not be able to provide you with information about our products or services that you may be interested in; or</li>
			<li>we may be unable to tailor the content of the Varian Services to your preference and your experience of the Varian Services may not be as enjoyable or useful.</li>
		</p>
		
		<b><em>Account Access and Controls</em></b>
		<p>When using Varian's Services, you should not provide any other Personal Information that is not specifically requested by Varian. You may update, modify, add or delete previously submitted information. If your information (such as your e-mail address) should change, please update all such information regularly, so that your pertinent information is up-to-date.   Access to your password is protected. You should not share your password with anyone, and you should periodically update your credentials.  Varian will never ask you for your password in an unsolicited telephone call or e-mail.</p>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Use of Personal Information</h1>
	<p>Any information that you provide when you access or use Varian's Services, including personal or non-personal information, may be read, collected and used by those that you have granted access to read your postings or with whom you have communicated and shared information.</p>
	<div style="border: 1px solid">
		<p>By accessing and using Varian's Services and providing your personal information, you grant Varian the right to use this information for reasonable business purposes, including without limitation, to:
			<ul>
				<li>Carry out our obligations arising from any contracts entered into between you and us and to provide you with the information, products and services that you request from us,</li>
				<li>Communicate with you,</li>
				<li>Further personalize your experience on Varian's Services,</li>
				<li>Manage accounts, </li>
				<li>Comply with applicable laws and regulations, </li>
				<li>Protect or exercise our legal rights or defend against legal claims,</li>
				<li>Protect the operation of Varian's Services and properties, </li>
				<li>Develop and improve programs, products, services, software and content.</li>
			</ul>
		</p>
	</div>
	<br/><br/>
	
	<div style="border:1px solid;">
		<center><h1 style="font-weight: bold; text-decoration: underline;">Sharing and Transfer of Personal Information</h1></center>
		<p>We do not disclose your Personal Information (that is not first de-identified) to any third party, except as set forth in this Privacy Statement and the Terms of Use.  However, we may disclose your Personal Information to third parties in the event (i) that we sell or buy any business or assets, in which case we may disclose your Personal Information to the prospective seller or buyer of such business or assets, or (ii) if we, or substantially all of our assets, are acquired by a third party, in which case Personal Information held by us about you may be one of the transferred assets.<br/><br/>
		We may, from time to time, provide your Personal Information to our affiliates or other trusted business partners to process it for us, based on our instructions and in compliance with our Privacy Statement and  <a style="color:blue;cursor:pointer;text-decoration: none;" onClick="$('#ocsugcterms').click();$('#tc_content').scrollTop(0);">Terms of Use</a>, applicable data protection regulations, and any other appropriate confidentiality and security measures.<br/><br/>
		We may also provide aggregate statistics about users, online traffic patterns and related information to reputable third parties, but we will use commercially reasonable efforts to ensure that these statistics will not include any Personal Information that could be used to identify you or third parties.<br/><br/>
		We may disclose your Personal Information if we have reasonable cause to believe that we are under a duty to disclose or share your Personal Information in order to comply with any legal obligation, or in order to enforce or apply our Terms of Use and other agreements, or to protect the rights, property, or safety of Varian, our customers, or others. We may disclose your Personal Information if we have reasonable cause to believe that we are under a duty to disclose or share your Personal Information in order to comply with any legal obligation, or in order to enforce or apply our Terms of Use and other agreements, or to protect the rights, property, or safety of Varian, our customers, or others. <br/><br/>
		We may, for some of the purposes listed above, transfer, collect, store and process your Personal Information outside your country of residence, for instance in the United States and any Member State of the EU, consistent with (and for the purposes set out in) this Privacy Statement. The data protection and other laws of countries to which your information may be transferred might not be as comprehensive as those in your country.</p>
	</div><br/>
	<h1 style="font-weight: bold; text-decoration: underline;">Safe Harbor</h1>
	<p>Varian abides by the "safe harbor" framework set forth by the U.S. Department of Commerce regarding the collection, use and retention of Personal Information from European Union Member States and Switzerland.  To learn more about the “safe harbor framework”, and to view Varian's certification, please visit <a href="http://www.export.gov/safeharbor">http://www.export.gov/safeharbor</a>.<br/>
	As part of Varian's participation in the Safe Harbor program, Varian agrees to resolve disputes you have with Varian in connection with related policies and practices through TRUSTe.  If you would like to contact TRUSTe, <a href="http://www.truste.com/about-TRUSTe/contact-us/">click here</a>.</p>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Educational Information</h1>
	<p>Users, including Varian, may be able to provide scientific, educational and informational health-related information when using Varian's Services. Any such materials are for educational and informational purposes only, and are not intended to and should not be construed as medical advice for diagnostic, treatment or other purposes. See the <a style="color:blue;cursor:pointer;text-decoration: none;" onClick="$('#ocsugcterms').click();$('#tc_content').scrollTop(0);">Terms of Use</a></p>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Use of Our Online Communities and Forums</h1>
	<p>Certain of Varian's Services offer you the ability to post information and exchange ideas. We caution all users not to disclose any Personal Information as part of their posts or communications, particularly where that Personal Information relates to third parties.  Varian will not be responsible in the event that you disclose Personal Information in your posts or during any other communication with other users of Varian's Services.</p>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Statement of E-Mail Privacy Practices</h1>
	<p style="border:1px solid;">You consent that your e-mail address may be used to communicate with you for the purposes set out in this Privacy Statement.  </p>
	<p>Your e-mail may also be used if you forget your password, or for other purposes in the future. You may opt out of receiving e-mail communications from Varian by clicking “Unsubscribe” in an e-mail, or by contacting us at oncopeer@varian.com. Please note that, despite your opt-out selections, where permitted by applicable laws and this Privacy Statement, we may send you communications regarding transactions or services you have specifically requested or to inform you of important changes to Varian's Services, products or policies.<br/><br/>
	   If you have multiple accounts, you may need to opt-out separately for each account in connection with the privacy choices described above.  Should you require assistance with this process, please contact us at the address set out below.
	</p>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Children's Privacy</h1>
	<p>We do not structure Varian's Services to attract children. We do not knowingly collect Personal Information from children under 13 years of age or younger and any such information that we do receive will be destroyed or de-identified as soon as practicable.  </p>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Security</h1>
	<p>We have taken measures to help protect information you submit from loss, misuse or unauthorized access or disclosure.  However, no data transmission over the Internet is certain to be secure and we cannot guarantee its security.  We suggest that you change your passwords often and that your passwords include a combination of letters and numbers, and that you make certain that you are using a secure browser.  When you enter sensitive information (such as a credit card number), we encrypt the transmission of that information using secure socket layer technology (SSL).  As a further security measure, we will destroy or de-identify any Personal Information when no longer needed.</p>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Access to Your Information</h1>
	<p>The accuracy of your information is important to us.  Many of Varian's Services allow you to view or update your information online.  Check the program you registered with to learn if you can view or update your information online.<br/><br/>
	   If information access and correction is not available where you registered and you would like to make a request to access, correct, or delete your information or otherwise object to our processing of such information, please contact us at oncopeer@varian.com.  We will respond to reasonable requests in accordance with applicable law and subject to legal and contractual restrictions.  Subject to applicable law, we may charge you a small fee to cover our administrative and other reasonable costs in providing the information to you.  We will not charge you for simply making the request and we will not charge you for making any corrections to your Personal Information.<br/><br/>
	   There may be instances where we cannot grant you access to the Personal Information we hold.  For example, where required or permitted by applicable law, we may need to refuse access if granting access would interfere with the privacy of others or if it would result in a breach of confidentiality.  If that happens, we will give you written reasons for any refusal.
	</p>
	
	<h1 style="font-weight: bold; text-decoration: underline;">Questions and Contacts</h1>
	<p>If you have any questions about this Privacy Statement, or any other aspects of your privacy, please contact Varian by e-mail at the address indicated above, or by letter to the address below, and we will endeavor to respond to your inquiry promptly:<br/><br/>

	Varian Medical Systems, Inc.<br/>
	Attn: Legal Department<br/>
	3100 Hansen Way <br/>
	Palo Alto, CA 94304<br/>
	</p>
	<div style="border:1px solid;">
		<h1 style="font-weight: bold; text-decoration: underline;">Revisions, Updates and Changes</h1>
		<p>Varian reserves the right to modify, amend and update this Privacy Statement and the Terms of Use at any time and for any reason, with notice to you if required by applicable law. When changes are made to this Privacy Statement or the Terms of Use, the updated document will be posted to the website and the "Last Modified" date at the top of the document will be revised. Your continued use of Varian's Services means that you accept such updates. We suggest that you periodically review this Privacy Statement to see if any changes have been made to it that may affect you.</p>
	</div>
<br>
</div>
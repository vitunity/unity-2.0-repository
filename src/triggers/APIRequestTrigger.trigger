trigger APIRequestTrigger on API_Request__c (after insert, after update, before insert, before update) {
	System.debug('----APIKeyRequestTriggerHandler.stopAPIKeyRequestTrigger--'+APIKeyRequestTriggerHandler.stopAPIKeyRequestTrigger);
	if(!APIKeyRequestTriggerHandler.stopAPIKeyRequestTrigger){
		if(trigger.isAfter){
			API_Request__c apiRequestNew = trigger.new[0];
			API_Request__c apiRequestOld;
			if(trigger.isUpdate){
				apiRequestOld = trigger.old[0];
				APIKeyRequestTriggerHandler.shareAPIRequestsWithContacts(trigger.new, trigger.oldMap);
			}
			
		    if(
		    (trigger.isInsert && apiRequestNew.Approval_Status__c != 'Approved' && apiRequestNew.Approval_Status__c != 'Rejected')
		    || (trigger.isUpdate && 
		    (apiRequestNew.Software_System__c != apiRequestOld.Software_System__c 
		    || apiRequestNew.API_Type__c != apiRequestOld.API_Type__c
		    || apiRequestNew.X3rd_party_Software__c != apiRequestOld.X3rd_party_Software__c)
		    )
		    ){
		    	APIKeyRequestTriggerHandler.sendForApproval(trigger.new[0]);
		    }
		    
		    System.debug('----apiRequestNew-'+apiRequestNew.Approval_Status__c);
		    //System.debug('----apiRequestOld-'+apiRequestOld.Approval_Status__c);
		    if(trigger.isUpdate && apiRequestNew.Approval_Status__c == 'Approved' && apiRequestOld.Approval_Status__c != apiRequestNew.Approval_Status__c){
		    	System.enqueueJob(new APIKeyBoomiRequest(apiRequestNew.Id));
		    }
		}else{
			APIKeyRequestTriggerHandler.updateAPIKeyRequestFields(trigger.new);
		}
	}
	if(trigger.isUpdate && trigger.isAfter){
		try{
			insert new API_Request__Share(ParentId = trigger.new[0].Id, UserOrGroupId = trigger.new[0].CreatedById, AccessLevel = 'Edit');
		}catch(Exception e){
			
		}
	}
}
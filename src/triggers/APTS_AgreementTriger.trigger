trigger APTS_AgreementTriger on Apttus__APTS_Agreement__c (after update,after insert) {
    static string TAG = 'AICSync APTS_AgreementTriger';
    if (trigger.isAfter && trigger.isUpdate ) 
    {        
        RecordType recordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Indirect Procurement Request' and sObjectType = 'Apttus__APTS_Agreement__c' LIMIT 1];
        
        for(Apttus__APTS_Agreement__c agreement : trigger.new) 
        {
            if (agreement.Apttus__Is_System_Update__c) {
                continue;
            }
            Apttus__APTS_Agreement__c oldAgreement = System.Trigger.oldMap.get(agreement.Id);
        
            //RecordType recordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Indirect Procurement Request' and sObjectType = 'Apttus__APTS_Agreement__c' LIMIT 1];
        
            if (agreement.RecordTypeId == recordType.id && oldAgreement != null && (agreement.AIC_Record_ID__c != null && agreement.AIC_Record_ID__c != ''))
            {              
                system.debug(TAG + ' Trigger Called for agreement:' + agreement);
                APTPS_FutureAgreementSyncToAIC.send(JSON.serialize(agreement));                
            }
        }
    }
    
    //Added logic to share child records with Indirect Procurement Queue when Parent Record Type is IP Request
    if( trigger.IsInsert || Trigger.IsUpdate){
         Set<Id> agreementIdSet = new Set<Id>();
         for(Apttus__APTS_Agreement__c agreement : trigger.new) 
         {
           agreementIdSet.add(agreement.id);
         }
        APTS_AgreemCustomSharHelperClass.customSharingRules(Trigger.new,Trigger.OldMap,agreementIdSet);
      
   }
        
}
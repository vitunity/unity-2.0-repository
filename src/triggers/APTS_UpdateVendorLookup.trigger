/*************************************************************************\
      @ Author          : Sadhana Sadu
      @ Date            : 13-Feb-2018
      @ Description     : This Trigger will be used to update Vendor Lookup.
      @ Last Modified By      :    
      @ Last Modified On      :     
      @ Last Modified Reason  :   
****************************************************************************/

trigger APTS_UpdateVendorLookup on Apttus__APTS_Agreement__c (Before Insert, Before Update) {
    if(trigger.isBefore){
        APTS_UpdateVendorLookupHandler.updateAgreementMasterVendor(Trigger.new);
    }
}
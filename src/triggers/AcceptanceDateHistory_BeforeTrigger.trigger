/*************************************************************************\
      @ Author          : Nikhil Verma
      @ Date            : 05-Feb-2014
      @ Description     : This Trigger will be used to update Sales Order & Sales Order Item Lookups.
      @ Last Modified By      :  kaushiki verma   
      @ Last Modified On      :  9/06/14    
      @ Last Modified Reason  : null check    
****************************************************************************/

trigger AcceptanceDateHistory_BeforeTrigger on Acceptance_Date_History__c (before Insert, before Update) 
{
    if(Trigger.isInsert)
    {
        SR_AccepDateHistUtility.updateSalesOrderAndSalesOrderItem(Trigger.new);
    }
    
    if(Trigger.isUpdate)
    {
        List<Acceptance_Date_History__c> accDatHistList = new List<Acceptance_Date_History__c>();
        for(Acceptance_Date_History__c ADH: Trigger.new)
        {
            if(Trigger.oldMap.get(ADH.Id).ERP_Sales_Order__c != Trigger.newMap.get(ADH.Id).ERP_Sales_Order__c || Trigger.oldMap.get(ADH.Id).ERP_Sales_Order_Item__c != Trigger.newMap.get(ADH.Id).ERP_Sales_Order_Item__c)
            {
                accDatHistList.add(ADH);
            }
        }
        if(accDatHistList!=null)
        {
            SR_AccepDateHistUtility.updateSalesOrderAndSalesOrderItem(accDatHistList );
        }
    }
}
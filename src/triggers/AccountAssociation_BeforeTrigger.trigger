/***********************************
    Created By  :   Nikita Gupta
    Created On  :   2-April-2014
    Created Reason: To add before trigger functions.
    Last Modified : Kaushiki Verma 09-06-14 Null Checks
***********************************/
trigger AccountAssociation_BeforeTrigger on Account_Associations__c (before insert, before update) 
{   
    if(trigger.isInsert)
    {
        Set<Account_Associations__c> setAccAssociation = new Set<Account_Associations__c>(); //set to store ERP Customer Number from Account Association
        
        for(Account_Associations__c AA : trigger.new)
        {
            if(AA.ERP_Customer_Number__c != null || AA.ERP_Partner_Number__c != null)
            {
                setAccAssociation.add(AA);
            }
        }
        if(setAccAssociation!=null)//null check
        {
            SR_ClassAccountAssociation obj = new SR_ClassAccountAssociation ();
            obj.updateAccountOnAccAssociation(setAccAssociation); //US4379, to update Account on Account Association based on ERP Customer Number
        }
    }
    
    if(trigger.isUpdate)
    {
        Set<Account_Associations__c> setAccAssociation = new Set<Account_Associations__c>(); //set to store ERP Customer Number from Account Association
        
        for(Account_Associations__c AA : trigger.new)
        {
            if((AA.ERP_Customer_Number__c != null  && Trigger.oldMap.get(AA.Id).ERP_Customer_Number__c != AA.ERP_Customer_Number__c) || (AA.ERP_Partner_Number__c != null && Trigger.oldMap.get(AA.Id).ERP_Partner_Number__c != AA.ERP_Partner_Number__c ))
            {
                setAccAssociation.add(AA);
            }
        }
        if(setAccAssociation!=null)//null check
        {
            SR_ClassAccountAssociation obj = new SR_ClassAccountAssociation ();
            obj.updateAccountOnAccAssociation(setAccAssociation); //US4379, to update Account on Account Association based on ERP Customer Number
        }
    }
    
    
}
/*************************************************************************\
      @ Author          : Sunil Bansal
      @ Date            : 04-Apr-2013
      @ Description     : This Trigger will be used to add default Account team for different Account Types.
      @ Last Modified By      :     Chandan Mishra
      @ Last Modified On      :     5-July-2013
      @ Last Modified Reason  :     This requirement has been removed. Please refer below for details.
                                    
                                    Trigger sends a callout to a web service defined on Boomi on creation of a new Account.
                                    Limitations: 
                                    Trigger only sends data if the Account has been created by the follwing users,
                                    Divakara Krishnamurthy - divakara.krishnamurthy@varian.com.sfpc1
                                    Chandan Mishra - chandan.mishra1@varian.com.sfpc1
                                    Manoj Kumar - manoj.kumar31@wipro.com.sfpc1
 
                                    Trigger references - Integration_WebServices.boomiCallout()
      @ Apex Test Class: DefaultAccountTeamTest 
    @ Last Modified By    :     bushra
      @ Last Modified On      :     9-dec-2014
      @ Last Modified Reason  :     de2312
****************************************************************************/

trigger AccountMaster_AfterTrigger on Account (after Insert, after update) 
{
    //Rakesh.02/17/2016.Start.ENHC0010817 
        if(Trigger.isUpdate)
        ContactPreferredLanguage.UpdateContact(Trigger.NewMap,Trigger.OldMap);
        // update all related contact
        ContactPreferredLanguage.UpdateAllContacts(Trigger.NewMap,Trigger.OldMap);
        
    //Rakesh.02/17/2016.End
    
    //Ram - aduit poc - comment it out if you face issues
     //   SObjectHistoryProcessor.trackHistory('Account');
    
    if(System.Label.Account_Triggers_Flag == 'True')
    {
    
        if(Trigger.isUpdate && DefaultAccountTeam.ACCOUNT_RECORD_UPDATED_FROM_ACCOUNT == true)
            return;
        
        List<Account> accountsForNSTeam = new List<Account>();
        List<Account> accountsForTerritoryTeam = new List<Account>();
        Set<id> accIdUpdt = new Set<id>();
        
        // code for ENHC0010587 starts
        List<Account> dupAccList = new List<Account>();
        
        //Rakesh.1/05/2016.Start
        Map<String,List<Account>> SPCtoAccount = new Map<String,List<Account>> ();
        //String ErpPartnerIdList = '';
        Set <String> ErpPartnerIdList = new Set <String>();
        //Rakesh.1/05/2016.End
        
        List<SVMXC__Site__c> LocList = new List<SVMXC__Site__c>();
        List<SVMXC__Site__c> LocList1 = new List<SVMXC__Site__c>();
        List<SVMXC__Site__c> LocList2 = new List<SVMXC__Site__c>();
        List<SVMXC__Site__c> UpdtLocList = new List<SVMXC__Site__c>();
        List<String> AccId = new List<String>();
        List<String> AccId1 = new List<String>();
        map<String, ID> erpAccMap = new Map<String, ID>();
        
        //Rakesh.1/05/2016.Start
        map<string,Integer> mapDuplicateErpAccount = new map<string,Integer>();
        //Rakesh.1/05/2016.End
        
        for(Account newAccount : Trigger.New)
        {
            system.debug('ERP Site P Id---'+newAccount.ERP_Site_Partner_Code__c);
            
            if(newAccount.ERP_Site_Partner_Code__c != Null && newAccount.ERP_Site_Partner_Code__c != '')
            {
                if(Trigger.isInsert || (trigger.isUpdate && newAccount.ERP_Site_Partner_Code__c != trigger.oldmap.get(newAccount.Id).ERP_Site_Partner_Code__c)){
                    ErpPartnerIdList.add( newAccount.ERP_Site_Partner_Code__c);
                }
            }
            
        }
        system.debug('ErpPartnerIdList---'+ErpPartnerIdList);
        if(ErpPartnerIdList != Null)
        {
            dupAccList = [select id, Name, ERP_Site_Partner_Code__c from Account where ERP_Site_Partner_Code__c = :ErpPartnerIdList];
            
            //Rakesh.1/06/2016.Start
            for(Account acc : dupAccList){
                mapDuplicateErpAccount.put(acc.ERP_Site_Partner_Code__c,0);
            }
            for(Account acc : dupAccList){
                if(mapDuplicateErpAccount.ContainsKey(acc.ERP_Site_Partner_Code__c)){
                    Integer count = mapDuplicateErpAccount.get(acc.ERP_Site_Partner_Code__c);
                    count++;
                    mapDuplicateErpAccount.put(acc.ERP_Site_Partner_Code__c,count);
                }
            }
            //Rakesh.1/06/2016.End
        }
        system.debug('dupAccList---'+dupAccList);
        system.debug('dupAccListsize---'+dupAccList.size());
        for(Account newAccount : Trigger.New)
        {
        
            //Rakesh.1/05/2016.Start  
            /*    if(dupAccList.size() > 1)
                {
                        system.debug('error---');
                        Acc.addError('Another Account with same ERP Site Partner ID already exists');
                }  
            */
            if(newAccount.ERP_Site_Partner_Code__c <> null){
                if (mapDuplicateErpAccount.containsKey(newAccount.ERP_Site_Partner_Code__c) && mapDuplicateErpAccount.get(newAccount.ERP_Site_Partner_Code__c)  > 1) {
                   newAccount.addError('Another Account with same ERP Site Partner ID already exists');
                    
                }
            }    
            //Rakesh.1/05/2016.End

        }
        
        if(Trigger.IsUpdate && Trigger.IsAfter)
        {
            for(Account newAccount : Trigger.New)
            {
                system.debug('ERP Site P Id---'+newAccount.ERP_Site_Partner_Code__c);
                if(newAccount.ERP_Site_Partner_Code__c != Null && newAccount.ERP_Site_Partner_Code__c != '' && newAccount.ERP_Site_Partner_Code__c != Trigger.oldMap.get(newAccount.id).ERP_Site_Partner_Code__c )
                { 
                    AccId.add(newAccount.Id);
                    erpAccMap.put(newAccount.ERP_Site_Partner_Code__c,newAccount.Id);
                }
                if((newAccount.ERP_Site_Partner_Code__c == Null || newAccount.ERP_Site_Partner_Code__c == '') && (newAccount.ERP_Site_Partner_Code__c != Trigger.oldMap.get(newAccount.id).ERP_Site_Partner_Code__c) )
                { 
                    AccId1.add(newAccount.Id);
                } 
            }
            system.debug('erpAccMap---'+erpAccMap);
            system.debug('erpAccMapsize---'+erpAccMap.size());
            system.debug('AccId---'+AccId);
            if(AccId.size() >0)
            {
                LocList = [select id, Name, SVMXC__Account__c, ERP_Site_Partner_Code__c from SVMXC__Site__c where SVMXC__Account__c IN :AccId];
            }
            if(AccId1.size() >0)
            {
                LocList2 = [select id, Name, SVMXC__Account__c, ERP_Site_Partner_Code__c from SVMXC__Site__c where SVMXC__Account__c IN :AccId1];
            }
            system.debug('LocList---'+LocList);
            if(LocList.size() >0)
            {
                Update LocList;
            }
            system.debug('LocList2---'+LocList2);
            if(LocList2.size() >0)
            {
                Update LocList2;
            }
            if(erpAccMap.size()>0)
            {
                LocList1 = [select id, Name, SVMXC__Account__c, ERP_Site_Partner_Code__c from SVMXC__Site__c where ERP_Site_Partner_Code__c IN :erpAccMap.KeySet()];
            }
            system.debug('LocList1---'+LocList1);
            if(LocList1.size() >0)
            {
                for(SVMXC__Site__c Loc1 : LocList1)
                {
                    system.debug('Loc1.ERP_Site_Partner_Code__c---'+Loc1.ERP_Site_Partner_Code__c);
                    if(erpAccMap.ContainsKey(Loc1.ERP_Site_Partner_Code__c))
                    {
                        system.debug('accid1---'+erpAccMap.get(Loc1.ERP_Site_Partner_Code__c));
                        Loc1.SVMXC__Account__c = erpAccMap.get(Loc1.ERP_Site_Partner_Code__c);
                        UpdtLocList.add(Loc1);
                    }
                }
                system.debug('UpdtLocList---'+UpdtLocList);
                if(UpdtLocList.size() >0)
                {
                    update UpdtLocList;
                }
            }
            
        }
        
        //code for ENHC0010587 ends 
        
        if(Trigger.isInsert)
        {
            accountsForNSTeam = Trigger.NewMap.values();
            accountsForTerritoryTeam = Trigger.NewMap.values();
        }
        
        if(Trigger.isUpdate)
        {
            for(Account newAccount: Trigger.NewMap.values())
            {
                String allowTeamFromTerritory = 'False';
                try
                {
                    Map<String, KeyValueMap_CS__c> keyValueMap = KeyValueMap_CS__c.getAll();
                    allowTeamFromTerritory = keyValueMap.get('AllowAccountTeamFromTerritory').Data_Value__c;
                }
                catch(Exception ex)
                {
                    // do nothing
                }
    
         
                Account oldAccount = Trigger.oldMap.get(newAccount.Id);
    //            if(oldAccount.BillingCountry != newAccount.BillingCountry || oldAccount.BillingPostalCode != newAccount.BillingPostalCode || oldAccount.BillingState != newAccount.BillingState)
                if(oldAccount.BillingCountry != newAccount.BillingCountry || oldAccount.BillingPostalCode != newAccount.BillingPostalCode || oldAccount.BillingState != newAccount.BillingState || allowTeamFromTerritory == 'True' || (oldAccount.Reset_Territory__c == false && newAccount.Reset_Territory__c == true)  
                    || oldAccount.Exclude_from_Team_Owner_rules__c != newAccount.Exclude_from_Team_Owner_rules__c){
                     accountsForTerritoryTeam.add(newAccount);
                     accIdUpdt.add(newAccount.id);
                }
              
            }
            DefaultAccountTeam objDefaultAccountTeam = new DefaultAccountTeam();
            objDefaultAccountTeam.updateDefaultAccountTeam(Trigger.OldMap, Trigger.NewMap);
            objDefaultAccountTeam.removeDefaultTeam(Trigger.OldMap, Trigger.NewMap); 
            objDefaultAccountTeam.removeTerritoryTeam(Trigger.OldMap, Trigger.NewMap);
            
            SR_Class_Account objaccountclass= new SR_Class_Account();
            objaccountclass.UpdateLocationAddress(trigger.new);
        }
      
        if(accountsForNSTeam.size() > 0)
        {
            DefaultAccountTeam objDefaultAccountTeam = new DefaultAccountTeam();
            objDefaultAccountTeam.addDefaultAccountTeam(accountsForNSTeam);
        }
        
        if(accountsForTerritoryTeam.size() > 0)
        {
            DefaultAccountTeam objDefaultAccountTeam = new DefaultAccountTeam();
            
            //objDefaultAccountTeam.changeAccountOwner(accountsForTerritoryTeam);
            List<Account> excludedTeamOwnerRuleAccounts = new List<Account>(); 
            // Let allow user to change account owner only if "Exclude from Team/Owner rules" flage is false.
            for(Account account : accountsForTerritoryTeam)
            {
                if(!account.Exclude_from_Team_Owner_rules__c){
                    excludedTeamOwnerRuleAccounts.add(account);
                }
            }
            
            system.debug('***excludedTeamOwnerRuleAccounts.size()***'+excludedTeamOwnerRuleAccounts.size());
            if(excludedTeamOwnerRuleAccounts.size() > 0)
            {
                objDefaultAccountTeam.addTerritoryTeam(excludedTeamOwnerRuleAccounts);
                objDefaultAccountTeam.changeAccountOwner(excludedTeamOwnerRuleAccounts);
            }
            
            //update Reset territory team flag
            objDefaultAccountTeam.updtResetTerrFlag(accIdUpdt); 
        }
        
        
    }
    
    
       
}
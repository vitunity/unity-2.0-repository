/*************************************************************************\
    @ Author        : Sunil Bansal
    @ Date          : 12-Apr-2013
    @ Description   : This trigger will update the account type based on Record type.
    @ Modified By  :   Sunil Bansal
    @ Modified On  :   07-Jun-2013
    @ Modified Reason  :   Update Billing Street from custom Billing Address fields and vice-versa
    @ Modified By  :   Jyoti Gaur
    @ Modified On  :   24-Jun-2013
    @ Modified Reason  :   Update Territory/geography fields on Account object from Territory team @ Modified On  :   24-Jun-2013
    @ Modified On  : 17-July-2013
    @ Modified Reason  : Update Overallblock, OMNI Account Name 1 and OMNI country logic.
    @ Modified By  :   Bhanu Devaguptapu
    @ Modified On  :   22-Aug-2013
    @ Modified Reason  : To Add Territory team based on 'Country + State' for Australia and Japan. 
    @ Last Modified By  :   Bushra
    @ Last Modified On  :   09-Dec-2014
    @ Last Modified Reason  :   DE2312
    Change Log: 
    26-Dec-17 - STSK0013523 - Nilesh Gorle - Calling 'deactivateContactFromAcct' method from ContactTriggerHandler class
                                 to deactivate 'Pending Removal' Account's Contact
    Sep-18-2017: Chandra: STSK0012910 : Changed the field data type to accommodate formatting changes
    @ Last Modified By  :   Harleen
    @ Last Modified On  :   24-Nov-2017
    @ Last Modified Reason  :STRY0018408
/****************************************************************************/


trigger AccountMaster_BeforeTrigger on Account (before insert, before update)
{
    public String userProfile{get;set;}
    public Profile profile;
    // DE 6743 : populating the TimeZone field on Technician record using ERP_TimeZone Field
    // TODO: Have to move the logic inside the Controller #FutureRelease
    Map<String, Id> timeZoneMap = new Map<String, Id>();
    
    /* STSK0013523 - Nilesh Gorle - Calling 'deactivateContactFromAcct' method from ContactTriggerHandler class to deactivate 'Pending Removal' Account's Contact */
    if(Trigger.isUpdate)
    ContactTriggerHandler.deactivateContactFromAcct(Trigger.New);

    //CM : May 2017 : STSK0011983
    set<id> accountuserids = new Set<Id>();
    //if(trigger.isBefore && trigger.isInsert) {
        Set<String> timeZoneTextValue = new Set<String>();
        for(Account acc : trigger.new) 
        {
            if(acc.ERP_Timezone__c != null && (acc.Timezone__c== null || trigger.oldmap == null || trigger.oldmap.get(acc.id) == null ||  trigger.oldmap.get(acc.id).ERP_Timezone__c == null ||  trigger.oldmap.get(acc.id).ERP_Timezone__c != acc.ERP_Timezone__c )) { //DE6743
                timeZoneTextValue.add(acc.ERP_Timezone__c);
            }
            //Chandra: Sep-18-2017: STSK0012910 : Changed the field data type to accommodate formatting changes
            if ( (!string.isBlank(acc.Special_Care_Instruction__c) && trigger.oldmap == null) || (!string.isBlank(acc.Special_Care_Instruction__c) && trigger.oldmap != null && acc.Special_Care_Instruction__c != trigger.oldmap.get(acc.id).Special_Care_Instruction__c))
            {
                acc.Special_Care_Instruction_new__c = '<font color="Red">'+'<b>'+acc.Special_Care_Instruction__c+'</b>'+'</font>'  ;
            }
            else if ( (!string.isBlank(acc.Special_Care_Instruction_new__c) && trigger.oldmap == null) || (!string.isBlank(acc.Special_Care_Instruction_new__c) && trigger.oldmap != null && acc.Special_Care_Instruction_new__c != trigger.oldmap.get(acc.id).Special_Care_Instruction_new__c))
            {
                system.debug('**in else');
                acc.Special_Care_Instruction_new__c = '<font color="Red">'+'<b>'+acc.Special_Care_Instruction_new__c+'</b>'+'</font>';
                String plaintext = acc.Special_Care_Instruction_new__c;
                plaintext = plaintext.replaceAll('<[^>]+>','');
                acc.Special_Care_Instruction__c = plaintext;
            }
        }
        if (timeZoneTextValue.size()>0) //DE7370
        for(ERP_Timezone__c erpTimeZone : [SELECT Id, Name FROM ERP_Timezone__c WHERE Name IN: timeZoneTextValue]) {
            timeZoneMap.put(erpTimeZone.Name, erpTimeZone.Id);
        }
    //}
    // DE 6743 //
    
    //STSK0011818:Start
    /*
    if(trigger.isbefore && (trigger.isInsert || trigger.isUpdate)){
        AccountConsentLoggedBy.updateConsentBy(trigger.new,trigger.oldmap);
    }
    */
    //STSK0011818:End

    
    set<id> OwnerIdSet = new set<id>(); //DCFT0011585
    //set<string> CurrentOwnerIdSet = new set<string>(); //DCFT0011585
    set<string> CurrentTeamSet = new set<string>(); //DCFT0011585
    
    if(System.Label.Account_Triggers_Flag == 'True')
    {
        Map<Id, RecordType> recTypes = new Map<Id, RecordType>([Select SobjectType, Name, Id From RecordType where SobjectType = 'Account']);
        Set<String>postCode = new set<String>();
        Set<String>allcountry = new set<String>();
        Set<String>USAcountry = new set<String>();
        List<Account> lstOfAccToUpdate = new List<Account>();
         
        /* 
        profile = [Select p.Name From Profile p where p.Id = :UserInfo.getProfileId()];
        if(profile != null)
        {
            userProfile = profile.Name;
        } 
        */
        for(Account newAccount: Trigger.New)
        {
          //**   newAccount.BillingCountry = newAccount.Country__c; || Commented As this loop of code has been moved only with insert condition of the records. 
            
           /**if (userProfile!= label.System_Integration)
           {
                newAccount.OMNI_Country__c= newAccount.Country__c;
                newAccount.BillingCity = newAccount.OMNI_City__c;
                newAccount.BillingCity = newAccount.OMNI_City__c;
                if(newAccount.OMNI_Postal_Code__c!=null)
                {
                    newAccount.BillingPostalCode = newAccount.OMNI_Postal_Code__c;
                }    
                    newAccount.BillingState = newAccount.OMNI_State__c;
            
            
               if(newAccount.OMNI_Account_Name_1__c == null)
               {
                    newAccount.OMNI_Account_Name_1__c = newAccount.name;
               }
          
          }
          **/
          
           if(newaccount.Order_Block__c == '01')
           {
                newAccount.Overall_Block__c=true;
           }
           else
           {
                newAccount.Overall_Block__c=false;
           }
           
            if(newAccount.RecordTypeId != null)
            {
                system.debug('coming in loop or not----------');
                String recTypeName = recTypes.get(newAccount.RecordTypeId).Name;
                    system.debug('recTypeName---'+recTypeName);
                if(recTypeName == 'Standard')
                    recTypeName = 'Prospect';
                    system.debug('Ext_Cust_Id__c---'+newAccount.Ext_Cust_Id__c);
                    
                    // Code added by Naren for ENHC0010587
                    if(recTypeName == 'Site Partner')
                    {
                       if(newAccount.Ext_Cust_Id__c != Null && newAccount.Ext_Cust_Id__c != '')
                            {
                                newAccount.ERP_Site_Partner_Code__c = newAccount.Ext_Cust_Id__c;
                                newAccount.Has_Location__c = True;
                            }
                    }
                    // Code complete Naren for ENHC0010587
                    
    /****************************************************/                
                String strBillingZipCode = '';
                if(newAccount.BillingPostalCode != null && newAccount.BillingPostalCode.length() > 4)
                    strBillingZipCode = newAccount.BillingPostalCode.substring(0,5);
                else
                    strBillingZipCode = newAccount.BillingPostalCode;
                    
                // update the account type based on Record type 
                
                newAccount.Type = recTypeName;
               if(newAccount.BillingPostalCode!=null)
               {
                    postCode.add(strBillingZipCode);
               }
                if(newAccount.BillingCountry =='USA' || newAccount.BillingCountry =='UNITED STATES')
                {
                    USAcountry.add(newAccount.BillingCountry );
                }
                else
                {
                    allcountry.add(newAccount.BillingCountry );
                }
                System.debug('MID*******BillingCountry'+newAccount.BillingCountry+'*******BillingCity'+newAccount.BillingCity+'***********BillingStreet'+newAccount.BillingStreet+'*********BillingPostalCode'+newAccount.BillingPostalCode+'MID******BillingState'+newAccount.BillingState);    
    /******************************************/
         
            }
        }
        
        
        if(Trigger.isInsert)
        {
            ///*
            profile = [Select p.Name From Profile p where p.Id = :UserInfo.getProfileId()];
            if(profile != null)
            {
                userProfile = profile.Name;
            }
            //*/
            for(Account newAccount: Trigger.New)
            {
                
                    //ENHC0010873.Start
                    if(newAccount.ownerid <> null)
                    OwnerIdSet.add(newAccount.ownerid);  //DCFT0011585
                    if(newAccount.CSS_District__c <> null)
                    CurrentTeamSet.add(newAccount.CSS_District__c); //DCFT0011585
                    //ENHC0010873.End
                    
                String billingStreet = ''; 
                if((newAccount.OMNI_Address1__c != null && newAccount.OMNI_Address1__c.trim().length() > 0) || (newAccount.OMNI_Address2__c != null && newAccount.OMNI_Address2__c.trim().length() > 0) || (newAccount.OMNI_Address3__c != null && newAccount.OMNI_Address3__c.trim().length() > 0))
                {
                    if(newAccount.OMNI_Address1__c != null && newAccount.OMNI_Address1__c.length() > 0)
                    {
                        billingStreet = newAccount.OMNI_Address1__c;
                    }
                    if(newAccount.OMNI_Address2__c != null && newAccount.OMNI_Address2__c.length() > 0)
                    {
                        billingStreet = billingStreet + ', '+ newAccount.OMNI_Address2__c;
                    }
                    if(newAccount.OMNI_Address3__c != null && newAccount.OMNI_Address3__c.length() > 0)
                    {
                        billingStreet = billingStreet + ', '+ newAccount.OMNI_Address3__c;
                    }
                     if (userProfile!= label.System_Integration)
                   {
                        newAccount.BillingStreet = billingStreet;
                        
                        //@ Bhanu.start new || Updated the code , So that only during insert these fields are populated. 
                        newAccount.BillingCountry = newAccount.Country__c;
                        newAccount.OMNI_Country__c= newAccount.Country__c;
                        newAccount.BillingCity = newAccount.OMNI_City__c;
                        newAccount.BillingCity = newAccount.OMNI_City__c;
                     if(newAccount.OMNI_Postal_Code__c!=null)
                   {
                    newAccount.BillingPostalCode = newAccount.OMNI_Postal_Code__c;
                   }    
                    newAccount.BillingState = newAccount.OMNI_State__c;
            
            
                   if(newAccount.OMNI_Account_Name_1__c == null)
                  {
                    newAccount.OMNI_Account_Name_1__c = newAccount.name;
                  }     
                    
                     }
                    }
                // INC4538233   
                newAccount.BillingCountry = newAccount.Country__c;
                
                }
                
            }
            
            
            
            
            
            

            
            //ENHC0010873.Start
            if(Trigger.isUpdate)
            {
                for(Account newAccount: Trigger.New)
                {
                    // INC4538233   
                newAccount.BillingCountry = newAccount.Country__c;
                   if(newAccount.ownerid <> trigger.oldmap.get(newAccount.Id).ownerid || newAccount.CSS_District__c <> trigger.oldmap.get(newAccount.Id).CSS_District__c ){
                        if(newAccount.ownerid <> null)OwnerIdSet.add(newAccount.ownerid); //DCFT0011585
                        if(newAccount.CSS_District__c <> null)CurrentTeamSet.add(newAccount.CSS_District__c); //DCFT0011585
                   
                }

            }
        }
            //ENHC0010873.End
                
        // @ Bhanu.end . new@ 09/11/2013.
        /** Bhanu.new start @ 09/11/2013 || Commented the update code as update to OMNI Address should not effect billing Address
        
        if(Trigger.isUpdate)
        {
            for(Account newAccount: Trigger.New)
            {
                system.debug('UI First record is-------->'+trigger.new[0].Id);
                Account oldAccount = Trigger.oldMap.get(newAccount.Id);
                

                String billingStreet = '';
                if((oldAccount.OMNI_Address1__c != newAccount.OMNI_Address1__c) || (oldAccount.OMNI_Address2__c != newAccount.OMNI_Address2__c) || (oldAccount.OMNI_Address3__c != newAccount.OMNI_Address3__c)||(oldAccount.BillingStreet!= newAccount.BillingStreet) )
                {
                    if(newAccount.OMNI_Address1__c != null && newAccount.OMNI_Address1__c.length() > 0)
                    {
                        billingStreet = newAccount.OMNI_Address1__c;
                    }
                    if(newAccount.OMNI_Address2__c != null && newAccount.OMNI_Address2__c.length() > 0)
                    {
                        billingStreet = billingStreet + ', '+ newAccount.OMNI_Address2__c;
                    }
                    if(newAccount.OMNI_Address3__c != null && newAccount.OMNI_Address3__c.length() > 0)
                    {
                        billingStreet = billingStreet + ', '+ newAccount.OMNI_Address3__c;
                    }
                    if (userProfile!= label.System_Integration)
                      {
                          newAccount.BillingStreet = billingStreet;
                      }
                }
            } 

         } 
      **/  
    /*
        

        

        
        Map<String, Territory_Team__c> TerritoryteamMap = new Map<String, Territory_Team__c>();//post code+country+territory
        Map<String, Territory_Team__c> TerritoryteamMapcountry = new Map<String, Territory_Team__c>();//country+territory
        for(Territory_Team__c team1: [Select id, District__c, HW_Install_District__c, SW_Install_District__c, Sub_Region__c, District_Manager__c,Software_Sales_Manager__c,District_Sales_Administrator__c,Network_Specialist__c,Network_Specialist_Sales_Admin__c,ISO_Country__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,VSS_Sales_Administrator__c,Zone_VP__c,Zip__c,Country__c, CSS_District__c,Region__c,Territory__c,Super_Territory__c from Territory_Team__c where Zip__c IN :postCode and Country__c IN :USAcountry])
        {
            if(team1.Zip__c != null && team1.Zip__c.length() > 4)
                TerritoryteamMap.put(team1.Zip__c.substring(0,5)+team1.Country__c,team1); 
            else
                TerritoryteamMap.put(team1.Zip__c+team1.Country__c,team1); 
        }
        system.debug('AccountMaster_BeforeTrigger - Initial Total Number of query rows called----->'+Limits.getQueryRows());
       
            for(Territory_Team__c team1: [Select id, District__c, HW_Install_District__c, SW_Install_District__c,ISO_Country__c, Sub_Region__c, District_Manager__c,Software_Sales_Manager__c, District_Sales_Administrator__c,Network_Specialist__c,Network_Specialist_Sales_Admin__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,VSS_Sales_Administrator__c,Zone_VP__c,Zip__c,Country__c, CSS_District__c,Region__c,Territory__c,Super_Territory__c from Territory_Team__c where Country__c IN :allcountry])
            {
               
                TerritoryteamMapcountry.put(team1.Country__c,team1); 
            }
    */       

        // If user created an account and set the false for the Exclude_from_Team_Owner_rules__c field and it will copy the Territory info.
        // Later user updated the flage and set it to true then that time we need to clear the territory information and owner is creator of an Account.
        List<Account> includedTeamOwnerRuleAccounts = new List<Account>();
        
        for(Account acc : Trigger.New){
            
            // DE 6743 //
            if(timeZoneMap.containsKey(acc.ERP_Timezone__c)) {
                acc.Timezone__c = timeZoneMap.get(acc.ERP_Timezone__c);
                system.debug('****** acc.Timezone__c ******* ' + acc.Timezone__c);
            }
            
                /*    
            If(Trigger.isInsert){
                if(acc.Exclude_from_Team_Owner_rules__c){
                    includedTeamOwnerRuleAccounts.add(acc);
                }
            }
            If(Trigger.isUpdate){
                if(acc.Exclude_from_Team_Owner_rules__c && (Trigger.newmap.get(acc.id).Exclude_from_Team_Owner_rules__c != Trigger.oldmap.get(acc.id).Exclude_from_Team_Owner_rules__c)){
                    includedTeamOwnerRuleAccounts.add(acc);
                }
                }*/
            }
        
        DefaultAccountTeam.prepareTerritoryMaps(Trigger.New);
        Map<String, Territory_Team__c> TerritoryteamMap = DefaultAccountTeam.TerritoryteamMap;
    //  Map<String, Territory_Team__c> TerritoryteamMapcountry = DefaultAccountTeam.TerritoryteamMapcountry;
           
        system.debug('AccountMaster_BeforeTrigger 2- Initial Total Number of query rows called----->'+Limits.getQueryRows());
        for(Account newAccount: Trigger.New)
        {
            Territory_Team__c tt;
            String strBillingZipCode = '';
                String strFrnBillingZipCode = '';
            if(newAccount.BillingPostalCode != null && newAccount.BillingPostalCode.length() > 4)
                strBillingZipCode = newAccount.BillingPostalCode.substring(0,5);
            else
                strBillingZipCode = newAccount.BillingPostalCode;
                
            System.debug('Country_Zip  == '+strBillingZipCode +newAccount.BillingCountry);
                if(newAccount.BillingCountry =='USA')
                {
                    tt = TerritoryteamMap.get(strBillingZipCode+newAccount.BillingCountry );
                }
                  else if((newAccount.BillingCountry=='France' || newAccount.BillingCountry=='FRANCE') && (newAccount.BillingPostalCode != null))
                {
                    strFrnBillingZipCode  = newAccount.BillingPostalCode.substring(0,2);
                    tt = TerritoryteamMap.get(strFrnBillingZipCode +newAccount.BillingCountry);
    
                }
               
                else if(newAccount.BillingCountry =='Canada' ||  newAccount.BillingCountry =='Australia' || newAccount.BillingCountry =='Japan' || newAccount.BillingCountry =='China' || newAccount.BillingCountry =='Germany')
                {
                    tt = TerritoryteamMap.get(newAccount.BillingState + newAccount.BillingCountry );
                }
                else
                {
                    tt = TerritoryteamMap.get(newAccount.BillingCountry );
            }   
                //tt = TerritoryteamMapcountry.get(newAccount.BillingCountry );
            System.debug('TERRITORY TEAM RECORD tt****** '+tt);
            if(
                trigger.isInsert || 
                
                    trigger.isUpdate && 
                    (
                        newAccount.BillingPostalCode != trigger.oldmap.get(newAccount.Id).BillingPostalCode ||
                        newAccount.BillingCountry != trigger.oldmap.get(newAccount.Id).BillingCountry ||
                        newAccount.BillingState != trigger.oldmap.get(newAccount.Id).BillingState ||
                        newAccount.Reset_Territory__c == true && trigger.oldmap.get(newAccount.Id).Reset_Territory__c == false  //Added by Harvey 17/02/13 for the issue of unable to reset GEO information
                    )
                
                
                
                )
            {
                if(tt!=null && newAccount.Sub_Region__c!='China Admin'){
                    system.debug('comimg in country and zip  loop--------- '+tt);
        //            newAccount.Zone1__c = tt.Zone__c;
                    newAccount.Super_Territory1__c = tt.Super_Territory__c;
                    newAccount.Region1__c = tt.Region__c;
                    newAccount.Territories1__c = tt.Territory__c;
                    //HarleenGulati.update SSM on Account creation
                    newAccount.Software_Sales_Manager__c = tt.Software_Sales_Manager__c;
                    //Harleen.Gulati.Start.commenting not required code
                    //newAccount.CSS_District__c = tt.CSS_District__c;
                    newAccount.District__c = tt.District__c;
                    //newAccount.HW_Install_District__c = tt.HW_Install_District__c;
                    //newAccount.SW_Install_District__c = tt.SW_Install_District__c;
                    newAccount.Sub_Region__c = tt.Sub_Region__c;
                    newAccount.Territory_Team__c = tt.id;
                    //newAccount.Country__c = tt.Country__c;
                    newAccount.ISO_Country__c = tt.ISO_Country__c;
                    //HarleenGulati.Update Training Team info from TT.STRY0018408.Start
                    newAccount.Training_Coordinator__c= tt.Training_Coordinator__c;
                    newAccount.Training_District__c= tt.Training_District__c;
                    newAccount.Training_Manager__c= tt.Training_Manager__c;
                    //HarleenGulati.Update Training Team info from TT.STRY0018408.Start
                    
                    
                }
                else
                {
        //            newAccount.Zone1__c = '';
                    newAccount.Super_Territory1__c = '';
                    newAccount.Region1__c = '';
                    newAccount.Territories1__c = '';
                    newAccount.CSS_District__c = '';
                    newAccount.District__c = '';
                    newAccount.HW_Install_District__c = '';
                    newAccount.SW_Install_District__c = '';
                    newAccount.ISO_Country__c = '';
                    newAccount.Territory_Team__c = null;
                    
                    
                //HarleenGulati.adding condition for China Admin.Start //
                    
                    if(newAccount.Sub_Region__c =='China Admin')
                        {
                            newAccount.Sub_Region__c = 'China Admin';
                        }
                    else
                    {
                            newAccount.Sub_Region__c = '';
                    }    
                
                //HarleenGulati.adding condition for China Admin.Finish //      
                    
                    
                    
              
                }   
            }
            
            //priti, US3924, 06/02/104
            if(newAccount.Country__c!=null)
            {
                lstOfAccToUpdate.add(newAccount);
            }
        }
        
         /*    // Clear the information of territory team related fields.
            if(includedTeamOwnerRuleAccounts.size() > 0)
            {
                for(Account newAccount : includedTeamOwnerRuleAccounts)
                {
                if(newAccount.CreatedById != null){
                    newAccount.OwnerId = newAccount.CreatedById;
                } else {
                    if(newAccount.OwnerId == null){
                        newAccount.OwnerId = Userinfo.getUserId();
                    }
                }
                
            }
        }
        
        // Clear the Account Team Members and appropriate Share records.
        if(trigger.isBefore && trigger.isUpdate){
            // Get the set of account id's whose exclude team owner rules is false.
            Set<Id> updatedAccountIds = new Set<Id>();
            for(Account acc : trigger.new){
            if(acc.Exclude_from_Team_Owner_rules__c && (Trigger.newmap.get(acc.id).Exclude_from_Team_Owner_rules__c != Trigger.oldmap.get(acc.id).Exclude_from_Team_Owner_rules__c)){
                    updatedAccountIds.add(acc.Id);
                }
            }
            
            if(updatedAccountIds.size() > 0){
                // delete Account shares
                List<AccountShare> accountShares = [Select Id from AccountShare where AccountId In : updatedAccountIds AND RowCause != 'Owner' AND RowCause != 'Territory' AND RowCause != 'PortalImplicit'];
                if(accountShares != null && accountShares.size() > 0){
                    delete accountShares;
                }
                
                // delete Account team members.
                List<AccountTeamMember> teamMembers = [Select Id from AccountTeamMember where AccountId In : updatedAccountIds];
                if(teamMembers != null && teamMembers.size() > 0){
                    delete teamMembers;
                }
            }
            
            }   */
        //priti, US3924, 06/02/104
        if(lstOfAccToUpdate.size()>0)
        {
            SR_Class_Account.updateAccountCuntry(lstOfAccToUpdate);
        }
         if (OwnerIdSet.size()>0 || CurrentTeamSet.size()>0) //DE7370 //DCFT0011585
         {   
            //ENHC0010873.Start
          
            Map <Id,User> MapCurrentUser = new Map<id,User>([Select id,Manager.ManagerId,ManagerId,Manager.Email from User where id in :OwnerIdSet]); 
            map<string,Id> mapDistrictManager = new map<string,Id>();
            map<string,Id> mapDistrictManagerManager = new map<string,Id>();
            map<string,String> mapServiceRegion = new map<string,String>(); 
            
            //REFACTOR: Add Service Team lookup on Account and set it once to avoid query'ing this information over and over again then on change this information when this field is set or reset or just use formula fields instead
            if (CurrentTeamSet.size()>0) //DCFT0011585
            for(SVMXC__Service_Group__c st:[select Id,(select Id,SVMXC__Parent_Territory__c,SVMXC__Parent_Territory__r.SVMXC__Description__c,SVMXC__Description__c from Territories__r limit 1),District_Manager__c,SVMXC__Description__c,District_Manager__r.Manager.ManagerId,District_Manager__r.ManagerId,SVMXC__Group_Code__c from SVMXC__Service_Group__c where SVMXC__Group_Code__c IN:  CurrentTeamSet]){
                if(st.District_Manager__c <> null){
                    mapDistrictManager.put(st.SVMXC__Group_Code__c,st.District_Manager__c);
                       if(st.District_Manager__r.ManagerId <> null){
                           mapDistrictManagerManager.put(st.SVMXC__Group_Code__c,st.District_Manager__r.ManagerId);
                       } 
                            
                } 
                if(st.Territories__r <> null && st.Territories__r.size() > 0 ){
                    if(st.Territories__r[0].SVMXC__Parent_Territory__c <> null && st.Territories__r[0].SVMXC__Parent_Territory__r.SVMXC__Description__c <> null){
                        mapServiceRegion.put(st.SVMXC__Group_Code__c, st.Territories__r[0].SVMXC__Parent_Territory__r.SVMXC__Description__c);
                    }
                   
               } 
                
            }
            for(Account Accountacc : trigger.new)
            { 
                if(MapCurrentUser.containsKey(Accountacc.OwnerId))
                    Accountacc.District_Sales_Manager__c = Accountacc.ownerId;
                if(MapCurrentUser.containsKey(Accountacc.OwnerId) && MapCurrentUser.get(Accountacc.OwnerId).ManagerId <> null)
                    Accountacc.Regional_Sales_Manager__c = MapCurrentUser.get(Accountacc.OwnerId).ManagerId;
                    //Harleen.Gulati.commenting not required code
                //if(mapDistrictManager.containsKey(Accountacc.CSS_District__c))
                  //  Accountacc.District_Service_Manager__c = mapDistrictManager.get(Accountacc.CSS_District__c);
                //if(mapDistrictManagerManager.containsKey(Accountacc.CSS_District__c))
                   // Accountacc.Regional_Service_Manager__c = mapDistrictManagerManager.get(Accountacc.CSS_District__c);
                //if(mapServiceRegion.containsKey(Accountacc.CSS_District__c))
                   // Accountacc.Service_Region__c = mapServiceRegion.get(Accountacc.CSS_District__c);
                //CM : May 2017 : STSK0011983
                if(Accountacc.District_Sales_Manager__c != null)
                {
                    accountuserids.add(Accountacc.District_Sales_Manager__c);
                }
                if(Accountacc.District_Service_Manager__c != null)
                {
                    accountuserids.add(Accountacc.District_Service_Manager__c);
                }
                
                if(Accountacc.Software_Sales_Manager__c!= null)
                {
                    accountuserids.add(Accountacc.Software_Sales_Manager__c);
                }
            }
            //ENHC0010873.End  
         }  
        //CM : May 2017 : STSK0011983
        Map<Id,User> distsalesman2user = new Map<Id, User>();    
        if (accountuserids.size() > 0)
        {   
            for (user u: [select id, phone, email from user where id in : accountuserids])
            {
                distsalesman2user.put(u.id, u);
            }
        }
        For(Account acc: trigger.new)
        {
            if (acc.District_Sales_Manager__c != null && distsalesman2user.size() > 0 && distsalesman2user.get(acc.District_Sales_Manager__c) != null)
            {
                acc.District_Sales_Manager_Phone__c = distsalesman2user.get(acc.District_Sales_Manager__c).Phone;
                acc.District_Sales_Manager_Email__c = distsalesman2user.get(acc.District_Sales_Manager__c).Email;
            }
            if (acc.District_Service_Manager__c != null && distsalesman2user.size() > 0 && distsalesman2user.get(acc.District_Service_Manager__c) != null)
            {
                acc.District_Service_Manager_Phone__c = distsalesman2user.get(acc.District_Service_Manager__c).Phone;
                acc.District_Service_Manager_Email__c = distsalesman2user.get(acc.District_Service_Manager__c).Email;
            }
            
            if (acc.Software_Sales_Manager__c!= null && distsalesman2user.size() > 0 && distsalesman2user.get(acc.Software_Sales_Manager__c) != null)
            {
                acc.Software_Sales_Manager_Phone__c= distsalesman2user.get(acc.Software_Sales_Manager__c).Phone;
                acc.Software_Sales_Manager_Email__c= distsalesman2user.get(acc.Software_Sales_Manager__c).Email;
            }
        } 
        
    }
    
    //20 Nov 2017, Puneet Mishra, STRY0018408
    //DefaultAccountTeam.populateTetorryTeam(trigger.newMap, trigger.oldMap);
}
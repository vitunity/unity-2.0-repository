trigger ActivatingContactRelatedFilter on Contact (before insert, before update) 
{
      for(Contact contact : Trigger.New)
      {
            if(contact.Inactive_Contact__c && (Trigger.OldMap == null || Trigger.OldMap.get(contact.id).Inactive_Contact__c != contact.Inactive_Contact__c))
            {
                  contact.Active_Contact__c = null;
            }
            else if(!contact.Inactive_Contact__c && (Trigger.OldMap == null || Trigger.OldMap.get(contact.id).Inactive_Contact__c != contact.Inactive_Contact__c || Trigger.OldMap.get(contact.id).AccountId != contact.AccountId))
            {
                  contact.Active_Contact__c = contact.AccountId;
            }
      }
}
/*
trigger OwnerChangeWhenActivatingContact on Contact (before insert, before update, after update) 
{
      List<User> users = [select Id, email from user where email = 'inactive.record@varian.com'];
      if(Trigger.isBefore && Trigger.isInsert)
      {
            for(Contact contact : Trigger.New)
            {
                  if(contact.Inactive_Contact__c && users.size() > 0)
                  {
                        contact.Previous_Owner__c = UserInfo.getUserId();
                        contact.OwnerId = users[0].id;
                        contact.AccountId = null;
                  }
            }
      }
      if(Trigger.isBefore && Trigger.isUpdate)
      {
            for(Contact contact : Trigger.New)
            {
                  if(contact.Inactive_Contact__c && Trigger.OldMap.get(contact.id).Inactive_Contact__c != contact.Inactive_Contact__c)
                  {
                        if(users.size() > 0)
                        {
                              contact.OwnerId = users[0].id;
                              contact.AccountId = null;
                        }
                  }
                  else if(!contact.Inactive_Contact__c && Trigger.OldMap.get(contact.id).Inactive_Contact__c != contact.Inactive_Contact__c)
                  {
                        if(contact.Previous_Owner__c != null)
                        {
                              contact.OwnerId = contact.Previous_Owner__c;
                        }
                        if(String.isNotBlank(contact.Previous_Account_Id__c))
                        {
                              contact.AccountId = contact.Previous_Account_Id__c;
                        }
                  }
            }
      }

      Map<Id, Id> contactIdToOwnerId = new Map<Id, Id>();
      Map<Id, String> contactIdToAccountId = new Map<Id, String>();
      List<Contact> contacts = new List<Contact>();
      if(Trigger.isAfter)
      {
            for(Contact contact : Trigger.New)
            {
                  if(contact.Inactive_Contact__c && (Trigger.OldMap.get(contact.id).Inactive_Contact__c != contact.Inactive_Contact__c))
                  {
                        if(Trigger.isInsert)
                        {
                              contactIdToOwnerId.put(contact.id, UserInfo.getUserId());
                        }
                        if(Trigger.isUpdate)
                        {
                              contactIdToOwnerId.put(contact.id, Trigger.OldMap.get(contact.id).OwnerId);
                              contactIdToAccountId.put(contact.id, Trigger.OldMap.get(contact.id).AccountId);
                        }
                  }
            }
      }
      for(Contact contact : [select Id, OwnerId, Inactive_Contact__c from Contact where Id in: contactIdToOwnerId.keySet()])
      {
            contact.Previous_Owner__c = contactIdToOwnerId.get(contact.id);
            contact.Previous_Account_Id__c = contactIdToAccountId.get(contact.id);
            contacts.add(contact);
      }
      if(contacts.size() > 0)
      {
            update contacts;
      }
}
*/
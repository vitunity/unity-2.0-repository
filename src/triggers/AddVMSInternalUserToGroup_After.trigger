/*************************************
Author: Mohit Sharma
Created Date: 08-May-2013
Description: An Apex Trigger to fetch a logged in User Products from Account.

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
9/21/2017 - Nilesh Gorle - STSK0012842 -  Check AU and NZ user and add user to chatter group 'VOS APAC ALL ANZ information'
************************************************************************/
trigger AddVMSInternalUserToGroup_After on User (after insert,after update)
{
    if(system.Label.User_Triggers_Flag == 'True')
    {
            if(System.Trigger.isInsert){ // After Insert Code Block
            
                set<id> stNewInstalledProduId=new set<id>();
                set<id> stOldInstalledProduId=new set<id>();   
                set<string> stAccountId=new set<string>();
                set<string> stProfile=new set<string>();
                set<string> stContactId=new set<string>();
                set<id> accountId = new set<id>(); //set for adding new user in the account's marketing kit.
                set<id> marketingkitids = new set<id>();
                list<contact> lstContoUP=new list<contact>();
                Map<string,string> mpConAccount=new Map<string,string>();   
                set<Id> userIdSet = new set<Id>();
                set<Id> conIdSet = new set<Id>();
                Set<Id> contactIds = new Set<Id>();
                set<Id> profileIds = new Set<Id>();  //DE7861
                Map <Id,Profile> profileMap = new Map <Id,Profile>(); //DE7861    
                boolean flag=false;
                String profName; 
                for (User u: Trigger.new) { //DE7861
                    if (u.profileId != null){
                        profileIds.add(u.profileId);
                    }
                }
                if (profileIds.size()>0) { //DE7861
                    profileMap = new Map <Id,Profile>([select id,name from profile where id in :profileIds ]);
                }

                for (User usr1 : Trigger.new) {
                    //profName = [select name from profile where id = :usr1.ProfileId  ].name; //DE7861
                    profName = profileMap.get(usr1.ProfileId).name; //DE7861

                    if ((usr1.ContactId != null) && (profName).toLowerCase().contains('myvarian') ) {
                       contactIds.add(usr1.ContactId);
                       //flag=true;
                       system.debug('################## FLAGLOGIC IF');
                    }
                    /**else if(! (profName).toLowerCase().contains('myvarian') ){
                        contactIds.add(usr1.ContactId);
                       flag=false;
                       system.debug('################## FLAGLOGIC ELSE ');
                    }
                    **/
                           
                }

                // Nilesh - Start - Validation Rule- duplicate contact email
                List<Contact> conList = [Select Id, DupContact__c From Contact Where Id in: contactIds];
                for(Contact cn:conList) {
                    if(cn.DupContact__c) {
                        for(User usr:Trigger.New) {
                            if(usr.ContactId == cn.Id) {
                                usr.addError('There are multiple contacts with the same email address. Please reconcile before enabling community user.');
                            }
                        }
                        
                    }
                }
                // Nilesh - Stop - Validation Rule- duplicate contact email

                if (contactIds.size() > 0) {
                    ProductUtils.updateContactPartnerFlag(contactIds, true);
                }

                // Get the set of Account Id's to create a group member in the "VMS MyVarian - Partners" group.
                
                
                for(user u: trigger.new){
                    if(u.contactid !=null){
                        contact con=new contact(id=u.contactid,Registered_Portal_User__c=true);
                        system.debug('Profile name----'+u.contact.account.id);
                        stProfile.add(u.ProfileId);
                        stContactId.add(u.contactid);
                        lstContoUP.add(con);
                        accountId.add(con.accountid);
                        userIdSet.add(u.Id);
                    }

                    
                }
                if(accountId.size() > 0){
                    for(MarketingKitRole__c mktr: [Select id from MarketingKitRole__c where Account__c in: accountId]){
                        marketingkitids.add(mktr.id); 
                    }
                }

                //update lstContoUP;
                //stAccountId.add(u.contact.accountId);
                Map<string,profile> mpProfile=new Map<string,Profile>([select Id, Name from profile where id IN: stProfile and Name !='Customer Portal User']);
               
                for(contact ct: [select id, accountId,MyVarian_Member__c from contact where ID in :stContactId]){ //To Get AccountId associated with Contact
                    mpConAccount.put(ct.id,ct.accountId);
                    //ct.MyVarian_Member__c=true;
                }
                
                for(user varUser: system.trigger.new){
                    if(varUser.contactId !=null  &&  mpProfile.keyset().contains(string.valueOf(varUser.profileID))){ //Filter Out Contact 
                        stAccountId.add(mpConAccount.get(varUser.contactId));
                    }
                }
            
                for( SVMXC__Installed_Product__c ip :[Select s.SVMXC__Product__c, s.Id, s.SVMXC__Company__c 
                                                                            From SVMXC__Installed_Product__c s 
                                                                            where s.SVMXC__Company__c IN:  stAccountId ]){
                    stNewInstalledProduId.add(ip.id);  
                }            
                    
                System.debug('-------------InstallProduct'+stNewInstalledProduId);              
                                
                If(stNewInstalledProduId.size()>0){
                    //CpProductPermissions.setContentRoles(stNewInstalledProduId,stOldInstalledProduId,userIdSet);
                    //CpProductPermissions.setContentRoles(stNewInstalledProduId,stOldInstalledProduId);
                    //CpProductPermissions.assignUserToSelectedGroup(userIdSet,accountId);
                    CpProductPermissions.setContentRoles_UserActivation(stNewInstalledProduId,userIdSet);
                    Id accId = [Select AccountId from Contact where Id =: trigger.newMap.values().get(0).ContactId].AccountId;
                    BatchAddUserToSelectedPG temp = new BatchAddUserToSelectedPG(userIdSet,accId);
                    database.executeBatch(temp,1);
                }
                if(marketingkitids.size() > 0){
              
                    CpProductPermissions.AddPortalUserInGroup(marketingkitids);
                }
                CpProductPermissions.AddVMSInternalUserToGroup(trigger.new);
             
                // Adding VMS Internal User To Public Group.
                /* commenting out below method call as there are no public groups that matches the criteria to insert group members */
                //CpProductPermissions.AddUserInGroup(trigger.new);
                CpProductPermissions.AddPremierMarketingKitforCpUserActive(stContactId);
            
                // Create a Group member in the group "VMS MyVarian - Partners" if user is partner and Account Type is 'Distributor'.
             
                
            }
        
                Set<Id> portalUserAccountIds = new Set<Id>();
                for(user u: trigger.new){
                    if(u.accountid!=null){
                        portalUserAccountIds.add(u.accountid);
                    }
                }
               if(portalUserAccountIds.size() > 0){
                    // Get the Accounts which are related to user whoost account type is distributor.
                    Map<Id,Account> accountsMap = new Map<Id,Account>([Select Id,Distributor__c, Account_Type__c from Account where Id in : portalUserAccountIds and Distributor__c = true and IsPartner = true]);
                    
                    Set<Id> distributorUserIds = new Set<Id>();
                    for(User newUser : trigger.new){
                        if(newUser.AccountId != null){
                            Account acc = accountsMap.get(newUser.AccountId);
                            if(acc != null){
                                distributorUserIds.add(newUser.Id);
                            }
                        }
                    }
                    
                       
                    if(distributorUserIds.size() > 0 && !System.isFuture() && !System.isBatch()){
                     FuturePartnerGroupMember.insertMember(distributorUserIds);
                    }
                }
            set<Id> stContactIddeactivate = new set<Id>();
             /* Added additional logic to call AddVMSInternalUserToGroup() method only when the contentPermission value is changed */
            List<String> UserIds = new List<String>();
            if(System.Trigger.isUpdate){ //After Update Code Block
             
                for(User u: trigger.new){
                    if(u.UserPermissionsSFContentUser != Trigger.oldMap.get(u.id).UserPermissionsSFContentUser && u.UserPermissionsSFContentUser == True){
                        UserIds.add(u.id);
                    }
                }
                
                //CpProductPermissions.AddPremierMarketingKitforCpUserActive(trigger.new);
                if(UserIds.size() > 0){
                    CpProductPermissions.AddVMSInternalUserToGroup(trigger.new);
                }
                /* commenting out below method call as there are no public groups that matches the criteria to insert group members */
                //CpProductPermissions.AddUserInGroup(trigger.new);
               
                Set<id> Setuserids=new Set<id>();
                /*  DE7155: deactivating Technician records when User record gets activated */
               Set<Id> deactivateUsrTechnicianId = new Set<Id>();

                for(user u: trigger.new){
                    if(u.contactid !=null){
                        if((Trigger.oldMap.get(u.id).IsActive != u.IsActive && u.IsActive == false) || u.IsPortalEnabled == false){
                            if(u.accountid!=null){
                                Setuserids.add(u.id);
                            }
                            stContactIddeactivate.add(u.contactid); // set for deactivating the user
                        }
                     }

                     /* DE7155 */
                     if(Trigger.isAfter) {
                        if(Trigger.oldMap.get(u.Id).IsActive != u.IsActive && u.IsActive == false) {
                            deactivateUsrTechnicianId.add(u.Id);
                        }
                     }
                     
                }
                if(!deactivateUsrTechnicianId.isEmpty()) 
                        if(!system.isFuture() && !System.isBatch())
                        SC_ClassTechnician_Equipment.deactivateTechnicianRecords(deactivateUsrTechnicianId);
                /* DE7155 Ends */
                
                if (stContactIddeactivate.size() > 0){
                    if(!system.isFuture() && !System.isBatch())
                    ProductUtils.updateContactPartnerFlag(stContactIddeactivate, false);
                    // ProductUtils.updateContactClearValues(stContactIddeactivate);
                }
                if(Setuserids.size()>0 ){
                    List<GroupMember> DelPartnerGroupMembers = new List<GroupMember>();
                       
                    DelPartnerGroupMembers = [Select Id,GroupId from GroupMember where  UserOrGroupId in: Setuserids];
                    if(DelPartnerGroupMembers.size() > 0){
                        Delete DelPartnerGroupMembers;
                    } 
                }

             /*List<user> userList = new List<user>();
                set<Id> userIdSet = new set<Id>();
                Id profileId = [select Id from Profile where Name =: Label.VMS_Customer_Portal_User].Id;
                for(User ur : trigger.new){
                    if(trigger.oldMap.get(ur.Id).IsPortalEnabled != trigger.newMap.get(ur.Id).IsPortalEnabled && ur.IsPortalEnabled && ur.ProfileId == profileId){
                        userList.add(ur);
                        userIdSet.add(ur.Id);
                    }
                }*/
                //CpProductPermissions.CpAddUserToSelectedPG(userIdSet);
            }
    }

    /* STSK0012842- check if user is from country AU or NZ  starts */
    // Adding new user to chatter group
    if(trigger.isAfter && trigger.isInsert) {
        Map<Id, String> userMap = new Map<Id, String>();
        for (User u:Trigger.new) {
            userMap.put(u.Id, u.Country);
        }
        UserGroupTriggerHandler.addUserToChatterGroup(userMap);
    }
    /* STSK0012842- check if user is from country AU or NZ  ends */
    
    /*
    if(trigger.isAfter && trigger.isUpdate) {
        for (User u:Trigger.new) {
            if(u.ManagerId <> null && u.ManagerId <> trigger.oldMap.get(u.id).ManagerId) {
                List<Account> acctList = [Select Id, Regional_Sales_Manager__c From Account Where OwnerId =: u.Id];
                if(!acctList.isEmpty()) {
                    for(Account acc:acctList) {
                        acc.Regional_Sales_Manager__c = u.ManagerId;
                    }
                    update acctList;
                }
            }
        }
    }
    */
}
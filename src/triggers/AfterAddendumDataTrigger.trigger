/**************************************************************************\
Test Class - AddendumDataIntegrationTest
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
/**************************************************************************/
trigger AfterAddendumDataTrigger on Addendum_Data__c (after insert, after update) {
    set<Id> setAddendum = new set<Id>();
    for(Addendum_Data__c p : trigger.new){
        setAddendum.add(p.Id);
    }
    
    if(setAddendum.size()>0){
        if(trigger.isInsert){
            AddendumDataSynchronization.Synchronization(setAddendum);
        }else if(trigger.isUpdate){
            AddendumDataSynchronization.UpdatePLCData(setAddendum);
        }
    }
}
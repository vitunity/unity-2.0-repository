/**************************************************************************\
    @ Author        : Rakesh Basani
    @ Date      : 08-Aug-2017
    @ Description   : STSK0012077 - Last modification data/time is not updated when emails are sent out of the case.
    Date/Modified By Name/Task or Story or Inc # /Description of Change
    15-Mar-2018 - Rakesh - STRY0049226 - Update case last modified on incoming emails.
****************************************************************************/
trigger AfterInsertSendEmailCase on EmailMessage (after Insert) {
    
    Set<Id> CasesId = new set<Id>();
    for(EmailMessage EM : trigger.new)
    {
        if(Em.ParentID.getSObjectType() == Case.getSObjectType()) {
      //!EM.Incoming && 
            CasesId.add(EM.ParentID);
        }
    }
    if(Label.Case_Lastmodified_Interval<>null){
        Integer intervalMinutes = Integer.valueOf(Label.Case_Lastmodified_Interval);
        if(casesId.size() >0 && !system.isFuture()){
            
            DateTime now  = Datetime.now();
            DateTime nextRunTime = now.addMinutes(intervalMinutes);
            String cronString = '' + nextRunTime.second() + ' ' + nextRunTime.minute() + ' ' + 
            nextRunTime.hour() + ' ' + nextRunTime.day() + ' ' + 
            nextRunTime.month() + ' ? ' + nextRunTime.year(); 
            System.schedule('Case LastModified' + ' - '+ (Math.random()*10000) +'-'+ DateTime.now(), cronString, new SendEmailLastModification(casesId));
            
        }
    }
   
           
}
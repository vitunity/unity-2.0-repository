trigger AfterInsertUpdate_SLADetail on SVMXC__SLA_Detail__c (after insert, after update)
{
    // US4729 : Create Counter Detail Associated with SLA Detail -MS 23-July-2014
    //Nov-15-2017 : Chandramouli : STSK0013428 : Modified the logic to calculate SLA end dates correctly.
    if(trigger.isupdate || trigger.isInsert)
    {
        SR_ClassSLADetailUtillTrigger.createCounterDetail(Trigger.New, Trigger.oldMap); //savon.FF 12-10-2015 Added Old Map to Method 
    }
    /* For future release
    if(trigger.isUpdate)
    {
    	List<SVMXC__SLA_Detail__c>CancelledSLA = new List<SVMXC__SLA_Detail__c>();
		for(SVMXC__SLA_Detail__c sla : trigger.new)
	    {
			if(sla.Status__c == 'Cancelled' && trigger.oldMap.get(sla.id).Status__c != 'Cancelled')
			{
				CancelledSLA.add(sla);
			}
		}
    	if(!CancelledSLA.isEmpty())
	        {
    		SR_ClassSLADetailUtillTrigger.updatePMPToCancelled(CancelledSLA);
	        }
	    }
    }*/
    
}
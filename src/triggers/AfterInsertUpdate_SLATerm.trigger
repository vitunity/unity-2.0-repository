/**************************************************************************
 *    Last Modified By      : Yukti Jain
 *    Last Modified on      : 10/03/2015
 *    Last Modified Reason  : Code Optimization for SR_ClassSLATermUtillTrigger
****************************************************************************/
trigger AfterInsertUpdate_SLATerm on SVMXC__Service_Level__c (after update)
{  
    SR_ClassSLATermUtillTrigger objSLATerm = new SR_ClassSLATermUtillTrigger(); //instantiating class
    //objSLATerm.updateServiceOffering(trigger.newMap); //method to update SLA Term fields //savon.FF 12-29-2015 TA7783 Commented out for future release 
}
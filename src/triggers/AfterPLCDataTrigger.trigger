/**************************************************************************\
@Created Date- 08/02/2017
@Created By - Rakesh Basani   
@Description - INC4202831 - Sync PLC data to BigMachines on Create and update.
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
/**************************************************************************/
trigger AfterPLCDataTrigger on PLC_Data__c (after insert, after update) {
    set<Id> setPLC = new set<Id>();
    for(PLC_Data__c  p : trigger.new){
        setPLC.add(p.Id);
    }
    
    if(setPLC.size()>0){
        if(trigger.isInsert){
            PLCDataSynchronization.Synchronization(setPLC);
        }else if(trigger.isUpdate){
            PLCDataSynchronization.UpdatePLCData(setPLC);
        }
    }
}
/***************************
STRY0068404, NS, 082118:  
     Trigger is to take inbound SOI_Delivery_Verification__c from BOOMI, query, and fill fields from originating record and Product2.
     This is required only for Insert, but may be expanded to be for update if new/old SOI Delivery Verification is different
****************************/

trigger AsInstalledTrigger on As_Installed__c (before insert, before update) {
    map<String, As_Installed__c> mapVerIdAsIns = new map<String, As_Installed__c>();
    map<String, As_Installed__c> mapMatAsIns = new map<String, As_Installed__c>();
    
    for(As_Installed__c Ins : Trigger.New) {
        
        system.debug('### debug Ins.SOI_Delivery_Verification__c = ' + Ins.SOI_Delivery_Verification__c);
        if(Ins.SOI_Delivery_Verification__c != null) {
            mapVerIdAsIns.put(Ins.SOI_Delivery_Verification__c, Ins);
        }
        //Add direct material update as ERP Material is available and SOI Delivery Verification can be null
        if(Ins.ERP_Material__c != null){
            mapMatAsIns.put(Ins.ERP_Material__c, Ins);
        }
    }
    
    if(mapVerIdAsIns.size() > 0) {
        AsInsTriggerHandler.pouplateFromBoomi(mapVerIdAsIns);
    }
    
    //Add new method to handler to populate material directly as ERP Material is always available
    if(mapMatAsIns.size() > 0) {
        AsInsTriggerHandler.populateMaterial(mapMatAsIns);
    }
}
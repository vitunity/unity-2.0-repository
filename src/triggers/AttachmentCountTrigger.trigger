/*************************************************************************
@ Author        : Nick Sauer
@ Date          : 23-Oct-2017
@ Description   : Trigger to Count Attachments to Parent ID

Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
***************************************************************************
23-Oct-2017 Nick Sauer    STSK0013053   1)  Add trigger to fill Work Order field w/ attachment count
            Chandra Vegi  STSK0013053   1)  Review and improve initial code
***************************************************************************/

trigger AttachmentCountTrigger on Attachment (after insert, after update, after delete, after undelete) {
    //Map<Id,List<Attachment>> parent = new Map<Id,List<Attachment>>();
    Map<String,Integer> wo2attachments = new Map<String,Integer>();
    set<id> attachid = new set<id>();
    Map<id,SVMXC__Service_Order__c> wo2update = new Map<id, SVMXC__Service_Order__c>();
 
    if(Trigger.new != null){
        for(Attachment record:Trigger.new){
            if(record.ParentId != null && record.ParentId.getSObjectType() == SVMXC__Service_Order__c.SObjectType){
                attachid.add(record.parentid);
        }
    }
    } else if(Trigger.old != null){
        for(Attachment record:Trigger.old){
            if(record.ParentId != null && record.ParentId.getSObjectType() == SVMXC__Service_Order__c.SObjectType){ 
                attachid.add(Trigger.oldMap.get(record.id).parentid);
        }
        }
    }
    //If there are Attachments for WO within context, then perform action
    if(attachid.size()>0){
        try{
            /* CVegi comments out to improve code  List<Attachment>attachList = new List<Attachment>();
            attachList = [select id, parentid from Attachment where parentid IN: attachid];
            Map<id,SVMXC__Service_Order__c> woMap = new Map<id,SVMXC__Service_Order__c>
                ([select id,Count_of_Attachments_WO__c from SVMXC__Service_Order__c where id IN: attachid]);

            for(Attachment a: attachList){
                List<Attachment>fullList = new List<Attachment>();
                if(parent.get(a.parentid) == null){
                    fullList.add(a);
                    parent.put(a.parentid,fullList);
                } else if(parent.get(a.parentid) != null){
                    fullList = parent.get(a.parentid);
                    fullList.add(a);
                    parent.put(a.parentid,fullList);
                }
             }
            for(Id i:attachid){
                //Work Order has 1 or more attachments
                if(woMap.get(i) !=null && parent.get(i) != null){
                    woMap.get(i).Count_of_Attachments_WO__c = parent.get(i).size();  
                    
                //Work Order has no attachments (deletion)    
                } else if(woMap.get(i) != null && parent.get(i) == null){
                    woMap.get(i).Count_of_Attachments_WO__c = 0;
                }
            }
            */
            AggregateResult[] agrresult = [select count(Id)wocnt, parentid from Attachment where parentid in :attachid group by parentid];
            for (AggregateResult ar : agrresult)
            {
                wo2attachments.put(String.valueOf(ar.get('parentid')), Integer.valueOf(ar.get('wocnt')));
            }
            for (String woid : attachid)
            {
                SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
                workorder.Id = woid;
                if (wo2attachments.size() > 0 && wo2attachments.containskey(woid) && wo2attachments.get(woid) != null)
                {
                    workorder.Count_of_Attachments_WO__c = wo2attachments.get(woid);
                }
                else if (wo2attachments.size () > 0 && !wo2attachments.containskey(woid))
                {
                    workorder.Count_of_Attachments_WO__c = 0;
                }
                else if (wo2attachments.size() == 0)
                {
                    workorder.Count_of_Attachments_WO__c = 0;
                }
                wo2update.put(woid,workorder);
            }
            //Execute DML operation
            update wo2update.values();
            //Debug log on DML operation
            System.Debug(wo2update.values());
        }catch(Exception e){}
      }
}
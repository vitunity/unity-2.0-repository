trigger AutoDeliverContentTrigger on ContentDocument (after update)
{
    String libraryId;

    for(ContentWorkspace workspace : [select Id from ContentWorkspace where Name = 'Charket – documentation' limit 1])
    {
        libraryId = workspace.Id;
    }

    if(String.isBlank(libraryId))
    {
        return;
    }

    Set<Id> documentIds = new Set<Id>();
    for(ContentDocument document : Trigger.new)
    {
        if(document.ParentId == libraryId && Trigger.oldMap.get(document.Id).ParentId != libraryId)
        {
            documentIds.add(document.Id);
        }
    }

    if(documentIds.size() > 0)
    {
        Charket__WeChatAccount__c weChatAccount = [select Name from Charket__WeChatAccount__c order by CreatedDate limit 1];
        List<ContentDistribution> distributions = new List<ContentDistribution>();

        for(ContentVersion version : [select Title from ContentVersion where ContentDocumentId in :documentIds and IsLatest = true])
        {
            ContentDistribution newDistribution = new ContentDistribution();

            newDistribution.Name = version.Title;
            newDistribution.ContentVersionId = version.Id;
            newDistribution.PreferencesAllowViewInBrowser = true;
            newDistribution.PreferencesLinkLatestVersion = true;
            newDistribution.PreferencesNotifyOnVisit = false;
            newDistribution.RelatedRecordId = weChatAccount.Id;

            distributions.add(newDistribution);
        }

        try
        {
            insert distributions;
        }
        catch(Exception ex)
        {
            Trigger.new[0].addError(ex.getMessage());
        }
    }
}
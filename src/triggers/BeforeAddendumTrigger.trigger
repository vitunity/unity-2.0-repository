/*
Test Class - AddendumDataIntegrationTest
*/
trigger BeforeAddendumTrigger on Addendum_Data__c (before insert, before update, before delete) {

    List<Addendum_Data__c > IsNewAddendumData = new List<Addendum_Data__c >();
    string regioncode;
    
    if(trigger.isInsert || trigger.isUpdate){
        for(Addendum_Data__c p : trigger.new){
            if(trigger.isInsert || (trigger.isUpdate && (trigger.oldmap.get(p.id).country__c <> p.country__c 
            //|| trigger.oldmap.get(p.id).region__c <> p.region__c
            ))){
                IsNewAddendumData.add(p);
            }
            if((trigger.isInsert) || (trigger.isUpdate && (trigger.oldmap.get(p.id).region__c <> p.region__c))){
                if(p.region__c.equalsignoreCase('WW')){
                    if(regioncode == null){
                        for(string s : PickListValueController.GetOptions('Addendum_Data__c','Region__c')){
                            if(!s.equalsignoreCase('WW')){
                                if(regioncode==null)regioncode = s;
                                else regioncode += '/'+s;
                            }
                        }                    
                    }
                    p.region_code__c = regioncode;
                }
                else{
                    p.region_code__c = p.region__c;
                }
                 
            }
        }
        
        if(IsNewAddendumData.size() > 0){
            set<string> setCountry = new set<string>();
            //map<string,list<string>> mapRegionCountry = DependentPickListValueController.GetDependentOptions('PLC_Data__c','Region__c','country__c');
            for(Addendum_Data__c p : IsNewAddendumData){
                if(p.Country__c <> null){
                    for(string s : (p.country__c).split(';')){
                        setCountry.add(s.trim());
                    }            
                }
                /*else if(p.region__c <> null){
                    if(mapRegionCountry.containsKey(p.region__c)){
                        for(string c : mapRegionCountry.get(p.region__c)){
                            setCountry.add(c);
                        }
                    }
                }*/
            }
            map<String,string> mapCountryCode = new map<string,string>();
            List<Country__c> lstCountry = [select Name,SAP_Code__c from Country__c where name IN: setCountry and SAP_Code__c <> null];
            for(Country__c c: lstCountry ){       
                mapCountryCode.put((c.Name).toLowerCase(),c.SAP_Code__c );
            }
            
            for(Addendum_Data__c p : IsNewAddendumData){
                string countrycode;
                List<string> lstCountries = new List<string>();
                if(p.Country__c <> null){
                    lstCountries = (p.country__c).split(';');                        
                }
                /*else if(p.region__c <> null){
                    if(mapRegionCountry.containsKey(p.region__c)){
                        lstCountries = mapRegionCountry.get(p.region__c);
                    }
                }*/
                for(string s : lstCountries){            
                    string country = s.trim();
                    country = country.toLowerCase();
                    if(mapCountryCode.containsKey(country )){
                        if(countrycode == null) countrycode = mapCountryCode.get(country );
                        else countrycode += '/'+mapCountryCode.get(country );
                    }
                }
               p.Country_Code__c = countrycode;
            }
        }
    }
    
    if(trigger.isDelete){
        set<string> setAddendumData = new set<string>();
        for(Addendum_Data__c p : trigger.old){
            setAddendumData.add(p.Name);
        }
        if(setAddendumData.size()>0)
        AddendumDataSynchronization.DeletePLCData(setAddendumData);
    }
    
    
    
}
trigger BeforeInsertUpdate_SLATerm on SVMXC__Service_Level__c (before insert, before update) 
{
    //To update Business Hours on SLA term's from business hours of contract// US3811  
   /* List<SVMXC__Service_Level__c> lstOFSLATermsWithERPContactNO = new List<SVMXC__Service_Level__c>();
    for(SVMXC__Service_Level__c  slaTerm:Trigger.new)
    {
        if(slaTerm.ERP_Contract_Nbr__c!=null)
        {
            lstOFSLATermsWithERPContactNO.add(slaTerm);
        }
    }
    if(lstOFSLATermsWithERPContactNO.size()>0)
    {
        
    } */
    system.debug('!##$%^%$#' + system.now());
    SR_ClassSLATermUtillTrigger objSLATerms = new SR_ClassSLATermUtillTrigger(); //initiating class
     if (trigger.isUpdate)
      {
    	objSLATerms.UpdateBusinessHoursOfSLATerm(trigger.new, trigger.oldmap); //added oldmap parameter
		}
}
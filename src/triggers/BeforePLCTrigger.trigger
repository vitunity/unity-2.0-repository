/**************************************************************************\
@Created Date- 08/02/2017
@Created By - Rakesh Basani   
@Description - INC4202831 - Sync PLC data to BigMachines on Delete and updating country based on region. 
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
/**************************************************************************/
trigger BeforePLCTrigger on PLC_Data__c (before insert, before update, before delete) {

    List<PLC_Data__c> lstNewPLCData = new List<PLC_Data__c>();
    string regioncode;
    
    if(trigger.isInsert || trigger.isUpdate){
        for(PLC_Data__c p : trigger.new){
            if(trigger.isInsert || (trigger.isUpdate && (trigger.oldmap.get(p.id).country__c <> p.country__c 
            //|| trigger.oldmap.get(p.id).region__c <> p.region__c
            ))){
                lstNewPLCData.add(p);
            }
            if((trigger.isInsert) || (trigger.isUpdate && (trigger.oldmap.get(p.id).region__c <> p.region__c))){
                if(p.region__c.equalsignoreCase('WW')){
                    if(regioncode == null){
                        for(string s : PickListValueController.GetOptions('PLC_Data__c','Region__c')){
                            if(!s.equalsignoreCase('WW')){
                                if(regioncode==null)regioncode = s;
                                else regioncode += '/'+s;
                            }
                        }                    
                    }
                    p.region_code__c = regioncode;
                }
                else{
                    p.region_code__c = p.region__c;
                }
                 
            }
        }
        
        if(lstNewPLCData.size() > 0){
            set<string> setCountry = new set<string>();
            //map<string,list<string>> mapRegionCountry = DependentPickListValueController.GetDependentOptions('PLC_Data__c','Region__c','country__c');
            for(PLC_Data__c p : lstNewPLCData){
                if(p.Country__c <> null){
                    for(string s : (p.country__c).split(';')){
                        setCountry.add(s.trim());
                    }            
                }
                /*else if(p.region__c <> null){
                    if(mapRegionCountry.containsKey(p.region__c)){
                        for(string c : mapRegionCountry.get(p.region__c)){
                            setCountry.add(c);
                        }
                    }
                }*/
            }
            map<String,string> mapCountryCode = new map<string,string>();
            List<Country__c> lstCountry = [select Name,SAP_Code__c from Country__c where name IN: setCountry and SAP_Code__c <> null];
            for(Country__c c: lstCountry ){       
                mapCountryCode.put((c.Name).toLowerCase(),c.SAP_Code__c );
            }
            
            for(PLC_Data__c p : lstNewPLCData){
                string countrycode;
                List<string> lstCountries = new List<string>();
                if(p.Country__c <> null){
                    lstCountries = (p.country__c).split(';');                        
                }
                /*else if(p.region__c <> null){
                    if(mapRegionCountry.containsKey(p.region__c)){
                        lstCountries = mapRegionCountry.get(p.region__c);
                    }
                }*/
                for(string s : lstCountries){            
                    string country = s.trim();
                    country = country.toLowerCase();
                    if(mapCountryCode.containsKey(country )){
                        if(countrycode == null) countrycode = mapCountryCode.get(country );
                        else countrycode += '/'+mapCountryCode.get(country );
                    }
                }
               p.Country_Code__c = countrycode;
            }
        }
    }
    if(trigger.isDelete){
        set<string> setPLCData = new set<string>();
        for(PLC_Data__c p : trigger.old){
            setPLCData.add(p.Name);
        }
        if(setPLCData.size()>0)
        PLCDataSynchronization.DeletePLCData(setPLCData);
    }
    
    
    
}
trigger BigMachinesQuoteProductTrigger on BigMachines__Quote_Product__c (after insert, after update, after delete) {
     system.debug('BookingValueCalculationHandler.isExecuted = '+BookingValueCalculationHandler.isExecuted);
     system.debug('BigMachinesQuoteProductTrigger  Inside');
    //Calculated booking value
     if(Trigger.isafter && (Trigger.isupdate || Trigger.isInsert || Trigger.isdelete)){
        if(!BookingValueCalculationHandler.isExecuted) {
            system.debug('booking value set method invoke');
            set<Id> quoteIds =  new set<Id>();
            list<BigMachines__Quote_Product__c> newList = Trigger.new;
            if(Trigger.isdelete){
                newList = Trigger.old;
            }
            for(BigMachines__Quote_Product__c  bqp : newList ) {
                quoteIds.add(bqp.BigMachines__Quote__c);
            }
            if(!quoteIds.isEmpty()){
                BookingValueCalculationHandler.calculateBookingValue(quoteIds);
            }
        }
     }
}
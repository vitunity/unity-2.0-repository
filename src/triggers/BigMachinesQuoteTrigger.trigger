/*
 * @author - amitkumar katre
 * @description - update and create omni quote convertion,
 *                updateOpportunityStage method added  by krishna
 * @date - 11/24/2015
 */ 
trigger BigMachinesQuoteTrigger on BigMachines__Quote__c (after insert, after update, before update, before insert) {
    
    if(trigger.isAfter){
        System.debug('------isAfter');
        if(PreventBMQ.runOnce())
        QuoteTriggerHandler.updateOpportunityFields(trigger.newMap, trigger.oldMap); //created by krishna 
    }
    
    if(trigger.isUpdate && trigger.isAfter){
        QuoteTriggerHandler.updateOMNIQuoteConversion(trigger.new, trigger.oldMap);
    }

    //  Update updateOldContractDetails
    if(trigger.isBefore){
        QuoteTriggerHandler.updateOldContractDetails(trigger.new);
    }
    
    /** 
        Edit by Abhinay
    **/
    
    if(trigger.isupdate && trigger.isbefore){
        Map <Id, BigMachines__Quote__c> oldMapRecords = Trigger.oldMap;
        List<BigMachines__Quote__c> newListRecords = Trigger.new;
        List<Quote_SAP_User_Tracker__c> userTrackerRecordsList = new List<Quote_SAP_User_Tracker__c>();
        
        for(BigMachines__Quote__c eachNewRecord : Trigger.new)
        {
            BigMachines__Quote__c oldRecord = oldMapRecords.get(eachNewRecord.Id);
            if(oldRecord.Submitted_To_SAP_By__c != NULL && oldRecord.Submitted_To_SAP_By__c != eachNewRecord.Submitted_To_SAP_By__c)
            {
                System.debug('Value changed');
                Quote_SAP_User_Tracker__c newJunctionrecord = new Quote_SAP_User_Tracker__c();
                newJunctionrecord.Submitted_to_SAP_by_User__c =  oldRecord.Submitted_To_SAP_By__c;
                newJunctionrecord.Quote__c = eachNewRecord.Id;
                
                userTrackerRecordsList.add(newJunctionrecord);
                //create the history record here
            }
        }
        
        insert userTrackerRecordsList;
    }
    
    /* Added by Ajinkya     
     * To fix Fomatting SAP Booked Service contract field which was causing problems in Layout      
     */
    List<BigMachines__Oracle_User__c> listDeligatedApprover = new List<BigMachines__Oracle_User__c>();
    if((trigger.isUpdate || trigger.isInsert) && trigger.isBefore){     
        for(BigMachines__Quote__c eachNewRecord : Trigger.new){     

            if(eachNewRecord.SAP_Booked_Service__c  != null){       
                SAPBookedServiceFormatter objformatter = new SAPBookedServiceFormatter(eachNewRecord.SAP_Booked_Service__c,5);      
                eachNewRecord.SAP_Booked_Service__c = objformatter.getFormattedContracts();     
            }
            
            if(eachNewRecord.Current_Approvers__c  != null){
                String currentApprovers = eachNewRecord.Current_Approvers__c;
                List<String> ListCurrentApprovers = currentApprovers.Split(',');
                String DeligatedApproversString = '';
                
                listDeligatedApprover = [Select BigMachines__Delegated_Approver__r.BigMachines__Login__c From BigMachines__Oracle_User__c Where BigMachines__Oracle_CPQ_Cloud_Login__c IN :ListCurrentApprovers];
                
                for (integer i=0;i<listDeligatedApprover.size();i++) {
                    if (i==0){
                        DeligatedApproversString = listDeligatedApprover[i].BigMachines__Delegated_Approver__r.BigMachines__Login__c;                        
                    }else{
                        DeligatedApproversString = DeligatedApproversString+','+ listDeligatedApprover[i].BigMachines__Delegated_Approver__r.BigMachines__Login__c;
                    }
                }
                
                eachNewRecord.Delegated_Approvers__c = DeligatedApproversString;
            }
            
        }
    } 
    /*
     * Cancel replace change quote on opportunity and quote
     */
    if((trigger.isUpdate || trigger.isInsert) && trigger.isAfter){       
        QuoteTriggerHandler.updateReplacedQuote(trigger.new,trigger.oldmap);
        
        // INC4779791 
        //for(BigMachines__Quote__c quote : trigger.new) {
        //    if(quote.BigMachines__Is_Primary__c && !quote.No_Contingencies__c) {
                //TestHandler.sendReminderEmail(trigger.new, trigger.oldMap );
        //    }
        //}

    }
    
}
trigger BlockedItemsTrigger on Blocked_Items__c (after insert, after update, after delete) {
    
    Blocked_Items__c blockedItemNew = trigger.new[0];
    string recordid = blockedItemNew.Id;
    Blocked_Items__c blockedItemOld;
    if(trigger.oldMap != null){
        blockedItemOld = trigger.oldMap.get(blockedItemNew.Id);
    }
    
    if(trigger.new.size() > 0)
    {
        /*for (Blocked_Items__c BI : trigger.new)
        //{
          //  recordid = BI.Id;
        String blockedItemsQuery = 'SELECT Id,'+ 
            'Input_Clearance__r.Model_Part_Number__c,Input_Clearance__r.Country__r.Name,Item_Part__r.Product_Code__c,CEF_Country__c,Item_Model__c '+
            'FROM Blocked_Items__c '+
            'WHERE Id = :recordid '+
            'AND Item_Part__r.Product_Code__c!=\'\'';
            
            if(trigger.isInsert || (trigger.isUpdate )){
        */
        String query = 'SELECT Id,'
        +'Input_Clearance__r.Model_Part_Number__c,Input_Clearance__r.Country__r.Name,Item_Part__r.Product_Code__c,Input_Clearance__r.Review_Product__c,Input_Clearance__r.Business_Unit__c,Item_Model__c '
        +'FROM Blocked_Items__c '
        +'WHERE Clearance_Status__c = \'Blocked\' and Item_Part__r.Product_Code__c!=\'\' and Item_Model__c!=\'\' ';

        
        SalesforceBMIDataSync syncData = new SalesforceBMIDataSync(
            query,
            'Blocked_Items__c',
            'BlockedItems',
            'Country',
            ''
        );
        Database.executeBatch(syncData);
        /*
                if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 50){ 
                    UpdateBMIDataTable sfBMiSync = new UpdateBMIDataTable('Blocked_Items__c', 'BlockedItems', 'Country', BI.CEF_Country__c, Database.query(blockedItemsQuery));
                    System.enqueueJob(sfBMiSync);
                }*/
      //      }
    //    }   
    }
    if (trigger.isDelete)
    {
        String query = 'SELECT Id,'
        +'Input_Clearance__r.Model_Part_Number__c,Input_Clearance__r.Country__r.Name,Item_Part__r.Product_Code__c,Input_Clearance__r.Review_Product__c,Input_Clearance__r.Business_Unit__c,Item_Model__c '
        +'FROM Blocked_Items__c '
        +'WHERE Clearance_Status__c = \'Blocked\' and Item_Part__r.Product_Code__c!=\'\' and Item_Model__c!=\'\'';

        
        SalesforceBMIDataSync syncData = new SalesforceBMIDataSync(
            query,
            'Blocked_Items__c',
            'BlockedItems',
            'Country',
            ''
        );
        Database.executeBatch(syncData);
    }
}
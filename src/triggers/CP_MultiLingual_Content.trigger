/*************************************************************************\
    @ Author        : Amit Kumar
    @ Date      : 17-May-2013
    @ Description   : This trigger is Used for MultiLanguage Updation on Content
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

trigger CP_MultiLingual_Content on ContentVersion (after update) {
    
    CP_MultiLingual_Content content_Multilingualupdation = new CP_MultiLingual_Content();
    map<Id,ContentVersion> oldmapofcontentversion = new map<Id,ContentVersion>(); // old values of map. This will be blank If insert event is fired
    if(trigger.Isupdate){
        oldmapofcontentversion = trigger.oldmap; // setting the old map values in case content is updated.
    }
    // method of apex class being called to update the multi language check box on content.
    content_Multilingualupdation.MultiLingual_Content(trigger.newmap,oldmapofcontentversion );

    

}
/*************************************************************************
Change Log:
24-Aug-2017 - Divya Hargunani - STSK0012692: MyVarian: CRA enhancement not working - Added after delete logic to remove members from product group
*************************************************************************/

trigger CRA_Delete_Contact on Contact_Role_Association__c (Before delete, After delete) 
{   
    if(Trigger.isBefore){ 
    List<ObjectPermissions> lstPermissions =  [SELECT ParentId FROM ObjectPermissions WHERE SObjectType = 'Contact' AND PermissionsEdit = true];
    set<Id> setPermissionIds = new set<Id>();
    for(ObjectPermissions p: lstPermissions){
        setPermissionIds.add(p.ParentId);
    }
           
    boolean isContactEditPermission = false;
    List<PermissionSetAssignment> lstPermissionAssignments = [select PermissionSet.Id from PermissionSetAssignment where  PermissionSetId IN: setPermissionIds and  AssigneeId =: userinfo.getuserId()];
    if(lstPermissionAssignments.size() > 0 ){
        isContactEditPermission = true;
    }
    set<Id> setContactIds = new set<Id>();
    for(Contact_Role_Association__c c : trigger.old){
        if(!isContactEditPermission){
            c.addError('You do not have permission to delete contact role association.',false);
        }
    }
    }
    
    if(Trigger.isAfter){
    CRATriggerHandler.removeProductGroup(Trigger.old); 
    }
}
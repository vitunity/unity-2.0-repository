Trigger CaseCommentTrigger on CaseComment(after insert){    
    set<Id> setCase = new set<Id>(); 
    List<caseComment> lstCC = new  List<caseComment>();  
    
    for(CaseComment cc : trigger.new){
        if(cc.IsPublished){
            setcase.add(cc.parentId);
            lstCC .add(cc);
        }
        
    }
    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: label.FromEmailCaseComment ];   //'variancustomersupport@varian.com'
    map<Id,Case> mapCase = new map<Id,Case>([select Id,ContactId,Local_Case_Activity__c,Local_Description__c,Description_en_new__c,ContactEmail, Description, Case_Activity__c,Reason from Case where Id IN: setCase]);
    List<EmailTemplate> lstCaseCommentEmail = [select Id from EmailTemplate where Name = 'Case_Comment_Email'];
    if(lstCaseCommentEmail.size() > 0){
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();
        for(CaseComment cc : lstCC){
            
            string commentBody = cc.Commentbody;
            /*string CaseDescription = mapCase.get(cc.parentId).Local_Description__c;
            if(CaseDescription<>null)
            CaseDescription = CaseDescription.replaceAll('<[/a-zAZ0-9]*>','');
            
            string CaseRecommendation = mapCase.get(cc.parentId).Local_Case_Activity__c;
            if(CaseRecommendation<>null)
            CaseRecommendation = CaseRecommendation.replaceAll('<[/a-zAZ0-9]*>','');*/
            //Chandra - Nov 20 2017 - STSK0012982 - Added below logic to skip sending of emails on case closure for cases of reason "Installation Assistance".
            if(!commentBody.StartsWith('Description:') && !commentBody.startsWith('Recommendation: ') && (cc.parentId != null && mapCase.size() > 0 && mapcase.containskey(cc.ParentID) && mapCase.get(cc.parentID) != null && mapCase.get(cc.parentId).Reason != null && mapCase.get(cc.parentId).Reason != 'Installation Assistance'))
            //if(commentBody.equals('Description:'+CaseDescription) || commentBody.equals('Recommendation: '+CaseRecommendation))
            {
                if(mapCase.get(cc.parentId).contactemail <> null)
                {           
                    setcase.add(cc.parentId);
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(mapCase.get(cc.parentId).ContactId);
                    mail.setTemplateId(lstCaseCommentEmail[0].id);
                    mail.setWhatId(cc.ParentId);
                    mail.setSaveAsActivity(true);     
                    lstEmail.add(mail); 
                    if ( owea.size() > 0 ) {
                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                    }    
                }   
            }   
        }      
            try{
            Messaging.sendEmail(lstEmail);
            }catch(Exception e){
                system.debug('##:'+e.getMessage());
            }
            
        }
      
}
trigger CatalogPartPriceTrigger on Catalog_Part_Price__c (before insert) {

  // Catalog_Part__c newPart = Trigger.new
  Catalog_Part_Price__c[] parts = Trigger.new;
  List<String> extIds = new List<String>();

  for(Catalog_Part_Price__c part : parts) {
    extIds.add(part.Catalog_Part_External_Id__c);
  }

  List<Catalog_Part__c> products = [SELECT Id, Name, External_ID__c FROM Catalog_Part__c WHERE External_ID__c IN :extIds];
  Map<String, Catalog_Part__c> partMap = new Map<String, Catalog_Part__c>();
  for(Catalog_Part__c product : products) {
    partMap.put(product.External_ID__c, product);
  }

  for(Catalog_Part_Price__c part : parts) {
    if(partMap.containsKey(part.Catalog_Part_External_Id__c)){
      Catalog_Part__c currentProduct = partMap.get(part.Catalog_Part_External_Id__c);
      part.Catalog_Part__c = currentProduct.Id;  
    }
  }
}
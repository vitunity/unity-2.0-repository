trigger CatalogPartTrigger on Catalog_Part__c (before insert) {

  // Catalog_Part__c newPart = Trigger.new
  Catalog_Part__c[] parts = Trigger.new;
  Set<String> productNumbers = new Set<String>();

  for(Catalog_Part__c part : parts) {
    productNumbers.add(part.Name);
  }

  List<Product2> products = [SELECT Id, BigMachines__Part_Number__c FROM Product2 WHERE BigMachines__Part_Number__c IN :productNumbers];
  Map<String, Product2> partMap = new Map<String, Product2>();
  for(Product2 product : products) {
    partMap.put(product.BigMachines__Part_Number__c, product);
  }

  for(Catalog_Part__c part : parts) {
    Product2 currentProduct = partMap.get(part.Name);
    if(currentProduct != null) {
      part.Product__c = currentProduct.Id;  
    }
  }
}
trigger ChowToolSendEmailAlerts on Chow_Tool__c (before insert,before update) {
    
    if(Trigger.isbefore && Trigger.isInsert)
    { 
        for(Chow_Tool__c chow : trigger.new){            
            if(chow.Active_Task_Assignee__c != null){
                  ChowToolEmailAlert.updateEmail(trigger.new);
            }
               
        }
        
    }
    if(Trigger.isbefore && Trigger.isUpdate)
    { 
        for(Chow_Tool__c chow : trigger.new){
            chow.Assignment_Date__c=system.today();
                 ChowToolEmailAlert.updateEmail(trigger.new);
        }
        
    }                                          
     
}
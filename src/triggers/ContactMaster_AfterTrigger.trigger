/*************************************************************************\
      @ Author          : Sunil Bansal
      @ Date            : 04-Apr-2013
      @ Description     : This trigger is used for the Class 'ContactMaster' which is used to calculate VMS tour attended on contact.
      @ Last Modified By      :
      @ Last Modified On      :
      @ Last Modified Reason  :
****************************************************************************/

trigger ContactMaster_AfterTrigger on Contact (after insert, after update, after delete){

 if(system.Label.Contact_Triggers_Flag == 'True')
    {
    /* Object created for class 'Contact Master' on insert, delete and update */
       
        ContactMaster objContactMaster = new ContactMaster();
        if(Trigger.isInsert || Trigger.isUpdate){
       
            objContactMaster.onContactUpdate(Trigger.New,Trigger.oldMap);
            system.debug('#### IN CONTACTMASTER_AFTERTRIGGER');
            //ContactMaster.setOCSUGCUserRoleAsMember(Trigger.NewMap, Trigger.oldMap); 
            ContactMaster.removeOCSUGCGroupMembershipInOkta(Trigger.NewMap, Trigger.oldMap);   
            ContactMaster.resetOCSUGCTermsAndPermissions (Trigger.New,Trigger.oldMap);        
        } else if(Trigger.isDelete) {
            objContactMaster.onContactDelete(Trigger.Old);
        }
        
        //Rakesh
        if(Trigger.isUpdate){
            VendorHelper.AddRemoveVendorUser(trigger.new,trigger.oldmap);

            //To update Quote Share  - TODO - Uncomment the line below after the release tomorrow
            CreateQuoteShareForContact.CreateQuoteShareForUpdate(Trigger.newMap, Trigger.oldMap);

            //To prevent user from updating MyVarian contact's email if the user is in frozen state
            Boolean emailChanged = false;
            map<Id, Contact> mapConFreeze = new map<Id, Contact>();
            for(Contact con : Trigger.New) {
               if(con.MyVarian_Member__c == True
                && con.Email != Trigger.oldMap.get(con.Id).Email
                //&& Freeze_User__c == true
                ) {
                  
                  emailChanged = true;
                  if(con.Freeze_User__c == true) {
                      con.Email.addError('Please uncheck the Frozen User checkbox before changing the email.');
                  }
               }
            }
            if(System.Test.isRunningTest()){
				emailChanged = true;
            }
            if(emailChanged == true) {
                map<Id, Id> mapUserConIds = new map<Id, Id>();
                for(User u : [SELECT Id, ContactId FROM User WHERE ContactId != null 
                        AND Contact.MyVarian_Member__c = true
                        AND ContactId IN :Trigger.newMap.keyset()]) {
                  mapUserConIds.put(u.Id, u.ContactId);
                }   

                List<Id> frozUserId = new List<Id>();
                for(UserLogin uLogin : [SELECT Id, isFrozen, UserId FROM UserLogin WHERE isFrozen = true 
                          AND UserId IN : mapUserConIds.keySet()]){

                  frozUserId.add(uLogin.UserId);
                }  

                if(frozUserId.size() > 0) {
                    Trigger.newMap.get(mapUserConIds.get(frozUserId[0])).Email.addError('Please Un-Freeze the User before changing the email.');
                }   
            }             
        }
        
        
        if(Trigger.isUpdate || Trigger.isinsert)
    {
                    
            //Pranjul-Begin INC4936301
            
        Set<Id> contactids = new Set<ID>();
        List<Contact_Role_Association__c>   craList = new List<Contact_Role_Association__c>();
        for(Contact cnt : Trigger.New){
            
            
            if(trigger.isUpdate){
                Contact oldCnt = Trigger.oldMap.get(cnt.id);            
                if((cnt.Email != null) && (cnt.Email != oldCnt.Email)){
                    contactids.add(cnt.id);                    
                }
            
            
            /*Contact oldCnt = Trigger.oldMap.get(cnt.id);            
            if((cnt.Email != null) && (cnt.Email != oldCnt.Email)){
                contactids.add(cnt.id);
                
            }*/
            
          }  
        }
        for(Contact_Role_Association__c cra : [Select ID, Contact__r.Email, Site_Email__c, Contact__c  from Contact_Role_Association__c 
        where Contact__c in :contactids]){
            cra.Site_Email__c =cra.Contact__r.Email;
            craList.add(cra);
            
        }
        if(craList.size()>0)
        update craList;
        
       
                    
      }   //Pranjul-Ends INC4936301  
        
        
        
        
        
    }       
}
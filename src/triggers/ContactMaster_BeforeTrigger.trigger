/*************************************************************************\
    @ Author        : Sunil Bansal
    @ Date          : 07-June-2013
    @ Description   : This trigger will update the Mailing Address Custom Fields from Standard Mailing Street field and vice-versa
    @ Last Modified By  :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
****************************************************************************/


trigger ContactMaster_BeforeTrigger on Contact (before insert, before update){
    
    private static final Set<String> OCSDC_USER_STATUS = new Set<String>{'Invited','Member'};
        
        
     //Pranjul- Start INC4934358 
    for(Contact cnt: Trigger.New){
        //Contact oldCnt = Trigger.oldMap.get(cnt.id);   
        
        
        if(trigger.isUpdate){
            
            if((cnt.Email_Opt_in__c !=Trigger.oldMap.get(cnt.id).Email_Opt_in__c) && (cnt.Email_Opt_in__c == True) ){
               cnt.HasOptedOutOfEmail = False;
                
            }
            else if ( (cnt.HasOptedOutOfEmail != Trigger.oldMap.get(cnt.id).HasOptedOutOfEmail) && (cnt.HasOptedOutOfEmail == True)){
             cnt.Email_Opt_in__c = False ;
            }
        }
        if(trigger.isInsert){
        
        if(cnt.Email_Opt_in__c == True){
           cnt.HasOptedOutOfEmail = False;
            
        }
        else if (cnt.HasOptedOutOfEmail == True){
         cnt.Email_Opt_in__c = False ;
        }
        
       }
    }
        //Pranjul Ends   INC4934358 
    
    //Rakesh Start 22nd feb
        if(trigger.isInsert)
            ContactPreferredLanguage.UpdatePreferredLanguage(trigger.New);
    //Rakesh End
    //STSK0011153 : start 
    if(trigger.isupdate && trigger.isBefore)
    MyVarianDupe.UpdateDupes(trigger.newmap,trigger.oldmap); 
    //STSK0011153 : end 
    
    if(system.Label.Contact_Triggers_Flag == 'True')
    {  
        ContactMaster.updateOCSUGCUserStatus(Trigger.new, Trigger.oldMap);

        set<string> functionalRoleSet = new set<string>{'Nurse - Manager','Pharmacist - Chief','Physicist - Chief','Therapist - Chief',
                                                             'Chief/Dept Administrator', 'Physician', 'IT - Management','Executive Management'};
                                                             
        //private static final Set<String> OCSDC_USER_STATUS = new Set<String>{'Invited','Member'};
                                                                     
        if(Trigger.isInsert)
        {
            String mailingStrt ='' ;
            for(Contact newContact: Trigger.New)   {
                system.debug('###########------AccountId = '+newContact.AccountId);
                system.debug('###########------FUNCTIONAL ROLE = '+newContact.Functional_Role__c);
                if(newContact.AccountId != null) {
                    //Added logic to populate contact fields from Account
                    Account accnt = [select id,name,Prefered_Language__c, Distributor__c, BillingCity, BillingCountry, BillingState, BillingStreet, BillingPostalCode from Account where id = :newContact.AccountId];

                    if ((newContact.Preferred_Language__c == null)  && (accnt.Prefered_Language__c != null))    newContact.Preferred_Language__c = accnt.Prefered_Language__c;
                    if ((newContact.MailingStreet == null) && (accnt.BillingStreet     != null))            { newcontact.MailingStreet = accnt.BillingStreet; mailingStrt = accnt.BillingStreet;}
                    else if(newContact.MailingStreet != null ) mailingStrt = newContact.MailingStreet;
                    if ((newContact.MailingCity == null) && (accnt.BillingCity         != null))                newcontact.MailingCity = accnt.BillingCity;
                    if ((newContact.MailingState == null) && (accnt.BillingState       != null))                newcontact.MailingState = accnt.BillingState;
                    if ((newContact.MailingPostalCode == null)  && (accnt.BillingPostalCode   != null))         newcontact.MailingPostalCode = accnt.BillingPostalCode;
                    if ((newContact.MailingCountry == null) && (accnt.BillingCountry       != null))            newcontact.MailingCountry = accnt.BillingCountry;
    //                if ((newContact.Distributor_Partner__c == false) && (accnt.Distributor__c == true))             newcontact.Distributor_Partner__c = accnt.Distributor__c;

                }


              /*  if(!newContact.Annual_Relationship_Survey__c) {
                    if (functionalRoleSet.contains(newContact.Functional_Role__c))
                        newContact.Annual_Relationship_Survey__c=true;
                }  */


                system.debug('###########------Mailing street = '+mailingStrt);
                //system.debug('###########------ACCOUNT Mailing street = '+accnt.BillingStreet);



                if(mailingStrt != null && mailingStrt.trim().length() > 0) {

                //if(newContact.MailingStreet != null && newContact.MailingStreet.trim().length() > 0) {
                    //String mailingStreet = newContact.MailingStreet;
                    String mailingStreet = mailingStrt;
                    Integer firstComma = mailingStreet.indexOf(',');
                    System.debug('FIRST COMMA == '+firstComma);
                    if(firstComma > -1) {
                        newContact.Mailing_Address1__c = mailingStreet.substring(0,firstComma);
                        Integer secondComma = mailingStreet.indexOf(',', firstComma+1);
                        System.debug('SECOND COMMA == '+secondComma+'   ----------Mailing_Address1__c'+newContact.Mailing_Address1__c);
                        if(secondComma > -1) {
                            newContact.Mailing_Address2__c = mailingStreet.substring(firstComma+1, secondComma);
                            System.debug('   ----------Mailing_Address2__c'+newContact.Mailing_Address2__c);

                            if(mailingStreet.length() >  (secondComma + 1)) {
                                System.debug('THIRD COMMA LENGTH == '+mailingStreet.length());
                                newContact.Mailing_Address3__c = mailingStreet.substring(secondComma+1);
                                System.debug('   ----------Mailing_Address3__c'+newContact.Mailing_Address3__c);
                            }
                        }
                        else if(mailingStreet.length() >  (firstComma + 1)) {
                            newContact.Mailing_Address2__c = mailingStreet.substring(firstComma+1);
                            newContact.Mailing_Address3__c = '';
                        }
                    }
                    else {
                        newContact.Mailing_Address1__c = mailingStreet.substring(0);
                        newContact.Mailing_Address2__c = '';
                        newContact.Mailing_Address3__c = '';
                        System.debug('   ----------Mailing_Address1__c'+newContact.Mailing_Address1__c);
                    }
                }



                String mailingStreet = '';
                if((newContact.Mailing_Address1__c != NULL && newContact.Mailing_Address1__c.trim().length() > 0) || (newContact.Mailing_Address2__c != NULL && newContact.Mailing_Address2__c.trim().length() > 0) || (newContact.Mailing_Address3__c != NULL && newContact.Mailing_Address3__c.trim().length() > 0))
                {
                    if(newContact.Mailing_Address1__c != null && newContact.Mailing_Address1__c.length() > 0)   mailingStreet = newContact.Mailing_Address1__c;
                    if(newContact.Mailing_Address2__c != null && newContact.Mailing_Address2__c.length() > 0)   mailingStreet = mailingStreet + ', '+ newContact.Mailing_Address2__c;
                    if(newContact.Mailing_Address3__c != null && newContact.Mailing_Address3__c.length() > 0)   mailingStreet = mailingStreet + ', '+ newContact.Mailing_Address3__c;

                    newContact.MailingStreet = mailingStreet;
                     System.debug('   ----------Mailing_Address --END'+mailingStreet);
                }
            }
        }

        if (trigger.isUpdate) {
            for(Contact newContact: Trigger.New) {
                
                if(!newContact.MyVarian_Member__c && newContact.MyVarian_Member__c != Trigger.oldMap.get(newContact.Id).MyVarian_Member__c){
                    if(Trigger.oldMap.get(newContact.Id).OCSUGC_UserStatus__c == OCSUGC_Constants.CONTACT_STATUS_APPROVED)
                        newContact.OCSUGC_UserStatus__c = Label.OCSUGC_Disabled_by_Admin;
                }
                
                if(newContact.OCSUGC_UserStatus__c == OCSUGC_Constants.CONTACT_STATUS_DISABLE_SELF 
                    && newContact.OCSUGC_UserStatus__c != Trigger.oldMap.get(newContact.Id).OCSUGC_UserStatus__c && OCSDC_USER_STATUS.contains(newContact.OCSDC_UserStatus__c)){
                    newContact.OCSDC_UserStatus__c = Label.OCSDC_Disabled_by_Self;
                }
                
                if(newContact.OCSUGC_UserStatus__c == OCSUGC_Constants.CONTACT_STATUS_DISABLE_ADMIN 
                                    && newContact.OCSUGC_UserStatus__c != Trigger.oldMap.get(newContact.Id).OCSUGC_UserStatus__c  && OCSDC_USER_STATUS.contains(newContact.OCSDC_UserStatus__c)){
                    newContact.OCSDC_UserStatus__c = Label.OCSDC_Disabled_by_Admin;
                }
                
                if(newContact.OCSUGC_UserStatus__c == OCSUGC_Constants.OCSUGC_STATUS_REJECTED 
                                    && newContact.OCSUGC_UserStatus__c != Trigger.oldMap.get(newContact.Id).OCSUGC_UserStatus__c  && OCSDC_USER_STATUS.contains(newContact.OCSDC_UserStatus__c)){
                    newContact.OCSDC_UserStatus__c = Label.OCSDC_Disabled_by_Admin;
                }

              //  if (functionalRoleSet.contains(newContact.Functional_Role__c))
              //      newContact.Annual_Relationship_Survey__c=true;


                Contact oldContact = Trigger.oldMap.get(newContact.Id);
                if(oldContact.MailingStreet != newContact.MailingStreet)
                {
                    if(newContact.MailingStreet != null && newContact.MailingStreet.trim().length() > 0)
                    {
                        String mailingStreet = newContact.MailingStreet;
                        Integer firstComma = mailingStreet.indexOf(',');
                        System.debug('FIRST COMMA == '+firstComma);
                        if(firstComma > -1) {
                            newContact.Mailing_Address1__c = mailingStreet.substring(0,firstComma);
                            Integer secondComma = mailingStreet.indexOf(',', firstComma+1);
                            System.debug('SECOND COMMA == '+secondComma);
                            if(secondComma > -1)  {
                                newContact.Mailing_Address2__c = mailingStreet.substring(firstComma+1, secondComma);
                                if(mailingStreet.length() >  (secondComma + 1)) {
                                    System.debug('THIRD COMMA LENGTH == '+mailingStreet.length());
                                    newContact.Mailing_Address3__c = mailingStreet.substring(secondComma+1);
                                }
                            }
                            else if(mailingStreet.length() >  (firstComma + 1))  {
                                newContact.Mailing_Address2__c = mailingStreet.substring(firstComma+1);
                                newContact.Mailing_Address3__c = '';
                            }
                        }
                        else {
                            newContact.Mailing_Address1__c = mailingStreet.substring(0);
                            newContact.Mailing_Address2__c = '';
                            newContact.Mailing_Address3__c = '';
                        }
                    }
                    else  {
                        newContact.Mailing_Address1__c = '';
                        newContact.Mailing_Address2__c = '';
                        newContact.Mailing_Address3__c = '';
                    }
                }

                String mailingStreet = '';
                if((oldContact.Mailing_Address1__c != newContact.Mailing_Address1__c) || (oldContact.Mailing_Address2__c != newContact.Mailing_Address2__c) || (oldContact.Mailing_Address3__c != newContact.Mailing_Address3__c))
                {
                    if(newContact.Mailing_Address1__c != null && newContact.Mailing_Address1__c.length() > 0)  mailingStreet = newContact.Mailing_Address1__c;
                    if(newContact.Mailing_Address2__c != null && newContact.Mailing_Address2__c.length() > 0)  mailingStreet = mailingStreet + ', '+ newContact.Mailing_Address2__c;
                    if(newContact.Mailing_Address3__c != null && newContact.Mailing_Address3__c.length() > 0)  mailingStreet = mailingStreet + ', '+ newContact.Mailing_Address3__c;

                    newContact.MailingStreet = mailingStreet;
                }

                if(oldContact.email != newContact.email)
                {
                  if(!Test.isRunningTest()){
                    if(duplicateContactSearch.isContactDuplicateByEmail(newcontact.Account.Id, newContact.email))
                    {
                        newContact.email.addError('A contact with this email address already exists.');
                    }
                  }
                }
            }
        }
    }
}
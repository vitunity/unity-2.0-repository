/**
* @Trigger         : ContactTrigger
* @Description     : Contact trigger that containst all the trigger specific contecxt for the contact Object    
* @author          : Rajamohan Vakati
* @version         : 1.0
* @since           : 2017 -03 -2017
* @Test Class      : ContactTrigger_Test
* *****####################################                       Change Log                               ################################################################*****
*
*
*    Date                       version                   Author                        Story No#               Description               
*   2017 -03 -2017              initial(1.0)             Rajamohan Vakati               N/A                     Global Contact trigger for all the trigger operations .
*
*
*
*/
trigger ContactTrigger on Contact (before insert,
                                   after insert , 
                                   after update , 
                                   before update ,
                                   before delete,
                                   after delete,
                                   after undelete
                                   
                                  ) {
                                      if(AvoidRecursion.isFirstRun())
                                       new ContactTriggerHandler().run();   
                                  }
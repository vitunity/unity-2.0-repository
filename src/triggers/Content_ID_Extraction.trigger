trigger Content_ID_Extraction on Software_Installation__c (Before insert, Before update) {

Set<Id> ContentDocumentsIDSet= new Set <Id>();
List <ContentVersion> ContentIDList= new List <ContentVersion>();
set<boolean> Mutli_LanguagesSet= new set<boolean>();
     for(Software_Installation__c  si : trigger.new)
        {
        system.debug('test'+si.Content_Id__c);
            if(si.Content_Id__c!= Null && (si.Content_Id__c.length() == 15 || si.Content_Id__c.length() == 18) )
            {
                ContentDocumentsIDSet.add(si.Content_Id__c);
            }
        }    
          
For (ContentVersion c: [SELECT ContentDocumentId,Id,Mutli_Languages__c FROM ContentVersion where ContentDocumentId IN: ContentDocumentsIDSet])
{
ContentIDList.add(c); 
Mutli_LanguagesSet.add(c.Mutli_Languages__c);
}

for(Software_Installation__c  si : trigger.new){
    for(ContentVersion c: ContentIDList){
            si.ContentID__c= c.id;
            si.Multi_Languages__c=c.Mutli_Languages__c;
            }
            }

}
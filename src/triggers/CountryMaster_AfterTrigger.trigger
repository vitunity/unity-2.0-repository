/*************************************************************************\
    @ Author        : Sunil Bansal
    @ Date          : 23-May-2013
    @ Description   : This trigger is used to create Market Clearance Evidence records for a country for all Product Versions
    @ Last Modified By  :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
    @ Code Coverage is taken care in 'MarketClearanceReportTest' apex test class.
****************************************************************************/

trigger CountryMaster_AfterTrigger on Country__c (after insert, after Update) 
{
    List<Input_Clearance__c> lstInpClrnc= new List<Input_Clearance__c>();
    if(Trigger.IsInsert)
    {
        List<Input_Clearance__c> recordToInsert = new List<Input_Clearance__c>();
        
        Map<Id, Review_Products__c> reviewProductsMap = new Map<Id, Review_Products__c>([SELECT id,Name, Product_Profile_Name__c, Product_Model__c FROM Review_Products__c]);
        
       //  if no product version record, then no action 
        
       if(reviewProductsMap != null && reviewProductsMap.size() > 0)
        {
            for(Country__c cntry : Trigger.new)
            {
                for(id reviewProductId: reviewProductsMap.keySet())
                {
                    Input_Clearance__c mce = new Input_Clearance__c(); 
                    mce.Clearance_Entry_Form_Name__c = reviewProductsMap.get(reviewProductId).Product_Profile_Name__c+', '+cntry.Name;
                    mce.Clearance_Status__c = 'Blocked';
                    mce.Review_Product__c = reviewProductId;
                    mce.Product_Model__c = reviewProductsMap.get(reviewProductId).Product_Model__c;
                    mce.Country__c = cntry.Id;
                    recordToInsert.add(mce);
                }
            }
            insert recordToInsert;
        } 
    }
    
    if(Trigger.IsUpdate)
    {
        List<String> CountryId = new List<String>();
        for(Country__c cntry1 : Trigger.New)
        {
            if(Trigger.OldMap.get(cntry1.id).Name != Trigger.NewMap.get(cntry1.id).Name)
            {
                CountryId.add(cntry1.id);
            }
        }
        
        if(CountryId.size()>0)
        {
            
            lstInpClrnc = [select Product_Profile_Name__c, Clearance_Entry_Form_Name__c, Review_Product__c, Country__r.name, Country__c  from Input_Clearance__c where Country__c IN: CountryId];
        }
        if(lstInpClrnc.size()>0)
        {
            For(Input_Clearance__c Inc : lstInpClrnc)
            {   
                Inc.Clearance_Entry_Form_Name__c = Inc.Product_Profile_Name__c != Null ?Inc.Product_Profile_Name__c+', '+Trigger.newMap.get(Inc.Country__c).Name : Trigger.newMap.get(Inc.Country__c).Name;
            }
            Update lstInpClrnc;
        }
    }
}
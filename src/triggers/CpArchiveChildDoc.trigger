trigger CpArchiveChildDoc on ContentDocument (before update,before Delete) {
IF(Trigger.IsUpdate)
{
    List<String> LstContDOC=New List<String>();
    Boolean IsFlag;
for(ContentDocument CD_NewRec: Trigger.New)
  {
       ContentDocument CD_OldRec=Trigger.oldmap.get(CD_NewRec.id);
       
       IsFlag=CD_NewRec.IsArchived;
      IF(CD_OldRec.IsArchived!=CD_NewRec.IsArchived)
      {
       LstContDOC.add(CD_NewRec.LatestPublishedVersionId);
       
      }
  }
    System.debug('DebugLog1-->'+LstContDOC);
    
    List<ContentVersion> LstParentDocArchive=[Select Document_Type__c,Document_Version__c,TagCsv,Document_Number__c,id,Parent_Documentation__c FROM ContentVersion Where id IN:LstContDOC AND isLatest=True ALL ROWS];
    Set<String> SetofDocNo=new  Set<String>();
    for(ContentVersion CV_DocNum:LstParentDocArchive)
    {
    If(CV_DocNum.Document_Number__c!=Null)
    {
    SetofDocNo.add(CV_DocNum.Document_Number__c);
    }
    }
    System.debug('DebugLog3-->'+SetofDocNo);
    If(SetofDocNo.size()>0)
    {
      List<ContentVersion> LstChildDocArchive=[Select Document_Type__c,Document_Version__c,TagCsv,Document_Number__c,id,Parent_Documentation__c FROM ContentVersion Where Parent_Documentation__c IN:SetofDocNo AND isLatest=True and isDeleted=False ALL ROWS];
     List<ContentDocument> LstcontArchiveChild=new List<ContentDocument>();
     If(LstChildDocArchive.size()>0)
     {
     LstcontArchiveChild=[Select LatestPublishedVersionId,IsArchived from ContentDocument where LatestPublishedVersionId in:LstChildDocArchive ALL ROWS];
            System.debug('DebugLog5-->'+LstcontArchiveChild);
     }  
     
     
    for(ContentDocument CD:LstcontArchiveChild)
    {
    if(IsFlag==True)
    CD.IsArchived=TRUE;
    ELSE
    CD.IsArchived=FALSE;
    /* if(CD.IsArchived)
     CD.IsArchived=FALSE;
     ELSE
     CD.IsArchived=TRUE;*/
    }
    If(LstcontArchiveChild.size()>0)
    {
    
    Update LstcontArchiveChild;
    
    }
    }
    }
    
    IF(Trigger.IsDelete)
    {
     List<String> LstContDOC=New List<String>();
    for(ContentDocument CD_NewRec: Trigger.OLD)
  {
    LstContDOC.add(CD_NewRec.LatestPublishedVersionId);
  }
  List<ContentVersion> LstParentDocArchive=[Select Document_Type__c,Document_Version__c,TagCsv,Document_Number__c,id,Parent_Documentation__c FROM ContentVersion Where id IN:LstContDOC AND isLatest=True ALL ROWS];
    Set<String> SetofDocNo=new  Set<String>();
    for(ContentVersion CV_DocNum:LstParentDocArchive)
    {
    If(CV_DocNum.Document_Number__c!=Null)
    {
    SetofDocNo.add(CV_DocNum.Document_Number__c);
    }
    }
    System.debug('DebugLog3-->'+SetofDocNo);
    If(SetofDocNo.size()>0)
    {
      List<ContentVersion> LstChildDocArchive=[Select Document_Type__c,Document_Version__c,TagCsv,Document_Number__c,id,Parent_Documentation__c FROM ContentVersion Where Parent_Documentation__c IN:SetofDocNo AND isLatest=True AND ISDELETED=FALSE ALL ROWS];
     List<ContentDocument> LstcontArchiveChild=new List<ContentDocument>();
     If(LstChildDocArchive.size()>0)
     {
     LstcontArchiveChild=[Select LatestPublishedVersionId,IsArchived from ContentDocument where LatestPublishedVersionId in:LstChildDocArchive ALL ROWS];
            System.debug('DebugLog5-->'+LstcontArchiveChild);
     }    
     If(LstcontArchiveChild.size()>0)
    {
    
    Delete LstcontArchiveChild;
    
    }
    }
    }
}
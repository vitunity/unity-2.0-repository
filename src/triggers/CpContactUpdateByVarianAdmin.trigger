trigger CpContactUpdateByVarianAdmin on Contact (before update,after update) 
{
    if(system.Label.Contact_Triggers_Flag == 'True')
    {
        User usr= [Select Id, Name, ProfileId from User where Id=:UserInfo.getUserId()];
        Profile prf = [Select Id, Name from Profile Where Id=:Usr.ProfileId ];
        set<id> stContactId= new set<id> ();
        set<id> contactemailupdtae = new set<Id>();
        stContactId .addall(trigger.newmap.keyset());
        set<id> contactid = new set<id>();
        if(trigger.IsBefore)
        {
            for(Contact ContMyVarianField: Trigger.New)
            {
                Contact ContMyVarianField_OldRec=Trigger.oldmap.get(ContMyVarianField.id);
                if( ContMyVarianField.Inactive_Contact__c==true && ContMyVarianField_OldRec.Inactive_Contact__c!=ContMyVarianField.Inactive_Contact__c)
                {
                    ContMyVarianField.MyVarian_Member__c=false;
                }
                
                if(ContMyVarianField.MyVarian_Member__c == false)
                {
                    ContMyVarianField.LMS_Account_Status__c = '';
                }
            }
            if(prf.Name==Label.VMS_Service_User || prf.Name == Label.VMS_Marketing_User || prf.Name == 'System Administrator')
            {
                for (Contact con: Trigger.new) 
                {
                    if(trigger.oldmap.get(con.id).VMSPortalAdminToUpdate__c==true)
                    {
                        con.VMSPortalAdminToUpdate__c=false;
                    }
                    system.debug('Email changed-----' + trigger.oldmap.get(con.id).Email);
                    Contact oldEmail = Trigger.oldMap.get(con.Id);
                    

                }
               
            }
            for(contact con: trigger.new)
            {
               
                if(trigger.oldmap.get(con.id).Email != con.Email || trigger.oldmap.get(con.id).FirstName != con.FirstName || trigger.oldmap.get(con.id).LastName != con.LastName)
                {
                    system.debug('Email changed-----' + contactemailupdtae);
                    contactemailupdtae.add(con.id);
                     if(con.MyVarian_Member__c == true && con.OktaLogin__c != Null && con.OktaLogin__c != '')
                        {
                          con.OktaLogin__c = con.Email;
                        
                        }
                } 
            }
            system.debug('Email changed after if-----' + contactemailupdtae);
            if(contactemailupdtae.size() > 0 && !System.isBatch())
            {
                //CpUserActivationClass.useractivationmethod(contactemailupdtae);
                CpUserActivationClass.updateuser(contactemailupdtae);
            }
        }
         
        /* Logic to add value to MarketingKit Agreement field on User record from Contact record MarketingKit Agreement field for Custom Market Place */
        if(trigger.IsAfter)
        {
            if(Trigger.IsUpdate)
            {
               
                 Map<id,Set<String>> MapcontatHavingMarketKit= new Map<id,Set<String>>();
                 for(Contact Cont_NewRec: Trigger.New)
                  {
                    Contact Cont_OldRec=Trigger.oldmap.get(Cont_NewRec.id);
                    if(Cont_OldRec.MarketingKit_Agreement__c!=Cont_NewRec.MarketingKit_Agreement__c)
                    {
                       set<String> objMarketKitAgreeSet = new set<String>(); 
                       If (cont_NewRec.MarketingKit_Agreement__c!=null)
                       { 
                       objMarketKitAgreeSet.addall(Cont_NewRec.MarketingKit_Agreement__c.split(';'));
                       MapcontatHavingMarketKit.put(Cont_NewRec.id,objMarketKitAgreeSet);
                       }
                       else
                       {
                       MapcontatHavingMarketKit.put(Cont_NewRec.id,objMarketKitAgreeSet);
                       }
         
                    }
                  }
                  System.debug('Results--->'+MapcontatHavingMarketKit);
                   Map<id,String> MapContactidWithMarketKitAgree=new Map<id,String>();    
                   for(id ids:MapcontatHavingMarketKit.keyset())

                   {
                     String MarketingKit='';
                      for(String Str:MapcontatHavingMarketKit.get(ids))
                      {
                      if(Str!='')
                       MarketingKit=MarketingKit+Str+','; 
                      }
                      if(MarketingKit!='' )
                      {
                      MarketingKit=MarketingKit.Substring(0,MarketingKit.Length()-1); 
                      }
                      MapContactidWithMarketKitAgree.put(ids,MarketingKit);
         
                   }
                    System.debug('Output results----->'+MapContactidWithMarketKitAgree);
                   List<User> userids = new List<User>();
                  If(!Test.isRunningtest()){
                  userids= [Select id,Contactid,contact.accountid,UserType from user where contactid in: MapContactidWithMarketKitAgree.keyset()];
                  
                   for(User varuser:userids)
                  {
                   varuser.MarketingKit_Agreement__c=MapContactidWithMarketKitAgree.get(varuser.Contactid); 
                  }
                  }
                  if(userids.size()>0)

                  {
                  Update userids;
                  }
                  Set<id> Setofids=new Set<id>();
                for(Contact ContInactv: Trigger.New)
                {
                    Contact ContInactv_OldRec=Trigger.oldmap.get(ContInactv.id);
                    if(ContInactv.Inactive_Contact__c==true && ContInactv_OldRec.Inactive_Contact__c!=ContInactv.Inactive_Contact__c)
                    {
                        Setofids.add(ContInactv.id);
                  
                    }
                }
                Id profileId = [select Id from Profile where Name =: Label.VMS_Customer_Portal_User].Id;
                List<User> Lstusersids=new List<User>();
              
                Lstusersids=[Select id,Contactid from user where Contactid in:Setofids and IsPortalEnabled=true and ProfileId =: profileId and IsActive=true];
                Set<id> SetofContactid=new Set<id>();
                for(User cont: Lstusersids)
                {
                    SetofContactid.add(cont.Contactid);
                }

                if(SetofContactid.size()>0)
                {
                    if(!System.isFuture() && !System.isBatch())
                    {
                      ProductUtils.UpdateUserActiveFlag(SetofContactid);
                      ProductUtils.updateContactPartnerFlag(SetofContactid, false);
                      CpUserActivationClass.userdeactivationmethod(SetofContactid);
                    }
                }
            }
        }
    }
}
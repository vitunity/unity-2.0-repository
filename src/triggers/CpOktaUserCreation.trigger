trigger CpOktaUserCreation on User (after insert,after update) {
    //Rakesh.2/25/2016.Start
    //Add User to chatter group when Synchronize with BigMachines=TRUE
        UserBigMachine.AddRemoveUserToGroup(trigger.Newmap,Trigger.oldmap,'Pricebook Notes','VMS Marketing - User' );  
        if(Trigger.isInsert)UserDivisionChatter.AddUserToGroup(trigger.New,'K-CHAT');  
    //Rakesh.2/25/2016.End

 if(system.Label.User_Triggers_Flag == 'True'&& !(System.Isbatch()))
  {
        set<Id> stContactId = new set<Id>();
        set<Id> stContactIddeactivate = new set<Id>();
        set<Id> stLMSContactId = new set<Id>();
        set<Id> stLMSContactIdInactivate = new set<Id>();
        
        set<Id> stKPIContactId = new set<Id>();
        set<Id> stKPIContactIdInactivate = new set<Id>();
        
        //for creating user
        if(Trigger.isInsert){
          //set<Id> stContactId = new set<Id>();
          for(user u: trigger.new){
                if(u.contactid !=null)
                    {
                      stContactId.add(u.contactid);
                    }
              }
          CpUserActivationClass.useractivationmethod(stContactId);
          }
          //User update and user is made active
          
          if(trigger.IsUpdate){
            for(user u: trigger.new){
                if(u.contactid !=null)
                 {
                    
                    if(u.IsActive == True && Trigger.oldMap.get(u.id).LMS_Status__c != u.LMS_Status__c && u.LMS_Status__c == 'Active')
                        stLMSContactId.add(u.contactId);
                    if(u.IsActive == True && Trigger.oldMap.get(u.id).LMS_Status__c != u.LMS_Status__c && u.LMS_Status__c == 'Inactive')
                        stLMSContactIdInactivate.add(u.contactId);
                    
                    //Rakesh.4/5/2016.Start
                    if(u.IsActive == True && Trigger.oldMap.get(u.id).KPI_App_Access__c != u.KPI_App_Access__c && u.KPI_App_Access__c == true)
                        stKPIContactId.add(u.contactId);
                    if(u.IsActive == True && Trigger.oldMap.get(u.id).KPI_App_Access__c != u.KPI_App_Access__c && u.KPI_App_Access__c == false)
                        stKPIContactIdInactivate.add(u.contactId);
                    //Rakesh.4/5/2016.End
                    
                   if(Trigger.oldMap.get(u.id).IsActive != u.IsActive && u.IsActive == true){
                      stContactId.add(u.contactid);
                    }else if((Trigger.oldMap.get(u.id).IsActive != u.IsActive && u.IsActive == false) || u.IsPortalEnabled == false){
                      stContactIddeactivate.add(u.contactid); // set for deactivating the user
                    }
                 }
              }
              if(stLMSContactId.size() > 0)
              {
                 CpUserActivationClass.addUsersInLMSOktaGroup(stLMSContactId);
              }
              if(stLMSContactIdInactivate.size() > 0)
              {
                 CpUserActivationClass.removeUserFromLMSOktaGroup(stLMSContactIdInactivate);
              }
              
              //Rakesh.4/5/2016.Start
              if(stKPIContactId.size() > 0)
              {
                 CpUserActivationClass.addUsersInKPIOktaGroup(stKPIContactId);
              }
              if(stKPIContactIdInactivate.size() > 0)
              {
                 CpUserActivationClass.removeUserFromKPIOktaGroup(stKPIContactIdInactivate);
              }
              //Rakesh.4/5/2016.End
              
              if(stContactId.size() > 0)
              {
                 CpUserActivationClass.useractivationmethod(stContactId);
              }
              if (stContactIddeactivate.size() > 0)
              {
               if(!System.isFuture())
               CpUserActivationClass.userdeactivationmethod(stContactIddeactivate);
              }
            }
    }
    
    // Puneet Mishra, STSK0012350 : deleting usersessionIds rec is user if deactivated
    Set<Id> deactivatedUser = new Set<Id>();
    if(trigger.isUpdate && trigger.isAfter) {
        for(User u : trigger.new ){
            deactivatedUser.add(u.Id);
        }
        if(!deactivatedUser.isEmpty())
            DeactivateUserTriggerHandler.deleteDeactivatedUser(deactivatedUser);
    }
    
}
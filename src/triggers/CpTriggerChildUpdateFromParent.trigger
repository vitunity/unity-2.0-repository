trigger CpTriggerChildUpdateFromParent on ContentVersion (after update) {
if(Trigger.IsUpdate)
{
List<ContentVersion> LstCV=new List<ContentVersion>();
Set<String> LstParentDocNumber= New Set<String>();

List<ContentVersion> LstChildDoc= New List<ContentVersion>();

Map<String,ContentVersion> MapDocNum_CV=New Map<String,ContentVersion>();

  for(ContentVersion CV_NewRec: Trigger.New)
  {
  ContentVersion CV_OldRec=Trigger.oldmap.get(CV_NewRec.id);
  If(CV_NewRec.Mutli_Languages__c==True && (CV_OldRec.Document_Type__c!=CV_NewRec.Document_Type__c || CV_OldRec.TagCsv!= CV_NewRec.TagCsv || CV_OldRec.Document_Version__c!= CV_NewRec.Document_Version__c 
  || CV_OldRec.Document_Number__c!= CV_NewRec.Document_Number__c) )
  {
  if(CV_OldRec.Document_Number__c!=null)
  {
  LstParentDocNumber.add(CV_OldRec.Document_Number__c);
  MapDocNum_CV.put(CV_OldRec.Document_Number__c,CV_NewRec);}
  
  }
  
  }
    
  IF(LstParentDocNumber.SIZE()>0)
  {
  LstChildDoc=[Select Document_Type__c,Document_Version__c,TagCsv,Document_Number__c,id,Parent_Documentation__c FROM ContentVersion Where Parent_Documentation__c IN:LstParentDocNumber AND isLatest=True];
  
 For(ContentVersion CV_ChildRec:LstChildDoc)
 {
   
   ContentVersion ParentDoc= MapDocNum_CV.get(CV_ChildRec.Parent_Documentation__c);
   CV_ChildRec.Parent_Documentation__c=ParentDoc.Document_Number__c;
   CV_ChildRec.TagCsv=ParentDoc.TagCsv;
   CV_ChildRec.Document_Version__c=ParentDoc.Document_Version__c;
   CV_ChildRec.Document_Type__c=ParentDoc.Document_Type__c;
   
 }
 }
 if(LstChildDoc.size()>0)
 {
 Update LstChildDoc;
 

 }
 
}

}
trigger Cp_productversion on ContentVersion (before update) {

    set<Id> DocIds = new set<Id>();
    Map<Id,set<String>> docwokspacemap = new map<Id,set<String>>();
    Map<String,Set<String>> workspversionmap = new map<String,Set<String>>();
    set<String> workspacenameset = new set<String>();
    Id recotypid = [Select id from recordtype where name = 'Product Document'].id;
     system.debug('testrectyid-->' +recotypid );
     
    for(Contentversion c: trigger.new){
    system.debug('@@@@@@@' + c.FirstPublishLocationId);
    system.debug('@@@@@@' + c.ContentDocumentId);
   if(trigger.oldmap.get(c.id).Document_Version__c != c.Document_Version__c){
    if(c.RecordtypeId == recotypid )
     DocIds.add(c.ContentDocumentId);
   }
    }
    
    
    if(DocIds.size() > 0){
    for(ContentWorkspaceDoc cwd: [Select ContentWorkspace.Name,ContentDocumentId from ContentWorkspaceDoc where ContentDocumentId in: DocIds])
     {
      set<String> workspacenamemapset = new set<String>();
      if(docwokspacemap.containskey(cwd.ContentDocumentId)){
        workspacenamemapset = docwokspacemap.get(cwd.ContentDocumentId);
        workspacenamemapset.add(cwd.ContentWorkspace.Name);
        docwokspacemap.put(cwd.ContentDocumentId,workspacenamemapset);
       }else{
         workspacenamemapset.add(cwd.ContentWorkspace.Name);
         docwokspacemap.put(cwd.ContentDocumentId,workspacenamemapset);
       }
       workspacenameset.add(cwd.ContentWorkspace.Name);
     }
     system.debug('@@@@@@docwokspacemap' +docwokspacemap);
     system.debug('@@@@@@wokspaceset' +workspacenameset);
     String Workspacename;
     for(String str: workspacenameset)
     {
       if(Workspacename == null)
       {    
         Workspacename = str +'\'';
       }else{
        Workspacename = Workspacename + ',\'' + str + '\'';
       }
     }
    
     System.debug('TestWorkSpace'+Workspacename );
     String query = 'Select id,name,Product_Group__c from product2 where Product_Group__c INCLUDES (\'' + Workspacename + ')';
      System.debug('Query-->'+query );
     List<Product2> prdlist = database.query(query);
     map<Id,Set<String>> productandgroupmap = new map<Id,Set<String>>();
     map<Id,Set<String>> productandversionmap = new map<Id,Set<String>>();
     set<Id> productversionset = new Set<Id>();
     for(Product2 prd: prdlist)
     {
       Set<String> productgroup = new Set<String>();
       if(prd.Product_Group__c != null){
       productgroup.addall(prd.Product_Group__c.split(';'));
       productandgroupmap.put(prd.Id,productgroup);
       productversionset.add(prd.id);
       }
     }
     for(Product_Version__c prdvrs : [Select Version__c,id,Product__c from Product_Version__c where Product__c in : productversionset and Version__c != null])
     {
        Set<String> versions = new set<String>();
        if(productandversionmap.containsKey(prdvrs.Product__c))
        {
        System.debug('test'+productandversionmap.get(prdvrs.id));
       // if(productandversionmap.get(prdvrs.id)!=null)
          versions = productandversionmap.get(prdvrs.Product__c);
          versions.add(prdvrs.Version__c.trim());
          productandversionmap.put(prdvrs.Product__c,versions);
          System.debug('test1'+productandversionmap); 
        }else{
          versions.add(prdvrs.Version__c.trim());
          productandversionmap.put(prdvrs.Product__c,versions);
         }
      }
     for(Id d: productandgroupmap.keyset())
     {
      for(String str: productandgroupmap.get(d))
      {
      if(productandversionmap.get(d) != null){
          Set<String> versionsworkspc = new set<String>();
          if(workspversionmap.containskey(str))
          {
                versionsworkspc = workspversionmap.get(str);
                versionsworkspc.addall(productandversionmap.get(d));
                workspversionmap.put(str,versionsworkspc);
              }else{
                versionsworkspc.addall(productandversionmap.get(d));
                workspversionmap.put(str,versionsworkspc);
              }
          }
      }
     }
     system.debug('@@@@@@@@@Map' + workspversionmap);
     system.debug('@@@@@@@@@Map' + productandversionmap);
     system.debug('@@@@@@@@@Map' + productandgroupmap);
     for(Contentversion cv: trigger.New)
     {
     if(cv.Document_Version__c != null && cv.RecordtypeId == recotypid)
      {
       for(String s : cv.Document_Version__c.split(','))
       {
        Set<String> validversion = new set<String>();
        for(String libname: docwokspacemap.get(cv.ContentDocumentId))
         {
          if(workspversionmap.get(libname) != null){
           validversion.addall(workspversionmap.get(libname));
          }
         }
         if(validversion != null){
         if(!validversion.contains(s.trim()))
         {
          cv.addError('Version(s) may not be: a) valid for this product or b) formatted correctly. Versions must be entered using xx.yy format such as 8.5 or 11.0. When entering multiple versions, they must be comma separated: 8.0, 8.2, 8.5.');
          break;
         }
         }else {
          cv.addError('Version(s) may not be: a) valid for this product or b) formatted correctly. Versions must be entered using xx.yy format such as 8.5 or 11.0. When entering multiple versions, they must be comma separated: 8.0, 8.2, 8.5.');
          break;
         }
        
       }
      }

     }
    }
    
     
}
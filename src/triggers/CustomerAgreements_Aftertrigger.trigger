/***************************************************************************
Author: Chandramouli Vegi
Created Date: 15-Sep-2017
Project/Story/Inc/Task : Keystone Project : Sep 2017 Release
Description: 
This is a after trigger on Customer Agreements object, which calculates the Account Remote Service Acceptance field value
based on the Customer Agreement type = "Remote Service".

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
*************************************************************************************/
trigger CustomerAgreements_Aftertrigger on Customer_Agreements__c (after insert,after update) 
{
    set<id> accountids = new set<id>();
    Map<id,Account> acct2update = new Map<Id,Account>();
    for (Customer_Agreements__c ca: trigger.new)
    {
        if (ca.Affected_Account__c != null)
        {
            accountids.add(ca.Affected_Account__c);
        }
    }
    if (accountids.size() > 0)
    {
        for (Account Acc : [select id, acc_RemoteServiceAcceptance__c,(select    Affected_Account__c, Agreement_Type__c,Agreement_Response__c from 
                                            Customer_Agreements__r where Agreement_Type__c = 'Remote Service'
                                            order by lastmodifieddate desc limit 1) from Account where id in :accountids])
        {
            Account a = new Account();
            a.id = Acc.id;
            for (Customer_Agreements__c ca : Acc.Customer_Agreements__r)
            if (ca.Agreement_Response__c == 'Yes')
            {
                a.acc_RemoteServiceAcceptance__c = true;
            }
            else
            {
                a.acc_RemoteServiceAcceptance__c = false;
            }
            acct2update.put(acc.id,a);
        }
    }
    if ( acct2update.size() > 0)
    {
        update acct2update.values();
    }
}
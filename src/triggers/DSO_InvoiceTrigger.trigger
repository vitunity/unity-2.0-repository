trigger DSO_InvoiceTrigger on Invoice__c (before insert, before update, after insert, after update) 
{
    
   TriggerDispatcher.TriggerConfig config = new TriggerDispatcher.TriggerConfig(InvoiceTriggerHandler.class, new Set<TriggerDispatcher.TriggerContext>{TriggerDispatcher.TriggerContext.BEFORE_INSERT, TriggerDispatcher.TriggerContext.BEFORE_UPDATE, TriggerDispatcher.TriggerContext.AFTER_INSERT, TriggerDispatcher.TriggerContext.AFTER_UPDATE}, false, false); //
   TriggerDispatcher.execute(config);  
   
   /*	
   if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate))
   {
        InvoiceTriggerHandler invHandler = new InvoiceTriggerHandler();
        invHandler.beforeInsert(Trigger.New);
   }

   if(Trigger.isBefore && Trigger.isUpdate) 
    {
        //SR_Varianinvoice_Triggerhandler varInvoice = new SR_Varianinvoice_Triggerhandler();
        //varInvoice.update_interface_status(Trigger.new, Trigger.oldMap);
    } 

    if(Trigger.isAfter)
    {
        InvoiceTriggerHandler invHandler = new InvoiceTriggerHandler();
        if(Trigger.isInsert)
        {
            invHandler.afterInsert(Trigger.New, Trigger.NewMap);
        }
    }*/

}
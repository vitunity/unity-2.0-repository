trigger DeveloperStripeInfoTrigger on vMarket_Developer_Stripe_Info__c (after insert, after update) {
    
    List<Id> devList = new List<Id>();
    
    for(vMarket_Developer_Stripe_Info__c d : trigger.new) {
        // populate the list which are active, accepted Terms of Use and Developer Status is Approved
        if(d.isActive__c && d.vMarket_Accept_Terms_of_Use__c && d.Developer_Request_Status__c == Label.vMarket_Approved_Status) {
            devList.add(d.Stripe_User__c);
        }
    }
    
    if(!devList.isEmpty()) {
        List<User> uList = new List<User>();
        uList = [SELECT id, Name, vMarket_User_Role__c, isActive FROM User WHERE Id IN: devList AND isActive =: true];
        for(User u : uList) {
            u.vMarket_User_Role__c = Label.vMarket_Developer;
        }
        
        update uList;
    }
     
}
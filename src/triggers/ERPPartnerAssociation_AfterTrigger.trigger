/*****************************************
Created By    :    Nikita Gupta
Created On    :    8/5/2014
Reason        :    US3124, to delete all existing duplicate ERP Partner Association records

*****************************************/
trigger ERPPartnerAssociation_AfterTrigger on ERP_Partner_Association__c (after insert, after update) 
{
    if(trigger.isInsert)
    {
        Set<ID> setCustAccount = new Set<ID>();
        Set<String> setCustNumber = new Set<String>();
        Set<String> setSalesOrg = new Set<String>();
        Set<String> setPartFunction = new Set<String>();
        Set<ID> setERPpartner = new Set<ID>();
        Set<ID> setERPpartAssID = new Set<ID>();
        Map<String, List<ERP_Partner_Association__c>> mapDeleteERPPartAssociation = new Map<String, List<ERP_Partner_Association__c>>();
        //Set<ERP_Partner_Association__c> setDeleteERPPartAssociation = new  Set<ERP_Partner_Association__c>();
        List<ERP_Partner_Association__c> listDeleteERPPartAssociation = new  List<ERP_Partner_Association__c>();
        
        for(ERP_Partner_Association__c varERP : trigger.new)
        {
            setERPpartAssID.add(varERP.ID);
            /*if(varERP.Customer_Account__c != null )
            {
                setCustAccount.add(varERP.Customer_Account__c);
            }*/
            if(varERP.ERP_Customer_Number__c != null )
            {
                setCustNumber.add(varERP.ERP_Customer_Number__c);
            }
            if(varERP.Sales_Org__c != null)
            {
                setSalesOrg.add(varERP.Sales_Org__c);
            }
            if(varERP.Partner_Function__c != null)
            {
                setPartFunction.add(varERP.Partner_Function__c);
            }
            if(varERP.ERP_Partner__c != null)
            {
                setERPpartner.add(varERP.ERP_Partner__c);
            }
        }
        
        //if(setCustAccount.size() > 0 && setSalesOrg.size() > 0 && setPartFunction.size() > 0 && setERPpartner.size() > 0)
        if(setCustNumber.size() > 0 && setSalesOrg.size() > 0 && setPartFunction.size() > 0 && setERPpartner.size() > 0)
        {
            List<ERP_Partner_Association__c> listPartnerAssociation = [Select ID, Customer_Account__c, ERP_Customer_Number__c, Sales_Org__c, Partner_Function__c, ERP_Partner__c from ERP_Partner_Association__c where ERP_Customer_Number__c in :setCustNumber AND Sales_Org__c in :setSalesOrg AND Partner_Function__c in :setPartFunction AND ERP_Partner__c in :setERPpartner AND ID NOT in :setERPpartAssID];
            
            if(listPartnerAssociation.size() > 0)
            {
                for(ERP_Partner_Association__c varERP : listPartnerAssociation)
                {
                    if (varERP.ERP_Customer_Number__c != null && varERP.Sales_Org__c != null && varERP.Partner_Function__c != null &&  varERP.ERP_Partner__c != null) //added by Nikita as per the Defect raised in email
                    {
                        list<ERP_Partner_Association__c> tempListPartAssociation = new list<ERP_Partner_Association__c>();
                        
                        string str = varERP.ERP_Customer_Number__c + varERP.Sales_Org__c + varERP.Partner_Function__c + varERP.ERP_Partner__c;
                        
                        if(mapDeleteERPPartAssociation.containskey(str))
                        {
                            tempListPartAssociation = mapDeleteERPPartAssociation.get(str);
                        }
                        
                        tempListPartAssociation.add(varERP);
                        mapDeleteERPPartAssociation.put(str, tempListPartAssociation);
                    }   
                }
            }
            
            if(mapDeleteERPPartAssociation.size() > 0)
            {
                Set<String> alreadyAdded = new Set<String>();
                for(ERP_Partner_Association__c varERP : trigger.new)
                {
                    String str = varERP.ERP_Customer_Number__c + varERP.Sales_Org__c + varERP.Partner_Function__c + varERP.ERP_Partner__c;
                    if(mapDeleteERPPartAssociation.containskey(str) && !alreadyAdded.contains(str))
                    {
                        listDeleteERPPartAssociation = mapDeleteERPPartAssociation.get(str);
                        alreadyAdded.add(str);
                    }
                }
                
                if(listDeleteERPPartAssociation.size() > 0)
                {
                    //listDeleteERPPartAssociation.addAll(setDeleteERPPartAssociation);
                    delete listDeleteERPPartAssociation;
                }
            }
        }
    }
}
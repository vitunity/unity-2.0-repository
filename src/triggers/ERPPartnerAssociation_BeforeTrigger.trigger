/*************************************************************************\
      @ Author          : Megha Arora
      @ Date            : 26-Feb-2014
      @ Description     : This Trigger will be used to update ERP Customer Number and ERP Partner Number.
      @ Last Modified By      :     Nikita Gupta
      @ Last Modified On      :     8/4/2014
      @ Last Modified Reason  :     US3124, to update Account based on other ERP Partner Function
****************************************************************************/

trigger ERPPartnerAssociation_BeforeTrigger on ERP_Partner_Association__c (before insert, before update) {

    //get the AccountId's and store it in a set.
    
    set<String> ERPNumberSet = new set<String>();
    set<String> ERPNumberSet2 = new set<String>();
    List<Account> accountList = new List<Account>();
    map<String,ID> AccountMap= new map<String,ID>();
    List<ERP_Partner__c> PartnerList = new List<ERP_Partner__c>();
    map<String,ID> PartnerMap= new map<String,ID>();
    Map<String, ID> mapPrtnrNumer_ERPPrtnrAssociation = new Map<String, ID>();
    
    for(ERP_Partner_Association__c ERPartner : trigger.new)
    {
        if(ERPartner.ERP_Customer_Number__c!= null)
        {
            ERPNumberSet.add(ERPartner.ERP_Customer_Number__c);
        }
        if(ERPartner.ERP_Partner_Number__c!= null)
        {
            ERPNumberSet2.add(ERPartner.ERP_Partner_Number__c);
        }
    }
    
    if(ERPNumberSet.size() > 0) //null check added by Nikita, 8/4/2014
    {
        accountList = [SELECT id, AccountNumber from Account where AccountNumber IN: ERPNumberSet]; 
        
        //Code by Nikita, 8/4/2014, US3124
        List<ERP_Partner_Association__c> listERPPartAssociation = [Select id, Customer_Account__c, ERP_Partner_Number__c from ERP_Partner_Association__c where ERP_Partner_Number__c in :ERPNumberSet and Partner_Function__c = :'Z1=Site Partner']; // to query ERP Partner Association records where ERP Partner ERP Partner Number = ERP Customer Number of the record the trigger is running on.
        
        if(listERPPartAssociation.size() > 0)
        {
            for(ERP_Partner_Association__c varPA : listERPPartAssociation)
            {
                mapPrtnrNumer_ERPPrtnrAssociation.put(varPA.ERP_Partner_Number__c, varPA.Customer_Account__c);
            }   
        }
        //Code by Nikita ends 8/4/2014
        
        if(accountList.size() > 0) //null check added by Nikita, 8/4/2014
        {
            for(Account acc : accountList)
            {
                AccountMap.put(acc.AccountNumber,acc.id);
            }
        }       
    }
 
    if(ERPNumberSet2.size() > 0) //null check added by Nikita, 8/4/2014
    {
        PartnerList = [SELECT id, Partner_Number__c from ERP_Partner__c where Partner_Number__c IN: ERPNumberSet2 ]; 
        
        if(PartnerList.size() > 0)  //null check added by Nikita, 8/4/2014
        {       
            for(ERP_Partner__c part : PartnerList)
            {
                PartnerMap.put(part.Partner_Number__c,part.id);
            }
        }   
    }   
    
    for(ERP_Partner_Association__c Erpartner: trigger.new)
    {
        if(AccountMap.containsKey(Erpartner.ERP_Customer_Number__c))
        {
            Erpartner.Customer_Account__c= AccountMap.get(Erpartner.ERP_Customer_Number__c);
        } 
        else
        {
            if(mapPrtnrNumer_ERPPrtnrAssociation.containsKey(Erpartner.ERP_Customer_Number__c))
            {
                Erpartner.Customer_Account__c= mapPrtnrNumer_ERPPrtnrAssociation.get(Erpartner.ERP_Customer_Number__c);
            }
        }
        if(PartnerMap.containsKey(Erpartner.ERP_Partner_Number__c))
        {
            Erpartner.ERP_Partner__c= PartnerMap.get(Erpartner.ERP_Partner_Number__c);
        } 
    }

}
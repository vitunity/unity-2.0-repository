/*
Test Class - Test Class - SR_SuccessEmailRestControllerTest
*/
trigger EmailNotificationOnQuote on BigMachines__Quote__c (after update) 
{
      
    if(RecursiveQuoteTriggerController.isSuccessEmailSent == false && trigger.new.size() == 1)
    {      

        //Addendum Logic//
        map<Id,set<string>> mapBMQProducts = new map<Id,set<string>>();
        set<string> setBQMPartNumbers = new set<string>();
        for(BigMachines__Quote_Product__c p: [select Id,Name,BigMachines__Quote__c from BigMachines__Quote_Product__c where BigMachines__Quote__c IN: trigger.new]){
            set<string> setBMQProdNames = new set<string>();
            setBMQProdNames.add((p.Name).tolowerCase());
            if(mapBMQProducts.containsKey(p.BigMachines__Quote__c)){
                setBMQProdNames.addAll(mapBMQProducts.get(p.BigMachines__Quote__c));
            }
            mapBMQProducts.put(p.BigMachines__Quote__c,setBMQProdNames);
            setBQMPartNumbers.add((p.Name).tolowerCase());
        }

        set<string> setPartNumbers = new set<string>();
        for(Addendum_Data__c p: [select Id,Exclude_from_EPOT__c,Product__r.BigMachines__Part_Number__c from Addendum_Data__c where Product__r.BigMachines__Part_Number__c IN: setBQMPartNumbers]){
            setPartNumbers.add((p.Product__r.BigMachines__Part_Number__c).tolowerCase());
        }
        //End Addendum Logic//
        
        for(BigMachines__Quote__c BQ : Trigger.new)
        {   
    
            boolean isChildObj = false;
            if(mapBMQProducts.containsKey(BQ.Id)){
                for(string n : mapBMQProducts.get(BQ.Id)){
                    if(setPartNumbers.contains(n))
                    isChildObj = true;
                }
            }
               
            System.debug('Booking Message :: '+BQ.Booking_Message__c); 
            System.debug('Interface sTATUS :: '+BQ.Interface_Status__c);   
                
            BigMachines__Quote__c BQ_old = Trigger.oldMap.get(BQ.Id);
            if(BQ_old.Quote_Status__c == null && BQ_old.Quote_Status__c == '')
            {
                BQ.Initial_Order_Status__c = BQ.Quote_Status__c;
            }
            system.debug('==BQ_old.Booking_Message__c=='+BQ_old.Booking_Message__c);
            system.debug('==BQ.Booking_Message__c=='+BQ.Booking_Message__c);
            system.debug('==BQ_old.SAP_Prebooked_Sales__c=='+BQ_old.SAP_Prebooked_Sales__c);
            system.debug('==BQ.SAP_Prebooked_Sales__c=='+BQ.SAP_Prebooked_Sales__c);
            // Send pre-booked sales SUCCESS email
            if(BQ_old.Booking_Message__c !=  BQ.Booking_Message__c && BQ_old.SAP_Prebooked_Sales__c != BQ.SAP_Prebooked_Sales__c)
            {
                if(BQ.Order_Type__c == 'Sales' && BQ.Interface_Status__c == 'Processed' && BQ.SAP_Prebooked_Sales__c != null )  
                {
                    RecursiveQuoteTriggerController.isSuccessEmailSent = true;
                    
                    string TemplateName = 'Quote SAP PreBookedSales - Success VF';//default
                    if(isChildObj)TemplateName = 'Addendum Quote SAP PreBookedSales - Success VF';//Addendum
                    
                    SR_OrderEmail orderEmail = new SR_OrderEmail(Trigger.new[0].Id, TemplateName);
                    System.enqueueJob(orderEmail);
                }  
            }
            
            
            
            // Send pre-booked service SUCCESS email
            if(BQ_old.Message_Service__c !=  BQ.Message_Service__c && BQ_old.SAP_Prebooked_Service__c != BQ.SAP_Prebooked_Service__c)
            {
                if(BQ.Order_Type__c == 'Services' && BQ.Interface_Status__c == 'Processed' && BQ.SAP_Prebooked_Service__c != null )  
                {
                    RecursiveQuoteTriggerController.isSuccessEmailSent = true;
                    //SR_SuccessEmailController.sendVF(Trigger.new[0].Id,'Quote SAP PreBookedService - Success VF',UserInfo.getSessionId());
                    SR_OrderEmail orderEmail = new SR_OrderEmail(Trigger.new[0].Id, 'Quote SAP PreBookedService - Success VF');
                    System.enqueueJob(orderEmail);
                }  
            }
            
            // Send pre-booked Combined SUCCESS email
            if((BQ_old.Booking_Message__c !=  BQ.Booking_Message__c && BQ_old.SAP_Prebooked_Sales__c != BQ.SAP_Prebooked_Sales__c && BQ.Message_Service__c!=null)
                || (BQ_old.Message_Service__c !=  BQ.Message_Service__c && BQ_old.SAP_Prebooked_Service__c != BQ.SAP_Prebooked_Service__c && BQ.Booking_Message__c!=null))
            {
                if(BQ.Order_Type__c == 'Combined' && BQ.Interface_Status__c == 'Processed' && BQ.SAP_Prebooked_Sales__c != null && !String.isBlank(BQ.Booking_Message__c) && BQ.SAP_Prebooked_Service__c != null && !String.isBlank(BQ.Message_Service__c))  
                {
                    System.debug('----Combined Quote--');
                    RecursiveQuoteTriggerController.isSuccessEmailSent = true;
                    string TemplateName = 'Quote SAP PreBookedCombined - Success VF';//default
                    if(isChildObj)TemplateName = 'Addendum_Quote SAP PreBookedCombined - Success VF';//Addendum
                    //SR_SuccessEmailController.sendVF(Trigger.new[0].Id,'Quote SAP PreBookedCombined - Success VF',UserInfo.getSessionId());
                    SR_OrderEmail orderEmail = new SR_OrderEmail(Trigger.new[0].Id,TemplateName);
                    System.enqueueJob(orderEmail);
                }  
            }     
            // Send booked sales SUCCESS email
            if(BQ_old.SAP_Booked_Sales__c != BQ.SAP_Booked_Sales__c)
            {
                if(BQ.Interface_Status__c == 'Processed' && BQ.SAP_Booked_Sales__c != null && !String.isBlank(BQ.Booking_Message__c))   
                {
                    System.debug('----Sales--');
                    RecursiveQuoteTriggerController.isSuccessEmailSent = true;
                    //SR_SuccessEmailController.sendVF(Trigger.new[0].Id,'Quote SAP BookedSales - Success VF',UserInfo.getSessionId());
                    SR_OrderEmail orderEmail = new SR_OrderEmail(Trigger.new[0].Id, 'Quote SAP BookedSales - Success VF');
                    System.enqueueJob(orderEmail);
                } 
            }
            
            /* Moved to time based workflow email will get trigged in 2 hours once contracts 
            // Send booked service SUCCESS email
            if(BQ_old.SAP_Booked_Service__c != BQ.SAP_Booked_Service__c)
            {
                if(BQ.Interface_Status__c == 'Processed' && BQ.SAP_Booked_Service__c != null && !String.isBlank(BQ.Message_Service__c))   
                {
                    System.debug('----Service--');
                    RecursiveQuoteTriggerController.isSuccessEmailSent = true;                    
                    //SR_SuccessEmailController.sendVF(Trigger.new[0].Id,'Quote SAP BookedService - Success VF',UserInfo.getSessionId());
                    SR_OrderEmail orderEmail = new SR_OrderEmail(Trigger.new[0].Id, 'Quote SAP BookedService - Success VF');
                    System.enqueueJob(orderEmail);
                } 
            }
            */
            
            // Send booked service SUCCESS email
            if(BQ_old.SAP_Booked_Service_Email__c == false && BQ.SAP_Booked_Service_Email__c  == true)
            {
                if(BQ.Interface_Status__c == 'Processed' && BQ.SAP_Booked_Service__c != null && !String.isBlank(BQ.Message_Service__c))   
                {
                    System.debug('----Service--');
                    RecursiveQuoteTriggerController.isSuccessEmailSent = true;                    
                    //SR_SuccessEmailController.sendVF(Trigger.new[0].Id,'Quote SAP BookedService - Success VF',UserInfo.getSessionId());
                    SR_OrderEmail orderEmail = new SR_OrderEmail(Trigger.new[0].Id, 'Quote SAP BookedService - Success VF');
                    System.enqueueJob(orderEmail);
                } 
            }
            
            // Send pre-booked sales ERROR email            
            if(BQ_old.Booking_Message__c !=  BQ.Booking_Message__c )
            {
                if(BQ.Order_Type__c == 'Sales' && BQ.Interface_Status__c == 'Processed' && !String.isBlank(BQ.Booking_Message__c) && BQ.SAP_Prebooked_Sales__c == null)
                {   
                    RecursiveQuoteTriggerController.isSuccessEmailSent = true;
                    string TemplateName = 'Quote SAP PreBookedSales - ERROR VF';//default
                    if(isChildObj)TemplateName = 'Addendum Quote SAP PreBookedSales - ERROR VF';//Addendum
                    AcknowledgementEmailController emailCntrl = new AcknowledgementEmailController();
                    emailCntrl.SendErrorEmail(Trigger.new[0],TemplateName);
                }
            }
            // Send pre-booked service ERROR email
            if(BQ_old.Message_Service__c !=  BQ.Message_Service__c)    
            {
                if(BQ.Order_Type__c == 'Services' && BQ.Interface_Status__c == 'Processed' && !String.isBlank(BQ.Message_Service__c) && BQ.SAP_Prebooked_Service__c == null)
                {
                    RecursiveQuoteTriggerController.isSuccessEmailSent = true;
                    AcknowledgementEmailController emailCntrl = new AcknowledgementEmailController();
                    emailCntrl.SendErrorEmail(Trigger.new[0], 'Quote SAP PreBookedService - Error VF');
                } 
            }
               
            // Send pre-booked Combined ERROR email
            System.debug('----combined Error outside--');
            if((BQ_old.Booking_Message__c !=  BQ.Booking_Message__c ) || (BQ_old.Message_Service__c !=  BQ.Message_Service__c ))
            {
                System.debug('----combined Error--');
                if((BQ.Order_Type__c == 'Combined' && BQ.Interface_Status__c == 'Processed' 
                    && !String.isBlank(BQ.Booking_Message__c) && !String.isBlank(BQ.Message_Service__c)) && 
                    (BQ.SAP_Prebooked_Sales__c == null || BQ.SAP_Prebooked_Service__c == null))
                {
                    System.debug('----combined Error processed--');
                    RecursiveQuoteTriggerController.isSuccessEmailSent = true;
                    string TemplateName = 'Quote SAP PreBookedCombined - ERROR VF';//default
                    if(isChildObj)TemplateName = 'Addendum_Quote SAP PreBookedCombined - ERROR VF';//Addendum
                    AcknowledgementEmailController emailCntrl = new AcknowledgementEmailController();
                    emailCntrl.SendErrorEmail(Trigger.new[0],TemplateName);
                } 
            }
            }
        }
        
 }
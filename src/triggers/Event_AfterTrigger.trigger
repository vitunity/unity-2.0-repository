/*******************************************************************************************************************************
@Author   :       Jyoti Gaur
@Date      :       30/09/2013
@Description   :  This trigger is for multiple technician assignements for Work Orders and for inserting servicemax event record as well as technician assignement record.
@Last Modified by: Priti Jaiswal
@Last Modified reason:  To update fields on Event (US4094)
@Last Modified Date : 28-Feb-2014
@Last Modified by:  Shubham Jaiswal    
@Last Modified reason: US4044 : added method InstallationWorkOrderAssignmentEmail
@Last Modified Date : 21/7/2014
*******************************************************************************************************************/

trigger Event_AfterTrigger on Event(after insert,after update)
{
    SR_Class_Event objClassEvent = new SR_Class_Event();    //initiate class

    if(trigger.isinsert)
    {
        /** 1C Sunil - 23 Jan 15   - INSTALLATION IS IN 1C
        List<Event> ListEvent = new List<Event>();
        Set<Id> SetWOId = new Set<Id>();

        Id WOInstallationId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        Schema.DescribeSObjectResult WO = SVMXC__Service_Order__c.sObjectType.getDescribe();

        for(Event newevent : trigger.new)
        {
            if(newevent!= null && newevent.WhatId != null && WO != null && String.valueOf(newevent.WhatId).startsWith(WO.getKeyPrefix()))
            {
                SetWOId.add(newevent.WhatId);
                ListEvent.add(newevent);
            }
        }
        if(SetWOId.size() > 0 && ListEvent.size() > 0)
        {
            List<Event> listInstallationWorkOrderAssignment = new List<Event>();    // US4044 Shubham 21/7/2014

            Map<id,SVMXC__Service_Order__c> MapWOId2WO = new Map<id,SVMXC__Service_Order__c>([SELECT id, recordtypeid From SVMXC__Service_Order__c
                                                                                                where id IN : SetWOId]);
            for(Event newevent : ListEvent)
            {
                if(MapWOId2WO.containsKey(newevent.WhatId) && MapWOId2WO.get(newevent.WhatId).recordtypeid == WOInstallationId)
                {
                    listInstallationWorkOrderAssignment.add(newevent);
                }
            }
             if(listInstallationWorkOrderAssignment.size() > 0)
            {
                system.debug('@@@@@@@@@@listInstallationWorkOrderAssignment' + listInstallationWorkOrderAssignment);
                objClassEvent.InstallationWorkOrderAssignmentEmail(listInstallationWorkOrderAssignment); 
            }
        } // US4044 Shubham 21/7/2014 ends
        1C Sunil - 23 Jan 15  **/
    }
     
    if(trigger.isupdate)
    {
       // Training : Update WorkOder Status to Cancelled : mohit sharma 6-may,2014 US4082
        objClassEvent.updateEventWorkOrder(Trigger.new,trigger.newmap,trigger.oldmap);
    }
    
    if(trigger.isInsert) {
        objClassEvent.createTimeEntry(Trigger.new,trigger.oldmap);
    }
 }
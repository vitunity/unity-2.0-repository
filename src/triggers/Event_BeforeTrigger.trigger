/*******************************************************************************************************************************
@Author   :       Jyoti Gaur
@Date      :       30/09/2013
@Description   :  This trigger is for multiple technician assignements for Work Orders and for inserting servicemax event record as well as technician assignement record.
@Last Modified by: Megha Arora
@Last Modified reason:  For refactoring the code
@Last Modified by   :   Nikita Gupta
@Last Modified on   :   6/27/2014
@Last Modified reason:  US2066, to update event recordtype with salesforce
@Last Modified by   :   Nikita Gupta
@Last Modified on   :   7/8/2014
@Last Modified reason:  merged before trigger code in class
*******************************************************************************************************************/

trigger Event_BeforeTrigger on Event(before delete,before update,before insert)
{
    if(trigger.isinsert || trigger.isupdate)
    {
        SR_Class_Event objEventClass = new SR_Class_Event();
        //priti, US4094 - To update fields on event
        objEventClass.updateFieldsOnEvent(trigger.new); //method moved from class SR_TechnicianAssignmentclass to SR_Class_Event
    }
}
//Part of the ServiceMax Connected Field Service Lighthouse Program
//Code Written by Amit Jain
//Purpose is to break apart the code populated into the Service Request Problem Description to allow for a break down into individual fields


 trigger ExtractPayloadFromServiceRequest on SVMXC__Service_Request__c (before insert, before update) {
    ExtractPayloadFromServiceRequest extPayload = new ExtractPayloadFromServiceRequest (trigger.new);
    extPayload.updateFieldsForPayload();
}
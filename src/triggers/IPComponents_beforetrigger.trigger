/***************************************************************************
Author: Chandramouli Vegi
Created Date: 15-Sep-2017
Project/Story/Inc/Task : Keystone Project : Sep 2017 Release
Description: 
This is a before trigger on IP Components object, which calculates the Functional Location lookup based on Functional location text field.

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
*************************************************************************************/
trigger IPComponents_beforetrigger on IP_Components__c (before insert,before update) 
{
    map<string,Id> ERPfunclocation2loc = new map<String,Id>();
    set<string> erpfuncloc = new set<string>();
    if (trigger.isInsert || trigger.isUpdate)
    {
        for (IP_Components__c ipc: trigger.new)
        {
            if (ipc.ipc_Functional_Location__c != null && !string.isBlank(ipc.ipc_Functional_Location__c))
            {
                erpfuncloc.add(ipc.ipc_Functional_Location__c);
            }
        }
        if (erpfuncloc.size() > 0)
        {
            for(SVMXC__Site__c site: [select Id,name, ERP_Functional_Location__c from SVMXC__Site__c where ERP_Functional_Location__c in :erpfuncloc])
            {
                ERPfunclocation2loc.put(site.ERP_Functional_Location__c, site.Id);
            }
        }
        for (IP_Components__c ipc: trigger.new)
        {
            if (ipc.ipc_Functional_Location__c != null && !string.isBlank(ipc.ipc_Functional_Location__c) && ERPfunclocation2loc.size() > 0)
            {
                if (ERPfunclocation2loc.containskey(ipc.ipc_Functional_Location__c))
                {
                    ipc.ipc_Functional_Locationref__c = ERPfunclocation2loc.get(ipc.ipc_Functional_Location__c);
                }
            }
        }
    }
}
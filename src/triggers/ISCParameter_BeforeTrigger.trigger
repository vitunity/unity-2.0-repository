trigger ISCParameter_BeforeTrigger on ISC_Parameters__c (before insert, before update)
{
    //Set<String> setOfParaName = new Set<String>();    //commented by Parul, moved to class
    //List<ISC_Parameters__c> lstOfParaToProcess = new List<ISC_Parameters__c>();   //commented by Parul, moved to class
    //Map<String, Id> mapOfISCParaDefNameAndId = new  Map<String, Id>();    //commented by Parul, moved to class
    
    SR_Class_IscParameter clsISCPara = new SR_Class_IscParameter(); //initiate class
    clsISCPara.updateIscParameterFields(trigger.new);   //method call for US5004
    
    for(ISC_Parameters__c ISCpara : trigger.new) 
    {
        if(ISCpara.Parameter_Value__c != null)
        {
            Pattern pt = Pattern.compile('(\\d+(\\.\\d+)+)*');
            Matcher mhr = pt.matcher(ISCpara.Parameter_Value__c);
            String matchedString = '';
            Pattern pt2 = Pattern.compile('^[0-9]+$');
            Matcher mhr2 = pt2.matcher(ISCpara.Parameter_Value__c);
            
            Pattern pt3 = Pattern.compile('(^(0[1-9]|1[012]|[1-9])[- /.](0[1-9]|[12][0-9]|3[01]|[1-9])[- /.](19|20)\\d\\d$)');
            Matcher mhr3 = pt3.matcher(ISCpara.Parameter_Value__c);
            
            Pattern pt4 = Pattern.compile('^[y,n,Y,N]{1}$');
            Matcher mhr4 = pt4.matcher(ISCpara.Parameter_Value__c);
            
            
            if(mhr.matches())
            {
                matchedString = ISCpara.Parameter_Value__c;
                ISCpara.Parameter_Type__c='Version';
            }
            
            
            else if(mhr2.matches())
            {
                
                ISCpara.Parameter_Type__c='Number';
                ISCpara.Parameter_Number__c=integer.valueof(ISCpara.Parameter_Value__c);
            }
            
            
            else if(mhr3.matches())
            {
                
                
                try{
                    ISCpara.Completion_Date__c=date.parse(ISCpara.Parameter_Value__c);
                    ISCpara.Parameter_Type__c='Date';
                } catch(Exception e){
                    ISCpara.Parameter_Type__c='String';
                    System.debug('An exception occurred: ' + e.getMessage());
                }    
            }
            
            
            
            else if(mhr4.matches())
            {
                //matchedString4 = ISCpara.Parameter_Value__c;
                
                if(ISCpara.Parameter_Value__c.trim()=='Y'||ISCpara.Parameter_Value__c.trim() =='y')
                {
                    ISCpara.Parameter_Boolean__c = TRUE;
                }
                else
                {
                    ISCpara.Parameter_Boolean__c = FALSE;
                }
                ISCpara.Parameter_Type__c='Boolean';
            } 
            else 
            ISCpara.Parameter_Type__c='String';           
        }
        
        //code commented by Parul, moved to class
        //priti, US5400, 10/15/2014 - if name when Name is set, set Parameter Definition = lookup(ISC Parameter Definition.Name=Name)
/*        if(ISCpara.Name != null)
        {
            setOfParaName.add(ISCpara.Name);
            lstOfParaToProcess.add(ISCpara);
        }   */
    }

    //code commented by Parul, moved to class
    //priti, US5400, 10/15/2014 - if name when Name is set, set Parameter Definition = lookup(ISC Parameter Definition.Name=Name)
/*    if(setOfParaName.size()>0)
    {
        for(ISC_Parameter_Definition__c iscParaDef : [select id, name from ISC_Parameter_Definition__c where name in : setOfParaName])
        {
            mapOfISCParaDefNameAndId.put(iscParaDef.name, iscParaDef.id);
        }
        for(ISC_Parameters__c ISCpara : lstOfParaToProcess)
        {
            if(mapOfISCParaDefNameAndId.containsKey(ISCpara.Name))
                ISCpara.Parameter_Definition__c = mapOfISCParaDefNameAndId.get(ISCpara.Name);
        }
    }   */
}
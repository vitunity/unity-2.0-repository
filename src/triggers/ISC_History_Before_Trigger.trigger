/********************************************************************************************************************************
      @ Author          : Nikita Gupta
      @ Date            : 10-Dec-2013.
      @ Description     : This Trigger is for updating Installed Product id on ISC History.US3657
      @ Last Modified By      :    Nikita Gupta
      @ Last Modified On      :    6/11/2014
      @ Last Modified Reason  :     US3542, to format the product version
      @ Last Modified By      :    Nikita Gupta
      @ Last Modified On      :    7/7/2014
      @ Last Modified Reason  :     optimize code
*********************************************************************************************************************************/

trigger ISC_History_Before_Trigger on ISC_History__c (before insert, before update) 
{
    SRClassISCHistory Cls = new SRClassISCHistory(); // initiated the instance of apex class.
    cls.buildISCApiTableMap(trigger.new, trigger.oldMap);//method to populate api tabel map
    cls.setISCAPITableLookup(trigger.new, trigger.oldMap);//method to populate api tabel lookup -STRY0014508
    cls.ISCHistoryUpdateIP_Product(trigger.new, trigger.oldMap);// method called to apply update IP and Product of ISC History based on PCSN.
    cls.setProductBuild(trigger.new, trigger.oldMap);
}
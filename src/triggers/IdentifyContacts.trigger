/*************************************************************************\
    @ Author        : Mohit Bansal    
    @ Date          : 24-May-2013
    @ Description   : A trigger to Identify at most two contacts on a Account can be lung phantom contacts.
    @ Last Modified By  :   Mohit Bansal
    @ Last Modified On  :   28-May-2013
    @ Last Modified Reason  : Code Completed  
****************************************************************************/

trigger IdentifyContacts on Lung_Phantom__c (before insert, before update) 
{

    //Rakesh.Start
        IPAccountsContactValidation.validate(Trigger.oldmap,Trigger.New);
    //Rakesh.End
    
    Set<ID> sConIDs = new Set<ID>();
    Set<ID> sAccIDs = new Set<ID>();
    List<Lung_Phantom__c> lLungPhantam = [Select id, CreatedDate,contact__c,contact__r.Is_Lung_Phantom_Contact__c from Lung_Phantom__c];
    
    for(Lung_Phantom__c lp : lLungPhantam )
    {
     if(lp.contact__r.Is_Lung_Phantom_Contact__c==true)
        sConIDs.add(lp.Contact__c);
    }
 
    for(Lung_Phantom__c lp : Trigger.new)
    {
        sConIDs.add(lp.Contact__c);
    }
  
    List<Contact> temp=new List<Contact>([Select id,Is_Lung_Phantom_Contact__c,accountID, email from Contact where id in : sConIDs ]);
    Map<ID, Contact> m_ID_Con = new Map<ID,Contact>();
    for(Contact c:temp)
    {
    m_ID_Con.put(c.id,c);
    }
    for(Contact c: m_ID_Con.values())
    {
      
        sAccIDs.add(c.AccountID); 
    }
    
    //Map<ID, Account> m_ID_Acc = new Map<ID,Account>([Select id from Account where id in : sAccIDs]);
    
    Map<ID,List<Contact>> map_AccID_NoofCon = new map<ID,List<Contact>>();
    for(Contact c : m_ID_Con.values())
    {
          if(map_AccID_NoofCon.containsKey(c.accountID))
            {    
                List<Contact> lCon = new List<Contact>();
             //   if(c.Is_Lung_Phantom_Contact__c==true)
           //   {
            //  (map_AccID_NoofCon.get(c.accountID)).add(c);
                lCon = map_AccID_NoofCon.get(c.accountID);
               lCon.add(c);
            //    }
                map_AccID_NoofCon.remove(c.accountID);
                map_AccID_NoofCon.put(c.accountID,lCon);
            }
            else
            {
              
                List<Contact> lCon = new List<Contact>();
              // if(c.Is_Lung_Phantom_Contact__c==true)
               // {
                lCon.add(c);
              // }
                map_AccID_NoofCon.put(c.accountID,lCon);
            }
        
    }
     

    Map<ID,List<Lung_Phantom__c>> map_ConID_NoofLP = new map<ID,List<Lung_Phantom__c>>();
    for(Lung_Phantom__c lp : lLungPhantam )
    {
            if(map_ConID_NoofLP.containsKey(lp.Contact__c))
            {    
                List<Lung_Phantom__c> lCon = new List<Lung_Phantom__c>();
                lCon = map_ConID_NoofLP.get(lp.Contact__c);
                lCon.add(lp);
                map_ConID_NoofLP.remove(lp.Contact__c);
                map_ConID_NoofLP.put(lp.Contact__c,lCon);
            }
            else
            {
              
                List<Lung_Phantom__c> lCon = new List<Lung_Phantom__c>();
                lCon.add(lp);
                map_ConID_NoofLP.put(lp.Contact__c,lCon);
            }
        
    }
    
    List<Contact> lConUpd = new List<Contact>();
    if((trigger.isInsert) && (Trigger.isBefore))
    {
    for(Lung_Phantom__c lp : Trigger.new)
    {
        Integer len = 6;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String VID= key.substring(0,len);
        System.debug('************ '+VID);
        lp.Voucher_ID__c = VID;
                
        if(lp.contact__c != null)
        {
          //  lp.Expiration_Date__c = System.now().date();
            Contact acc = m_ID_Con.get(lp.Contact__c);
            
            
            List<Contact> lContact = new List<Contact>();
            lContact = map_AccID_NoofCon.get(acc.accountID);
            
            System.debug('>>>>>>>>> Value of map' + map_AccID_NoofCon );
              System.debug('>>>>>>>>> Value of size' + lContact.size()  );
            if(lContact.size() > 2)
            {
                lp.addError('There could only be two people at a site identified as Lung Phantom (LP) contacts');
            }
            acc.Is_Lung_Phantom_Contact__c= true;
            lConUpd.add(acc);
            List<Lung_Phantom__c> lpList = map_ConID_NoofLP.get(lp.contact__c);
            if(lplist != null)
            {
                for(Lung_Phantom__c l : lpList)
                {
                  /*  if(lp.Expiration_Date__c > l.CreatedDate.date())
                    {
                      //  lp.expiration_date__c = l.createdDate.date();
                    }*/
                }    
             }   
        }
        
    }    
    
    if(lConUpd != null)
    {
        update lConUpd;
    }
    
   } 
   
    
}
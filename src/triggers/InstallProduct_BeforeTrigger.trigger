/*---------------------------------------------------------------------
- Author        : Megha Arora
- Functionality : Trigger on Installed Product.
- Description   :  to update the Account Field from Location and when location is also null, it will check ERP functional location from Location Object.
- Last Modified By: Priti Jaiswal
- Last Modified Date: 2/19/2014
- Last Modified Reason: US4030 - to update Account oF IP when ERP Site Partner is not set and ERP Sold To is set 
- Last Modified By    : Shubham Jaiswal
- Last Modified Date  : 2-Apr-2014
- Last Modified Reason: US4275 - to update Account,Location & Product Lookup of Installed product; Commented Line 26-30
- Last Modified By    : Megha Arora
- Last Modified Date  : 7-Apr-2014
- Last Modified Reason: US3612 - to update the Account Field from Location and when location is also null, it will check ERP functional location from Location Object.
- Last Modified Date  : 09-06-2014
- Last Modified Reason: Kaushiki Verma ,refactoring
- Moved the Code from Trigger to class.
- Last Modified By      : Nikita Gupta
- Last Modified On      : 7/30/2014    
- Last Modified Reason  : US4626, update interface Status 
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
31-Aug-2017 - Rakesh - STSK0012636 - Update Product Version field when the Product Version Build field is updated.
Date/Modified By Name/Task or Story or Inc # /Description of Change
31-Aug-2017 - Pranjul - STSK0012962 - Installed Product Acceptance Process to Automate Transfer Responsibility to Field Service
and update of Warranty Dates 
17-Jan-2018 - Pranjul - STSK0013674 -Add automation to change the installed product status from "Installation" to "Pending Warranty" to "Installed"
------------------------------------------------------------------------*/
trigger InstallProduct_BeforeTrigger on SVMXC__Installed_Product__c(before insert, before update) {

    //STSK0011437.Start 
    //InstalledProductFieldUpdate.updateFieldsOnAcceptanceChange(trigger.New,Trigger.OldMap);
    //InstalledProductFieldUpdate.updateFieldsOnPCSNChange(trigger.New,Trigger.OldMap);
    ConveredServiceContract.updateContract(trigger.New, Trigger.OldMap); //Covered Product
    if (trigger.isupdate && trigger.isBefore) {
        DuplicatePCSN.Validate(trigger.New, Trigger.OldMap);
    }

    //STSK0011437.End
    if (System.Label.IP_Triggers_Flag == 'True') { 
        //SR_ClassInstalledProduct cls1 = new SR_ClassInstalledProduct(); //initiate class
        //cls1.updateInstalledProductFields(trigger.new, trigger.oldmap); //method call
    }

    if (Trigger.isUpdate || Trigger.isInsert) {

        SR_ClassInstalledProduct cls1 = new SR_ClassInstalledProduct();
        cls1.updateIPFieldsbeforetrigger(trigger.new, trigger.oldmap);
        //STSK0011901, update ERP Status Field when installed Product Status is changed
        cls1.updateERPStatusFields(trigger.new);
        //CM : DE7656 : Moved the below code to new method in trigger handler. 
        /*List<SVMXC__Installed_Product__c> ipupdateList = new List<SVMXC__Installed_Product__c>();
        Set<Id> erpId = new Set<Id>();

       Set<Id> servContIds = new Set<Id>(); //savon.FF set billing type - same as formula field 10-13-15 US5225 TA7729
        List<SVMXC__Installed_Product__c> triggerInstalledProducts = new List<SVMXC__Installed_Product__c>();
        triggerInstalledProducts = [SELECT Id, SVMXC__Serial_Lot_Number__c, Name, PCSN__c, SVMXC__Product__c, SVMXC__Product__r.Is_Top__c, 
                                    SVMXC__Parent__c, SVMXC__Parent__r.Name, SVMXC__Service_Contract__c, ERP_Parent__c, ERP_Pcodes__c FROM SVMXC__Installed_Product__c WHERE Id IN: Trigger.new ];
        //for(SVMXC__Installed_Product__c ip : Trigger.new)
        for(SVMXC__Installed_Product__c ip : triggerInstalledProducts)
        {
            if(ip.ERP_Pcodes__c != null)
            {
                erpid.add(ip.ERP_Pcodes__c);
                ipupdateList.add(ip);
            }

        //savon.FF 12-11-2015 US5078 DE6854
        if (Trigger.oldMap != null && Trigger.oldMap.get(ip.Id) != null && ip.ERP_Parent__c != Trigger.oldMap.get(ip.Id).ERP_Parent__c && ip.ERP_Parent__c != null) {
          //ip.SVMXC__Top_Level__c = ip.ERP_Parent__c;
        }

        //savon.FF added to set name to serial / lot number 10-27-15 US5259 DE6482 
        if (Trigger.isInsert && ip.SVMXC__Serial_Lot_Number__c != null) {
          ip.Name = ip.SVMXC__Serial_Lot_Number__c;
        }

        // 2Feb2016 : DEXXXX, Being done as part of Richard Defect List
        // Updating Installed Product Name When PCSN(PCSN__c) is populated
          if(ip.PCSN__c != trigger.oldMap.get(ip.Id).PCSN__c) {
              system.debug( ' ** B$ **' + ip.PCSN__c);
              ip.Name = ip.PCSN__c;
              system.debug( ' ** After **' + ip.PCSN__c);
          }

        // 2 Feb 2016 : DEXXXX, Being done as part of Richard Defect List
        //When Parent is null on Installed Products, set the ERP_Parent__c to null if Product isTop = false
        if(ip.SVMXC__Product__c != null && ip.SVMXC__Product__r.Is_Top__c == false) {
          ip.ERP_Parent__c = null;
        } 
        if(ip.SVMXC__Parent__c != null && trigger.isUpdate) {
          ip.ERP_Parent__c = ip.SVMXC__Parent__r.Name;
        } 
        

        //savon.FF set billing type - same as formula field 10-13-15 US5225 TA7729
        //IF(SVMXC__Service_Contract__c == null , 'P-Paid Service',IF(SVMXC__Service_Contract__r.ERP_Contract_Type__c== 'ZH3', 'W-Warranty', 'C-Contract'))
        if (ip.SVMXC__Service_Contract__c == null && !ip.Subscription_IsActive__c) {
          ip.Billing_Type__c = 'P – Paid Service'; //savon.FF 12-2-2015 DE6820 Long Dash
       } else {
          servContIds.add(ip.SVMXC__Service_Contract__c);
       }

      }

      if (!servContIds.isEmpty()) {
        List<SVMXC__Service_Contract__c> serviceContracts = [SELECT Id, ERP_Contract_Type__c FROM SVMXC__Service_Contract__c WHERE Id IN :servContIds];

        for (SVMXC__Installed_Product__c ip : Trigger.new) {
          for (SVMXC__Service_Contract__c sc : serviceContracts) {
            if (ip.SVMXC__Service_Contract__c == sc.Id && ip.Subscription_IsActive__c) {
              if (sc.ERP_Contract_Type__c == 'ZH3') {
                ip.Billing_Type__c = 'W – Warranty'; //savon.FF 12-2-2015 DE6820 Long Dash
              } else {
                ip.Billing_Type__c = 'C – Contract'; //savon.FF 12-2-2015 DE6820 Long Dash
              }
            }
          }
        }

      }

      //cdelfattore@forefront 12/2/2015 DE6511
      //per JS/NS: I unset the PMP Active Flag on the Installed Product and the PMP Flag remained True on the PMP Plan
      //The pmp active flag on the installed product must map to the pmp flag on the pmp plan
      if(trigger.isUpdate){
        Set<Id> installedProductsSet = new Set<Id>(); //set of installed products to update the pmp plan with
        for(SVMXC__Installed_Product__c ip : trigger.new){
          if(trigger.oldMap.get(ip.Id).PMP_Active__c != trigger.newMap.get(ip.Id).PMP_Active__c){
            installedProductsSet.add(ip.Id);
          }
        }

        //savon.FF 12-18-2015 If Statement for Query Optimization
        if (!installedProductsSet.isEmpty()) {
          //query for pmp plans
          List<SVMXC__PM_Plan__c> pmpPlansToUpates = [SELECT Id, Name, Top_Level__c FROM SVMXC__PM_Plan__c WHERE Top_Level__c IN :installedProductsSet];
          if(!pmpPlansToUpates.isEmpty()){
            for(SVMXC__PM_Plan__c pmpPlan  : pmpPlansToUpates){
              pmpPlan.PMP__c = trigger.newMap.get(pmpPlan.Top_Level__c).PMP_Active__c;

              //savon.FF 12-3-2015 US5246 DE6511 Set PMP Plan Name
              if (pmpPlan.PMP__c) {
                pmpPlan.Name = 'PMP' + pmpPlan.Name.substring(3);
              } else {
                pmpPlan.Name = 'PMI' + pmpPlan.Name.substring(3);
              }
            }

            //update pmpPlansToUpates;
          }
        }


      }*/

        //savon.FF wrapped query in if-statement 10-27-15
        //if (!erpid.isEmpty()) {
        //  Map<Id, ERP_Pcode__c> ERPMap = new Map<Id, ERP_Pcode__c>([SELECT id,PMP_Capable__c From  ERP_Pcode__c where id in: erpid]);

        //  for(SVMXC__Installed_Product__c iprod : ipupdateList)
        //  {
        //    if(erpMap.get(iprod.ERP_Pcodes__c).PMP_Capable__c == True)
        //    {
        //      iprod.PMP_Active__c = True;
        //    }
        //  }        
        //}
        //STSK0012636: Update Product Version field when the Product Version Build field is updated.
        ProduceVersionClass.VersionUpdate(trigger.new, trigger.oldmap);
    }
          
      
        
    //Chandramouli : 31-Oct-2017 : Dec 2017 : STSK0012962 : Added new logic and placed at the end of the call. 
    if (System.Label.IP_Triggers_Flag == 'True') 
    {
      SR_ClassInstalledProduct cls1 = new SR_ClassInstalledProduct(); //initiate class
      cls1.updateInstalledProductFields(trigger.new, trigger.oldmap); //method call
      
      //17-Jan-2018 - Pranjul - STSK0013674
      //cls1.changeIpStatus(Trigger.new,Trigger.oldmap);

    }
}
/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 08-May-2013
    @ Description   : This Trigger is used to set ContentRoles.
    @ Last Modified By  : Megha Arora,Kaushiki Verma(09/06/14 Null check added)
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  : Added logic to calculate installed product count on Account.
    @ Apex Test Class: InstallProductMaster_Test
    @ Last Modified By      : Nikita Gupta
    @ Last Modified On      : 6/26/2014
    @ Last Modified Reason  : US4066, to update Installed Product on Sales Order Item object
    @ Last Modified By      : Nikita Gupta
    @ Last Modified On      : 7/21/2014    
    @ Last Modified Reason  : US4732, update Top level and Parent on Installed Product and child IP
****************************************************************************/

trigger InstalledProductMaster_AfterTrigger on SVMXC__Installed_Product__c (after insert, after update, after delete)
{
   if(System.Label.IP_Triggers_Flag == 'True')
   {
        if(trigger.isinsert || trigger.isupdate)
        {
            SR_ClassInstalledProduct  objInstProd = new SR_ClassInstalledProduct ();  //initiate class
            objInstProd.updateFieldsOnChildInstProd(trigger.new, trigger.oldMap); //method call to update fields on Child IP, US4732, 7/21/2014
            objInstProd.updateLocationOnStbStatus(trigger.new, trigger.oldMap); // STRY0020320 : Regulatory:  STB Status Location Field Should Update on Change to Installed Product Location/Site
        }
    
        /* Future release
        if(trigger.isupdate){ 
            //this will be usefull after go live 1c
            //System.debug('---PMPPMIHandler.handle');
            //PMPPMIHandler.handle(Trigger.new, Trigger.oldmap);
        }
        */

    /************ CODE FOR CUSTOMER PORTAL STARTS *********************************/
        /* Added logic to calculate installed product count on Account. */ 
        // build list of Account SVMXC__Installed_Product__c ids   
        Set<Id> AccountIds = new Set<Id>();
        List<Account> AccountsToUpdate = new List<Account>();
        Set<String> mktKitsList;
        Map<Id,List<MarketingKitRole__c>> addmarketKits = new Map<Id,List<MarketingKitRole__c>>(); 
        Map<String,Market_kit__c> mkitAccess = Market_kit__c.getAll();
        map<Id,Set<String>> accKits = new Map<Id,Set<String>>();
        Set<String> kitInstallPdt;
        Map<Id,Double> AccountMap = new Map<id,Double>();
        Set<Id> IpIds = new Set<Id>();
    
        if(trigger.isinsert || trigger.isupdate)
        {
            //Assign user to content library on install product addition. Customer portal begins        
            for (SVMXC__Installed_Product__c varIP : Trigger.New)
            {
                system.debug('@@@@@@varIP.SVMXC__Company__c@@@@@@' +varIP.SVMXC__Company__c);
                // other trigger    //Account is set or re-set (for future call)
                if(varIP.SVMXC__Company__c != null && (trigger.isInsert || (trigger.isUpdate && (trigger.oldmap.get(varIP.id).SVMXC__Company__c == null || trigger.oldmap.get(varIP.id).SVMXC__Company__c != varIP.SVMXC__Company__c)))) // added by harshita 4th nov due to too many future calls in sfsit2
                {
                    IpIds.add(varIP.id);
                }
    
                if(trigger.isinsert && varIP.SVMXC__Company__c != null) 
                {
                    AccountIds.add(varIP.SVMXC__Company__c);
                }
                //  if SVMXC__Installed_Product__c previously had Account, but no longer does, also add so we can recalculate   
                if(trigger.isupdate && varIP.SVMXC__Company__c != null && (trigger.oldmap.get(varIP.id).SVMXC__Company__c != varIP.SVMXC__Company__c))
                {
                    AccountIds.add(varIP.SVMXC__Company__c);
                    
                    if(trigger.oldmap.get(varIP.id).SVMXC__Company__c != null)
                    {
                       system.debug('@@@@@@Inside if@@@@@@');
                       system.debug('@@@@@@trigger.oldmap.get(varIP.id).SVMXC__Company__c@@@@@@' + trigger.oldmap.get(varIP.id).SVMXC__Company__c);
                       AccountIds.add(trigger.oldmap.get(varIP.id).SVMXC__Company__c);
                    }
                }
            }
            if(IpIds.size() > 0)
            {
                CpProductPermissions.setContentRoles(IpIds, IpIds);   //CP class
            }
    
            // Store all the marketkits record Ids associated to accounts and to grant market kit access based on Install Base product codes
            for (SVMXC__Installed_Product__c c: [Select id, SVMXC__Company__c, SVMXC__Product__r.ProductCode from SVMXC__Installed_Product__c
                                                    where id in :Trigger.NewMap.keyset()])
            {
                for(Market_kit__c mkit:mkitAccess.values())
                {
                    mktKitsList = new set<String>();
                    kitInstallPdt = new Set<String>();
                    kitInstallPdt.addAll(mkit.Install_Products__c.split(';'));

                    if((c.SVMXC__Company__c != null && c.SVMXC__Product__r.ProductCode != null) && kitInstallPdt.contains(c.SVMXC__Product__r.ProductCode))
                    {
                        if(accKits.containskey(c.SVMXC__Company__c))
                        {
                            mktKitsList = accKits.get(c.SVMXC__Company__c);
                        }
                        mktKitsList.add(mkit.MarketKit_Id__c);
                        accKits.put(c.SVMXC__Company__c,mktKitsList);
                    }
            }
        }
            System.debug('++++++++++++++++++++++++++++account kits to add:'+accKits);
        }
    
        //if delete, make sure we update Account
        else if(trigger.isdelete)
        {
            for(SVMXC__Installed_Product__c c: Trigger.Old)
            {
                if(c.SVMXC__Company__c != null)
                {
                    AccountIds.add(c.SVMXC__Company__c);
                }
            }
        }
       
        /* DFCT0011814
        if(AccountIds.size()>0)
        {
            
            for(AggregateResult q : [select count(Id), SVMXC__Company__c from SVMXC__Installed_Product__c where
                                                     SVMXC__Company__c IN :AccountIds group by SVMXC__Company__c ])
            {
                AccountMap.put((Id)q.get('SVMXC__Company__c'), (Double)q.get('expr0'));
            }
            if(AccountMap.size()>0)
            {
                for(Account acc : [Select Id, Install_Product_Count__c from Account where Id IN :AccountIds])
                {
                    if(AccountMap.containsKey(acc.Id) && AccountMap.get(acc.Id) != null )
                    {
                        Double count = AccountMap.get(acc.Id);
                        acc.Install_Product_Count__c = count;
                        AccountsToUpdate.add(acc);
                    }
                }
            }
            
        }
        
        if(AccountsToUpdate.size()>0)
        {
            update AccountsToUpdate; 
        }
        
        DFCT0011814 */
    
        //Assign user to content library on install product addition. Customer portal begins
        if(trigger.isInsert || trigger.isUpdate)
        {
            /*IpIds = new Set<Id>();
    
            for(SVMXC__Installed_Product__c  ip : trigger.new)
            {
                IpIds.add(ip.id);
            }    
            CpProductPermissions.setContentRoles(IpIds, IpIds); */
    
            // Adding new market kit role object based on IP and prevent adding duplicate
            Set<String> mktLstExist = new set<String>();
            Map<Id,Set<String>> existmarketKits = new Map<Id,Set<String>>();
            List<MarketingKitRole__c> mkList =  new List<MarketingKitRole__c>();
    
            if(AccountIds.size() > 0)
            {
                for(MarketingKitRole__c m : [Select id, Account__c, Product__c from MarketingKitRole__c where Account__c in :AccountIds])
                {
                    if(existmarketKits.containskey(m.Account__c))
                    {
                        mktLstExist = existmarketKits.get(m.Account__c);
                    }
                    mktLstExist.add(m.Product__c);
                    existmarketKits.put(m.Account__c,mktLstExist);
                }
                System.debug('+++++++++++++++++++++account kits to add'+accKits);
                System.debug('+++++++++++++++++++++existing market Kits'+existmarketKits);
            }
    
            for (String accId: accKits.keySet())
            {
                for(String mktKits: accKits.get(accId))
                {
                    if(existmarketKits.containskey(accId) && (!(existmarketKits.get(accId).contains(mktKits))))
                    {
                        MarketingKitRole__c newkit = new MarketingKitRole__c(); 
                        newkit.Account__c = accId;
                        newkit.Product__c = mktKits;
                        mkList.add(newkit);
                    }
                }
            }
            System.debug('++++++++++++++++++++++++++++++++Market Kit record list to insert:'+mkList);
    
            try
            {
                insert mkList;
            }
            catch(System.DmlException e)
            {
                for (Integer i = 0; i < e.getNumDml(); i++)
                {
                    System.debug('An error has occured while inserting the Market Kit role record. Message:'+e.getDmlMessage(i)); 
                }
            }
            accKits.clear();         
            mktLstExist.clear();
            existmarketKits.clear();
        }
    /************ CODE FOR CUSTOMER PORTAL ENDS *********************************/
    // No longer required - Amit
    /************ code to check if ISC History on IP is of Latest Revision Date, if not update it ***********/
    //if(Trigger.IsAfter && Trigger.IsUpdate)
    //{
        //SR_ClassInstalledProduct  InstProdCls = new SR_ClassInstalledProduct ();
        //InstProdCls.InstPrdISCPVBUpdate(trigger.new, trigger.oldMap);
    //}
    /************ Finished code to check if ISC History on IP is of Latest Revision Date, if not update it ***********/ 


    /* Deprecated cpde
    Map<Id, List<SVMXC__PM_Plan__c>> pmpMaptoIP = new Map<Id, List<SVMXC__PM_Plan__c>>();
    Set<Id> IdPMP = new Set<Id>();
    Set<Id> ipIdList = new Set<Id>();
    //Set<Id> pmpTemplateIdList = new Set<Id>(); //savon.FF 12-8-2015 US5246 DE6888
    Set<Id> slaDetailIdSet = new Set<Id>(); //savon.FF 12-8-2015 US5246 DE6888
    Map<Id, Id> ipTopmpMap = new Map<Id, Id>();
    List<SVMXC__PM_Plan__c> filteredpmplist = new List<SVMXC__PM_Plan__c>();
    /* For release
    if((trigger.isUpdate || trigger.isInsert) && trigger.new != Null)
    {
         for(SVMXC__Installed_Product__c ip: trigger.new)
        {

            if(ip.PMP_Active__c && ( trigger.oldmap == null || trigger.oldmap.get(ip.id) == null || trigger.oldmap.get(ip.id).PMP_Active__c == null || trigger.oldmap.get(ip.id).PMP_Active__c == false))
            {
                ipIdList.add(ip.Id);
                List<SVMXC__PM_Plan__c> pmplists = new List<SVMXC__PM_Plan__c>();
                pmpMaptoIP.put(ip.id, pmplists);
            }
        }

        
        //savon.FF 11-11-2015 If statement around query for optimization
        List<SVMXC__PM_Plan__c> pmpList = new List<SVMXC__PM_Plan__c>();        
        
        if (!ipIdList.isEmpty()) {
            //savon.FF 12-8-2015 US5246 DE6888 added Top_Level__r.SVMXC__Service_Contract__r.SVMXC__Service_Level__c, SLA_Detail__c to query
            pmpList = [select ID, SVMXC__Status__c,PM_Group__c, SVMXC__Frequency__c, Top_Level__c, Top_Level__r.SVMXC__Service_Contract__r.SVMXC__Service_Level__c, SLA_Detail__c,
                    SVMXC__PM_Plan_Template__c,(select SVMXC__PM_Plan__c, SVMXC__Scheduled_On__c from SVMXC__PM_Schedule__r),
                    (select SVMXC__PM_Plan__c from SVMXC__PM_Coverage__r where 
                    SVMXC__PM_Plan__r.PM_Group__c = '2') from SVMXC__PM_Plan__c where Top_Level__c
                    in: ipIdList];
        }



        for(SVMXC__PM_Plan__c pmp: pmpList)
        {
            if(pmpMaptoIP.containsKey(pmp.Top_Level__c) && (( pmp.PM_Group__c == '2' && pmp.SVMXC__Status__c !='Superseded') || pmp.PM_Group__c == '1'))
            {
                //savon.FF 12-8-2015 US5246 DE6888 
                if (pmp.PM_Group__c == '2') {
                    pmp.SVMXC__Status__c = 'Superseded';
                }

                List<SVMXC__PM_Plan__c> pmpMapList = pmpMaptoIP.get(pmp.Top_Level__c);
                pmpMapList.add(pmp);
                pmpMaptoIP.put(pmp.Top_Level__c, pmpMapList);
            }
            if(pmp.PM_Group__c == '2')
            {
                filteredpmplist.add(pmp);
            } 
        }

        if(!pmpMaptoIP.isEmpty())
        {
            List<SVMXC__PM_Schedule__c> updateSchedules = new List<SVMXC__PM_Schedule__c>();
            for(SVMXC__Installed_Product__c ip: trigger.new)
            {
               if(pmpMaptoIP.containskey(ip.id))
               {
                    List<SVMXC__PM_Plan__c> relPMP = pmpMaptoIP.get(ip.id);
                    for(SVMXC__PM_Plan__c relPMPs : relPMP)
                    {
                        //savon.FF 12-3-2015 US5246 DE6511 - pmp should not have more than one schedule
                        if (relPMPs.SVMXC__PM_Schedule__r !=null && relPMPs.SVMXC__PM_Schedule__r.size() > 1) {
                            ip.addError('Associated Preventative Maintenance Plan should NOT have more than one Preventative Maintenance Schedule');
                        } else {

                            SVMXC__PM_Schedule__c group2schedule = new SVMXC__PM_Schedule__c();
                            SVMXC__PM_Schedule__c group1schedule = new SVMXC__PM_Schedule__c();
                            Date group1Date;
                            Date group2Date;
                            //savon.FF 10-14-15 added the empty check to if statement below
                            if(relPMPs.PM_Group__c == '1' && relPMPs.SVMXC__PM_Schedule__r !=null && !relPMPs.SVMXC__PM_Schedule__r.isEmpty())    
                            {
                                group1schedule = relPMPs.SVMXC__PM_Schedule__r;
                                group1Date = group1schedule.SVMXC__Scheduled_On__c;
                            
                            }else if(relPMPs.PM_Group__c == '2' && relPMPs.SVMXC__PM_Schedule__r!=null && !relPMPs.SVMXC__PM_Schedule__r.isEmpty()) //savon.FF 10-14-15 added the empty check to if statement below
                            {
                                group2schedule = relPMPs.SVMXC__PM_Schedule__r;
                                group2Date = group2schedule.SVMXC__Scheduled_On__c;
                            }
                            if(group1Date > group2Date)
                            {
                                    group1schedule.SVMXC__Scheduled_On__c = group2Date;
                                    updateSchedules.add(group1schedule);
                            }
                        }
                    }
                }
            }
            if(!updateSchedules.isEmpty())
            {
               // update updateSchedules;    
            }
            
        }
        for(SVMXC__PM_Plan__c pmp: filteredpmplist)
        {
            if(pmp.PM_Group__c == '1')
            {
                iptopmpmap.put(pmp.Top_Level__c, pmp.ID);
                //pmpTemplateIdList.add(pmp.SVMXC__PM_Plan_Template__c); //savon.FF 12-8-2015 US5246 DE6888
                slaDetailIdSet.add(pmp.SLA_Detail__c); //savon.FF 12-8-2015 US5246 DE6888
            }
  }

        //savon.FF 12-8-2015 US5246 DE6888 - commented out old query added new
        List<SVMXC__SLA_Detail__c> slaDetailList = new List<SVMXC__SLA_Detail__c>();

        if (!slaDetailIdSet.isEmpty()) {
            slaDetailList = [SELECT Id, SVMXC__Available_Services__c, SVMXC__SLA_Terms__c, SVMXC__Available_Services__r.Frequency__c FROM SVMXC__SLA_Detail__c WHERE Id in: slaDetailIdSet AND SVMXC__Available_Services__r.PMP__c = true];
        }

        //List<SVMXC__PM_Plan_Template__c>ptList = [select Id, (Select Frequency__c from Available_Services__r where Frequency__c != null) from SVMXC__PM_Plan_Template__c where Id in: pmpTemplateIdList];
        //Map<Id, SVMXC__PM_Plan_Template__c> ptMap = new Map<id , SVMXC__PM_Plan_Template__c>(); //savon.FF 12-8-2015 US5246 DE6888
        //if(!ptList.isEmpty())
        //{
        //    for(SVMXC__PM_Plan_Template__c ptp:ptList)
        //    {
        //        ptmap.put(ptp.id, ptp);
        //    }
        //}

        //savon.FF 12-8-2015 US5246 DE6888
        Map<Id, SVMXC__SLA_Detail__c> slaMap = new Map<id , SVMXC__SLA_Detail__c>(); 
        
        if (!slaDetailList.isEmpty()) {
            for (SVMXC__SLA_Detail__c sla : slaDetailList) {
                slaMap.put(sla.Id, sla);
            }
        }


        //savon.FF 11-11-2015 US4917 DE6683 
        for(SVMXC__PM_Plan__c pmp: pmpList)
        {
            IdPMP.add(ipTopmpMap.get(pmp.Top_Level__c));
        }

        List<SVMXC__PM_Plan__c> pmpLists = new List<SVMXC__PM_Plan__c>();

        //JW@ff 11-5-2015 moved query outside foor loop 
        //savon.FF 11-11-2015 US4917 DE6683 - don't run if list empty
        if (!IdPMP.isEmpty()) {
            String query = QueryUtilClass.getCreatableFieldsSOQL('SVMXC__PM_Plan__c') +' FROM SVMXC__PM_Plan__c WHERE Id IN :IdPMP';
            pmpLists = Database.query(query);              
        }

        for(SVMXC__PM_Plan__c pmp: pmpList)
        {
            
            IdPMP.add(ipTopmpMap.get(pmp.Top_Level__c));
            Map<Id, SVMXC__PM_Plan__c> pmpMaptoProduct = new Map<Id, SVMXC__PM_Plan__c>();

            for(SVMXC__PM_Plan__c pm : pmpLists)
            {
                SVMXC__PM_Plan__c clone = pm.clone(False, True, True, True);
                pmpMaptoProduct.put(clone.Top_Level__c, clone);
            }
            
            if(pmp.PM_Group__c == '1')
            {
                //savon.FF 12-8-2015 US5246 DE6888
                //if(ptmap.get(pmp.SVMXC__PM_Plan_Template__c) != null)
                //{ 
                //    for(SVMXC__Service__c availService : ptmap.get(pmp.SVMXC__PM_Plan_Template__c).Available_Services__r)
                //    {
                //        pmp.SVMXC__Frequency__c = availService.frequency__C; 
                //        break;
                //    }
                //}

                //savon.FF 12-8-2015 US5246 DE6888
                if (slaMap.get(pmp.SLA_Detail__c) != null && slaMap.get(pmp.SLA_Detail__c).SVMXC__Available_Services__r.Frequency__c != null && pmp.Top_Level__r.SVMXC__Service_Contract__r.SVMXC__Service_Level__c == 
                        slaMap.get(pmp.SLA_Detail__c).SVMXC__SLA_Terms__c) 
                {
                    pmp.SVMXC__Frequency__c = slaMap.get(pmp.SLA_Detail__c).SVMXC__Available_Services__r.Frequency__c;
                }


            }else if(pmp.PM_Group__c == '2')
                {
                    for(SVMXC__PM_Coverage__c pc : pmp.SVMXC__PM_Coverage__r)
                    {
                        if(pmp.Top_Level__c != null && pmpMaptoProduct.containsKey(pmp.Top_Level__c)) //JW@ff 10-22-2015 DE6440 - fix nullPointerException on pmpMaptoProduct
                        {
                            pc.SVMXC__PM_Plan__c = pmpMaptoProduct.get(pmp.Top_Level__c).id;
                        }
                        
                    }
                }
            }

        //update pmplist;
    
        }*/

        

    }

    
}
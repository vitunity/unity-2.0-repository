/***************************************************************************
Author: Steve Avon / Forefront
Created Date: 10-15-2015
Description: 
US5252 - Set Alert If No Action field if creator has technician record

Change Log:
6-Oct-2017 - Divya Hargunani - STSK0013046\STSK0013564 - RAQA:  More L2848 Object enhancements: Send email when ECF status is set to Additional Information Provided
4-Jan-2018 - Divya Hargunani - STSK0013618 - STRY0046316 : RAQA - Auto populate recommendation of field from ECF
8-Jan-2018 - Divya Hargunani - STSK0013563 - STRY0031505 : RAQA: Make "ETQ #" field read only and populate from ECF form
*************************************************************************************/

trigger L2848_AfterTrigger on L2848__c (after insert, after update, after delete, after undelete) {
    
    //Audit Tracking Future Invoke
    if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter) SObjectHistoryProcessor.trackHistory('L2848__c');
    
    if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter) {
        //STSK0013618 - STRY0046316 : RAQA - Auto populate recommendation of field from ECF
        L2848TriggerHandler.updateInvestigationNotesOnCase(Trigger.New,  Trigger.oldmap);
        
        //STSK0013563 - STRY0031505 : RAQA: Make "ETQ #" field read only and populate from ECF form
        L2848TriggerHandler.updateETQnumberToCase(Trigger.New, Trigger.oldmap);
        
        //STSK0013046 - RAQA:  More L2848 Object enhancements
        L2848TriggerHandler.AdditionalInfoReqEmail(trigger.new, trigger.oldmap);
        
        set<Id> createdByIds = new set<Id>(); //savon.FF 10-15-2015 US5252 TA7572
        //savon.FF 10-15-2015 US5252 TA7572
        for(L2848__c objL2848: Trigger.New)
        {        
            if (Trigger.isInsert && objL2848.Alert_If_No_Action__c == null) {
                createdByIds.add(objL2848.CreatedById);
            }   
        }
        //savon.FF 10-15-2015 US5252 TA7572
        //if (SR_Class_L2848.alertNumber == 0) { //DE7129
        List<SVMXC__Service_Group_Members__c> technicianList = new List<SVMXC__Service_Group_Members__c>();
        if (!createdByIds.isEmpty()) {
            technicianList = [SELECT Id, User__c FROM SVMXC__Service_Group_Members__c WHERE User__c IN :createdByIds];
        }
        if (!technicianList.isEmpty()) {
            List<L2848__c> cltList = [SELECT Id, CreatedById, Alert_If_No_Action__c FROM L2848__c WHERE Id IN :Trigger.New];
            for (L2848__c clt : cltList) {
                for (SVMXC__Service_Group_Members__c tech : technicianList) {
                    if (clt.CreatedById == tech.User__c && clt.Alert_If_No_Action__c == null) {
                        clt.Alert_If_No_Action__c = tech.User__c;
                    }
                } 
            }
            update cltList;
        }
    }
    
    if ((trigger.isundelete || trigger.isInsert || trigger.isUpdate) && trigger.isAfter) {
        L2848TriggerHandler.updateL2848CountWo(Trigger.New);
    }
    
    if ((trigger.isdelete) && trigger.isAfter) {
        L2848TriggerHandler.updateL2848CountWo(Trigger.old);
        
    }
    
    //        SR_Class_L2848.alertNumber++;
    // }    //DE7129
    // STSK0013618, STSK0013563 - Divya Hargunani - Added this to update the case only once 
    if(!L2848TriggerHandler.caseMapforUpdate.isEmpty()){
        L2848TriggerHandler.updateCase();
    }
}
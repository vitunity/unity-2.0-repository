/*
    Change Log:
    Date/Modified By Name/Task or Story or Inc # /Description of Change
    31-Aug-2017 - Pranjul - STSK0012781 - INC4668431 - L2848 Triggers for Alerts is not working.
*/

trigger L2848_BeforeTrigger on L2848__c (before insert,before update) 
{
    
    if (Trigger.isInsert) {
        List<L2848__c> listL2848 = new List<L2848__c>();
        Set<id> CaseIds = new set<Id>();
        //DE6852
        Set<Id> installIds = new Set<Id> ();
        //Set<String> InstallProdNames = new Set <String> ();
        Set<Id> woIds = new Set<Id> ();
        Set<Id> userIds = new Set<Id> ();
        for(L2848__c objL2848: Trigger.New)
        {
             //STSK0012781 - INC4668431 - L2848 Triggers for Alerts is not working
            objL2848.Is_Submitted__c = 'Not yet';
            if(objL2848.Case__c!=null && (trigger.isInsert || (trigger.isUpdate && trigger.oldmap.get(objL2848.id).Case__c != objL2848.Case__c)))
            {
                CaseIds.add(objL2848.Case__c);
                listL2848.add(objL2848);
                installIds.add(objL2848.Top_Level_Installed_Product__c);
                installIds.add(objL2848.Component_Installed_Product__c);
                Id usrId = objL2848.createdById != null ? objL2848.createdById : System.UserInfo.getUserId(); 
                userIds.add(usrId);
                userIds.add(objL2848.District_Manager__c);
                woIds.add(objL2848.Work_Order__c);
                //InstallProdNames.add(objL2848.Device_or_Application_Name2__c);
                //InstallProdNames.add(objL2848.Device_or_Application_Name__c);
            } 
            
            if(objL2848.Work_Order__c!=null && (trigger.isInsert || (trigger.isUpdate && trigger.oldmap.get(objL2848.id).Work_Order__c!= objL2848.Work_Order__c))) {
                woIds.add(objL2848.Work_Order__c);
            } 
            
        }
        Map<string,Case> mapCaseWithRelObject = new Map<string,Case>( [SELECT Id,Contact_Phone__c, Description,
                                                                    SVMXC__Top_Level__r.Service_Team__r.SVMXC__Group_Code__c, //DE1416
                                                                    SVMXC__Top_Level__r.Equipment_Description__c,
                                                                    Case_Activity__c,SVMXC__Top_Level__r.Service_Team__r.District_Manager__r.Name,
                                                                    SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__State__c,
                                                                    SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__City__c,
                                                                    SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__Country__c ,
                                                                    Contact.Name,Subject,CreatedDate ,
                                                                    Was_anyone_injured__c,Customer_Malfunction_Start_Date_time_Str__c, 
                                                                    Owner.Title, Owner.Phone, Malfunction_Start__c ,
                                                                    Contact.Email,
                                                                    Original_Owner__c,owner.name,
                                                                    Is_This_a_Complaint__c,
                                                                    Account.District_Manager__c,
                                                                    Is_escalation_to_the_CLT_required__c ,
                                                                    Account.Id ,
                                                                    Account.Name ,
                                                                    Account.ShippingCity,Account.ShippingCountry,
                                                                    Account.ShippingState,
                                                                    Account.BillingCity,Account.BillingCountry,
                                                                    Account.BillingState,
                                                                    Account.CSS_District__c,
                                                                    Employee_Phone__c,
                                                                    Phone_Number__c,
                                                                    SVMXC__Top_Level__c,
                                                                    SVMXC__Top_Level__r.Name,
                                                                    SVMXC__Top_Level__r.SVMXC__Site__c,
                                                                    SVMXC__Top_Level__r.SVMXC__Product__r.Name,
                                                                    SVMXC__Top_Level__r.Service_Team__r.District_Manager__c,
                                                                    Device_or_Application_Name2__c,
                                                                    SVMXC__Top_Level__r.Product_Version_Build__r.Name,
                                                                    Account.District__c,
                                                                    City2__c,
                                                                     Contact.Title,                                
                                                                    SVMXC__Product__r.Name,
                                                                    CreatedBy.Phone,
                                                                    E_mail_Address__c ,PCSN__c,
                                                                    PCSN2__c ,
                                                                    Service_Pack__c,Service_Pack2__c,
                                                                    CaseNumber ,
                                                                    ProductSystem__r.name,
                                                                    State_Province2__c,
                                                                    Title_Role__c,
                                                                    Version_Mode__c,Version_Model2__c 
                                                                    FROM Case where id = :CaseIds]
                                                                 );
           
        //DE6852
        Map<Id, SVMXC__Installed_Product__c> installedProducts = new Map<Id, SVMXC__Installed_Product__c > ([Select s.Product_Version_Build_Name__c, s.Name, s.Id, Service_Team__r.SVMXC__Group_Code__c, SVMXC__Product_Name__c From SVMXC__Installed_Product__c s where Id IN :installIds]);
        Map <Id, User> users = new Map <Id, User>( [Select u.Title, u.Phone, u.Id, u.Department, u.Name From User u where Id IN: userIds]);
        Map<Id, SVMXC__Service_Order__c> workOrders = new Map <Id, SVMXC__Service_Order__c> ([Select SVMXC__Group_Member__c, Id, Name, Service_Team__r.SVMXC__Group_Code__c From SVMXC__Service_Order__c where Id IN :woIds]);
        System.debug(logginglevel.info, 'Users ========= '+users);
        for(L2848__c objL2848 : listL2848)
        {   
            
            if(workOrders.containskey(objL2848.Work_Order__c)){
                objL2848.Technician_Equipment__c = workOrders.get(objL2848.Work_Order__c).SVMXC__Group_Member__c;
            }
            
            if(mapCaseWithRelObject.containsKey(objL2848.Case__c) && mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingCity != Null && mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingCountry != Null && mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingState != Null)
            {

                if (objL2848.City__c == null) {  objL2848.City__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingCity; }
                if (objL2848.Country__c == null) { objL2848.Country__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingCountry; }
                if (objL2848.State_Province__c == null) { objL2848.State_Province__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingState; }
            }
            else
            {
                if (objL2848.City__c == null) { objL2848.City__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.BillingCity;
                }
                if (objL2848.Country__c == null) { objL2848.Country__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.BillingCountry;
                }
                if (objL2848.State_Province__c == null) { objL2848.State_Province__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.BillingState;               
                }
            }
            if(installedProducts.containsKey(objL2848.Top_Level_Installed_Product__c)) {
                if (objL2848.Device_or_Application_Name__c == null || String.isBlank(objL2848.Version_Model__c)) {
                    objL2848.Device_or_Application_Name__c = installedProducts.get(objL2848.Top_Level_Installed_Product__c).SVMXC__Product_Name__c;  //DE6852
                    //objL2848.Version_Model__c = installedProducts.get(objL2848.Top_Level_Installed_Product__c).Product_Version_Build_Name__c;
                    //objL2848.District_or_Area__c = installedProducts.get(objL2848.Top_Level_Installed_Product__c).Service_Team__r.SVMXC__Group_Code__c;
                }   
            } 
            if(installedProducts.containsKey(objL2848.Component_Installed_Product__c)) {
                if (objL2848.Device_or_Application_Name2__c == null || String.isBlank(objL2848.Version_Model2__c)) {
                    objL2848.Device_or_Application_Name2__c = installedProducts.get(objL2848.Component_Installed_Product__c).SVMXC__Product_Name__c; //DE6852
                    objL2848.Version_Model2__c = installedProducts.get(objL2848.Component_Installed_Product__c).Product_Version_Build_Name__c;
                }   
            }
            //set verain employee info
            System.debug(logginglevel.info, 'User Info ========= '+ users.get(objL2848.CreatedById));
            Id usrId = objL2848.createdById != null ? objL2848.createdById : System.UserInfo.getUserId();
            if ( (String.isBlank(objL2848.Title_Department__c) || String.isBlank(objL2848.Employee_Phone__c) || String.isBlank(objL2848.Varian_Employee_Name__c)) && users.containsKey(usrId)) {
                objL2848.Varian_Employee__c = objL2848.Varian_Employee__c !=null ? objL2848.Varian_Employee__c : usrId;
                objL2848.Title_Department__c = users.get(usrId).title + ' / '+ users.get(usrId).Department;
                objL2848.Employee_Phone__c = users.get(usrId).phone;
                objL2848.Varian_Employee_Name__c = users.get(usrId).Name;
            }
            //DM Name
            if (objL2848.Varian_District_Manager_for_Site__c  == null && users.containsKey(objL2848.District_Manager__c)) {
                objL2848.Varian_District_Manager_for_Site__c = users.get(objL2848.District_Manager__c).Name;   
            }
            if (objL2848.District_or_Area__c == null && workOrders.get(objL2848.Work_Order__c) !=null && workOrders.get(objL2848.Work_Order__c).Service_Team__r != null) {
                objL2848.District_or_Area__c = workOrders.get(objL2848.Work_Order__c).Service_Team__r.SVMXC__Group_Code__c; 
            }
            if(mapCaseWithRelObject.containsKey(objL2848.Case__c)) 
            {
                if (objL2848.E_mail_Address__c == null && mapCaseWithRelObject.get(objL2848.Case__c).Contact != null) {
                    objL2848.E_mail_Address__c = mapCaseWithRelObject.get(objL2848.Case__c).Contact.Email;  
                }
                if (objL2848.Name_of_Customer_Contact__c == null) {
                    objL2848.Name_of_Customer_Contact__c = mapCaseWithRelObject.get(objL2848.Case__c).Contact.Name;      
                }
                if (objL2848.PCSN__c == null) {
                    objL2848.PCSN__c = mapCaseWithRelObject.get(objL2848.Case__c).ProductSystem__r.Name;
                }
                if (objL2848.PCSN2__c == null) {
                    objL2848.PCSN2__c = mapCaseWithRelObject.get(objL2848.Case__c).PCSN2__c ;
                }
                if (objL2848.Phone_Number__c == null) {
                    objL2848.Phone_Number__c = mapCaseWithRelObject.get(objL2848.Case__c).Contact_Phone__c;
                }
                if (objL2848.Service_Pack__c == null) {
                    objL2848.Service_Pack__c = mapCaseWithRelObject.get(objL2848.Case__c).Service_Pack__c;
                }
                if (objL2848.Service_Pack2__c == null) {
                    objL2848.Service_Pack2__c = mapCaseWithRelObject.get(objL2848.Case__c).Service_Pack2__c;
                }
                if (objL2848.Service_Request_Notification__c == null) {
                    objL2848.Service_Request_Notification__c = mapCaseWithRelObject.get(objL2848.Case__c).CaseNumber;
                }

                if (objL2848.Site_Name__c == null) {
                    objL2848.Site_Name__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.Name;
                }

                if (objL2848.Location__c == null) {
                    objL2848.Location__c = mapCaseWithRelObject.get(objL2848.Case__c).SVMXC__Top_Level__r.SVMXC__Site__c;
                }

                if (objL2848.Top_Level_Installed_Product__c == null) {
                    objL2848.Top_Level_Installed_Product__c = mapCaseWithRelObject.get(objL2848.Case__c).SVMXC__Top_Level__c;
                }

                if (objL2848.Title_Role__c == null) {
                    objL2848.Title_Role__c = mapCaseWithRelObject.get(objL2848.Case__c).Contact.Title;    
                }
                
                if (objL2848.Varian_Employee_Receiving_Info__c == null) {
                    objL2848.Varian_Employee_Receiving_Info__c = mapCaseWithRelObject.get(objL2848.Case__c).Owner.Name;//Original_Owner__c;
                }
                // objL2848.Version_Model__c = mapCaseWithRelObject.get(objL2848.Case__c).SVMXC__Top_Level__r.Product_Version_Build__r.Name; // Wave 1C - Nikita 1/25/2015
                if (objL2848.Version_Model2__c == null) {
                    objL2848.Version_Model2__c = mapCaseWithRelObject.get(objL2848.Case__c).Version_Model2__c;
                }
    
                //savon.FF 12-7-2015 US5252 DE6851 changed from When_Where_Who_What__c to Facts_Details_of_Complaint__c for field consistency
                if (objL2848.Facts_Details_of_Complaint__c == null) {
                    objL2848.Facts_Details_of_Complaint__c = mapCaseWithRelObject.get(objL2848.Case__c).Description;
                }
                if (objL2848.Complaint_Short_Description__c == null) {
                    objL2848.Complaint_Short_Description__c = mapCaseWithRelObject.get(objL2848.Case__c).Subject;
                }
                //objL2848.Date_of_Event__c = mapCaseWithRelObject.get(objL2848.Case__c).Malfunction_Start__c; //commented for DFCT0010380, Nikita
                //objL2848.Date_Reported_to_Varian__c = mapCaseWithRelObject.get(objL2848.Case__c).createdDate; //commented for DFCT0010380, Nikita
                if (objL2848.Was_anyone_injured__c == null) {
                    objL2848.Was_anyone_injured__c = mapCaseWithRelObject.get(objL2848.Case__c).Was_anyone_injured__c;        
                }
                if (objL2848.Customer_Malfunction_Start__c == null) {
                    objL2848.Customer_Malfunction_Start__c =  mapCaseWithRelObject.get(objL2848.Case__c).Customer_Malfunction_Start_Date_time_Str__c;
                }   
                objL2848.Device_or_Application_Name__c = mapCaseWithRelObject.get(objL2848.Case__c).SVMXC__Top_Level__r.SVMXC__Product__r.Name; //Case__r.Product.Name;
                objL2848.Device_or_Application_Name2__c = mapCaseWithRelObject.get(objL2848.Case__c).Device_or_Application_Name2__c;
                //objL2848.Version_Model__c = mapCaseWithRelObject.get(objL2848.Case__c).SVMXC__Top_Level__r.Product_Version_Build__r.Name;
                objL2848.District_or_Area__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.CSS_District__c;
                objL2848.District_Manager__c = mapCaseWithRelObject.get(objL2848.Case__c).SVMXC__Top_Level__r.Service_Team__r.District_Manager__c;
            }  
        }  
    }
}
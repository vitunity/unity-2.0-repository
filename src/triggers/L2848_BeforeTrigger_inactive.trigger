trigger L2848_BeforeTrigger_inactive on L2848__c (before insert,before update) 
{
    List<L2848__c> listL2848 = new List<L2848__c>();
    Set<id> CaseIds = new set<Id>();
     //Set<id> L2848ids = new set<Id>();
    

    for(L2848__c objL2848: Trigger.New)
    {
        if(objL2848.Case__c!=null && (trigger.isInsert || (trigger.isUpdate && trigger.oldmap.get(objL2848.id).Case__c != objL2848.Case__c)))
        {
            CaseIds.add(objL2848.Case__c);
            listL2848.add(objL2848);
        }
    }
    Map<string,Case> mapCaseWithRelObject = new Map<string,Case>( [SELECT Id,Contact_Phone__c,Description,
                                                                ProductSystem__r.Service_Team__r.SVMXC__Group_Code__c, //DE1416
                                                                ProductSystem__r.Equipment_Description__c,
                                                                Case_Activity__c,ProductSystem__r.Service_Team__r.District_Manager__r.Name,
                                                                ProductSystem__r.SVMXC__Site__r.SVMXC__State__c,
                                                                ProductSystem__r.SVMXC__Site__r.SVMXC__City__c,
                                                                ProductSystem__r.SVMXC__Site__r.SVMXC__Country__c ,
                                                                Contact.Name,Subject,
                                                                Was_anyone_injured__c,
                                                                Owner.title,
                                                                Contact.Email,
                                                                ProductSystem__r.SVMXC__Product__r.Name,
                                                                /*SVMXC__Top_Level__r.Product_Version_Build__r.Name, 
                                                                Wave 1C - Nikita 1/25/2015*/
                                                                Original_Owner__c,
                                                                Is_This_a_Complaint__c,
                                                                Account.District_Manager__c,
                                                                Is_escalation_to_the_CLT_required__c ,
                                                                Account.Name ,
                                                                Account.ShippingCity,Account.ShippingCountry,
                                                                Account.ShippingState,
                                                                Account.BillingCity,Account.BillingCountry,
                                                                Account.BillingState,
                                                                Employee_Phone__c,
                                                                Phone_Number__c,
                                                                //ProductSystem__r.Name,

                                                                Account.District__c,
                                                                City2__c,
                                                                 Contact.Title,                                
                                                                SVMXC__Product__r.Name,
                                                                CreatedBy.Phone,
                                                                Device_or_Application_Name2__c ,
                                                                E_mail_Address__c ,PCSN__c,
                                                                PCSN2__c ,
                                                                Service_Pack__c,Service_Pack2__c,
                                                                CaseNumber ,
                                                                ProductSystem__r.name,
                                                                State_Province2__c,
                                                                Title_Role__c,
                                                                Version_Mode__c,Version_Model2__c 
                                                                FROM Case where id = :CaseIds]
                                                             );
       

    for(L2848__c objL2848 : listL2848)
    {   
        //objL2848 = new L2848__c();
        
        if(mapCaseWithRelObject.containsKey(objL2848.Case__c) && mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingCity != Null && mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingCountry != Null && mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingState != Null)
        {
            objL2848.City__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingCity;
            objL2848.Country__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingCountry;
            objL2848.State_Province__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.ShippingState;
        }
        else
        {
            objL2848.City__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.BillingCity;
            objL2848.Country__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.BillingCountry;
            objL2848.State_Province__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.BillingState;               
        } 

        system.debug('CaseId>>'+mapCaseWithRelObject.get(objL2848.Case__c).Id);
        //system.debug('CaseId>>>>>>>>>>'+mapCaseWithRelObject.get(objL2848.Case__c).Account.CSS_District__c);
        //objL2848.Case__c = mapCaseWithRelObject.get(objL2848.Case__c).Id;
        objL2848.Device_or_Application_Name__c = mapCaseWithRelObject.get(objL2848.Case__c).ProductSystem__r.SVMXC__Product__r.Name; //Case__r.Product.Name;
        objL2848.Device_or_Application_Name2__c = mapCaseWithRelObject.get(objL2848.Case__c).Device_or_Application_Name2__c ;
        objL2848.District_or_Area__c = mapCaseWithRelObject.get(objL2848.Case__c).ProductSystem__r.Service_Team__r.SVMXC__Group_Code__c; 
        objL2848.E_mail_Address__c = mapCaseWithRelObject.get(objL2848.Case__c).Contact.Email;          
        objL2848.Employee_Phone__c = mapCaseWithRelObject.get(objL2848.Case__c).CreatedBy.Phone;
        objL2848.Name_of_Customer_Contact__c = mapCaseWithRelObject.get(objL2848.Case__c).Contact.Name;        
        objL2848.PCSN__c = mapCaseWithRelObject.get(objL2848.Case__c).ProductSystem__r.Name;
        objL2848.PCSN2__c = mapCaseWithRelObject.get(objL2848.Case__c).PCSN2__c ;
        objL2848.Phone_Number__c = mapCaseWithRelObject.get(objL2848.Case__c).Contact_Phone__c;
        objL2848.Service_Pack__c = mapCaseWithRelObject.get(objL2848.Case__c).Service_Pack__c;
        objL2848.Service_Pack2__c = mapCaseWithRelObject.get(objL2848.Case__c).Service_Pack2__c;
        objL2848.Service_Request_Notification__c = mapCaseWithRelObject.get(objL2848.Case__c).CaseNumber ;
        objL2848.Site_Name__c = mapCaseWithRelObject.get(objL2848.Case__c).Account.Name;
        objL2848.Title_Department__c = mapCaseWithRelObject.get(objL2848.Case__c).owner.Title;
        objL2848.Title_Role__c = mapCaseWithRelObject.get(objL2848.Case__c).Contact.Title;
        objL2848.Varian_District_Manager_for_Site__c = mapCaseWithRelObject.get(objL2848.Case__c).ProductSystem__r.Service_Team__r.District_Manager__r.Name;//varCase.Account.Territory_Team__r.CSS_District_Manager__r.Name
        objL2848.Varian_Employee_Receiving_Info__c = mapCaseWithRelObject.get(objL2848.Case__c).Original_Owner__c;
        // objL2848.Version_Model__c = mapCaseWithRelObject.get(objL2848.Case__c).ProductSystem__r.Product_Version_Build__r.Name; // Wave 1C - Nikita 1/25/2015
        objL2848.Version_Model2__c = mapCaseWithRelObject.get(objL2848.Case__c).Version_Model2__c;
        objL2848.When_Where_Who_What__c = mapCaseWithRelObject.get(objL2848.Case__c).Description;
        objL2848.Complaint_Short_Description__c = mapCaseWithRelObject.get(objL2848.Case__c).Subject;
        //objL2848.Date_of_Event__c = System.Today();
        //objL2848.Date_Reported_to_Varian__c = System.Today();
        objL2848.Was_anyone_injured__c = mapCaseWithRelObject.get(objL2848.Case__c).Was_anyone_injured__c;        


    }   



}
/*************************************************************************\
    @ Author        : Amit kumar
    @ Date      : 14-May-2013
    @ Description   : This Trigger is used to add Portal UsersIn Group on MarketingKitRole .
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

trigger MarketingKit_AddPortalUserInGroup on MarketingKitRole__c (after insert) {
    
    //if(trigger.Isinsert){
        //CpProductPermissions.AddPortalUserInGroup(trigger.newmap.keyset());
    //}
    set<id> productids = new set<id>();
    map<id,String> productmodelpartnumber = new map<id,String>();
    set<String> Modelpart = new set<String>();
    map<String,Set<String>> mapofpartnumbernprd = new map<String,Set<String>>();
    Set<MarketingKitRole__c> listtoinsert = new Set<MarketingKitRole__c>();
    set<String> accountproduct = new set<string>();
    set<id> accid = new set<id>();
    for(MarketingKitRole__c mkt: trigger.new)
    {
      productids.add(mkt.product__c);
      accid.add(mkt.account__c);      
    }
    for(Product2 p:[Select ProductCode from product2 where id in: productids])
    {
      productmodelpartnumber.put(p.id,p.ProductCode);
      Modelpart.add(p.ProductCode);
    }
    for(product2 prt: [Select id,productcode from product2 where productcode in:Modelpart and id not in: productids])
    {
      Set<String> productlist = new set<String>();
      if(mapofpartnumbernprd.containskey(prt.productcode))
      {
        
        productlist = mapofpartnumbernprd.get(prt.productcode);
        productlist.add(prt.id);
        mapofpartnumbernprd.put(prt.productcode,productlist);
      }else{
        productlist.add(prt.id);
        mapofpartnumbernprd.put(prt.productcode,productlist);
      }
    }
    for(MarketingKitRole__c mkrt: trigger.new)
    {
      if(mkrt.product__c != null)
      {
       if(productmodelpartnumber.get(mkrt.Product__c) != null)
       {
         if(mapofpartnumbernprd.get(productmodelpartnumber.get(mkrt.Product__c)) != null)
         {
          for(String prodid : mapofpartnumbernprd.get(productmodelpartnumber.get(mkrt.Product__c)))
          {
          MarketingKitRole__c relatedmrkt = new MarketingKitRole__c();
          relatedmrkt.Account__c = mkrt.Account__c;
          relatedmrkt.Product__c = prodid;
          listtoinsert.add(relatedmrkt);
          }
         }
       }
      }
    }
    for(MarketingKitRole__c mktt : [Select Account__c,product__c from MarketingKitRole__c where account__c in : accid])
    {
      String accproductcom = mktt.Account__c + '---' + mktt.product__c;
      accountproduct.add(accproductcom);
    }
    List<MarketingKitRole__c> listtoinsertfromset = new list<MarketingKitRole__c>();
    listtoinsertfromset.addall(listtoinsert);
    for(MarketingKitRole__c s: listtoinsertfromset)
    {
       String str = s.Account__c + '---' + s.Product__c;
       if(accountproduct.contains(str))
       {
        listtoinsert.remove(s);
       }
    }
    if(listtoinsert.size() > 0)
    {
     system.debug('List to be inserted-----' + listtoinsert);
     listtoinsertfromset.clear();
     listtoinsertfromset.addall(listtoinsert);
     insert listtoinsertfromset;
    }
    
}
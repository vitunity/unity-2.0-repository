trigger MarketingKit_preventdupe on MarketingKitRole__c (before insert) {
	
	Set<Id> AccountIds = new Set<Id>();
	map<Id,Set<String>> rectoinsert = new Map<Id,Set<String>>();
	
	for(MarketingKitRole__c mkt: trigger.new){
    	AccountIds.add(mkt.account__c);      
	}
	
	for (MarketingKitRole__c mkit: [Select Account__c,Product__c from MarketingKitRole__c where Account__c in :AccountIds]){
		Set<String> mklists = new Set<String>();  
      	if(rectoinsert.containskey(mkit.Account__c)){
      		mklists = rectoinsert.get(mkit.Account__c);
      	}
    	mklists.add(mkit.Product__c);
      	rectoinsert.put(mkit.Account__c,mklists); 
    }
    
    system.debug('++++++++++++++++++++++++rectoinsert'+rectoinsert);
  
    for(MarketingKitRole__c m:Trigger.new){
    	if(rectoinsert.containskey(m.Account__c) && rectoinsert.get(m.Account__c).contains(m.Product__c)){
        		m.adderror('Duplicate record. A market kit role record already exists for this Account and Market Kit');
        }
    	
    }
       
        
        
       
}
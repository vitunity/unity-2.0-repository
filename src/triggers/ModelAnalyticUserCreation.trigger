/**************************************************************************\
    @ Author        : Rakesh Basani
    @ Date      : 08-Aug-2017
    @ Description   : STSK0012079 - MyVarian User with Eclipse access should have Model Analytics access.
    Change Log:
    Date/Modified By Name/Task or Story or Inc # /Description of Change
    14-Nov-2017 - Rakesh - STSK0013441 - Added Queueable Interface to fix HTTP callout issue.
****************************************************************************/

trigger ModelAnalyticUserCreation on User (after insert,after update) {

    set<Id> stModelAnalyticsId = new set<Id>();
    set<Id> stModelAnalyticsInactivate = new set<Id>();
    
    //for creating user
    if(Trigger.isInsert){
        for(user u: trigger.new){
            if(u.contactid != null){
                if(u.IsActive == True && u.is_Model_Analytic_Member__c)
                    stModelAnalyticsId.add(u.contactId);                    
            }  
        }
    }
    
    //User update and user is made active          
    if(trigger.IsUpdate){for(user u: trigger.new){ if(u.contactid !=null){if(u.IsActive && u.is_Model_Analytic_Member__c && (Trigger.oldMap.get(u.id).IsActive != u.IsActive || Trigger.oldMap.get(u.id).is_Model_Analytic_Member__c != u.is_Model_Analytic_Member__c))stModelAnalyticsId.add(u.contactId); if((!u.IsActive && Trigger.oldMap.get(u.id).IsActive != u.IsActive) || (!u.is_Model_Analytic_Member__c && Trigger.oldMap.get(u.id).is_Model_Analytic_Member__c != u.is_Model_Analytic_Member__c ))stModelAnalyticsInactivate.add(u.contactId); }}
    }
    if(stModelAnalyticsId.size() > 0)
    {
        ID jobID = System.enqueueJob(new ModelAnalytics(stModelAnalyticsId,'PUT'));
    }
    if(stModelAnalyticsInactivate.size() > 0 )
    {
        ID jobID = System.enqueueJob(new ModelAnalytics(stModelAnalyticsInactivate,'DELETE'));
    }       
}
//
// (c) 2014 Appirio, Inc.
//
// Trigger for OCSUGC_CollaborationGroupInfo__c object
//
// Nov 14, 2014   Ajay Gautam (Appirio)   Original [ Ref: T-334411 ]


trigger OCSUGC_CollaborationGroupInfoTrigger on OCSUGC_CollaborationGroupInfo__c (after update) {

    if(Trigger.isAfter && Trigger.isUpdate) {
        OCSUGC_CollaborationGroupInfoTriggerMgmt.afterUpdate(Trigger.new,Trigger.oldMap);
    }

}
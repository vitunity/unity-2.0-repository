/*
Name        : FeedCommentAfterInsertUpdateDelete
Updated By  : Manmeet Manethiya (Appirio India)
Date        : 9th July, 2014
Purpose     :
*/

trigger OCSUGC_CollaborationGroupMemberAfterInsertUpdateDelete on CollaborationGroupMember (after delete, after insert, after update) {
    if(Trigger.isInsert){
        OCSUGC_ChatterManagement.insertIntranetNotificationRecordsForChatterGroup(Trigger.newMap);
    }else if(Trigger.isDelete){

    }
}
trigger OCSUGC_CollaborationGroupTrigger on CollaborationGroup (after update, after delete) {

    //Updating CollaborationGroup
    if(trigger.isUpdate) {
        //Sending email notification to group members
        OCSUGC_ChatterManagement.sendEmailNotificationToGroupMembers(trigger.newMap, trigger.oldMap);
    }

    //Deleting CollaborationGroup
    if(trigger.isDelete) {
        //Deleting CollaborationGroup's other details (T-326265)
        OCSUGC_ChatterManagement.deleteGroup(trigger.oldMap);
    }
}
/*
Name        : OCSUGC_FeedCommentAfterInsertUpdateDelete
Updated By  : Manmeet Manethiya (Appirio India)
Date        : 9th July, 2014
Purpose     :
*/
trigger OCSUGC_FeedCommentAfterInsertUpdateDelete on FeedComment (after delete, after insert, after update) {

  if(Trigger.isInsert){
    OCSUGC_ChatterManagement.insertIntranetNotificationRecordsForFeedComments(Trigger.newMap);
  }else if(Trigger.isDelete && Trigger.isAfter){
    OCSUGC_ChatterManagement.updateNumberOfComments((List<FeedComment>)Trigger.old);
  }
}
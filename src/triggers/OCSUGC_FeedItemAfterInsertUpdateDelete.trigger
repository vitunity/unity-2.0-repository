/*
Name        : FeedItemAfterInsertUpdateDelete
Updated By  : Manmeet Manethiya (Appirio India)
Date        : 9th July, 2014
Purpose     : 
*/
trigger OCSUGC_FeedItemAfterInsertUpdateDelete on FeedItem (after delete, after insert, after update) {
    if(Trigger.isUpdate){
        //OCSUGC_ChatterManagement.insertIntranetNotificationRecordsForFeedComments(Trigger.newMap);
    }else if(Trigger.isDelete){
        
    }
}
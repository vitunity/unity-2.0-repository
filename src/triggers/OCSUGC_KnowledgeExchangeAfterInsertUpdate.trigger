/*
Name        : OCSUGC_KnowledgeExchangeAfterInsertUpdate
Updated By  : Manmeet Manethiya (Appirio India)
Date        : 25th June, 2014
Purpose     :
Refrence    : T-289387
*/
trigger OCSUGC_KnowledgeExchangeAfterInsertUpdate on OCSUGC_Knowledge_Exchange__c (after insert, after update) {
        
    if(Trigger.isUpdate){
        OCSUGC_KnowledgeExchangeManagement.shareArtifactWithFroup(Trigger.newMap, Trigger.oldMap); 
    }    
}
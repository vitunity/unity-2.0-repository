trigger OCSUGC_UserTrigger on User (before insert, before update, after insert, after update) {

    if(system.Label.User_Triggers_Flag == 'True')
     {
        if(trigger.isBefore) {
            OCSUGC_UserTriggerHandler.onBeforeInsertUpdate(trigger.new, trigger.isInsert, trigger.oldMap, trigger.newMap);
        } else {
            OCSUGC_UserTriggerHandler.onAfterInsertUpdate(trigger.newMap, trigger.oldMap);
        }
     }
}
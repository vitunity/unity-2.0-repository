/*************************************************************************\
      @ Author          : Sunil Bansal
      @ Date            : 04-Apr-2013
      @ Description     : This Trigger is use apex Class 'DefaultSalesTeam' to add Opportunity Sales team associated with an account.
      @ Last Modified By      :     
      @ Last Modified On      :     
      @ Last Modified Reason  :    
****************************************************************************/

trigger OpportunityMasterTrigger_AfterTrigger on Opportunity (after insert, after update) 
{
    
 /*   // List of Opportunites 
    List<Opportunity> opptyList = new List<Opportunity>();

    // When Opportunity is created, check if Account is associated with an opportunity, put all the Opportunities in  opptyList 
    if(Trigger.isInsert && Trigger.isAfter)
    {
        for(Opportunity opp: Trigger.New)
        {
            if(opp.AccountId != null)
                opptyList.add(opp);
        }
    }
    
    // When Opportunity is updated, check if Account is associated with an opportunity, put all the Opportunities in opptyList
    if(Trigger.isUpdate && Trigger.isAfter)
    {
    
       System.debug('Aebug = '+Trigger.New);
        for(Opportunity opp: Trigger.New)
        {
     
            String AllowSalesTeamFromTerritory = 'False';
            try
            {
                Map<String, KeyValueMap_CS__c> keyValueMap = KeyValueMap_CS__c.getAll();
                AllowSalesTeamFromTerritory = keyValueMap.get('AllowSalesTeamFromTerritory').Data_Value__c;
            }
            catch(Exception ex)
            {
                // do nothing
            }   
            
            Opportunity oldOpp = Trigger.oldMap.get(opp.Id);
          //  if(opp.AccountId != null && opp.AccountId != oldOpp.AccountId)
              if((opp.AccountId != null && opp.AccountId != oldOpp.AccountId) || AllowSalesTeamFromTerritory == 'True')
                opptyList.add(opp);
       
        }
    }

    // call apex class function to finally add sales team
    DefaultSalesTeam objSalesTeam = new DefaultSalesTeam();
    objSalesTeam.addSalesteam(opptyList);
    objSalesTeam.changeopportunityOwner(opptyList);
   */
}
/*************************************************************************\
      @ Author          : Megha Arora
      @ Date            : 13-May-2013
      @ Description     : This trigger calls the class 'DefaultSalesTeam' to add sales team in Opportunity.
      @ Last Modified By      :     
      @ Last Modified On      :    
      @ Last Modified Reason  :     Remove this trigger
/****************************************************************************/

trigger OpportunityMasterTrigger_BeforeTrigger on Opportunity (before insert, before update) 
{
 /* List<Opportunity> opptyList = new List<Opportunity>();

    if(Trigger.isInsert && Trigger.isbefore)
    {
        for(Opportunity opp: Trigger.New)
        {
            if(opp.AccountId != null)
                opptyList.add(opp);
        }
    }
    
    // When Opportunity is updated, check if Account is associated with an opportunity, put all the Opportunities in opptyList
    if(Trigger.isUpdate && Trigger.isAfter)
    {
    
       System.debug('Aebug = '+Trigger.New);
        for(Opportunity opp: Trigger.New)
        {
     
            Opportunity oldOpp = Trigger.oldMap.get(opp.Id);
          if(opp.AccountId != null && opp.AccountId != oldOpp.AccountId)
           // if(opp.AccountId != null || opp.AccountId != oldOpp.AccountId || AllowSalesTeamFromTerritory == 'True')
                opptyList.add(opp);
       
        }
    }
    
   /* Change Opportunity Owner of when inserted */
   
  /*  DefaultSalesTeam objSalesTeam1 = new DefaultSalesTeam();
    objSalesTeam1.changeopportunityOwner(opptyList); */
}
/*************************************************************************\
      @ Author          : Ajinkya Raut  
      @ Date            : 15-DEC-2016
      @ Description     : This trigger runs before insert and update.
      @ Last Modified By      :     
      @ Last Modified On      :    
      @ Last Modified Reason  :    
/****************************************************************************/

trigger Opportunity_BeforeTrigger on Opportunity (before insert, before update,before delete) 
{
    if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isbefore){
        OpportunityTriggerHandler oppTH = new OpportunityTriggerHandler();
        oppTH.checkExpectedDeliveryDate(Trigger.new,Trigger.oldMap);   
    }
    //CM : Apr 25: Added for Inc #  INC4557251
    if(Trigger.isDelete && Trigger.isBefore)
    {
      for(Opportunity opp : [SELECT Id, (Select Id from BigMachines__BigMachines_Quotes__r) FROM Opportunity WHERE Id in:Trigger.oldmap.keyset()])
      {
        if(opp.BigMachines__BigMachines_Quotes__r != null && opp.BigMachines__BigMachines_Quotes__r.size()>0)
        {
          Opportunity oppObj = Trigger.oldMap.get(opp.Id);
          oppObj.adderror('Please move or delete all associated quotes before deleting the opportunity');
        }
      }
    }
}
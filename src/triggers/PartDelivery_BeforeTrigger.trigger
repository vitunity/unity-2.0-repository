/*************************
Created by :    Nikita Gupta
Created On :    14-feb-2014
created for:    Before trigger on Part Delivery
Last Modified By    :   Shubham Jaiswal
Last Modified Date  :   3/27/14
Last Modified Reason:   US4377, to populate Sales Order Item field using ERP Sales Order Item field
*************************/

trigger PartDelivery_BeforeTrigger on Part_Delivery__c (before insert, before update) 
{
    SR_Class_PartDelivery objPD = new SR_Class_PartDelivery();
    objPD.updatePartDeliveryFields(trigger.new, trigger.oldMap); //By Nikita, US4080, code moved from method updateSalesOrderItem, code optimization
}
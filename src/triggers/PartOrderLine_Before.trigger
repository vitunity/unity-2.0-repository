/*************************************************************
Last Modified By: Parul Gupta
Last Modified Date: 01/08/2014
Last Modified Reason: US1964, Update Product Serviced from ERP PCSN
*************************************************************/

trigger  PartOrderLine_Before on SVMXC__RMA_Shipment_Line__c (before Insert, before update) 
{
    SR_PartOrderLine objPOL = new SR_PartOrderLine();
    objPOL.PopulateSerialNumberOnPOL(trigger.new, Trigger.oldMap);//Code added to method, US1964, 01/08/2014
    //objPOL.updatePOLRemainingQty(trigger.new, trigger.oldMap); //pszagdaj/FF, 11/19/2015, DE6641 commented by amit
    //we might need to remove above method from class as well because its unused.    
    //objPOL.updatePOLRemainingQtyNew(trigger.new); // added by amit moved to after trigger
    
    //INC4421418 / DFCT0012048 - free issue parts failed 
    for (SVMXC__RMA_Shipment_Line__c pol : Trigger.new) {
        //if (trigger.isUpdate && pol.SVMXC__Line_Status__c != 'Completed' && pol.Consumed__c == true && Trigger.oldMap.get(pol.Id).Consumed__c == false) { //DE7916
        //    pol.addError('Consumed cannot be changed to true when Line Status is not Completed');
        //}
        if(pol.Consumed__c){
            pol.SVMXC__Line_Status__c = 'Completed';
        }
    }
}
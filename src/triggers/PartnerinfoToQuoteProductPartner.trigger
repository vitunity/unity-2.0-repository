trigger PartnerinfoToQuoteProductPartner on BigMachines__Quote_Product__c (after insert, after update, before insert, before update) {
    
    if(trigger.isAfter){    
        QuoteProductTriggerhandler.createQuoteProductPartners(trigger.oldMap);
    }else{
        QuoteProductTriggerhandler.initialize(trigger.new);
        QuoteProductTriggerhandler.updateERPSitePartner();
        QuoteProductTriggerhandler.updateForecastBookingValue(trigger.new);
    }
    
     if(!AutoCreateBillingPlan.STOP_BILLING_PLAN_CRERATION){
       if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
          AutoCreateBillingPlan.createSubscriptionLines(trigger.new,trigger.oldMap);
       }
       if(trigger.isBefore && trigger.isInsert){
         AutoCreateBillingPlan.updateGroupSequence(trigger.new);
       }
    }
    
    if(trigger.isAfter && trigger.isInsert){
      AutoRenewSaaSSubscription.closeSaasSubscription(trigger.new);
    }
}
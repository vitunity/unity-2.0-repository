/*************************************************************************\
Created By              :   Nikita Gupta
Created On              :   3/19/2014
Created Reason          :   To call methods to be executed in an after trigger on Parts Order Line
Last Modified By        :   Parul Gupta
Last Modified Date      :   5/23/2014
Last Modified Reason    :   US4481, to update fields on Parts Delivery related object based on recieved qty
Last Modified By        :   Parul Gupta
Last Modified On        :   9/04/2014
Last Modified Reason    :   US4291, removed code for US4073
****************************************************************************/

trigger PartsOrderLine_AfterTrigger on SVMXC__RMA_Shipment_Line__c (after insert, after update, after delete, after undelete) 
{
    if(SR_PartOrderLine.UPDATE_POL_FROM_POL == true){
        return;
    }
    
    if(Trigger.isInsert || Trigger.isUpdate){
        SR_PartOrderLine varClass = new SR_PartOrderLine(); //Initiate Class of Parts Order
        varClass.updatePOLAndRelatedWD(trigger.new, trigger.oldMap);    //method call for US4422, 26/09/2014
        SR_PartOrderLine.completePOwhenAllPOLCompleted(trigger.new, Trigger.oldMap); //jkondrat.FF 11-19-15 DE6711
        varClass.updatePOLRemainingQtyNew(trigger.new);
        varClass.updateReturnUsedSWOTOWDL(trigger.new, trigger.oldMap);
        /********************************* MAP TO  UPDATE PARTS ORDER FORM PARTS ORDER LINE *******************************************************/
        if(SR_PartOrderLine.mapUpdatePartsOrderFromPartsOrderLine.size() > 0){
            update SR_PartOrderLine.mapUpdatePartsOrderFromPartsOrderLine.values();
            SR_PartOrderLine.mapUpdatePartsOrderFromPartsOrderLine = new Map<Id, SVMXC__RMA_Shipment_Order__c>();
        }
        /********************************* MAP TO  UPDATE WORK DETAIL FORM PARTS ORDER LINE *******************************************************/
        if(SR_PartOrderLine.mapUpdateWorkDetailFromPartsOrderLine.size() > 0){
            update SR_PartOrderLine.mapUpdateWorkDetailFromPartsOrderLine.values();
            SR_PartOrderLine.mapUpdateWorkDetailFromPartsOrderLine = new Map<Id, SVMXC__Service_Order_Line__c>();
        }
        /****************************** MAP TO UPDATE MASTER ORDER LINE FORM PARTS ORDER LINE *****************************************************/
        if(SR_PartOrderLine.mapUpdateMasterOrderLine.size() > 0){
            update SR_PartOrderLine.mapUpdateMasterOrderLine.values();
            SR_PartOrderLine.mapUpdateMasterOrderLine = new Map<Id, SVMXC__RMA_Shipment_Line__c>();
        }
    }else if(Trigger.isDelete){
        SR_PartOrderLine varClass = new SR_PartOrderLine();
        varClass.updatePOLRemainingQtyNew(trigger.old);
    }else if(Trigger.isUndelete){
        SR_PartOrderLine varClass = new SR_PartOrderLine();
        varClass.updatePOLRemainingQtyNew(trigger.new);
    }

}
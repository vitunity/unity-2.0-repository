/********************************************************************
Last Modified By        : Parul Gupta
Last Modified Date      : 09/02/2014
Last Modified Reason    : US4291, Create/Update Product Stock and Stock History
********************************************************************/

trigger PartsOrder_AfterTrigger  on SVMXC__RMA_Shipment_Order__c  (after insert,after update)
{
    /******** Initiating class **********/
    SR_classPartsOder clsPO = new SR_classPartsOder();
    /************* Record Type Id **************/
    Id recordTypeIdRMA = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('RMA').getRecordTypeId();
    Id recordTypeIDShpmnt = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
    Set<Id> setOfPoId = new Set<Id>();
    List<SVMXC__RMA_Shipment_Order__c> lstOfPO = new List<SVMXC__RMA_Shipment_Order__c>();
    Map<Id, ProcessInstanceWorkitem> MapOfObjIdAndWoItem = new Map<Id, ProcessInstanceWorkitem>();
    List<SVMXC__RMA_Shipment_Order__c> poNewRMA = new List<SVMXC__RMA_Shipment_Order__c>(); //savon.FF 1-27-2016 DE7017

    for (SVMXC__RMA_Shipment_Order__c  POrder: Trigger.new){
        //savon.FF 1-27-2016 DE7017
        
        system.debug('rma number provided b yboomi ====='+POrder.RMA_Number__c+'===User==='+Userinfo.getUsername());
        system.debug('rma number provided b SVMXC__Order_Status__c ====='+POrder.SVMXC__Order_Status__c +'===User==='+Userinfo.getUsername());
        system.debug('rma number provided b Interface_Status__c====='+POrder.Interface_Status__c+'===User==='+Userinfo.getUsername());
        system.debug('rma number provided b yboomi porder====='+POrder+'===User==='+Userinfo.getUsername());
        if(Trigger.oldMap!= null && Trigger.oldMap.get(POrder.Id)!= null)
        system.debug('rma number provided b yboomi old ====='+Trigger.oldMap.get(POrder.Id).RMA_Number__c+'===User==='+Userinfo.getUsername());
        set<String> orderTypes = new set<String>{'Transfer','Return Unused'};
        if(POrder.RMA_Number__c != null && 
          (Trigger.oldMap.get(POrder.Id).RMA_Number__c == null || 
           POrder.RMA_Number__c != Trigger.oldMap.get(POrder.Id).RMA_Number__c)
           && recordTypeIdRMA == POrder.RecordTypeId
           && orderTypes.contains(POrder.SVMXC__Order_Type__c)) {
            poNewRMA.add(POrder);
        }
        
        If(Trigger.isUpdate){
             //to trigger PO approval    //if condition changed for DE2296
            //pszagdaj/FF, 12/09/2015, DE6461 - new PO approval entry condition
            SVMXC__RMA_Shipment_Order__c oldPOrder =  trigger.oldMap.get(POrder.ID);
            Boolean amntPOLimitChanged =  (oldPOrder.Amount_Over_Parts_Order_Limit__c != POrder.Amount_Over_Parts_Order_Limit__c);
            Boolean dmOverrideChanged =  (oldPOrder.District_Manager_Override__c != POrder.District_Manager_Override__c);
            Boolean dmApprReqChanged =  (oldPOrder.DM_Approval_Required__c != POrder.DM_Approval_Required__c);
            Boolean partExcTLPriceChanged =  (oldPOrder.Partline_Exceeding_Total_Line_Price__c != POrder.Partline_Exceeding_Total_Line_Price__c);
            Boolean pseApprReqChanged =  (oldPOrder.PSE_Approval_Required__c != POrder.PSE_Approval_Required__c);
            Boolean subLineCntChanged =  (oldPOrder.Submitted_Line_Count__c != POrder.Submitted_Line_Count__c);
            Boolean statusChanged =  (oldPOrder.SVMXC__Order_Status__c != POrder.SVMXC__Order_Status__c);
            Boolean timeLimitExpChanged =  (oldPOrder.Time_Limit_Expired__c != POrder.Time_Limit_Expired__c);
            Boolean totalLineCntChanged =  (oldPOrder.Total_Line_Count__c != POrder.Total_Line_Count__c);
            if ((POrder.SVMXC__Order_Status__c == 'Pending Approval' && Porder.RecordTypeId == recordTypeIDShpmnt && POrder.Time_Limit_Expired__c && !POrder.District_Manager_Override__c && (statusChanged || dmOverrideChanged || timeLimitExpChanged))
                || (POrder.SVMXC__Order_Status__c == 'Submitted' && Porder.RecordTypeId == recordTypeIDShpmnt && (POrder.PSE_Approval_Required__c > 0 || POrder.DM_Approval_Required__c || POrder.Partline_Exceeding_Total_Line_Price__c > 0 || POrder.Amount_Over_Parts_Order_Limit__c > 0) && Porder.Submitted_Line_Count__c > 0 && (statusChanged || pseApprReqChanged || dmApprReqChanged || partExcTLPriceChanged || amntPOLimitChanged ||subLineCntChanged || totalLineCntChanged))
                || (((POrder.SVMXC__Order_Status__c == 'Submitted' && Porder.RecordTypeId == recordTypeIDShpmnt && !POrder.DM_Approval_Required__c && POrder.PSE_Approval_Required__c == 0 && !POrder.District_Manager_Override__c && !POrder.Time_Limit_Expired__c) 
                     || (POrder.SVMXC__Order_Status__c == 'PSE Approval Pending' && Porder.RecordTypeId == recordTypeIDShpmnt && POrder.District_Manager_Override__c && Porder.Submitted_Line_Count__c > 0) 
                     || (POrder.SVMXC__Order_Status__c == 'Submitted' && Porder.RecordTypeId == recordTypeIdRMA)) 
                    && (statusChanged || dmApprReqChanged || pseApprReqChanged || dmOverrideChanged || timeLimitExpChanged || subLineCntChanged || totalLineCntChanged) ) 
                )
            {
                lstOfPO.add(POrder);
                setOfPoId.add(POrder.id);
            }
        }
    }

    //priit, US4354,US4914, US4164 - to automate approval process for Parts Order
    system.debug('>>> lstOfPO.size(): ' + lstOfPO.size());
    if(!lstOfPO.isEmpty() && Trigger.isUpdate){
        PartsOrderAutoApproval.execute(lstOfPO,setOfPoId);
    }
    
    if(Trigger.isUpdate){
        if(PartsOrderAutoApproval.partsOrderApprovedFlag){
            PartsOrderAutoApproval.partsOrderApprovedFlag = false;
            PartsOrderAutoApproval.updatePOLIStatusToApproved(trigger.new, trigger.oldMap);
        }
        if(PartsOrderAutoApproval.partsOrderRejectedFlag){
            PartsOrderAutoApproval.partsOrderRejectedFlag = false;
            PartsOrderAutoApproval.updatePOLIStatusToRejected(trigger.new, trigger.oldMap);
        }
    }
    
    
    /*********** Method Calls *******************/
    //savon.FF 1-27-2016 DE7017
    if (!poNewRMA.isEmpty()) {
        POProductStockHistoryController pops = new POProductStockHistoryController();
        pops.createProductStockHistory(poNewRMA);   
        //clsPO.createProductStockStockHistory(poNewRMA, trigger.oldMap); //US4291 //savon.FF 1-27-2016 Changed from Trigger.new to poNewRMA
        // above code commented duplicate id issue.
        // refactored by amit
    }
    
    clsPO.updatePOLFromPOAfterTrigger(trigger.new, trigger.oldMap); //savon.FF 12-22-2015 DE6748   
}
/********************************************
Last Modified By    :   Nikita Gupta
Last Modified Date  :   3/14/14
Last Modified Reason:   US3749, to populate 'ERP Delivery Block' based on Country Country Rule record.
Last Modified By    :   Nikita Gupta
Last Modified Date  :   3/20/14
Last Modified Reason:   US3047, to populate 'Service Engineer' on Parts Order with Work Order's Technician's Salesforce User
Last Modified By    :   Nikita Gupta
Last Modified Date  :   3-April-14
Last Modified Reason:   US3749, to populate To Country and From Country with To Location.Country and From Location.Country respectively.
Last Modified By    :   Nikita Gupta
Last Modified Date  :   11-April-14
Last Modified Reason:   US4347, commented code for 'To Location' to 'To Country'.
Last Modified Date  :   6-May-14
Last Modified Reason:   US4502 , to populate sales order name field in partsorder look up .
Last Modified By    :   Priti Jaiswal
Last Modified Date  :   27-June-14
Last Modified Reason:   US4378 - to update Transfer Inventory Location on Parts Order 
Last Modified By    :   Priti Jaiswal
Last Modified Date  :   30 -July-14
Last Modified Reason:   DE874 - commented code that updates To Location On Parts Order from Work Order's Inventory Location
Last Modified By    :   Mili Sudhakar
Last Modified Date  :   31 -July-14
Last Modified Reason:   US4354- Added code to call the class PartsOrderApprovalTime which updates the approval time limit and approval exceed time limit
********************************************/

trigger PartsOrder_BeforeTrigger on SVMXC__RMA_Shipment_Order__c(before update,before insert) 
{

    Id rma = RecordTypeHelperClass.PARTS_ORDER_RECORDTYPES.get('RMA').getRecordTypeId(); //savon.FF 12-17-2015 DE6809 
    Id shipment = RecordTypeHelperClass.PARTS_ORDER_RECORDTYPES.get('Shipment').getRecordTypeId(); //savon.FF 12-17-2015 DE6809 

    if(trigger.isUpdate){
        //savon.FF 10-23-15 DE5987 US3100 
        //cdelfattore@forefront 11/5/2015 - DE6512
        for (SVMXC__RMA_Shipment_Order__c po : Trigger.new) {

            //savon.FF 12-17-2015 DE6809 added if statement so this block was only for shipment record types and added rma block below
            if (po.RecordTypeId == shipment) {
                if (po.SVMXC__Sales_Order_Number__c  != null && po.Interface_Error_Message__c == null) {  
                    po.ERP_Result_Status__c = 'SUCCESS';
                    trigger.newMap.get(po.Id).ERP_Result_Status__c = 'SUCCESS';
                }
                else if(po.SVMXC__Sales_Order_Number__c == null && po.Interface_Error_Message__c != null) { 
                    po.ERP_Result_Status__c = 'FAILURE';
                    trigger.newMap.get(po.Id).ERP_Result_Status__c = 'FAILURE';
                }
            }
            else if (po.RecordTypeId == rma) {
                if (po.RMA_Number__c  != null && po.Interface_Error_Message__c == null) { 
                    po.ERP_Result_Status__c = 'SUCCESS';
                    trigger.newMap.get(po.Id).ERP_Result_Status__c = 'SUCCESS';
                }
                else if(po.RMA_Number__c == null && po.Interface_Error_Message__c != null) { 
                    po.ERP_Result_Status__c = 'FAILURE';
                    trigger.newMap.get(po.Id).ERP_Result_Status__c = 'FAILURE';
                }
            }


            if (po.Interface_Error_Message__c != null) {
                po.ERP_Process_Message__c = po.Interface_Error_Message__c;
            }

            //savon.FF 11-2-2015 DE6569 US5092 
            //cdelfattore 11/4/2015 commente out else statement and added interface status == null to if statement
            //added if condition to fix defect DE6627
            if (po.SVMXC__Order_Status__c == 'Approved' && po.Interface_Status__c == null) {
                po.Interface_Status__c = 'Process';
            }
            /*else {
                po.Interface_Status__c = null;
            }*/
        }
    }

    //pszagdaj/FF, 2015/10/14, US5092/TA7659
    if (trigger.isInsert) {
        SR_classPartsOder.updateStatusWhenCancelFlagged(trigger.new);
        SR_classPartsOder.updateServiceTeamCoordinatorFields(trigger.new);

        //cdelfattore@forefront 10/20/2015 (mm/dd/yyyy) - US5148 TA7698
        SR_classPartsOder.setOwnerToQueueIfStatusIsFailure(trigger.new);

    } else if (trigger.isUpdate) {
        SR_classPartsOder.updateStatusWhenCancelFlagged(trigger.oldMap,trigger.newMap);
        SR_classPartsOder.updateServiceTeamCoordinatorFields(trigger.oldMap,trigger.newMap);

        //cdelfattore@forefront 10/20/2015 (mm/dd/yyyy) - US5148 TA7698
        SR_classPartsOder.setOwnerToQueueIfStatusIsFailure(trigger.oldMap,trigger.newMap);

        //cdelfattore@forefront 11/4/2015 (mm/dd/yyyy) - DE6632
        //JW@ff 11/16/15 - moved code to updateFieldsOnPartOrder to fix self-referencing issue
        //SR_classPartsOder.updatePartsOrderLine(trigger.oldMap, trigger.newMap);
    }
    
    /********initiate class of Parts Order*******************/
    SR_classPartsOder objPO = new SR_classPartsOder();
    objPO.updateToFromCountry(Trigger.new, Trigger.oldMap);  // Consolidated Method -Need review
    objPO.updateFieldsOnPartOrder(Trigger.new, Trigger.oldMap);


}
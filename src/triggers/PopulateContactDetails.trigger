/*************************************************************************\
    @ Author        : Harshita Sawlani
    @ Date      : 19-Apr-2013
    @ Description   : This trigger is used to Update Contact Details on Consignee.
    @ Last Modified By  :   Harshita Sawlani
    @ Last Modified On  :   29-May-2013
    @ Last Modified Reason  :   Removed some of the unwanted code
****************************************************************************/

trigger PopulateContactDetails on Consignee__c (after Insert, after Update) {
Map <id,Contact> RAQAContact = new Map<id,Contact>();
Set<id> Complaintids = new set<Id>();
Set<id> InstalledProdIDs = new Set<id>();
List<SVMXC__Installed_Product__c> IPStatusList = new List<SVMXC__Installed_Product__c>();


 if(trigger.IsAfter)
   {
    for(Consignee__c con: trigger.new)
      Complaintids.add(con.Complaint_Number__c);
     
    
    Map<id,Complaint__c> complaintmap2 = new Map<id,Complaint__c>([Select id,RA_Region__c,Language__c from Complaint__c where id in:Complaintids]);
    Map<id,Complaint__c> mapupdate = new Map<id,Complaint__c>();
    system.debug('-----------Mapupdate-----'+mapupdate);
    Map<id,List<Consignee__c>> consigneecomplmap1 = new Map<id,list<Consignee__c>>();
    
     List<Complaint__c> updatelist = new List<Complaint__c>();
     List<String> RA1 = new List<String>();
     Set<String> RA2 = new Set<String>();
     List<String> LG1 = new List<String>();
     Set<String> LG2 = new Set<String>();
 for (consignee__c r : trigger.new){
  if(r.RA_Region__c != null && r.Language__c != null){
 
   Complaint__c updaterec = new Complaint__c(id = r.Complaint_Number__c);
   System.debug('-------Complaint Number'+r.Complaint_Number__c);
   if (complaintmap2.get(r.Complaint_Number__c).RA_Region__c != null && complaintmap2.get(r.Complaint_Number__c).RA_Region__c!= null)
    {
      
       if (mapupdate.get(r.Complaint_Number__c) == null){
         if (!complaintmap2.get(r.Complaint_Number__c).RA_Region__c.contains(r.RA_Region__c)){
             updaterec.RA_Region__c = complaintmap2.get(r.Complaint_Number__c).RA_Region__c + ',' + r.RA_Region__c;
            }
       else
          updaterec.RA_Region__c = complaintmap2.get(r.Complaint_Number__c).RA_Region__c; 
       system.debug('----Multi Language----before if loop' + complaintmap2);
     if(r.multiple_language__c != null){
         system.debug('----Multi Language----' + r.multiple_language__c);
         List<String> languagelist = r.multiple_language__c.split(',');
         languagelist.add(r.Preferred_language__c);
         for(String eachlanguage: languagelist){
         system.debug('----Multi Language----' + r.multiple_language__c);
        if (!complaintmap2.get(r.Complaint_Number__c).Language__c.contains(eachlanguage)){
             updaterec.Language__c = complaintmap2.get(r.Complaint_Number__c).Language__c + ',' + eachlanguage;
            }
        else
            updaterec.Language__c = complaintmap2.get(r.Complaint_Number__c).Language__c; 
        }
     }else{
       if (!complaintmap2.get(r.Complaint_Number__c).Language__c.contains(r.language__c)){
             updaterec.Language__c = complaintmap2.get(r.Complaint_Number__c).Language__c + ',' + r.Language__c;
            }
        else
            updaterec.Language__c = complaintmap2.get(r.Complaint_Number__c).Language__c; 
     }
        mapupdate.put(updaterec.id,updaterec);
       }
       else
       {
        if(!mapupdate.get(r.Complaint_Number__c).RA_Region__c.contains(r.RA_Region__c)){
           updaterec.RA_Region__c = mapupdate.get(r.Complaint_Number__c).RA_Region__c + ',' + r.RA_Region__c;
           }else
           updaterec.RA_Region__c = mapupdate.get(r.Complaint_Number__c).RA_Region__c;
        if(!mapupdate.get(r.Complaint_Number__c).Language__c.contains(r.Language__c)){
           updaterec.Language__c = mapupdate.get(r.Complaint_Number__c).Language__c + ',' + r.Language__c;
           }else
               updaterec.Language__c = mapupdate.get(r.Complaint_Number__c).Language__c;
        mapupdate.put(updaterec.id,updaterec);
       }
   system.debug('-----------Mapupdate-----'+mapupdate);   
 }
 else 
  {
     system.debug('-----------Mapupdate-----'+mapupdate);
    if (!mapupdate.containskey(r.Complaint_Number__c)){
         
             updaterec.RA_Region__c = r.RA_Region__c;
             updaterec.Language__c = r.Language__c;
             mapupdate.put(updaterec.id,updaterec);
         
       }
    else
       {
        if(!mapupdate.get(r.Complaint_Number__c).RA_Region__c.contains(r.RA_Region__c)){
           updaterec.RA_Region__c = mapupdate.get(r.Complaint_Number__c).RA_Region__c + ',' + r.RA_Region__c;
            }
         else
           updaterec.RA_Region__c = mapupdate.get(r.Complaint_Number__c).RA_Region__c;
     if(r.multiple_language__c != null){
             system.debug('----Multi Language----' + r.multiple_language__c);
         List<String> languagelist = r.multiple_language__c.split(',');
         languagelist.add(r.Preferred_language__c);
         for(String eachlanguage: languagelist){
        if(!mapupdate.get(r.Complaint_Number__c).Language__c.contains(eachlanguage)){
           updaterec.Language__c = mapupdate.get(r.Complaint_Number__c).Language__c + ',' + eachlanguage;
           mapupdate.put(updaterec.id,updaterec);
           system.debug('MapLanguage-----' + updaterec.Language__c);
            }
         else{
            updaterec.Language__c = mapupdate.get(r.Complaint_Number__c).Language__c;
            mapupdate.put(updaterec.id,updaterec);
            system.debug('maplanguageelse----'+ updaterec.Language__c);
            }
        }
        }else{
          if(!mapupdate.get(r.Complaint_Number__c).Language__c.contains(r.Language__c)){
           updaterec.Language__c = mapupdate.get(r.Complaint_Number__c).Language__c + ',' + r.Language__c;
            }
         else
            updaterec.Language__c = mapupdate.get(r.Complaint_Number__c).Language__c;
         }
         mapupdate.put(updaterec.id,updaterec);
       }
  
   }
  
  }
 }
 
    system.debug('---- updatelist ------ ' + updatelist);
    system.debug('-----------Mapupdate-----'+mapupdate); 
     //update updatelist;
    update mapupdate.values();
 }
/*
 if(trigger.IsUpdate)
 {
   for(Consignee__c t: trigger.new)
    { 
     if ((trigger.oldMap.get(t.id).PCSN_PNT_Status__c != trigger.newMap.get(t.id).PCSN_PNT_Status__c)&&(t.PCSN_PNT_Status__c =='Admin Completed'))
     {
         InstalledProdIDs.add(t.Installed_Product__c);
     }
     if(InstalledProdIDs != null)
     {
         for(id d: InstalledProdIDs)
         {
              SVMXC__Installed_Product__c P = new SVMXC__Installed_Product__c(id=d);
              P.SVMXC__Status__c = 'Scrap';
              IPStatusList.add(P);
         }
      }
    }
  }
 update IPStatusList;*/
}
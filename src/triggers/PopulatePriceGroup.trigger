/*****24/12/14 Null checks added by kaushiki..*/
trigger PopulatePriceGroup on BigMachines__Quote__c (before insert, before update) {
        
   
    Map<Id,Account> PriceGroupMap = new Map<Id, Account>();
    Set<Id> AccountIDS = new Set<Id>();
    
    /*
     * Bigmachines updates Distributor number in National distributor field on quote.
     * THis method queries account with that number if found updates National/Distributor field
     */
    QuoteTriggerHandler.updateNationalDistributorName(Trigger.new);
    
    /*
     * Updates Clearance field of BigMachines__Quote__c 
     */
    if(trigger.isInsert){
        QuoteTriggerHandler.updateClearanceField(Trigger.new);
    }
      
    QuoteTriggerHandler.updateQuoteFields(trigger.new, trigger.oldMap);
    /***** 5/10/16 Added by Narmada to update Reference Quote Lookup on POS Quotes **/
    QuoteTriggerHandler qt = new QuoteTriggerHandler();
    qt.updateQuote(trigger.new, trigger.oldMap, trigger.isInsert);
    
    system.debug('PriceGroupMap ' +PriceGroupMap );
   
    if(trigger.isUpdate){  
        for(BigMachines__Quote__c BQ : Trigger.new){
            BigMachines__Quote__c BQOld = Trigger.oldMap.get(BQ.Id);
            if(BQOld.BigMachines__Is_Primary__c == True && BQ.BigMachines__Is_Primary__c == False && (BQ.SAP_Prebooked_Sales__c != null || BQ.SAP_Prebooked_Service__c != null)){
                BQ.addError('Quote cannot be set as Primary, As this opportunity has already a Quote that has been submitted/processed.');
            }
            
            //new code added to trigger timebased workflow
            if(BQOld.SAP_Booked_Service__c ==null &&  BQ.SAP_Booked_Service__c != null){
                BQ.SAP_Booked_Service_Start_Update__c = system.now();
            }
            
        }
     }
}
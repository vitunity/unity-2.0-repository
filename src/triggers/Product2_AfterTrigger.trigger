/*************************************************************************\
      @ Author          : Kaushiki Verma(WIPRO Technologies)
      @ Date            : 8/5/2013
      @ Description     : This Trigger will be used to create a new complaint record under case if Is 'This a Complaint?' of case is 'yes'.
**************************************************************************/
trigger Product2_AfterTrigger on Product2 (after insert, after update)
{
    SR_Class_Product2 objCls = new SR_Class_Product2();
    objCls.sendEmailToLogistic(Trigger.new, Trigger.oldMap); // Uncommented by kaushiki 1A 1C Code merge 15/5/15
       
    
    if(trigger.isUpdate)
    {
        // Code Added By Mohit 11/11/2014
        objCls.updateCounterDetailFrmProduct(trigger.new,trigger.oldmap,trigger.newmap);
        
        //Code to update quote products forecast booking value if relavent_non_relavant flag is changed on product
        //Added by krishna 07172017
        String productIds = '(';
        Integer counter = 0;
        for(Product2 product : trigger.new){
            if(product.Relevant_Non_Relevant__c!=trigger.oldMap.get(product.Id).Relevant_Non_Relevant__c){
                productIds += '\''+product.Id+'\',';
                counter += 1;
            }           
        }
        productIds = productIds.removeEnd(',')+')';
        if(counter > 0){
            UpdateForecastBookingAmount updateForecastAmount = new UpdateForecastBookingAmount('SELECT Id FROM BigMachines__Quote_Product__c WHERE BigMachines__Product__c IN '+productIds);
            System.enqueueJob(updateForecastAmount);
        }
    }
    else
    {
        //AutoCreateClearanceByProduct.createClearanceByProduct(trigger.new);         
    }
}
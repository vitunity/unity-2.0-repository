trigger ProductDescriptionLanguageTrigger on Product_Description_Language__c (before insert, before update) {
  ProductDescriptionLanguageTriggerHandler.updateUniqueKey(trigger.new);
}
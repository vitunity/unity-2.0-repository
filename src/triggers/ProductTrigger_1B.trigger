/**
 * Auto creates clearance by product record for products with type sellable
 */
trigger ProductTrigger_1B on Product2 (after insert) {
    AutoCreateClearanceByProduct.createClearanceByProduct(trigger.new);
}
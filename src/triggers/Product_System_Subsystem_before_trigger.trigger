/****************************
Last Modified By    :    Nikita Gupta
Last Modified On    :    4/9/2014
Last Modified Reason:    US4056, to update System and Subsystem fields.Last Modified By    :    Nikita Gupta
Last Modified On    :    4/16/2014
Last Modified Reason:    US4056, Commented part of the code for US4056
Last Modified By    :    Priti Jaiswal
Last Modified On    :    03/06/2014
Last Modified Reason:    US4257 - Set Require explanantion to true when System or Subsystem Contains 'Other'
Last Modified Reason:    US4056, Commented part of the code for US4056
Last Modified By    :    Priti Jaiswal
Last Modified On    :    05/06/2014
Last Modified Reason:    US4579 - Set Covered Part Code field to 'SY'+PCode+ERP System Code+ERP Subsystem code
****************************/
trigger Product_System_Subsystem_before_trigger on Product_System_Subsystem__c (before update, before insert)
{
    SR_Class_ProductSystemSubsystem clsObj = new SR_Class_ProductSystemSubsystem();
    clsObj.setFieldsOnProdSysSubSystem(Trigger.New); 
}
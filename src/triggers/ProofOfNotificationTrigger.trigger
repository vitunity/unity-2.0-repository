/*---------------------------------------------------------------------
- Author        : Pranjul Maindola 
- Created Date  :29/08/2017
-----------------------------------------------------------------------*/
trigger ProofOfNotificationTrigger on PON__c (before insert, before update) {

    ProofOfNotificationHandler ponObj = new ProofOfNotificationHandler ();
    //updates Installed Product lookup field on 'Proof of Notification' Object.
    ponObj.updateInstProd(Trigger.new);
        
}
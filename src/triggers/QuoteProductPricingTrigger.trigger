trigger QuoteProductPricingTrigger on Quote_Product_Pricing__c (before insert, before update) {
	
	QuoteProductPricingTriggerHandler.updateQuoteProductOnQPP(trigger.new);
}
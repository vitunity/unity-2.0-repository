/*************************************************************************\
    @ Author        : Megha Arora
    @ Date          : 26-Apr-2013
    @ Description   : This trigger is to create Market Clearance Evidence records for a Product Versions for all countries
    @ Last Modified By  :   Naren Godugula
    @ Last Modified On  :   10/03/2014
    @ Last Modified Reason  :   Included Logic to Update Clrarance Entry Form when ever Transition Phase Gate or National Language Support or Design Output Review is updated on Product Profile Records
    @ Code Coverage is taken care in 'MarketClearanceReportTest' apex test class.
****************************************************************************/
trigger Review_Products_AfterTrigger on Review_Products__c (after insert, after update) 
{
  List<Input_Clearance__c> recordToInsert = new List<Input_Clearance__c>();
  List<Input_Clearance__c> CEFsForecast= new List<Input_Clearance__c>();
  List<String> ForecastList = new List<String>();  
    Map<Id, Country__c> countryMap = new Map<Id, Country__c>([SELECT id, Name FROM Country__c]);
    
     // if no country record, then no action
   
if(Trigger.isAfter){

  if(Trigger.isInsert){

   if(countryMap != null && countryMap.size() > 0)
    {   
     // Insert Market Clearance records 
        for(Review_Products__c revProduct : Trigger.new)
        {
            
            for(id countryId: countryMap.keySet())
            {
                Input_Clearance__c mce = new Input_Clearance__c(); 
                mce.Clearance_Entry_Form_Name__c= revProduct.Product_Profile_Name__c+', '+countryMap.get(countryId).Name;
                mce.Clearance_Status__c = 'Blocked';
                mce.Review_Product__c = revProduct.id;
                mce.Product_Model__c = revProduct.Product_Model__c;
                mce.Country__c = countryId;
                recordToInsert.add(mce);
            }
        }
        insert recordToInsert;
    } 
    }if(Trigger.isUpdate){
    
        Set<Id> setProductProfileIds = New set<Id>();
        
        for(Review_Products__c revProduct : Trigger.new)
        {
        
                if((Trigger.oldMap.get(revProduct.id).Transition_Phase_Gate__c != Trigger.newMap.get(revProduct.id).Transition_Phase_Gate__c) || (Trigger.oldMap.get(revProduct.id).National_Language_Support__c!= Trigger.newMap.get(revProduct.id).National_Language_Support__c)  || (Trigger.oldMap.get(revProduct.id).Design_Output_Review__c!= Trigger.newMap.get(revProduct.id).Design_Output_Review__c)){
                
                ForecastList.add(revProduct.id);
                }
                
                if(Trigger.oldMap.get(revProduct.id).Product_Profile_Name__c != Trigger.newMap.get(revProduct.id).Product_Profile_Name__c) 
                {
                    setProductProfileIds.add(revProduct.id);
                }
        }
        if(setProductProfileIds.size()>0)
        {
            List<Input_Clearance__c> lstInputClearance=[select Clearance_Entry_Form_Name__c,Review_Product__c,Country__r.name  from Input_Clearance__c where Review_Product__c IN : setProductProfileIds];
            
            if(lstInputClearance !=null && !lstInputClearance.isEmpty())
            {
                for(Input_Clearance__c obj : lstInputClearance )
                {
                  //String [] splitstr =obj.Clearance_Entry_Form_Name__c.split(',');
                  obj.Clearance_Entry_Form_Name__c=obj.Country__r.name !=null ?Trigger.newMap.get(obj.Review_Product__c ).Product_Profile_Name__c+', '+obj.Country__r.name
                  : Trigger.newMap.get(obj.Review_Product__c ).Product_Profile_Name__c;
                }
                
                update lstInputClearance;
            }
        }
        
        if(ForecastList.size()>0){
        
         for(Input_Clearance__c IC : [select id, name, Country__c,Review_Product__c, Expected_Clearance_Date__c, Estimated_Submission_Date__c, Design_Output_Review__c,Transition_Phase_Gate__c, National_Language_Support__c,  Milestone_Selection__c from Input_Clearance__c where Current_Status__c!= 'Submitted' AND Actual_Submission_Date__c = Null AND Clearance_Status__c != 'Permitted' AND Review_Product__c IN:ForecastList]){
                system.debug('IC---'+IC);
                CEFsForecast.add(IC);
            }
            
            system.debug('CEFsForecast---'+CEFsForecast);
            if(CEFsForecast.size()>0){
            update CEFsForecast;
            }
        }
    }if(Trigger.isUpdate){
        
        Set<Id> setPPIds = New set<Id>();
        
        for(Review_Products__c revProduct : Trigger.new)
        {
        
                if(Trigger.oldMap.get(revProduct.id).Business_Unit__c != Trigger.newMap.get(revProduct.id).Business_Unit__c /*&& revProduct.Business_Unit__c =='VBT'*/){
                    
                    setPPIds.add(revProduct.id);
                }
                
        }
        
        if(setPPIds.size()>0)
        {
            List<Input_Clearance__c> lstInClearance=[select Clearance_Entry_Form_Name__c,Review_Product__c,Country__r.name  from Input_Clearance__c where Review_Product__c IN : setPPIds];
            
            if(lstInClearance !=null && !lstInClearance.isEmpty())
            {
                update lstInClearance;
            }
        }
    }
    
    }
}
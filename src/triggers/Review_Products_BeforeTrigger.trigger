trigger Review_Products_BeforeTrigger on Review_Products__c (before insert,before update) {
	Map<id,string> Bundledprodprofileslog = new Map<Id,String>();
    Map<id,integer> bundledprofseq = new Map<id,integer>();
    Map<id,string> bundledprofbus = new Map<Id,string>();
    Map<integer,string> seq2value = new map<integer,string>();
    map<id,string> issuedbundledprodprof = new map<id,string>();
    map<Id,boolean> Bundledprodprofiles2update = new Map<Id,boolean>();
    public static ID BundledRTID = Schema.SObjectType.Review_Products__c.getRecordTypeInfosByName().get('Bundle').getRecordTypeId();
    public static boolean vferror = false;
	productprofile pp = new productprofile();
	pp.beforeupdate(trigger.new);
	if(LostFiscalYearRecursive.runOnce() || test.isRunningtest())
    {
        for(Review_Products__c revProduct : Trigger.new)
        {
            system.debug('revproduct: '+revproduct);
            system.debug('BundledRTID: '+BundledRTID);
            if (revProduct.recordtypeID == BundledRTID)
            {
                if (!String.isBlank(revProduct.Bundled_Filter_Logic__c))
                {
                    Bundledprodprofileslog.put(revProduct.id,revProduct.Bundled_Filter_Logic__c);
                }
            }
        }
    }
    if (Bundledprodprofileslog.size() > 0)
    {
        integer maxkey = 0;
        List<integer> bundledlognumbers = new List<integer>();
        for(Id BPID : Bundledprodprofileslog.keyset())
        {
            maxkey = 0;
            String Bundledlogic = '';
            bundledlogic = Bundledprodprofileslog.get(BPID);
            integer maxnum = 0;
            for (integer i =0;i<bundledlogic.length();i++)
            {
                integer charuc = bundledlogic.charAt(i);
                List<Integer> charArr= new Integer[]{charuc};
                String convertedChar = String.fromCharArray(charArr);
                if (convertedChar.isNumeric())
                {
                    bundledlognumbers.add(integer.valueOf(convertedChar));
                    maxnum = Math.max(maxnum, Integer.valueOf(convertedChar));
                }
            }
            seq2value = new map<integer,string>();
            bundledprofbus = new Map<Id,string>();
            for (Product_Profile_Association__c ppa : [select id,Bundle_Product_Profile__c,Bundle_Product_Profile__r.Bundled_Filter_Logic__c,Product_Profile_Business_Unit__c,Sequence_Number__c,PP_Third_Party_Product__c from Product_Profile_Association__c where Bundle_Product_Profile__c = :BPID order by name asc])
            {
                system.debug('***ppa : '+ppa);
                seq2value.put(Integer.valueOf(ppa.Sequence_Number__c),String.valueOf(ppa.PP_Third_Party_Product__c));
                if(!ppa.PP_Third_Party_Product__c)
                {
                    bundledprofbus.put(ppa.Bundle_Product_Profile__c,ppa.Product_Profile_Business_Unit__c);
                }
            }
            system.debug('***CM seq2value: '+seq2value);
            if (bundledprofbus.size() > 0)
            {
                if (seq2value.size() > 0)
                {
                    list<integer> keys = new list<integer>();
                    keys.addAll(seq2value.keyset());
                    keys.sort();
                    maxkey = keys[keys.size()-1];
                    if (keys.size() == bundledlognumbers.size())
                    {
                        if (maxkey == maxnum)
                        {
                            string finlog = '';
                            for (integer seqnum : seq2value.keyset())
                            {
                                system.debug('***bundledlogic: '+bundledlogic);
                                Bundledlogic = bundledlogic.replaceAll(String.valueOf(seqNum),seq2value.get(seqnum));
                                string andop = 'AND';
                                string orop = 'OR';
                                string andsfop = '&&';
                                string orsfop = '||';
                                finlog = Bundledlogic.replace(andop, andsfop);
                                system.debug('*** finlog : after AND: '+finlog);
                                finlog = finlog.replace(orop,orsfop);
                            }
                            system.debug('***CM finlog : '+finlog);
                            boolean result = booleanexpression.evaluate(finlog);
                            Bundledprodprofiles2update.put(BPID,result);
                        }
                        else
                        {
                            issuedbundledprodprof.put(BPID,'error2');
                        }
                    }
                    else
                    {
                        issuedbundledprodprof.put(BPID,'error1');
                    }
                }
                else
                {
                    continue;
                }
            }
        }
        for (Review_Products__c pb: trigger.new)
        {
            if (issuedbundledprodprof.size() > 0 && issuedbundledprodprof.containskey(pb.id))
            {
                if (issuedbundledprodprof.get(pb.id) == 'error1')
                {
                    pb.addError('Incorrect Bundle Logic, All the Product Profile associated to Bundle are not included in the logic, Please make sure to correct the logic');
                }
                else
                {
                    pb.addError('Incorrect Bundle Logic, Product Profile associated to Bundle are '+ maxkey +', Please make sure to correct the logic');   
                }
                bundledproductprofiles.vferror = true;
            }
            else if (Bundledprodprofiles2update.size() > 0 && Bundledprodprofiles2update.containskey(pb.id) && Bundledprodprofiles2update.get(pb.id) != null)
            {
                if (!Bundledprodprofiles2update.get(pb.id))
                {
                    PB.Business_Unit__c = bundledprofbus.get(pb.id);
                }
                else
                {
                    PB.Business_Unit__c = null;
                }
            }
        }
    }
}
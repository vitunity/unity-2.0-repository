trigger RevisedQuoteInfo on BigMachines__Quote__c (before insert,after insert) {
    
    Set<String> nameSet = new Set<String>(); 
    Set<Id> quoteIdSet = new Set<Id>();
    for(BigMachines__Quote__c BQ : Trigger.new){
        if(BQ.Name.contains('-'))
        {    
            List<String> quoteStrings = BQ.Name.split('-');
            if(!String.isBlank(BQ.Parent_Quote__c)){                
                nameSet.add(BQ.Parent_Quote__c);
            }    
        }     
    }
    System.debug('----nameSet'+nameSet);
    List<BigMachines__Quote__c> existQuoteList = [SELECT Id, Name,Sales_Org__c,Service_Org__c,Distribution_Channel__c,BigMachines__Opportunity__r.National_Account__r.National_Account__c,
                                                  Principal_Contact__c,Purchase_Order_Number__c,Purchase_Order_Date__c,Projected_Ship_Date__c,Purchasing_Contact__c,Medical_Contact__c,
                                                  Physics_Contact__c,Technical_Contact__c,Voltage__c,Frequency__c,prd_ht1__c,prd_ht2__c,prd_ht3__c,srv_ht1__c,srv_ht2__c,srv_ht3__c,
                                                  Shipping_instructions__c,Configuration_Instructions__c,Installation_Acceptance_Instructions__c,Reference_Documents__c,Actions_Required__c,
                                                  Reference_Quotation__c,Penalty_Terms__c
                                                  FROM BigMachines__Quote__c 
                                                  WHERE Name in: nameSet];
    System.debug('----existQuoteList'+existQuoteList);                                              
    Map<String,BigMachines__Quote__c> existingQuotes = new Map<String,BigMachines__Quote__c>();
    for(BigMachines__Quote__c bmQuote : existQuoteList){
    	existingQuotes.put(bmQuote.Name, bmQuote);
    	quoteIdSet.add(bmQuote.Id);
    }
    
    System.debug('----existingQuotes'+existingQuotes);
    
    if(Trigger.isBefore){    
        for(BigMachines__Quote__c BQ : Trigger.new){
            if(BQ.Parent_Quote__c != null && BQ.Parent_Quote__c != ''){
            	BigMachines__Quote__c BQ1 = existingQuotes.get(BQ.Parent_Quote__c);
            	System.debug('----BQ1'+BQ1);
                BQ.Sales_Org__c = BQ1.Sales_Org__c;
                BQ.Service_Org__c = BQ1.Service_Org__c;
                BQ.Distribution_Channel__c = BQ1.Distribution_Channel__c;
                //BQ.BigMachines__Opportunity__r.National_Account__r.National_Account__c = BQ1.BigMachines__Opportunity__r.National_Account__r.National_Account__c;
                BQ.Purchase_Order_Number__c = BQ1.Purchase_Order_Number__c;
                BQ.Purchase_Order_Date__c = BQ1.Purchase_Order_Date__c;
                BQ.Projected_Ship_Date__c = BQ1.Projected_Ship_Date__c;
                BQ.Principal_Contact__c = BQ1.Principal_Contact__c;
                BQ.Purchasing_Contact__c = BQ1.Purchasing_Contact__c;
                BQ.Medical_Contact__c = BQ1.Medical_Contact__c;
                BQ.Physics_Contact__c = BQ1.Physics_Contact__c;
                BQ.Technical_Contact__c = BQ1.Technical_Contact__c;
                BQ.Voltage__c = BQ1.Voltage__c;
                BQ.Frequency__c = BQ1.Frequency__c;
                BQ.prd_ht1__c = BQ1.prd_ht1__c ;
                BQ.prd_ht2__c = BQ1.prd_ht2__c ;
                BQ.prd_ht3__c = BQ1.prd_ht3__c ;
                BQ.srv_ht1__c = BQ1.srv_ht1__c ;
                BQ.srv_ht2__c = BQ1.srv_ht2__c ;
                BQ.srv_ht3__c = BQ1.srv_ht3__c ;
                BQ.Shipping_instructions__c = BQ1.Shipping_instructions__c ;
                BQ.Configuration_Instructions__c = BQ1.Configuration_Instructions__c ;
                BQ.Installation_Acceptance_Instructions__c = BQ1.Installation_Acceptance_Instructions__c ;
                BQ.Reference_Documents__c = BQ1.Reference_Documents__c ;
                BQ.Actions_Required__c = BQ1.Actions_Required__c ;
                BQ.Reference_Quotation__c = BQ1.Reference_Quotation__c ;
                BQ.Penalty_Terms__c = BQ1.Penalty_Terms__c ;
                
                breaK;
            }
        }
    }
    
    if(Trigger.isAfter){
        
        List<Quote_Product_Partner__c> QProPartnerList = [SELECT Id,Name,Comments__c,Contact__c,Contact_Email__c,ERP_Contact_Function__c,ERP_Contact_Number__c,
															ERP_Partner__c,ERP_Partner_Function__c,ERP_Partner_Number__c,Quote__c,Quote_Product__c 
                                                         	FROM Quote_Product_Partner__c 
                                                         	WHERE Quote__c in: quoteIdSet];
        List<Quote_Product_Partner__c> insertQuoteProPartnerList = new List<Quote_Product_Partner__c>();
        
        for(BigMachines__Quote__c BQ : Trigger.new){
            if(!String.isBlank(BQ.Parent_Quote__c)){
            	BigMachines__Quote__c BQ1 = existingQuotes.get(BQ.Parent_Quote__c);
            	
                for(Quote_Product_Partner__c QPP1 : QProPartnerList){	
                    if(QPP1.Quote__c == BQ1.Id){	
                        Quote_Product_Partner__c QPP = new Quote_Product_Partner__c();
                        QPP.Quote__c = BQ.Id;
                        QPP.Comments__c = QPP1.Comments__c;
                        QPP.Contact__c = QPP1.Contact__c;
                        QPP.Contact_Email__c = QPP1.Contact_Email__c;
                        QPP.ERP_Contact_Function__c = QPP1.ERP_Contact_Function__c;
                        QPP.ERP_Contact_Number__c = QPP1.ERP_Contact_Number__c;
                        QPP.ERP_Partner__c = QPP1.ERP_Partner__c;
                        QPP.ERP_Partner_Function__c = QPP1.ERP_Partner_Function__c;
                        QPP.ERP_Partner_Number__c = QPP1.ERP_Partner_Number__c;
                        insertQuoteProPartnerList.add(QPP);
                    }    
                }                  
            }
        }
         
        if(!insertQuoteProPartnerList.isEmpty())
            insert insertQuoteProPartnerList;
        
    }
}
/*************************************************************************\
    @ Author                : Kaushiki Verma(WIPRO Technologies)
    @ Date                  : 8/1/2014
    @ Description           : This trigger is populating fields on technician/equipment from Timecard Profile(US3018). 
    @ Last Modified On      :    
    @ Last Modified Reason  :    
    
    Change Log -:
    Nilesh Gorle - 26 Feb 2018 - STSK0013884 - Set Calibration Status to 'Not Required' when Inventory Location is changed to Retired and do not clear fields
     
****************************************************************************/
trigger SC_Technicianfieldpopulation on SVMXC__Service_Group_Members__c (before insert, before update)
{   
    // DE 6743 : populating the TimeZone field on Technician record using ERP_TimeZone Field
    // TODO: Have to move the logic inside the Controller #FutureRelease
    Map<String, Id> timeZoneMap = new Map<String, Id>();
    //if(trigger.isBefore && trigger.isInsert) {
        Set<String> timeZoneTextValue = new Set<String>();
        for(SVMXC__Service_Group_Members__c technicianRec : trigger.new) {
            if(technicianRec.ERP_Timezone__c != '' && technicianRec.ERP_Timezone__c != null && (technicianRec.Timezone_lookup__c == null || trigger.oldmap == null || trigger.oldmap.get(technicianRec.id) == null ||  trigger.oldmap.get(TechnicianRec.id).ERP_Timezone__c == null ||  trigger.oldmap.get(technicianRec.id).ERP_Timezone__c != technicianRec.ERP_Timezone__c )) {
                timeZoneTextValue.add(technicianRec.ERP_Timezone__c);
            }
        }
        if (timeZoneTextValue.size()>0)  //DE7370
        for(ERP_Timezone__c erpTimeZone : [SELECT Id, Name FROM ERP_Timezone__c WHERE Name IN: timeZoneTextValue]) {
            timeZoneMap.put(erpTimeZone.Name, erpTimeZone.Id);
        }
    //}

    for(SVMXC__Service_Group_Members__c techMember : trigger.new) {
        if(timeZoneMap.containsKey(techMember.ERP_Timezone__c)) {
            techMember.Timezone_Lookup__c = timeZoneMap.get(techMember.ERP_Timezone__c);
        }
    }
    // DE 6743 //
    SC_ClassTechnician_Equipment objCls = new  SC_ClassTechnician_Equipment();
    objCls.populatefields(trigger.new, Trigger.oldMap);
    
    
    /* Start - STSK0013884 - Set Calibration Status to 'Not Required' when Inventory Location is changed to Retired or scrapped and do not clear fields */
    if (Trigger.isUpdate) {
        Map<Id, SVMXC__Service_Group_Members__c> oldMap = Trigger.oldMap;
        for (SVMXC__Service_Group_Members__c servGroup: Trigger.New) {
            if(oldMap.containsKey(servGroup.Id) && servGroup.SVMXC__Inventory_Location__c!=null && 
                oldMap.get(servGroup.Id).SVMXC__Inventory_Location__c !=servGroup.SVMXC__Inventory_Location__c) {
                list<SVMXC__Site__c> sites = [select Id, SVMXC__Location_Type__c from SVMXC__Site__c Where Id=:servGroup.SVMXC__Inventory_Location__c limit 1];
                if(sites[0].SVMXC__Location_Type__c == 'Retired') {            
                    servGroup.Calibration_Status__c = 'Not Required';
                    servGroup.User__c = oldMap.get(servGroup.Id).User__c;
                    servGroup.SVMXC__Active__c = oldMap.get(servGroup.Id).SVMXC__Active__c;
                }
            }
        }
    }
    /* End- STSK0013884 - Set Calibration Status to 'Not Required' when Inventory Location is changed to Retired or scrapped and do not clear fields */
    
    objCls.updateEmailandCssDistrict(trigger.new, Trigger.oldMap);
}
/**************************************************************\
@ Last Modified By    : Kaushiki 18/06/14 Null checks added
***************************************************************/
trigger SRPopulateCaseCommentonCaseActivity on Case_Comments__c (after Insert,after Update,before Update) 
 {
   Set<Id> SetofCaseIds = new Set<Id>();
   List<case> ListofCases = new List<case>();
   List<case> ListofCasestoUpdate = new List<case>();
   List<case> ListofUpdateCases = new List<case>();
   List<String> ListofCaseActivity = new List<String>();
   List<Case_Comments__c> casecommentlist = new List<Case_Comments__c> ();
   Map<Id,Case> MapofCase = new Map<Id,Case> ();
   for(Case_Comments__c commentalias: trigger.new)
   {    
      if(commentalias.Case__c!=null)//Kaushiki - 18/06/14 - null check
      SetofCaseIds.add(commentalias.Case__c); 
   }
   if(SetofCaseIds.size()>0 && SetofCaseIds!=null)//Kaushiki - 18/06/14 - for null check
   {
        MapofCase= new Map<Id,Case>([Select Case_Activity__c from Case where id in:SetofCaseIds]);
   }
   if(Trigger.isAfter && Trigger.IsInsert)
  {  

   for(Case_Comments__c casecommentalias: Trigger.New )
   {

       Case casealias = new Case();
       if(MapofCase.containsKey(casecommentalias.Case__c) && MapofCase.get(casecommentalias.Case__c)!=null )//Kaushiki - 18/06/14 - for null check
       {
           casealias = MapofCase.get(casecommentalias.Case__c);
       }
       if(casealias.case_Activity__c == Null)
       {
           casealias.case_Activity__c = casecommentalias.Comments__c;
       }
       else
       {
           casealias.case_Activity__c=casealias.case_Activity__c+'<br></br>'+casecommentalias.Comments__c;
       }
       ListofCasestoUpdate.add(casealias);
      
   }
   if(ListofCasestoUpdate.size()>0)//Kaushiki - 18/06/14 - for null check
   {
       update ListofCasestoUpdate;
   }

   }

   if(Trigger.isAfter && Trigger.IsUpdate)
   {
       if(SetofCaseIds.size()>0)//Kaushiki - 18/06/14 - null check
       {
           casecommentlist=[Select Id,Comments__c,Case__c from Case_Comments__c where Case__c in:SetofCaseIds ];
       }
       Map<Case_Comments__c,List<String>> MapofCaseActivityString = new Map<Case_Comments__c,List<String>>();
       system.debug('&&&&'+casecommentlist);
       if(casecommentlist.size()>0)//Kaushiki - 18/06/14 - null check
       {
           for(Case_Comments__c commentalias:casecommentlist)
           {    
                if(commentalias.Comments__c!=null)//Kaushiki - 18/06/14 - null check
                ListofCaseActivity.add(commentalias.Comments__c);
                /*if(ListofCaseActivity.size()>0)
                {
                    MapofCaseActivityString.put(commentalias,ListofCaseActivity);//MapofCaseActivityString not used anywhere therefore commented Kaushiki - 18/06/14 -
                }*/
           }
       }
       for(Case_Comments__c comments: Trigger.New)
       {
           Case casealias = new Case();
           if(MapofCase.containsKey(comments.Case__c) && MapofCase.get(comments.Case__c)!=null)//Kaushiki - 18/06/14 - null check
           casealias = MapofCase.get(comments.Case__c);
           casealias.Case_Activity__c='';
             if(ListofCaseActivity.size()>0)//Kaushiki - 18/06/14 - null check
             {
               for(String s:ListofCaseActivity)
               {            
                   casealias.Case_Activity__c = casealias.Case_Activity__c+'<br></br>'+s;             
               }
             }
           ListofUpdateCases.add(casealias);
       }
       if(ListofUpdateCases.size()>0)//Kaushiki - 18/06/14 - null check
       update ListofUpdateCases;
       
   }

 }
trigger SRPopulateNextEntitleDate on Opportunity (after Update) 
{Set<string> stContractId=new Set<string>();
Set<string> stOpptyId=new Set<string>();
for(opportunity varOpt: trigger.new)
{
 if(Trigger.newmap.get(varOpt.id).Expected_Close_Date__c !=trigger.oldmap.get(varOpt.id).Expected_Close_Date__c)
 {
stOpptyId.add(varOpt.id);
}
}
map<string,string> mpContractOpptyId=new map<string,string>();
Map<string,SVMXC__Service_Contract__c> mapContractOpportunity=new Map<string,SVMXC__Service_Contract__c>([select id,(select id,SVMXC__Installed_Product__c from R00N70000001hzdaEAA__r),Renewal_Opportunity__c from SVMXC__Service_Contract__c where Renewal_Opportunity__c In :stOpptyId]);
for(SVMXC__Service_Contract__c varContract: mapContractOpportunity.values())
{
mpContractOpptyId.put(varContract.Renewal_Opportunity__c,varContract.id) ;

}
list<SVMXC__Installed_Product__c> lstIP=new list<SVMXC__Installed_Product__c>();
for(opportunity varOpt: trigger.new)
{
 if(Trigger.newmap.get(varOpt.id).Expected_Close_Date__c !=trigger.oldmap.get(varOpt.id).Expected_Close_Date__c)
 {
  for( SVMXC__Service_Contract_Products__c varCoveredProduct: mapContractOpportunity.get(mpContractOpptyId.get(varOpt.id)).R00N70000001hzdaEAA__r)
  {
  SVMXC__Installed_Product__c ip=new SVMXC__Installed_Product__c(id=varCoveredProduct.SVMXC__Installed_Product__c,  Next_Entitle_Date__c=varOpt.Expected_Close_Date__c);
  
  lstIP.add(ip);
  
  }
 }
}
update lstIP;
}
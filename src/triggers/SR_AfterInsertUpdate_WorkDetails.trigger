/***************************************************************************
Author: Amitkumar Katre
Created Date: 10-31-2017
Project/Story/Inc/Task : refactoring work detail triggers
Description: Removed all commented lines and un used code.
Change Log:
Apr-19-2018 : Chandramouli : STSK0014234 : Skipping creation of TE & TS depending technician field. 
*************************************************************************************/
trigger SR_AfterInsertUpdate_WorkDetails on SVMXC__Service_Order_Line__c ( after insert, after update) 
{
    system.debug('SOQL Count on after work detail start-----'+Limits.getQueries());
    
    if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter&&!Test.isRunningTest()) {
        SObjectHistoryProcessor.trackHistory('SVMXC__Service_Order_Line__c');
    }
    
    /************************* Initiating classes ****************************************/
    SRClassWorkOrderDetail objWD = new SRClassWorkOrderDetail();    
    WorkDetail_TriggerHandler wdth = new WorkDetail_TriggerHandler();
    
    
    List<SVMXC__Service_Order_Line__c> LstWD = new list<SVMXC__Service_Order_Line__c>();
    ID usageConsumptionWDRecType = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order_Line__c').get('UsageConsumption');
    //Not required form Time Entry Matrix. SOQL 101 Check.
    if(!constantFlagController.wrkDetailFlag){
        objWD.updateActualHoursInIWO(trigger.new, trigger.newMap, trigger.oldMap);
    }
    Set<Id> completedPSDetailsParentIdList = new Set<Id>(); 
    
    if(trigger.isinsert || trigger.isupdate) {  
        //Checking if the Static Worder Line List have some values which is been set from Before Trigger
        if(WorkDetail_TriggerHandler.insertWorkOrdersLine != null && WorkDetail_TriggerHandler.insertWorkOrdersLine.size() > 0 ) {
            List <SVMXC__Service_Order_Line__c> lwd = WorkDetail_TriggerHandler.insertWorkOrdersLine;
            WorkDetail_TriggerHandler.insertWorkOrdersLine.clear();
            insert lwd;
        }
        objWD.updateERP_NWAInfo(trigger.new, trigger.oldmap);
        //objWD.erpUpdateAndTimeSheets(trigger.new, trigger.oldmap);
       
        for(SVMXC__Service_Order_Line__c varWD : trigger.new) {
            if(Trigger.isUpdate && varWD.Completed__c && varWD.Completed__c != Trigger.oldMap.get(varWD.Id).Completed__c) {
                completedPSDetailsParentIdList.add(varWD.SVMXC__Service_Order__c);
            }            
        }
    }

    if((SRClassWorkOrderDetail.publicMapInsertChildWD != null && SRClassWorkOrderDetail.publicMapInsertChildWD.size() > 0) 
        || (SRClassWorkOrderDetail.mapParentWDwithIP != null && SRClassWorkOrderDetail.mapParentWDwithIP.size() > 0))
    {
        objWD.outOfRecursiveAfterWD(trigger.new);
    }
    
    if(trigger.isUpdate){
        objWD.updateCounterDetail(trigger.new, trigger.oldmap);
        if(!completedPSDetailsParentIdList.isEmpty()){
            wdth.closeParentWorkOrders(completedPSDetailsParentIdList);
        }
    }
    
    /********************* On Insert and Update **************************************/
    if(trigger.isinsert || trigger.isupdate){
        objWD.updateConsumedOnPartsOrderLine(trigger.new,trigger.newMap,trigger.oldMap);
    }
    //Apr-19-2018 : Chandramouli : STSK0014234 : Skipping creation of TE & TS depending technician field. 
    List<SVMXC__Service_Order_Line__c> wd2createTE = new List<SVMXC__Service_Order_Line__c>();
    for (SVMXC__Service_Order_Line__c wd : trigger.new)
    {
        if (!wd.Technician_Timecard_Required__c)
        {
            wd2createTE.add(wd);
        }
    }
    if (wd2createTE.size() > 0)
    {
        ServiceMaxTimesheetUtils.createTimeEventsFromWorkDetails(wd2createTE, trigger.oldMap, FALSE);
    }
    objWD.updatePOLRemainingQty(trigger.new,trigger.oldMap);
    //This must called end of the trigger.      
    objWD.writeback(); 
    set<Id> setWD = new set<Id>();
    if (trigger.isUpdate && trigger.isAfter) {            
        for(SVMXC__Service_Order_Line__c wd : trigger.new){
            if(wd.SVMXC__Work_Detail__c  == null && wd.SVMXC__Line_Type__c == 'labor'){
                setWD.add(wd.Id);
            }
        }
    }
    if(!system.isFuture() && setWD.size() > 0){
        WorkDetailUpdateUtility.updateWorkDetailLookup(setWD);
    }
    system.debug('SOQL Count on after work detail end -----'+Limits.getQueries());
       
}
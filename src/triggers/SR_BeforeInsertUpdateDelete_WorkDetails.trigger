/*
 * Last Modified By - Amitkumar Katre 
 * Re factored by Amitkumar
 * back up of this trigger is SR_BeforeInsertUpdateDelete_WorkDetailsBackup (Inactive)
 */
trigger SR_BeforeInsertUpdateDelete_WorkDetails on SVMXC__Service_Order_Line__c (before insert, before update,before delete) {
    SRClassWorkOrderDetail objWD = new SRClassWorkOrderDetail();
    try{
        //SRClassWorkOrderDetail objWD = new SRClassWorkOrderDetail();
       /* if(trigger.isInsert)
        {
            for(SVMXC__Service_Order_Line__c wd : trigger.new){
                system.debug('***WD1 : '+wd);
                if (wd.SVMXC__Actual_Price2__c == 0 || wd.SVMXC__Actual_Price2__c == null)
                {
                    if ( wd.SVMXC__Total_Line_Price__c > 0 && wd.SVMXC__Actual_Quantity2__c > 0)
                    {
                        wd.SVMXC__Actual_Price2__c = wd.SVMXC__Total_Line_Price__c / wd.SVMXC__Actual_Quantity2__c;
                    }
                }
           }
        }*/
        // STSK0014747 - Avoid recursion multiple times
        if((trigger.isInsert || trigger.isUpdate) && SRClassWorkOrderDetail.runOnce()){
            // Work detail fields auto-populate
            objWD.updateFieldsOnWorkDetail(trigger.new, trigger.oldmap);
            //Rakesh:start      
                WordOrderLocalTimeFormat.correctTimeFormat(trigger.new);        
            //Rakesh:End
        }
        if(trigger.isDelete){
            objWD.errorForChildDetailsOnDelete(trigger.oldMap);
            WorkOrderPSValidator.psDeleteValidate(trigger.oldMap);
        }else if(trigger.IsInsert || trigger.IsUpdate){
            List<SVMXC__Service_Order_Line__c> LstWD = new list<SVMXC__Service_Order_Line__c>();
            for(SVMXC__Service_Order_Line__c wd : trigger.new){
                system.debug('***WD2 : '+wd);
                if(wd.SVMXC__Start_Date_and_Time__c != null 
                && wd.SVMXC__End_Date_and_Time__c != null 
                && (trigger.oldmap == null || trigger.oldmap.get(wd.id).SVMXC__Start_Date_and_Time__c == null 
                || trigger.oldmap.get(wd.id).SVMXC__End_Date_and_Time__c == null 
                || trigger.oldmap.get(wd.id).SVMXC__Start_Date_and_Time__c != wd.SVMXC__Start_Date_and_Time__c  
                || trigger.oldmap.get(wd.id).SVMXC__End_Date_and_Time__c != wd.SVMXC__End_Date_and_Time__c))
                {
                    if(wd.SVMXC__Line_Type__c == 'Labor'){
                        SR_Class_WorkOrder.machineReleaseFromWDL = true;
                    }
                    LstWD.add(wd); 
                } 
            }
            if(LstWD.size() > 0){
                // work detail overlapping Check
                ServiceMaxTimesheetUtils.checkForWorkDetailOverlap(LstWD, trigger.oldMap);
                
            }
             for(SVMXC__Service_Order_Line__c wd : trigger.new){
                system.debug('***WD3 : '+wd);
            }
            //set from and to location
            objWD.setFromAndToLocationOnWD(trigger.new, trigger.oldmap);
             for(SVMXC__Service_Order_Line__c wd : trigger.new){
                system.debug('***WD4 : '+wd);
            }
        }
        //INC4295524  DFCT0011762 Amitkumar Katre Parts Work Details: Source of Parts and Consumed from Location Incorrect
        if(trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
            SourceofPartsWorkDetailHandler.execute(Trigger.new,trigger.oldmap);
        }
        // Billing type covered or not covered chech for helpdesk
        if(trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
            WorkDetailCoveredBillingTypeHandler.hdWOCovredBillingType(Trigger.new,trigger.oldmap);
        }
        //DFCT0011851 : DE8010: Deletion of Master Activity should Delete Related Parts Below
        if(trigger.isDelete && Trigger.isBefore){
            WorkDetailRelatedActivityDeleteHandler.invoke(trigger.old);
        }
        
        //STRY0013754 - Set interface status do not process for poland
        if(trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){ 
            WorkDetailInterfaceStatusHandler.execute(Trigger.new,trigger.oldmap);
        }
    }catch(Exception e){
        System.debug('Error -----'+e);
        throw new WorkOrderProcessException('Error Message::'+e.getMessage()+'::Code Line number::'+e.getLineNumber());
    }
}
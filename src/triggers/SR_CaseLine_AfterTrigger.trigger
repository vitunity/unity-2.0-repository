/**************************************************************
Created By  :   Nikita Gupta
Created on  :   1/6/2015
US4557
de1996
Refactored by Amitkumar - 1 dec 2016 
**************************************************************/

trigger SR_CaseLine_AfterTrigger on SVMXC__Case_Line__c (after Insert, after Update) {
	
    SR_Class_CaseLine objCaseLine = new SR_Class_CaseLine();
   
    //insert case line history
    if(trigger.isinsert && trigger.IsAfter) {
        objCaseLine.insertCaseHistory(trigger.new);
    }
    
    //delete installed product
    if (trigger.isUpdate) { 
        objCaseLine.deleteOrderedInstalledProducts(trigger.oldMap, trigger.newMap);
    }
    //delete installed product
    if (trigger.isUpdate && trigger.IsAfter) { 
        objCaseLine.updateDatesOnPWO(trigger.new, trigger.oldMap);
    }
    
    //create work order and work detail	
    if(trigger.isinsert){ 
    	objCaseLine.createWorkOrderandDetail(trigger.new,trigger.oldmap); 
    }
}
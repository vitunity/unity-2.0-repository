/*
 * Updated by Amitkumar  1dec 2016
 * Removed business logic to hanlder class.
 */
trigger SR_CaseLine_BeforeDeleteTrigger on SVMXC__Case_Line__c (before delete) {
    
    if(trigger.isBefore && trigger.isDelete) {
         SR_Class_CaseLine caseLineObj = new SR_Class_CaseLine();
         caseLineObj.caseLineDeleteHandler(trigger.old,trigger.oldmap);
    }
    
}
/**************************************************************
Created By  :   Nikita Gupta
Created on  :   1/6/2015
*************************************************************
US5215 - de
*/
trigger SR_CaseLine_BeforeTrigger on SVMXC__Case_Line__c (before Insert, before update){
	
	SR_Class_CaseLine objClass = new SR_Class_CaseLine();
	
   	objClass.prePoulateFldsOnCaseLine(trigger.new, trigger.oldmap);
   	
    Set<Id> SetCaseLineSOI = new Set<Id>(); //added by Shubham for US4557 22/05/2014 
    set<SVMXC__Case_Line__c> set_CaseLine = new set<SVMXC__Case_Line__c>(); //Set of Case line Data
    set<id> setIPId = new set<id>(); //Set of Installed Product Ids linked to Case Line
    set<id> setCaseId = new set<id>(); //set with Case ids 
    map<id, Case> mapCase = new map<id, Case>();// map<string, Case> mapCase = new map<string, Case>();
    map<Id, Case> mapCaseId_Case = new map<Id, Case>();
    Set<Id> setCaseId_POno = new Set<Id>(); //DE1687
    map<Id, string> mapCaseId_POno = new map<Id, string>(); //DE1687
    ServiceTriggerUtil objServTrig = new ServiceTriggerUtil();
    for(SVMXC__Case_Line__c varCL2: trigger.new)
    {
        if(varCL2.End_date_time__c != null && varCL2.Start_Date_time__c != null &&  varCL2.End_date_time__c > varCL2.Start_Date_time__c)
        {
            Decimal days = date.valueof(varCL2.Start_Date_time__c).daysBetween(date.valueof(varCL2.End_date_time__c)) ;
            Decimal hoursTemp = days*24;
            Decimal hr;

            Time T1 = varCL2.Start_Date_time__c.time();
            Time T2 = varCL2.End_date_time__c.time();

            if( T2.hour() >= T1.hour()) // added by Shubham on 14/10/2014 // midnight cross check
            {
                hr = T2.hour() - T1.hour();
            }
            else
            {
                hr = 24 -(T1.hour() - T2.hour());
            }
            Decimal mins = T2.minute() - T1.minute();
            if(mins != 0.0)
            {
                mins = mins/60;
            }   

            Decimal finalHours = (hoursTemp+hr+mins);
            varCL2.Hours__c = finalHours;
            system.debug('Case Line Hours *** '+ varCL2.Hours__c);
        }
        if(varCL2.Start_Date_time__c != null && varCL2.Hours__c != null && varCL2.End_date_time__c == null && varCL2.Hours__c != 0)
        {
            integer finalHours = integer.valueof(varCL2.Hours__c);
            varCL2.End_date_time__c = varCL2.Start_Date_time__c.addHours(integer.valueOf(finalHours));
            system.debug('Case Line End Date Time !!! '+ varCL2.End_date_time__c);
        }
        //code for DE1687
        if((varCL2.Billing_Type__c != NULL && varCL2.Billing_Type__c.substring(0,1) == 'P') && (trigger.oldMap == null || trigger.oldMap.get(varCL2.Id).Billing_Type__c == null || trigger.oldMap.get(varCL2.Id).Billing_Type__c.substring(0,1) != 'P'))  // DE4371 (substring fix)
        {
            setCaseId_POno.add(varCL2.SVMXC__Case__c);
        }

    }   

    
    for(SVMXC__Case_Line__c objCaseLine : trigger.new) //Iterating Case line records  
    {
        system.debug('$$$$$$$$$$$$$$$' + objCaseLine.SVMXC__Installed_Product__c);
        //Checking if Installed product is in Case line and also changing in case of Updating
        if(objCaseLine.SVMXC__Installed_Product__c!=null && (trigger.isInsert || (trigger.isUpdate && objCaseLine.SVMXC__Installed_Product__c != trigger.oldMap.get(objCaseLine.id).SVMXC__Installed_Product__c)))
        {
            set_CaseLine.add(objCaseLine);//putting Case line in set which fulfils the criteria 
            setIPId.add(objCaseLine.SVMXC__Installed_Product__c);//Maintaining Set with Installed Product Ids

            if(objCaseLine.SVMXC__Case__c!=null) //Kaushiki - 18/06/14 - for null check
                setCaseId.add(objCaseLine.SVMXC__Case__c);//Maintaining Set with Case ids   
        }
    }
    //checks if records present after filtering
    if(setCaseId.size() >0 || setCaseId_POno.size() > 0)
    {
        mapCaseId_Case = new map<Id, Case>([select id, Ownerid, AccountId, City__c, SVMXC__Component__c, Contactid, SVMXC__Initial_Response_Customer_By__c,
                        SVMXC__Initial_Response_Internal_By__c, SVMXC__Is_Entitlement_Performed__c, SVMXC__Is_Service_Covered__c, SVMXC__Site__c, ProductSystem__c,
                        SVMXC__Onsite_Response_Customer_By__c, SVMXC__Onsite_Response_Internal_By__c, SVMXC__Perform_Auto_Entitlement__c, BusinessHoursId,
                        Requested_End_Date_Time__c, SVMXC__Preferred_Start_Time__c, Requested_Start_Date_Time__c, SVMXC__BW_Territory__c, SVMXC__Product__c,
                        Priority, Description, SVMXC__Is_SLA_Calculated__c, SVMXC__Resolution_Customer_By__c, SVMXC__PM_Plan__c, SVMXC__Service_Contract__c,
                        SVMXC__Resolution_Internal_By__c, SVMXC__Restoration_Customer_By__c, SVMXC__Restoration_Internal_By__c, SVMXC__SLA_Clock_Paused__c,
                        SVMXC__SLA_Clock_Extension_Minutes__c, SVMXC__SLA_Clock_Pause_Days__c, SVMXC__SLA_Clock_Pause_Hours__c, SVMXC__SLA_Terms__c,
                        SVMXC__SLA_Clock_Pause_Minutes__c, SVMXC__SLA_Clock_Pause_Reason__c, SVMXC__SLA_Clock_Pause_Restart_Time__c, SVMXC__Warranty__c,
                        SVMXC__SLA_Clock_Pause_Time__c, State_Province2__c, Subject, SVMXC__Top_Level__c, SVMXC__Preferred_End_Time__c, Site_Country__c,
                        SVMXC__Service_Contract__r.Name, P_O_Number__c from case where id in:setCaseId OR Id in :setCaseId_POno]);

        if(mapCaseId_Case.size() > 0)
        {
            for(Case varCase : mapCaseId_Case.values())
            {
                if(setCaseId.size() > 0 && setCaseId.contains(varCase.Id))
                {
                    mapCase.put(varCase.Id, varCase);
                }
                //code for DE1687
                if(setCaseId_POno.size() > 0 && setCaseId_POno.contains(varCase.Id) && varCase.P_O_Number__c != null)
                {
                    mapCaseId_POno.put(varCase.Id, varCase.P_O_Number__c);
                }
            }
        }
    }
    //code for DE1687
    if(mapCaseId_POno.size() > 0)
    {
        for(SVMXC__Case_Line__c objCL : trigger.new)
        {
            if(mapCaseId_POno.containsKey(objCL.SVMXC__Case__c))
            {
                objCL.PO_Number__c = mapCaseId_POno.get(objCL.SVMXC__Case__c);
            }
        }
    }

    if(setIPId.size() > 0 && set_CaseLine.size() > 0 && setCaseId.size() > 0 && mapCase.size() > 0){
        system.debug('ServiceTrigger Call for MethodScCaseLineTrigger');
        objServTrig.methodScCaseLineTrigger(set_CaseLine,setIPId,setCaseId, mapCase); //Call MethodScCaseLineTrigger method while passing Input parameters
    }

    if(trigger.isinsert && mapCase.size() > 0){
        objServTrig.prePoulateFldsOnCaseLine(trigger.new,mapCase);
        system.debug('ServiceTrigger Call for PrePoulateFldsOnCaseLine !@#');
    }
   	//pszagdaj/FF, 10/16/2015, DE5875
   	if (trigger.isInsert) {
       objClass.isCaseCancelled(trigger.new);
       objClass.updateWorkOrderField(trigger.new);
   	}
    
   	if(trigger.IsInsert || trigger.IsUpdate){
    	objClass.validateOverLap(trigger.new,trigger.oldmap); 
	}
}
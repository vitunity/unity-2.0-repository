trigger SR_CaseWorkOrderTrigger on Case_Work_Order__c (before insert, before update, after insert, after update, before delete, after delete ) {
    
    If(SR_CaseWorkOrderTriggerRun.isFirstRun()){
        /* Copy Email address from technician(test equipment) */
        if(Trigger.isBefore && Trigger.isInsert){
            SR_CaseWorkOrderTriggerHandler.set_FOA_DM_SC_EmailAddress(Trigger.new);
            SR_CaseWorkOrderTriggerHandler.setCWOOwner(Trigger.new);
        }
        /*Recall Case approval process */
        if(Trigger.isAfter && Trigger.isUpdate){
            SR_CaseWorkOrderTriggerHandler.recallCaseApproval(Trigger.new, Trigger.oldmap);
        }
    }
    
    if(Trigger.isAfter){
        SR_CaseWorkOrderTriggerRun.setFirstRun();
    }
    
}
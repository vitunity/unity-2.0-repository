/*************************************************************************\
      @ Author          : Kaushiki Verma(WIPRO Technologies)
      @ Date            : 8/5/2013
      @ Description     : This Trigger will be used to create a new complaint record under case if Is 'This a Complaint?' of case is 'yes'.  
      @ Last Modified By      :     Chiranjeeb Dhar
      @ Last Modified On      :     04-10-2013
      @ Last Modified Reason  :     To incorporate new complaint flow

    Change log: 
    1-March-2018 - Divya Hargunani - STSK0013858 : Chatter:  Post to Chatter to Feed of Preferred Technician when Case Closed
****************************************************************************/
trigger SR_Case_AfterTrigger on Case (after insert,after update) 
{   

    //STSK0011138.Start
    if(trigger.isUpdate && trigger.isAfter){    
        CaseClosedRecommendation.WorkPerformed(trigger.New,trigger.oldmap);
        //STSK0013858 - Added by Divya Hargunani
        ChatterPostCase.postOnCaseClose(trigger.New,trigger.oldmap);
    }
    //STSK0011138.End
  //Audit Tracking Future Invoke
 if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter) SObjectHistoryProcessor.trackHistory('Case');

     set<Id> Caseids = new set<Id>();
     Map<Id, Case> newCaseMap = new Map<id, Case>();
     Map<Id, Case> oldCaseMap = new Map<id, Case>();
     if(SR_Class_case.CheckRecusrsiveCaseafter == false){    
        for(case ca : trigger.new){
            //SR_Class_case.recordTypeINST : Record Type Id Installation
            if(ca.RecordTypeId != SR_Class_case.recordTypeINST ) // if 'Installation' record type, no need for recording fields value changes, may include other record type as well
            {
                newCaseMap.put(ca.Id, ca);
                if(trigger.IsUpdate){
                    oldCaseMap.put(ca.Id, Trigger.oldMap.get(ca.Id));
                }
            }
            if(ca.recordtypeid == SR_Class_case.Caserecrdtypid || ca.recordtypeid == SR_Class_case.CaserecrdtypidCA )  // For case feed to auto pupulate the values.
            {
                if((trigger.IsInsert && (ca.Local_Subject__c != null || ca.Local_description__c != null || ca.Local_Case_Activity__c != null || ca.Local_Internal_Notes__c != null))|| (trigger.IsUpdate && (ca.Local_Subject__c != trigger.oldmap.get(ca.id).Local_Subject__c || ca.Local_description__c != trigger.oldmap.get(ca.id).Local_description__c || ca.Local_Case_Activity__c != trigger.oldmap.get(ca.id).Local_Case_Activity__c || ca.Local_Internal_Notes__c != trigger.oldmap.get(ca.id).Local_Internal_Notes__c)))
                {
                    Caseids.add(ca.id);
                }
            }
        }
        //Class Instance
        SR_Class_case clsCase= new SR_Class_case();
        SR_Class_case.CheckRecusrsiveCaseafter = true;        
        //Method Calling -After insert After Update
        clsCase.createL2848FromCase(Trigger.new,trigger.Oldmap);                   //Creating L2848 
        clsCase.WorkOrderCreated(trigger.new,trigger.oldmap);                       //added by kaushiki on 12/17/13 for US3668..
        SR_Class_case.CasefeedPHI_caseCommnts(trigger.new,trigger.oldmap);          // case feed phi and case comments

        if(Trigger.isUpdate) {
            clsCase.submitCaseForOOTApproval(trigger.new,trigger.oldmap);
            system.debug('Update Trigger Start------>');
            clsCase.CheckL2848CloseStatus(trigger.new,trigger.oldmap);                  //Method Checking L2848 Status on Case Closure
            clsCase.CalculateEquipmentTimeonIP(trigger.new);   //, trigger.Oldmap         //Method Calculating Eqipment Time On IP
            clsCase.CalculateAnnualDowntimeIP(trigger.new,trigger.oldmap);              // logic added in the method for DE3183
            if(newCaseMap.size() > 0)   // Checking if newCaseMap contains record
            {
             CustomHistoryTrackingEnabler.LogHistoryForLongAndRich(newCaseMap, oldCaseMap);//US1499 : After updating record Log History
            }
            system.debug('Update Trigger End------>');
        } 
        if(Caseids.size() > 0)
        {
            SR_Class_case.Translatethedescrp(Caseids);
        }
    }
}
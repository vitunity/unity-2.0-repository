/*************************************************************************\
      @ Author          : Chiranjeeb Dhar
      @ Date            : 22-Aug-2013
      @ Description     : This Trigger populates the Site respective to Account on Case creation/Updation.
      @ Last Modified By      : Kaushiki Verma    
      @ Last Modified On      : 04-Sep-2013    
      @ Last Modified Reason  : 'Site' field of case is to be populated also according to 'ERP Functional Location' of case and location. 
      @ Last Modified By  :   Shubham Jaiswal
      @ Last Modified On  :    27th March 2014
      @ Last Modified Reason  : US4277 to populate Case fields from SAP data  
      @ Last Modified By  :   Nikita Gupta
      @ Last Modified On  :    31st March 2014
      @ Last Modified Reason  : US3307 to populate Case Preferred Technician Case field from
      @ Last Modified By  :   Nikita Gupta
      @ Last Modified On  :    15th May 2014
      @ Last Modified Reason  : commented 'updatePreferredTechOnCase' method call and merged it with 'populateSiteInCase' method
      @ Last Modified By  :   Nikita Gupta
      @ Last Modified On  :    16th May 2014
      @ Last Modified Reason  : US4119, to update Service/Maintenance Contract, Subject and Product/System fields on case
      @ Last Modified By  :   Nikita Gupta
      @ Last Modified On  :    22nd May 2014
      @ Last Modified Reason  : US4119, to update Service/Maintenance Contract, Subject and Product/System fields on case
      @ Last Modified By  :   Harshita 
      @ Last Modified On  :    18th june 2014
      @ Last Modified Reason  : Added picklist value reference with label value.
       @ Last Modified By  :   Harshdeep Singh
      @ Last Modified On  :    22nd September 2014
      @ Last Modified Reason  : US4765.
    Change Log: 
    21-May-2018 - Nilesh Gorle - STSK0014747 - Avoid recursion
    Nov-27-2017 : Chandra : STSK0012982 : Added new logic for HD Cases where reason is "Installation Assistance"
    Oct-23-2017 : Chandra : STSK0013186 : Added new logic for M2M case record type : 
    Sep-18-2017: Chandra: STSK0012910 : Changed the field data type to accommodate formatting changes
****************************************************************************/
trigger SR_Case_BeforeTrigger on Case (before insert,before update) 
{

    
    SR_Class_case cls = new SR_Class_case();
    /*STSK0011537.Start*/
    if(trigger.isupdate && trigger.isBefore)
        ReopenCase.makeUpdate(trigger.New,trigger.oldmap);
    /*STSK0011537.End*/
    Id CaserecrdtypidHD = RecordTypeUtility.getInstance().rtMaps.get('Case').get('Education');
    Id CaserecrdtypidCS = RecordTypeUtility.getInstance().rtMaps.get('Case').get('Customer_Service');//STSK0014182
    /* STSK0014747 - To avoid recursion, Added flag to execute this trigger once per cycle */
    Boolean isRunOnce = SR_Class_case.runOnce();
    
    if(Trigger.IsBefore && Trigger.IsInsert){   
        /*Rakesh.Start*/
        SR_Class_case.ActiveContactValidation(trigger.new);      
        /*Rakesh.End*/
        //Rakesh.Start : Validation
        ProductSystemValidation.validate(trigger.new,trigger.oldmap);
        //Rakesh.End
        /* 
         *Start Logic to Prevent OOO Cases and to Prevent Internal ?Users from Creating cases through Email to Case 
         *Business logic moved to class SR_Class_case under caseInsertValidation method.
         */
        SR_Class_case.caseInsertValidation(trigger.new);
    
        for(Case ca : trigger.new)
        {    
            if(ca.Origin == 'Email' && ca.recordtypeId == CaserecrdtypidCS) {    //STSK0014182
                ca.Reason = 'waiting on information from requestor';
             }
            
            if(ca.RecordTypeId == CaserecrdtypidHD)
            {
                if(String.isNotBlank(ca.SuppliedEmail))
                {
                    for(Contact con : [select AccountId, Email, Phone from Contact where Email =: ca.SuppliedEmail])
                    {
                        ca.AccountId = con.AccountId;
                        ca.ContactId = con.Id;
                    }
                }

            }
        }
        
    }
    //STSK0011774:Start
        PreferredServiceTeam.UpdateField(trigger.new,trigger.oldmap);
    //STSK0011774:End
    
    
    if(SR_Class_case.CheckRecusrsiveCase == false)  {
        
        SR_Class_case.CheckRecusrsiveCase = true;        
        /* new record type id retrive methods adde for local issues. Record type query remmoved*/ 
        Id Caserecrdtypid = RecordTypeUtility.getInstance().rtMaps.get('Case').get('CA_Customer_Advocacy'); //record type id of Helpdesk Case
        Id CaserecrdtypidHD = RecordTypeUtility.getInstance().rtMaps.get('Case').get('Helpdesk');//record type id of Field Service WO
        //Chandra : Oct 23 : 2017 : Added new logic for M2M case record type : STSK0013186
        Id CaserecrdtypidM2M = RecordTypeUtility.getInstance().rtMaps.get('Case').get('M2M');//record type id of Field Service WO
        
        Set<ID> ACCID = new Set<ID>();
        set<id> caseownersid = new set<id>();
        Set<ID> Set_CaseID = new Set<ID>(); // set to store case id's for filtering WO. added by Nikita US4054
        Set<Case> setCACasetoUpdate = new Set<Case>(); //US4119, Case records of CA record type where erp top level is not null. (Nikita, 5/22/2014)
        Set<String> caErpSitePartrCodeset = new Set<String>();// Shubham 
        Set<String> caErpContactCodeset = new Set<String>();
        Set<Case> caset = new Set<Case>(); //Shubham US4277 to populate Account & Contact lookup acc. to SAP fields starts 
        set<id> setAccountIDS= new set<id>(); //
        map<id,Account> mapAccounts = new map<id,Account>(); //Harshdeep US4765 22 September
        map<id,id> AccBHmap = new map<id,id>();//map for account and business hour id.
        Map<id,QueueSobject> mapofQueue = new map<id,QueueSobject>();
        //Chandra : Oct 23 : 2017 : Added new logic for M2M case record type : STSK0013186
        set<id> m2mcaseipids = new set<id>();
        Map<id,String> M2Mcase2ipname = new Map<Id,String>();
     
        // Refracted code for Case trigger harshita
        for(Case ca: trigger.new)
        {
            if(String.isNotBlank(ca.Case_Queue_Formula__c) && (ca.Priority == 'Emergency' || ca.Priority == 'High' || ca.Priority == 'Medium' || ca.Priority == 'Low'))
            {
                ca.Warning_time__c = System.Now(); // for STRY0063274 by Harvey Li on June 10th, 2018
            }

            system.debug('#######' + ca.ownerId);
            //all the work flow field updates   ---- Harshita 16/10/2014
            if(ca.SVMXC__Actual_Onsite_Response__c != null && ca.Initial_Schedule_Date_Time__c != null)
            {
                ca.Onsite_Response_Time__c = (ca.SVMXC__Actual_Onsite_Response__c.minute() - ca.Initial_Schedule_Date_Time__c.minute())*24;
            }   // Calculate Initial Onsite Response time on case
            if(ca.SVMXC__Scheduled_Date__c != null && ca.Initial_Schedule_Date_Time__c != null)
            {
                ca.Initial_Schedule_Date_Time__c = system.now();
            }   //Set Initial Schedula Date on case
            if(ca.Reason == label.Case_Reason_System_down && ca.Malfunction_Start__c != null && ca.Requested_Start_Date_Time__c == null)
            {
                ca.Requested_Start_Date_Time__c = ca.Malfunction_Start__c;
            }   //SR CaseMalfunctionStartToPreferredStartTime
            // ends here few are in class.          
            
            //Chandra: Sep-18-2017: STSK0012910 : Changed the field data type to accommodate formatting changes
            if (ca.Case_Special_Care_Instructions__c != null)
            {
                ca.Case_Special_Care_Instructions__c = '<font color="Red">'+'<b>'+ca.Case_Special_Care_Instructions__c+'</b>'+'</font>'  ;
            }
            if(ca.recordtypeid == CaserecrdtypidHD || ca.recordtypeid == Caserecrdtypid)  // For case feed to auto pupulate the values.
            { 
                if((trigger.IsInsert && ca.Local_Subject__c != null )|| (trigger.IsUpdate && (ca.Local_Subject__c != trigger.oldmap.get(ca.id).Local_Subject__c || ca.Local_description__c != trigger.oldmap.get(ca.id).Local_description__c || ca.Local_Case_Activity__c != trigger.oldmap.get(ca.id).Local_Case_Activity__c || ca.Local_Internal_Notes__c != trigger.oldmap.get(ca.id).Local_Internal_Notes__c)))
                {
                    ca.subject = ca.Local_Subject__c.ReplaceAll('<[^>]*br>',' ').ReplaceAll('<pre>',' ').ReplaceAll('</pre><<br>',' ');

                    if(ca.Local_description__c != null)
                    {
                        ca.Description = ca.Local_description__c.ReplaceAll('<[^>]*br>',' ').ReplaceAll('<pre>',' ').ReplaceAll('</pre><BR>',' ');
                    }
                    if(ca.Local_Case_Activity__c!=null)
                    {
                        ca.Case_Activity__c = ca.Local_Case_Activity__c.ReplaceAll('<[^>]*br>',' ').ReplaceAll('<pre>',' ').ReplaceAll('</pre><BR>',' ');
                    }
                    if(ca.Local_Internal_Notes__c!=null)
                    {
                        ca.Internal_Comments__c = ca.Local_Internal_Notes__c.ReplaceAll('<[^>]*>',' ');
                    }
                }
            }
            //Chandra : Oct 23 : 2017 : Added new logic for M2M case record type : STSK0013186
            if (ca.recordtypeid == CaserecrdtypidM2M)
            {
                system.debug('***CM case: '+ca);
                if (ca.SVMXC__Component__c != null)
                {
                    m2mcaseipids.add(ca.SVMXC__Component__c);
                }
            }
            if(ca.Reason== Label.Case_Reason_Repair || ca.Reason== Label.Case_Reason_System_down)  
            {
                ca.Type= label.Case_Type_Problem;
            }
            //Added by Nikita, US4119,, 5/22/2014, to update Top Level, Service Maintenance Contract and Subject fields on Ca case record
            if(ca.recordtypeid == CaserecrdtypidHD) //To check if the record type is CA
            {
                if((trigger.isInsert && ca.ERP_Top_Level__c != null) || (trigger.isupdate && ca.ERP_Top_Level__c != null && ca.ERP_Top_Level__c != trigger.oldMap.get(ca.id).ERP_Top_Level__c) || (trigger.isInsert && ca.ProductSystem__c != null) || (trigger.isupdate && ca.ProductSystem__c != null && ca.ProductSystem__c != trigger.oldMap.get(ca.id).ProductSystem__c)) //DE3449, updated from SVMXC__Top_Level__c to ProductSystem__c
                {
                    setCACasetoUpdate.add(ca); //CA case where erp Top Level has value
                }
                //Nov-27-2017 : Chandra : STSK0012982 : Added new logic for HD Cases where reason is "Installation Assistance"
                if(ca.Reason == 'Installation Assistance')
                {
                    ca.Is_This_a_Complaint__c = 'No';
                    ca.Was_anyone_injured__c = 'No';
                    ca.Is_escalation_to_the_CLT_required__c = 'No';
                }   
            }

            Set_CaseID.add(Ca.ID);

            if(ca.Downtime_start__c!=null && ca.Downtime_End__c!=null)
            {
               ACCID.add(ca.accountid);//accid will have id of associated account.
               system.debug('-accid-'+accid);
            }
            //Shubham US4277 to populate Account & Contact lookup acc. to SAP fields starts
            if(ca.ERP_Site_Partner_Code__c != Null || (ca.ERP_Site_Partner_Code__c != NULL && Trigger.oldMap.get(ca.Id).ERP_Site_Partner_Code__c != ca.ERP_Site_Partner_Code__c))
            {
                caErpSitePartrCodeset.add(ca.ERP_Site_Partner_Code__c);
                caset.add(ca);
            }
            if(ca.ERP_Contact_Code__c != Null || (ca.ERP_Contact_Code__c != NULL && Trigger.oldMap.get(ca.Id).ERP_Contact_Code__c != ca.ERP_Contact_Code__c))
            {
                caErpContactCodeset.add(ca.ERP_Contact_Code__c);
                caset.add(ca);
            }
            if(ca.ERP_Contact_Code__c != Null || (ca.ERP_Contact_Code__c != NULL && Trigger.oldMap.get(ca.Id).ERP_Contact_Code__c != ca.ERP_Contact_Code__c))
            {
                caErpContactCodeset.add(ca.ERP_Contact_Code__c);
                caset.add(ca);
            }
            if(trigger.Isinsert && ca.Origin == 'Email')
            {
                //ca.Priority = 'Medium';
                IF(ca.recordtypeId == CaserecrdtypidHD)// kaushiki DE1435 since the body of mail bydefault will be attached to description and same has to be taken to local description also
                {
                    ca.Local_Description__c = ca.Description.replaceAll('<','(').replaceAll('>',')').replaceAll('\n','<br/>');
                    ca.Local_Subject__c = ca.Subject;
                }
            }
            /**************** US4765 starts added by Harsheep 22 September 2014 ********************/
            if((trigger.Isinsert && ca.OwnerId != null) || (trigger.IsUpdate && trigger.oldMap != null && ca.ownerId != trigger.oldMap.get(ca.id).ownerId))
            {
                if (string.valueof(ca.ownerId).startswith(user.sObjectType.getDescribe().getKeyPrefix()) && ca.AccountId != null)
                {               
                    setAccountIDS.add(ca.AccountId);   
                }
                else
                {
                    caseownersid.add(ca.ownerid);
                }                 
            }
            /****************US4765 by Harshdeep ends********************/

        }   //US1184 

        //Queue functionality
        if(caseownersid  != null && caseownersid.size() > 0)
        {
            for(QueueSobject lstQueueName : [Select QueueId,Queue.Name from QueueSobject where SobjectType = 'Case' AND QueueId in: caseownersid])
            {
                mapofQueue.put(lstQueueName.QueueId,lstQueueName);
            }
        }
        mapAccounts = new map<id,Account>(); //DE7370
        for(account a : [select SVMXC__Access_Hours__c, Country1__r.Case_queues__c, name, id from account where id in : ACCID or id in : setAccountIDS ]) //DE8020
        { //this if is used to prevent exception.               
            if(a.SVMXC__Access_Hours__c != null)//if access hours in account is null, map will not add any value in it.
            { 
                AccBHmap.put(a.id,a.SVMXC__Access_Hours__c);
            }
            mapAccounts.put(a.id,a); //DE8020
        }


        /* DE8020: consolidated with above
        if(setAccountIDS.size() > 0) // US4765
        {
            mapAccounts = new map<id,Account>([select id, Country1__r.Case_queues__c from Account where id in : setAccountIDS]);      
        }*/
        //Chandra : Oct 23 : 2017 : Added new logic for M2M case record type : STSK0013186
        if (m2mcaseipids.size() > 0)
        {
            for (SVMXC__Installed_Product__c ip : [select id, name from SVMXC__Installed_Product__c where id in : m2mcaseipids])
            {
                M2Mcase2ipname.put(ip.id,ip.name);
            }
        }
        for(case ca:trigger.new)
        {
        
            //Chandra : Oct 23 : 2017 : Added new logic for M2M case record type : STSK0013186
            if (M2Mcase2ipname.size() > 0 && ca.SVMXC__Component__c != null && M2Mcase2ipname.containskey(ca.SVMXC__Component__c) && ca.accountID != null)
            {
                string firstname = M2Mcase2ipname.get(ca.SVMXC__Component__c);
                contact con;
                //M2M case will be created one by one and no data loader will be done for M2M records : Chandra 
                for (Contact c : [select id,Phone from Contact where Account.id = :ca.accountid and lastname like '%console%' and Firstname = :firstname limit 1])
                {
                   con = c;
                }
                if (con != null)
                {
                    ca.contactId = con.Id;
                    //ca.ContactPhone = con.phone;
                }
            }
            if(ca.Downtime_start__c!=null && ca.Downtime_End__c!=null )
            {
                if(AccBHmap.size()>0 && AccBHmap!=null && AccBHmap.get(ca.Accountid)!= null )
                {
                    //this function is used to calculate outage time of case.
                    ca.outage_time__c=(businesshours.diff(AccBHmap.get(ca.accountid),ca.Downtime_start__c,ca.Downtime_end__c))/60000;//outage time is calculated in minutes.
                    system.debug('-----Outage Time----' + ca.outage_time__c*60000);
                }
            }
            system.debug('ca.SVMXC__Site__c5>>>>'+ca.SVMXC__Site__c);
            
            if(setAccountIDS.contains(ca.AccountId) && mapAccounts.containskey(ca.AccountId) && mapAccounts.get(ca.AccountId).Country1__r.Case_queues__c != null) // US4765 //DE8020
            {
                ca.case_group__c =  mapAccounts.get(ca.AccountId).Country1__r.Case_queues__c;
            }
            //Queue Logic
            if((trigger.Isinsert && ca.OwnerId != null) || (trigger.IsUpdate && trigger.oldMap != null && ca.ownerId != trigger.oldMap.get(ca.id).ownerId))
            {
                if (string.valueof(ca.ownerId).startswith(user.sObjectType.getDescribe().getKeyPrefix()) && ca.AccountId != null)
                {               
                    //setAccountIDS.add(ca.AccountId);   
                }
                else
                {
                    if(mapofQueue != Null && mapofQueue.size() > 0 && ca.ownerId != null && mapofQueue.get(ca.ownerId).Queue.Name != Null)
                    {
                        ca.case_group__c = mapofQueue.get(ca.ownerId).Queue.Name;
                    }
                }                 
            }
        }       

        if(isRunOnce) {
            /******************** METHOD CALLS ************************************************************/
            cls.populateSiteInCase(caErpSitePartrCodeset,caErpContactCodeset,caset); // rest all field updates are in below method. -- Harshita 16/10/2014
            cls.updateCaseFields(trigger.new, trigger.oldmap); //updated by Nikita, 5/16/2014, Added oldmap to the method call
            /**********************************************************************************************/
        }
           
        if(Trigger.ISInsert)
        {
            if(setCACasetoUpdate.size() > 0)
            {
                cls.updateCACaseFields(setCACasetoUpdate); //method call for US4119, to update IP, Contract and Subject for case of CA recordtype
            }
            //Below method moved out of recursive, DE3826
            //cls.setMalfunctionDowntimeStartOnCase(trigger.new,'ins');//kaushiki US3860 5-feb-14
        }
        else if(trigger.isUpdate)
        {
            //Added by Nikita, US4119,, 5/22/2014, to update Top Level, Service Maintenance Contract and Subject fields on Ca case record
            if(setCACasetoUpdate.size() > 0)
            {
                cls.updateCACaseFields(setCACasetoUpdate); //method call for US4119, to update IP, Contract and Subject for case of CA recordtype
            }
            for(case varCase : trigger.new)
            {
                system.debug('trigger.oldMap.get(varCase.id).Status'+trigger.oldMap.get(varCase.id).Status);
                system.debug(' varCase.Status'+ varCase.Status);
                if(trigger.oldMap.get(varCase.id).Status!= 'Closed' && (varCase.Status == 'Closed' || varCase.Status =='Closed by Work Order') && trigger.oldMap.get(varCase.id).Status!= varCase.Status )
                {                  
                   varCase.ClosedBy__c = userinfo.getUserId();
                }
            }
            //Below method moved out of recursive, DE3826
            //cls.setMalfunctionDowntimeStartOnCase(trigger.new,'Upd');//kaushiki US3860 5-feb-14  
        }  
    }

    //DE3826 - Need to bring this out side recursive loop as Account Id is not set on case for Email to case un till Before trigger finishes execution, so this method be called again in update to set the customer malfunction fields
    if(Trigger.isInsert && isRunOnce)
    {
        cls.setMalfunctionDowntimeStartOnCase(trigger.new,'ins');//kaushiki US3860 5-feb-14
    }

    if(trigger.isUpdate && isRunOnce)
    {
        cls.setMalfunctionDowntimeStartOnCase(trigger.new,'Upd');//kaushiki US3860 5-feb-14     
    }
    if(trigger.isBefore && trigger.isUpdate && isRunOnce) {
        cls.setPHIInfo(trigger.new);
    }
    
    //STSK0014174: Update Site on Case
    if(trigger.isBefore && (trigger.isUpdate || trigger.isInsert)){
        SiteLocationUpdate.updateSite(trigger.new,trigger.oldmap);
    }
    //Start Add Manager Email- STSK0013404
    if(trigger.isBefore && trigger.isUpdate) {
        set<Id> setOwnerIds = new set<Id>();
        for(Case c: trigger.New){
            if(c.Case_Manager_Email__c <> null && c.Case_Manager_Email__c <> c.Case_Owner_Manager_Email__c){
                setOwnerIds.add(c.OwnerID);
            }
        }
        if(setOwnerIds.size()>0){
            map<Id,User> mapUser = new map<Id,User>([select Id,Manager.Email from User Where Id IN: setOwnerIds and ManagerId <> null ]);
            for(Case c: trigger.New){
                if(c.Case_Manager_Email__c <> null && c.Case_Manager_Email__c <> c.Case_Owner_Manager_Email__c){
                    if(mapUser.containskey(c.OwnerId)){
                        c.Case_Owner_Manager_Email__c = mapUser.get(c.OwnerId).Manager.Email;
                    }
                }
            }
        }
    }
    //End 
}
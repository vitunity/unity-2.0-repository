/*
//11-Jul-2018, STRY0067325:  Add Active check for user to avoid validation error.  
 */

trigger SR_ContactRoleAssoAfterTrigger on Contact_Role_Association__c (after insert, after update, after delete) 
{
    if(Trigger.isInsert || Trigger.isUpdate)
    {
        Map<Id, Id> accountMap = new Map<Id, Id>();
        Map<id, Invoice__c> inv2quote = new map<id,Invoice__c>();
        List<AccountShare> shares = new List<AccountShare>();
        map<id,Id> contact2user = new Map<id,ID>();
        List<Bigmachines__Quote__Share> quoteshares = new List<Bigmachines__Quote__Share>();
        map<Id, Boolean> mapRoleIdConActPay = new map<Id, Boolean>();
        map<Id, Boolean> mapRoleIdConMyVar = new map<Id, Boolean>();
        for(Contact_Role_Association__c cRol : [SELECT Id, Contact__r.Accts_Payable_Contact__c, Contact__r.MyVarian_Member__c 
                FROM Contact_Role_Association__c WHERE Id IN : Trigger.NewMap.keySet() ]) {
             mapRoleIdConActPay.put(cRol.Id, cRol.Contact__r.Accts_Payable_Contact__c);
             mapRoleIdConMyVar.put(cRol.Id, cRol.Contact__r.MyVarian_Member__c); 
        }    
        
        if(Trigger.isInsert)
        {
            for(Contact_Role_Association__c cra : Trigger.new)
            {
                //if(cra.Role__c == 'Account Payable Capital' || cra.Role__c == 'Account Payable Service' || cra.Role__c == 'Account Payable Capital & Service')
                //{
                    accountMap.put(cra.Contact__c, cra.Account__c);
                //}
            }
        }
        
        if(Trigger.isUpdate)
        {
            for(Contact_Role_Association__c cra : Trigger.new)
            {
                System.debug('#### debug contact Id = ' + Trigger.OldMap.get(cra.Id).Contact__c);
                System.debug('#### debug contact Act payable = ' + mapRoleIdConActPay.get(cra.Id));
                System.debug('#### debug contact MyVarian = '+ mapRoleIdConMyVar.get(cra.Id));
                if(Trigger.OldMap.get(cra.Id).Contact__c != cra.Contact__c 
                    || (Trigger.OldMap.get(cra.Id).Contact__c == cra.Contact__c 
                         && mapRoleIdConActPay.get(cra.Id) == true && mapRoleIdConMyVar.get(cra.Id) == true     //Role__c != 'Account Payable Capital' 
                         //&& Trigger.OldMap.get(cra.Id).Role__c != 'Account Payable Service' 
                         //&& Trigger.OldMap.get(cra.Id).Role__c != 'Account Payable Capital & Service'
                       )
                    /*&& (cra.Role__c == 'Account Payable Capital' || cra.Role__c == 'Account Payable Service' || cra.Role__c == 'Account Payable Capital & Service')*/
                  )
                
                {
                    accountMap.put(cra.Contact__c, cra.Account__c);
                    System.debug('#### accountMap: '+accountMap);
                }
            }
        }

        //Site Partner sharing was to be removed per input from business
        for (Invoice__c inv : [select id, Quote__c, site_partner__c , sold_to__c from Invoice__c where /*site_partner__c in : accountMap.values() or */sold_to__c in :accountMap.values()])
        {
            inv2quote.put(inv.id, inv);
        }
        System.debug('Invoice List: '+inv2quote);
        //Add Active check for User to avoid insertion errors on isActive = false
        for(User user : [select Id, ContactId from User where ContactId in: accountMap.keySet() AND isActive=true])
        {
            contact2user.put(user.contactId, user.id);
            AccountShare share = new AccountShare();
            share.AccountId = accountMap.get(user.ContactId);
            share.UserOrGroupId = user.Id;
            share.AccountAccessLevel = 'Read';
            share.OpportunityAccessLevel = 'Read';
            shares.add(share);
        }
        if (trigger.isinsert)
        {
            for (Contact_Role_Association__c cra : trigger.new)
            {
                for (id invid : inv2quote.keySet())
                {
                    //Site Partner sharing was to be removed per input from business
                    if (/*cra.Account__c == inv2quote.get(invid).site_partner__c || */cra.Account__c == inv2quote.get(invid).sold_to__c)
                    {
                        //added check for MyVarian and Accounts Payable contact before adding quote share - only for DSO project
                        if (inv2quote.get(invid).Quote__c != null && contact2user.get(cra.Contact__c) != null && mapRoleIdConActPay.get(cra.Id) == true && mapRoleIdConMyVar.get(cra.Id) == true)
                        {
                            Bigmachines__Quote__Share qshare = new Bigmachines__Quote__Share();
                            qshare.ParentId = inv2quote.get(invid).Quote__c;
                            qshare.UserOrGroupID = contact2user.get(cra.Contact__c);
                            qshare.AccessLevel = 'Read';
                            qshare.RowCause = 'Manual';
                            quoteshares.add(qshare);
                            System.debug('quoteShares:' + quoteshares);
                        }
                    }
                }
            }    
        }
        
        if(shares.size() > 0)
        {
            insert shares;
        }
        if(quoteshares.size() > 0)
        {
            System.debug('QuoteShares: '+quoteshares);
            insert quoteshares;
        }
    }
    if(Trigger.isDelete || Trigger.isUpdate)
    {
        Map<Id, Id> accountMap = new Map<Id, Id>();
        List<Bigmachines__Quote__c> quotes = new List<Bigmachines__Quote__c>();
        Set<Id> quoteSet = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        List<AccountShare> shares = new List<AccountShare>();
        List<Bigmachines__Quote__Share> quoteShares = new List<Bigmachines__Quote__Share>();
        //Add BMI Quotation Share deletion
        
        if(Trigger.isDelete)
        {
            for(Contact_Role_Association__c cra : Trigger.old)
            {
                accountMap.put(cra.Contact__c, cra.Account__c);
                System.debug('accountMap for Deletion: '+accountMap);
            }
        }
        else
        {
            map<Id, Boolean> mapRoleIdConActPay1 = new map<Id, Boolean>();
            map<Id, Boolean> mapRoleIdConMyVar1 = new map<Id, Boolean>();
            for(Contact_Role_Association__c cRol : [SELECT Id, Contact__r.Accts_Payable_Contact__c, Contact__r.MyVarian_Member__c 
                                FROM Contact_Role_Association__c WHERE Id IN : Trigger.NewMap.keySet() ]) {
                mapRoleIdConActPay1.put(cRol.Id, cRol.Contact__r.Accts_Payable_Contact__c);
                mapRoleIdConMyVar1.put(cRol.Id, cRol.Contact__r.MyVarian_Member__c);
            }            
            for(Contact_Role_Association__c cra : Trigger.new)
            {
                System.debug('#### debug contact Id = ' + Trigger.OldMap.get(cra.Id).Contact__c);
                System.debug('#### debug contact Act payable = ' + mapRoleIdConActPay1.get(cra.Id));  
                System.debug('#### debug contact MyVarian = ' + mapRoleIdConMyVar1.get(cra.Id)); 
                if(Trigger.OldMap.get(cra.Id).Contact__c != cra.Contact__c)
                {
                    accountMap.put(Trigger.OldMap.get(cra.Id).Contact__c, Trigger.OldMap.get(cra.Id).Account__c);
                }
                //if(Trigger.OldMap.get(cra.Id).Contact__c == cra.Contact__c && Trigger.OldMap.get(cra.Id).Role__c != cra.Role__c && cra.Role__c != 'Account Payable Capital' && cra.Role__c != 'Account Payable Service' && cra.Role__c != 'Account Payable Capital & Service')
                if(Trigger.OldMap.get(cra.Id).Contact__c == cra.Contact__c && 
                   (Trigger.OldMap.get(cra.Id).Contact__r.Accts_Payable_Contact__c == true && Trigger.OldMap.get(cra.Id).Contact__r.MyVarian_Member__c == true)
                    //&& Trigger.OldMap.get(cra.Id).Role__c != cra.Role__c 
                    //&& cra.Role__c != 'Account Payable Capital' && cra.Role__c != 'Account Payable Service' && cra.Role__c != 'Account Payable Capital & Service')
                    && (mapRoleIdConActPay1.get(cra.Id) != true || mapRoleIdConMyVar1.get(cra.Id) != true) )
                {
                    accountMap.put(cra.Contact__c, cra.Account__c);
                }
            }
        }
		System.debug('accountMap: '+accountMap);
        System.debug('accountMapKey: '+accountMap.keySet());
        for(User user : [select Id, ContactId from User where ContactId in: accountMap.keySet()])
        {
            userIds.add(user.Id);
        }
        for(AccountShare share : [select AccountId, UserOrGroupId from AccountShare where AccountId in: accountMap.values() AND ROWCAUSE IN ('Manual', 'Manual Sharing')])
        {
            if(userIds.contains(share.UserOrGroupId))
            {
                shares.add(share);
            }
        }
        //If CRA Deleted or Contact is changed, then AccountMap is populated.  If not empty, then get Quotes for these Accounts to clear Sharing.
        If(!accountMap.keySet().isEmpty() && !accountMap.values().isEmpty()){

            /*
            quotes = [SELECT Id FROM Bigmachines__Quote__c WHERE BigMachines__Account__c in: accountMap.values()];
            for (Bigmachines__Quote__c quo: quotes){
                quoteSet.add(quo.Id);
            }
            System.debug('QuotesSet: '+quoteSet);
            */
            for (Invoice__c inv : [select id, Quote__c, site_partner__c , sold_to__c from Invoice__c 
                where site_partner__c in : accountMap.values() or sold_to__c in :accountMap.values()])
            {
                quoteSet.add(inv.Quote__c);
            }  
            System.debug('QuotesSet for deletion : '+quoteSet);          
        }
        //Where Quote Share is found, add to deletion list.
        for(Bigmachines__Quote__Share quoteShare: [SELECT ParentId, UserOrGroupId FROM Bigmachines__Quote__Share WHERE ParentId in:quoteSet AND UserOrGroupId in:userIds AND ROWCAUSE IN ('Manual', 'Manual Sharing')])
        {
           quoteShares.add(quoteShare);
        }

        System.debug('shares for deletion : '+shares);
        if(shares.size() > 0)
        {
            delete shares;
        }

        System.debug('quoteShares for deletion : '+quoteShares);
        if(quoteShares.size() > 0)
        {
            delete quoteShares;
        }
    }
}
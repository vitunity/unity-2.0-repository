/*****Kaushiki 23/1/15 DE2427 Populationg default values of phone email and role from contact*****/
trigger SR_ContactRoleAssoBeforeTrigger on Contact_Role_Association__c (Before insert) 
{
    set<Id> ContactId = new set<Id>();
    for(Contact_Role_Association__c VarCRA : Trigger.new)
    {
        if(VarCRA.Contact__c!=null)
        ContactId.add(VarCRA.Contact__c);
    }
    
    Map<Id,Contact> ContactMap = new Map<id,contact>([select id,name,Functional_Role__c,Phone,Email from contact where id in : ContactId]);
    for(Contact_Role_Association__c VarCRA : Trigger.new)
    {
        if(ContactMap.containsKey(VarCRA.Contact__c) )
        {
            if(ContactMap.get(VarCRA.Contact__c).Functional_Role__c != null && VarCRA.Role__c == null)
            {
                VarCRA.Role__c = ContactMap.get(VarCRA.Contact__c).Functional_Role__c;
            }
            if(ContactMap.get(VarCRA.Contact__c).Phone != null && VarCRA.Site_Phone_Number__c == null)
            {
                VarCRA.Site_Phone_Number__c = ContactMap.get(VarCRA.Contact__c).Phone;
            }
            if(ContactMap.get(VarCRA.Contact__c).Email != null)
            {
                VarCRA.Site_Email__c = ContactMap.get(VarCRA.Contact__c).Email;
            }
        }
    }
}
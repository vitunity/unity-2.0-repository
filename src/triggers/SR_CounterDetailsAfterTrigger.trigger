/*************************************************************************\
@ Author : Shubham Jaiswal  
@ Date: 24/03/2014
@ Description  : US4287 
@ Last Modified By: 
@ Last Modified On: 
@ Last Modified Reason  : 
/**************************************************************************/


trigger SR_CounterDetailsAfterTrigger on SVMXC__Counter_Details__c (after update, after insert) 
{
   if(trigger.isinsert)
   {
       List<SVMXC__Counter_Details__c> cdlst = new List<SVMXC__Counter_Details__c>();
        system.debug('trigger.new----------->' + trigger.new.size());
       for(SVMXC__Counter_Details__c cd : trigger.new)
       {
           system.debug('recordtype----------->' + cd.recordtypeid );
             system.debug('contract----------->' + cd.SVMXC__Service_Maintenance_Contract__c);
             //Below Line commented by MOHIT : Synch of Reading Counter Detail for coverage from SOI/Interface US4951
            //if(Schema.SObjectType.SVMXC__Counter_Details__c.getRecordTypeInfosByName().get('Coverage').getRecordTypeId() == cd.recordtypeid  && cd.SVMXC__Service_Maintenance_Contract__c  != NULL)
              if(Schema.SObjectType.SVMXC__Counter_Details__c.getRecordTypeInfosByName().get('Coverage').getRecordTypeId() == cd.recordtypeid )
                cdlst.add(cd);
       }
       
       SR_ClassCounterDetails obj = new SR_ClassCounterDetails();
       system.debug('cdlst----------->' + cdlst.size());
       if(cdlst.size() > 0)
       {
            obj.CloneCoverageCD(cdlst);
       }
   }
}
/******************************************************
    Author      :   Bushra Begum
    Created on  :   22nd-May-2014
    Created Reason: This Trigger include logic to create/update record whenever a counter detail is created or updated
******************************************************/
trigger SR_CounterDetailsBeforeTrigger on SVMXC__Counter_Details__c (Before insert, before update)
{
    SR_ClassCounterDetails clsCD = new SR_ClassCounterDetails(); //initiating Class
    
    if(trigger.isInsert || trigger.isUpdate)
    {
        clsCD.beforeUpdateFieldsCounterDetails(Trigger.new, trigger.oldMap); //US4582, Method call to update installed Product
    }
}
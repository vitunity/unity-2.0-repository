/**************************************************************************
    @Last Modified By    :    Nikita Gupta
    @Last Modified On    :    4/30/2014
    @Last Modified Reason:    Renamed from 'AfterInsertUpdate_CoveredProd' to 'SR_CoveredProduct_AfterTrigger'
    @Last Modified Reason:    US4473, To populate Account field from ERP Partner Association if not found in Account.
    @Last Modified By    :    Nikita Gupta
    @Last Modified On    :    5/2/2014
    @Last Modified Reason:    Removed code written for US4473
****************************************************************************/
trigger SR_CoveredProduct_AfterTrigger on SVMXC__Service_Contract_Products__c (after insert, after update)
{
    /************* Below code commented for Wave 1C, Parul, DE3900, 3/25/15
        List<SVMXC__Service_Contract_Products__c> lstOfCovrdProdToCreatePMCoverage = new List<SVMXC__Service_Contract_Products__c>();

        for(SVMXC__Service_Contract_Products__c covrdProd : trigger.new)
        {
            if(covrdProd.SVMXC__Service_Contract__c != null && covrdProd.Status__c == true) //if condition changed for DE3900
            {
                lstOfCovrdProdToCreatePMCoverage.add(covrdProd);
            }
        }

        //**************** US1897, DO NOT BULKIFY THIS METHOD, WILL RUN ONLY FOR SINGLE BATCH **************************
            if(trigger.isinsert)
            {
                if(lstOfCovrdProdToCreatePMCoverage.size() == 1)
                {
                    SR_CoveredProduct.createPMCoverage(lstOfCovrdProdToCreatePMCoverage); 
                }
            }
        //**************** US1897, DO NOT BULKIFY THIS METHOD, WILL RUN ONLY FOR SINGLE BATCH *************************

    Code commented for Wave 1C, Parul, DE3900, 3/25/15 ***********************/
     ////CM : DE7665 : Uncommented the below code to handle the install product updates on Service Contract changes. 
     SR_CoveredProduct.updateInstProd(Trigger.new); //This line is after the if statement in production
    if(trigger.isUpdate)
    {
        /*********************** Code commented for Wave 1C, Parul, DE3900, 3/25/15
            SR_CoveredProduct.createPMCoverage(lstOfCovrdProdToCreatePMCoverage);   //method call added to isUpdate block for DE3900
        Code commented for post Wave 1C, Parul, DE3900, 3/25/15 ***********************/

        SR_Class_CoveredProduct.updateCDFromCoveredProduct(trigger.new, trigger.oldmap);// US4769/US4082 Cancellation Covered product mohit Sharma 17-oct-2014
    }
    
     /* PMP and PMI Coverage 
    if(Trigger.isAfter && (Trigger.isUpdate || Trigger.isinsert) ) {

        PMPPMIHandler.handle([Select SVMXC__Installed_Product__c,
                            SVMXC__Installed_Product__r.SVMXC__Top_Level__c,
                            Status__c, Reason_for_rejection__c
                            from SVMXC__Service_Contract_Products__c
                            where Id in: trigger.new and SVMXC__Service_Contract__c != null], Trigger.oldmap);
        
    }
    */ 
}
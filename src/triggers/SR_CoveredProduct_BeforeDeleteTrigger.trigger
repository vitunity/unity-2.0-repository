trigger SR_CoveredProduct_BeforeDeleteTrigger  on SVMXC__Service_Contract_Products__c(before delete) {
    set<Id> setCoveredProductIds = new set<Id>();
    for(SVMXC__Service_Contract_Products__c s : trigger.old){
        setCoveredProductIds.add(s.Id);
    }
    
    List<SVMXC__Installed_Product__c> lstInstalledProduct = [select Id,SVMXC__Service_Contract__c,SVMXC__Service_Contract_Start_Date__c,SVMXC__Service_Contract_End_Date__c 
    from SVMXC__Installed_Product__c where SVMXC__Service_Contract_Line__c IN:setCoveredProductIds ];
    
    for(SVMXC__Installed_Product__c  p: lstInstalledProduct ){   p.SVMXC__Service_Contract__c = null;    p.SVMXC__Service_Contract_Start_Date__c= null;p.SVMXC__Service_Contract_End_Date__c= null;
    }
    
    if(lstInstalledProduct.size()>0){   update lstInstalledProduct;
    }
}
/**************************************
Last Modified By    :   Nikita Gupta
Last Modified On    :   4/29/2014
Last Modified Reason:   US4473, to Populate Account On Covered Products based on ERP Site Partner
Last Modified By    :    Nikita Gupta
Last Modified On    :    4/30/2014
Last Modified Reason:    Removed code for US4473 and added in after trigger.
Last Modified By    :    Nikita Gupta
Last Modified On    :    5/2/2014
Last Modified Reason:    Added an if check to fix a defect
Last Modified By    :    Nikita Gupta
Last Modified On    :    5/2/2014
Last Modified Reason:    US4473, Added code for calling method to update Site Partner on Covered Products based on ERP Site Partner.
Last Modified By    :    Kaushiki - 18/06/14 - Null Check
**************************************/

trigger SR_CoveredProduct_BeforeTrigger on SVMXC__Service_Contract_Products__c (before insert,before update)
{
    //Code added for 1A-1C merge by Shradha on 05/14/2015
    //CM : DE7665 : Uncommented the below code to handle the install product updates on Service Contract changes. 
    SR_coveredProduct objCovProd  = new  SR_coveredProduct();//Added by Nikita, US4473 on 5/2/2014
    objCovProd.updateCoveredProductFields(trigger.new, trigger.oldmap); //3/10/2015, Moved all the code from before trigger to Covered Product Class, Nikita
    
    
    /******Code commented for 1A-1C merge by Shradha on 05/14/2015
    Set<Id> SetofServiceMaintenanceContractId = new Set<Id>();
    Set<Id> SetofAccountId = new Set<Id>();
    //Set<Id> ProductId = new Set<Id>();
    Set<String> AccountNumber = new Set<String>();
    Set<String> ERPFunctionalLocation = new Set<String>();
    map<string,id> InstalledProdMap = new map<string,id>();
    Set<String> ProductCode = new Set<String>();
    Map<Id,SVMXC__Service_Contract__c > MapofAccount = new Map<Id,SVMXC__Service_Contract__c >();
    Map<String,SVMXC__Site__c > MapofSite = new Map<String,SVMXC__Site__c >();
    Map<String,Product2> MapofProduct = new Map<String,Product2>();
    Set<string> serialNoPCSNs = new Set<String>();//added by ritika
    Set<string> SetofERPSitePartner = new Set<string>();
    Public ID InstalledProdId,SitePartnerId ;
    Public String ERPShipTo,ERPBilltoName,ERPSitePartnerName,ERPLocationName;
    List<SVMXC__Site__c> ListofSite = new List<SVMXC__Site__c> ();
    List<Product2> ListofProduct = new List<Product2> ();
    List<SVMXC__Installed_Product__c> ListofIP = new List<SVMXC__Installed_Product__c> ();
    List<Account> ListofAccount = new List<Account> ();
    List<Account> ListofERPAccounts = new List<Account> ();
    List<SVMXC__Service_Contract_Products__c> listCovProd = new List<SVMXC__Service_Contract_Products__c>(); //For US4473. List to store Covered Product records where ERP Site Partner has value. 
        
    for(SVMXC__Service_Contract_Products__c covered: Trigger.New)
    {   if(covered.SVMXC__Service_Contract__c!=null)
        SetofServiceMaintenanceContractId.add(covered.SVMXC__Service_Contract__c);
        //ProductId.add(covered.SVMXC__Service_Contract__c);//Kaushiki - 18/06/14 - Not used in the code
        
        if(covered.ERP_Functional_Location__c != null) //Check added by Nikita on 5/2/2014 to filter out false values
        {
            ERPFunctionalLocation.add(covered.ERP_Functional_Location__c);
        }
        if(covered.Product_Code__c != NULL)   
            ProductCode.add(covered.Product_Code__c); 
        if(covered.Serial_Number_PCSN__c != NULL)
            serialNoPCSNs.add(covered.Serial_Number_PCSN__c);
        if(covered.ERP_Site_Partner__c != NULL)
            SetofERPSitePartner.add(covered.ERP_Site_Partner__c);
        
         //Added by Nikita, US4473 on 5/2/2014
        if((trigger.IsInsert && covered.ERP_Site_Partner__c != null) || (trigger.IsUpdate && covered.ERP_Site_Partner__c != null && covered.ERP_Site_Partner__c != trigger.oldmap.get(covered.ID).ERP_Site_Partner__c))
        {
            listCovProd.add(covered);
        }
    }
    
     //Added by Nikita, for US4473, Creating object for class to update Site Partner on Covered Products based on ERP Site Partner.
    if(listCovProd.size() > 0)
    {
        SR_coveredProduct cls  = new  SR_coveredProduct();//Added by Nikita, US4473 on 5/2/2014
        cls.updateSitePartner(listCovProd);
    }
        
    List<SVMXC__Service_Contract__c> ListofAccountsinServiceContract = new List<SVMXC__Service_Contract__c>();
    if(SetofServiceMaintenanceContractId.size()>0)//Kaushiki - 18/06/14 - Null Check
    {
        ListofAccountsinServiceContract =[Select Id,SVMXC__Company__c from SVMXC__Service_Contract__c where Id in:SetofServiceMaintenanceContractId] ;
    }
    if(ERPFunctionalLocation.size()>0)  //Kaushiki - 18/06/14 - Null Check
    {
        ListofSite = [Select Id,ERP_Functional_Location__c from SVMXC__Site__c where ERP_Functional_Location__c in: ERPFunctionalLocation ];
    }
    if(ProductCode.size()>0)//Kaushiki - 18/06/14 - Null Check
    {
         ListofProduct = [Select Id,ProductCode from Product2 where ProductCode in:ProductCode and ProductCode!=Null];
    }
    if(serialNoPCSNs.size()>0 )//Kaushiki - 18/06/14 - Null Check
    {
         ListofIP = [Select Id,Name,SVMXC__Serial_Lot_Number__c from SVMXC__Installed_Product__c where SVMXC__Serial_Lot_Number__c IN :serialNoPCSNs AND Name IN :serialNoPCSNs ];//added by ritika //modified by shubham in both SFSIT & SFPC2 12/5/2014
    }
    List<Account> ListERPSitePartnerAcnt = new List<Account>();
    if(SetofERPSitePartner.size()>0)//Kaushiki - 18/06/14 - Null Check
    {
        ListERPSitePartnerAcnt = [Select ID,Name,AccountNumber from Account where AccountNumber in : SetofERPSitePartner];
    }
    if(ListofIP.size()>0)//Kaushiki - 18/06/14 - Null Check
    {
        for(SVMXC__Installed_Product__c IPalias :ListofIP )
        {
            InstalledProdId = IPalias.Id;
            if(IPalias.SVMXC__Serial_Lot_Number__c!= null)
                InstalledProdMap.put(IPalias.SVMXC__Serial_Lot_Number__c,IPalias.id);   
        }
    }
    if(ListERPSitePartnerAcnt.size()>0)//Kaushiki - 18/06/14 - Null Check
    {
        for(Account Accalias:ListERPSitePartnerAcnt )
        {
            SitePartnerId = Accalias.Id;  
            if(Accalias.AccountNumber!=null)//Kaushiki - 18/06/14 - Null Check
            ERPLocationName = Accalias.AccountNumber;
        }
    }
   if(ListofSite.size()>0)//Kaushiki - 18/06/14 - Null Check
   {
       for(SVMXC__Site__c sitealias: ListofSite)
       {
          if(sitealias.ERP_Functional_Location__c!=null)//Kaushiki - 18/06/14 - Null Check
          MapofSite.put(sitealias.ERP_Functional_Location__c,sitealias); 
       }
   }
   if(ListofAccountsinServiceContract.size()>0)//Kaushiki - 18/06/14 - Null Check
   {
       for(SVMXC__Service_Contract__c contract : ListofAccountsinServiceContract )
        { 
            if(contract.SVMXC__Company__c!=null)//Kaushiki - 18/06/14 - Null Check
            {
                SetofAccountId.add(contract.SVMXC__Company__c );
            }
            MapofAccount.put(contract.Id,contract);
        }
    }
    if(ListofProduct.size()>0)//Kaushiki - 18/06/14 - Null Check
    {
        for(Product2 productalias :ListofProduct)
        {
           if(productalias.ProductCode!=null)//Kaushiki - 18/06/14 - Null Check
           MapofProduct.put(productalias.ProductCode,productalias);
        }   
    }
    if(SetofAccountId.size()>0)//Kaushiki - 18/06/14 - Null Check
    {
        ListofAccount = [Select AccountNumber,Id,RecordType.DeveloperName,Name from Account where id in:SetofAccountId];
        if(ListofAccount.size()>0)//Kaushiki - 18/06/14 - Null Check
        {
            for(Account acc: ListofAccount )
            {   if(acc.AccountNumber!=null)//Kaushiki - 18/06/14 - Null Check
            
                AccountNumber.add(acc.AccountNumber);
            } 
        }
    }
    if(AccountNumber.size()>0)//Kaushiki - 18/06/14 - Null Check
    {
     ListofERPAccounts = [Select AccountNumber,Id,RecordType.DeveloperName,Name,Affiliation__c from Account where AccountNumber in:AccountNumber]; 
    
        if(ListofERPAccounts.size()>0)//Kaushiki - 18/06/14 - Null Check
        {
            for(Account accountalias :ListofERPAccounts)
            {
                if(accountalias.RecordType.DeveloperName=='Ship_to')
                {
                    system.debug('INSIDEEEEE');
                    ERPShipTo=accountalias.Name; 
                
                }
                if(accountalias.RecordType.DeveloperName=='Bill_To')
                {
                    system.debug('INSIDEEEEE');
                    ERPBilltoName=accountalias.Name; 
                }
              
                if(accountalias.RecordType.DeveloperName=='Site_Partner')
                {
                    ERPLocationName=accountalias.Name;
                }  
            
              
            } 
        }
    }
    for(SVMXC__Service_Contract_Products__c  conproduct: Trigger.New)
    {
        SVMXC__Service_Contract__c service= new SVMXC__Service_Contract__c();
        if(MapofAccount.get(conproduct.SVMXC__Service_Contract__c)!=null )//Kaushiki - 18/06/14 - Null Check
        service=MapofAccount.get(conproduct.SVMXC__Service_Contract__c);
        //if(service!=Null) US 4385 (Ranjan -31/3/2014 )
        //{
        //conproduct.Plant__c=service.SVMXC__Company__c;
        //}
        // conproduct.ERP_Bill_To_del__c=ERPBilltoName; commented by kaushiki US3728
        //if(ERPShipTo != NULL) US 4385 (Ranjan -31/3/2014)       //check added as per bug reported by Integration team (Nikhil - 27/3/14)
        //{
        // conproduct.ERP_Ship_To__c=ERPShipTo;
        //}
        //conproduct.ERP_Site_Partner__c=ERPLocationName; commented by kaushiki for US3608..
        SVMXC__Site__c sitealias= new SVMXC__Site__c();
        if(MapofSite.containsKey(conproduct.ERP_Functional_Location__c) && MapofSite.get(conproduct.ERP_Functional_Location__c)!=null)//Kaushiki - 18/06/14 - Null Check
        {
            sitealias = MapofSite.get(conproduct.ERP_Functional_Location__c);
            system.debug('###########' +MapofSite);
            if(sitealias!=Null)
            {
                conproduct.Location__c=sitealias.Id;
                system.debug('@@@@@@@@@@@@@@' +conproduct.Location__c);
            }
        }
        Product2 prodalias= new Product2();
        if(MapofProduct.containsKey(conproduct.Product_Code__c) && MapofProduct.get(conproduct.Product_Code__c)!=null)//Kaushiki - 18/06/14 - Null Check
        {
            prodalias = MapofProduct.get(conproduct.Product_Code__c);
            if(prodalias!=Null)
            {
                conproduct.SVMXC__Product__c=prodalias.Id;
            }
        }
        if(conproduct.Serial_Number_PCSN__c == null)
        {
            conproduct.SVMXC__Installed_Product__c = null;
        }
        else if(conproduct.Serial_Number_PCSN__c != null)
        {
            //if(InstalledProdMap.size()>0)
            if(InstalledProdMap.containsKey(conproduct.Serial_Number_PCSN__c) && InstalledProdMap.get(conproduct.Serial_Number_PCSN__c)!=null) //Kaushiki - 18/06/14 - Null Check            
            conproduct.SVMXC__Installed_Product__c = InstalledProdMap.get(conproduct.Serial_Number_PCSN__c);
        }
        
        //conproduct.Site_Partner__c= SitePartnerId; commented by kaushiki for US3608..
    }
    
        
    // Code By Ranjan starts 
         
    Set<String> SetBillTo = new Set<String>(); //Ranjan
    Map<String, Id> myMap = new Map<String, Id>(); //Ranjan
    Set<String> SetShipTo = new Set<String>();
    Map<String, Id> myMap1 = new Map<String, Id>();
    for(SVMXC__Service_Contract_Products__c  conprodt: Trigger.New)//Ranjan US4385(3/31/2014)
    { 
        if( conprodt.ERP_Bill_To__c!=NULL)
        { 
            SetBillTo.add(conprodt.ERP_Bill_To__c);
        }
        if( conprodt.ERP_Ship_To__c!=NULL)
        { 
            SetShipTo.add(conprodt.ERP_Ship_To__c);
        }
         
    }
            
    if(SetBillTo.size()>0)
    {
        for(ERP_Partner__c ep : [select id,Partner_Number__c from ERP_Partner__c where Partner_Number__c in:SetBillTo])
        {
            myMap.put(ep.Partner_Number__c ,ep.id);
        }
    }
    if (myMap.size()>0)
    {
        for(SVMXC__Service_Contract_Products__c cp : trigger.new )
        {
            if(myMap.containskey(cp.ERP_Bill_To__c) && myMap.get(cp.ERP_Bill_To__c)!=null)//Kaushiki - 18/06/14 - Null Check
            {
                cp.Bill_To__c = myMap.get(cp.ERP_Bill_To__c);
            }
        }
    }
         
            
    if(SetShipTo.size()>0)
    {
        for(ERP_Partner__c epshp : [select id,Partner_Number__c from ERP_Partner__c where Partner_Number__c in:SetShipTo])
        {
            myMap1.put(epshp.Partner_Number__c ,epshp.id);
        }
    }
    
    if (myMap1.size()>0)
    {
        for(SVMXC__Service_Contract_Products__c cps : trigger.new )
        {
            if(myMap.containskey(cps.ERP_Ship_To__c) && myMap1.get(cps.ERP_Ship_To__c)!=null)//Kaushiki - 18/06/14 - Null Check
            {
                cps.Ship_To__c = myMap1.get(cps.ERP_Ship_To__c);
            }
        }
    }*/
    
}
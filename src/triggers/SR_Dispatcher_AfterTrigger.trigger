trigger SR_Dispatcher_AfterTrigger on SVMXC__Dispatcher_Access__c (after insert,after update,after delete) 
{
    SR_Class_DispatchAccess cls = new SR_Class_DispatchAccess();
      
    if(Trigger.isInsert)
    {       
        cls.CreateDispatchAccessTerritory(Trigger.New);
    }
    if(Trigger.isDelete)
    { 
        cls.delDispatchAccess(trigger.old, null);
    }
  
    if(Trigger.isUpdate)
    {       
        cls.delDispatchAccess(trigger.old, trigger.newmap); 
        cls.CreateDispatchAccessTerritory(Trigger.New);
    }
}
trigger SR_Dispatcher_BeforeTrigger on SVMXC__Dispatcher_Access__c (before Insert, before update, before delete) 
{
    SR_Class_DispatchAccess clsDispatch = new  SR_Class_DispatchAccess();
    if( trigger.isinsert || trigger.isUpdate)
    {   
        clsDispatch.checkDuplicateRecord(trigger.new,Trigger.oldMap,Trigger.newMap);
    }   
}
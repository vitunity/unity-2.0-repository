/**************************************************
Created By  :   Nikita Gupta
Created On  :   8/11/2014
Created For :   US4583, to create WD if WRP WBS ends with '99'
Last Modified By  :   Nikita Gupta
Last Modified On  :   9/19/2014
Last Modified For :   US4945, to update Problem Description on Work Order of type PM
last modified by    :   Shradha Jain
Last Modified On    :   3/16/2015
Last Modified Reason:   code optimization
last modified by    :   Nina Gronowska
Last Modified On    :   8/12/2015
Last Modified Reason:   adding updateServiceOrder for US5171
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
25-Apr-2018 : Chandramouli : STSK0014566 : Added a condition to close PS line for INST NWA's only
05-Apr-2018 - CHandramouli - STSK0014365 : Added new method to fix the counter calculation of available NWA's.
18-JAN-2017 - Amitkumar - STSK0013566 Close PS line if budget=0 
**************************************************/

trigger SR_ERPNWA_AfterTrigger on ERP_NWA__c (after insert, after update) 
{    
     ////Audit Tracking Future Invoke
     if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter&&!Test.isRunningTest()) SObjectHistoryProcessor.trackHistory('ERP_NWA__c');

    if(!SR_Class_ERP_NWA.byPassFromWoUpdate){
        //To control soql added by amit
        //ERPNWAInterfaceStatusHandler.isTriggerExecuting = true;  //US5232
        SR_Class_ERP_NWA objcls = new SR_Class_ERP_NWA();
        objcls.createServiceOrder(trigger.new, trigger.oldMap);
        if(trigger.isUpdate){
            objcls.updateServiceOrder(trigger.new, trigger.oldMap);
            //added by Chandra for DE7479
            objcls.updateRelatedWorkDetail(trigger.new, trigger.oldMap);
        }
        
        Set<Id> erpCloseWD = new Set<Id>(); //savon/FF DE5903 9-22-15
        Set<Id> erpbudgethrsWD = new Set<Id>(); //CM: DFCT0012218 : added a new set to update PS line of Installation IWO's budgetted hrs.
        Map<id,decimal> nwa2budgethrs = new Map<id,decimal>();
        Map<Id, ERP_NWA__c> wdIdToNWAMap = new Map<Id, ERP_NWA__c>(); //JW@ff 10-27-2015 US5096 TA7719
        //STSK0012074 : CM : July 2017 
        Map<Id,integer> wbs2validnwacnt = new Map<Id,integer>();
        Map<Id,integer> wbs2invalidnwacnt = new Map<Id,integer>();
        List<ERP_WBS__c> wbs2update= new list<ERP_WBS__c>();
        Set<Id> wbsids = new Set<Id>();
    
        for (ERP_NWA__c erp : trigger.new)
        {   
            //CM : STSK0011424 : May 2017 : Modified the below logic.
            if(erp.work_detail__c != null && trigger.isInsert && erp.ERP_Status__c != null && (erp.ERP_Status__c.containsIgnoreCase('DLFL') || erp.ERP_Status__c.containsIgnoreCase('CLSD') || ( erp.ERP_Status__c.containsIgnoreCase('TECO')  && erp.ERP_Std_Text_Key__c != null && !erp.ERP_Std_Text_Key__c.containsIgnoreCase('inst') ) || ( erp.ERP_Status__c.containsIgnoreCase('CNF') && erp.ERP_Std_Text_Key__c != null && !erp.ERP_Std_Text_Key__c.containsIgnoreCase('inst') ) ) ){ //DE8020 DFCT0011585
                erpCloseWD.add(erp.Id);
            }
            //STSK0012074 : CM : July 2017 
            if (erp.ERP_Std_Text_Key__c != null && (erp.ERP_Std_Text_Key__c == 'INST001'  
                || erp.ERP_Std_Text_Key__c == 'COM0001'
                || erp.ERP_Std_Text_Key__c == 'COM0002'
                || erp.ERP_Std_Text_Key__c == 'COM0003'
                || erp.ERP_Std_Text_Key__c == 'COM0005'))
            {
                wbsids.add(erp.WBS_Element__c);
            }
            /*else if (trigger.isUpdate && erp.ERP_Status__c != null &&(
                    ((trigger.oldMap.get(erp.Id).ERP_Status__c == null || !trigger.oldMap.get(erp.Id).ERP_Status__c.containsIgnoreCase('DLFL')) && erp.ERP_Status__c.containsIgnoreCase('DLFL')) ||
                    ((trigger.oldMap.get(erp.Id).ERP_Status__c == null || !trigger.oldMap.get(erp.Id).ERP_Status__c.containsIgnoreCase('CLSD')) && erp.ERP_Status__c.containsIgnoreCase('CLSD')) ||
                    ((trigger.oldMap.get(erp.Id).ERP_Status__c == null || !trigger.oldMap.get(erp.Id).ERP_Status__c.containsIgnoreCase('TECO')) && erp.ERP_Status__c.containsIgnoreCase('TECO')) ||
                    ((trigger.oldMap.get(erp.Id).ERP_Status__c == null || !trigger.oldMap.get(erp.Id).ERP_Status__c.containsIgnoreCase('CNF')) && erp.ERP_Status__c.containsIgnoreCase('CNF'))
                ))
            {
                erpCloseWD.add(erp.Id);
            }*/
            
            else if (trigger.isUpdate //DE8020 DFCT0011585
                      && erp.ERP_Status__c != null 
                      && trigger.oldMap.get(erp.Id).ERP_Status__c != erp.ERP_Status__c
                      && (erp.ERP_Status__c.containsIgnoreCase('DLFL')
                          || erp.ERP_Status__c.containsIgnoreCase('CLSD')
                          || ( erp.ERP_Status__c.containsIgnoreCase('TECO')  && erp.ERP_Std_Text_Key__c != null && !erp.ERP_Std_Text_Key__c.containsIgnoreCase('inst') ) //CM : STSK0011424 : May 2017
                          || ( erp.Is_CNF__c && erp.ERP_Std_Text_Key__c != null && !erp.ERP_Std_Text_Key__c.containsIgnoreCase('inst'))
                          || ( erp.ERP_Estimated_Work__c == 0 && erp.ERP_Std_Text_Key__c != null && erp.ERP_Std_Text_Key__c.containsIgnoreCase('inst')))) //CM : DE7604 changed the logic to check if its confirmed then close the WD. //CM : STSK0011424 : May 2017 
                          //|| erp.ERP_Status__c.containsIgnoreCase('CNF')))
            {
                erpCloseWD.add(erp.Id);
            
            
            }
            
            //Added for STSK0013566 : Close PS line if budget=0
            //25-Apr-2018 : Chandramouli : STSK0014566 : Added a condition to close PS line for INST NWA's only
            if (trigger.isUpdate && erp.ERP_Std_Text_Key__c != null && erp.ERP_Std_Text_Key__c.containsIgnoreCase('inst') 
                      && ( erp.ERP_Estimated_Work__c == 0 ||  erp.ERP_Estimated_Work__c == null )
                      && trigger.oldMap.get(erp.Id).ERP_Estimated_Work__c != erp.ERP_Estimated_Work__c
                      ) //CM : DE7604 changed the logic to check if its confirmed then close the WD. //CM : STSK0011424 : May 2017 
            {
                erpCloseWD.add(erp.Id);
            }
    
            //JW@ff 10-27-2015 US5096 generate list of inserted NWAs or updated NWAs whose status has changed to include REL, CNF, or MCNF
            //only retrieves NWA's that has a related Work Detail with the Multiples checkbox checked.
            //if(erp.Work_Detail__c != null && erp.Work_Detail_Multiples__c == true && (Trigger.isInsert || (Trigger.isUpdate && (erp.Is_REL__c && !trigger.oldMap.get(erp.Id).Is_REL__c) || (erp.Is_CNF__c && !trigger.oldMap.get(erp.Id).Is_CNF__c) || (erp.IS_MCNF__c && !trigger.oldMap.get(erp.Id).IS_MCNF__c)))) //DE7604
            // {
            //     wdIdToNWAMap.put(erp.Work_Detail__c, erp);
            // }
            
        }
    
        //CM: DFCT0012218 : added a new set to update PS line of Installation IWO's budgetted hrs.
        for (ERP_NWA__c erp : trigger.new)
        {
            if (erp.ERP_Estimated_Work__c > 0 && trigger.isUpdate 
                && trigger.oldMap.get(erp.Id).ERP_Estimated_Work__c != erp.ERP_Estimated_Work__c 
                && !erpCloseWD.contains(erp.id))
            {
                erpbudgethrsWD.add(erp.id);
                nwa2budgethrs.put(erp.id,erp.ERP_Estimated_Work__c);
            }
        }
    
        system.debug(' ---wd to erpCloseWD-----'+erpCloseWD);
        
        //REFACTOR:  Move to class file; doesn't belong in trigger
        
        List<SVMXC__Service_Order_Line__c> workDetailListToUpdate = new List<SVMXC__Service_Order_Line__c> (); //DE8020
        // DFCT0011577 : PM Record Type Id of Work Order, as NWA CNF value closing the WD, which should not be happening.
        Id projMngmntRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get(label.sr_ProjectManagement).getRecordTypeId();
        Id instlRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        //Modified the code for optimization. CM : Nov 8 2016, commenting the below code and to include in for loop.
        /* 
        if (erpCloseWD.size()>0) //DE8020 //DFCT0011585
            workDetailListToUpdate = [SELECT Id, SVMXC__Service_Order__c, SVMXC__Service_Order__r.SVMX_Usage_Consumption_Work_Detail__c, SVMXC__Service_Order__r.RecordTypeID,
                                                                    SVMXC__Service_Order__r.No_of_Closed_Work_Details__c,SVMXC__Service_Order__r.recordtype.name
                                                                    FROM SVMXC__Service_Order_Line__c 
                                                                    WHERE ERP_NWA__c IN :erpCloseWD 
                                                                    AND RecordType.Name = 'Products Serviced' AND SVMXC__Line_Status__c != 'Closed'];//CM : Commented : DFCT0011768:  AND
                                                                    //SVMXC__Service_Order__r.RecordTypeID !=: projMngmntRecTypeID]; //DE8020: If it already closed then there is nothing to do to avoid cascading DML's
        */                                                            
        Set<Id> woIds = new Set<Id>();
        Set<Id> ClosedWoIDs = new Set<Id> ();    
        if(erpCloseWD.size()>0){
            for(SVMXC__Service_Order_Line__c wd : [SELECT Id, SVMXC__Service_Order__c, SVMXC__Service_Order__r.SVMX_Usage_Consumption_Work_Detail__c, 
                                                   SVMXC__Service_Order__r.RecordTypeID,Work_Order_RecordType__c,
                                                   SVMXC__Service_Order__r.No_of_Closed_Work_Details__c,SVMXC__Service_Order__r.recordtype.name,
                                                   SVMXC__Service_Order__r.WD_Count__c,
                                                   ERP_NWA__r.ERP_Estimated_Work__c
                                                   FROM SVMXC__Service_Order_Line__c 
                                                   WHERE ERP_NWA__c IN :erpCloseWD 
                                                   AND RecordType.Name = 'Products Serviced' AND SVMXC__Line_Status__c != 'Closed'] ){
    
                // CM : DFCT0011768 
                //if(wd.SVMXC__Service_Order__r.SVMX_Usage_Consumption_Work_Detail__c == wd.SVMXC__Service_Order__r.No_of_Closed_Work_Details__c){
                    //STSK0013812
                    if(wd.Work_Order_RecordType__c != null && wd.Work_Order_RecordType__c == 'Installation')
                    wd.Budgeted_Hours__c = wd.ERP_NWA__r.ERP_Estimated_Work__c;
                                                       
                    wd.SVMXC__Line_Status__c = 'Closed';
                    /*//CM : DFCT0011768
                    if (wd.SVMXC__Service_Order__r.recordtypeId == projMngmntRecTypeID && wd.SVMXC__Service_Order__r.WD_Count__c == 0) 
                    {
                        ClosedWoIDs.add(wd.SVMXC__Service_Order__c);
                    }*/
                    woIds.add(wd.SVMXC__Service_Order__c);
                //}
                workDetailListToUpdate.add(wd);
               // woIds.add(wd.SVMXC__Service_Order__c);
            }
        }
    
        //CM: DFCT0012218 : added a new set to update PS line of Installation IWO's budgetted hrs.
        if (erpbudgethrsWD.size() > 0)
        {
            for (SVMXC__Service_Order_Line__c wd : [SELECT Id, SVMXC__Service_Order__c, Work_Order_RecordType__c,
                                                    Manual_Override_for_PS_WDL_Line_Status__c,  
                                                    SVMXC__Service_Order__r.SVMX_Usage_Consumption_Work_Detail__c, 
                                                    SVMXC__Service_Order__r.RecordTypeID,ERP_NWA__c,
                                                    SVMXC__Service_Order__r.No_of_Closed_Work_Details__c,SVMXC__Service_Order__r.recordtype.name
                                                    FROM SVMXC__Service_Order_Line__c 
                                                    WHERE ERP_NWA__c IN :erpbudgethrsWD AND RecordType.Name = 'Products Serviced' AND 
                                                    SVMXC__Service_Order__r.SVMXC__Order_Status__c != 'Closed'
                                                    and SVMXC__Service_Order__r.RecordTypeID = :instlRecTypeID and SVMXC__Service_Order__r.Event__c =false] ){
    
                if (nwa2budgethrs.size() > 0 && nwa2budgethrs.containsKey(wd.ERP_NWA__c))
                {
                    wd.Budgeted_Hours__c = nwa2budgethrs.get(wd.ERP_NWA__c);
                    if(!wd.Manual_Override_for_PS_WDL_Line_Status__c)
                    wd.SVMXC__Line_Status__c = 'Open';
                    workDetailListToUpdate.add(wd);
                }
    
            }    
        }
    
    
        System.debug('workDetailListToUpdate================'+workDetailListToUpdate);
        //CM : STSK0011424 : May 2017 : 
        if(!workDetailListToUpdate.isEmpty()){
            update workDetailListToUpdate;
        }
        //cdelfattore@forefront 11/30/2015 - DE6782
        //need to query for the Usage/Consumption line and set the line status to close
        
        // CM : DFCT0011768 
        if (woIds.size()>0) 
        {
            //CM : STSK0011424 : May 2017 : Modified the below logic 
            for(SVMXC__Service_Order__c wo : [SELECT Id, Name, RecordType.Name, Closed_PS_Line_Count__c, Closed_U_C_Lines__c, SVMX_Usage_Consumption_Work_Detail__c,
                                                No_of_Work_Details__c, (select id, name, Remaining_Qty__c from SVMXC__RMA_Shipment_Line__r where Remaining_Qty__c > 0)
                                                From SVMXC__Service_Order__c where id IN :woIds and RecordType.Name = 'Installation'])
            {
                if( (wo.No_of_Work_Details__c == wo.Closed_PS_Line_Count__c) && (wo.SVMX_Usage_Consumption_Work_Detail__c == wo.Closed_U_C_Lines__c) && wo.SVMXC__RMA_Shipment_Line__r.size() == 0)
                {
                    ClosedWoIDs.add(wo.id);
                }
            }
        }
    
        system.debug('--woids-----'+woIds);
        List<SVMXC__Service_Order__c> woCheck = new List<SVMXC__Service_Order__c> (); //DFCT0011585
       // if (woIds.size()>0)
        //  woCheck = [SELECT Id, (SELECT Id FROM SVMXC__Service_Order_Line__r WHERE RecordType.Name != 'Products Serviced') //, (SELECT Id FROM Time_Entry_Matrices__r) 
        //                                                FROM SVMXC__Service_Order__c WHERE Id IN :woIds AND RecordType.Name = 'Project Management'];
        //system.debug('woCheck to close --------'+woCheck);
        /*if(!woCheck.isEmpty()){
        for (SVMXC__Service_Order__c wo : woCheck) {
            //if (wo.SVMXC__Service_Order_Line__r.isEmpty() && wo.Time_Entry_Matrices__r.isEmpty()) {
                wo.SVMXC__Order_Status__c = 'Closed';
            //  }
        }
        }*/
        
        // CM : DFCT0011768 
        if(!ClosedWoIDs.isEmpty()){ //DE8020  //DFCT0011585
            for (Id woid : ClosedWoIDs) {
                
                    SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(id=woid);
                    wo.SVMXC__Order_Status__c = 'Closed';
                    woCheck.add(wo);
               
            }
        }
        if (!woCheck.isEmpty()) {
            update woCheck;
        }
    
    
        /*pszagdaj/FF, US5099/TA7691, 10/20/2015, made obsolete by TA7692 */
        objcls.createCounterDetailsOnAfter(trigger.oldMap,trigger.newMap); //pszagdaj/FF, US5099/TA7692, 10/23/2015,
    
        //JW@ff 10-27-2015 US5096 TA7719 
        //if(!wdIdToNWAMap.isEmpty())
        //{
            //objcls.setRelatedNWAsToProcess(wdIdToNWAMap);
        //}
        
        //pszagdaj/FF, 12/22/2015, US5133 DE6911
        objcls.updateRelatedSalesOrderItems(trigger.new, trigger.oldMap);
        //STSK0012074 : CM : July 2017 
        if (wbsids.size() > 0)
        {
            for (AggregateResult  ar : [select count(Id) cnt, WBS_Element__c from ERP_NWA__c where WBS_Element__c in : wbsids and Is_CLSD__c = false          and Is_TECO__c = false 
                      and Is_DLFL__c = false 
                      and (ERP_Std_Text_Key__c = 'INST001' 
                          OR ERP_Std_Text_Key__c = 'COM0001'
                          OR ERP_Std_Text_Key__c = 'COM0002' 
                          OR ERP_Std_Text_Key__c = 'COM0003'
                          OR ERP_Std_Text_Key__c = 'COM0005') group by WBS_Element__c])
            {
                wbs2validnwacnt.put((Id)ar.get('WBS_Element__c'), (Integer)ar.get('cnt'));
            }
            for (AggregateResult  ar : [select count(Id) cnt, WBS_Element__c from ERP_NWA__c where WBS_Element__c in : wbsids and (Is_CLSD__c = true          or Is_TECO__c = true 
                      or Is_DLFL__c = true) 
                      and (ERP_Std_Text_Key__c = 'INST001' 
                          OR ERP_Std_Text_Key__c = 'COM0001'
                          OR ERP_Std_Text_Key__c = 'COM0002' 
                          OR ERP_Std_Text_Key__c = 'COM0003'
                          OR ERP_Std_Text_Key__c = 'COM0005') 
                      group by WBS_Element__c])
            {
                if (wbs2validnwacnt.size() > 0 && !wbs2validnwacnt.containskey((Id)ar.get('WBS_Element__c')))
                {
                    wbs2invalidnwacnt.put((Id)ar.get('WBS_Element__c'), (Integer)ar.get('cnt'));
                }
                else if (wbs2validnwacnt.size() == 0)
                {
                    wbs2invalidnwacnt.put((Id)ar.get('WBS_Element__c'), (Integer)ar.get('cnt'));
                }
            }
        }
    
        if (wbs2validnwacnt.size() > 0)
        {
            system.debug('***CM wbs2validnwacnt : '+wbs2validnwacnt);
            for (id wbsid : wbs2validnwacnt.keyset())
            {
                ERP_WBS__c wbs = new ERP_WBS__c();
                wbs.id = wbsid;
                wbs.PS_Count__c = wbs2validnwacnt.get(wbsid);
                wbs2update.add(wbs);
            }
        }
        if (wbs2invalidnwacnt.size() > 0)
        {
            system.debug('***CM wbs2invalidnwacnt : '+wbs2invalidnwacnt);
            for (id wbsid : wbs2invalidnwacnt.keyset())
            {
                ERP_WBS__c wbs = new ERP_WBS__c();
                wbs.id = wbsid;
                wbs.PS_Count__c = 0;
                wbs2update.add(wbs);
            }
        }
        if (wbs2update.size() > 0)
        {
            update wbs2update;
        }
        //05-Apr-2018 - CHandramouli - STSK0014365 : Added new method to fix the counter calculation of available NWA's.
        set<id> SOIids = new set<id>();
        For (ERP_NWA__c nwa : trigger.new)
        {
            system.debug('NWA: '+nwa);
            if (nwa.erpnwa_SalesOrderItem__c != null && nwa.ERP_Std_Text_Key__c != null && nwa.ERP_Std_Text_Key__c.containsIgnoreCase('trn'))
            {
                if (trigger.isInsert)
                {
                    SOIIds.add(nwa.erpnwa_SalesOrderItem__c);
                }
                else
                {
                    if (nwa.ERP_Status__c != trigger.oldMap.get(nwa.Id).ERP_Status__c || nwa.Work_Detail__c != trigger.oldMap.get(nwa.Id).Work_Detail__c || System.Label.counter_calculation_flag == 'True')
                    {
                        SOIIds.add(nwa.erpnwa_SalesOrderItem__c);
                    }
                }
            } 
        }
        if (SOIids.size() > 0 )
        {
            objcls.updatecounterswithcounts(SOIids);
        }
    //STSK0012074 : CM : July 2017 
    }
}
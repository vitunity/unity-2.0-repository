/*************************************
last modified by    :   Nikita Gupta
Last Modified On    :   8/14/2014
Last Modified Reason:   code optimization
last modified by    :   Shradha Jain
Last Modified On    :   3/16/2015
Last Modified Reason:   code optimization

Change Log:
2-Nov-2017 - Divya Hargunani - STSK0013220 : SFDC: Populate SO and SOI on ERP NWA (All Training NWAs)
    
*************************************/

//cdelfattore@forefront 11/18/2015
//this trigger should be firing on after insert and after update
//there is a query that depends on a set of Ids of the ERP_NWA__c object
//becuase this use to fire before insert there would be no Ids and all
//of the SVMXC__Service_Order_Line__c objects would be returned from salesforce database
//changed the below trigger to fire on after insert and after update instead of before insert and before update

//cdelfattore@forefront 11/19/2015
//changed it back to before insert and before update, was causing a read only record trigger problem
trigger SR_ERPNWA_BeforeTrigger on ERP_NWA__c (before insert,before update) 
{   
    if(!SR_Class_ERP_NWA.byPassFromWoUpdate){
        //DFCT0011575.08/30/2016.Start
            if(trigger.isUpdate && trigger.isBefore){
                ERPNWALog.CreateERPNWSTrace(trigger.newmap,trigger.oldmap);
            }
        //DFCT0011575.08/30/2016.End
    
    
        SR_Class_ERP_NWA objcls = new SR_Class_ERP_NWA();
        objcls.populateFields(trigger.new, trigger.oldMap); 
        // objcls.updateStatusFields(trigger.new, trigger.oldMap);
        
        // STSK0013220: SFDC: Populate SO and SOI on ERP NWA (All Training NWAs) - For populating SO and SOI lookup
        objcls.setSalesOrderNumber(trigger.new, trigger.oldMap); 
        objcls.setSalesOrderItemNumber(trigger.new, trigger.oldMap); 
    
    
        //cdelfattore@forefront 11/19/2015
        //comment out the below code since it was moved to the after trigger. we no longer need the below code.
        //below code also throws a null pointer because trigger.oldMap doesn't exist in an on insert trigger.
        
        //Set<Id> eRPID = new Set<Id>();
        //String cnflowercase = 'cnf';
        //String CNFuppercase = 'CNF';
        //for(ERP_NWA__c erpNWA: Trigger.new){
        //    if(erpNWA.ERP_Status__c != null && erpNWA.ERP_Status__c != trigger.oldMap.get(erpNWA.id).ERP_Status__c && String.isNotBlank(trigger.oldMap.get(erpNWA.id).ERP_Status__c) && !trigger.oldMap.get(erpNWA.id).ERP_Status__c.contains(CNFuppercase) && !trigger.oldMap.get(erpNWA.id).ERP_Status__c.contains(cnflowercase) && (erpNWA.ERP_Status__c.contains(CNFuppercase) || erpNWA.ERP_Status__c.contains(cnflowercase))){
        //        eRPID.add(erpNWA.id);
        //    }
        //}
    
        
        //if(eRPID.size() > 0){
        //    List<RecordType> prodServiceRecID = [SELECT Id,Name FROM RecordType WHERE Name = 'Products Serviced' Limit 1];
            
    
        //    Id prodServRecID;
        //    if(prodServiceRecID.size() > 0)
        //    {            
        //    prodServRecID = prodServiceRecID[0].id;
        //    }
    
        //    //cdelfattore@forefront 11/18/2015
        //    //when this fires on before insert or before update there will be no value for the id field. thus the eRPID set will be empty
        //    //the query will ignore the filter ERP_NWA__c IN: eRPID
        //    //the query below will return all of the sales order lines in the database. that's bad ummmkay
        //    //changed this trigger to fire after insert and after update
        //    List<SVMXC__Service_Order_Line__c> relWD = [SELECT id, ERP_NWA__c,  SVMXC__Line_Status__c, RecordTypeid FROM SVMXC__Service_Order_Line__c WHERE ERP_NWA__c IN: eRPID AND RecordTypeid =: prodServRecID AND SVMXC__Line_Status__c != 'Closed'];
        //    if(relWD.size() > 0){
        //        for(SVMXC__Service_Order_Line__c WD : relWD){
        //            WD.SVMXC__Line_Status__c = 'Closed';
        //        }
        //    //cdelfattore@forefront 11/19/2015
        //    //comment out the below update because the code was moved to the after trigger
        //    //update relWD;
        //    }
        //}
    }
}
trigger SR_ERPPcode_AfterTrigger on ERP_Pcode__c (after update)
{
    //Set<Id> setERPpcodeId = new Set<Id>();
    Map<Id, string> mapERPpcodeId_OldSerProdGroup = new Map<Id, string>();
    Map<Id, string> mapERPpcodeId_NewSerProdGroup = new Map<Id, string>();
    List<Product2> listUpdateProduct = new List<Product2>();

    for(ERP_Pcode__c varPcode : trigger.new)
    {
        if(varPcode.Service_Product_Group__c != null && trigger.oldMap.get(varPcode.Id).Service_Product_Group__c != null && trigger.oldMap.get(varPcode.Id).Service_Product_Group__c != varPcode.Service_Product_Group__c)
        {
            mapERPpcodeId_OldSerProdGroup.put(varPcode.Id, trigger.oldMap.get(varPcode.Id).Service_Product_Group__c);   //setERPpcodeId.add(varPcode.Id);
            mapERPpcodeId_NewSerProdGroup.put(varPcode.Id, varPcode.Service_Product_Group__c);
            System.debug('old value of Service_Product_Group__c ==== ' + mapERPpcodeId_OldSerProdGroup.get(varPcode.Id));
            System.debug('new value of Service_Product_Group__c ==== ' + mapERPpcodeId_NewSerProdGroup.get(varPcode.Id));
        }
    }

    if(mapERPpcodeId_OldSerProdGroup.size() > 0 && mapERPpcodeId_NewSerProdGroup.size() > 0)
    {
        System.debug('inside if condition');
        for(Product2 varProd : [select Id, SVMXC__Product_Line__c, Service_Product_Group__c, ERP_Pcode__c from Product2 where
                                ERP_Pcode__c in :mapERPpcodeId_OldSerProdGroup.keySet() AND SVMXC__Product_Line__c in :mapERPpcodeId_OldSerProdGroup.values()])
        {
            if(mapERPpcodeId_NewSerProdGroup.containsKey(varProd.ERP_Pcode__c) && mapERPpcodeId_NewSerProdGroup.get(varProd.ERP_Pcode__c) != null)
            {
                System.debug('SVMXC__Product_Line__c before change');
                varProd.SVMXC__Product_Line__c = mapERPpcodeId_NewSerProdGroup.get(varProd.ERP_Pcode__c);
                System.debug('SVMXC__Product_Line__c after change');
                listUpdateProduct.add(varProd);
            }
        }
        if(listUpdateProduct.size() > 0)
        {
            System.debug('listUpdateProduct size //// ' + listUpdateProduct.size());
            update listUpdateProduct;
        }
    }
}
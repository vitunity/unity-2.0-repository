trigger SR_ERPProject_AfterTrigger on ERP_Project__c (after insert,after update) 
{
  /*Set<Id> SetofERPWBSId = new Set<Id>();
  Map<Id,ERP_WBS__c> MapofERPWBS = new Map<Id,ERP_WBS__c>();
  List<SVMXC__Service_Order__c> ListofWO = new List<SVMXC__Service_Order__c>();
  for(ERP_Project__c ERPProject : Trigger.New)
  {
    SetofERPWBSId.add(ERPProject.Id);
  }
 List<ERP_WBS__c> ListofERPWBS =[Select Id,ERP_Project__c,WBS_Element__c from ERP_WBS__c where ERP_Project__c in:SetofERPWBSId ]; 
  for(ERP_WBS__c ERPWBS : ListofERPWBS )
  {
    MapofERPWBS.put(ERPWBS.ERP_Project__c,ERPWBS);
  
  }
  
  for(ERP_Project__c ERPProject : Trigger.New)
  {
   ERP_WBS__c ERPWBS = new ERP_WBS__c();
   ERPWBS=MapofERPWBS.get(ERPProject.Id);
   
   if(ERPWBS!=Null)
   {
   String WBSElementlength= ERPWBS.WBS_Element__c;
   if(ERPProject.Status__c=='REL' && ERPWBS.WBS_Element__c.EndsWith('99') && WBSElementlength.length()==17)
   {
     SVMXC__Service_Order__c WO = new SVMXC__Service_Order__c();
     ListofWO.add(WO);
   }
   }
  
  }
  if(ListofWO.size()>0)
  {
    Insert ListofWO;
  }*/
  Schema.DescribeSObjectResult d = Schema.SObjectType.Case; 
  Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
  Schema.RecordTypeInfo rtByName =  rtMapByName.get('PM'); //per Rich //INC42047
  Id RecordTypeId=rtByName.getRecordTypeId();
  system.debug('*****'+rtByName);
  system.debug('*****'+RecordTypeId);
  Set<String> SetofERPSalesOrderNumber = new Set<String>();
  Set<Id> SalesOrderId = new Set<Id>();
  Map<String,Sales_Order__c> MapofSalesOrder = new Map<String,Sales_Order__c>();
  List<Case> ListofCasestoInsert = new List<Case>();    
  for(ERP_Project__c ERPProject : Trigger.New)
  { 
   if (ERPProject.ERP_Sales_Order_Number__c != null) SetofERPSalesOrderNumber.add(ERPProject.ERP_Sales_Order_Number__c);  //DFCT0011585
  }
  //List<Sales_Order__c> ListofSalesOrder =[Select Id,Name from Sales_Order__c where name in:SetofERPSalesOrderNumber];// code commented acc to US4170 & replaced with
 // List<Sales_Order__c> ListofSalesOrder =;
  if (SetofERPSalesOrderNumber.size()>0) //DE8283  //DFCT0011585
  for(Sales_Order__c sales : [Select Id,ERP_Reference__c from Sales_Order__c where  ERP_Reference__c in:SetofERPSalesOrderNumber] ) //DE8283
  {
    //MapofSalesOrder.put(sales.Name,sales);// code commented acc to US4170 & replaced with
    MapofSalesOrder.put(sales.ERP_Reference__c,sales);
    
    SalesOrderId.add(sales.Id);
  }
  List<Sales_Order_Partner__c> salesorderpartner= new List<Sales_Order_Partner__c> ();
  if(SalesOrderId.size()>0) //DE8283  //DFCT0011585
    salesorderpartner=[Select Id,Contact__r.User__r.Id from Sales_Order_Partner__c where Sales_Order__c in:SalesOrderId and Role__c=:'PM' Limit 1];
 
  for(ERP_Project__c ERPProject : Trigger.New)
  { 
    if(trigger.IsInsert)
    {
        Case casealias = new Case();
        casealias.RecordTypeId=RecordTypeId;
        system.debug('casealias.AccountId before is  =' +ERPProject.Site_Partner__c);
        casealias.AccountId=ERPProject.Site_Partner__c;
        system.debug('casealias.AccountId afetr is =' +casealias.AccountId);
        casealias.Type='Request';
        casealias.Service_Type__c='Installation';
        casealias.ERP_Project_Number__c=ERPProject.Name;
        casealias.ERP_Sales_Order_Number__c=ERPProject.ERP_Sales_Order_Number__c;
        Sales_Order__c sales = new Sales_Order__c();
        sales=MapofSalesOrder.get(ERPProject.ERP_Sales_Order_Number__c);
        if(sales!=Null)
        {
          casealias.Sales_Order__c=sales.Id;
        }
        if(salesorderpartner.size()>0)
        {
          if (salesorderpartner[0].Contact__r.User__r.Id != null)
            casealias.OwnerId=salesorderpartner[0].Contact__r.User__r.Id;
        }
        ListofCasestoInsert.add(casealias);
    }
  }
  if(ListofCasestoInsert.size()>0)
  {
   Insert ListofCasestoInsert;
  }
}
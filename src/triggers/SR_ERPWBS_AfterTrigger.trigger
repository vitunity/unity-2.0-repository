/********************************************************************
Last Modified By:       Nikita Gupta
Last Modified On:       10th Feb, 2014
Last Modified Reason:   1. US4054, to update WO's Acceptance date with ERP WBS's Acceptance Date if their Sales Order Item value match    
                        2. renamed class from SR_ErpWbsUtility to SR_ClassERP_WBS and changed indenting of the code
Last Modified By:       Nikhil Verma
Last Modified On:       10th Mar, 2014
Last Modified Reason:   Removed Insert part of trigger as per new rquirement from my code. Added a check to call method only if any Acceptance Date of a record is updated

Last Modified By:       Shubham Jaiswal
Last Modified On:       30th April, 2014
Last Modified Reason:   US4487, to update PMO WO creation condition & assigning ERPWBS projet manager as owner of WO created

Last Modified By:       Shubham Jaiswal
Last Modified On:       5th May, 2014
Last Modified Reason:   Added Null Checks

 
Last Modified By:       Shubham Jaiswal
Last Modified On:       29th May, 2014
Last Modified Reason:   US4557

Last Modified By:       Nikita Gupta
Last Modified On:       8/6/2014
Last Modified Reason:   DE973

Last Modified By:       Nikita Gupta
Last Modified On:       8/8/2014
Last Modified Reason:   US4583- to extend code written for US3790 and create a new Work Detail of record type Products Serviced as well.

Last Modified By:       Nikita Gupta
Last Modified On:       8/8/2014
Last Modified Reason:   Removed Code for US4583 and moved to ERP NWA after trigger

Last Modified By:       Nikita Gupta
Last Modified On:       9/18/2014
Last Modified Reason:   updated for US4945
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
06-Oct-2017 - Chandramouli - STSK0013074 - Optmized code to run only when there is change in ERP PM Work Center.
********************************************************************/
trigger SR_ERPWBS_AfterTrigger on ERP_WBS__c (after insert,after update) 
{
    //Audit Tracking Future Invoke
    if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter&&!Test.isRunningTest()) SObjectHistoryProcessor.trackHistory('ERP_WBS__c');
    
    If(!SR_ClassERP_WBS.byPassFromWoUpdate){ 
        SR_ClassERP_WBS objWBS = new SR_ClassERP_WBS(); //initiate class
        List<ERP_WBS__c> ListERPWBS = new List<ERP_WBS__c>();
        List<String> ListERPWBSErpRef = new  List<String>();
        List<ERP_WBS__c> ListERPWBS2updateWD = new List<ERP_WBS__c>(); // US4616
        List<ERP_WBS__c> erpWbsList = new List<ERP_WBS__c>();
        map<id,ERP_WBS__c> ERPWBSIds = new map<id,ERP_WBS__c>(); // US5106   1C
        //Oct 6 2017 - Chandramouli - STSK0013074 _ Added the below logic to run the createWO method only for PM workorders and when there is a change in the PM WorkCenter.
        List<ERP_WBS__c> wbs2runworkorder = new list<ERP_WBS__c>();

        
        for (ERP_WBS__c ERPWBS : trigger.new)
        {
            if(ERPWBS.ERP_Reference__c != null && ERPWBS.ERP_Reference__c.EndsWith('99') && !ERPWBS.Is_DLFL__c  && !ERPWBS.Is_CLSD__c && !ERPWBS.Is_TECO__c && ERPWBS.ERP_PM_Work_Center__c != null && ( (trigger.oldmap != null && erpwbs.ERP_PM_Work_Center__c != trigger.oldmap.get(erpWBS.id).ERP_PM_Work_Center__c) || trigger.oldmap == null))
            {
                wbs2runworkorder.add(ERPWBS);
            }
        }
        if (wbs2runworkorder.size() > 0 )
        { 
            objWBS.CreateWO(wbs2runworkorder,trigger.oldmap);
        }
        objWBS.updateERPWBSAcceptanceDateOnWO(Trigger.new); //Method call by Nikita for US4054 to update ERP WBS Acceptance Date on WO
        objWBS.updateChildWBS(trigger.new, trigger.oldmap); //method call by nikita for US4945, 9/18/2014
         
        for(ERP_WBS__c erpwbs : trigger.New)
        {
            //only those records will be updated whose Acceptance Date is updated (checked this below)
            if(Trigger.IsUpdate && (Trigger.oldMap.get(erpwbs.Id).Acceptance_Date__c != Trigger.newMap.get(erpwbs.Id).Acceptance_Date__c 
                ||Trigger.oldMap.get(erpwbs.Id).ErpWbs_Installation_Date__c != Trigger.newMap.get(erpwbs.Id).ErpWbs_Installation_Date__c))
            {
                erpWbsList.add(erpwbs);    //Id of all records whose Accep. Date is updated is added to Set.
            }
            if(Trigger.IsUpdate && (Trigger.oldMap.get(erpwbs.Id).Forecast_Start_Date__c != erpwbs.Forecast_Start_Date__c || Trigger.oldMap.get(erpwbs.Id).Forecast_End_Date__c !=  erpwbs.Forecast_End_Date__c) && ERPWBS.ERP_Reference__c != Null) // Shubham
            {
                ListERPWBS.add(ERPWBS);
                ListERPWBSErpRef.add(ERPWBS.ERP_Reference__c);
            }
            if(Trigger.IsUpdate && (Trigger.oldMap.get(erpwbs.Id).Forecast_Start_Date__c!= erpwbs.Forecast_Start_Date__c || Trigger.oldMap.get(erpwbs.Id).Forecast_End_Date__c !=  erpwbs.Forecast_End_Date__c)) // US4616 Shubham
            {
                ListERPWBS2updateWD.add(ERPWBS);
            }
            // US5106 for update of Acceptance date on all the product serviced WD related to the ERP WBS
            if(erpwbs.Acceptance_Date__c != null && (Trigger.IsInsert || (Trigger.IsUpdate && Trigger.oldMap.get(erpwbs.Id).Acceptance_Date__c != erpwbs.Acceptance_Date__c)))//Acceptance_Date__c
            {
                ERPWBSIds.put(erpwbs.id,erpwbs);
            }
        }
        
        if(erpWbsList.size() > 0) //check if any records are to be worked on
        {
            objWBS.updateAccepDateOnErpWbs(erpWbsList );
        }
        
        if(trigger.ISUpdate)
        {
            objWBS.populateDateFieldsOnCounterDetails(trigger.oldMap,trigger.newMap);//kaushiki 13/3/14 for US4057
        }
        system.debug('>>>>>>trigger' + ListERPWBS);
        
        if((ListERPWBS.size() > 0 && ListERPWBSErpRef.size() > 0) || ListERPWBS2updateWD.size()>0 || ERPWBSIds.size() > 0) //  US4557 29/5/2014 Shubham Jaiswal
        {   
            objWBS.PopulateCaseLineStartAndEndDateFromERPWBS(ListERPWBS,ListERPWBSErpRef,ListERPWBS2updateWD,ERPWBSIds);
        }
    }       
}
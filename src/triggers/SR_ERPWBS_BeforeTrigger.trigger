/*************************************************************************\
    @ Author        : Chiranjeeb Dhar
    @ Date      :     08-Jan-2014
    @ Description   : US3809
    @ Last Modified By  :  Shubham Jaiswal    
    @ Last Modified On  :  09-04-2014    
    @ Last Modified Reason  :  Replaced Name,Sales_Order_Item__c with ERP_Reference for US4170
    @ Last Modified By  :  Shubham Jaiswal    
    @ Last Modified On  :  26-06-2014    
    @ Last Modified Reason  :  Added Null Checks
    @ Last Modified By  :  Nikita Gupta   
    @ Last Modified On  :  9/18/2014
    @ Last Modified Reason  :  US4945, Commented code to poopulate Project Manager field.
   ****************************************************************************/

trigger SR_ERPWBS_BeforeTrigger on ERP_WBS__c (before Insert,before Update) 
{
    If(!SR_ClassERP_WBS.byPassFromWoUpdate){
        SR_ClassERP_WBS objERPWBS = new SR_ClassERP_WBS();
        objERPWBS.updateERPWBSFields(trigger.new,trigger.oldmap);
        objERPWBS.populateSiteAndLocationOnWBS(trigger.new);
    }
    //STRY0064906,Dipali Chede,Reason:  set Actual_Start_Date__c to null if ActStart in PS has nulll value
 

     for(ERP_WBS__c record:trigger.new)
    {
     Date myDate = date.newInstance(4000,12,31);
     if(record.Actual_Start_Date__c == myDate)
     {
       record.Actual_Start_Date__c = null;
     }
    }
    
}
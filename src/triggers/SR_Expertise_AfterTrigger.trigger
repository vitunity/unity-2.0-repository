/**************************************************************************
 *    Last Modified By      : Yukti Jain
 *    Last Modified on      : 10/03/2015
 *    Last Modified Reason  : Code Optimization for SR_Class_Expertise
****************************************************************************/
trigger SR_Expertise_AfterTrigger on SVMXC__Service_Group_Skills__c (After Insert,after update) 
{

//Audit Tracking Future Invoke
    if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter && !System.isFuture()) SObjectHistoryProcessor.trackHistory('SVMXC__Service_Group_Skills__c');
    

    SR_Class_Expertise clsObj = new SR_Class_Expertise(); // Initializing class
    clsObj.createCaseWOupdateTechnician(Trigger.New, Trigger.oldmap); // Method call
}
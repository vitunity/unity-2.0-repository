/************************************************************************
    @ Author        : Priti Jaiswal
    @ Date      :     21-April-2014
    @ Description   : This trigger execute all the method defined for expertis in class SR_Class_Expertise 
    @ Last Modified By  :  Yukti Jain
    @ Last Modified On  :   10/03/2015
    @ Last Modified Reason  : Code Optimization for SR_Class_Expertise
****************************************************************************/
trigger SR_Expertise_BeforeTrigger on SVMXC__Service_Group_Skills__c (Before insert, before update)
{
    SR_Class_Expertise clsObj = new SR_Class_Expertise(); // Initializing class
    clsObj.updateFieldsOnExpertise(Trigger.New, Trigger.oldMap); // Method call
}
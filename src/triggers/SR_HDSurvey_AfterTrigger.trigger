/***************************************************************************
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
09-06-2017 - Rakesh - STSK0012699 - English traslation for field Write-in Comments.
*************************************************************************************/
/***Us4134 kaushiki 26/08/14 translation of write in comments from userlocale to english if user locale is not english*****/
trigger SR_HDSurvey_AfterTrigger on HD_Case_Closed_Survey_Results__c (after Insert,after update) {
set<Id> HDidSet = new set<Id>();
set<Id> FSSurveyIds = new set<Id>(); //STSK0012699
set<Id> HDRelidSet = new set<Id>();
set<id> caseIdset = new set<id>();
SR_Class_HDSurvey classHD = new SR_Class_HDSurvey();
    if(trigger.isInsert)
    {
        Id HelpDeskId = Schema.SObjectType.HD_Case_Closed_Survey_Results__c.getRecordTypeInfosByName().get('HelpDesk').getRecordTypeId();
        Id RelationshipId = Schema.SObjectType.HD_Case_Closed_Survey_Results__c.getRecordTypeInfosByName().get('Relationship').getRecordTypeId();
        Id FSRTId = Schema.SObjectType.HD_Case_Closed_Survey_Results__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId(); //STSK0012699: Added new recordtype. 
        for(HD_Case_Closed_Survey_Results__c varHD : trigger.new)
        {
            if(varHD.Write_in_comments__c!=null && varHD.RecordTypeId == HelpDeskId)
            {
              HDidSet.add(varHD.id);  
            }
            //STSK0012699.Start
            if(varHD.Write_in_Comments_FS__c != null && varHD.RecordTypeId == FSRTId && 
            (trigger.isInsert || (trigger.isUpdate && trigger.oldmap.get(varHD.id).Write_in_Comments_FS__c <> varHD.Write_in_Comments_FS__c)))
            {
              FSSurveyIds.add(varHD.id);  
            }
            //STSK0012699.End
        }
        if(HDidSet != null)
        SR_Class_HDSurvey.TranslatetheComments(HDidSet);
    
        if(FSSurveyIds != null) //STSK0012699
        SR_Class_HDSurvey.TranslateFSComment(FSSurveyIds); //STSK0012699
    
        //Rakesh.04/11/2016.Start
        for(HD_Case_Closed_Survey_Results__c varHD : trigger.new)
        {
            if(  varHD.RecordTypeId == RelationshipId && (varHD.Comments_on_products_and_services__c != null || varHD.Write_in_comments__c!=null))
            {
              HDRelidSet.add(varHD.id);  
            }
                
        }
        if(HDRelidSet != null)
        SR_Class_HDSurvey.RltshipTranslatetheComments(HDRelidSet);
        //Rakesh.04/11/2016.End
        
        
        
    }
    List<HD_Case_Closed_Survey_Results__c> SurvList = new List<HD_Case_Closed_Survey_Results__c>();
    list<string> objidlist = new list<string>();
    for(HD_Case_Closed_Survey_Results__c SurveyRec : trigger.new)
    {
        if(Trigger.isInsert)
        {
            if(SurveyRec.Follow_Up_Status__c == 'Complete' && SurveyRec.Action_Item_Approver__c != Null)
            {
                SurvList.add(SurveyRec);
                objidlist.add(SurveyRec.id);
            }
        }
        
        if(Trigger.isUpdate)
        {
            
            system.debug('new value---'+SurveyRec.Follow_Up_Status__c);
            system.debug('old value---'+Trigger.OldMap.get(SurveyRec.id).Follow_Up_Status__c);
            if(SurveyRec.Follow_Up_Status__c == 'Complete' && SurveyRec.Follow_Up_Status__c != Trigger.OldMap.get(SurveyRec.id).Follow_Up_Status__c && SurveyRec.Action_Item_Approver__c != Null)
            {
                SurvList.add(SurveyRec);
                objidlist.add(SurveyRec.id);
            }
        }
    }
    system.debug('SurvList---'+SurvList);
    Map<string,ProcessInstance> instancemap = new map<String,ProcessInstance>();
    for(Processinstance p :[select id,status,TargetObjectId from Processinstance where TargetObjectId IN: objidlist and (status ='Pending' or status ='Started')]){
        Instancemap.put(p.TargetObjectId ,p);
    }
    
    map<Id,RecordType> mapRecordType = new map<Id,RecordType>([SELECT id, Name FROM RecordType WHERE SobjectType= 'HD_Case_Closed_Survey_Results__c' and (Name = 'HelpDesk' or Name = 'Relationship' or name = 'Field Service')]); //ENHC0010862
    
    if(SurvList.size() > 0){
        for (HD_Case_Closed_Survey_Results__c SubAprrvl : SurvList) {
           if(mapRecordType <> null && mapRecordType.size()>0 && mapRecordType.containsKey(SubAprrvl.RecordTypeId)){  //ENHC0010862
           if(!Instancemap.containskey(SubAprrvl.id)){

               Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
               app.setObjectId(SubAprrvl.id);
               //app.setSubmitterId(SubAprrvl.OwnerId);
               Approval.ProcessResult result = Approval.process(app);
           }
        }
      }
    }   
}
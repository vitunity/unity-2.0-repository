/***************************************************************************
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
12-14-2017 - Rakesh - STSK0013524 - Overall Sat Score to update if RT is not Field Service.
09-06-2017 - Rakesh - STSK0012699 - Survey Owner for Field Service RT.
*************************************************************************************/
trigger SR_HDSurvey_BeforeTrigger on HD_Case_Closed_Survey_Results__c (before update,before Insert) {
set<id> OwnerIdSet = new set<id>();
set<id> CurrentOwnerIdSet = new set<id>();
Map<Id, HD_Case_Closed_Survey_Results__c> rejectedStatements = new Map<Id, HD_Case_Closed_Survey_Results__c>{};

        if(trigger.isInsert && trigger.isBefore){
            AutoFillAccountfield.FillSurvey(trigger.new);
        }
    
  // US4134 kaushiki start.   
  if(trigger.IsUpdate)
    {
        
        /*Custom Owner*/
        Id relationshipRT = Schema.SObjectType.HD_Case_Closed_Survey_Results__c.getRecordTypeInfosByName().get('Relationship').getRecordTypeId();
        //map<Id,RecordType> RsMap = new map<Id,RecordType>([select Id from RecordType Where Name = 'Relationship' and sObjectTYpe = 'HD_Case_Closed_Survey_Results__c']);
        for(HD_Case_Closed_Survey_Results__c varHD : trigger.new)
        {
            if(varHD.recordTypeId == relationshipRT  && varHD.Survey_Owner__c == null){
                varHD.addError('Owner should not be empty');
            }
            if(varHD.recordTypeId == relationshipRT && varHD.Survey_Owner__c <> null && varHD.Survey_Owner__c <> Trigger.OldMap.get(varHD.Id).Survey_Owner__c){
                varHD.OwnerId = varHD.Survey_Owner__c;
            }
        }
        /*End Custom Owner*/
        
        
    /*  //ENHC0010862.Start
        set<Id> setPermissionSetIds = new set<Id>();
        for(PermissionSetAssignment p : [SELECT Assignee.Id FROM PermissionSetAssignment WHERE PermissionSet.Name = 'Survey_Results']){
            setPermissionSetIds.add(p.Assignee.Id); 
        }
        
        for(HD_Case_Closed_Survey_Results__c varHD : trigger.new)
        {
            if(varHD.Follow_Up_Status__c <> Trigger.OldMap.get(varHD.Id).Follow_Up_Status__c){
                Id CUserId = userinfo.getUserId();
                if(!setPermissionSetIds.contains(CUserId) && varHD.OwnerId <> CUserId){
                    if(varHD.Action_Item_Approver__c <> null && varHD.Action_Item_Approver__c <> CUserId){
                        varHD.addError('You do not have permission to update Followup Status');
                    }
                    else if(varHD.Approvers_Manager__c <> null && varHD.Approvers_Manager__c <> CUserId){
                        varHD.addError('You do not have permission to update Followup Status');
                    }else{
                        varHD.addError('You do not have permission to update Followup Status');
                    }
                }                   
            }
        }
        //ENHC0010862.End  */
        Id FSRT = Schema.SObjectType.HD_Case_Closed_Survey_Results__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        for(HD_Case_Closed_Survey_Results__c varHD : trigger.new)
        {
        
            
            if (Trigger.oldMap.get(varHD.Id).Approval_Status__c != 'Requires Further Action' && varHD.Approval_Status__c == 'Requires Further Action')
            { 
              rejectedStatements.put(varHD.Id, varHD);  
            }
            system.debug('Overall_Sat__c---'+varHD.Overall_Sat__c);
             if(varHD.Overall_Sat__c != Null && varHD.Overall_Sat__c != '' && FSRT <> varHD.RecordTypeId) //Added RT. Rakesh STSK0013524 
            {
                varHD.Overall_Sat_Score__c = Integer.valueof(varHD.Overall_Sat__c);
            }
            system.debug('Access_to_HD__c---'+varHD.Access_to_HD__c);
            if(varHD.Access_to_HD__c != Null && varHD.Access_to_HD__c != '')
            {
                varHD.Access_to_HD_Score__c = Integer.valueof(varHD.Access_to_HD__c);
            }
            system.debug('Agent_skills__c---'+varHD.Agent_skills__c);
            if(varHD.Agent_skills__c != Null && varHD.Agent_skills__c != '')
            {
                varHD.Agent_skills_score__c = Integer.valueof(varHD.Agent_skills__c);
            }
            system.debug('Issue_Handling_Resolution__c---'+varHD.Issue_Handling_Resolution__c);
            if(varHD.Issue_Handling_Resolution__c != Null && varHD.Issue_Handling_Resolution__c != '')
            {
                varHD.Issue_Handling_Resolution_Score__c = Integer.valueof(varHD.Issue_Handling_Resolution__c);
            }
            system.debug('Product_functionality__c---'+varHD.Product_functionality__c);
            if(varHD.Product_functionality__c != Null && varHD.Product_functionality__c != '')
            {
                varHD.Product_functionality_score__c = Integer.valueof(varHD.Product_functionality__c);
            }
            system.debug('time_to_resolve__c---'+varHD.time_to_resolve__c);
            if(varHD.time_to_resolve__c != Null && varHD.time_to_resolve__c != '')
            {
                varHD.Time_to_resolve_score__c = Integer.valueof(varHD.time_to_resolve__c);
            }
        }     
      if (!rejectedStatements.isEmpty()){ map<Id,HD_Case_Closed_Survey_Results__c> processInstanceIds = new map<Id,HD_Case_Closed_Survey_Results__c>{};for (HD_Case_Closed_Survey_Results__c HdSurvey : [SELECT (SELECT ID FROM ProcessInstances ORDER BY CreatedDate DESC ) FROM HD_Case_Closed_Survey_Results__c  WHERE ID IN :rejectedStatements.keySet()]){
        // if(HdSurvey.ProcessInstaces <> null && HdSurvey.ProcessInstance.size() >0 )
        processInstanceIds.put(HdSurvey.ProcessInstances[0].Id,HdSurvey);}       
        
        // the most recent process steps for comments.  
        for (ProcessInstance pi : [SELECT TargetObjectId,(SELECT Id, StepStatus, Comments FROM Steps ORDER BY CreatedDate DESC  LIMIT 1 ) FROM ProcessInstance  WHERE Id IN :processInstanceIds.keySet()  ORDER BY CreatedDate DESC]){   if ((pi.Steps[0].Comments == null ||  pi.Steps[0].Comments.trim().length() == 0))         rejectedStatements.get(pi.TargetObjectId).addError(' Please provide a rejection reason!');
          else
          {   HD_Case_Closed_Survey_Results__c hd = new HD_Case_Closed_Survey_Results__c(id = processInstanceIds.get(pi.id).id);          system.debug('check hd--->'+hd+'---->'+hd.id);          trigger.newmap.get(processInstanceIds.get(pi.id).id).Rejection_Reason__c = pi.Steps[0].Comments;          system.debug('check hd.Rejection_Reason__c--->'+hd.Rejection_Reason__c+'pi.Steps[0].Comments---->'+pi.Steps[0].Comments);}
        }  
      }
       //ENHC0010862.Start 
        for(HD_Case_Closed_Survey_Results__c s: trigger.new)      {    if(s.OwnerId <> trigger.oldmap.get(s.Id).OwnerId)  s.Old_Owner_Email__c =trigger.oldmap.get(s.Id).OwnerId; }           
        
       //ENHC0010862.End
    }
    // US4134 kaushiki end.
    // kaushiki 26/08/14 US4134 update owner with case closedBy manager id sat is 1,2 or 3
    
    set<id> caseIdset = new set<id>();
    set<id> caseIdsetWhenSurveyIsDissatisfied = new set<id>();
    list<HD_Case_Closed_Survey_Results__c> HDList = new list<HD_Case_Closed_Survey_Results__c>();
    
    if(trigger.IsInsert)
    {   
        
        
        
        for(HD_Case_Closed_Survey_Results__c varHD : trigger.new)
        {
                        
            /*Custom Owner*/
            if(varHD.OwnerId <> null)
                varHD.Survey_Owner__c  = varHD.OwnerId;
            /*End Custom Owner*/
        
            if(varHD.Case__c!=null)
            {
                caseIdset.add(varHD.Case__c);
            }
            system.debug('sat---->'+varHD.Overall_Sat__c);
            If(varHD.Overall_Sat__c == '1' || varHD.Overall_Sat__c == '2' || varHD.Overall_Sat__c == '3')
            {
               caseIdsetWhenSurveyIsDissatisfied.add(varHD.Case__c);
               HDList.add(varHD);
            }
            if(varHD.Overall_Sat__c != Null && varHD.Overall_Sat__c != '')
            {
                varHD.Overall_Sat_Score__c = Integer.valueof(varHD.Overall_Sat__c);
            }
            system.debug('Access_to_HD__c---'+varHD.Access_to_HD__c);
            if(varHD.Access_to_HD__c != Null && varHD.Access_to_HD__c != '')
            {
                varHD.Access_to_HD_Score__c = Integer.valueof(varHD.Access_to_HD__c);
            }
            system.debug('Agent_skills__c---'+varHD.Agent_skills__c);
            if(varHD.Agent_skills__c != Null && varHD.Agent_skills__c != '')
            {
                varHD.Agent_skills_score__c = Integer.valueof(varHD.Agent_skills__c);
            }
            system.debug('Issue_Handling_Resolution__c---'+varHD.Issue_Handling_Resolution__c);
            if(varHD.Issue_Handling_Resolution__c != Null && varHD.Issue_Handling_Resolution__c != '')
            {
                varHD.Issue_Handling_Resolution_Score__c = Integer.valueof(varHD.Issue_Handling_Resolution__c);
            }
            system.debug('Product_functionality__c---'+varHD.Product_functionality__c);
            if(varHD.Product_functionality__c != Null && varHD.Product_functionality__c != '')
            {
                varHD.Product_functionality_score__c = Integer.valueof(varHD.Product_functionality__c);
            }
            system.debug('time_to_resolve__c---'+varHD.time_to_resolve__c);
            if(varHD.time_to_resolve__c != Null && varHD.time_to_resolve__c != '')
            {
                varHD.Time_to_resolve_score__c = Integer.valueof(varHD.time_to_resolve__c);
            }
            
        }
        map<id,case> caseMapForAllClosedByManagers = new map<id,case>([select id,Owner.Name,Ownerid,ClosedBy__r.Email,ClosedBy__c,ClosedBy__r.ManagerId,ClosedBy__r.Manager.Email,ClosedBy__r.Manager.Manager.Email,ClosedBy__r.Manager.Manager.Manager.Email from case where id in :caseIdset]);
        map<id,case> caseMap = new map<id,case>( [select id,Owner.Name,ClosedBy__c,ClosedBy__r.ManagerId from case where id in :caseIdsetWhenSurveyIsDissatisfied ]);
       
        system.debug('caseMap ---->'+caseMap );
        set<Id> setAccountIds = new set<Id>(); 
        for(HD_Case_Closed_Survey_Results__c varHD : trigger.new)
        {
            
            setAccountIds.add(varHD.Account__c);
            setAccountIds.add(varHD.Customer_Account__c);
            
            if(caseMapForAllClosedByManagers.containsKey(varHD.Case__c))
            {
                system.debug('caseMapForAllClosedByManagers.get(varHD.Case__c).Ownerid----'+caseMapForAllClosedByManagers.get(varHD.Case__c).Ownerid);
                varHD.User__c = caseMapForAllClosedByManagers.get(varHD.Case__c).OwnerId;
               system.debug('varHD.Overall_Sat__c---'+varHD.Overall_Sat__c);
                If(varHD.Overall_Sat__c == '4' || varHD.Overall_Sat__c == '5' || varHD.Overall_Sat__c == '6')
                {
                   varHD.Follow_Up_Status__c = '';
                   varHD.Target_Completion_Date__c = Null;
                }
                system.debug('varHD.Case_Owner__c----'+varHD.User__c);
            }
            if(caseMap.containsKey(varHD.Case__c))
            {
                
            system.debug('contains  ---->'+caseMap );
                if(caseMap.get(varHD.Case__c).ClosedBy__r.ManagerId!=null)
                {
                system.debug('caseMap.get(varHD.Case__c).ClosedBy__r.ManagerId ---->'+caseMap.get(varHD.Case__c).ClosedBy__r.ManagerId);
                    varHD.OwnerId = caseMap.get(varHD.Case__c).ClosedBy__r.ManagerId;
                    system.debug(' varHD.OwnerId ---->'+ varHD.OwnerId +'magr id --->'+caseMap.get(varHD.Case__c).ClosedBy__r.ManagerId );
                }
            }
            if(caseMapForAllClosedByManagers.containsKey(varHD.Case__c))
            {
            system.debug('***********************');
                /*if(caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Email!=null)
                {
                system.debug('***********************caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Email'+caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Email);
                    varHD.Case_ClosedBy_Email__c = caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Email;
                system.debug('***********************varHD.Case_ClosedBy_Email__c'+varHD.Case_ClosedBy_Email__c);
                } */   
                if(caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Email!=null)
                {
                system.debug('***********************caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Email'+caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Email);
                    varHD.Case_ClosedBy_Manager_Email__c = caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Email;
                    system.debug('***********************varHD.Case_ClosedBy_Manager_Email__c'+varHD.Case_ClosedBy_Manager_Email__c);
                }
                if(caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Manager.Email!=null)
                {
                system.debug('***********************caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Manager.Email'+caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Manager.Email);
                    varHD.Case_ClosedBy_Manager_Manager_Email__c = caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Manager.Email;
                    system.debug('********************** varHD.Case_ClosedBy_Manager_Manager_Email__c'+ varHD.Case_ClosedBy_Manager_Manager_Email__c);
                }
                if(caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Manager.Manager.Email!=null)
                {
                system.debug('***********************caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Manager.Manager.Email'+caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Manager.Manager.Email);
                    varHD.ClosedBy_Manager_Manager_Manager_Email__c = caseMapForAllClosedByManagers.get(varHD.Case__c).ClosedBy__r.Manager.Manager.Manager.Email;
                system.debug('********************** varHD.ClosedBy_Manager_Manager_Manager_Email__c'+ varHD.ClosedBy_Manager_Manager_Manager_Email__c);
                }
            }
            CurrentOwnerIdSet.add(varHD.ownerId);
        }

        //ENHC0010862.Start     
        map<string,Id> mapCSSAccount = new map<string,Id>();
        map<Id,Id> mapAccountOwner = new map<Id,Id>();
        map<Id,Id> mapAccountOwnerManager = new map<Id,Id>();
        map<Id,Id> mapDistrictManager = new map<Id,Id>();
        map<Id,Id> mapDistrictManagerManager = new map<Id,Id>();
        for(Account Acc:[select Id,OwnerId,Owner.ManagerId,CSS_District__c from Account where Id IN: setAccountIds]){
            mapAccountOwner.put(Acc.Id,Acc.OwnerId);
            if(Acc.OwnerId <> null){
                if(Acc.Owner.ManagerId <> null){
                    mapAccountOwnerManager.put(Acc.Id,Acc.Owner.ManagerId);
                }
            }               
            if(Acc.CSS_District__c <> null)
                mapCSSAccount.put(Acc.CSS_District__c,Acc.Id);
        }
        
        for(SVMXC__Service_Group__c st:[select Id,District_Manager__c,District_Manager__r.Manager.ManagerId,District_Manager__r.ManagerId,SVMXC__Group_Code__c from SVMXC__Service_Group__c where SVMXC__Group_Code__c IN: mapCSSAccount.keyset()]){
           
    /*     if(st.District_Manager__c <> null){
                if(st.District_Manager__r.ManagerId <> null){
                    mapDistrictManager.put(mapCSSAccount.get(st.SVMXC__Group_Code__c),st.District_Manager__r.ManagerId);
                    if(st.District_Manager__r.Manager.ManagerId <> null){
                        mapDistrictManagerManager.put(mapCSSAccount.get(st.SVMXC__Group_Code__c),st.District_Manager__r.Manager.ManagerId);
                    }
                }
            } */
           if(st.District_Manager__c <> null){
               mapDistrictManager.put(mapCSSAccount.get(st.SVMXC__Group_Code__c),st.District_Manager__c);
                   if(st.District_Manager__r.ManagerId <> null){
                       mapDistrictManagerManager.put(mapCSSAccount.get(st.SVMXC__Group_Code__c),st.District_Manager__r.ManagerId);
                   } 
           } 
            
        }
        
        Id FSRTId = Schema.SObjectType.HD_Case_Closed_Survey_Results__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        Id ClickToolUser = [select Id from User where Name = 'VMS-clicktools'].id;
        for(HD_Case_Closed_Survey_Results__c varHD : trigger.new)
        {
         
          
          /*  if(varHD.Account__c <> null){
                if(mapAccountOwner.containsKey(varHD.Account__c))
                    varHD.District_Sales_Manager__c = mapAccountOwner.get(varHD.Account__c);
                if(mapAccountOwnerManager.containsKey(varHD.Account__c))
                    varHD.Sales_Regional_Manager__c = mapAccountOwnerManager.get(varHD.Account__c);
                if(mapDistrictManager.containsKey(varHD.Account__c))
                    varHD.District_Service_Manager__c = mapDistrictManager.get(varHD.Account__c);
                if(mapDistrictManagerManager.containsKey(varHD.Account__c))
                    varHD.Service_Regional_Manager__c = mapDistrictManagerManager.get(varHD.Account__c);
            }  */
             if(varHD.Customer_Account__c <> null){
                if(mapAccountOwner.containsKey(varHD.Customer_Account__c))
                {
                    varHD.District_Sales_Manager__c = mapAccountOwner.get(varHD.Customer_Account__c);
                }
                if(mapAccountOwnerManager.containsKey(varHD.Customer_Account__c)) varHD.Sales_Regional_Manager__c = mapAccountOwnerManager.get(varHD.Customer_Account__c);
                if(mapDistrictManager.containsKey(varHD.Customer_Account__c)) varHD.District_Service_Manager__c = mapDistrictManager.get(varHD.Customer_Account__c);
                if(mapDistrictManagerManager.containsKey(varHD.Customer_Account__c)) varHD.Service_Regional_Manager__c = mapDistrictManagerManager.get(varHD.Customer_Account__c);
            }
            //ENHC0010862.End 
            /*STSK0012699 - Survey Owner for Field Service RT*/  
            If(ClickToolUser <> null && (varHD.Overall_Sat__c == '4' || varHD.Overall_Sat__c == '5' || varHD.Overall_Sat__c == '6' || varHD.Overall_Sat__c == null)){
                varHD.Survey_Owner__c = ClickToolUser;
            }
            else If(varHD.Overall_Sat__c == '1' || varHD.Overall_Sat__c == '2' || varHD.Overall_Sat__c == '3'){
                if(varHD.RecordTypeId == FSRTId){
                    varHD.Survey_Owner__c  = varHD.District_Service_Manager__c ;
                    CurrentOwnerIdSet.add(varHD.Survey_Owner__c );
                }           
            }            
            /*STSK0012699 End -Survey Owner for Field Service RT*/
            
        }
        
        Map <Id,User> MapCurrentUser = new Map<id,User>([Select id,Manager.ManagerId,ManagerId,Manager.Email from User where id in :CurrentOwnerIdSet]); 
             
             
        for(HD_Case_Closed_Survey_Results__c varHD : trigger.new)
        { 
            Id SurveyOwner = varHD.OwnerId;
            if(varHD.RecordTypeId == FSRTId){
                SurveyOwner = varHD.Survey_Owner__c;
            }
            if(MapCurrentUser.containsKey(SurveyOwner))
            {
                if(MapCurrentUser.get(SurveyOwner).ManagerId!=null)
                {
                    varHD.Action_Item_Approver__c = MapCurrentUser.get(SurveyOwner).ManagerID;
                }
                if(MapCurrentUser.get(SurveyOwner).Manager.ManagerId!=null)
                {
                    varHD.Approvers_Manager__c = MapCurrentUser.get(SurveyOwner).Manager.ManagerId;
                }
            }
                
        }
   
    }
}
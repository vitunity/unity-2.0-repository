/********************************************************************************************************************************
      @ Author          : Nikita Gupta
      @ Date            : 11-Dec-2013.
      @ Description     : This Trigger is for updating Installed Product with Product Version Build ID based on ISC History.
                          US3676
      @ Last Modified By      :  Priti jaiswal  
      @ Last Modified On      :   09/05/2014 
      @ Last Modified Reason  :   US1694 - to update Current ISC config on Insatalled product
*********************************************************************************************************************************/
Trigger SR_ISC_History_c_After_trigger on ISC_History__c(After insert,after update)
{
    SRClassISCHistory Cls = new SRClassISCHistory();
    Cls.ISCHistoryUpdateIPversion(trigger.new, trigger.oldMap);        // method called to apply update IP and Product of ISC History based on PCSN.
}
/***************************************************************************
Author: Chandramouli Vegi
Created Date: 15-Sep-2017
Project/Story/Inc/Task : Keystone Project : Sep 2017 Release
Description: 
This is a after trigger on ISC Parameter object, which calculates the IP Components Varian function field value
based on the ISC Parameter name "Varian Function".

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
*************************************************************************************/
trigger SR_ISC_Parameters_AfterTrigger on ISC_Parameters__c (after insert, after update, after delete) 
{
    map<id,IP_Components__c> ipcomp2update = new Map<Id,IP_Components__c>();
    set<Id> iscparameters = new Set<Id>();
    set<id> deletediscparameters = new set<Id>();
    if (trigger.isDelete)
    {
    	for (ISC_Parameters__c iscp : trigger.old)
    	{
    		if (iscp.iscp_IP_Components__c != null && iscp.name == 'Varian Function')
    		{
                iscparameters.add(iscp.iscp_IP_Components__c);
    			IP_Components__c ipc1 = new IP_Components__c();
                ipc1.id = iscp.iscp_IP_Components__c;
                ipc1.ipc_Varian_Function__c = null;
                ipcomp2update.put(iscp.iscp_IP_Components__c,ipc1);
    		}
    	}
    }
    if (trigger.isInsert || trigger.isUpdate)
    {
    	SR_Class_IscParameter objCls = new SR_Class_IscParameter();
    	objCls.updateInstProdFrmISCParam(trigger.new);        //method to update ISC Parameter List and Product Version List on Installed Product
    	for (ISC_Parameters__c iscp : trigger.new)
    	{
    		if (iscp.iscp_IP_Components__c != null)
    		{
    			iscparameters.add(iscp.iscp_IP_Components__c);
    		}
    	}
    }
    if (iscparameters.size() > 0)
    {
    	for (ISC_Parameters__c iscparameters : [select id,iscp_IP_Components__c,Parameter_Value__c from ISC_Parameters__c where iscp_IP_Components__c in 
    	                                        :iscparameters and Name = 'Varian Function' order by lastmodifieddate desc limit 1 ])
    	{
    		IP_Components__c ipc = new IP_Components__c();
    		ipc.id = iscparameters.iscp_IP_Components__c;
    		ipc.ipc_Varian_Function__c = iscparameters.Parameter_Value__c;
    		ipcomp2update.put(iscparameters.iscp_IP_Components__c,ipc);
    	}
    }
    if (ipcomp2update.size() > 0 )
    {
    	update ipcomp2update.values();
    }
}
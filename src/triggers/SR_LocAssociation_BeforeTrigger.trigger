/**************************************************************

Created By  :   Jaroslaw Kondrat
Created on  :   12/02/2015

**************************************************************/
trigger SR_LocAssociation_BeforeTrigger on Location_Association__c (before insert, before update) {
    for(Location_Association__c varLoc : trigger.new){
        if(varLoc.Name.startsWithIgnoreCase('AT') && varLoc.Location__c != null && varLoc.Partner_Function__c != null){ //jkondrat.FF DE6351
            varLoc.Name=varLoc.Location__r.Name +'-'+ varLoc.Partner_Function__c; 
        }
    }
}
/******************************
Created By      :   Parul Gupta
Created on      :   01/02/2014
Created Reason  :   DE2307
/***********************/
trigger SR_Location_AfterTrigger on SVMXC__Site__c (after insert, after update) 
{
    Map<Id, string> mapLocationId_ERPdistrict = new Map<Id, string>();
    Map<Id, Id> mapLocationId_Account = new Map<Id, Id>();
    Map<Id, SVMXC__Installed_Product__c> mapUpdateInstalledProduct = new Map<Id, SVMXC__Installed_Product__c>();
    //List<SVMXC__Installed_Product__c> listUpdateInstalledProduct = new List<SVMXC__Installed_Product__c>();

    for(SVMXC__Site__c varLoc : Trigger.new)
    {
        if(Trigger.oldMap != null && varLoc.ERP_CSS_District__c != null && Trigger.oldMap.get(varLoc.Id).ERP_CSS_District__c != varLoc.ERP_CSS_District__c)
        {
            mapLocationId_ERPdistrict.put(varLoc.Id, varLoc.ERP_CSS_District__c);
        }
        //update Account on child IP if Location.Account is changed (and update it with null if location.Account is changed to null)
        if(Trigger.oldMap != null && (Trigger.oldMap.get(varLoc.Id).SVMXC__Account__c != varLoc.SVMXC__Account__c))
        {
           mapLocationId_Account.put(varLoc.Id, varLoc.SVMXC__Account__c); 
        }
    }
        
    if(mapLocationId_ERPdistrict.size() > 0 || mapLocationId_Account.size() > 0)
    {
        //List<SVMXC__Installed_Product__c> listInstalledProduct = [select Id, SVMXC__Site__c, SVMXC__Company__c, ERP_CSS_District__c from
       //                                                            SVMXC__Installed_Product__c where SVMXC__Site__c in :mapLocationId_ERPdistrict.keySet()
       //                                                             OR SVMXC__Site__c in :mapLocationId_Account.keySet()];
        //{
            //system.debug('IP >'+listInstalledProduct);
           // if(listInstalledProduct.size() > 0)
            //{
                for(SVMXC__Installed_Product__c varIP : [select Id, SVMXC__Site__c, SVMXC__Company__c, ERP_CSS_District__c from
                                                                    SVMXC__Installed_Product__c where SVMXC__Site__c in :mapLocationId_ERPdistrict.keySet()
                                                                    OR SVMXC__Site__c in :mapLocationId_Account.keySet()])
                {
                    if(mapLocationId_ERPdistrict.containsKey(varIP.SVMXC__Site__c) && mapLocationId_ERPdistrict.get(varIP.SVMXC__Site__c) != null)
                    {
                        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c();

                        if(mapUpdateInstalledProduct.containsKey(varIP.Id))
                        {
                            objIP = mapUpdateInstalledProduct.get(varIP.Id);
                        }
                        else
                        {
                            objIP = varIP;
                        }

                        objIP.ERP_CSS_District__c = mapLocationId_ERPdistrict.get(objIP.SVMXC__Site__c);
                        mapUpdateInstalledProduct.put(objIP.Id, objIP);
                        //listUpdateInstalledProduct.add(varIP);
                    }
                    if(mapLocationId_Account.containsKey(varIP.SVMXC__Site__c))
                    {
                        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c();

                        if(mapUpdateInstalledProduct.containsKey(varIP.Id))
                        {
                            objIP = mapUpdateInstalledProduct.get(varIP.Id);
                        }
                        else
                        {
                            objIP = varIP;
                        }

                        if(mapLocationId_Account.get(objIP.SVMXC__Site__c) != null)
                        {
                            objIP.SVMXC__Company__c = mapLocationId_Account.get(objIP.SVMXC__Site__c);
                        }
                        else
                        {
                            objIP.SVMXC__Company__c = null;
                        }
                        mapUpdateInstalledProduct.put(objIP.Id, objIP);
                    }
                }
                if(mapUpdateInstalledProduct.size() > 0)
                {
                    update mapUpdateInstalledProduct.values();
                }
           // }
        //}
    }
        //Rakesh.06/28/2016.Start
    set<Id> setAccountIds = new set<Id>();
    for(SVMXC__Site__c l : Trigger.new)
    {
        if(
            (trigger.isInsert && l.ERP_CSS_District__c <> null && l.SVMXC__Account__c <> null) || 
            (trigger.isUpdate && l.ERP_CSS_District__c <> null && l.SVMXC__Account__c <> null && trigger.oldmap.get(l.id).ERP_CSS_District__c <> l.ERP_CSS_District__c)
        )
        {
                setAccountIds.add(l.SVMXC__Account__c);
        }
    }
    
    map<Id,Account> mapAccount = new map<Id,Account>([select Id,CSS_District__c from Account where Id IN: setAccountIds]);
    List<Account> lstAccount = new List<Account>();
    
    for(SVMXC__Site__c l : Trigger.new)
    {
        if(
            (trigger.isInsert && l.ERP_CSS_District__c <> null) || 
            (trigger.isUpdate && l.ERP_CSS_District__c <> null && trigger.oldmap.get(l.id).ERP_CSS_District__c <> l.ERP_CSS_District__c)
        )
        {
            if(mapAccount.containsKey(l.SVMXC__Account__c))
            {
                Account Acc = mapAccount.get(l.SVMXC__Account__c);
                Acc.CSS_District__c = l.ERP_CSS_District__c;
                lstAccount.add(Acc);
            }
        }
    }       
    
    if(lstAccount.size() > 0 ){
        update lstAccount;
    }
    //Rakesh.06/28/2016.End
}
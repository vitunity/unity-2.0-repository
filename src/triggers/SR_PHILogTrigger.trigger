trigger SR_PHILogTrigger on PHI_Log__c (before insert, before update, after insert, after update ) {
    //Audit Tracking Future Invoke (After Insert, Update enabled)

    Map<Id, Case> mapCase;
    List<String> caseIdList = new List<String>();

    if ((trigger.isUpdate || trigger.isInsert) && trigger.isBefore){
        list<PHI_Log__c> lstPhiLogsChangeOwner = new list<PHI_Log__c>();
        list<PHI_Log__c> lstPhiLogsChangetoqueue = new list<PHI_Log__c>();
        list<PHI_Log__c> lstPhiLogsmovefolder = new list<PHI_Log__c>();
        list<PHI_Log__c> lstPhiLogsdeletefolder = new list<PHI_Log__c>();
        list<PHI_Log__c> lstPhiLogsrenamefolder = new list<PHI_Log__c>();

        for (PHI_Log__c PL:trigger.new) {
            if(PL.Case__c != null && trigger.oldmap == null) {
                caseIdList.add(PL.Case__c);
                //PL.Account__c = PL.Case__r.AccountId;
            } else if(PL.Case__c != null && trigger.oldmap != null && 
                trigger.oldmap.get(PL.id).Case__c != PL.Case__c) {
                caseIdList.add(PL.Case__c);
                //PL.Account__c = PL.Case__r.AccountId;
            }

            if(PL.Disposition2__c =='Transferred - Owner Copy Destroyed/Sent to New Owner' && 
               !(String.ValueOf(PL.OwnerId).startswith('00G')) && PL.Owner_Transferred__c==FALSE && 
               PL.Folder_Id__c!=Null && PL.Case_Folder_Id__c!=Null && 
               PL.Disposition2__c != 'Destroyed') {
                system.debug('@@call changeofownershipclass');
                lstPhiLogsChangeOwner.add(PL);
            }

            if(trigger.oldmap != null && (String.ValueOf(PL.OwnerId).startswith('00G')) && 
                PL.Folder_Id__c!=Null && PL.Case_Folder_Id__c!=Null && 
                PL.Disposition2__c != 'Destroyed') {
                system.debug('@@Call changeofQueue');
                lstPhiLogsChangetoqueue.add(PL);
            }

            if(PL.Complaint__c != Null && (trigger.oldmap != null) &&  
               trigger.oldmap.get(PL.id).Complaint__c != PL.Complaint__c &&  
               PL.IsFolderMovedToECT__c) {
                lstPhiLogsrenamefolder.add(PL);
            }

            if(PL.Complaint__c != Null && (trigger.oldmap != null) &&  
               //(String.ValueOf(PL.OwnerId).startswith('00G') || trigger.oldmap.get(PL.id).OwnerId != PL.OwnerId) &&   
               //trigger.oldmap.get(PL.id).OwnerId != PL.OwnerId &&  
               PL.Disposition2__c != 'Destroyed' && !PL.IsFolderMovedToECT__c) {
                //List<Group> grpList = [select Id, Name from Group where  Id =: PL.OwnerId Limit 1];
                //if((!grpList.isEmpty() && grpList[0].Name == label.Box_Queue_Name) || 
                //   trigger.oldmap.get(PL.id).OwnerId != PL.OwnerId) {
                //  lstPhiLogsmovefolder.add(PL);
                //}
                lstPhiLogsmovefolder.add(PL);
            }

            if(PL.Disposition2__c == 'Destroyed') {
                lstPhiLogsdeletefolder.add(PL);
            }

            if(PL.OwnerId != Null && trigger.oldmap != null && trigger.oldmap.get(PL.id).OwnerId != PL.OwnerId && PL.Disposition2__c != 'Destroyed') {
                PL.Disposition_Date__c = date.today();
            }
        }

        if(lstPhiLogsmovefolder.size()>0 && !Ltng_BoxProcessorControl.inFutureContext && !System.isBatch()) {
            System.debug('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'+lstPhiLogsmovefolder);
            //BoxMoveFolder.AfterUpdate(lstPhiLogsmovefolder,Trigger.oldMap);
            Ltng_BoxMoveFolder.moveBoxFolder(lstPhiLogsmovefolder,Trigger.oldMap);
        }
        if(!lstPhiLogsChangeOwner.isEmpty() && !Ltng_BoxProcessorControl.inFutureContext && !System.isBatch()) {
            Ltng_BoxChangeOfOwnership.updateBoxOwner(lstPhiLogsChangeOwner, Trigger.oldmap);
        }
        if(!lstPhiLogsChangetoqueue.isEmpty() && lstPhiLogsmovefolder.isEmpty() && !Ltng_BoxProcessorControl.inFutureContext && !System.isBatch()) {
            Ltng_BoxChangeToQueue.updateBoxOwnerQueue(lstPhiLogsChangetoqueue, Trigger.oldmap);
        }
        if(!lstPhiLogsdeletefolder.isEmpty() && !Ltng_BoxProcessorControl.inFutureContext && !System.isBatch()) {
            Ltng_BoxFolderDeletion.deleteBoxFolder(lstPhiLogsdeletefolder);
        }
        if(!lstPhiLogsrenamefolder.isEmpty() && !Ltng_BoxProcessorControl.inFutureContext && !System.isBatch()) {
            Ltng_BoxMoveFolder.renameBoxFolder(lstPhiLogsrenamefolder);
        }
    }

    if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter){
        SObjectHistoryProcessor.trackHistory('PHI_Log__c');
        list<PHI_Log__c> lstPhilogsgtngentrpsids = new list<PHI_Log__c>();

        for (PHI_Log__c PL:trigger.new) {
            if(trigger.oldmap==null || (trigger.oldmap.get(Pl.id).OwnerId!=Pl.OwnerId) && pl.Populated_UserID__c==FALSE) {
                lstPhilogsgtngentrpsids.add(pl);
            }
        }

        if(lstPhilogsgtngentrpsids.size()>0) {
            Boxgetentrprsid.AfterInsert(lstPhilogsgtngentrpsids,Trigger.oldMap);
        }
    }

    if(caseIdList.size()>0) {
        mapCase = new Map<Id, Case>([Select Id, AccountId from Case Where Id in: caseIdList]);
    }
    
    if ((trigger.isInsert || trigger.isUpdate) && trigger.isBefore) {
        set < id > UserID=new set < id >();
        Set<Id> caseIds = new Set<Id>();  //DFCT0011585
        //Marshaling information from PHI Log for Query
        for (PHI_Log__c PL:trigger.new) {
            if((trigger.oldmap == null) || ((trigger.oldmap != null) && (trigger.oldmap.get(PL.id).OwnerID != PL.OwnerID || PL.Employee_s_Manager__c == null || PL.Director_Dept_Head__c == null))) {
                userID.add(PL.OwnerID);
                System.debug(PL.OwnerId);
            }

            if(pl.Case__c != null) { 
                //DFCT0011585
                caseIds.add(pl.Case__c);   
            }            
        }

        //Mapping Owners Manager and Managers manger into the PHI LOG Fields
        map < id , user > mapuser = new map < id , user > ();
        if(UserID.size() > 0){
            mapuser = new map < id , user >([select id, ManagerID , manager.ManagerID from user where id in :userID]);
       
            for (PHI_Log__c UPL:trigger.new) {
                System.debug(UPL.owner.type);
                if(mapuser.containsKey(UPL.OwnerID))  {
                    UPL.Employee_s_Manager__c = mapuser.get(UPL.OwnerID).ManagerID;
                    UPL.Director_Dept_Head__c = mapuser.get(UPL.OwnerID).Manager.ManagerID;
                } else {
                    UPL.Employee_s_Manager__c = null;
                    UPL.Director_Dept_Head__c = null;
                }
            }
        }

        for(PHI_Log__c pl:trigger.new) {
            if(trigger.oldmap!=null && (trigger.oldmap.get(PL.id).Populated_UserID__c==TRUE)) {
                pl.Populated_UserID__c=false;
            }

            if(mapCase != null && mapCase.containsKey(pl.Case__c)) {
                pl.Account__c = mapCase.get(pl.Case__c).AccountId;
            }
        }
    }
}
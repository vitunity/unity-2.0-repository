/*************************************************************************\
    @ Author                : Shubham Jaiswal
    @ Date                  : 3/28/2014
    @ Description           : US4963
    @ Last Modified By      :   Parul Gupta
    @ Last Modified Date    :   5/23/2014
    @ Last Modified Reason  :   US4481, to update fields on Parts Delivery related object based on recieved qty
    @ Last Modified By      :   Parul Gupta
    @ Last Modified Date    :   08/12/2014
    @ Last Modified Reason  :   US4117, to update Fulfillment Qty on Parts Order Line.
    @ Last Modified By      :   Parul Gupta
    @ Last Modified Date    :   08/12/2014
    @ Last Modified Reason  :   Code consolidation.

****************************************************************************/
trigger SR_PartDelivery_AfterTrigger on Part_Delivery__c (after insert,after update) 
{
    if(SR_Class_PartDelivery.UPDATE_PART_DELIVERY_FROM_PART_DELIVERY == true)
        return;

    SR_Class_PartDelivery objPD = new SR_Class_PartDelivery(); //initiating class

    //objPD.updatePartWithProduct(trigger.new, trigger.oldMap); //US4080, code moved to method updatePartsOrderLine, code optimization 
    objPD.updatePartsOrderLine(trigger.new, trigger.oldMap);   //method call for US4481, US4117, US4369
    //objPD.createPartOrderAndLine(trigger.new);// create return part order and line
    objPD.createProdStockStockHistory(trigger.new, trigger.oldMap);     //US4291 

    /********************UPDATE RELATED OBJECTS OF PART DELIVERY**********************/
    //JW@ff 10-23-15 modified updates on maps to use class variable instead of static map as per DM for DE6345
    //update PARTS ORDER LINE from part delivery
    if(SR_Class_PartDelivery.mapUpdatePartsOrderLineFromPD.size() > 0)
    {
        update SR_Class_PartDelivery.mapUpdatePartsOrderLineFromPD.values();
        SR_Class_PartDelivery.mapUpdatePartsOrderLineFromPD = new Map<Id, SVMXC__RMA_Shipment_Line__c>();
    }

    //update WORK DETAIL from part delivery
    if(SR_Class_PartDelivery.mapUpdateWorkDetailFromPD.size() > 0)
    {
        update SR_Class_PartDelivery.mapUpdateWorkDetailFromPD.values();
        SR_Class_PartDelivery.mapUpdateWorkDetailFromPD = new Map<Id, SVMXC__Service_Order_Line__c>();
    }

    //update INSTALLED PRODUCT from part delivery
    if(SR_Class_PartDelivery.mapUpdateInstalledProductFromPD.size() > 0)
    {
        update SR_Class_PartDelivery.mapUpdateInstalledProductFromPD.values();
        SR_Class_PartDelivery.mapUpdateInstalledProductFromPD = new Map<Id, SVMXC__Installed_Product__c>();
    }

    //update PRODUCT STOCK from part delivery
    if(SR_Class_PartDelivery.mapUpdateProductStockFromPD.size() > 0)
    {
        SR_Class_PartDelivery.mapUpdateProductStockFromPD.values();
        SR_Class_PartDelivery.mapUpdateProductStockFromPD = new Map<Id, SVMXC__Product_Stock__c>();
    }

    //update STOCKED SERIAL from part delivery
    if(SR_Class_PartDelivery.mapUpdateProductSerialFromPD.size() > 0)
    {
        update SR_Class_PartDelivery.mapUpdateProductSerialFromPD.values();
        SR_Class_PartDelivery.mapUpdateProductSerialFromPD = new Map<Id, SVMXC__Product_Serial__c>();
    }
}
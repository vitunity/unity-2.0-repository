/***********************************************
Created By  :   Nikita Gupta
Created On  :   6/12/2014
Reason      :   US4386, to populate Business Hours on SLA Detail
last modified by : Bushra
Reason      : Code optimization
***********************************************/

trigger SR_SLADetail_BeforeTrigger on SVMXC__SLA_Detail__c (before insert, before update) 
{
    SR_ClassSLADetailUtillTrigger objSLadetail = new SR_ClassSLADetailUtillTrigger();
    objSLadetail.populateFieldsOnSlaDetail(Trigger.new, trigger.oldmap); //call to method to pre populate sla detail
}
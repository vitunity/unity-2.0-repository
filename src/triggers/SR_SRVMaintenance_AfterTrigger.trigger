trigger SR_SRVMaintenance_AfterTrigger on SVMXC__Service_Contract__c (after Insert,after update)  {

    SR_Class_ServiceContract objClass =  new SR_Class_ServiceContract(); //initiate class
    objClass.UpdateChildAndRelatedObject(Trigger.New, Trigger.oldMap); // call to a method to update other related objects
    objClass.createOpportunity(Trigger.New, Trigger.oldMap); // call to a method for Opportunity creation 
    if(trigger.isinsert){
        objClass.createSCP(Trigger.New); // call to a method for creation of service contract period on contract insert only
    }

    /* PM Plan update with Cancellation of Service Contract 
    if(Trigger.isUpdate){
        PMPPMIHandler.handle(Trigger.new,Trigger.oldMap);
    }    */
    
    //Logic added by Shital Bhujbal - Contract Uplift project 
    if(trigger.isinsert){
        ServiceContract_Autorenewal autoRenewalObj =  new ServiceContract_Autorenewal(); //initiate class
        autoRenewalObj.createOpptyAndQuote(Trigger.New); 
    }
}
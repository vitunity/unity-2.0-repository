/*************************************************************************\
    @ Author        : Chiranjeeb Dhar
    @ Date      :     04-Jan-2014
    @ Description   : This class is being called from triggers
                      related to SOI..
    @ Last Modified By  : Anupam Tripathi
    @ Last Modified On  : 21-Jan-2014
    @ Last Modified Reason  : Null pointer check
    @ Last Modified By  : Shubham Jaiswal
    @ Last Modified On  : 09-Apr-2014
    @ Last Modified Reason  : US4170 : Replaced Sales_Order_Item__c with ERP_Reference__c
    @ Last Modified By      : Nikita Gupta
    @ Last Modified On      : 6/25/2014
    @ Last Modified Reason  : US4654, added a method call to update Parent SOI's Product Model field based on SOI
    @ Last Modified By      : Nikita Gupta
    @ Last Modified On      : 6/27/2014
    @ Last Modified Reason  : US4319, replaced field SO Item Category with ERP Item Category
****************************************************************************/

trigger SR_SalesOrderItem_AfterTrigger on Sales_Order_Item__c (after Update,after insert) 
{
    private final String ERP_ITEM_CATEGORY_Z003 = 'Z003';
    private final String ERP_ITEM_CATEGORY_Z001 = 'Z001';

    SR_Class_SalesOrderItem objSOI = new SR_Class_SalesOrderItem(); //all class instantiation replaced with one
    Set<Sales_Order_Item__c> setSOI = new Set<Sales_Order_Item__c>(); //US4654
    Set<ID> setProductID = new Set<ID>(); //US4654
    Set<String> SalesOrderItem = new Set<String>();
    Map<String,SVMXC__Service_Order__c> MapofWO = new Map<String,SVMXC__Service_Order__c>();
    List<SVMXC__Service_Order__c> ListofWOUpdate = new List<SVMXC__Service_Order__c>();
    List<Sales_Order_Item__c> updatedSOIProdModel = new List<Sales_Order_Item__c>(); // Shubham US4326
    List<Sales_Order_Item__c> insertedSOIProdModel = new List<Sales_Order_Item__c>(); // Shubham US4326
    List<Sales_Order_Item__c> setOrResetProductModel = new List<Sales_Order_Item__c>();    
    Set<Id> setSOIId = new Set<Id>();

    Set<Id> cancelledItemIdList = new Set<Id>();  //JW/FF 6-23-15 Var-36 US5121 TA6797 
    Map<Id, String> soiIdToNameMap = new Map<Id, String>(); //savon.FF 10-13-2015 US5121 DE6287
  
    for(Sales_Order_Item__c SOI : Trigger.new)
    {
        //Addded by Nikita, US4654, 6/25/2014
        if(SOI.Parent_Item__c != null && ((trigger.isInsert && SOI.Product__c != null) || (trigger.isUpdate && SOI.Product__c != null && SOI.Product__c != trigger.oldmap.get(SOI.ID).Product__c)))
        {
            setSOI.add(SOI);
            setProductID.add(SOI.Product__c);
        }
        // for update only 
        if(trigger.isupdate && Trigger.oldMap.get(SOI.Id).Location__c!=Trigger.newMap.get(SOI.Id).Location__c)
        {
            if(SOI.ERP_Reference__c != null)
            {
                SalesOrderItem.add(SOI.ERP_Reference__c);
            }
        } // Shubham US4326 starts
        if(trigger.isInsert && SOI.Product_Model__c!=null || (trigger.isupdate &&  Trigger.oldMap.get(SOI.Id).Product_Model__c!=Trigger.newMap.get(SOI.Id).Product_Model__c))
        {
            setOrResetProductModel.add(SOI);   
            setSOIId.add(SOI.id);         
        }
        if(trigger.isupdate && Trigger.oldMap.get(SOI.Id).Product_Model__c != Trigger.newMap.get(SOI.Id).Product_Model__c)
        {
            if(SOI.Product_Model__c != NULL && SOI.ERP_Item_Category__c == ERP_ITEM_CATEGORY_Z003 && SOI.Parent_Item__c != NULL)
            {
                updatedSOIProdModel.add(SOI);
            }
        } // Shubham US4326 ends
        
        // for insert only
        if(trigger.isinsert && SOI.Product_Model__c != NULL && SOI.ERP_Item_Category__c == ERP_ITEM_CATEGORY_Z003 && SOI.Parent_Item__c != NULL)
        {
            insertedSOIProdModel.add(SOI);  
        }

        //savon.FF 10-9-2015 Changed Reject Reason if statement to v2-Clerical Cancelled from just Cancelled DE6214 .. //savon/FF 9-2-2015 DE5674 - Combined list 10-12-15
        //DE7905 : CM : Change reject reason to 'V2-Clerical Cancellation'
        if(Trigger.isUpdate && SOI.Reject_Reason__c == 'V2-Clerical Cancellation' && SOI.Reject_Reason__c != Trigger.oldMap.get(SOI.Id).Reject_Reason__c) //JW/FF 6-23-15 Var-36 US5121 TA6797 //SOI.Reject_Reason__c == 'v2-Clerical Cancelled'
        {
            cancelledItemIdList.add(SOI.Id);
            soiIdToNameMap.put(SOI.Id, SOI.Name); //savon.FF 10-13-2015 US5121 DE6287
            
        }
    }

    if(setOrResetProductModel.size()>0)
    { 
        //objSOI.createAndUpdateInstalledProduct(setOrResetProductModel,setSOIId);
    }
    if(setSOI.size() > 0 && setProductID.size() > 0)
    {
        //objSOI.updateParentProductModel(setSOI, setProductID); //US4654, method call, Nikita, 6/25/2014
    }
    //Shubham US4319 ends
    objSOI.updateWorkOrderAndDetailandParent(trigger.new, trigger.oldmap); // this method is also for canceling counterdetails
        
    if(updatedSOIProdModel.size() > 0)
    {
        objSOI.UpdateZ001SOItemProductModel(updatedSOIProdModel);
    } // Shubham US4326 ends

    if(SalesOrderItem.size() > 0)
    {
        /*List<SVMXC__Service_Order__c> ListofWO =[Select Id,SVMXC__Site__c,ERP_Sales_Order_Item__c from SVMXC__Service_Order__c 
                                                                                where ERP_Sales_Order_Item__c in:SalesOrderItem];*/
        /* DE7187 : Decommissioned For Future Deletion*/
        List<SVMXC__Service_Order__c> ListofWO = new List<SVMXC__Service_Order__c>();
        if(ListofWO.size() > 0)
        {
            for(SVMXC__Service_Order__c WO: ListofWO )
            {
                MapofWO.put(WO.ERP_Sales_Order_Item__c, WO);
            }
        }
        for(Sales_Order_Item__c SOI : Trigger.new)
        {
            if(Trigger.oldMap.get(SOI.Id).Location__c != Trigger.newMap.get(SOI.Id).Location__c)
            {
                SVMXC__Service_Order__c WO = new SVMXC__Service_Order__c(); //WO = MapofWO.get(SOI.Sales_Order_Item__c); // code replaced for US4170 with
                if(SOI.ERP_Reference__c != null && MapofWO.containsKey(SOI.ERP_Reference__c))
                {
                    WO = MapofWO.get(SOI.ERP_Reference__c);
                    if(WO != Null)
                    {
                        WO.SVMXC__Site__c=SOI.Location__c;
                        ListofWOUpdate.add(WO);
                    }
                }
            }
        }
        if(ListofWOUpdate.size()>0)
        {
            system.debug('$$$$$$'+ListofWOUpdate);
            Update ListofWOUpdate;
        }
        /* DE7187 : Decommissioned For Future Deletion*/
    }
    if(trigger.ISUpdate)
    {
        objSOI.updateCounterDetails (trigger.oldMap,trigger.newMap);
        objSOI.cancelCounterDetail(trigger.new,trigger.oldmap);
    }
    if(trigger.IsInsert || trigger.IsUpdate)//DE7639 : CM : || trigger.IsUpdate 
    {
        objSOI.createCounterDetails(trigger.new); //Added By Mohit DE3059,Counter Detail Creation //Harshita - Please verify, I think this should be called on insert only, but not sure
    }
    if(insertedSOIProdModel.size() > 0)
    {
        objSOI.UpdateZ001SOItemProductModel(insertedSOIProdModel);
    } 
    if(!cancelledItemIdList.isEmpty())  //JW/FF 6-23-15 Var-36 US5121 TA6797 ... second method added savon.FF 9-2-15, combined here 10-12-15
    {
        //savon.FF 10-12-15 - if their are IWO's don't cancel anything
        Map<Id, Id> salesToCaseMap = new Map<Id, Id>();
        Map<String, List<SVMXC__Service_Order__c>> soiToIWOMap = new Map<String, List<SVMXC__Service_Order__c>>(); //savon.FF 10-13-2015 US5121 DE6287
 
        List<SVMXC__Case_Line__c> checkIWO = [SELECT Id, SVMXC__Case__c, Sales_Order_Item__c FROM SVMXC__Case_Line__c WHERE Sales_Order_Item__c IN :cancelledItemIdList];
        Set<Id> caseCheckIWOIds = new Set<Id>();
        for (SVMXC__Case_Line__c cl : checkIWO) {
            salesToCaseMap.put(cl.SVMXC__Case__c, cl.Sales_Order_Item__c);
            caseCheckIWOIds.add(cl.SVMXC__Case__c);
        }
        
        List<Case> caseCheckIWO = [SELECT Id, (SELECT Id, Service_Super_Region__c, Event__c, Name FROM SVMXC__Service_Order__r) FROM Case WHERE Id IN :caseCheckIWOIds];
         
        for (Case c: caseCheckIWO) {
            for (SVMXC__Service_Order__c wo : c.SVMXC__Service_Order__r) {
                if (wo.Event__c == false) {
                    Id soiId = salesToCaseMap.get(c.Id);
                    cancelledItemIdList.remove(soiId);
                    soiToIWOMap.put(soiIdToNameMap.get(soiId), c.SVMXC__Service_Order__r);                    
                }
            }
        }
        System.Debug('***CM cancelledItemIdList: '+cancelledItemIdList);
        if (!cancelledItemIdList.isEmpty()) {
            SalesOrderAndOrderItemHelper.cancelledSalesOrderOrLine(cancelledItemIdList, null);
            System.Debug('***CM cancelledItemIdList: '+cancelledItemIdList);
            SalesOrderAndOrderItemHelper.cancelRelatedItems(cancelledItemIdList);  
        } else {
            //savon.FF 10-13-15 US5121 DE6287
            SalesOrderAndOrderItemHelper.sendEmailToFOA(soiToIWOMap); 
        }
        
    }
    
    if (trigger.isUpdate) { //pszagdaj/FF 09/10/2015 DE6204
        SalesOrderAndOrderItemHelper.deleteOrderedInstalledProducts(trigger.oldMap, trigger.newMap);
    }
}
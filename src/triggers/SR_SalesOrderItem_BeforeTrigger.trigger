/*************************************************************************\
      @ Author                : Chiranjeeb Dhar
      @ Date                  : 30/12/2013
      @ Description           : This trigger populates Product and Sales order for US2484. 
      @ Last Modified By      : Shubham Jaiswal
      @ Last Modified On      : 20-Mar-2014    
      @ Last Modified Reason  : 20-Feb-2014 Updated for US3807  to fix the defect : Priti Jaiswal
                                20-Mar-2014 Updated for US4325  : Shubham Jaiswal
      @ Last Modified On      : 5-May-2014 updated for error 
                                "SR_SalesOrderItem_BeforeTrigger: execution of BeforeInsert
                                caused by: System.NullPointerException: Attempt to de-reference a null object
                                Trigger.SR_SalesOrderItem_BeforeTrigger: line 206, column 1"
      @ Last Modified By      : Shubham Jaiswal
      @ Last Modified On      : 19-May-2014    
      @ Last Modified Reason  : US3554 : Code for Item_Hierachy_Key__c assignment commented 
      @ Last Modified By      : Nikita Gupta
      @ Last Modified On      : 6-25-2014    
      @ Last Modified Reason  : code for US4325 commented as requirement changed for US4654
      @ Last Modified By      : Parul Gupta
      @ Last Modified On      : 8/25/2014
      @ Last Modified Reason  : DE1079, Update ERP Partner lookups for interface
****************************************************************************/

trigger SR_SalesOrderItem_BeforeTrigger on Sales_Order_Item__c(before insert,before update) 
{
    private final String ERP_ITEM_CATEGORY_Z003 = 'Z003';
    private final String ERP_ITEM_CATEGORY_Z001 = 'Z001';
    SR_Class_SalesOrderItem clsSOI = new SR_Class_SalesOrderItem();     //Class call
    clsSOI.updateSalesOrderItem(trigger.new, trigger.oldMap);   //method call for DE1079
}
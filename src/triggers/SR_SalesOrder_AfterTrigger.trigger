/*************************************************************************\
      @ Author          : Chiranjeeb Dhar
      @ Date            : 30-Jan-2014
      @ Description     : This Trigger will be used to Update IP US3853.
      @ Last Modified By      :     
      @ Last Modified On      :     
      @ Last Modified Reason  :     

****************************************************************************/

trigger SR_SalesOrder_AfterTrigger on Sales_Order__c (after insert, after Update) 
{
 
    SR_classSalesOrder obj = new SR_classSalesOrder();
    obj.setSalesOrderOnQuote(trigger.new);//added by kaushiki US2774 & 2776
     
	if(trigger.isUpdate){
	    Set<Id> cancelledOrderIdSet = new Set<Id>();
	     
	    for(Sales_Order__c so: Trigger.new){
	    	//JW/FF 6-23-15 Var-36 US5121 TA6797 
	    	if(Trigger.isUpdate && so.Block__c == '70-Order Cancellation' && so.Block__c != Trigger.oldMap.get(so.Id).Block__c){
	    		cancelledOrderIdSet.add(so.Id);
	    	}
	    }
		//JW/FF 6-23-15 Var-36 US5121 TA6797 
	    if(!cancelledOrderIdSet.isEmpty()){
	    	SalesOrderAndOrderItemHelper.cancelledSalesOrderOrLine(null, cancelledOrderIdSet);
	    }
	}
}
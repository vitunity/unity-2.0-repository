/*************************************************************************\
        @ Author                : Priti Jaiwal
        @ Date                  : 19-Feb-2014
        @ Description           : Inlcude logic on isert/update of Sales Order.
        @ Last Modified By      :     
        @ Last Modified On      :     
        @ Last Modified Reason  :     

****************************************************************************/

trigger SR_SalesOrder_BeforeTrigger on Sales_Order__c (before insert, before update) 
{
    
    Set<String> quoteIds = new Set<String>();
   
   for(Sales_Order__c S : Trigger.new)
   {
       quoteIds.add(S.Ext_Quote_Number__c);
   }
    
   List<BigMachines__Quote__c> quoteList = [Select Id, Name from BigMachines__Quote__c where Name in: quoteIds];
   
   for(Sales_Order__c S : Trigger.new)
   {
       for(BigMachines__Quote__c Q : quoteList)
       {
           if(S.Ext_Quote_Number__c == Q.Name)
           {
               S.Quote__c = Q.Id;
               break;
           }
       }
   }
    
    
    List<Sales_Order__c> relatedsalesOrder = new List<Sales_Order__c>();
    for(Sales_Order__c so : Trigger.New)
    {
        //priti, US4030, 19/02/2014 - To update Sold To and Quote on Sales Order
        if(so.ERP_Sold_To__c != null || so.Ext_Quote_Number__c != null || so.ERP_Site_Partner__c != null)// DE1804,11/21/2014,Shubham - To update ERP Partner  
        {
            relatedsalesOrder.add(so);
            
        }
    }
    if(relatedsalesOrder != null && relatedsalesOrder.size() > 0)
    {
        SR_classSalesOrder obj = new SR_classSalesOrder();
        obj.updateSO(relatedsalesOrder, trigger.oldMap);
    }
}
/***************************

    @Last Modified By    :    Nikita Gupta
    @Last Modified On    :    4/28/2014
    @Last Modified Reason:    Renamed Trigger from :‘SR_ServiceContract_Populatefields’ to: ‘SR_ServiceContract_BeforeTrigger’
    @Last Modified By    :    Nikita Gupta
    @Last Modified On    :    4/28/2014
    @Last Modified Reason:    US4473, To populate Account field Based on ERP Sold To, and Site Partner based on ERP Site Partner.
    @Last Modified By:    Nikita Gupta
    @Last Modified On:    4/29/2014
    @Last Modified Reason:    US4473, to remove the Site Partner functionality from above written code.
    @Last Modified By:    Bushra
    @Last Modified On:    12/05/2014
    @Last Modified Reason:    code optimisation
    @Last Modified By:    Bushra
    @Last Modified On:    1/08/2015
    @Last Modified Reason:    method commented and moved to After trigger
***************************/

trigger SR_ServiceContract_BeforeTrigger on SVMXC__Service_Contract__c (before insert,before update) 
{
    
    SR_Class_ServiceContract objSVC = new SR_Class_ServiceContract();
        objSVC.populateFieldsOnContract(trigger.new,trigger.oldmap); // call to a method for fields updation
    
    /*if(Trigger.isUpdate) // commented by bushra on jan-8-2015 and moved to After trigger
    {
       // SR_Class_ServiceMaintenanceContract objSRVC=new   SR_Class_ServiceMaintenanceContract();
        objSVC.updateRelatedObjFromContract(trigger.new,trigger.oldmap); // call to a method for updation of related object's field
     }*/
}
/*---------------------------------------------------------------------
- Author        : Shubham Jaiswal
- Functionality : To create events for Technician when assigned Work Order from Dispatch Console
- User Story    : US4486  
- Creation Date  : 14-May-2014
- Last Modified By  :   Nikita Gupta
- Last Modified On  :   6/13/2014
- Last Modified Reason: add null check to the code
- Last Modified By  :   Nikita Gupta
- Last Modified On  :   8/28/2014
- Last Modified Reason: changed static method call to standard
- Last Modified By  :   Nikita Gupta
- Last Modified On  :   9/5/2014
- Last Modified Reason: US4901, added after update functionality

WP; In Progress; US5165; Nikita Gupta/Wipro; 20150625 
Change Log:
//Oct-11-2017 : Chandramouli : STSK0013132 : Updated the Query to fetch the user only impacted of Deleted events.
------------------------------------------------------------------------*/

trigger SR_ServiceMaxEventAfterTrigger on SVMXC__SVMX_Event__c (After insert, after update, after delete) //
{
    Set<String> TechSet = new Set<String>() ;
    List<SVMXC__Service_Group_Members__c> Techlist = new List<SVMXC__Service_Group_Members__c>();
    List<SVMXC__SVMX_Event__c> SVMXEvent = new List<SVMXC__SVMX_Event__c> ();
    Map<Id,Id> TechId2UserMap = new Map<Id,Id>();
    Set<id> WorkOrderids = new Set<id>();
    Set<ID> setSVMXCEventID = new Set<ID>();
    set<Id> deletedsvmxevents = new set<Id>();
    List<SVMXC__SVMX_Event__c> deletedEvent = new List<SVMXC__SVMX_Event__c> ();
    SR_ClassSVMXEvent objSVMXEvent = new SR_ClassSVMXEvent(); //initiating class
    SR_TechnicianAssignmentclass objTechAssign = new SR_TechnicianAssignmentclass();// added by megha de3300
    //Oct-11-2017 : Chandramouli : STSK0013132 : Updated the Query to fetch the user only impacted of Deleted events.
    Set<Id> deletedEventUserIds = new set<id>();
    Map<Id, User> mapid2user = new Map<Id, User>();
    Map<Id, Id> mapeventid2deletedby = new Map<Id, id>();

    if(trigger.isDelete)
    {
        for(SVMXC__SVMX_Event__c eventSVMX : trigger.old)
        {
            deletedsvmxevents.add(eventSVMX.id);
            if(eventSVMX.SVMXC__Technician__c != null)
            {
                TechSet.add(eventSVMX.SVMXC__Technician__c );
                SVMXEvent.add(eventSVMX);
                    deletedEvent.add(eventSVMX);   //US5165
            }

            System.debug('LastModifiedBy : ' + eventSVMX.LastModifiedById);
            //Oct-11-2017 : Chandramouli : STSK0013132 : Updated the Query to fetch the user only impacted of Deleted events.
            deletedEventUserIds.add(eventSVMX.OwnerId);
            deletedEventUserIds.add(eventSVMX.LastModifiedById);
            WorkOrderids.add(eventSVMX.SVMXC__Service_Order__c); //US5165

        }

        // DFCT0012403 - Dec 7, 2016, Yogesh
        //Oct-11-2017 : Chandramouli : STSK0013132 : Updated the Query to fetch the user only impacted of Deleted events.
        if ( !deletedEventUserIds.isEmpty() )
        {
            for(User userRecord : [Select Id, Name, Email From User where id in :deletedEventUserIds])
            {
                mapid2user.put(userRecord.id, userRecord);
            }
        }

        if(deletedsvmxevents.size() > 0)
        {
            for (SVMXC__SVMX_Event__c svmxevnt : [select id, LastModifiedById from SVMXC__SVMX_Event__c where id in :deletedsvmxevents ALL ROWS])
            {
                mapeventid2deletedby.put(svmxevnt.id, svmxevnt.LastModifiedById);
            }
        }

        objTechAssign.deleteTechnicianAssignment(trigger.oldMap); //DE3644
    }
    if(trigger.isInsert)
    {
        objTechAssign.insertTechnicianAssignment(trigger.new);// added by megha de3300
    }
    if(trigger.isUpdate)
    {
        objTechAssign.updateTechnicianAssign(trigger.new, trigger.oldMap);  //DE3644
        for(SVMXC__SVMX_Event__c eventSVMX : trigger.old)
        {
            if(eventSVMX.SVMXC__Technician__c != null && trigger.newMap.get(eventSVMX.ID).SVMXC__Technician__c != eventSVMX.SVMXC__Technician__c)
            {
                deletedEvent.add(eventSVMX);   
            }
        }        
    }

    if(trigger.IsInsert || trigger.isUpdate)
    {
        for(SVMXC__SVMX_Event__c eventSVMX : trigger.new)
        {
              //CM : DFCT0012104 : added for updating the salesforce event.                      //Add booking type and description as conditions for update event 11/10/2017
              if(eventSVMX.SVMXC__Technician__c != null && (trigger.oldmap == null || (trigger.oldmap != null && 
              (trigger.oldmap.get(eventSVMX.ID).SVMXC__Technician__c != eventSVMX.SVMXC__Technician__c || trigger.oldmap.get(eventSVMX.ID).Name != eventSVMX.Name ||
              trigger.oldmap.get(eventSVMX.ID).SVMXC__WhatId__c != eventSVMX.SVMXC__WhatId__c || 
              (trigger.oldmap.get(eventSVMX.ID).SVMXC__StartDateTime__c != eventSVMX.SVMXC__StartDateTime__c || trigger.oldmap.get(eventSVMX.ID).SVMXC__StartDateTime__c == null)|| (trigger.oldmap.get(eventSVMX.ID).SVMXC__Description__c != eventSVMX.SVMXC__Description__c || trigger.oldmap.get(eventSVMX.ID).SVMXC__Description__c == null) ||(trigger.oldmap.get(eventSVMX.ID).Name != eventSVMX.Name || trigger.oldmap.get(eventSVMX.ID).Name == null)||
              (trigger.oldmap.get(eventSVMX.ID).Booking_Type__c != eventSVMX.Booking_Type__c || trigger.oldmap.get(eventSVMX.ID).Booking_Type__c == null) ||
              (trigger.oldmap.get(eventSVMX.ID).SVMXC__EndDateTime__c != eventSVMX.SVMXC__EndDateTime__c || trigger.oldmap.get(eventSVMX.ID).SVMXC__EndDateTime__c == null)))) )
            {
                TechSet.add(eventSVMX.SVMXC__Technician__c );
                SVMXEvent.add(eventSVMX);
                
            }
            if(trigger.oldmap != null && eventSVMX.Status__c == 'Cancelled' && trigger.oldmap.get(eventSVMX.Id).Status__c != 'Cancelled')
                {
                    setSVMXCEventID.add(eventSVMX.ID);
                }
             if(eventSVMX.SVMXC__Technician__c != null && eventSVMX.SVMXC__Service_Order__c != null 
                && (trigger.oldmap == null || (trigger.oldmap != null && 
              (trigger.oldmap.get(eventSVMX.ID).SVMXC__Technician__c != eventSVMX.SVMXC__Technician__c || 
              trigger.oldmap.get(eventSVMX.ID).SVMXC__WhatId__c != eventSVMX.SVMXC__WhatId__c ||
              trigger.oldmap.get(eventSVMX.ID).SVMXC__StartDateTime__c != eventSVMX.SVMXC__StartDateTime__c))))
               {                               
                    WorkOrderids.add(eventSVMX.SVMXC__Service_Order__c);                                
               }
            
            if(trigger.oldmap != null && eventSVMX.Status__c == 'Cancelled' && trigger.oldmap.get(eventSVMX.Id).Status__c != 'Cancelled')
            {
                setSVMXCEventID.add(eventSVMX.ID);
            }
        }

        if(setSVMXCEventID.size() > 0)
        {
            objSVMXEvent.cancelSalesforceEvent(setSVMXCEventID);   //method call to update Sales Force event from SVMXE event
        } 
    }  
    for(SVMXC__SVMX_Event__c varEvent : deletedEvent)
        {
            if(String.isNotBlank(varEvent.Technician_s_Email__c))
            {
                String LastModifiedByName = '';
                String ownerEmail = '';

                //// DFCT0012403 - Dec 7, 2016

                if (mapid2user.size() > 0 && mapeventid2deletedby.size()>0 && mapeventid2deletedby.containskey(varEvent.id) && mapid2user.containsKey(mapeventid2deletedby.get(varEvent.id)) && mapid2user.get(mapeventid2deletedby.get(varEvent.id)) != null)
                {
                    LastModifiedByName = mapid2user.get(mapeventid2deletedby.get(varEvent.id)).Name;
                }

                String body = '';
                String bookType = String.isBlank(varEvent.Booking_Type__c) ? '' : varEvent.Booking_Type__c;
                body += 'Hi ' + varEvent.Technician_Name__c + ',' + '\n';
                body += ' ' + '\n';
                body += 'A Event or Work Order has been deleted from your calendar on your behalf. ' + '\n';
                body += ' ' + '\n';
                body += 'Event Start Date: ' + varEvent.SVMXC__StartDateTime__c + '\n';      
                body += 'Event End Date: ' + varEvent.SVMXC__EndDateTime__c + '\n';                   
                body += 'Event Subject: ' + varEvent.Name;     
                body += '\nBooking Type: ' + bookType;
                body += '\nDeleted By: ' + LastModifiedByName + '\n';  
                body += ' ' + '\n';
                body += ' ' + '\n';               
                body += 'Thanks,' + '\n';  
                body += 'Varian Team';  
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

                // DFCT0012403 - Dec 7, 2016

                if (mapid2user.size() > 0 && mapid2user.containsKey(varEvent.OwnerId))
                {
                    ownerEmail = mapid2user.get(varEvent.OwnerId).Email;
                }
                
                // DFCT0012403 - Dec 7, 2016
                if(ownerEmail != '' && ownerEmail != varEvent.Technician_s_Email__c)
                    email.setToAddresses(new String[]{varEvent.Technician_s_Email__c, ownerEmail});  
                else
                    email.setToAddresses(new String[]{varEvent.Technician_s_Email__c});

                email.setSubject('Event has been deleted');
                for(OrgWideEmailAddress owa : [select id, Address, DisplayName from OrgWideEmailAddress where DisplayName = 'Varian Support']) 
                {
                    if(owa.Address.contains(system.label.SR_FromAddressDoNotReply)) 
                    email.setOrgWideEmailAddressId(owa.id); 
                } 
                email.setPlainTextBody(body);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            } 
        } 
    
        if(TechSet.size() > 0) //null check added by Nikita, 6/13/2014
        {
            Techlist =  [SELECT id,User__c from SVMXC__Service_Group_Members__c where id in : TechSet AND User__c != null];
            System.debug('-----Techlist' + Techlist);
            
            if(Techlist.size() > 0)
            {
                for(SVMXC__Service_Group_Members__c tech : Techlist)
                {
                    TechId2UserMap.put(tech.id,tech.User__c);            
                }
                    
                if(TechId2UserMap.size() > 0 && SVMXEvent.size() > 0)
                {
                    objSVMXEvent.CreateTechnicianEvent(TechId2UserMap, SVMXEvent, trigger.newMap, trigger.oldmap); //method call
                }
            }
        
        }
        if(setSVMXCEventID.size() > 0)
        {
            objSVMXEvent.cancelSalesforceEvent(setSVMXCEventID);   //method call to update Sales Force event from SVMXE event
        }

        if(WorkOrderids.size() > 0)
        {
            objSVMXEvent.updatedWorkOrder(WorkOrderids, trigger.newmap, trigger.oldmap);
        } 
    
}
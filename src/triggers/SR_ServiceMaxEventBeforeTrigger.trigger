/***************************************************************
Created             :   Kaushiki Verma US4036 changed from (US4032 )26/8/14
Reason              :   populating location and subject field of serviceMax event
Last Modified By    :   Nikita Gupta (US3620), 9/10/2014 To updtae Duration on Evevnts based on WO Record Type.
                        Code optimization
Last Modified By    :   Kaushiki for changing logic from work order to whatId

WP; In Progress; US5165; Nikita Gupta/Wipro; 20150625 
***************************************************************/

trigger SR_ServiceMaxEventBeforeTrigger on SVMXC__SVMX_Event__c (before insert, before update, before delete) 
{
    SR_ClassSVMXEvent objSVMXCEvent = new SR_ClassSVMXEvent();
    
    if(Trigger.IsInsert || Trigger.IsUpdate)
        objSVMXCEvent.updateSVMXCEvent(trigger.new, trigger.oldmap);
        
    if(Trigger.Isdelete)
        objSVMXCEvent.onDeleteAction(trigger.old); //US5165
    
    if(Trigger.IsInsert || Trigger.IsUpdate)
        objSVMXCEvent.setDescription(Trigger.new);    
     
}
trigger SR_ServiceSurveyResults_AfterTrigger on Service_Survey_Results__c (after Insert,after update) {
set<Id> SSidSet = new set<Id>();
//class SR_Class_ServiceSurvey cls = new class SR_Class_ServiceSurvey();
    if(trigger.isInsert)
    {
        for(Service_Survey_Results__c varSS : trigger.new)
        {
            if(varSS.Write_in_comments__c!=null)
            {
              SSidSet.add(varSS.id);  
            }
        }
        if(SSidSet != null)
         SR_Class_ServiceSurvey.TranslatetheComments(SSidSet);
    }
    
}
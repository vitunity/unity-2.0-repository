/********kaushiki US4130 for getting Manager_Assigned_to_Survey_Action_Item__c and Case_Owner_Manager__c  and to get comment of latest approval if rejected in Approval_Rejection_Reason__c *****/
trigger SR_ServiceSurvey_BeforeTrigger on Service_Survey_Results__c (before update,before Insert) {
set<id> CurrentOwnerIdSet = new set<id>();
set<id> OwnerIdSet = new set<id>();
Map<Id, Service_Survey_Results__c > rejectedStatements = new Map<Id, Service_Survey_Results__c>{};
set<id> caseIdset = new set<id>();
set<id> caseIdsetWhenSurveyIsDissatisfied = new set<id>();
 
 
 
list<Service_Survey_Results__c> ServiceList = new list<Service_Survey_Results__c>();
if(trigger.IsUpdate)
{
    for(Service_Survey_Results__c varSurvey : trigger.new)
    {
        CurrentOwnerIdSet.add(varSurvey.ownerId);
        if(trigger.oldMap.get(varSurvey.id).OwnerID != null && varSurvey.OwnerID!=null && trigger.oldMap.get(varSurvey.id).OwnerID != varSurvey.OwnerID )
        {
           OwnerIdSet.add(trigger.oldMap.get(varSurvey.id).OwnerID);
        }
    
    }
    Map <Id,User> MapCurrentUser = new Map<id,User>([Select id,ManagerId,Manager.Email from User where id in :CurrentOwnerIdSet]); 
     map<id,User> mapUser = new map<id,User>([select id,name,Email from user where id in :OwnerIdSet]); 
     
     for(Service_Survey_Results__c varSurvey : trigger.new)
    {
        if (Trigger.oldMap.get(varSurvey.Id).Approval_Status__c!= 'Requires Further Action' && varSurvey.Approval_Status__c == 'Requires Further Action')
        { 
          rejectedStatements.put(varSurvey.Id, varSurvey);  
        }
     
        if(MapCurrentUser.containsKey(varSurvey.OwnerId))
        {
            if(MapCurrentUser.get(varSurvey.OwnerId).Manager.Email!=null)
            {
                varSurvey.Case_Owner_Manager__c = MapCurrentUser.get(varSurvey.OwnerId).Manager.Email;
            }
        
        }
        if(mapUser.containsKey(trigger.oldMap.get(varSurvey.id).OwnerID))
        {
            if(mapUser.get(trigger.oldMap.get(varSurvey.id).OwnerID)!=null && mapUser.get(trigger.oldMap.get(varSurvey.id).OwnerID).Email!=null)
            {
                varSurvey.Manager_Assigned_to_Survey_Action_Item__c = mapUser.get(trigger.oldMap.get(varSurvey.id).OwnerID).Email;
            }
        }  
    }
 if (!rejectedStatements.isEmpty())  
  {
    
    // get the most recent process instance 
    map<Id,Service_Survey_Results__c> processInstanceIds = new map<Id,Service_Survey_Results__c>{};
    
    for (Service_Survey_Results__c SeriviceSurvey : [SELECT (SELECT ID
                                              FROM ProcessInstances
                                              ORDER BY CreatedDate DESC
                                              LIMIT 1)
                                      FROM Service_Survey_Results__c
                                      WHERE ID IN :rejectedStatements.keySet()])
    {
        processInstanceIds.put(SeriviceSurvey.ProcessInstances[0].Id,SeriviceSurvey);
    }
      
    
    // the most recent process steps for comments.  
    for (ProcessInstance pi : [SELECT TargetObjectId,
                                   (SELECT Id, StepStatus, Comments 
                                    FROM Steps
                                    ORDER BY CreatedDate DESC
                                    LIMIT 1 )
                               FROM ProcessInstance
                               WHERE Id IN :processInstanceIds.keySet()
                               ORDER BY CreatedDate DESC])   
    {                   
      if ((pi.Steps[0].Comments == null || 
           pi.Steps[0].Comments.trim().length() == 0))
      {
        rejectedStatements.get(pi.TargetObjectId).addError(
          ' Please provide a rejection reason!');
      }
      else
      {
          Service_Survey_Results__c Service = new Service_Survey_Results__c(id = processInstanceIds.get(pi.id).id);
          system.debug('check Service--->'+Service+'---->'+Service.id);
          trigger.newmap.get(processInstanceIds.get(pi.id).id).Approval_Rejection_Reason__c = pi.Steps[0].Comments;
          system.debug('check Service.Approval_Rejection_Reason__c--->'+Service.Approval_Rejection_Reason__c+'pi.Steps[0].Comments---->'+pi.Steps[0].Comments);
      
      }
    }  
  }
}
  if(trigger.IsInsert)
    {
        for(Service_Survey_Results__c varSer : trigger.new)
        {
            if(varSer.Case__c!=null)
            {
                caseIdset.add(varSer.Case__c);
            }
             
            system.debug('sat---->'+varSer.Overall_Sat__c);
            if(varSer.Overall_Sat__c.contains('Dissatisfied'))
            {
               caseIdsetWhenSurveyIsDissatisfied.add(varSer.Case__c);
               ServiceList.add(varSer);
            }
        }
        map<id,case> caseMapForAllClosedByManagers = new map<id,case>([select id,OwnerId,ClosedBy__r.Email,ClosedBy__c,ClosedBy__r.ManagerId,ClosedBy__r.Manager.Email,ClosedBy__r.Manager.Manager.Email,ClosedBy__r.Manager.Manager.Manager.Email from case where id in :caseIdset]);
       
           map<id,case> caseMap = new map<id,case>( [select id,ClosedBy__c,ClosedBy__r.ManagerId from case where id in :caseIdsetWhenSurveyIsDissatisfied ]);
       
        system.debug('caseMap ---->'+caseMap );
        for(Service_Survey_Results__c varSer : trigger.new)
        {
        
        if(caseMapForAllClosedByManagers.containsKey(varSer.Case__c))
            {
                system.debug('caseMapForAllClosedByManagers.get(varSer.Case__c).Ownerid----'+caseMapForAllClosedByManagers.get(varSer.Case__c).Ownerid);
                varSer.User__c = caseMapForAllClosedByManagers.get(varSer.Case__c).OwnerId;
                system.debug('varSer.Case_Owner__c----'+varSer.User__c);
            }
            if(caseMap.containsKey(varSer.Case__c))
            {
            system.debug('contains  ---->'+caseMap );
                if(caseMap.get(varSer.Case__c).ClosedBy__r.ManagerId!=null)
                {
                system.debug('caseMap.get(varSer.Case__c).ClosedBy__r.ManagerId ---->'+caseMap.get(varSer.Case__c).ClosedBy__r.ManagerId);
                    varSer.OwnerId = caseMap.get(varSer.Case__c).ClosedBy__r.ManagerId;
                    system.debug(' varSer.OwnerId ---->'+ varSer.OwnerId +'magr id --->'+caseMap.get(varSer.Case__c).ClosedBy__r.ManagerId );
                }
            }
            if(caseMapForAllClosedByManagers.containsKey(varSer.Case__c))
            {
                if(caseMapForAllClosedByManagers.get(varSer.Case__c).ClosedBy__r.Email!=null)
                {
                    varSer.Action_Item_Completed_By__c = caseMapForAllClosedByManagers.get(varSer.Case__c).ClosedBy__r.Email;
                }    
                if(caseMapForAllClosedByManagers.get(varSer.Case__c).ClosedBy__r.Manager.Email!=null)
                {
                    varSer.Case_Owner_Manager__c = caseMapForAllClosedByManagers.get(varSer.Case__c).ClosedBy__r.Manager.Email;
                }
                if(caseMapForAllClosedByManagers.get(varSer.Case__c).ClosedBy__r.Manager.Manager.Email!=null)
                {
                    varSer.Case_Owner_2nd_Level_Manager__c = caseMapForAllClosedByManagers.get(varSer.Case__c).ClosedBy__r.Manager.Manager.Email;
                }
                if(caseMapForAllClosedByManagers.get(varSer.Case__c).ClosedBy__r.Manager.Manager.Manager.Email!=null)
                {
                    varSer.X3rd_Level_Manager__c = caseMapForAllClosedByManagers.get(varSer.Case__c).ClosedBy__r.Manager.Manager.Manager.Email;
                }
            }
        }
    // kaushiki 26/08/14 US4134 update owner with case closedBy manager id sat is dissatisfied end........
    }
}
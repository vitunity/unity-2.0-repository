/*********
Nikita Gupta
2/2/2015
DE3371

Puneet Mishra 27 July 2017
STSK0012095 : Update Account's Service District Manager and Service Regional Manager when Service Team's DM is updated.

*********/
trigger SR_ServiceTeam_AfterTrigger on SVMXC__Service_Group__c (after insert, after update)
{
    /******************* RECORD TYPE ID OF EQUIPMENT ***************************/
    Id recTypeIdSTEquipment = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Equipment').getRecordTypeId();

    Set<string> setGroupCode = new Set<string>();                                                       //DE3620, set of group code + '-E'
    Set<string> setOldDispatchQueue = new Set<string>();                                                //DE3774, set of old Dispatch Queue on service team
    Map<string, Id> mapGroupCode_Terrirory = new Map<string, Id>();                                     //DE3774, set of service team on insert
    Map<Id, string> mapServiceTeamId_DispatchQueue = new Map<Id, string>();                             //DE3774, map of Service TeamId : Dispatch Queue value
    Map<Id, SVMXC__Service_Group_Members__c> mapUpdateTechnician = new Map<Id, SVMXC__Service_Group_Members__c>();  //DE3774, map to update Technician records
    Map<ID, SVMXC__Territory__c> mapUpdateTerritory = new Map<ID, SVMXC__Territory__c>();               //map to update Territory Team records
    Map<string, SVMXC__Service_Group__c> mapServiceTeam = new Map<string, SVMXC__Service_Group__c>();   //DE3620, map group code : service team
    List<SVMXC__Service_Group__c> listInsertServiceTeam = new List<SVMXC__Service_Group__c>();          //DE3620, list to insert Service Team records
    List<SVMXC__Service_Group__c> listUpdateServiceTeam = new List<SVMXC__Service_Group__c>();          //DE3620, list to update Service Team records
    //Map<String, Id> mapGroupCode = new Map<String, Id>();                                             //map of GroupCode : service Team Id

    for(SVMXC__Service_Group__c varST : trigger.new)
    {
        //DE3620
        if(varST.Equipment_Required__c == true && (trigger.oldmap == null || trigger.oldmap.get(varST.ID).Equipment_Required__c == false) && varST.SVMXC__Group_Code__c != null)
        {
            setGroupCode.add(varST.SVMXC__Group_Code__c + '-E');
        }
        //DE3774, only on INSERT
        if(trigger.oldmap == null && varST.SVMXC__Group_Code__c != null)
        {
            mapGroupCode_Terrirory.put(varST.SVMXC__Group_Code__c, varST.Id);
        }
        //DE3774, only on UPDATE, 
        if(trigger.oldmap != null && varST.Dispatch_Queue__c != null && (trigger.oldmap.get(varST.ID).Dispatch_Queue__c == null || varST.Dispatch_Queue__c != trigger.oldmap.get(varST.ID).Dispatch_Queue__c))
        {
            mapServiceTeamId_DispatchQueue.put(varST.Id, varST.Dispatch_Queue__c);

            if(trigger.oldmap.get(varST.ID).Dispatch_Queue__c != null)
            {
                setOldDispatchQueue.add(trigger.oldmap.get(varST.ID).Dispatch_Queue__c);
            }
        }
        /*if(varST.SVMXC__Group_Code__c != null && (trigger.oldmap == null || trigger.oldmap.get(varST.ID).SVMXC__Group_Code__c == null || varST.SVMXC__Group_Code__c != trigger.oldmap.get(varST.ID).SVMXC__Group_Code__c))
            mapGroupCode.put(varST.SVMXC__Group_Code__c, varST.ID); */
    }

    if(/*mapGroupCode.size() > 0 ||*/ mapGroupCode_Terrirory.size() > 0)
    {
        for(SVMXC__Territory__c varTer : [select ID, name, SVMXC__Territory_Code__c, Service_Team__c from SVMXC__Territory__c where /*SVMXC__Territory_Code__c in :mapGroupCode.Keyset() OR*/ SVMXC__Territory_Code__c in :mapGroupCode_Terrirory.keySet() AND Service_Team__c = null])
        {
            //DE3774, update Service Team of Territory when Territory.Service Team = null and Territory.Territory Code = Service Team.Group Code
            if(mapGroupCode_Terrirory.size() > 0 && mapGroupCode_Terrirory.containsKey(varTer.SVMXC__Territory_Code__c) && varTer.Service_Team__c == null)
            {
                varTer.Service_Team__c = mapGroupCode_Terrirory.get(varTer.SVMXC__Territory_Code__c);
                mapUpdateTerritory.put(varTer.ID, varTer);
            }
            /*if(mapGroupCode.containsKey(varTer.SVMXC__Territory_Code__c))
                varTer.Service_Team__c = mapGroupCode.get(varTer.SVMXC__Territory_Code__c); */
        }
    }

    //DE3774
    if(mapServiceTeamId_DispatchQueue.size() > 0)
    {
        for(SVMXC__Service_Group_Members__c varTech : [select Id, SVMXC__Service_Group__c, Dispatch_Queue__c from SVMXC__Service_Group_Members__c where
                                                        SVMXC__Service_Group__c in :mapServiceTeamId_DispatchQueue.keySet()])
        {
            if(mapServiceTeamId_DispatchQueue.containsKey(varTech.SVMXC__Service_Group__c) && (varTech.Dispatch_Queue__c == null || (setOldDispatchQueue.size() > 0 && setOldDispatchQueue.contains(varTech.Dispatch_Queue__c))))
            {
                varTech.Dispatch_Queue__c = mapServiceTeamId_DispatchQueue.get(varTech.SVMXC__Service_Group__c);
                mapUpdateTechnician.put(varTech.Id, varTech);
            }
        }
    }

    //DE3620
    if(setGroupCode.size() > 0)
    {
        for(SVMXC__Service_Group__c varServTeam : [select Id, SVMXC__Group_Code__c, RecordtypeId, Name, Service_Team__c from SVMXC__Service_Group__c
                                                    where SVMXC__Group_Code__c in :setGroupCode])
        {
            mapServiceTeam.put(varServTeam.SVMXC__Group_Code__c , varServTeam);
            System.debug('already present record of service team to update found' + varServTeam.Id);
        }
    }

    for(SVMXC__Service_Group__c varST : trigger.new)
    {
        //DE3620
        if(varST.Equipment_Required__c == true && varST.SVMXC__Group_Code__c != null && (trigger.oldmap == null || trigger.oldmap.get(varST.ID).Equipment_Required__c == false))
        {
            SVMXC__Service_Group__c objST = new SVMXC__Service_Group__c();
            string strKey = varST.SVMXC__Group_Code__c + '-E';
            if(strKey != null && mapServiceTeam.containsKey(strKey) && mapServiceTeam.get(strKey) != null)  //service team with the group code exists
            {
                objST = mapServiceTeam.get(strKey);
                objST.Name = varST.Name + ' (Equipment)';
                objST.Service_Team__c = varST.Id;
                listUpdateServiceTeam.add(objST);
            }
            else    //service team with the group code does not exist
            {
                objST.Name = varST.Name + ' (Equipment)';
                objST.SVMXC__Group_Code__c = strKey;
                objST.RecordtypeId = recTypeIdSTEquipment;
                objST.Service_Team__c = varST.Id;
                listInsertServiceTeam.add(objST);
            }
        }
    }

    if(listInsertServiceTeam.size() > 0)
    {
        insert listInsertServiceTeam;
        System.debug('new created service team record' + listInsertServiceTeam);
    }
    if(listUpdateServiceTeam.size() > 0)
    {
        update listUpdateServiceTeam;
    }
    if(mapUpdateTerritory.size() > 0)
    {
        update mapUpdateTerritory.values();
    }
    if(mapUpdateTechnician.size() > 0)
    {
        update mapUpdateTechnician.values();
    }
    
    //STSK0012092
    if(trigger.isUpdate && trigger.isAfter)
        //STSK0012095: STRY0024706 : Service Manager updates from Service Team to Account, Puneet Mishra: 27 July 2017 
        SR_Class_ServiceTeam.serviceManagerUpdatefromTeamtoAccount(trigger.new, trigger.oldMap);
        //STSK0012092
        SR_Class_ServiceTeam.UpdateDM_FOA_SC(trigger.new, trigger.oldmap, trigger.isUpdate);
}
/***********************************************************************
* Author         : NA
*Created         : NA
Date/Modified By Name/Task or Story or Inc # /Description of Change
11-20-2017 - Rakesh - STSK0013241 - Update RM on DM field Update.
************************************************************************/

trigger SR_ServiceTeam_BeforeTrigger on SVMXC__Service_Group__c (before insert,before update) 
{
  if(SR_Class_ServiceTeam.chk_recursive  == false)
   {
     SR_Class_ServiceTeam.create_InvLocation(trigger.new,trigger.oldmap); // Creating Location
   }
   
   //STSK0013241 - Update RM on DM Update
   set<Id> setDMUser = new set<Id>();
   list<SVMXC__Service_Group__c> lstSG = new List<SVMXC__Service_Group__c>();
   for(SVMXC__Service_Group__c g : trigger.new){
       if(trigger.isInsert || (Trigger.isUpdate && g.District_Manager__c != (trigger.oldmap).get(g.Id).District_Manager__c)){
           if(g.District_Manager__c == null)
               g.Regional_Manager__c = null;
           else{ 
               lstSG.add(g);
               setDMUser.add(g.District_Manager__c);
           }
       } 
   }
   
   if(setDMUser.size()>0){
       map<Id,user> mapUser =new map<Id,user>([select Id,ManagerId from User where Id IN: setDMUser]);
       for(SVMXC__Service_Group__c g : lstSG){
           if(mapUser.containsKey(g.District_Manager__c)) g.Regional_Manager__c = mapUser.get(g.District_Manager__c).ManagerId ;
       }
   }
}
/*************************************************************************************************************************************
Created By    : Shubham Jaiswal
Created Date   : 9/25/2014
Reason  : US4672, to create for Task created with CallType value set
**************************************************************************************************************************************/

trigger SR_Task_AfterInsertUpdate_Trigger on Task (after insert,after update)
{
    List<Task> TaskList = new  List<Task>();
    Set<Id> SetCaseid = new Set<Id>();
    Map<id,Case> mapCaseId2CaseOwnerId;
    List<SVMXC__Case_Line__c> InsertCaseLineList = new List<SVMXC__Case_Line__c>();
    
    for(Task taskalias : trigger.new)
    {
        system.debug('@@@@@@@@@@@@' +taskalias);
        if(trigger.isinsert)
        {
            if(taskalias.CallType != Null && taskalias.WhatId != Null && String.valueof(taskalias.WhatId).startsWith(Case.sObjectType.getDescribe().getKeyPrefix()))
            {
                TaskList.add(taskalias);
                SetCaseid.add(taskalias.WhatId);
            }
        }
    }
    
    if(SetCaseid.size() > 0)
    {
        mapCaseId2CaseOwnerId = new Map<id,Case>([SELECT id, ownerid, SVMXC__Billing_Type__c, origin FROM Case Where Id IN:SetCaseid ]);
        for(Task taskalias : TaskList)
        {
           if(mapCaseId2CaseOwnerId.containsKey(taskalias.WhatId) && mapCaseId2CaseOwnerId.get(taskalias.WhatId).Origin == 'Phone')
           {
               SVMXC__Case_Line__c caseline = new SVMXC__Case_Line__c();
               caseline.SVMXC__Case__c = taskalias.WhatId;
               //caseline.Case_Line_Owner__c = mapCaseId2CaseOwnerId.get(taskalias.WhatId).ownerid ; 
               caseline.Case_Line_Owner__c = taskalias.OwnerId; // Changes for defect DFCT0010365
               
               if(taskalias.CallDurationInSeconds <= 360 )
               {
                   system.debug('@@@@@@@@caseline.Start_Date_time__c' + caseline.Start_Date_time__c);
                   system.debug('@@@@@@@@taskalias.CreatedDate' + taskalias.CreatedDate);
                   system.debug('@@@@@@@@taskalias.CreatedDate - 0.04166666667@@@@' + (taskalias.CreatedDate - 0.04166666667) );
                   caseline.Start_Date_time__c = taskalias.CreatedDate - 0.004166666667 ;
                   system.debug('@@@@@@@@caseline.Start_Date_time__c' + caseline.Start_Date_time__c);
               }
               else
               {
                    caseline.Start_Date_time__c = taskalias.CreatedDate - (Double.valueof(taskalias.CallDurationInSeconds)/86400) ;
               }    
               caseline.End_date_time__c = taskalias.CreatedDate;
               system.debug('@@@@@@@@caseline.End_date_time__c' +caseline.End_date_time__c);
               caseline.SVMXC__Summary__c = taskalias.CallType +' '+taskalias.Subject;
               InsertCaseLineList.add(caseline);
               caseline.Billing_Type__c = mapCaseId2CaseOwnerId.get(taskalias.WhatId).SVMXC__Billing_Type__c;
            }
        }
        
        if(InsertCaseLineList.size() > 0)
        {
            insert InsertCaseLineList;
            system.debug('@@@@@@@@@InsertCaseLineList' + InsertCaseLineList );
        }
    }
}
/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** *
@ Author: Chiranjeeb Dhar
@ Date: 29 - Oct - 2013
@ Description: This Trigger is Used to Create a new Case when a Customer replies to an Case Email the
case for which is closed.Trigger sets Parent Case and owner as Queue.
@ Last Modified On:
@ Last Modified Reason:
** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */
trigger SR_Task_CreateCaseonReplytoClosedCase on Task(before Insert) {

  Boolean checkcaseinsertion = false;
  List < Case > ListofCasestobeinserted = new List < Case > ();
  for (Task taskalias: Trigger.New) {
    String TaskWhatid = taskalias.Whatid;
    if (TaskWhatid != Null && TaskWhatid.StartsWith('500')) {
      Case casealias = [Select Id, Status, AccountId, ContactId, Reason from Case where id =: taskalias.whatid];
      if (casealias.status == 'Closed (Close case)' && checkcaseinsertion == false) {
        List < QueueSobject > queueowner = [Select QueueId from QueueSobject where SobjectType =: 'Case'
          and Queue.DeveloperName =: 'Email_Web_To_Case_EMEA'
        ];
        Case newcasealias = new Case();
        newcasealias.AccountId = casealias.AccountId;
        newcasealias.ContactId = casealias.ContactId;
        newcasealias.Reason = casealias.Reason;
        newcasealias.ParentId = casealias.Id;
        newcasealias.Origin = 'Email';
        newcasealias.OwnerId = queueowner[0].QueueId;
        ListofCasestobeinserted.add(newcasealias);
        checkcaseinsertion = true;

      }
    }
  }
  if (ListofCasestobeinserted.size() > 0) {
    insert ListofCasestobeinserted;
  }
  /*
   * Planning Request
   */
  String planReqRecId = RecordTypeUtility.getInstance().rtMaps.get('Task').get('PlanningRequest');
    set<Id> oppIds = new set<Id>();
    
    for(Task taskalias: Trigger.New) {
    if(taskalias.RecordTypeId == planReqRecId){
            String whatId = taskalias.WhatId;
            if(whatId.startsWith('006')){
                oppIds.add(taskalias.WhatId);
            }
      taskalias.Subject = 'Planning / Pre-Sales Request';
      taskalias.OwnerId = Label.Planning_Request_User_Id == null?Userinfo.getuserId():Label.Planning_Request_User_Id;
      taskalias.ActivityDate = System.today().addDays(7);
    }
  }
    if(!oppIds.isEmpty()) {
        map<Id,Opportunity> oppMap  = new map<Id,Opportunity>([select Id, Owner.Email from Opportunity where id in :oppIds ]);
        for(Task taskalias: Trigger.New) {
            String whatId = taskalias.WhatId;
            if(taskalias.RecordTypeId == planReqRecId && whatId.startsWith('006') 
               && oppMap.containsKey(taskalias.WhatId)){
                taskalias.Opportunity_Owner_Email__c = oppMap.get(taskalias.WhatId).Owner.Email;
            }
    }
    }
    
}
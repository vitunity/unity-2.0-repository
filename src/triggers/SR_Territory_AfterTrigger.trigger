/******************************************************
STSK0014941 - update Account Service Region based on Parent territory description update 
******************************************************/

trigger SR_Territory_AfterTrigger on SVMXC__Territory__c (after insert, after update) 
{
    
    if(trigger.isinsert || trigger.isupdate){
        AccountFieldUpdate.findCSSDistrict(trigger.new,trigger.oldmap);
    }
}
/******************************************************
Created By: Parul Gupta
Created Date: 2/18/2015
Created Reason: DE3500, If a Service Territory is created after Service Team is created, the Service Team lookup should be set automatically.
******************************************************/

trigger SR_Territory_BeforeTrigger on SVMXC__Territory__c (before insert, before update) 
{
    Set<string> setTerritoryCode = new Set<string>();
    Map<string, Id> mapSerTeam_GroupCode = new Map<string, Id>();

    for(SVMXC__Territory__c varTer : trigger.new)
    {
        if(varTer.SVMXC__Territory_Code__c != null && ((trigger.oldMap == null || trigger.oldMap.get(varTer.Id).SVMXC__Territory_Code__c == null || trigger.oldMap.get(varTer.Id).SVMXC__Territory_Code__c != varTer.SVMXC__Territory_Code__c) || (varTer.Service_Team__c == null)))
        {
            setTerritoryCode.add(varTer.SVMXC__Territory_Code__c);
        }
    }

    if(setTerritoryCode.size() > 0)
    {
        for(SVMXC__Service_Group__c varTeam : [select Id, SVMXC__Group_Code__c from SVMXC__Service_Group__c where SVMXC__Group_Code__c in :setTerritoryCode])
        {
            mapSerTeam_GroupCode.put(varTeam.SVMXC__Group_Code__c, varTeam.Id);
        }
    }

    for(SVMXC__Territory__c objTer : trigger.new)
    {
        if(objTer.SVMXC__Territory_Code__c != null && mapSerTeam_GroupCode.containsKey(objTer.SVMXC__Territory_Code__c) && mapSerTeam_GroupCode.get(objTer.SVMXC__Territory_Code__c) != null)
        {
            objTer.Service_Team__c = mapSerTeam_GroupCode.get(objTer.SVMXC__Territory_Code__c);
        }
    }
}
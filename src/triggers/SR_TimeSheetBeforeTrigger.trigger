trigger SR_TimeSheetBeforeTrigger on SVMXC_Timesheet__c (before insert, before update) 
{
    System.debug('JKO Number of Queries before SR_TimeSheetBeforeTrigger : ' + Limits.getQueries());
    if(Trigger.isUpdate)
    {
        System.debug('TIMESHEET BEFORE UPDATE TRIGGER FIRED');
        
        for(SVMXC_Timesheet__c newTimeSheetRecord: Trigger.New) {
            SVMXC_Timesheet__c oldTimeSheetRecord = Trigger.oldMap.get(newTimeSheetRecord.id);
            
            newTimeSheetRecord.Total_Time_Worked__c = newTimeSheetRecord.Roll_Up_Direct_Time__c + newTimeSheetRecord.Roll_Up_InDirect_Time_Exc_PPL_Holi__c;
            //DK/Forefront Update for US5169 - Training 
            
            if((newTimeSheetRecord.Roll_Up_Direct_Time__c != oldTimeSheetRecord.Roll_Up_Direct_Time__c) || (newTimeSheetRecord.Roll_Up_InDirect_Time_Exc_PPL_Holi__c != oldTimeSheetRecord.Roll_Up_InDirect_Time_Exc_PPL_Holi__c))
                return; // if trigger is called by update in ROll up summary fields, then it should not execute actually
        }
        
    }
    
    if(Trigger.isUpdate)
    {
        list<SVMXC_Timesheet__c> tmList = new list<SVMXC_Timesheet__c>();
        
        map<Id,string> tmIdCommentsMap = new map<Id,string>();
        
        tmList =[Select Id, Comments_Available__c, Comments__c, (Select Id, IsPending, ProcessInstanceId,
                TargetObjectId, StepStatus, OriginalActorId, 
                ActorId, RemindersSent, Comments, IsDeleted, 
                CreatedDate, CreatedById, SystemModstamp From ProcessSteps order by CreatedDate desc) 
                From SVMXC_Timesheet__c where id=:trigger.new];
        system.debug('tmList====='+tmList);
        
        String commentsStr='';
        if (tmList.size()>0)
        {
            SVMXC_Timesheet__c tm= tmList[0];
            
            for (ProcessInstanceHistory ps : tm.ProcessSteps)
            {
                if(ps.comments!= null && ps.comments.contains('Rejection')){
                    commentsStr += ps.comments; 
                    commentsStr = commentsStr.substringAfter(':');
                    break;
                }
                //commentsStr += ps.comments;
                
            }
            
        }
        
        for(SVMXC_Timesheet__c tm : trigger.new){
            if(tm.Comments_Available__c == true){
                tm.Comments__c = commentsStr;
                //tm.Comments_Available__c = false; 
            }
        }
    }
    
    //Logic from workflow for updating the TimeSheet Unique
    /*if(trigger.ISInsert)
    {
        set<id> techid = new set<id>();
        Map<id,SVMXC__Service_Group_Members__c> mapOfTech = new Map<id,SVMXC__Service_Group_Members__c>();
        for(SVMXC_Timesheet__c tm : trigger.New)
        {
            if(tm.Technician__c != null)
                techid.add(tm.Technician__c);
        }
        if(techid.size() > 0)
        {
            for(SVMXC__Service_Group_Members__c tech : [Select id,ERP_Work_Center__c from SVMXC__Service_Group_Members__c where id in: techid])
            {
                mapOfTech.put(tech.id,tech);
            }
        }
        if(mapOfTech != null)
        {
            for(SVMXC_Timesheet__c tm : trigger.new)
            {
                if(tm.Technician__c != null && mapOfTech.containskey(tm.Technician__c))
                    tm.TimeSheet_Unique__c = mapOfTech.get(tm.Technician__c).id+mapOfTech.get(tm.Technician__c).ERP_Work_Center__c + string.ValueOf(tm.Start_Date__c);
            }
        }
     
    }*/
    
    if(trigger.isInsert || trigger.isUpdate){
     System.debug('JKO Number');
        SR_Class_SVMXCTimesheet objTS = new SR_Class_SVMXCTimesheet();
        objTS.udpateFieldsOnTimesheetInBeforeInsertUpdateTrigger(Trigger.new, trigger.oldmap); //DM Uncomment after method is saved in class file
    }
    
    System.debug('INSIDE TIMESHEET BEFORE RIGGER');
    SR_Class_SVMXCTimesheet objTS = new SR_Class_SVMXCTimesheet(); //initialising class for US4001
    if(trigger.IsInsert || trigger.isUpdate){
        
        objTS.udpateFieldsOnTimesheetInBeforeTrigger(Trigger.new);  //method call for US4001 //DM Uncomment after method is saved in class file
    }
    System.debug('JKO Number of Queries after SR_TimeSheetBeforeTrigger : ' + Limits.getQueries());    
}
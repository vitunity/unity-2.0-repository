/***************************************
Created By :    Parul Gupta
Created On :    4-June-2014
created for:    US4001, Trigger on Time Entry for Auto-Approval Process.
Modified By :   Parul Gupta
Modified On :   6-June-2014
Modified for:   US4001, Trigger on Time Entry for Auto-Approval Process.
Modified By :   Parul Gupta
Modified On :   11-June-2014
Modified for:   US4001, Added labels and Auto-Reject Process.
***************************************/

trigger SR_TimeSheet_AfterTrigger on SVMXC_Timesheet__c (after insert, after update) //Trigger to  update TE and WD fields and Initiate Auto-Approval Process for TE and WD
{
    if(ServiceMaxTimesheetUtils.TIMESHEET_UPDATED_FROM_TIME_ENTRY == true) // Sunil 6Feb
        return; // This mean Child (Time Entry) updated (set some fields) the parent Timesheet, but it should not further trigger
    
    List<sObject> objectDeletes = new List<sObject>();
    Set<Id> timeEntries2delete = new Set<Id>();
    List<Id> workDetailDeleteIds = new List<Id>();
    Set<Id> workOrdersToUpdate = new Set<Id>();
    
    if(Trigger.isUpdate)
    {
        System.debug('TIMESHEET AFTER UPDATE TRIGGER FIRED');

        //savon/FF 9-17-15 US5108 DE5799 & DE5802
        List<SVMXC_Timesheet__c> timeSheets = [SELECT Id, Status__c, (SELECT Id, Work_Details__c, Work_Details__r.SVMXC__Service_Order__c, Work_Details__r.SVMXC__Service_Order__r.RecordTypeId 
                                                    FROM Time_Entries__r) FROM SVMXC_Timesheet__c WHERE Id IN :Trigger.new];
        
        List<Id> workDetailApprovalIds = new List<Id>();
        List<Id> workOrderApprovalIds = new List<Id>();

        Id projectManagement = RecordTypeHelperClass.WORK_ORDER_RECORDTYPES.get('Project Management').getRecordTypeId();
       
        Set<Id> SubmittedTimesheets = new Set<Id>();

        for (SVMXC_Timesheet__c ts : timeSheets) {
            //DE6311
            SVMXC_Timesheet__c oldTimeSheetRecord = Trigger.oldMap.get(ts.id);
            if(ts.Status__c =='Submitted' && oldTimeSheetRecord.Status__c != 'Submitted'){
                SubmittedTimesheets.add(ts.id);
            }
            
            //DE5802
            if (ts.Status__c == 'Incomplete' && Trigger.oldMap.get(ts.Id).Status__c == 'Submitted') {
                for (SVMXC_Time_Entry__c te : ts.Time_Entries__r) {
                    if (te.Work_Details__r.SVMXC__Service_Order__r.RecordTypeId == projectManagement) {
                        objectDeletes.add(te);
                        timeEntries2delete.add(te.Id);
                        if (te.Work_Details__c != null) {
                            workDetailDeleteIds.add(te.Work_Details__c);
                            if (!workOrdersToUpdate.contains(te.Work_Details__r.SVMXC__Service_Order__c)) {
                                workOrdersToUpdate.add(te.Work_Details__r.SVMXC__Service_Order__c);
                            }
                        }                        
                    }
                }
            }  

            /*
            //DE5799
            if (ts.Status__c == 'Approved' && Trigger.oldMap.get(ts.Id).Status__c == 'Submitted') {
                for (SVMXC_Time_Entry__c te : ts.Time_Entries__r) {
                    if (te.Work_Details__r.SVMXC__Service_Order__r.RecordTypeId == projectManagement) {
                        if (te.Work_Details__c != null) {
                            workDetailApprovalIds.add(te.Work_Details__c);
                        }
                        if (te.Work_Details__r.SVMXC__Service_Order__c != null) {
                           workOrderApprovalIds.add(te.Work_Details__r.SVMXC__Service_Order__c); 
                        }
                    }else{

                    }

                }
            }  */ //SRK Commented out for DE7238   n     
        }
        
        if( SubmittedTimesheets.size() > 0){
            SR_Class_SVMXCTimesheet objTe = new SR_Class_SVMXCTimesheet();
            objTe.updateTimeEntryWorkDetailWorkOrder(SubmittedTimesheets);
        }
        
        //DE5802
        /*if (!workDetailDeleteIds.isEmpty()) {
            List<sObject> wdDeletes = [SELECT Id FROM SVMXC__Service_Order_Line__c WHERE Id IN :workDetailDeleteIds];
            objectDeletes.addAll(wdDeletes);
        }*/
        /* pszagdaj/FF, 1/22/2016, DE6812 - Needs to ba executed after Rejection actions
        if (!objectDeletes.isEmpty()) {
            delete objectDeletes;
        }
        
        if (!workOrdersToUpdate.isEmpty()) {
            List<SVMXC__Service_Order__c> workOrdersAssiged = [SELECT Id FROM SVMXC__Service_Order__c WHERE Id IN :workOrdersToUpdate];
            for (SVMXC__Service_Order__c wo: workOrdersAssiged) {
                wo.SVMXC__Order_Status__c = 'Assigned';
            }
            update workOrdersAssiged;
        }
        
        */
        
       /*
        //DE5799
        if (!workOrderApprovalIds.isEmpty()) {
            List<SVMXC__Service_Order__c> workOrderApprovals = [SELECT Id FROM SVMXC__Service_Order__c WHERE Id IN :workOrderApprovalIds];
            for (SVMXC__Service_Order__c wo : workOrderApprovals) {
                wo.SVMXC__Order_Status__c = 'Approved';
                wo.Interface_Status__c = 'Process';
            }
            update workOrderApprovals;
        }
        if (!workDetailApprovalIds.isEmpty()) {
            List<SVMXC__Service_Order_Line__c> workDetailApprovals = [SELECT Id FROM SVMXC__Service_Order_Line__c WHERE Id IN :workDetailApprovalIds];
            for (SVMXC__Service_Order_Line__c wd : workDetailApprovals) {
                wd.SVMXC__Line_Status__c = 'Approved';
                wd.Interface_Status__c = 'Process';
            }
            update workDetailApprovals;
        }

		*/ //SRK commented out for DE7238
        //----------------------------------------------------------

        for(SVMXC_Timesheet__c newTimeSheetRecord: Trigger.New)
        {

            SVMXC_Timesheet__c oldTimeSheetRecord = Trigger.oldMap.get(newTimeSheetRecord.id);
            //System.debug('Old Roll_Up_Direct_Time__c = '+oldTimeSheetRecord.Roll_Up_Direct_Time__c + '   **  New  Roll_Up_Direct_Time__c = '+newTimeSheetRecord.Roll_Up_Direct_Time__c);
            //System.debug('Old Roll_Up_InDirect_Time_Exc_PPL_Holi__c = '+oldTimeSheetRecord.Roll_Up_InDirect_Time_Exc_PPL_Holi__c + '   **  New  Roll_Up_InDirect_Time_Exc_PPL_Holi__c = '+newTimeSheetRecord.Roll_Up_InDirect_Time_Exc_PPL_Holi__c);
            if((newTimeSheetRecord.Roll_Up_Direct_Time__c != oldTimeSheetRecord.Roll_Up_Direct_Time__c) || (newTimeSheetRecord.Roll_Up_InDirect_Time_Exc_PPL_Holi__c != oldTimeSheetRecord.Roll_Up_InDirect_Time_Exc_PPL_Holi__c))
                return; // if trigger is called by update in ROll up summary fields, then it should not execute actually
        }
        
    }
    System.debug('INSIDE TIMESHEET BEFORE RIGGER');
    //After updating TS record Log History
    /*if(Trigger.isUpdate)   
    {
        CustomHistoryTrackingEnabler.LogHistoryForLongAndRich(Trigger.newMap, Trigger.oldMap);
    }*/
    
    Set<ID> setTimeSheetID = new Set<ID>(); //Set to store timesheet IDs where status is either Submitted or Approved
    FINAL String TS_Status_Submitted = system.label.TimeSheet_Status_Submitted; //custom label to add Status Picklist value Submitted in object Time Sheet
    FINAL String TS_Status_Approved = system.label.TimeSheet_Status_Approved; //custom label to add Status Picklist value Approved in object Time Sheet
    FINAL String TS_Status_Open = system.label.Time_Sheet_Status_Open;  //custom label to add Status Picklist value Open in object Time Sheet

    Set<Id> rejectedTSIdList = new Set<Id>(); //JW 6-23-2015 VAR-57 US5169 TA6836
     Set<Id> approvedTimeSheets = new Set<ID>(); 
    for(SVMXC_Timesheet__c varST : trigger.new) //Iterate TS records
    {
        if(Trigger.isInsert)    //After creating new TS record
        {
            if((string.valueOf(varST.Status__c) == TS_Status_Submitted) || (string.valueOf(varST.Status__c) == TS_Status_Approved) || (string.valueOf(varST.Status__c) == TS_Status_Open))
            {
                setTimeSheetID.add(varST.id);
                system.debug('Time sheet ID !##@$' +setTimeSheetID);
            }
        }
        if(Trigger.isUpdate)    //After updating TS record
        {
            if((string.valueOf(varST.Status__c) == TS_Status_Submitted && string.valueOf(Trigger.oldMap.get(varST.id).Status__c)  != TS_Status_Submitted) || (string.valueOf(varST.Status__c) == TS_Status_Approved && string.valueOf(Trigger.oldMap.get(varST.id).Status__c)  != TS_Status_Approved) || (string.valueOf(varST.Status__c) == TS_Status_Open && string.valueOf(Trigger.oldMap.get(varST.id).Status__c)  != TS_Status_Open))
            {
                setTimeSheetID.add(varST.id);
                system.debug('Time sheet ID !##@$' +setTimeSheetID);
            }
            
            if(varST.Status__c == 'Incomplete' && varST.Status__c != Trigger.oldMap.get(varST.id).Status__c) //JW 6-23-2015 VAR-57 US5169 TA6836
            {
                rejectedTSIdList.add(varST.Id);
            }

            if(varST.Status__c == 'Approved' && varST.Status__c != Trigger.oldMap.get(varST.id).Status__c){
                approvedTimeSheets.add(varST.Id);
            }
        }
    }

    if(!approvedTimeSheets.isEmpty())
    {
        Timesheet_TriggerHandler.updateApprovedTSRelatedRecs(approvedTimeSheets); 
    }


    if(!rejectedTSIdList.isEmpty())
    {
        Timesheet_TriggerHandler.updateRejectedTSRelatedRecs(rejectedTSIdList, timeEntries2delete); //JW 6-23-2015 VAR-57 US5169 TA6836
    }
    
    // pszagdaj/FF, 1/22/2016, DE6812 - Needs to ba executed after Rejection actions
    /*if (!objectDeletes.isEmpty()) {
        system.debug('pete>> trigger objectDeletes: ' + objectDeletes);
        delete objectDeletes;
    }*/
    
    if (!workOrdersToUpdate.isEmpty()) { //REFACTOR:  Should call a method on the Work Order to do this so the code is not distributed
        List<SVMXC__Service_Order__c> workOrdersAssiged = [SELECT Id FROM SVMXC__Service_Order__c WHERE Id IN :workOrdersToUpdate];
        for (SVMXC__Service_Order__c wo: workOrdersAssiged) {
            wo.SVMXC__Order_Status__c = 'Assigned';
            wo.Interface_STatus__c = null;
            wo.ERP_Status__c = null;
        }
        update workOrdersAssiged;
    }
    //
    
    SR_Class_SVMXCTimesheet objTS = new SR_Class_SVMXCTimesheet(); //initialising class for US4001
    if(setTimeSheetID.size() > 0)
    {
        system.debug('Initialising Class' +system.now());
        objTS.updateTimeEntryAndWorkDetail(setTimeSheetID);  //method call for US4001
    }
    if(Trigger.isInsert)    //After creating new TS record
    {
         system.debug('Calling CheckProfileAndCreateDefaultLunchBreaksOnTimeSheetCreation');
        objTS.CheckProfileAndCreateDefaultLunchBreaksOnTimeSheetCreation(Trigger.new); // For US4398 (Time Card -- System auto-creates Default Lunch Entries)
    }
}
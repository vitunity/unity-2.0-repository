/******************************
Last Modified By : Kaushiki Verma for US4683 on 9/07/14
Last Modified By :    Nikita Gupta
Last Modified on :    7/24/2014
Last Modified Reason: US3124, to update Country Lookup based on Country picklist field
Last Modified By :    Priti Jaiswal
Last Modified on :    01-Aug-2014
Last Modified Reason: DE833 - set the Country Lookup on Location to [Country.name = Country (picklist).value] else [Country.ISO Code 3 = Country (picklist).value] or [Country.SAP code = Country (picklist).value]
***********************/
trigger SR_Update_LocationAdress on SVMXC__Site__c (before insert,before update) 
{
    Set<String> setOfERPContryCode = new Set<String>();
    Set<String> setErpPartnerCode = new Set<String>();                                                              //DE3238
    Set<string> setERPSoldTo = new Set<string>();                                                                   //DE3238
    Set<String> setCountryName = new Set<String>();                                                                 //Nikita, US3124
    Set<String> setOfSalesOrgs = new Set<String>();
    Set<string> setWorkCenter = new Set<string>();                                                                  //DE1777
    Map<string, Id> mapWorkCenter_TechUser = new Map<string, Id>();                                                 //DE1777
    Map<string, Account> mapERPSitePartner_Account = new Map<string, Account>();                                              //DE3238
    Map<string, Account> mapERPSoldTo_Account = new Map<string, Account>();                                                   //DE3238
    Map<String, ID> mapCountryName_ID = new Map<String, ID>();
    Map<String, Id> mapOfConSAPCodeAndConId = new Map<String, Id>();
    Map <String,ERP_Org__c> mapOfERPorgs = new Map<String,ERP_Org__c>();
    Map<string,ERP_Partner_Association__c> mapPartnerNoAndPartner = new Map<string,ERP_Partner_Association__c>();
    //Set<Id> AccountIds = new Set<Id>();
    //Map<Id,Account> MapofAccount = new Map<Id,Account>();

    for(SVMXC__Site__c  varLoc : Trigger.new)
    {
        //US4683, 9/07/14   //code updated for DE3238.
        if(varLoc.ERP_Site_Partner_Code__c != null && (trigger.oldmap != null || trigger.oldmap == null || trigger.oldmap.get(varLoc.Id).ERP_Site_Partner_Code__c == null || trigger.oldmap.get(varLoc.ID).ERP_Site_Partner_Code__c != varLoc.ERP_Site_Partner_Code__c))
        {
            setErpPartnerCode.add(varLoc.ERP_Site_Partner_Code__c);   //set of ERP Site partner to populate account on location
            System.debug('set of erp site partnet code value === ' + setErpPartnerCode);
        }
        
        /*Commented by Naren for ENHC0010587 
        if(varLoc.ERP_Sold_To_Code__c != null && (trigger.oldmap != null || trigger.oldmap == null || trigger.oldmap.get(varLoc.Id).ERP_Sold_To_Code__c == null || trigger.oldmap.get(varLoc.ID).ERP_Sold_To_Code__c != varLoc.ERP_Sold_To_Code__c))
        {
            setERPSoldTo.add(varLoc.ERP_Sold_To_Code__c);
            System.debug('set of erp sold to code value === ' + setERPSoldTo);
            if(varLoc.ERP_Site_Partner_Code__c != null)
            {
                setErpPartnerCode.add(varLoc.ERP_Site_Partner_Code__c);
            } 
        }
        Commented by Naren for ENHC0010587 */
        
        //Code added by Nikita for US3124, 7/24/2014; DE1500           
        if(varLoc.SVMXC__Country__c != null && (Trigger.IsInsert || (Trigger.IsUpdate && (varLoc.Country__c == null || trigger.oldmap.get(varLoc.ID).SVMXC__Country__c == null || varLoc.SVMXC__Country__c != trigger.oldmap.get(varLoc.ID).SVMXC__Country__c))))
        {
            setCountryName.add(varLoc.SVMXC__Country__c); //set of country to populate country picklist on location
        }
        //added by priti for DE833, 08/01/2014; DE1500
        if(varLoc.ERP_Country_Code__c != null && ((Trigger.IsInsert && varLoc.Country__c == null)  || (Trigger.IsUpdate && (varLoc.Country__c == null || trigger.oldmap.get(varLoc.ID).ERP_Country_Code__c == null || varLoc.ERP_Country_Code__c != trigger.oldmap.get(varLoc.ID).ERP_Country_Code__c))))
        {
            setOfERPContryCode.add(varLoc.ERP_Country_Code__c);   //set of erp country code from location
        }
        if(varLoc.Sales_Org__c != null && ((Trigger.IsInsert && varLoc.ERP_Org__c == null)  || (Trigger.IsUpdate && (varLoc.ERP_Org__c == null || trigger.oldmap.get(varLoc.ID).Sales_Org__c == null || varLoc.Sales_Org__c != trigger.oldmap.get(varLoc.ID).Sales_Org__c))))
        {
            setOfSalesOrgs.add(varLoc.Sales_Org__c);  //set of sales org from location
        }
        //code for DE1777, to populate Service Engineer from Technician's Work Center
        if(varLoc.SVMXC__Location_Type__c == 'Field' && varLoc.Work_Center__c != null && (trigger.oldMap == null || trigger.oldMap.get(varLoc.Id).Work_Center__c == null || trigger.oldMap.get(varLoc.Id).Work_Center__c != varLoc.Work_Center__c))
        {
            setWorkCenter.add(varLoc.Work_Center__c);
        }
        /*if(varLoc.SVMXC__Account__c != null)
            AccountIds.add(varLoc.SVMXC__Account__c);    //set of Account Id's to update Account values on location */
    }

    //Kaushiki Verma for US4683 on 9/07/14 start
    if(setErpPartnerCode.size() > 0) // || setERPSoldTo.size() > 0)// || AccountIds.size() > 0
    {
        List<Account> AccountList = [Select id, name, AccountNumber, ERP_Site_Partner_Code__c, BillingCity, BillingCountry, BillingState, BillingStreet, BillingPostalCode,
                                                    ShippingCity, ShippingCountry, ShippingState, ShippingStreet, ShippingPostalCode from Account
                                                    where ERP_Site_Partner_Code__c in :setErpPartnerCode ];
        if(AccountList.size() > 0)
        {
            system.debug('AccountList size() is == ' + AccountList.size() + 'AccountList is == ' + AccountList);
            for(Account varAcc : AccountList)
            {
                if(varAcc.ERP_Site_Partner_Code__c != null && setErpPartnerCode.size() > 0 && setErpPartnerCode.contains(varAcc.ERP_Site_Partner_Code__c))
                {
                    mapERPSitePartner_Account.put(varAcc.ERP_Site_Partner_Code__c, varAcc);
                    System.debug('map of account and site partner code of location ' + mapERPSitePartner_Account.get(varAcc.ERP_Site_Partner_Code__c));
                }
                
                /* Commented by Naren for ENHC0010587
                else if(varAcc.AccountNumber != null && setERPSoldTo.size() > 0 && setERPSoldTo.contains(varAcc.AccountNumber))
                {
                    mapERPSoldTo_Account.put(varAcc.AccountNumber, varAcc);
                    System.debug('map of account and sold to code of location ' + mapERPSoldTo_Account.get(varAcc.AccountNumber));
                }
                Commented by Naren for ENHC0010587 */
                
                /*if(AccountIds.size() > 0 && AccountIds.contains(varAcc.Id))
                    MapofAccount.put(varAcc.Id, varAcc);    */
            }
        }
        
        /* commented by Naren for ENHC0010587 
        if(setErpPartnerCode.size() > 0)
        {
            for(ERP_Partner_Association__c varErpPartnerAss : [select id, name, ERP_Partner__r.Partner_Number__c, ERP_Partner__c, Customer_Account__c, 
                                                                Customer_Account__r.Functional_Location__c,
                                                                Partner_Function__c from ERP_Partner_Association__c where ERP_Partner__r.Partner_Number__c != null AND
                                                                ERP_Partner__r.Partner_Number__c in : setErpPartnerCode and Partner_Function__c = 'Z1=Site Partner'])
            {
                mapPartnerNoAndPartner.put(varErpPartnerAss.ERP_Partner__r.Partner_Number__c, varErpPartnerAss);
            }
        }
        commented by Naren for ENHC0010587 */
    }

    //code for DE1777, to populate Service Engineer from Technician's Work Center
    if(setWorkCenter.size() > 0)
    {
        List<SVMXC__Service_Group_Members__c> listTempTech = [select Id, ERP_Work_Center__c, User__c from SVMXC__Service_Group_Members__c
                                                                where ERP_Work_Center__c in :setWorkCenter];
        if(listTempTech.size() > 0)
        {
            for(SVMXC__Service_Group_Members__c varTech : listTempTech)
            {
                if(varTech.ERP_Work_Center__c != null && varTech.User__c != null)
                {
                    mapWorkCenter_TechUser.put(varTech.ERP_Work_Center__c, varTech.User__c);
                }
            }
        }
    }

    if(setOfSalesOrgs.size() > 0)
    {
        for(ERP_Org__c erp : [select id, Sales_Org__c from ERP_Org__c where Sales_Org__c in :setOfSalesOrgs])
        {
            mapOfERPorgs.put(erp.Sales_Org__c,erp);
        }
    }
    System.debug('set of country name--> ' + setCountryName);

    //Code added by Nikita for US3124, 7/24/2014
    if(setCountryName.size() > 0 || setOfERPContryCode.size() > 0)
    {
        //where condition modified by priti for DE833, 01/08/2014 //updated for DE5594
        List<Country__c> listCountryID_Name = [Select ID, Name, Alternate_Name__c, ISO_Code_2__c, ISO_Code_3__c, SAP_Code__c from Country__c 
                                                where (name in : setCountryName OR Alternate_Name__c in : setCountryName 
                                                OR ISO_Code_2__c in : setCountryName OR ISO_Code_3__c in : setCountryName 
                                                OR SAP_Code__c in : setCountryName OR SAP_Code__c in : setOfERPContryCode)];
        if(listCountryID_Name.size() > 0)
        {
            for(Country__c varC : listCountryID_Name)
            {
                System.debug('country name --> ' + varC.Name);
                System.debug('country iso code3 --> ' + varC.ISO_Code_3__c);
                System.debug('country sap code --> ' + varC.SAP_Code__c);

                if(varC.SAP_Code__c != null && setOfERPContryCode.size() > 0 && setOfERPContryCode.contains(varC.SAP_Code__c))
                {
                    mapOfConSAPCodeAndConId.put(varC.SAP_Code__c, varC.Id);
                }
                if(varC.Name != null && setCountryName.size() > 0 && setCountryName.contains(varC.Name))
                {
                    mapCountryName_ID.put(varC.Name, varC.ID);
                }
                else if(varC.Alternate_Name__c != null && setCountryName.size() > 0 && setCountryName.contains(varC.Alternate_Name__c))
                {
                    mapCountryName_ID.put(varC.Alternate_Name__c, varC.ID);
                }
                else if(varC.ISO_Code_2__c != null && setCountryName.size() > 0 && setCountryName.contains(varC.ISO_Code_2__c))
                {
                    mapCountryName_ID.put(varC.ISO_Code_2__c, varC.ID);
                }
                else if(varC.ISO_Code_3__c != null && setCountryName.size() > 0 && setCountryName.contains(varC.ISO_Code_3__c))
                {
                    mapCountryName_ID.put(varC.ISO_Code_3__c, varC.ID);
                }
                else if(varC.SAP_Code__c != null && setCountryName.size() > 0 && setCountryName.contains(varC.SAP_Code__c))
                {
                    mapCountryName_ID.put(varC.SAP_Code__c, varC.ID);                       
                }
            }   
        }            
    }

    for(SVMXC__Site__c varLoc: Trigger.new)
    {
        /* commented by Naren for ENHC0010587 
        if(varLoc.ERP_Site_Partner_Code__c != null && mapPartnerNoAndPartner.containskey(varLoc.ERP_Site_Partner_Code__c) && mapPartnerNoAndPartner.get(varLoc.ERP_Site_Partner_Code__c) != null)
        {
            system.debug('in the else condition');
            varLoc.SVMXC__Account__c = mapPartnerNoAndPartner.get(varLoc.ERP_Site_Partner_Code__c).Customer_Account__c; 
            //functionalLocation = mapPartnerNoAndPartner.get(varLoc.ERP_Site_Partner_Code__c).Customer_Account__r.Functional_Location__c;
            System.debug('about to set value of account from erp partner association.partner number ' + varLoc.SVMXC__Account__c);
        }
        commented by Naren for ENHC0010587 */
        
        //update account on location with ERP Site Partner
        if(varLoc.ERP_Site_Partner_Code__c != null && mapERPSitePartner_Account.containskey(varLoc.ERP_Site_Partner_Code__c) &&  mapERPSitePartner_Account.get(varLoc.ERP_Site_Partner_Code__c) != null)
        {
            varLoc.SVMXC__Account__c = mapERPSitePartner_Account.get(varLoc.ERP_Site_Partner_Code__c).ID;
            //functionalLocation = mapERPSitePartner_Account.get(varLoc.ERP_Site_Partner_Code__c).Functional_Location__c;
            System.debug('about to set value of account from erp site partner code ' + varLoc.SVMXC__Account__c);
        }
        //update account on location with ERP Sold To Code
        
        /* Commented by Naren for ENHC0010587
        else if(varLoc.ERP_Sold_To_Code__c != null && mapERPSoldTo_Account.containskey(varLoc.ERP_Sold_To_Code__c) &&  mapERPSoldTo_Account.get(varLoc.ERP_Sold_To_Code__c) != null)
        {
            varLoc.SVMXC__Account__c = mapERPSoldTo_Account.get(varLoc.ERP_Sold_To_Code__c).ID;
            //functionalLocation = mapERPSoldTo_Account.get(varLoc.ERP_Sold_To_Code__c).Functional_Location__c;
            System.debug('about to set value of account from erp sold to code ' + varLoc.SVMXC__Account__c);
        }        
        Commented by Naren for ENHC0010587 */
        
        else
        {
            if(!test.isRunningTest())varLoc.SVMXC__Account__c = null;
        }
        
        string functionalLocation; //DE4164
        
        if(varLoc.ERP_Functional_Location__c != null && (trigger.oldmap == null || (trigger.oldmap != null && (trigger.oldmap.get(varLoc.ID).ERP_Functional_Location__c == null || trigger.oldmap.get(varLoc.ID).ERP_Functional_Location__c != varLoc.ERP_Functional_Location__c)))) //DE4164
        {
            functionalLocation = varLoc.ERP_Functional_Location__c;
            functionalLocation = functionalLocation.replaceAll('(\\s+)', ' ');
            string stringURL = EncodingUtil.urlEncode(functionalLocation, 'UTF-8');
            system.debug('jsgchdb : ' + functionalLocation + ' : jdhsbsdj : ' + stringURL);
                        
            varLoc.Smart_Alert_Link__c = label.SR_Location_Smart_Alert_Link + stringURL;
            system.debug('jdghskjdhjksd: ' + varLoc.Smart_Alert_Link__c);
        }
        //update account on location with ERP Partner Association.Partner Code
        
        System.debug('final value of account is ==== ' + varLoc.SVMXC__Account__c);

        //code for DE1777, populate Service Engineer from Technician's Work Center
        if(varLoc.SVMXC__Location_Type__c == 'Field' && varLoc.Work_Center__c != null && (trigger.oldMap == null || trigger.oldMap.get(varLoc.Id).Work_Center__c == null || trigger.oldMap.get(varLoc.Id).Work_Center__c != varLoc.Work_Center__c) && mapWorkCenter_TechUser.containsKey(varLoc.Work_Center__c))
        {
            varLoc.SVMXC__Service_Engineer__c = mapWorkCenter_TechUser.get(varLoc.Work_Center__c);
        }
        //Code added by Nikita for US3124, 7/24/2014, update country on location
        if(varLoc.SVMXC__Country__c != null && mapCountryName_ID.containsKey(varLoc.SVMXC__Country__c) && mapCountryName_ID.get(varLoc.SVMXC__Country__c) != null) // DE1500
        {
            varLoc.Country__c = mapCountryName_ID.get(varLoc.SVMXC__Country__c);
        }
        //Code added by Priti for DE833, 08/01/2014, update country on location
        if(varLoc.ERP_Country_Code__c != null && mapOfConSAPCodeAndConId.containsKey(varLoc.ERP_Country_Code__c) && mapOfConSAPCodeAndConId.get(varLoc.ERP_Country_Code__c) != null) //DE1500
        {
            varLoc.Country__c = mapOfConSAPCodeAndConId.get(varLoc.ERP_Country_Code__c); 
        }
        //update erp org on location
        if (varLoc.Sales_Org__c != null && mapOfERPorgs.containsKey(varLoc.Sales_Org__c) && mapOfERPorgs.get(varLoc.Sales_Org__c) != null)
        {
            varLoc.ERP_Org__c = mapOfERPorgs.get(varLoc.Sales_Org__c).id;
        }
        /* //update account details on location
        if(varLoc.SVMXC__Account__c != null && MapofAccount.containsKey(varLoc.SVMXC__Account__c) && MapofAccount.get(varLoc.SVMXC__Account__c) != null)
        {
            Account acc = MapofAccount.get(varLoc.SVMXC__Account__c);
            if(acc != null)
            {
                if(acc.ShippingCity != Null && acc.ShippingStreet != Null && acc.ShippingState != Null && acc.ShippingPostalCode != Null && acc.ShippingCountry != Null)
                {
                    varLoc.SVMXC__City__c = acc.ShippingCity;
                    varLoc.SVMXC__Country__c = acc.ShippingCountry;
                    varLoc.SVMXC__State__c = acc.ShippingState;
                    varLoc.SVMXC__Street__c = acc.ShippingStreet;
                    varLoc.SVMXC__Zip__c = acc.ShippingPostalCode;
                }
                if(acc.ShippingCity == Null && acc.ShippingStreet == Null && acc.ShippingState == Null && acc.ShippingPostalCode == Null && acc.ShippingCountry == Null && (acc.BillingCity != Null && acc.BillingStreet != Null && acc.BillingState != Null && acc.BillingPostalCode != Null && acc.BillingCountry != Null))
                {
                    varLoc.SVMXC__City__c = acc.BillingCity;
                    varLoc.SVMXC__Country__c = acc.BillingCountry;
                    varLoc.SVMXC__State__c = acc.BillingState;
                    varLoc.SVMXC__Street__c = acc.BillingStreet;
                    varLoc.SVMXC__Zip__c = acc.BillingPostalCode;
                }
                else if((acc.ShippingCity == Null && acc.ShippingStreet == Null && acc.ShippingState == Null && acc.ShippingPostalCode == Null && acc.ShippingCountry == Null) && (acc.BillingCity == Null && acc.BillingStreet == Null && acc.BillingState == Null && acc.BillingPostalCode == Null && acc.BillingCountry == Null) )
                {
                    varLoc.SVMXC__City__c = '';
                    varLoc.SVMXC__Country__c = '';
                    varLoc.SVMXC__State__c = '';
                    varLoc.SVMXC__Street__c = '';
                    varLoc.SVMXC__Zip__c = '';
                }
            }
        }*/
    } //Kaushiki Verma for US4683 on 9/07/14 end

    //cdelfattore@forefront - 11/11/2015 (mm/dd/yyyy) - DE6617
    //In the before trigger on Location, if the Service Engineer (lookup to user) is set or reset or Techician lookup
    //is not set yet, set Technician[User=this Location.Service Engineer] (SVMXC__Service_Engineer__c)

    //first need to get the user's related to the service engineer
    Set<Id> userIdSet = new Set<Id>();
    for(SVMXC__Site__c site : Trigger.new){
        //userIdSet.add(site.SVMXC__Service_Engineer__c); //DE7081
        if (site.SVMXC__Location_Type__c =='Field' && site.SVMXC__Service_Engineer__c != null &&
            ((trigger.oldmap == null && site.Technician__c == null)  || trigger.oldmap != null && (site.Technician__c == null || trigger.oldmap.get(site.id).SVMXC__service_Engineer__c == null ||  trigger.oldmap.get(site.id).SVMXC__service_Engineer__c != site.SVMXC__Service_Engineer__c )))  //DFCT0011585
                            userIdSet.add(site.SVMXC__Service_Engineer__c); //DE7370


    }

    Map<Id,Id> userIdTechIdMap = new Map<Id,Id>();
    if (userIdSet.size()>0) //DE7370         
    for(SVMXC__Service_Group_Members__c tech : [SELECT Id, User__c,  recordtype.name FROM SVMXC__Service_Group_Members__c WHERE  recordtype.name = 'Technician' AND  User__c IN :userIdSet]){
        userIdTechIdMap.put(tech.User__c, tech.Id);
    }

   /* for(SVMXC__Site__c site : Trigger.new){
        if(site.Technician__c == null || trigger.oldMap.get(site.Id).SVMXC__Service_Engineer__c != trigger.newMap.get(site.Id).SVMXC__Service_Engineer__c){
            site.Technician__c = userIdTechIdMap.get(site.SVMXC__Service_Engineer__c);
        }
    }   */
     for(SVMXC__Site__c site : Trigger.new){
        if(site.SVMXC__Location_Type__c != null && site.SVMXC__Location_Type__c =='Field' && site.SVMXC__Service_Engineer__c != null && userIdTechIdMap.get(site.SVMXC__Service_Engineer__c) != null && ( trigger.oldMap == null || site.Technician__c == null || trigger.oldMap.get(site.Id).SVMXC__Service_Engineer__c == null || trigger.oldMap.get(site.Id).SVMXC__Service_Engineer__c != trigger.newMap.get(site.Id).SVMXC__Service_Engineer__c) ){ //DFCT0011585

            site.Technician__c = userIdTechIdMap.get(site.SVMXC__Service_Engineer__c);
        }
    }   

    LocationTriggerHandler.updateFEServiceTeam(trigger.new,trigger.oldmap);

}
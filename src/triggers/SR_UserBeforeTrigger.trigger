/*************************************************************************\      
@ Author                : Shubham Jaiswal     
@ Date                  : 25/03/2014      
@ Description           : US4286       
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
11/16/2017 - Rakesh - STSK0013241 - Update Regional Manager field on Service Team. 
/*************************************************************************/
trigger SR_UserBeforeTrigger on User (before insert, before update) 
{
     if(trigger.isupdate)
        AccountDistrictSalesManagerUpdate.updateAccountFields(trigger.new,trigger.oldmap);
        AccountDistrictServiceManagerUpdate.updateAccountFields(trigger.new,trigger.oldmap);
        AccountSoftwareSalesManagerUpdate.updateAccountFields(trigger.new,trigger.oldmap);    
    //STSK0013241.Start
   // if(trigger.isupdate){
   //     RegionalManagerUtility.UpdateTechnicianRM(trigger.new,trigger.oldmap);
   // }
    //STSK0013241.End
    if(system.Label.User_Triggers_Flag == 'True')
    {
        List<User> uslst = new List<User>();
        Set<String> ErpCountry = new Set<String>();
        
        set<Id> conId = new set<Id>();
        for(User us : trigger.new)
        {
            if(trigger.isInsert || (Trigger.isUpdate && trigger.oldmap.get(us.Id).LMS_Status__c != us.LMS_Status__c)){
               if(us.ContactId <> null){
                    conId.add(us.ContactId);
                }
            }
        }
        
        map<Id,Contact> mapContact = new map<Id,Contact>();
        if(conId.size() > 0){
            for(Contact c : [select Id,LMS_Account_Status__c from Contact where Id IN: conId ]){
                mapContact.put(c.Id,c);
            }
        }
        
        
        List<Contact> lstContact = new List<Contact>();
        //Rakesh.5/4/2016.Update 'LMS Account Status' on Contact.End
        
        for(User us : trigger.new)
        {
            if(us.ERP_Country__c != Null)
            {
                uslst.add(us);
                ErpCountry.add(us.ERP_Country__c);
            }
            // capture user lms activated date
            if(trigger.isInsert || (Trigger.isUpdate && trigger.oldmap.get(us.Id).LMS_Status__c != us.LMS_Status__c)){
                
                //Rakesh.5/4/2016.Update 'LMS Account Status' on Contact.Start
                if(us.ContactId <> null && mapContact.ContainsKey(us.ContactId)){
                    Contact con = mapContact.get(us.ContactId);
                    con.LMS_Account_Status__c = us.LMS_Status__c;
                    lstContact.add(con);
                }
                //Rakesh.5/4/2016.Update 'LMS Account Status' on Contact.End
                
                if(us.LMS_Status__c == 'Active')
                    us.LMS_Activated_Date__c = system.now();
            }
            
        }
        
        //Rakesh.5/4/2016.Update 'LMS Account Status' on Contact.Start
        try{
            update lstContact;
        }catch(Exception e){system.debug(e);}
        //Rakesh.5/4/2016.Update 'LMS Account Status' on Contact.End
        
        SR_ClassUser obj = new SR_ClassUser();
        if(uslst.size() > 0 && ErpCountry.size() > 0)
        {
            obj.PopulateCountry(uslst,ErpCountry);
        }
        
   
   
   
        //Ranjan 12/01/2015
        if(Trigger.IsInsert)
        {
            list<id> contlist = new list<id>();
            for(User u :trigger.new)
            {
                if(u.contactid!=NULL)
                {
                    contlist.add(u.contactid);
                    
                }
            }
            System.debug('@@@@@@@contlist'+contlist);
            map<Id,Contact> RelatedContacts = new map<Id,Contact>([SELECT Id, Name , Need_LMS_Access__c FROM Contact WHERE Id IN :contlist]);  
            System.debug('@@@@@@@RelatedContacts'+RelatedContacts);
             
            for (User u : Trigger.New)
            {
                if (RelatedContacts.containsKey(u.contactid) && RelatedContacts.get(u.contactid).Need_LMS_Access__c )
                {   
                    u.LMS_Status__c = 'Request';
                    u.LMS_Registration_Date__c = Datetime.now();
                }
                System.debug('@@@@@@@user'+u.LMS_Status__c);
                System.debug('@@@@@@@user'+u.LMS_Registration_Date__c);
           
            }
        }  
    } 
}
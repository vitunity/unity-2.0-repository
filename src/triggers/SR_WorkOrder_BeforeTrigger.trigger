/*************************************************************************\
    @ Last Modified By      : Nikita Gupta
    @ Last Modified On      : 28-Mar-2014
    @ Last Modified Reason  : US3307- To update Technician Email address on WO
    @ Last Modified By      : Shubham Jaiswal
    @ Last Modified On      : 3-Apr-2014
    @ Last Modified Reason  : US4277- To update fields on WO
    @ Last Modified On      : 21-Apr-2014 
    @ Last Modified Reason  : US4463, populate Technician Country field
    @ Last Modified Reason  : US4277- To update fields on WO
    @ Last Modified By      : Nikita Gupta
    @ Last Modified On      : 9-may-2014 
    @ Last Modified Reason  : US4051,to populate recordtype name in Work Type field
    @ Last Modified By      : Kaushiki 09/06/14 Null checks added
    @ Last Modified By      : Nikita Gupta   
    @ Last Modified on      : 6/24/2014
    @ Last Modified Reason  : Moved method 'prePopulatePrefferedTechnician on top and changed it from static to standard method call
                              US4516, to populate WO field Problem Description with Case field Case Activity.
    @ Last Modified By      : Parul Gupta   
    @ Last Modified on      : 01/07/2014
    @ Last Modified Reason  : US4119
    @ Last Modified By      : Nikita Gupta   
    @ Last Modified on      : 7/15/2014
    @ Last Modified Reason  : US4511, to update 'No Test Equipment Used' flag on WO.
    @ Last Modified By      : Priti Jaiswal   
    @ Last Modified on      : 7/31/2014
    @ Last Modified Reason  : US4562 - cancel related checklist when IWO is cancelled
    @ Last Modified By      : Parul Gupta   
    @ Last Modified on      : 8/18/2014
    @ Last Modified Reason  : Modified trigger condition to update Technician from Preferred Technician.
***************************************************************************
    Last Modified By Amitkumar
    Description : Commented code removed. (if needed will refer from bit-bucket repository)

*/
trigger SR_WorkOrder_BeforeTrigger on SVMXC__Service_Order__c (Before update, Before Insert) {
    //DFCT0012096.start
    ValidationInventoryLocation.checkValidLocation(trigger.new,trigger.oldmap);
    //DFCT0012096.End
    SR_Class_WorkOrder objWO = new SR_Class_WorkOrder(); //initiating object for class Work Order.
    if(Trigger.isInsert || Trigger.IsUpdate) {
        //DFCT0012091 : Component must have Location = Work Order.Site
        WorkOrderPSValidator.componentLocValidator(Trigger.newMap,Trigger.oldMap);
        objWO.updateWorkOrderFields(trigger.new, Trigger.oldMap,Trigger.newMap);  //DM:  ONLY METHOD TO BE USED
        try{
            WorkOrderDateFieldsHandler dateObj = new WorkOrderDateFieldsHandler();
            dateObj.exceute(trigger.new, Trigger.oldMap,Trigger.newMap); 
            //DFCT0012231 : DE8269:  OJT Work Orders Get Stuck at Submit/Null 
            WorkOrderOJTHandler ojtObj = new WorkOrderOJTHandler ();
            ojtObj.exceute(trigger.new, Trigger.oldMap,Trigger.newMap);
            
        }catch(Exception e){
            system.debug('Work order before trigger exception :'+e);
                throw new WorkOrderProcessException('Error Message::'+e.getMessage()+'::Code Line number::'+e.getLineNumber());
        }        
    }
}
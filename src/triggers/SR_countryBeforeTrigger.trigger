trigger SR_countryBeforeTrigger on Country__c (Before insert,Before Update) 
{
    Set<String> DefPrimryLngSet = new set<String>();
    List<Country__c> CountryList = new List<Country__c>();
    Map<string,id> MapLanguage = new Map<string,id>();
    for(Country__c VarC : trigger.new)
    {
        If(trigger.OldMap == null)
        {
            if(VarC.Default_Primary_Language__c!=null)
            {
                DefPrimryLngSet.add(VarC.Default_Primary_Language__c);
                CountryList.add(VarC);
            }
        }
        if(trigger.OldMap!= null)
        {
            if(trigger.OldMap.get(VarC.id).Default_Primary_Language__c != VarC.Default_Primary_Language__c && VarC.Default_Primary_Language__c!=null)
            {
                DefPrimryLngSet.add(VarC.Default_Primary_Language__c);
                CountryList.add(VarC);
            }
        }
    }
    if(DefPrimryLngSet.size()>0)
    {
        for(Language__c VarLang : [Select id,name from Language__c where name in:DefPrimryLngSet ] )
        {
            MapLanguage.put(VarLang.name,VarLang.id);
        }
    }
    for(Country__c varc : CountryList)
    {
        if(MapLanguage.containsKey(varc.Default_Primary_Language__c))
        {
            if(MapLanguage.get(varc.Default_Primary_Language__c)!=null)
            {
                varc.Language__c = MapLanguage.get(varc.Default_Primary_Language__c);
            }
        }
    }

}
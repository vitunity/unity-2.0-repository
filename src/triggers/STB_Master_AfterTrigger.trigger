/******************************************************************
Last Modified By: Parul Gupta
Last Modified Date: 4/08/2014
Last Modified Reason: Modification in US3553.
******************************************************************/

trigger STB_Master_AfterTrigger on STB_Master__c (After update,after insert) 
{
    Set<Id> setSTBMasterId = new Set<Id>();
    Set<String> setAllPcodes = new Set<String>();
    Set<String> setOpenPcodes = new Set<String>();
    Set<String> setNotOpenCodes = new Set<String>();


    Map<String, STB_Pcode__c> mapMasterERPpcode_STBpcode = new Map<String, STB_Pcode__c>();
    Map<String, String> prodCodetoIdMap = new Map<String, String>();
    Map<String,ERP_Pcode__c > mapERPpcode2_ERPpcode = new  Map<String,ERP_Pcode__c >();
    List<Optional_STB__c> createOptionalSTBList = new List<Optional_STB__c>();
    List<Optional_STB__c> deleteOptionSTBList = new List<Optional_STB__c>();
    List<STB_Pcode__c> listInsertSTBpcode = new List<STB_Pcode__c>();
    //Set<String> PCode_Set = new Set<String>();
    //List<STB_Pcode__c> STBpcodeList = New List<STB_Pcode__c>();   //Map<String,Id> MapPCode2ERPPcode = new  Map<String,Id>();

    

    //Jira Task VAR-211 Create Optional STB Records
    // DK -- 8/10
    for(STB_Master__c varMaster : Trigger.New)
    {
        //if STB Master(Optional STB) value of product code affected is set or re-set   //changed for DE3043
        if(varMaster.Type__c != null && !varMaster.Type__c.startsWith('1') && !varMaster.Type__c.startsWith('2') && varMaster.Product_Codes_Affected__c!= null && (Trigger.old == null || Trigger.oldmap.get(varMaster.id).Product_Codes_Affected__c == null || varMaster.Product_Codes_Affected__c != Trigger.oldmap.get(varMaster.id).Product_Codes_Affected__c))
        {
            setSTBMasterId.add(varMaster.Id);
            system.debug('varMaster.Product_Codes_Affected__c>>>>>>> ' + varMaster.Product_Codes_Affected__c);

            String temp = varMaster.Product_Codes_Affected__c;
            string[] all = temp.split(',');
            if(all != null)
            {
                for(integer i = 0; i < all.size(); i++)
                {
                    if(String.isNotBlank(all[i]))
                    {
                        setAllPcodes.add(all[i]);
                        if(varMaster.is_Open__c == True){
                            setOpenPcodes.add(all[i]);
                            if(prodCodetoIdMap.get(all[i]) == null && String.isNotBlank(varMaster.Id)){
                                prodCodetoIdMap.put(all[i],varMaster.Id);
                            }
                        }else if(varMaster.is_Open__c == False){
                            setNotOpenCodes.add(all[i]);
                        }
                    }
                }
            }

        }
        /*if(varMaster.Product_Codes_Affected__c!= null && varMaster.Type__c != null && !varMaster.Type__c.startsWith('1') && !varMaster.Type__c.startsWith('2')) {
            String temp = varMaster.Product_Codes_Affected__c;
            String[] all = temp.split(','); //to separate the product codes based on comma(,)
            PCode_Set.addAll(all);}*/
    }
    List <ERP_PCode__c> lep = [select id from ERP_PCode__c where ERP_Pcode_2__c in :setAllPcodes]; 
    System.debug('prodCodetoIdMap.values() ' + prodCodetoIdMap.values());
    System.debug('setNotOpenCodes ' + setNotOpenCodes);
    System.debug('setOpenPcodes ' + setOpenPcodes);
    System.debug('setOpenPcodes ' + lep);

    List<SVMXC__Installed_Product__c> installedProducts = [SELECT id, ERP_Pcode_2__c,(SELECT District__c,Due_Date__c,FE__c, Id ,Org__c,
                                                            PCSN__c,Site__c,Status__c,STB_Master__c FROM Optional_STBs__r WHERE STB_Master__c 
                                                            IN: prodCodetoIdMap.values()),(SELECT CurrencyIsoCode,Date_Due__c,
                                                            Installed_Product__c,Location__c,Name,PCSN__c,Status__c,STB_Master__c FROM STB_Status__r)
                                                            from SVMXC__Installed_Product__c WHERE ERP_Pcodes__c in :lep]; //ERP_Pcodes__c in :lep //ERP_Pcodes__r.ERP_Pcode_2__c IN: setAllPcodes IN: setAllPcodes
                                                        //     OR ERP_Pcodes__r.ERP_PCode_2__c 
                                                           // IN: setNotOpenCodes


    if(installedProducts.size() > 0 ) {
        for(SVMXC__Installed_Product__c ip :  installedProducts){
            if (setOpenPcodes.contains(ip.ERP_PCode_2__c) && ip.STB_Status__r.size() == 0){
                Optional_STB__c createSTB = new Optional_STB__c(); 
                createSTB.STB_Master__c = prodCodetoIdMap.get(ip.ERP_PCode_2__c);
                //createSTB.Due_Date__c = ip.STB_Status__r.Date_Due__c;
                //createSTB.PCSN__c = ip.STB_Status__r.PCSN__c;
                //createSTB.Site__c = ip.STB_Status__r.Location__c;
                //createSTB.Status__c = ip.STB_Status__r.Status__c;
                createOptionalSTBList.add(createSTB);
            }
            else if(setNotOpenCodes.contains(ip.ERP_PCode_2__c) && ip.Optional_STBs__r.size() >0){
                deleteOptionSTBList.add(ip.Optional_STBs__r);
            }
        }
    }
    
    if(createOptionalSTBList.size() >0){
        try{
            insert createOptionalSTBList;
        }
        catch(DmlException e){
             System.debug('The following exception has occurred during Optional STB insertion: ' + e.getMessage()); 
        }
    }

    if(deleteOptionSTBList.size() > 0){
        try{
            delete deleteOptionSTBList;
        }
        catch(DmlException e){
            System.debug('The following exception has occurred during Optional STB delete: ' + e.getMessage());
        }
    }


    if(setSTBMasterId.size() > 0 && setAllPcodes.size() > 0)    //changed for DE3043
    {
        system.debug('setSTBMasterId >>> ' + setSTBMasterId + ' -- ' + setAllPcodes);
        for(STB_Pcode__c STBpcode : [select id, STB__c, STB__r.name, ERP_Pcode__c, ERP_Pcode_2__c from STB_Pcode__c where STB__c in :setSTBMasterId
                                        AND ERP_Pcode_2__c in :setAllPcodes])
        {
            if(STBpcode.STB__c != null && STBpcode.ERP_Pcode_2__c != null)
            {
                string strKey = string.valueOf(STBpcode.STB__r.Name) + string.valueOf(STBpcode.ERP_Pcode_2__c);
                mapMasterERPpcode_STBpcode.put(strKey, STBpcode);
                system.debug('mapMasterERPpcode_STBpcode >>> ' + strKey + ' -- ' + mapMasterERPpcode_STBpcode.size());
            }
        }
    }

    if(setAllPcodes.size() > 0) //changed for DE3043
    {
        for(ERP_Pcode__c varPcode : [select id, name, ERP_Pcode_2__c from ERP_Pcode__c where ERP_Pcode_2__c in :setAllPcodes])
        {
            mapERPpcode2_ERPpcode.put(varPcode.ERP_Pcode_2__c, varPcode);
            system.debug('mapERPpcode2_ERPpcode >>> ' + varPcode.ERP_Pcode_2__c + ' -- ' + mapERPpcode2_ERPpcode.size());
        }
    }
    /*if(PCode_Set.size() > 0 ) {
        List<ERP_PCode__c> ERP_Pcodes = [select ID, Name, ERP_PCode_2__c from ERP_PCode__c where ERP_PCode_2__c in :PCode_Set];
        if(ERP_Pcodes.size() > 0) {
            for(ERP_PCode__c Pcode: ERP_Pcodes)
                MapPCode2ERPPcode.put(Pcode.ERP_PCode_2__c, Pcode.id); }
        /*ProductList = [select ID, Name, ERP_PCode_2__c from Product2 where ERP_PCode_2__c in :PCode_Set];
        for(Product2 Prod: ProductList) { list<Product2> listTempProduct = new list<Product2>();
            if(ProductMap.containsKey(Prod.ERP_PCode_2__c)) { listTempProduct = ProductMap.get(Prod.ERP_PCode_2__c); }
            listTempProduct.add(Prod); ProductMap.put(Prod.ERP_PCode_2__c, listTempProduct); } }*/

    for(STB_Master__c varMaster : trigger.new)
    {
        if(varMaster.Product_Codes_Affected__c!= null && (Trigger.old == null || Trigger.oldmap.get(varMaster.Id).Product_Codes_Affected__c == null || varMaster.Product_Codes_Affected__c != Trigger.oldmap.get(varMaster.id).Product_Codes_Affected__c))//to check if STB Master record has a value for product code
        {
            String temp = varMaster.Product_Codes_Affected__c;
            String[] all = temp.split(','); //to separate the product codes based on comma(,)
            System.debug('<<<<ALL>>>>>'+ all);

            system.debug('allallall >>> ' + all);
            if(all != null)
            {
                System.debug(' all not null @@@ ' + all);
                //for(integer i = 0; i < all.size(); i++)
                //{
                    for(String strPcode : all)
                    {
                        string strKey = varMaster.name + strPcode;
                        if(mapMasterERPpcode_STBpcode.get(strKey) == null)
                        {
                            STB_Pcode__c varPcode = new STB_Pcode__c();
                            varPcode.STB__c = varMaster.ID;  
                            varPcode.ERP_Pcode_2__c = strPcode;

                            if(mapERPpcode2_ERPpcode.containsKey(strPcode) && mapERPpcode2_ERPpcode.get(strPcode) != null)
                            {
                                varPcode.ERP_Pcode__c = mapERPpcode2_ERPpcode.get(strPcode).Id;
                            }
                            listInsertSTBpcode.add(varPcode);
                            system.debug('listInsertSTBpcode >>> ' + listInsertSTBpcode);
                        }
                    }
                    /*if(MapPCode2ERPPcode.containskey(all[i])) {
                    //for(Product2 varProd : MapPCode2ERPPcode.get(all[i])){
                    STB_Pcode__c stbPcode = new STB_Pcode__c();
                    stbPcode.STB__c = varMaster.ID;   
                    stbPcode.ERP_Pcode_2__c = all[i];  
                    stbPcode.ERP_Pcode__c = MapPCode2ERPPcode.get(all[i]);
                    STBpcodeList.add(stbPcode);}*/
                    /*if(ProductMap.containskey(all[i])){
                    for(Product2 varProd : ProductMap.get(all[i])) { stbProd = new STB_Pcode__c(); stbProd.STB__c = varMaster.ID;   
                    stbProd.Product__c = varProd.ID; STBprdList.add(stbProd); } } */
                //}
            }
        }
    }
    if(listInsertSTBpcode.size() > 0)
    {
        system.debug('about to insert record of stb pcode >>> ' + listInsertSTBpcode);
        insert listInsertSTBpcode;
    }
    
    
}
trigger STB_Mod_BeforeTrigger on STB_Mod__c (before update, before Insert) 
{
    Set<String> PCSNset = new Set<String>();// Set to save the value of PCSN extracted from STB Mod
    Set<String> STBset = new Set<String>();// Set to save the value of PCSN extracted from STB Mod
    Map<String,SVMXC__Installed_Product__c> IPmap = new Map<String,SVMXC__Installed_Product__c>();//map to save Installed Product details corresponding to it's PCSN.
    Map<String,STB_Master__c> STBmap = new Map<String,STB_Master__c>();//map to save STB Master details corresponding to it's STB Name.
    List<SVMXC__Installed_Product__c> IPlist=new List<SVMXC__Installed_Product__c>();// List to save Installed Product Details for creating IP map.
    List<STB_Master__c> STBlist = new List<STB_Master__c>();//List to save STB Master details for creating STB map.

    for(STB_Mod__c Mod : Trigger.New)
    {
        Mod.Mod_Status_Key__c = Mod.Installed_Product__c+''+Mod.STB_Master__c+''+Mod.Mod_Status_Text__c;
        system.debug('Mod.PCSN__c>>>>>>>'+Mod.PCSN__c);// to check if PCSN is present on STB Mod.
        if(Mod.PCSN__c != NULL)
        {
            PCSNset.add(Mod.PCSN__c); //extracting PCSN from STB Mod
        }
        system.debug('Mod.Name>>>>>>'+Mod.Name); // To check if STB Name is present on STB Mod.
        STBset.add(Mod.Name); //extracting STB Name from STB Mod
        system.debug('Mod.PCSN__c>>>>>>>'+PCSNset);
        system.debug('Mod.Name>>>>>>>'+STBset);
    }
    
    if(PCSNset.size() > 0 )//if PCSN of STB Mod is not blank
    {
        IPlist=[select ID, Name, SVMXC__Serial_Lot_Number__c from SVMXC__Installed_Product__c where Name in :PCSNset];
        //IPmap = new Map<String,SVMXC__Installed_Product__c>([select ID, Name, SVMXC__Serial_Lot_Number__c from SVMXC__Installed_Product__c where Name in :PCSNset]);
        
        for(SVMXC__Installed_Product__c Ip_obj : IPlist)
        {
            IPmap.put(Ip_obj.Name,Ip_obj);
            system.debug('****************'+IPmap);
        }
    }
    
    if(STBset.size() > 0 )//if STB Name of STB Mod is not blank
    {
        STBlist=[select ID, Name from STB_Master__c where Name in : STBset];
        for(STB_Master__c STB_obj : STBlist)
        {
            STBmap.put(STB_obj.Name,STB_obj);
            system.debug('****************'+STBmap);
        }
    }
 
            
    for(STB_Mod__c Mod : Trigger.New)
    {
        if(Mod.PCSN__c!=null && IPmap.get(Mod.PCSN__c)!=null)
        {
            System.debug('Get IPmap Value???????????' +IPmap.get(Mod.PCSN__c).ID);
            System.debug('Get IPmap Value???????????' +IPmap.get(Mod.PCSN__c));
            Mod.Installed_Product__c = IPmap.get(Mod.PCSN__c).ID;
        }
    
        if(Mod.Name!=null && STBmap.get(Mod.Name)!=null)
        {
            System.debug('Get STBmap Value???????????' +STBmap.get(Mod.Name).ID);
            System.debug('Get STBmap Value???????????' +STBmap.get(Mod.Name));
            Mod.STB_Master__c = STBmap.get(Mod.Name).ID;
        }
    }
}
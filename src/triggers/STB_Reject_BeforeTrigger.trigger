trigger STB_Reject_BeforeTrigger on STB_Reject__c (before update,before insert) 
{
    Set<String> PCSNset = new Set<String>();// Set to save the value of PCSN extracted from STB Reject
    Set<String> STBset = new Set<String>();// Set to save the value of PCSN extracted from STB Reject
    Set<String> WDnameSet = new Set<String>();// Set to save the value of WD extracted from STB Reject
    Map<String,SVMXC__Installed_Product__c> IPmap = new Map<String,SVMXC__Installed_Product__c>();//map to save Installed Product details corresponding to it's PCSN.
    Map<String,STB_Master__c> STBmap = new Map<String,STB_Master__c>();//map to save STB Master details corresponding to it's STB Name.
    Map<String,SVMXC__Service_Order_Line__c> WDmap = new Map<String,SVMXC__Service_Order_Line__c>();//map to save Work Detail details corresponding to it's WD Name.
    List<ID> IP_ID_List = new List<ID>(); //List to store the IP lookup IDs from STB Reject.
    List<ID> STB_MasterID_List = new List<ID>(); //List to store the STB Master lookup IDs from STB Reject.
    List<ID> WorkDetailID_List = new List<ID>(); //List to store the WD lookup IDs from STB Reject.
    Map<ID,STB_Mod__c> IP_STBmod_Map = new Map<ID,STB_Mod__c>(); //map to store STB Mod details against IP.
    Map<ID,STB_Mod__c> STB_STBmod_Map = new Map<ID,STB_Mod__c>(); //map to store STB Mod details against STB Master.
    Map<ID,STB_Mod__c> WD_STBmod_Map = new Map<ID,STB_Mod__c>(); //map to store STB Mod details against WD.
    Map<Date,ID> LastModDate_STB_Mod_Map = new Map<Date,ID>(); //Map to store Last Modified Date against STB Mod ID.

    for(STB_Reject__c Reject : Trigger.New)
    {
        system.debug('Reject.PCSN__c>>>>>>>'+Reject.PCSN__c);
        if(Reject.PCSN__c != NULL)
        {
            PCSNset.add(Reject.PCSN__c); //extracting PCSN from STB Reject
        }
        system.debug('Reject.STB_Name__c>>>>>>>'+Reject.STB_Name__c);
        if(Reject.STB_Name__c != NULL)
        {
            STBset.add(Reject.STB_Name__c); //extracting STB Name from STB Reject
        }
        system.debug('Reject.Work_Detail_Name__c>>>>>>>'+Reject.Work_Detail_Name__c);
        if(Reject.Work_Detail_Name__c != NULL)
        {
            WDnameSet.add(Reject.Work_Detail_Name__c); //extracting WD Name from STB Reject
        }
        system.debug('Reject.PCSN__c>>>>>>>'+PCSNset);
        system.debug('Reject.STB_Name__c>>>>>>>'+STBset);
        system.debug('Reject.Work_Detail_Name__c>>>>>>>'+WDnameSet);
    }
    
    if(PCSNset.size() > 0 )//if PCSN of STB Reject is not blank
    {
        List<SVMXC__Installed_Product__c> IPlist = [select ID, Name from SVMXC__Installed_Product__c where Name IN :PCSNset];
        for(SVMXC__Installed_Product__c Ip_obj : IPlist)    //List to save Installed Product Details for creating IP map
        {
            IPmap.put(Ip_obj.Name, Ip_obj);
            system.debug('****************'+IPmap);
        }
    }
    
    if(STBset.size() > 0 )//if STB Name of STB Reject is not blank
    {
        List<STB_Master__c> STBlist = [select ID, Name from STB_Master__c where Name in : STBset];  //List to save STB Master details for creating STB map
        for(STB_Master__c STB_obj : STBlist)
        {
            STBmap.put(STB_obj.Name,STB_obj);
            system.debug('****************'+STBmap);
        }
    }
    
    if(WDnameSet.size() > 0 )//if WD Name of STB Reject is not blank
    {
        List<SVMXC__Service_Order_Line__c> WDlist = [select ID, Name from SVMXC__Service_Order_Line__c where Name in :WDnameSet];
        for(SVMXC__Service_Order_Line__c WD_obj : WDlist)   //List to save STB Master details for creating STB map
        {
            WDmap.put(WD_obj.Name,WD_obj);
            system.debug('****************'+WDmap);
        }
    }
 
            
    for(STB_Reject__c Reject : Trigger.New)
    {
        if(Reject.PCSN__c!=null && IPmap.get(Reject.PCSN__c)!=null)
        {
            System.debug('Get IPmap Value???????????' +IPmap.get(Reject.PCSN__c).ID);
            System.debug('Get IPmap Value???????????' +IPmap.get(Reject.PCSN__c));
            Reject.Installed_Product__c = IPmap.get(Reject.PCSN__c).ID;
        }
    
        if(Reject.STB_Name__c!=null && STBmap.get(Reject.STB_Name__c)!=null)
        {
            System.debug('Get STBmap Value???????????' +STBmap.get(Reject.STB_Name__c).ID);
            System.debug('Get STBmap Value???????????' +STBmap.get(Reject.STB_Name__c));
            Reject.STB_Master__c = STBmap.get(Reject.STB_Name__c).ID;
        }
        
        if(Reject.Work_Detail_Name__c!=null && WDmap.get(Reject.Work_Detail_Name__c)!=null)
        {
            System.debug('Get WDmap Value???????????' +WDmap.get(Reject.Work_Detail_Name__c).ID);
            System.debug('Get WDmap Value???????????' +WDmap.get(Reject.Work_Detail_Name__c));
            Reject.Work_Detail__c = WDmap.get(Reject.Work_Detail_Name__c).ID;
        }
        
        IP_ID_List.add(Reject.Installed_Product__c);
        STB_MasterID_List.add(Reject.STB_Master__c);
        WorkDetailID_List.add(Reject.Work_Detail__c);
    }
    
    if(IP_ID_List.size() > 0 && STB_MasterID_List.size() > 0 && WorkDetailID_List.size() >0)
    {
        List<STB_Mod__c> STB_Mod_List = [Select ID, LastModifiedDate, Installed_Product__c, STB_Master__c, Work_Details__c from STB_Mod__c where
                                        Installed_Product__c in: IP_ID_List and STB_Master__c in: STB_MasterID_List and Work_Details__c in: WorkDetailID_List];
        for(STB_Mod__c Mod : STB_Mod_List)  //List to store the STB Mod details based on filter from STB reject details
        {
            IP_STBmod_Map.put(Mod.Installed_Product__c, Mod);
            STB_STBmod_Map.put(Mod.STB_Master__c, Mod);
            WD_STBmod_Map.put(Mod.Work_Details__c, Mod);
            LastModDate_STB_Mod_Map.put(date.newinstance(Mod.LastModifiedDate.year(),Mod.LastModifiedDate.month(),Mod.LastModifiedDate.day()),Mod.ID);
            System.debug('IP_STBmod_Map>>>>>>>>>>>>>>>'+IP_STBmod_Map);
            System.debug('STB_STBmod_Map>>>>>>>>>>>>>>>'+STB_STBmod_Map);
            System.debug('WD_STBmod_Map>>>>>>>>>>>>>>>'+WD_STBmod_Map);
        }
    }
    
    for(STB_Reject__c Reject : Trigger.New)
    {
        if(Reject.Installed_Product__c != null && IP_STBmod_Map.containsKey(Reject.Installed_Product__c) && IP_STBmod_Map.get(Reject.Installed_Product__c) != null)
        {
            if(Reject.STB_Master__c != null && STB_STBmod_Map.containsKey(Reject.STB_Master__c) && STB_STBmod_Map.get(Reject.STB_Master__c) != null)
            {
                if(Reject.Work_Detail__c != null && WD_STBmod_Map.containsKey(Reject.Work_Detail__c) && WD_STBmod_Map.get(Reject.Work_Detail__c) != null)
                {
                    if(Reject.Rev_Date__c != null)
                    {
                        if(date.newinstance(Reject.Rev_Date__c.year(),Reject.Rev_Date__c.month(),Reject.Rev_Date__c.day()) != null && LastModDate_STB_Mod_Map.containsKey(date.newinstance(Reject.Rev_Date__c.year(),Reject.Rev_Date__c.month(),Reject.Rev_Date__c.day())) && LastModDate_STB_Mod_Map.get(date.newinstance(Reject.Rev_Date__c.year(),Reject.Rev_Date__c.month(),Reject.Rev_Date__c.day())) != null)
                        {
                            Reject.STB_Mod__c = WD_STBmod_Map.get(Reject.Work_Detail__c).ID;
                        }   
                    }   
                }   
            }
        }
    } 
    // STSK0011461 :Regulatory: STB Reject Object:  Update Record with Fields from PSE - Interface
    // auto populate stb technician
    // if required will add more trigger conditions for work center change only
    set<String> technicianWorkCno = new set<String>();
    for(STB_Reject__c rejectObj : Trigger.New){
        if(rejectObj.Technician_Work_Center__c != null){
            technicianWorkCno.add(rejectObj.Technician_Work_Center__c);
        }
    }
    if(!technicianWorkCno.isEmpty()){
        map<String,SVMXC__Service_Group_Members__c> technicianMap = new map<String,SVMXC__Service_Group_Members__c>();
        for(SVMXC__Service_Group_Members__c techObj : [select Id, ERP_Work_Center__c, SVMXC__Email__c from SVMXC__Service_Group_Members__c 
                                                       where ERP_Work_Center__c in : technicianWorkCno]){
        	technicianMap.put(techObj.ERP_Work_Center__c,techObj);
        }
        for(STB_Reject__c rejectObj : Trigger.New){
            if(rejectObj.Technician_Work_Center__c != null && technicianMap.containsKey(rejectObj.Technician_Work_Center__c)){
                rejectObj.Technician__c = technicianMap.get(rejectObj.Technician_Work_Center__c).Id;
                rejectObj.Technician_Email__c = technicianMap.get(rejectObj.Technician_Work_Center__c).SVMXC__Email__c;
            }
    	}
    }//end STSK0011461
}
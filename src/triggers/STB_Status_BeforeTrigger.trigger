/***************************************************************
Last Modified By:       Parul Gupta
Last Modified Date:     22/07/2014
Last Modified Reason:   US4560, Update Location on STB Status
Last Modified By      : Nikita Gupta
Last Modified Date    : 25/7/2014
Last Modified Reason  : US4583, Update Location from IP on both insert and update.
****************************************************************/

trigger STB_Status_BeforeTrigger on STB_Status__c (before update,before insert) 
{
    Set<String> PCSNset = new Set<String>();// Set to save the value of PCSN extracted from STB Status
    Set<String> STBset = new Set<String>();// Set to save the value of PCSN extracted from STB Status
    Map<String,SVMXC__Installed_Product__c> IPmap = new Map<String,SVMXC__Installed_Product__c>();//map to save Installed Product details corresponding to it's PCSN.
    Map<String,STB_Master__c> STBmap = new Map<String,STB_Master__c>();//map to save STB Master details corresponding to it's STB Name.

    //Class and Method call added by Parul, 22/07/2014, US4560
    SR_STB_Status objStatus = new SR_STB_Status();  // initiated the instance of apex class.

    for(STB_Status__c Status : Trigger.New)
    {
        system.debug('Status.PCSN__c>>>>>>>'+Status.PCSN__c);
        if(Status.PCSN__c != null)        //null check added by Nikhil on 03-May-14
        {
            PCSNset.add(Status.PCSN__c); //extracting PCSN from STB Status
        }
        if(Status.STB_Name__c != null)    //null check added by Nikhil on 03-May-14
        {
            STBset.add(Status.STB_Name__c); //extracting STB Name from STB Status
        }
        system.debug('Status.PCSN__c>>>>>>> ' + PCSNset);
        system.debug('Status.STB_Name__c>>>>>>> ' + STBset);
    }

    if(PCSNset.size() > 0 )//if PCSN of STB Status is not blank
    {
        List<SVMXC__Installed_Product__c> IPlist = [select ID, Name from SVMXC__Installed_Product__c where Name in:PCSNset]; // 06/05/2014 Shubham 
        if(IPlist.size() > 0)   //List to save Installed Product Details for creating IP map
        {
            for(SVMXC__Installed_Product__c Ip_obj : IPlist)
            {
                IPmap.put(Ip_obj.Name,Ip_obj);
                system.debug('****************'+IPmap);
            }
        }
    }

    if(STBset.size() > 0 )//if STB Name of STB Status is not blank
    {
        List<STB_Master__c> STBlist = [select ID, Name from STB_Master__c where Name in : STBset];  //List to save STB Master details for creating STB map
        for(STB_Master__c STB_obj : STBlist)
        {
            STBmap.put(STB_obj.Name, STB_obj);
            system.debug('**************** ' + STBmap);
        }
    }

    for(STB_Status__c Status : Trigger.New)
    {
        if(Status.PCSN__c != null && IPmap.get(Status.PCSN__c) != null)
        {
            System.debug('Get IPmap Value??????????? ' + IPmap.get(Status.PCSN__c).ID);
            System.debug('Get IPmap Value??????????? ' + IPmap.get(Status.PCSN__c));
            Status.Installed_Product__c = IPmap.get(Status.PCSN__c).ID;
        }

        if(Status.STB_Name__c != null && STBmap.get(Status.STB_Name__c) != null)
        {
            System.debug('Get STBmap Value??????????? ' + STBmap.get(Status.STB_Name__c).ID);
            System.debug('Get STBmap Value??????????? ' + STBmap.get(Status.STB_Name__c));
            Status.STB_Master__c = STBmap.get(Status.STB_Name__c).ID;
        }
    }

    objStatus.updateFieldsOnSTBStatus(trigger.new, trigger.oldMap); //method call //oldMap argument added by Nikita, 7/25/2014, US4583
    objStatus.updateSTBJONumber(trigger.new, trigger.oldMap); //STRY0013023 : Offline: DE8164:  Map STB Org to STB Status - Required for MFL Use without Sync
}
trigger SVMXC_AfterInsertUpdateDelete_TimeEntry on SVMXC_Time_Entry__c (after delete, after insert, after update) {

        
    if (trigger.isDelete)
    {
        // ServiceMaxTimesheetUtils.updateTimesheetTotals(trigger.old); Comemnting as discussed  with Anupam
        ServiceMaxTimesheetUtils.deleteRelatedEvents(trigger.old);
        ServiceMaxTimesheetUtils.RollUpRecuperativeHoursToTimeSheet(trigger.oldMap, null, 3);
    }
    else
    {
       // ServiceMaxTimesheetUtils.updatetimesheetTotals(trigger.new);  Comemnting as discussed  with Anupam
       List<SVMXC_Time_Entry__c> updateEvents = new List<SVMXC_Time_Entry__c>();
       for (SVMXC_Time_Entry__c te : Trigger.new) {
            if (te.Create_Event__c && (Trigger.oldMap == null || Trigger.oldmap.get(te.Id).Create_Event__c == false)) {
                updateEvents.add(te);
            }
       }

       if (!updateEvents.isEmpty()) {
            ServiceMaxTimesheetUtils.updateRelatedEvents(updateEvents);

       }


        if(trigger.isInsert) {
           ServiceMaxTimesheetUtils.RollUpRecuperativeHoursToTimeSheet(null, trigger.newMap, 1);
        }
        if(trigger.isUpdate) {
           ServiceMaxTimesheetUtils.RollUpRecuperativeHoursToTimeSheet(trigger.oldMap, trigger.newMap, 2);
           ServiceMaxTimesheetUtils.interfaceStatusOnTE(trigger.new,trigger.oldmap);
        }
        //ServiceMaxTimesheetUtils.UpdateIgnoreTimeEntryForUtilization(trigger.newMap);
        if(trigger.isInsert) {
            if(ServiceMaxTimesheetUtils.runOnce()) {
                ServiceMaxTimesheetUtils obj = new ServiceMaxTimesheetUtils();
                System.debug('entering time utl 1');
                obj.UpdateMostRecentChangeToTimeSheet(null,trigger.newMap, 1);
            }
        }
        else {
            if(ServiceMaxTimesheetUtils.runOnce()) {
                Map<id,SVMXC_Time_Entry__c> newselectedtimeentry = new map<id,SVMXC_Time_Entry__c>();
                for(SVMXC_Time_Entry__c te : trigger.new)
                {
                    if(te.Start_Date_Time__c != trigger.oldmap.get(te.id).Start_Date_Time__c || te.End_Date_Time__c != trigger.oldmap.get(te.id).End_Date_Time__c)
                    {
                        newselectedtimeentry.put(te.id,te);
                    }
                }
                if(newselectedtimeentry != null && newselectedtimeentry.size() > 0)
                {
                    ServiceMaxTimesheetUtils obj = new ServiceMaxTimesheetUtils();
                    System.debug('entering time utl 2');

                    obj.UpdateMostRecentChangeToTimeSheet(trigger.oldmap, newselectedtimeentry, 2);//trigger.newMap, 2);
                }
            }
        }
    }
}
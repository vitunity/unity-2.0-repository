/*
 * Change Log -
 *     4-Apr-2018 - Nilesh Gorle - STSK0014169 - Avoid recursion
 */
trigger SVMXC_BeforeInsertUpdate_TimeEntry on SVMXC_Time_Entry__c (before insert, before update, before delete){
    if(Trigger.isInsert || Trigger.isUpdate){
        TimeEntryHandler.setOrgActivity(Trigger.new);
        TimeEntryHandler.setTimesFormats(Trigger.new, Trigger.oldmap);
        //if (ServiceMaxTimesheetUtils.trg_insert_fired == false)
        //{
        /*ServiceMaxTimesheetUtils.updateTimeEntries(trigger.new, trigger.oldMap);
        ServiceMaxTimesheetUtils.UpdateIgnoreTimeEntryForUtilization(trigger.new);*/
        ServiceMaxTimesheetUtils obj = new ServiceMaxTimesheetUtils();
        // STSK0014169 - Avoid recursion
        //if(ServiceMaxTimesheetUtils.runOnce())
            obj.updateTimeEntriesBefore(trigger.new, trigger.oldMap);
        //ServiceMaxTimesheetUtils.trg_insert_fired = true;
        //}
        TimeEntryHandler.updatePublicTimeEntry(trigger.new,trigger.oldMap);
        
        map<id,SVMXC__Service_Group_Members__c> techniciansMap; 
        set<Id> techIds = new set<Id>();
        
        for(SVMXC_Time_Entry__c te : trigger.new){
            techIds.add(te.Technician__c);
        }
        
        techniciansMap = new Map<id,SVMXC__Service_Group_Members__c>([Select id, Timecard_Profile__r.Lunch_Time__c, 
                         Timecard_Profile__r.Generate_Holiday_TE__c, Timecard_Profile__r.Max_Hours_per_Week__c,
                         Timecard_Profile__r.First_Day_of_Week__c, Timecard_Profile__r.Max_Hours_Day_1__c,
                         Timecard_Profile__r.Max_Hours_Day_2__c, Timecard_Profile__r.Max_Hours_Day_3__c, 
                         Timecard_Profile__r.Max_Hours_Day_4__c, Timecard_Profile__r.Max_Hours_Day_5__c,
                         Timecard_Profile__r.Max_Hours_Day_6__c, Timecard_Profile__r.Max_Hours_Day_7__c, 
                         SVMXC__Service_Group__c ,Timecard_Profile__c,Timecard_Profile__r.Name, User__r.TimeZoneSidKey,
                         Timecard_Profile__r.Lunch_Time_Start__c,Timecard_Profile__r.Lunch_Time_duration__c,
                         Organizational_Calendar__c,Weekend_Override__c,Name, SVMXC__Salesforce_User__c,
                         ERP_Timezone__c, ERP_Work_Center__c, SVMXC__Working_Hours__c,Additional_Availability__c,
                         Timecard_Profile__r.X30_min_Travel_Time__c, Timecard_Profile__r.Normal_End_Time__c,
                         Timecard_Profile__r.Normal_Start_Time__c, Additional_Availability__r.MondayEndTime, 
                         Additional_Availability__r.MondayStartTime,Additional_Availability__r.TuesdayStartTime,
                         Additional_Availability__r.WednesdayStartTime,Additional_Availability__r.ThursdayStartTime,
                         Additional_Availability__r.FridayStartTime, Additional_Availability__r.SaturdayStartTime,
                         Additional_Availability__r.SundayStartTime, Additional_Availability__r.TuesdayEndTime,
                         Additional_Availability__r.WednesdayEndTime, Additional_Availability__r.ThursdayEndTime,
                         Additional_Availability__r.FridayEndTime, Additional_Availability__r.SaturdayEndTime,
                         Additional_Availability__r.SundayEndTime 
                         
                         from SVMXC__Service_Group_Members__c where id in: techIds]);
       
        
        
    }
    if (Trigger.isDelete){
        ServiceMaxTimesheetUtils Obj = new ServiceMaxTimesheetUtils();
        System.debug('entering time utl 3');
        Obj.UpdateMostRecentChangeToTimeSheet(trigger.oldmap, null, 3);
    }
   
}
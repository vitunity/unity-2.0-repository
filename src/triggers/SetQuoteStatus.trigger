Trigger SetQuoteStatus on BigMachines__Quote__c (before insert,before update) {
  
   //Added Functionality by rakesh
   if(trigger.isUpdate)
       OfferInUSD.setOffer(Trigger.NewMap,Trigger.OldMap);
       
   for (BigMachines__Quote__c Quote : Trigger.new)
   {
      if((Quote.Order_Type__c != 'Combined')&&((Quote.Message_Service__c != null && Quote.Message_Service__c == 'SUCCESS') || (Quote.Booking_Message__c != null && Quote.Booking_Message__c == 'SUCCESS')))
      {
          Quote.Quote_Status__c = 'SUCCESS';
      }  
      else if((Quote.Order_Type__c == 'Combined')&&((Quote.Message_Service__c != null && Quote.Message_Service__c == 'SUCCESS') && (Quote.Booking_Message__c != null && Quote.Booking_Message__c == 'SUCCESS')))
      {
          Quote.Quote_Status__c = 'SUCCESS';
      } 
      else if((Quote.Order_Type__c == 'Combined')&&((Quote.Message_Service__c != null && Quote.Message_Service__c.containsIgnoreCase('SUCCESS')&& Quote.Message_Service__c != 'SUCCESS')||(Quote.Booking_Message__c != null && Quote.Booking_Message__c.containsIgnoreCase('SUCCESS') && Quote.Booking_Message__c != 'SUCCESS')))
      {
          Quote.Quote_Status__c = 'SUCCESS Action Needed';
      }
      else if((Quote.Order_Type__c != 'Combined')&&((Quote.Message_Service__c != null && Quote.Message_Service__c.containsIgnoreCase('SUCCESS')&& Quote.Message_Service__c != 'SUCCESS') || (Quote.Booking_Message__c != null && Quote.Booking_Message__c.containsIgnoreCase('SUCCESS') && Quote.Booking_Message__c != 'SUCCESS')))
      {
          Quote.Quote_Status__c = 'SUCCESS Action Needed';
      }
      else if((Quote.Message_Service__c != null && !Quote.Message_Service__c.containsIgnoreCase('SUCCESS')) || (Quote.Booking_Message__c != null && !Quote.Booking_Message__c.containsIgnoreCase('SUCCESS'))) 
      {
          Quote.Quote_Status__c = 'ERROR';
      }
      else if (Quote.Quote_Type__c == 'Amendments' && (Quote.Order_Type__c == 'Sales' || Quote.Order_Type__c == 'Combined' ) && Quote.SAP_Prebooked_Sales__c != null)
      {
          Quote.Quote_Status__c = 'Amended';
      }
      else if (Quote.Quote_Type__c == 'Amendments' && (Quote.Order_Type__c == 'Services' || Quote.Order_Type__c == 'Combined') && Quote.SAP_Prebooked_Service__c != null)
      {
          Quote.Quote_Status__c = 'Amended';
      }
      else
      {
          
      }
   }
}
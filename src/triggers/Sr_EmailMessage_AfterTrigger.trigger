trigger Sr_EmailMessage_AfterTrigger on EmailMessage (before Insert) {
    /*Schema.DescribeSObjectResult resultCase = case.sObjectType.getDescribe();
    system.debug('resultCase --->'+resultCase );
    string keyPrefix = resultCase.getKeyPrefix();//
    system.debug('keyPrefix--->'+keyPrefix );
    //set<id> ParentIdset = new set<id>();
    List<case> UpdateCase = new List<case>();
    List<CaseComment> newCaseComment = new List<CaseComment>();
    Database.DMLOptions dmlOpts = new Database.DMLOptions();
    //map<id,List<EmailMessage>> MapParentAndEmailMessage = new map<id,List<EmailMessage>>();
    //Integer i = 2/0;
        for(EmailMessage EM : trigger.new)
        {
            String EmailMessageParent = EM.ParentId;
            
            if(EM.ParentId !=null && EmailMessageParent.startsWith(keyPrefix) == true && EM.TextBody!=null)
            {
            
            CaseComment cse = new CaseComment();
            cse.ParentId = EM.ParentId;
            cse.IsPublished = TRUE;
            cse.CommentBody =  EM.TextBody.replaceAll('<br>',' ');
            dmlOpts.allowFieldTruncation = true;

            cse.setOptions(dmlOpts);
            newCaseComment.ADD(cse);
         
         
         /*Commented by Anupam 10/09 as code is not required    
                ParentIdset.add(EM.ParentId);
                List<EmailMessage> emList = new List<EmailMessage>();
                if(MapParentAndEmailMessage.get(EM.ParentId)!=null)
                {
                    emList = MapParentAndEmailMessage.get(EM.ParentId);
                }
                emList.add(EM);
                MapParentAndEmailMessage.put(EM.ParentId,emList);
          */
           /* }
        }
        
    if(newCaseComment.size()>0)
        {
            Database.SaveResult[] result = Database.insert(newCaseComment); 
        }
            
    
    /* Commented by Anupam 10/09 as code is not required
        for(case varCase : [select id,Description,Local_Description__c from case where id in :ParentIdset ])
        {
            CaseComment cse = new CaseComment();
            system.debug('varCase id is =='+varCase);
            for(EmailMessage EM : MapParentAndEmailMessage.get(varCase.id))
            {
                //Commented as per US4171
                //varCase.Local_Description__c = (varCase.Local_Description__c !=null?varCase.Local_Description__c :'') + EM.TextBody;
                cse.ParentId = EM.ParentId;
                cse.CommentBody = (varCase.Local_Description__c !=null?varCase.Local_Description__c :'') + EM.TextBody;
                
                system.debug('varCase.Description is =='+varCase.Local_Description__c);
            }
            //Commented as per US4171
           //UpdateCase.add(varCase); 
           //newCaseComment.add(cse);
        }
        Commented as per US4171
        if(UpdateCase.size()> 0)
        {
            update UpdateCase;
        }
        */
     Set<String> fromemails = new set<String>();
     map<String,String> emailtoname = new map<String,String>();
    for(EmailMessage EM : trigger.new)
    {
        if(EM.Incoming) //(EM.fromName != null && EM.fromName.contains('@')) || (EM.fromName == null)
            fromemails.add(EM.FromAddress);
        if(EM.HTMLBody == null && EM.TextBody != null)
            EM.HTMLBody = EM.TextBody;
    }
    if(fromemails != null && fromemails.size() > 0)
    {
        for(Contact con : [Select name,email from contact where email in: fromemails])
        {
            emailtoname.put(con.email,con.name);
        }
    }
    system.debug('@@@@@@' + emailtoname);
    if(emailtoname != null && emailtoname.size() > 0)
    {
        for(EmailMessage EM : trigger.new)
        {
            if(emailtoname.containskey(EM.FromAddress))
            {
                EM.FromName = emailtoname.get(EM.FromAddress);
            }
        }
    }
    for(EmailMessage EM : trigger.new)
    {
        if(EM.ParentId !=null && EM.TextBody!=null && EM.FromAddress != 'supportqa@varian.com')
        {
            System.debug('**EM.ParentId**: ' + EM.ParentId);              
            List<Case> parentCases = [select Id, Status, OwnerId from Case where Id =:EM.ParentId and Status = 'Closed'];
            List<Contact> contacts = new List<Contact>();
            if(EM.FromAddress != Null && EM.FromAddress != '')
            {
                contacts = [select Id, AccountId, Email from Contact where Email =: EM.FromAddress];
            }
            if(parentCases.size() > 0  && String.isNotBlank(EM.Subject) && String.isNotBlank(EM.TextBody))
            {
                Case newCase = new Case();
                newCase.Status = 'New';
                newCase.Subject = EM.Subject;
                newCase.Local_Subject__c = EM.Subject;
                newCase.Origin = 'Email';
                newCase.Description = EM.TextBody;
                newCase.Local_Description__c = EM.TextBody;                
                newCase.Priority = 'Medium';              
                if(contacts.size() > 0)
                {
                    newCase.ContactId = contacts[0].Id;
                    newCase.AccountId= contacts[0].AccountId;                        
                }
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.useDefaultRule = true;
                newCase.setOptions(dmo);
                Database.upsert(newCase);  
                EM.ParentId = newCase.id;
            }
        }
    }                            
}
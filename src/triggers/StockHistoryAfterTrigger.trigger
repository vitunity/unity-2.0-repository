/*************************************************************************\
      @ Author          : Steve Avon
      @ Date            : 11-Jan-2016
      @ Description     : This Trigger was created for DE7019 - to set the Work Order field on the Stock History Record   
****************************************************************************/

trigger StockHistoryAfterTrigger on SVMXC__Stock_History__c (after insert, after update) {
    
    try{
        Set<Id> setSHNoWO = new Set<Id>();
        for (SVMXC__Stock_History__c sh : Trigger.new) {
            if (sh.SVMXC__Service_Order__c == null) {
                setSHNoWO.add(sh.Id);
            }
        }
        if (!setSHNoWO.isEmpty()) {
            StockHistoryClass.setStockHistoryWorkOrder(setSHNoWO, Trigger.newMap);
        }
    
    
        /* DE7116 
         * Product Stock - Manual Stock Adjustments
         * Product_Stock.Qty. field should update based on most recent Stock_History.Qty_After_Change
         */
        StockHistoryClass.updateQtyChangeOnProductStock(Trigger.new);
    }catch(Exception e){
        system.debug('exception====='+e);
    }

}
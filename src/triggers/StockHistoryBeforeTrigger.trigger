/*************************************************************************\
      @ Author          : Steve Avon
      @ Date            : 11-Jan-2016
      @ Description     : This Trigger was created for DE7019 - to set the Work Order field on the Stock History Record   
****************************************************************************/

trigger StockHistoryBeforeTrigger on SVMXC__Stock_History__c (before insert, before update) {
	Set<Id> setSHNoWO = new Set<Id>();

	for (SVMXC__Stock_History__c sh : Trigger.new) {
		if (sh.SVMXC__Service_Order__c == null) {
			setSHNoWO.add(sh.Id);
		}
	}

	if (!setSHNoWO.isEmpty()) {
		StockHistoryClass.setStockHistoryWorkOrder(setSHNoWO, Trigger.newMap);
	}
}
/* Set Location from product stock */
trigger StockedSerialTrigger on SVMXC__Product_Serial__c (before insert , before update) {
    try{
        set<Id> productStockIds = new set<Id>();
        map<Id,SVMXC__Product_Stock__c> productStockMap; 
        for(SVMXC__Product_Serial__c ss : trigger.new){
            if(ss.SVMXC__Product_Stock__c != null)
            productStockIds.add(ss.SVMXC__Product_Stock__c);
        }
        if(!productStockIds.isEmpty()){
            productStockMap = new map<Id,SVMXC__Product_Stock__c>([select SVMXC__Location__c from SVMXC__Product_Stock__c where id in :productStockIds ]);
            for(SVMXC__Product_Serial__c ss : trigger.new){
                if(ss.SVMXC__Product_Stock__c != null){
                    ss.Location__c = productStockMap.get(ss.SVMXC__Product_Stock__c).SVMXC__Location__c;
                }
            }       
        }
    }catch(Exception e){}
}
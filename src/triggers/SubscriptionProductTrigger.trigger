trigger SubscriptionProductTrigger on Subscription_Product__c (before insert, before update) {
    SubscriptionProductTriggerHandler.updateSubscriptionProductFields(trigger.new);
}
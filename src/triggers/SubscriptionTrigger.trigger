trigger SubscriptionTrigger on Subscription__c (before insert,after insert,after update) {
    if(trigger.isBefore){
        SubscriptionTriggerHandler.updateSubscriptionFields();
    }else{
        SubscriptionTriggerHandler.updateSubscriptionLineItems();
        SaasInstallation.createInstallationPlan(trigger.new,trigger.oldMap);
        if(trigger.new[0].Renew_Subscription__c && !trigger.oldMap.get(trigger.new[0].Id).Renew_Subscription__c){
            AutoRenewSaaSSubscription.renewSubscription(trigger.new[0].Id);
        }
    }

    if(trigger.isAfter && trigger.isInsert){
        SubscriptionTriggerHandler.updateQuoteFields(trigger.new);
        
        if(trigger.new[0].SubscriptionOnlyQuote__c){
            SubscriptionConfirmation.sendEmail(trigger.new[0].Id,'Quote_SAP_BookedSubscription_Success_VF');
        }
    }

    if (trigger.isAfter) {
         //STSK0011740 - Start
         //SubscriptionTriggerHandler.updateInstProdForBillingType(trigger.new);
         SubscriptionTriggerHandler.updateInstProdFields(trigger.new);
        // //STSK0011740 - End
    }
}
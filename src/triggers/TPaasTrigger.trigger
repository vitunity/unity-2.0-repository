trigger TPaasTrigger on TPaaS_Package_Detail__c (before Insert,before update,after Insert,after update) {
    
    if( Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate) ){
        //TPassTriggerHandler.setSequenceNumberBefore(trigger.New,Trigger.oldmap); // NO Need Old Code 
        TPaasSlackHandler.setSequenceNumberBefore(Trigger.new,trigger.oldmap);       
    }
    if( Trigger.isAfter && Trigger.isInsert ){
        TPaasSlackHandler.AfterInsertTPaas(Trigger.new);
    }
    
    if( Trigger.isAfter && Trigger.isUpdate ){
        TPaasSlackHandler.AfterApprovedTPaas(Trigger.new,trigger.oldmap);
        TPaasSlackHandler.AddTpaasStatusChangeNotification(Trigger.new,trigger.oldmap);        
    }
    
    
}
trigger TPassTrigger on TPaaS_Package_Detail__c (before insert, after Insert,after update,before update) {

    if( Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate) ){
        for(TPaaS_Package_Detail__c  t: trigger.new){
            if(Trigger.isInsert || (Trigger.isUpdate && t.Priority__c <> trigger.oldmap.get(t.Id).Priority__c))
            t.Plan_Due_Date__c = (t.Priority__c == 'Standard (3 days)')?system.Now().addDays(3):((t.Priority__c == 'Priority (2 days)')?system.Now().addDays(2):system.Now().addDays(1)); 
        }
        TPassTriggerHandler.setSequenceNumberBefore(trigger.New,Trigger.oldmap);
        
    }
    
    TPassTriggerHandler handler = new TPassTriggerHandler ();
    
    if( Trigger.isAfter && Trigger.isInsert ){
        handler.AfterInsert(Trigger.new, Trigger.newMap);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate){
        handler.AfterUpdate(Trigger.new, Trigger.oldMap);
    }  
    
}
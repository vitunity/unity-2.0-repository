trigger TechnicianEquipment_AfterTrigger on SVMXC__Service_Group_Members__c (after insert,after update) 
{
    //Audit Tracking Future Invoke
    if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter) SObjectHistoryProcessor.trackHistory('SVMXC__Service_Group_Members__c');
    
    SC_ClassTechnician_Equipment objTechcls= new SC_ClassTechnician_Equipment();
    if(objTechcls.checkrecursiveFlag)
    {
          //if(Trigger.isUpdate) 
            {
                objTechcls.updateOOCFlag(Trigger.new, Trigger.oldMap);
            }
    }
    
    Set<Id> UserIds = new Set<Id>(); //DE7370
    for(SVMXC__Service_Group_Members__c  Techqui:trigger.new){
          if(trigger.isinsert )
          {
            if((string.valueof(Techqui.User__c) != Null && string.valueof(Techqui.User__c) != '') && ((string.valueof(Techqui.SVMXC__Service_Group__c) != Null && string.valueof(Techqui.SVMXC__Service_Group__c) != '') || (techqui.ERP_CSS_District__c != Null && Techqui.ERP_CSS_District__c != '')))
            UserIds.add(Techqui.User__c);
          }
          if(trigger.isupdate)
            {
                system.debug('oldUserId---'+string.valueof(trigger.oldmap.get(Techqui.id).User__c));
               if(((string.valueof(Techqui.User__c) != string.valueof(trigger.oldmap.get(Techqui.id).User__c)) && (string.valueof(Techqui.User__c) != Null) && (string.valueof(Techqui.User__c) != '')) || ((string.valueof(Techqui.SVMXC__Service_Group__c) != string.valueof(trigger.oldmap.get(Techqui.id).SVMXC__Service_Group__c))) || ((Techqui.ERP_CSS_District__c != Trigger.OldMap.get(Techqui.id).ERP_CSS_District__c))){
               UserIds.add(Techqui.User__c);
               }
            }
          
    }
    system.debug('UserIds---'+UserIds);
    if(UserIds.size() > 0){
        map<id,user> Usermap = new map<id,user>();
        /*DE7370: Don't need to read the record to write on it if you already have the id
        for(user u : [select id, District_Manager__c, Service_Team__c from User where id IN :UserIds]){
           usermap.put(u.id,u);
        }*/
        
        try{
       
            List<SVMXC__Service_Group_Members__c> TechList = new List<SVMXC__Service_Group_Members__c>([select id, User__c, ERP_CSS_District__c, 
            SVMXC__Service_Group__r.Name, SVMXC__Service_Group__r.District_Manager__r.Name 
            from SVMXC__Service_Group_Members__c where User__c IN: UserIds for update]);
            system.debug('TechList---'+TechList);
            for(SVMXC__Service_Group_Members__c  Techqui:Techlist){
                if (Techqui.User__c == null || !UserIds.contains(Techqui.User__c)) continue; //DE7370
                if(usermap.containskey(Techqui.User__c)){
                    system.debug('Service Group---'+Techqui.ERP_CSS_District__c);
                    system.debug('District Manager---'+Techqui.SVMXC__Service_Group__r.District_Manager__r.Name);
                    usermap.get(Techqui.User__c).Service_Team__c = Techqui.ERP_CSS_District__c;
                    usermap.get(Techqui.User__c).District_Manager__c = Techqui.SVMXC__Service_Group__r.District_Manager__r.Name;
                } else { //DE7370
                    usermap.put(Techqui.User__c,new User(id=Techqui.User__c,Service_Team__c = Techqui.ERP_CSS_District__c,District_Manager__c = Techqui.SVMXC__Service_Group__r.District_Manager__r.Name));
                }
            }
        update usermap.values();
        }catch(Exception e){
        
        }
    }
}
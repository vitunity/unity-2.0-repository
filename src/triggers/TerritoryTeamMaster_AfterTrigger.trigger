/*
    There can be 2 parts when Territory team record is updated
    1. Territory team members are updated (different role/postion - users) - for thsi some members may be added/ removed from AccountTeam
    2. Territory attributes itself got updated like Country, ZipCode, State - which is very less likely but still need to take care
*/

trigger TerritoryTeamMaster_AfterTrigger on Territory_Team__c (after update,before update) 
{
DefaultAccountTeam dac = new DefaultAccountTeam();

   //Sales team member update/added
   if(trigger.isAfter)dac.updateTerritoryTeamOnAccount(Trigger.newMap,trigger.oldmap); 
    
   //update Account Information
   dac.updateAccountGeographicalData(trigger.newmap,trigger.oldmap);
}
trigger TpassComments  on TPassPackage_Comment__c (before insert, after Insert,after update,before update) {
    if( Trigger.isAfter && Trigger.isInsert ){
        set<Id> setCommentIds = new set<Id>();
        for(TPassPackage_Comment__c tp: trigger.new){
            if(tp.Outbound__c==false){setCommentIds.add(tp.id); }
        } 
        if(setCommentIds.size()>0){
            TPaasSlackHandler.AddTpaasCommentsinSlack(setCommentIds);
        }
    }
}
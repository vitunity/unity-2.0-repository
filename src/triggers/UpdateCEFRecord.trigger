trigger UpdateCEFRecord on Input_Clearance__c (before insert,before update,after insert,after update) {
    if(Trigger.isBefore){
        CEFUtility.updateMCMMarketClearanceForecast(Trigger.new);

        // Mark IsProductBundle flag on the basis of Product Profile
        CEFUtility.setProductBundleFlag(Trigger.new);
    }else if(Trigger.isAfter){
        if(AvoidRecursion.isFirstRun()){
            CEFUtility.UpdateBlockItem(Trigger.new);
            CEFUtility.createClearanceDocumentsLink(Trigger.new);
        }
    }
}
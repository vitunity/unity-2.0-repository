/**************************************
Created by: Naren Godugula

Functionality: 1)This Code updates the Milestone Selection to Transition Phase Gate on Clearance Entry Form
                    when ever a Country Rule is created with Test Box checked to True or when ever Test Box 
                    checked to True on existing Country Rule record
               2) This code also updates the Estimated Submission Date and Expected Clearance Date on 
                       Clearance Entry Form when ever # Weeks to Prepare Submission or # Weeks Estimated 
                       Review Cycle is updated on existing Country Rules  

******************************************/

trigger UpdateMileStoneSelection_CEFs on Country_Rules__c ( after insert, after update) {

    //ENHC0010996.start
        ClearanceEvidenceRequirement.updateAllClearance(trigger.New,trigger.Oldmap);
    //ENHC0010996.End


List<String> Countryid = new List<String>();
List<String> Countryid2 = new List<String>();

    
    if((Trigger.isInsert && Trigger.isafter) || (Trigger.isUpdate && Trigger.isafter)){
    
        for(Country_Rules__c CR: Trigger.New){
                        
            if(((Trigger.isInsert && Trigger.isafter) && CR.TPG_True__c== True)){
            
                Countryid.add(CR.Country__c);
                system.debug('Countryid---'+Countryid);
            }
            
            if((Trigger.isUpdate && Trigger.isafter) && (Trigger.oldMap.get(CR.id).TPG_True__c!= Trigger.NewMap.get(CR.id).TPG_True__c) && (Trigger.NewMap.get(CR.id).TPG_True__c== True)){
            
                Countryid.add(CR.Country__c);
                system.debug('Countryid---'+Countryid);
            }
            
            if((Trigger.isUpdate && Trigger.isafter) && ((Trigger.oldMap.get(CR.id).Weeks_to_Prepare_Submission__c != Trigger.NewMap.get(CR.id).Weeks_to_Prepare_Submission__c) ||(Trigger.oldMap.get(CR.id).Weeks_Estimated_Review_Cycle__c!= Trigger.NewMap.get(CR.id).Weeks_Estimated_Review_Cycle__c))){
            
                Countryid2.add(CR.Country__c);
                system.debug('Countryid2---'+Countryid2);
            }
        
        }
        
        List<Input_Clearance__c> UpdateCEFs = new List<Input_Clearance__c>();
        
        if(Countryid.size()>0){
        
            for(Input_Clearance__c IC : [select id, name, Milestone_Selection__c from Input_Clearance__c where Current_Status__c!= 'Submitted' AND Actual_Submission_Date__c = Null AND Clearance_Status__c != 'Permitted' AND Country__c IN:Countryid]){
                system.debug('IC---'+IC);
                IC.Milestone_Selection__c = 'Transition Phase Gate';
                UpdateCEFs.add(IC);
                
            }
            
        system.debug('UpdateCEFs---'+UpdateCEFs);
        if(UpdateCEFs.size()>0){
            Update UpdateCEFs;
        }
        
       }
       
       List<Input_Clearance__c> UpdateCEFs2 = new List<Input_Clearance__c>();
        
        if(Countryid2.size()>0){
        
            for(Input_Clearance__c IC2 : [select id, name, Country__c,Review_Product__c, Expected_Clearance_Date__c, Estimated_Submission_Date__c, Design_Output_Review__c,Transition_Phase_Gate__c, National_Language_Support__c,  Milestone_Selection__c from Input_Clearance__c where Current_Status__c!= 'Submitted' AND Actual_Submission_Date__c = Null AND Clearance_Status__c != 'Permitted' AND Country__c IN:Countryid2]){
                system.debug('IC2---'+IC2);
                UpdateCEFs2.add(IC2);
                
            }
            
        system.debug('UpdateCEFs2---'+UpdateCEFs2);   
        if(UpdateCEFs2.size()>0){
            Update UpdateCEFs2;
        }
        
       }
       
      
    }
    
}
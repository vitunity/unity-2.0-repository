trigger UpdatePaymentTerms on BigMachines__Quote__c (before insert, before Update) {

    Set<String> Paymentterms  = new Set<String>();
    for(BigMachines__Quote__c BQ :trigger.new )
    {
        if(Trigger.isupdate){
        //BigMachines__Quote__c BQ_old = Trigger.oldMap.get(BQ.Id);
        if(BQ.Initial_Order_Status__c == null )
        {
            String status = BQ.Quote_Status__c;
        if(BQ.Quote_Status__c != null && BQ.Quote_Status__c != trigger.oldmap.get(BQ.id).Quote_Status__c && ( status.contains('SUCCESS')||BQ.Quote_Status__c == 'Error') )
        
        {
            system.debug('Quote_Status__c---'+BQ.Quote_Status__c);
            system.debug('Initial_Order_Status__c---'+BQ.Initial_Order_Status__c);
            if(status.contains('SUCCESS'))
            {
                BQ.Initial_Order_Status__c = 'Success';
            }
            else
            {
               BQ.Initial_Order_Status__c = BQ.Quote_Status__c; 
            }
        }
        
        }
        }
        /**
        if(BQ.Initial_Submitted_To_SAP_By__c == null )
        {
         if(BQ.Submitted_To_SAP_By__c != null)
         {
             BQ.Initial_Submitted_To_SAP_By__c = BQ.Submitted_To_SAP_By__c;
         }
        }
        **/    
        system.debug('Paymentterms'+BQ.Payment_Terms__c);
        if(BQ.Payment_Terms__c != null && BQ.Payment_Terms__c != '' )
         Paymentterms.add(BQ.Payment_Terms__c)  ;
        system.debug('Paymentterms---'+Paymentterms);
       
    }
    List<LookUp_values__c> LUList = new List<LookUp_values__c> ([Select Id, Name from LookUp_values__c where  Name IN : Paymentterms]);
    system.debug('LUList---'+LUList);
    if(LUList.size()>0)
    {
    for(BigMachines__Quote__c BQ :trigger.new )
      { 
         BQ.SalesPaymentTerms__c = LUList[0].id;
         system.debug('SalesPaymentTerms---'+ BQ.SalesPaymentTerms__c); 
      }
    }
}
trigger UpdateTechnicianFromGPSLog on SVMXC__User_GPS_Log__c (after insert) 
{
    Map<Id, SVMXC__User_GPS_Log__c> userToGPSLog = new Map<Id, SVMXC__User_GPS_Log__c>();
    List<SVMXC__Service_Group_Members__c> members = new List<SVMXC__Service_Group_Members__c>();
    for(SVMXC__User_GPS_Log__c gpsLog : Trigger.new)
    {
        if(gpsLog.SVMXC__User__c != null)
        {
            userToGPSLog.put(gpsLog.SVMXC__User__c, gpsLog);
        }
    }
    for(SVMXC__Service_Group_Members__c technician : [select Id, User__c, SVMXC__Salesforce_User__c, Tracking_Additional_Info__c, Tracking_Device_Type__c, Tracking_Status__c, Tracking_Sync_DateTime__c from SVMXC__Service_Group_Members__c where User__c in :userToGPSLog.keySet() AND Tracking_Technician__c = True])
    {
        technician.Tracking_Additional_Info__c = userToGPSLog.get(technician.User__c).SVMXC__Additional_Info__c;
        technician.Tracking_Device_Type__c = userToGPSLog.get(technician.User__c).SVMXC__Device_Type__c;     
        technician.Tracking_Status__c = userToGPSLog.get(technician.User__c).SVMXC__Status__c;
        technician.Tracking_Sync_DateTime__c = userToGPSLog.get(technician.User__c).Sync_DateTime__c;
        technician.SVMXC__Latitude__c = userToGPSLog.get(technician.User__c).SVMXC__Latitude__c;
        technician.SVMXC__Longitude__c = userToGPSLog.get(technician.User__c).SVMXC__Longitude__c;
        
        members.add(technician);    
    }
    if(members.size() > 0)
    {   
        update members;
    }
}
trigger UpdateUsername_before on User (before insert, before update) {
    if(system.Label.User_Triggers_Flag == 'True')
    {
        Profile p  = Check_UpdateUsername.runOnce();       
        
        for(user u: Trigger.New){
            if(p <> null && u.contactid !=null && u.IsActive ==true && u.profileid == p.Id && !(u.Username.contains(Label.MyVarianUserNameText))){
                u.Username = u.Username+Label.MyVarianUserNameText;
            }
         }
     }
  

}
trigger VMarketApprovalLogTrigger on VMarket_Approval_Log__c (before update,before delete) 
{

if(Trigger.IsUpdate || Trigger.isDelete) 

if(Trigger.New != Null)
{
for(VMarket_Approval_Log__c log : Trigger.New)
{
log.addError('YOU CANNOT UPDATE LOGS');
}
}
else
{
for(Id i : Trigger.OldMap.keySet())
{
Trigger.OldMap.get(i).addError('YOU CANNOT DELETE LOGS');
}
}
}
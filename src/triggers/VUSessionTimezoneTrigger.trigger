trigger VUSessionTimezoneTrigger on VU_Event_Schedule__c (before insert, before update) {

    Set<ID> eventID = new Set<ID>();

    for(VU_Event_Schedule__c VS: trigger.New){
        system.debug('-------'+VS.Event_Webinar__c);
        eventID.add(VS.Event_Webinar__c);
    }
    system.debug('EVENT IDS---'+eventID);
    list<Event_Webinar__c> listEventWeb= [SELECT TimeZones__c FROM Event_Webinar__c WHERE ID IN :eventID];
    map<id, string> eventIdandTimeZone= new map<id, string>();
    
    for(Event_Webinar__c eWeb: listEventWeb){
        eventIdandTimeZone.put(eWeb.Id, eWeb.TimeZones__c);
    }
    System.debug('xxx'+eventIdandTimeZone);
    for(VU_Event_Schedule__c VES:  trigger.New ){
        String tzo;
        system.debug('---------EVENTID'+VES.Event_Webinar__c);
        tzo = eventIdandTimeZone.get(VES.Event_Webinar__c);
        System.debug('City'+tzo);
        
        TimeZone tz = UserInfo.getTimeZone();
        System.debug('tzy'+tz);
        integer offsetToUserTimeZone = tz.getOffset(VES.Start_Time__c);
        integer offsetToUserEndTimeZone = tz.getOffset(VES.End_Time__c);
        
        //System.debug('String: ' + tzo);
        TimeZone EventTimeZone = TimeZone.getTimeZone(tzo);
        System.debug('ID2: ' + EventTimeZone );
        System.debug('Display name1: ' + EventTimeZone .getDisplayName());
        System.debug('ID1: ' + EventTimeZone .getID());
        System.debug('String format: ' + EventTimeZone .toString());
        System.debug('Offset1: ' + EventTimeZone .getOffset(VES.Start_Time__c)); 
        integer offsetToEventTimeZone = EventTimeZone.getOffset(VES.Start_Time__c);
        integer offsetToEventEndTimeZone = EventTimeZone.getOffset(VES.End_Time__c);
        integer var= offsetToUserEndTimeZone -offsetToEventEndTimeZone ;
        integer varTime= offsetToUserTimeZone -offsetToEventTimeZone ;
        System.debug('I2: ' + var);

        VES.Session_Start_Time__c=VES.Start_Time__c;
        VES.Session_End_Time__c=VES.End_Time__c;
        VES.Start_Time__c=VES.Start_Time__c.addMinutes(varTime/ (1000 * 60));
        VES.End_Time__c=VES.End_Time__c.addMinutes(var/ (1000 * 60));
        VES.Date__c=VES.Start_Time__c.date();
        System.debug('IDs: ' + VES.Start_Time__c);

    }
}
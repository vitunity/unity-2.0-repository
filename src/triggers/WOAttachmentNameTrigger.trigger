trigger WOAttachmentNameTrigger on Attachment (before insert) 
{
   TriggerDispatcher.TriggerConfig config = new TriggerDispatcher.TriggerConfig(AttachmentTriggerHandler.class, new Set<TriggerDispatcher.TriggerContext>{TriggerDispatcher.TriggerContext.BEFORE_INSERT}, false, false); //
   TriggerDispatcher.execute(config);  
}
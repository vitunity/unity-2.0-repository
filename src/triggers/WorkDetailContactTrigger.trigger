trigger WorkDetailContactTrigger on Work_Detail_Contacts__c (before insert, before update, after insert, after update) {
	new WorkDetailContactTriggerHandler().run();
}
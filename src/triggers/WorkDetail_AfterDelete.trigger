trigger WorkDetail_AfterDelete on SVMXC__Service_Order_Line__c (before delete, after delete) {
    if(Trigger.isBefore)
    {
        WorkDetail_TriggerHandler.deleteAllWorkDetailTimeEntries(trigger.oldMap);
    }
    if(Trigger.isAfter)
    {
        SRClassWorkOrderDetail objWD = new SRClassWorkOrderDetail();    //initiate class of Work Detail
        objWD.updateActualHoursInIWO(trigger.new, trigger.newMap, trigger.oldMap);
        WorkDetailAfterDeleteHandler.updatePOLRemainingQty(Trigger.old);
    }
}
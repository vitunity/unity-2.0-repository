/***************************************************************************
Author: NA
Created Date: NA
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
31-Aug-2017 - Rakesh - STSK0012416 - Calculate Total Work Time field on all case related WO's and updating Total Work Order Time field on Case .
*************************************************************************************/
    trigger WorkOrderDeleteTrigger on SVMXC__Service_Order__c (before delete) 
    {
        List<SVMXC_Time_Entry__c> timeEntriesToDelete;
        timeEntriesToDelete = [SELECT Id FROM SVMXC_Time_Entry__c where Work_Details__r.SVMXC__Service_Order__c IN : trigger.old];
        delete timeEntriesToDelete;
        //Calculate Total Work Time field on all case related WO's and updating Total Work Order Time field on Case.
        CalculateTotalWorkTimeClass.doTotal(trigger.old);
    }
/*************************************************************************\
@ Author          : Kaushiki Verma(WIPRO Technologies)
@ Date            : 8/5/2013
@ Description     : This Trigger will be used to create a new complaint record under case if Is 'This a Complaint?' of case is 'yes'.  
@ Last Modified By      : kaushiki
@ Last Modified On      : 7/feb/14
@ Last Modified Reason  : commented and uncommented CaseUpdateOnsiteResponseTime called to check the functionality for us3691    
@ Last Modified By      : Nikita Gupta   
@ Last Modified on      : 4/1/2014
@ Last Modified Reason  : US3307, Called a method to Create an Event for preferred technician
@ Last Modified By      : Priti Jaiswal   
@ Last Modified on      : 14/5/2014
@ Last Modified Reason  : US1694, called method from work order class to create and clone Technician Checklist
@ Last Modified By      : Nikita Gupta   
@ Last Modified on      : 6/3/2014
@ Last Modified Reason  : US4592, to create new Work Detail record for Field Service record type.
@ Last Modified by      : Kaushiki 09/06/14 for null check
@ Last Modified By     : Bushra   
@ Last Modified on      : 6/24/2014
@ Last Modified Reason  : US1323,called a method to populate child WO field order status with parent WO.
@ Last Modified By     : Priti Jaiswal   
@ Last Modified on      : 6/25/2014
@ Last Modified Reason  : US4595,add recortype condition when updating case status to "Closed by Work Order (Close case)" 
@ Last Modified By      : Parul Gupta   
@ Last Modified on      : 01/07/2014
@ Last Modified Reason  : US4119, to update Case Preffered Tech with WO Preffered Tech.
@ Last Modified By      : Priti Jaiswal  
@ Last Modified on      : 10/07/2014
@ Last Modified Reason  : US4643 - To update fields on Usage/Consumption work detail 
@ Last Modified By      : Priti Jaiswal  
@ Last Modified on      : 08/12/2014
@ Last Modified Reason  : US3791 - create service max event when "IS PM Work Oder" is set  
@ Last Modified Reason  : System debug added kaushiki 4/10
@ Last Modified By      : Nina Gronowska 
@ Last Modified on      : 08/12/2015
@ Last Modified Reason  : US5171 - Sales Order Change affecting PM (Project Management) Work Orders 
@ Last Modified Reason  : System debug added savon/FF 8-13-15

Change Log -
29/12/17 - Nilesh Gorle - STSK0013531 - Calling SR_Class_CaseUtil's close_pm_case method after work order update.
***************************************************************************
  Modified By Amitkumar Katre
  commented code removed
  billing review 101 soql fixed.
  

*/ 
trigger WorkOrder_AfterTrigger on SVMXC__Service_Order__c (after insert,after update) {  
    Map<Id, SVMXC__Service_Order__c> mapIdWorkOrder = new Map<Id, SVMXC__Service_Order__c>(); 
    SVMXC__Service_Order__c order = Trigger.New[0];
    system.debug('#1After:'+order.Tech_AssignDateTime__c+'::'+order.SVMXC__Order_Status__c+'::'+order.SVMXC__Dispatch_Response__c+'::'+order.Replied__c);
    //Audit Tracking Future Invoke
    if ((trigger.isUpdate || trigger.isInsert) && trigger.isAfter && !System.isFuture()&&!Test.isRunningTest()) {
        SObjectHistoryProcessor.trackHistory('SVMXC__Service_Order__c');

        //Added by Ujjal to publish Platform Event - Start
        for(SVMXC__Service_Order__c wo : Trigger.New) {

            if (wo.Event__c != true) {
                mapIdWorkOrder.put(wo.Id, wo);
            }
        }
		//system.debug('mapIdWorkOrder: '+mapIdWorkOrder);
        if(mapIdWorkOrder.size() > 0) {
            //SoiPlatformEventHandler.publishPlatformEventsWO(mapIdWorkOrder);
        }
        //Added by Ujjal to publish Platform Event - End
    }
    
    /* STSK0013531 - Call SR_Class_CaseUtil's close_pm_case method after work order update. */
    if (trigger.isUpdate) {
        SR_Class_CaseUtil.close_pm_case(Trigger.New, Trigger.oldMap);
    }

    SR_Class_WorkOrder objWO = new SR_Class_WorkOrder();
        
    //savon.FF 9-28-2015 US5211 TA7541
    if (WorkDetail_TriggerHandler.cancelProductServicedWDs == 0) {  //TODO:  This should be unneeded.  Remove and fix root cause: missing trigger logic
        Set<Id> cancelProductServicedIds = new Set<Id>(); 
        for (SVMXC__Service_Order__c wo : Trigger.new) {
            if (wo.Cancel__c && (Trigger.oldMap == null || Trigger.oldMap.get(wo.Id).Cancel__c == false)) {
                cancelProductServicedIds.add(wo.Id);
            }
        }
        if (!cancelProductServicedIds.isEmpty()) {
            WorkDetail_TriggerHandler wdth = new WorkDetail_TriggerHandler();
            //WorkDetail_TriggerHandler.cancelProductServicedWDs++; //TODO:  This should be unneeded.  Remove and fix root cause: missing trigger logic : DE8084: CM : 
            wdth.cancelProductServicedWD(cancelProductServicedIds); 
            wdth.cancelChildWO(cancelProductServicedIds); //TODO: Refactor to workorder_triggerHandler in the future 
        }        
    }
 
       
    Set<Id> woProcessIds = new Set<Id>(); //DM: simulate work order interface during testing
    if(Trigger.isUpdate)
    { 
        Set<Id> woCancelledIds = new Set<Id>(); //savon/FF 10-14-15 US5121 DE6306 
        //savon.FF 11-9-2015 US5133 DE6365 
        Id profServices = RecordTypeHelperClass.WORK_ORDER_RECORDTYPES.get('Professional Services').getRecordTypeId();
        Id training = RecordTypeHelperClass.WORK_ORDER_RECORDTYPES.get('Training').getRecordTypeId();
        Id consulting = RecordTypeHelperClass.WORK_ORDER_RECORDTYPES.get('Consulting').getRecordTypeId();
        for(SVMXC__Service_Order__c newWORecord: Trigger.New)
        {
            SVMXC__Service_Order__c oldWORecord = Trigger.oldMap.get(newWORecord.id); 
            if(newWORecord.Interface_Status__c == 'Process' && oldWOrecord.Interface_STatus__c != 'Process')
                woProcessIds.add(newWORecord.id);
        }   
    }
        
    if(Trigger.IsInsert) {   
        SR_ClassERP_WBS wb = new SR_ClassERP_WBS();
        wb.createProdSrvcfRomMasterWO(trigger.new);// fix for event -master work Order
        //objWO.createProdSrvcfRomMasterWO(trigger.new);// fix for event -master work Order
    } 
    
    if(trigger.isUpdate) //is update section added by nikita to run the trigger for both update and insert
    {
        //cdelfattore@forefront 12/17/2015 - Commentted out the below line for US5215 story decomission
        //objWO.cancelCaseChecklist(trigger.new, trigger.oldmap); //priti, US4562, 07/31/2014 - to cancel related checklist when IWO is cancelled
        objWO.closePlanningWO(trigger.new, trigger.oldMap);
        objWO.submitPMTimesheetforApproval(trigger.new, trigger.oldMap);
    } 
    
    if(trigger.IsInsert || trigger.isUpdate)
    { 
       // objWO.updateCounterReadingRec(trigger.new, Trigger.oldMap);
        objWO.createUpdateServiceMaxEventProbDesc(trigger.new, Trigger.oldMap); //Nikita, combined createServiceMaxEvent(US3791) and updateServiceMaxEventProbDesc (US4899)
        objWO.UpdateTechEqtFromWO(trigger.new,trigger.oldmap);
        objWO.createZSHOrderBrazil(trigger.new, trigger.oldMap);    //creat ZSH Order (parts order RMA fro 4400)
        objWO.updateWorkOrderStatus(trigger.new, trigger.oldMap);  //added by bushra reference to us1323
        objWO.updateEventandlinkedWO(trigger.new,trigger.oldmap);  // Updated by mohit (US4082) 5/5/2014 -Training //updated by Nikita, on 6/3/2014 //US4657, Parul on 23/07/2014
        objWO.UpdateCaseFromWO(trigger.new,trigger.oldmap,trigger.newMap); //method updated to club Case updates //pszagdaj/ff: 11-05-2015, newMap added to call after merge on a helper class
        objWO.updateWorkDetailFromWO(trigger.new, Trigger.oldMap); //consolidated method for all WD updates// Nikita
        objWO.updateTimeMatrixFromWO(trigger.new, Trigger.oldMap); //KL US5108
        objWO.updateCaseWithPMAccount(trigger.new, Trigger.oldMap);
    }
        
    if(Trigger.isUpdate){
        WOProductStockHistoryController obj =  new WOProductStockHistoryController(Trigger.new,objWO.getWorkDetailOfWorkOrders(),Trigger.oldmap);
        obj.validateRemaningQty();
        obj.createStockHistory();
    }
    
   
    if(Trigger.isUpdate){
        objWO.scheduledDateBilingType(Trigger.new,objWO.getWorkDetailOfWorkOrders(),Trigger.oldmap);
    }
    
    /* Update malfunction date form work order to workdetail line if malfunction date get changed on work order*/
    if(trigger.isupdate && trigger.isafter ){
        //SR_Class_WorkOrderUtils.updateMalFunDtToWL(trigger.new, trigger.oldmap);
        SR_Class_WorkOrderUtils.updateCaseWorkOrderResolution(trigger.new, trigger.oldmap);
    }
    
    objWO.writebackWorkDetails();
    //SRClassWorkOrderDetail.workOrderCount = SRClassWorkOrderDetail.workOrderCount - 1; //savon.FF 12-28-2015 DE6164 //DE6743
    system.debug(woProcessIds);
    if (Test.isRunningTest() && !System.isFuture() && woProcessIds.size()>0) SR_TestData.simulateWorkOrderInterface(woProcessIds); //Runs when Test.stopTest() is executed in test methods
   
    system.debug('#2After:'+order.Tech_AssignDateTime__c+'::'+order.SVMXC__Order_Status__c+'::'+order.SVMXC__Dispatch_Response__c+'::'+order.Replied__c);

}
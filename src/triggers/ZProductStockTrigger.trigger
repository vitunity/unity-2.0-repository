trigger ZProductStockTrigger on SVMXC__Product_Stock__c (before update, before insert) {
    for(SVMXC__Product_Stock__c pObj : Trigger.new){
        if(String.isNotBlank(pObj.Batch_Number__c))
        pObj.SVMXC__ProdStockCompositeKey__c = pObj.SVMXC__Product__c+'_'+pObj.SVMXC__Location__c+'_'+pObj.SVMXC__Status__c+'_'+pObj.Batch_Number__c;
    }
}
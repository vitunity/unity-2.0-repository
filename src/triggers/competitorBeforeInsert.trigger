trigger competitorBeforeInsert on Competitor__c (before insert, before update) {

    for(Competitor__c comp : trigger.new) {
        if(comp.NVIP_Key__c == null || comp.NVIP_Key__c == '') {
            comp.NVIP_Key__c = comp.Competitor_Account_Name__c + comp.Model__c + comp.Vendor__c;
        }
    }
}
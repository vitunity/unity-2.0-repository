/**************************************
Created by: Naren Godugula

Functionality: This Code Deletes the corresponding Clearance Entry Form 
                records when ever a Product Profile is Deleted

******************************************/
trigger deleteCorrespondingCEFs on Review_Products__c (before delete) {
    
    List<Input_Clearance__c> CEFsList = new List<Input_Clearance__c>();
    if(Trigger.isdelete && Trigger.isbefore){
    
    system.debug('trigger.oldmap.keyset()'+trigger.oldmap.keyset());
        for(Input_Clearance__c PP : [select id,name from Input_Clearance__c where Review_Product__c IN:trigger.oldmap.keyset()]){
        system.debug('pp'+PP);
            CEFsList.add(PP);
            system.debug('CEFsList'+CEFsList);
        }
        
        Delete CEFsList;
    }
    
}
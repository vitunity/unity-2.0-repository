/*
@Author: Ajinkya Raut
@Description: Libra Phase II (Create opportunities bases on follow up requested and Installed Product removed flags)
*/
trigger insertNewOpportunity on PON__c(after update, after insert) { 

  List<PON__c> ponList = new List <PON__c>();

  for (PON__c pon: Trigger.new) {

   if (Trigger.IsUpdate) {
    PON__c oldPon = Trigger.oldMap.get(pon.ID);

    if ((pon.Follow_up_Requested__c != oldPon.Follow_up_Requested__c) && pon.Follow_up_Requested__c != false) {
      ponList.add(pon);
     }

    }

    if (Trigger.IsInsert) {
     if (pon.Follow_up_Requested__c == true)
      ponList.add(pon);
    }

   }


   if (ponList.size() > 0) {
    insertNewOpportunityHandler insertOpp = new insertNewOpportunityHandler();
    insertOpp.insertNewOpportunity(ponList);
   }
   

  }
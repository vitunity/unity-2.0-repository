trigger oktaupdate on CpOktaUserActivation__c (before insert) 
{ 
  set<String> useremailslisttrg = new set<string>();
  for(CpOktaUserActivation__c obj : trigger.new)
  {
    useremailslisttrg.add(obj.Email__c);
   }  
  Oktauserupdate.useractivationmethodforokta(useremailslisttrg);
    
}
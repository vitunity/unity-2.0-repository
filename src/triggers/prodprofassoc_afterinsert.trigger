trigger prodprofassoc_afterinsert on Product_Profile_Association__c (after insert,after update,after delete) {
	map<id,Review_Products__c> bundleprodprofilemap = new Map<Id,Review_Products__c>();
	Map<id,Product_Profile_Association__c> updateppaafterdelete = new Map<id,Product_Profile_Association__c>();
	map<id,string> prodprofmap = new Map<id,String>();
	Set<Id> standaloneprofiles = new set<Id>();
	Set<Id> deletedbundleprofiles = new set<Id>();
	Map<Id,Decimal> bundleprofseq = new Map<id, Decimal>();
	if (trigger.isDelete)
	{
		for( Product_Profile_Association__c ppa : trigger.old)
		{
			if (ppa.Bundle_Product_Profile__c != null)
			{
				deletedbundleprofiles.add(ppa.Bundle_Product_Profile__c);
			}
		}
	}
	/*
	if (trigger.isInsert || trigger.isUpdate)
	{
		For (Product_Profile_Association__c ppa: trigger.new)
		{
			if (ppa.Standalone_Product_Profile__c != null)
			{
				standaloneprofiles.add(ppa.Standalone_Product_Profile__c);
			}
		}
		if (standaloneprofiles.size() > 0)
		{
			For (Review_Products__c prodprof : [select id, Business_Unit__c from Review_Products__c where id in :standaloneprofiles])
			{
				prodprofmap.put(prodprof.id,prodprof.Business_Unit__c);
			}
		}
		if (prodprofmap.size() > 0)
		{
			for(Product_Profile_Association__c ppa : trigger.new)
			{
				if (ppa.Standalone_Product_Profile__c != null && prodprofmap.containskey(ppa.Standalone_Product_Profile__c) && prodprofmap.get(ppa.Standalone_Product_Profile__c) != null)
				{
					string busunit = prodprofmap.get(ppa.Standalone_Product_Profile__c);
					if (busunit.containsIgnoreCase('3rd Party'))
					{
						Review_Products__c pp = new Review_Products__c();
						pp.id = ppa.Bundle_Product_Profile__c;
						pp.Business_Unit__c = busunit;
						bundleprodprofilemap.put(pp.id, pp);
					}
				} 
			}
		}
		if (bundleprodprofilemap.size() > 0)
		{
			update bundleprodprofilemap.values();
		}
	}
	bundleprodprofilemap = new Map<Id,Review_Products__c>();
	*/
	if (deletedbundleprofiles.size() > 0 )
	{
		for (Product_Profile_Association__c ppa : [select id,Bundle_Product_Profile__c,Product_Profile_Business_Unit__c,Sequence_Number__c from Product_Profile_Association__c where Bundle_Product_Profile__c in :deletedbundleprofiles order by name asc])
		{
			if (bundleprofseq.size() > 0)
			{
				if (bundleprofseq.containsKey(ppa.Bundle_Product_Profile__c))
				{
					decimal seqnum = bundleprofseq.get(ppa.Bundle_Product_Profile__c) + 1;
					ppa.Sequence_Number__c = seqnum;
					bundleprofseq.put(ppa.Bundle_Product_Profile__c, seqnum);
				}
				else
				{
					ppa.Sequence_Number__c = 1;
					bundleprofseq.put(ppa.Bundle_Product_Profile__c, 1);
				}
			}
			else
			{
				ppa.Sequence_Number__c = 1;
				bundleprofseq.put(ppa.Bundle_Product_Profile__c, 1);
			}
			updateppaafterdelete.put(ppa.id,ppa);
			/*
			if (ppa.product_profile_Business_Unit__c.containsIgnoreCase('3rd Party'))
			{
				if(bundleprodprofilemap.size() > 0 )
				{
					Review_Products__c pp = new Review_Products__c();
					pp.id = ppa.Bundle_Product_Profile__c;
					pp.Business_Unit__c = ppa.Product_Profile_Business_Unit__c;
					bundleprodprofilemap.put(pp.id,pp);
				}
				else if (bundleprodprofilemap.size() == 0)
				{
					Review_Products__c pp = new Review_Products__c();
					pp.id = ppa.Bundle_Product_Profile__c;
					pp.Business_Unit__c = ppa.Product_Profile_Business_Unit__c;
					bundleprodprofilemap.put(pp.id,pp);
				}
			}
			else if (bundleprodprofilemap.size() == 0 || (bundleprodprofilemap.size() > 0 && !bundleprodprofilemap.containsKey(ppa.Bundle_Product_Profile__c)))
			{
				Review_Products__c pp = new Review_Products__c();
				pp.id = ppa.Bundle_Product_Profile__c;
				pp.Business_Unit__c = ' ';
				bundleprodprofilemap.put(pp.id,pp);
			}?*/

		}
		/*
		if (bundleprodprofilemap.size() > 0)
		{
			update bundleprodprofilemap.values();
		}*/
		if(updateppaafterdelete.size() > 0)
		{
			update updateppaafterdelete.values();
		}
	}
}
trigger prodprofassoc_beforeinsert on Product_Profile_Association__c (before insert) {
	Set<id> parentprodprofiles = new set<Id>();
	Map<Id,Decimal> bundleprofseq = new Map<id, Decimal>();
	For (Product_Profile_Association__c ppa: trigger.new)
	{
		if (ppa.Bundle_Product_Profile__c != null)
		{
			parentprodprofiles.add(ppa.Bundle_Product_Profile__c);
		}
	}
	if (parentprodprofiles.size() > 0 )
	{
		for (AggregateResult  ar : [select Max(Sequence_Number__c) seqnum, Bundle_Product_Profile__c from Product_Profile_Association__c where Bundle_Product_Profile__c in : parentprodprofiles Group By Bundle_Product_Profile__c])
        {
        	bundleprofseq.put((Id)ar.get('Bundle_Product_Profile__c'), (Decimal)ar.get('seqnum'));
		}
	}
	for (Product_Profile_Association__c ppa : trigger.new)
	{
		String comb = String.valueOf(ppa.Bundle_Product_Profile__c) + String.valueOf(ppa.Standalone_Product_Profile__c);
		ppa.Bundle_standalone_combination__c = comb;
		if (bundleprofseq.size() > 0)
		{
			if (bundleprofseq.containsKey(ppa.Bundle_Product_Profile__c))
			{
				ppa.Sequence_Number__c = bundleprofseq.get(ppa.Bundle_Product_Profile__c) + 1;
			}
			else
				ppa.Sequence_Number__c = 1;
		}
		else
		{
			ppa.Sequence_Number__c = 1;
		}
	}
}
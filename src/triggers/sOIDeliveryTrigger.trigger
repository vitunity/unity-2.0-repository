trigger sOIDeliveryTrigger on SOI_Delivery__c (before insert, before update) {

    map<String, SOI_Delivery__c> mapProdCdSOI = new map<String, SOI_Delivery__c>();
    map<String, SOI_Delivery__c> mapSONameSOI = new map<String, SOI_Delivery__c>();

    map<String, SOI_Delivery__c> mapSONameSOI2 = new map<String, SOI_Delivery__c>();

    map<String, map<String, SOI_Delivery__c>> mapSoiNamesoNameSoiDel = new map<String, map<String,SOI_Delivery__c>>();
    map<String, map<String, SOI_Delivery__c>> mapSoiNamesoHLvlNameSoiDel = new map<String, map<String,SOI_Delivery__c>>();

    map<String, SOI_Delivery__c> mapSoNameSoiNameSoiDel = new map<String, SOI_Delivery__c>();
    map<String, SOI_Delivery__c> mapSoNameHLvlSoiNameSoiDel = new map<String, SOI_Delivery__c>();
    
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        for(SOI_Delivery__c soiDel : Trigger.New) {
            if(soiDel.ERP_Material__c != null && !String.isBlank(soiDel.ERP_Material__c)) {
                mapProdCdSOI.put(soiDel.ERP_Material__c, soiDel);
            }
            /*
            if(soiDel.ERP_Sales_Order__c != null && !String.isBlank(soiDel.ERP_Sales_Order__c)) {
                mapSONameSOI.put(soiDel.ERP_Sales_Order__c, soiDel);
            }*/

            if(soiDel.ERP_Sales_Order__c != null && !String.isBlank(soiDel.ERP_Sales_Order__c)
                && soiDel.ERP_Sales_Order_Item__c != null && !String.isBlank(soiDel.ERP_Sales_Order_Item__c)
               ) 
            {
                mapSONameSOI.put(soiDel.ERP_Sales_Order__c, soiDel);
                mapSoiNamesoNameSoiDel.put(soiDel.ERP_Sales_Order_Item__c, mapSONameSOI);

                String combinedStr = soiDel.ERP_Sales_Order__c + '-' + soiDel.ERP_Sales_Order_Item__c;
                mapSoNameSoiNameSoiDel.put(combinedStr, soiDel);
            }

            if(soiDel.ERP_Sales_Order__c != null && !String.isBlank(soiDel.ERP_Sales_Order__c)
                && soiDel.ERP_Sales_Order_Higher_Level_Item__c != null && !String.isBlank(soiDel.ERP_Sales_Order_Higher_Level_Item__c)
               ) 
            {
                mapSONameSOI2.put(soiDel.ERP_Sales_Order__c, soiDel);
                mapSoiNamesoHLvlNameSoiDel.put(soiDel.ERP_Sales_Order_Higher_Level_Item__c, mapSONameSOI2);

                String combinedStr2 = soiDel.ERP_Sales_Order__c + '-' + soiDel.ERP_Sales_Order_Higher_Level_Item__c;
                mapSoNameHLvlSoiNameSoiDel.put(combinedStr2, soiDel);
            }            
        }

        System.debug('#### debug mapProdCdSOI = ' + mapProdCdSOI);
        System.debug('#### debug mapSoNameSoiNameSoiDel = ' + mapSoNameSoiNameSoiDel);
        System.debug('#### debug mapSoNameHLvlSoiNameSoiDel = ' + mapSoNameHLvlSoiNameSoiDel);

        if(mapProdCdSOI != null && mapSoNameSoiNameSoiDel != null && mapSoNameHLvlSoiNameSoiDel != null) {
             sOITriggerHandler.preProductFromSAP(mapProdCdSOI, mapSoNameSoiNameSoiDel, mapSoNameHLvlSoiNameSoiDel, Trigger.New);  
        }   
    }
}
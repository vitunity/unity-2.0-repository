trigger updateAssociatedAccount on SVMXC__Site__c (after insert, after update, after delete) 
{
    Set<Id> accountIds = new Set<Id>();
    Set<Id> oldAccountIds = new Set<Id>();
    map<string,String> ErpSiteCodeMap = new Map<string,String>();
    if(Trigger.isUpdate)
    {
        for(SVMXC__Site__c site : Trigger.new)
        {
            if(site.SVMXC__Account__c != Trigger.OldMap.get(site.Id).SVMXC__Account__c)
            {
                accountIds.add(site.SVMXC__Account__c);
                ErpSiteCodeMap.put(site.SVMXC__Account__c,site.ERP_Site_Partner_Code__c);
                oldAccountIds.add(Trigger.OldMap.get(site.Id).SVMXC__Account__c);
            }
        }
        system.debug('accountIds1---'+accountIds);
        system.debug('oldAccountIds1---'+oldAccountIds);
    }
    
    if(Trigger.isInsert)
    {
        for(SVMXC__Site__c site : Trigger.new)
        {
            if(site.SVMXC__Account__c != null)
            {
                accountIds.add(site.SVMXC__Account__c);
                ErpSiteCodeMap.put(site.SVMXC__Account__c,site.ERP_Site_Partner_Code__c);
            }
        }
        system.debug('accountIds2---'+accountIds);
    }
    
    if(Trigger.isDelete)
    {
        for(SVMXC__Site__c site : Trigger.old)
        {
            oldAccountIds.add(site.SVMXC__Account__c);
        }
        system.debug('oldAccountIds2---'+oldAccountIds);
    }
    
    List<Account> accountsWithLocation = new List<Account>();
    List<Account> accountsWithoutLocation = new List<Account>();  
    system.debug('accountIds3---'+accountIds);
    system.debug('oldAccountIds3---'+oldAccountIds); 
    
    for(Account account : [select Id, Has_Location__c, ERP_Site_Partner_Code__c from Account where Id in :accountIds])
    {
        if(ErpSiteCodeMap.containskey(account.id))
        {
            account.Has_Location__c = true;
            account.ERP_Site_Partner_Code__c = ErpSiteCodeMap.get(account.id);
            accountsWithLocation.add(account);
        }
    }
    
    system.debug('accountsWithLocation---'+accountsWithLocation);
    map<string,list<SVMXC__Site__c>> accountsitemap = new map<string,list<SVMXC__Site__c>>();
    map<string,account> accountmap = new map<string,account>();
    
    for(Account account : [select Id, Has_Location__c, ERP_Site_Partner_Code__c, (select Id, ERP_Site_Partner_Code__c from SVMXC__Sites__r order by createddate desc) from Account where Id in :oldAccountIds ])
    {
        accountmap.put(account.id,account);
        list<SVMXC__site__c> emptylist = new list<SVMXC__site__c>();
        accountsitemap.put(account.id,emptylist);
        
        integer i = 0;
        for(SVMXC__Site__c site : account.SVMXC__Sites__r)
        {
           if(accountsitemap.containskey(account.id))
             {
               list<SVMXC__Site__c> s = new list<SVMXC__Site__c>();
               s = accountsitemap.get(account.id);
               s.add(site);
               accountsitemap.put(account.id,s);
             
             }
            
         }
    }
    
    if(trigger.isupdate)
    { 
        for(SVMXC__Site__c site : Trigger.new)
        {
            if(site.SVMXC__Account__c != Trigger.OldMap.get(site.Id).SVMXC__Account__c)
            {
                if(accountsitemap.containskey(Trigger.OldMap.get(site.Id).SVMXC__Account__c))
                {
                   list<SVMXC__site__c> sitelist = new list<SVMXC__site__c>();
                   sitelist = accountsitemap.get(Trigger.OldMap.get(site.Id).SVMXC__Account__c);
                   system.debug('sitelist------------'+sitelist);
                   system.debug('sitelist------------'+sitelist.size());
                   if(sitelist.size()>0)
                   {
                       SVMXC__site__c  s = new SVMXC__site__c();
                       s = sitelist[0];
                       system.debug('s-----'+s);
                       account acc = new account();
                       //system.debug('');
                       acc = accountmap.get(Trigger.OldMap.get(site.Id).SVMXC__Account__c);
                       acc.Has_Location__c = True;
                       acc.ERP_Site_Partner_Code__c = s.ERP_Site_Partner_Code__c;
                       accountsWithoutLocation.add(acc);
                   }
                   else
                   {
                        account acc = new account();
                        acc = accountmap.get(Trigger.OldMap.get(site.Id).SVMXC__Account__c);
                        acc.Has_Location__c = false;
                        acc.ERP_Site_Partner_Code__c = '';
                        accountsWithoutLocation.add(acc);
                   }
                }
            }
        }
    }
    
    if(trigger.isDelete)
    { 
        system.debug('isdelete if condition-----------'); 
        for(SVMXC__Site__c site : Trigger.old)
        {
          
            if(accountsitemap.containskey(site.SVMXC__Account__c))
            {
               list<SVMXC__site__c> sitelist = new list<SVMXC__site__c>();
               sitelist = accountsitemap.get(site.SVMXC__Account__c);
               system.debug('sitelist------------'+sitelist);
               system.debug('sitelist------------'+sitelist.size());
               if(sitelist.size()>0)
                {
                   SVMXC__site__c  s = new SVMXC__site__c();
                   s = sitelist[0];
                   system.debug('s-----'+s);
                   account acc = new account();
                   //system.debug('');
                   acc = accountmap.get(site.SVMXC__Account__c);
                   acc.Has_Location__c = True;
                   acc.ERP_Site_Partner_Code__c = s.ERP_Site_Partner_Code__c;
                   accountsWithoutLocation.add(acc);
               
                }
               else
                {
                    account acc = new account();
                    acc = accountmap.get(site.SVMXC__Account__c);
                    acc.Has_Location__c = false;
                    acc.ERP_Site_Partner_Code__c = '';
                    accountsWithoutLocation.add(acc);
                }
            }
        }
    }
    
    system.debug('accountsWithoutLocation3---'+accountsWithoutLocation);
    if(accountsWithLocation.size() > 0)
    {
        update accountsWithLocation;
    }
    if(accountsWithoutLocation.size() > 0)
    {
        update accountsWithoutLocation;
    }
}
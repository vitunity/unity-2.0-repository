trigger updateProbabilityPercentage on Opportunity (after insert, before update, after update){
    
    public string s;
    Public String UnifiedProb;
    Public String UnifiedFund;
    Public List<Opportunity> newOppList = new List<Opportunity>();
    
    
    //Update FiscalYear.12/14/2015.start.By Rakesh
    if((Trigger.isinsert && Trigger.isafter) || (Trigger.isupdate && Trigger.isafter)){
        if(LostFiscalYearRecursive.runOnce()){LostFiscalYear.setLostTime(trigger.newmap,trigger.oldmap);}
    }
    //Update FiscalYear.12/14/2015.End.By Rakesh
  
    
    
    if((Trigger.isinsert && Trigger.isbefore) || (Trigger.isupdate && Trigger.isbefore)){
        
        //rakesh
        Set<Id> setAccountIds = new Set<Id>();
        for(opportunity opp: Trigger.New){
            setAccountIds.add(opp.AccountId);
        }
        
       map<Id,Account> mapAccount = new map<Id,Account>([select Id,BillingCountry from Account where Id IN: setAccountIds]);
        //end
        
        //Any opportunity field updates in before trigger should go here
        OpportunityTriggerHandler.updateOpportunityFields(trigger.new, mapAccount);
        
        for(opportunity opp: Trigger.New){

            List<ProbabilityOnOpty__c> CustomSettings = ProbabilityOnOpty__c.getAll().Values();
            
            //Rakesh.1/14/2016.Start
            boolean isWithoutDealProb = false;
            Account oppAccount = mapAccount.get(opp.AccountId);
            system.debug('===='+oppAccount);
            if(oppAccount!=null){
            	if(oppAccount.BillingCountry == 'USA' || oppAccount.BillingCountry == 'Canada')
                	isWithoutDealProb = true;
            }
            //Rakesh.1/14/2016.End
            system.debug('===='+opp.Unified_Funding_Status__c);
            if(opp.Unified_Funding_Status__c != Null){
                UnifiedFund = string.valueof(opp.Unified_Funding_Status__c.replaceAll('[%]',''));
                
                //Rakesh.1/14/2016.Start
                if(!isWithoutDealProb && opp.Unified_Probability__c <> Null){
                    UnifiedFund = string.valueof(opp.Unified_Funding_Status__c.replaceAll('[%]',''));
                    UnifiedProb = string.valueof(opp.Unified_Probability__c.replaceAll('[%]',''));
                }
                //Rakesh.1/14/2016.End
                
                for(ProbabilityOnOpty__c CS: CustomSettings){

                    if(CS.UnifiedProbability__c == UnifiedProb && CS.FundingStatus__c == UnifiedFund){                    
                        opp.Probability = decimal.ValueOf(CS.ContributionToForecast__c);
                        break;                       
                    }else{                    
                        opp.Probability = 0;                        
                    }
                }
            }else{            
                opp.Probability = 0;            
            }
        }
    }
    
    //Logic added for contract uplift Project - Shital Bhujbal - 27 Aug 2018
    if((Trigger.isinsert && Trigger.isbefore) || (Trigger.isupdate && Trigger.isbefore)){
        for(Opportunity oppty: Trigger.New){
            if(Trigger.oldMap.get(oppty.Id).StageName != Trigger.newMap.get(oppty.Id).StageName && Trigger.newMap.get(oppty.Id).StageName=='7 - CLOSED WON'){
                newOppList.add(oppty);
            }
        }
        system.debug('***newOppList**********'+newOppList.size()+'*****'+newOppList);
        if(newOppList.size()>0){
            OpportunityUplift oppUplift = new OpportunityUplift();
            oppUplift.check_UpliftQuotes(newOppList);
        }
    }
}
/**
 * Update quote lookup on service contract and update booked service contract number on quote
 */
trigger updateQuote on SVMXC__Service_Contract__c (before insert) {
   
   Set<String> quoteNames = new Set<String>();
   Map<String,Id> quoteNameIdMap = new Map<String,Id>();
   Map<String,String> quoteServiceContracts = new Map<String,String>();
   
    for(SVMXC__Service_Contract__c  serviceContract : Trigger.new){
        if(serviceContract.EXT_Quote_Number__c != null){
            quoteNames.add(serviceContract.EXT_Quote_Number__c);
            
            if(quoteServiceContracts.containsKey(serviceContract.EXT_Quote_Number__c)){
                String serviceContractName = quoteServiceContracts.get(serviceContract.EXT_Quote_Number__c) + ', '+serviceContract.Name;
                quoteServiceContracts.put(serviceContract.EXT_Quote_Number__c,serviceContractName);
            }else{
                quoteServiceContracts.put(serviceContract.EXT_Quote_Number__c,serviceContract.Name);
            }
        }
    }
    
    List<BigMachines__Quote__c> quoteList = [Select Id,Name,SAP_Booked_Service__c  From BigMachines__Quote__c Where Name IN :quoteNames];
    
    for(BigMachines__Quote__c quote : quoteList){
        
        String bookedServiceContract = quote.SAP_Booked_Service__c+','; 
        
        quoteNameIdMap.put(quote.Name,quote.Id);
        quote.SAP_Booked_Service__c = !String.isBlank(quote.SAP_Booked_Service__c)
                                        ?(quote.SAP_Booked_Service__c+','+quoteServiceContracts.get(quote.Name))
                                        :quoteServiceContracts.get(quote.Name);
    }
    
    for(SVMXC__Service_Contract__c  serviceContract : Trigger.new){
        if(quoteNameIdMap.containsKey(serviceContract.EXT_Quote_Number__c)){
            serviceContract.Quote__c = quoteNameIdMap.get(serviceContract.EXT_Quote_Number__c);
        }      
    }
    
    if(!quoteList.isEmpty()){
        update quoteList;
    }
}
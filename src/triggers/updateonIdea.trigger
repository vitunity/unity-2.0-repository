/**************************************************************************\
    @ Author        : Rakesh Basani
    @ Date      : 08-Aug-2017
    @ Description   : STSK0012364 - Updating field User_Submitted__c Product Idea.
****************************************************************************/
trigger updateonIdea on Idea(before insert, before update) {
  for (Idea record:trigger.new) {
      record.User_Submitted__c = null;
      
          if(trigger.isInsert || (trigger.isUpdate && trigger.oldmap.get(record.id).Contact__c  <> record.Contact__c)){
              record.User_Submitted__c = userinfo.getUserId();
          }
      
  }
}
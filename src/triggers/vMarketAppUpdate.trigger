/**************************************************************************
    @ Author    : Nilesh Gorle
    @ Date      : 20-March-2017
    @ Description   : Trigger which Updates published date and isActive for app on app listing creation
****************************************************************************/
trigger vMarketAppUpdate on vMarket_Listing__c (after insert) {
	List<vMarket_App__c> appList = new List<vMarket_App__c>();

    for(vMarket_Listing__c listing:trigger.new) {
    	vMarket_App__c app = [SELECT Id, Published_Date__c, IsActive__c FROM vMarket_App__c WHERE Id=:listing.App__c limit 1];

		//update app with isActive and published date		
		app.Published_Date__c = Date.today();
		app.IsActive__c = true;
		appList.add(app);    	
    }
    if (!appList.isEmpty())
    	update appList;    
}
trigger vMarketAppWorkFlow on vMarketApprovalWorkflow__c (after insert) {
    vMarket_UpdateApp vmupdate = new vMarket_UpdateApp();

    List<ProcessInstance> processInstances = new List<ProcessInstance>(); 
    // Admin Email
    List<String> adminEmailList = new List<String>();
    adminEmailList.addAll(Label.vMarket_Admin_Emails.split(','));

    for(vMarketApprovalWorkflow__c workflow:trigger.new) {
        Id appId = workflow.vMarket_App__c;
        String Status = workflow.Status__c;
        vmupdate.updateFlagOnly(appId);

        
        /*if (Status=='Approved') {
            vmupdate.updateStatus(appId, Status);
            // Send approval mail to ADMIN here
            vmupdate.sendEmailToReviewer(adminEmailList, appId, Status);
        } else if (Status=='Rejected') {
            vmupdate.updateStatus(appId, Status);
            // Send rejection mail to ADMIN here
            vmupdate.sendEmailToReviewer(adminEmailList, appId, Status); // Mail to Admin
            vmupdate.sendEmailToReviewer(new List<String>(), appId, 'RejectedByRev'); // Mail to Developer
        } else {
            vmupdate.updateFlagOnly(appId); 
        }*/
        
        //system.debug('----------------------------------------------------------------');
        //if(!Test.isRunningTest()) {
            List<ProcessInstance> allProcessInstances = [SELECT Id FROM ProcessInstance Where TargetObjectId=:appId Order By CreatedDate];
            Set<Id> piIdSet = (new Map<Id, ProcessInstance>(allProcessInstances)).keySet();
            List<Id> piIdList = new List<Id>(piIdSet);

            ProcessInstanceStep prStep = [Select Comments, CreatedDate, ActorId From ProcessInstanceStep
                                            Where StepStatus='Started' AND StepNodeId=null  AND ProcessInstanceId In : piIdList
                                            Order By CreatedDate Desc limit 1];
            DateTime firstCreatedDate = prStep.CreatedDate.addSeconds(-40);
            DateTime secondCreatedDate = prStep.CreatedDate.addSeconds(10);
            processInstances = [SELECT Id, TargetObjectId, ProcessDefinitionId, CompletedDate, Status, LastActorId, (SELECT Id, StepStatus, ActorId, OriginalActorId, Comments FROM Steps Order By SystemModstamp Desc) 
                                FROM ProcessInstance 
                                Where TargetObjectId=:appId AND CreatedDate >: firstCreatedDate  AND CreatedDate <: secondCreatedDate];


            String reviewerTableTBodyStatus = '';
            for(ProcessInstance pi:processInstances) {
                system.debug('-------STATUS------'+String.valueOf(pi.Status));

                User submitUser = [select Id, username, firstname, lastname from User where Id=: pi.Steps[0].ActorId];
                User approverUser = [select Id, username, firstname, lastname from User where Id=: pi.Steps[0].OriginalActorId];

                if (pi.Steps[0].StepStatus!='Started')
                    reviewerTableTBodyStatus += ('<tr><td>'+submitUser.firstname+ ' ' +submitUser.lastname+ '</td><td>'+ approverUser.firstname + ' ' + approverUser.lastname +'</td><td>'+ pi.Status +'</td><td>'+pi.Steps[0].Comments+'</td></tr>');
                else
                    reviewerTableTBodyStatus += ('<tr><td>'+submitUser.firstname+ ' ' +submitUser.lastname+ '</td><td>'+ approverUser.firstname + ' ' + approverUser.lastname +'</td><td>'+ pi.Status +'</td><td></td></tr>');
            }
        
        
            vMarket_App__c app = [select Id, Name from vMarket_App__c where Id =: workflow.vMarket_App__c limit 1];

            String reviewerInfoTable = '<table border=\'1\'><thead><tr><th>Assigned By</th><th>Assigned To</th><th>Status</th><th>Comments</th></tr></thead><tbody>'+ reviewerTableTBodyStatus +'</tbody></table>';
            String emailSubject = 'vMarket Admin Review Status';
            String reviewStatus = 'Under Review';
            String appPageUrl = 'https://' + URL.getSalesforceBaseUrl().getHost() + '/apex/vMarket_AdminApproval';
            String appSandboxUrl = 'https://' + URL.getSalesforceBaseUrl().getHost() + '/' + appId;

            String emailBody = 'Hello Admin,<br/><br/><b>App Name: </b>'+app.Name+'<br/><br/><b>Current Status: </b>'+ reviewStatus +'<br/><br/><b>App URL: </b>'+ appPageUrl + ' OR ' + appSandboxUrl + '<br/><br/><b>Reviewer Status:</b><br/><br/>'+reviewerInfoTable;
            vmupdate.sendEmailToAdminForReview(adminEmailList, emailSubject, emailBody);
        //}
    }
}
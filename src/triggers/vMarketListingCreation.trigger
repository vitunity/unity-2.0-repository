/**************************************************************************
    @ Author    : Nilesh Gorle
    @ Date      : 20-March-2017
    @ Description   : Trigger which insert app listing on app published
****************************************************************************/
trigger vMarketListingCreation on vMarket_App__c (before insert,before update,after update,after insert) {

// For Preventing Code Injections
    if(Trigger.isBefore && Trigger.isUpdate)
    {
        for(vMarket_App__c app : Trigger.new)
        {
        if(String.isNotBlank(Trigger.oldMap.get(app.id).ApprovalStatus__c) && String.isNotBlank(app.ApprovalStatus__c))
        {
        if(!Trigger.oldMap.get(app.id).ApprovalStatus__c.equals('Submitted') && app.ApprovalStatus__c.equals('Submitted')) app.Date_of_Submission__c = System.now();
        if((!Trigger.oldMap.get(app.id).ApprovalStatus__c.equals('Published') && app.ApprovalStatus__c.equals('Published')) || (!Trigger.oldMap.get(app.id).ApprovalStatus__c.equals('Rejected') && app.ApprovalStatus__c.equals('Rejected'))) app.Date_of_Approve_Reject__c = System.now();
        if(!Trigger.oldMap.get(app.id).ApprovalStatus__c.equals('Info Requested') && app.ApprovalStatus__c.equals('Info Requested')) app.Date_of_Info_requested__c = System.now();
        if(!Trigger.oldMap.get(app.id).ApprovalStatus__c.equals('Under Review') && app.ApprovalStatus__c.equals('Under Review')) app.Date_of_Approval_reuqest__c = System.now();
        }
        }
    
        /*for(vMarket_App__c app : Trigger.new)
        {
        //System.debug('-------desc'+app);
        Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
        if(String.isNotBlank(app.Name))
        {
        Matcher matcher = nonAlphanumeric.matcher(app.Name);
        app.Name = matcher.replaceAll('');
        }
        if(String.isNotBlank(app.PublisherName__c))
        {
        Matcher matcher1 = nonAlphanumeric.matcher(app.PublisherName__c);
        app.PublisherName__c = matcher1.replaceAll('');
        }
        app.Name = app.Name.escapeHtml4();
        app.PublisherName__c= app.PublisherName__c!=null ? app.PublisherName__c.escapeHtml4():app.PublisherName__c;
        app.bug_fix_features__c = app.bug_fix_features__c!=null ? app.bug_fix_features__c.escapeHtml4():app.bug_fix_features__c;
        app.Expert_Opinion__c= app.Expert_Opinion__c!=null ?  app.Expert_Opinion__c.escapeHtml4() : app.Expert_Opinion__c;
        //app.Key_features__c= app.Key_features__c!=null ? app.Key_features__c.escapeHtml4() :app.Key_features__c;
        app.Keywords__c= app.Keywords__c!=null ? app.Keywords__c.escapeHtml4():app.Keywords__c;
        app.K_Number__c= app.K_Number__c!=null ? app.K_Number__c.escapeHtml4():app.K_Number__c;      
        app.Long_Description__c= app.Long_Description__c!=null ? app.Long_Description__c.escapeHtml4():app.Long_Description__c;
        app.Publisher_Developer_Support__c= app.Publisher_Developer_Support__c!=null ? app.Publisher_Developer_Support__c.escapeHtml4() : app.Publisher_Developer_Support__c;
        
        app.Short_Description__c= app.Short_Description__c!=null ? app.Short_Description__c.escapeHtml4():app.Short_Description__c;
        if(app.Short_Description__c!=null && app.Short_Description__c.length()>250) app.Short_Description__c = app.Short_Description__c.substring(0,250);
        }*/
    }

    if(Trigger.isAfter && Trigger.isUpdate)
    {
    List<vMarket_Listing__c> listingObjList = new List<vMarket_Listing__c>();
    vMarketApprovalWorkflow__c workflowObj = new vMarketApprovalWorkflow__c();

    String Rejected_Status = 'Rejected';
    String Approved_Status = 'Approved';

    List<ProcessInstance> processInstances = new List<ProcessInstance>(); 
    Boolean rejected_flag = false;
    Boolean approved_flag = true;
    String rejected_comment;
    Id rejectedUserId;
    Id appId;

    for(vMarket_App__c app:trigger.new) {
        appId = app.Id;
        if (app.ApprovalStatus__c=='Submitted') 
        {
        insert vMarket_StripeAPIUtil.createLog('Developer','Dev Submitted','None','Developer Role Ends',app);
        }
        if (app.ApprovalStatus__c=='Published') {
        
        vMarket_StripeAPIUtil.createsubscriptionplans(app.id,app.Subscription_Price__c);
        
            Integer rows = [SELECT COUNT() FROM vMarket_Listing__c WHERE App__c=:appId];
            
            if (rows==0) {
                vMarket_Listing__c listingObj = new vMarket_Listing__c();
                listingObj.Name = app.Name;
                listingObj.OwnerId = app.OwnerId;
                listingObj.App__c = app.Id;
                listingObjList.add(listingObj);
            }
        }

        system.debug('-----------------------WORKFLOW START HERE-----------------------------------------');
        system.debug('*****'+app.name);
        system.debug('*****'+app.price__C);
        system.debug('*****'+app.Long_Description__c);
        system.debug('*****'+app.short_Description__c);
        system.debug(app.ApprovalStatus__c);
        if (app.ApprovalStatus__c=='Under Review' && app.isApprovedOrRejected__c) {
            /*system.debug('----------------------------------------------------------------');
            List<ProcessInstance> allProcessInstances = [SELECT Id FROM ProcessInstance Where TargetObjectId=:appId Order By CreatedDate];
            Set<Id> piIdSet = (new Map<Id, ProcessInstance>(allProcessInstances)).keySet();
            List<Id> piIdList = new List<Id>(piIdSet);

            ProcessInstanceStep prStep = [Select Comments, CreatedDate, ActorId From ProcessInstanceStep
                                            Where StepStatus='Started' AND StepNodeId=null  AND ProcessInstanceId In : piIdList
                                            Order By CreatedDate Desc limit 1];
            DateTime firstCreatedDate = prStep.CreatedDate.addSeconds(-40);
            DateTime secondCreatedDate = prStep.CreatedDate.addSeconds(10);
            processInstances = [SELECT Id, TargetObjectId, ProcessDefinitionId, CompletedDate, Status, (SELECT Id, StepStatus, Comments FROM Steps Order By SystemModstamp Desc) 
                                                  FROM ProcessInstance 
                                                  Where TargetObjectId=:app.Id AND CreatedDate >: firstCreatedDate  AND CreatedDate <: secondCreatedDate];

            for(ProcessInstance pi:processInstances) {
                system.debug('-------STATUS------'+String.valueOf(pi.Status));
                if(pi.Status=='Rejected') {
                    rejected_flag = true;
                    
                    ProcessInstanceStep rejectedStep = [Select Comments, CreatedDate, ActorId From ProcessInstanceStep Where StepStatus='Rejected' 
                                Order By CreatedDate Desc limit 1];
                    rejected_comment = rejectedStep.Comments;
                    rejectedUserId = rejectedStep.ActorId;
                    break;
                } else if (pi.Status=='Approved') {
                    approved_flag = (approved_flag && true);
                } else {
                    approved_flag = (approved_flag && false);
                }
            }

            workflowObj.vMarket_App__c = appId;
            if  (rejected_flag) {
                system.debug('======REJECTED ==========');
                Set<Id> pIds = new Set<Id>();
                for(ProcessInstance pi:processInstances) {
                    pIds.add(pi.Id);
                }
                Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
                Approval.ProcessResult[] allReq = New Approval.ProcessResult[]{}; 
                for (Id pInstanceWorkitemsId:pInstanceWorkitems){
                    Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                    req.setComments(rejected_comment);
                    req.setAction('Reject');
                    req.setWorkitemId(pInstanceWorkitemsId);
                    system.debug('================'+req);
                    Approval.ProcessResult result =  Approval.process(req);
                }
                workflowObj.Status__c = Rejected_Status;
            } else if (approved_flag) {
                system.debug('======APPROVED ==========');
                workflowObj.Status__c = Approved_Status;
            }
            insert workflowObj;*/


            /* Nilesh - on each update of reviewer = mail will goes to admin about app status */
            workflowObj.vMarket_App__c = appId;
            workflowObj.Status__c = 'Under Review';
            insert workflowObj;

            system.debug('-----------------------WORKFLOW END HERE-----------------------------------------');
        }
    }

    if (!listingObjList.isEmpty())
        insert listingObjList;
        
        
        }
        
    if(Trigger.isAfter)
    {
        
   List<VMarket_Country_App__c> countryApps = new List<VMarket_Country_App__c>();
   List<VMarket_Country_App__c> countryAppsToUpdate = new List<VMarket_Country_App__c>(); 
   List<VMarket_Country_App__c> countryAppsToUpdateSubmitted = new List<VMarket_Country_App__c>(); 
   
    for(vmarket_app__c app : Trigger.new)
    {
    if(String.isNotBlank(app.ApprovalStatus__c) && !app.ApprovalStatus__c.equals('Draft') && String.isNotBlank(app.Countries__c))
    {
    Set<String> newCountries = new Set<String>(app.Countries__c.split(';',-1));
    Set<String> addCountries = new Set<String>();
    
    System.debug('*******newcountries'+newCountries);
    if(Trigger.isupdate && String.isNOTBLANK(Trigger.oldMap.get(app.id).Countries__c))
    {
    Set<String> oldCountries = new Set<String>(Trigger.oldMap.get(app.id).Countries__c.split(';',-1));
    Boolean newthings = oldCountries.containsAll(newCountries);
    
    //system.debug('oldCountries---'+oldCountries);
    addCountries = newCountries.clone();
    //system.debug('addCountries ---1'+addCountries);
    addCountries.removeAll(oldCountries);
    //system.debug('addCountries ---2'+addCountries);
    
    Set<String> removeCountries = oldCountries.clone();
    //system.debug('removeCountries---1'+removeCountries);
    removeCountries.removeAll(newCountries);
    //system.debug('removeCountries---2'+removeCountries);
    
    for(VMarket_Country_App__c capp : [Select Country__c,Status__c, VMarket_Approval_Status__c ,VMarket_App__c from VMarket_Country_App__c where Status__c='Active' and VMarket_App__c=:app.id])
    {
    
     System.debug('*******newcountries'+app.ApprovalStatus__c+capp.status__C);
    
    if(removeCountries.contains(capp.Country__c))
    {
        capp.Status__c='Disabled';
        countryAppsToUpdate.add(capp);    
    }
    else if((app.ApprovalStatus__c.equals('Submitted')||app.ApprovalStatus__c.equals('Published'))&& String.isNOtBlank(capp.VMarket_Approval_Status__c) && capp.VMarket_Approval_Status__c.equals('Draft')) 
    {
        capp.VMarket_Approval_Status__c='Submitted';
        countryAppsToUpdateSubmitted.add(capp); 
    
    }
    
    
    }
    
    }
    else addCountries.addAll(newCountries);
    
    System.debug('*******addcountries'+addCountries);
    for(String country : addCountries)
    {
        if(string.isBlank(country))
        countryApps.add(new VMarket_Country_App__c(VMarket_App__c=app.id,Status__c='Active',Country__c=country));
    }
    
    }
    }   
    
    if(countryApps.size()>0) Insert countryApps;
    //System.debug('*******countryAppsToUpdate'+countryAppsToUpdate);
    if(countryAppsToUpdate.size()>0) update countryAppsToUpdate;
    //System.debug('*******countryAppsToUpdateSubmitted'+countryAppsToUpdateSubmitted);
    if(countryAppsToUpdateSubmitted.size()>0) update countryAppsToUpdateSubmitted; 
        }

}
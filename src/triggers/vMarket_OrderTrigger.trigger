trigger vMarket_OrderTrigger on vMarketOrderItem__c (before insert,before update,after update) {

// For Preventing Code Injections
if(Trigger.isBefore)
{
    for(vMarketOrderItem__c order : Trigger.new)
    {
        order.ShippingAddress__c = order.ShippingAddress__c!=null ? order.ShippingAddress__c.escapeHtml4():order.ShippingAddress__c;
        order.ZipCode__c = order.ZipCode__c!=null ? order.ZipCode__c.escapeHtml4():order.ZipCode__c;
    }
}

if(Trigger.isAfter && Trigger.isUpdate)
{
    if(!vMarket_StripeAccount.orderTriggerStarted)  {
        vMarket_StripeAccount.orderTriggerStarted = true;
        List<String> orderTransfers = new List<String>();
        for(vMarketOrderItem__c order : Trigger.new) {
            try {
                if(order.Number_of_Line_Items__c > 1 && String.isNotBlank(order.Status__c)  && order.Status__c.equals(Label.vMarket_Success)) {
                    vMarketOrderItem__c old_order= Trigger.oldMap.get(order.Id);
        
                    if(String.isBlank(old_order.Status__c) || !old_order.Status__c.equals(order.Status__c) || Test.isRunningTest()) {
        
                        for(vMarketOrderItemLine__c lineItem : [Select id, vMarket_App__r.Developer_Stripe_Acc_Id__c, Tax__c,vMarket_App__r.Is_Medical_Device__c, Price__c 
                                                                from vMarketOrderItemLine__c where vMarket_Order_Item__c =:order.Id]) {
                            if(lineItem.Price__c > 0) {
                                system.debug('============== lineItem ============= ' + lineItem);
                                Map<String, String> requestBody = new Map<String, String>();
                                requestBody.put('currency', 'USD');
                                Integer tax = 0;
                                //if(lineItem.tax__c!=null) tax =(Integer)lineItem.tax__c; 
                                System.debug('---pricetax--'+lineItem.Price__c+'--'+tax);
                                Integer price = (Integer)lineItem.Price__c*100;
                                //vMarket_Tax__c vMarketTax = vMarket_Tax__c.getInstance('tax');
                                Integer application_fee = 0;//Integer.valueOf((price * vMarketTax.Tax__c)/100);
                                
                                if(!lineItem.vMarket_App__r.Is_Medical_Device__c)
                                {    
                                vMarket_Tax__c vMarketTax = vMarket_Tax__c.getInstance('tax');
                                application_fee = Integer.valueOf((price * vMarketTax.Tax__c)/100);
                                }
                                else if(lineItem.vMarket_App__r.Is_Medical_Device__c)
                                {
                                vMarket_Tax__c vMarketMedicalTax = vMarket_Tax__c.getInstance('medical_tax');
                                application_fee = Integer.valueOf((price * vMarketMedicalTax.Tax__c)/100);
                                } 
                                System.debug('--price---'+price);
                                System.debug('---appfee--'+application_fee);
                                 price = price -application_fee; 
                                requestBody.put('amount', String.valueOf(price));
                                requestBody.put('description', order.Id+'_'+lineItem.Id);
                                requestBody.put('destination',lineItem.vMarket_App__r.Developer_Stripe_Acc_Id__c);
                                requestBody.put('transfer_group',order.Id);
                                //requestBody.put('application_fee',string.valueOf(application_fee));
                                orderTransfers.add(JSON.serialize(requestBody));
                            }    
                        }
                    }
                }
                if(!System.Test.isRunningTest())
                vMarket_StripeAccount.createIndividualTransfers(orderTransfers,false,order.tax__c);
            } catch(Exception e) {
                order.addError('Error Occured while Making Individual Payments'+e.getMessage());
                vMarket_StripeAPIUtil.sendEmailWithAttachment(vMarket_StripeAPIUtil.populateAdminRecipients(),'Error Occured while Making Individual Payments',e.getMessage(),'VMS Error');
                vMarket_LogController.createCodeErrorLogs('Code Failed in vMarket_OrderTrigger',e.getMessage());
            }
        }
    }

}    
    
}
<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Ready_to_be_converted_from_Prospect_to_Customer</fullName>
        <description>Ready to be converted from Prospect to Customer</description>
        <protected>false</protected>
        <recipients>
            <recipient>vms-nshetty@varian.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vms-spuppal0@varian.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Email_Template_for_Customer</template>
    </alerts>
    <fieldUpdates>
        <fullName>Survey_Candidate_Set_False</fullName>
        <field>Survey_Candidate__c</field>
        <literalValue>0</literalValue>
        <name>Survey Candidate Set False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Prospect to Customer</fullName>
        <actions>
            <name>Ready_to_be_converted_from_Prospect_to_Customer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Ready_to_be_Converted_To_Customer__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Survey Candidate Set False</fullName>
        <actions>
            <name>Survey_Candidate_Set_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Pending Removal</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

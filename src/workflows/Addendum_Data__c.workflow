<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Addendum_DefaultMessage</fullName>
        <field>Message__c</field>
        <formula>&quot;Contact USPA-Product Commercialization.&quot;</formula>
        <name>Addendum_DefaultMessage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Exclude_from_EPOT_update</fullName>
        <field>Exclude_from_EPOT__c</field>
        <literalValue>1</literalValue>
        <name>Exclude from EPOT update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Addendum_DefaultMessage</fullName>
        <actions>
            <name>Addendum_DefaultMessage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Addendum_Data__c.Message__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Exclude from EPOT update</fullName>
        <actions>
            <name>Exclude_from_EPOT_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Addendum_Data__c.Exclude_from_EPOT__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_not_closed_within_48_hours</fullName>
        <description>Case not closed within 48 hours</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>pranjul.maindola@varian.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>customerservice-anzqa@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Helpdesk_Email_Templates/TR_Email_For_Case_Open_48_hours_of_Modification</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_on_CustomerService_case_closure</fullName>
        <description>Email notification on CustomerService case closure</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customerservice-anzqa@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Helpdesk_Email_Templates/Notification_On_CustomerService_Case_Closure</template>
    </alerts>
    <alerts>
        <fullName>No_case_owner_for_24_hours</fullName>
        <description>No case owner for 24 hours</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>Customer_Service_ANZ</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>customerservice-anzqa@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Helpdesk_Email_Templates/TR_Email_For_Case_Open_for_24_hours</template>
    </alerts>
    <alerts>
        <fullName>Notification_on_CustomerService_RecordType_case_creaion</fullName>
        <description>Notification on CustomerService RecordType case creaion</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customerservice-anzqa@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Helpdesk_Email_Templates/Notification_On_CustomerService_Case_creation</template>
    </alerts>
    <alerts>
        <fullName>SR_Summary_Service_Report_to_Customer_for_FieldService</fullName>
        <ccEmails>supportqa@7fmgjydpt85zsdfjzhk84x9d167anqnvgbok1er8ul7m9by8q.e-5f3xkeac.cs15.case.sandbox.salesforce.com</ccEmails>
        <description>SR Summary Service Report to Customer for FieldService</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>supportqa@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Survey_Email_Templates/Field_Service_Survey_Template</template>
    </alerts>
    <rules>
        <fullName>Case Customer Service Notification on Creation</fullName>
        <actions>
            <name>Notification_on_CustomerService_RecordType_case_creaion</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Notification to contact on customer service case creation.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case_not_closed_within_48_hours</fullName>
        <actions>
            <name>Case_not_closed_within_48_hours</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email to owner/owner manager after 48 hours if owner is a user.</description>
        <formula>AND( 
NOT(ISPICKVAL(Status,&apos;Closed&apos;)), 
RecordType.DeveloperName == &apos;Customer_Service&apos;, 
if((now() &lt;&gt; LastModifiedDate),if(LastModifiedDate &lt;= now()-2,true,false),true) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>No case owner for 24 hours</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Customer Service - ANZ</value>
        </criteriaItems>
        <description>sends notification mail to the Customerservice public group when case is not assigned for 24 hours.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>No_case_owner_for_24_hours</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Created_Date_Time__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notification email on CustomerService case Closure</fullName>
        <actions>
            <name>Email_notification_on_CustomerService_case_closure</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Service</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Notification email on Customer Service case closure.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

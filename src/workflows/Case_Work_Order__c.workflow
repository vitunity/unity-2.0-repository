<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_All_Case_Work_Order_Closed_Approval_FOA_and_SC</fullName>
        <description>Email Alert : All Case Work Order Closed Approval FOA and SC</description>
        <protected>false</protected>
        <recipients>
            <field>FOA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SC_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Work_Order_Email_Template/OOT_Approval_Email_FOA_SC</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_OOT_CWO_created_reminder_email_45_Day</fullName>
        <description>Email Alert : OOT CWO created reminder email 45 Day</description>
        <protected>false</protected>
        <recipients>
            <field>FOA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SC_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Case_Work_Order_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Work_Order_Email_Template/OOT_Day_45_Create_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_OOT_CWO_created_reminder_email_60_Day</fullName>
        <description>Email Alert : OOT CWO created reminder email 60 Day</description>
        <protected>false</protected>
        <recipients>
            <field>District_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>FOA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SC_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Work_Order_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Work_Order_Email_Template/OOT_Day_60_Create_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_OOT_CWO_created_reminder_email_day_1_and_day_15</fullName>
        <description>Email Alert : OOT CWO created reminder email day 1 and day 15</description>
        <protected>false</protected>
        <recipients>
            <field>FOA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SC_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Work_Order_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Work_Order_Email_Template/OOT_Day_1_and_day_15_Create_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_OOT_Case_and_Case_Work_Orders_Day_1_Email</fullName>
        <description>Email Alert : OOT Case and Case Work Orders Day 1 Email SM</description>
        <protected>false</protected>
        <recipients>
            <field>FOA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SC_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Work_Order_Email_Template/Email_1_To_SM_FOA_SC</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_OOT_Case_and_Case_Work_Orders_Day_1_Email_FE</fullName>
        <description>Email Alert : OOT Case and Case Work Orders Day 1 Email FE</description>
        <protected>false</protected>
        <recipients>
            <field>FOA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SC_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Work_Order_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Work_Order_Email_Template/Email_1_To_FE_FOA_SC</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_Send_Email_OOT_Case_and_Case_Work_Orders_are_closed</fullName>
        <description>Email Alert : Send Email (OOT) Case and Case Work Orders are closed</description>
        <protected>false</protected>
        <recipients>
            <field>District_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>FOA_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SC_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Case_Work_Order_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Work_Order_Email_Template/OOT_case_Case_Work_Orders_are_closed</template>
    </alerts>
    <rules>
        <fullName>Send Email OOT Case and Case Work Orders are closed approval FOA and SC</fullName>
        <actions>
            <name>Email_Alert_All_Case_Work_Order_Closed_Approval_FOA_and_SC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Approval email to FOA and SC</description>
        <formula>AND(((Case__r.Number_of_OOT_Case_Work_Orders__c - Case__r.No_of_OOT_Case_WO_Verified_Completed__c) = 1 ), OR(ISPICKVAL( Resolution__c , &apos;Verified No Impact&apos;), ISPICKVAL( Resolution__c , &apos;Investigation Complete&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email if the Resolution on the Case Work Order is changed from Investigation Complete to Open</fullName>
        <actions>
            <name>Email_Alert_OOT_Case_and_Case_Work_Orders_Day_1_Email_FE</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(ISPICKVAL(PRIORVALUE(Resolution__c),&quot;Investigation Complete&quot;)) || (ISPICKVAL(PRIORVALUE(Resolution__c),&quot;Verified No Impact&quot;))&amp;&amp;(ISPICKVAL(Resolution__c,&quot;Open&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to technician OOT Case and Case Work Order is Investigation Completed</fullName>
        <actions>
            <name>Email_Alert_Send_Email_OOT_Case_and_Case_Work_Orders_are_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case_Work_Order__c.Follow_Work_Order_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Time based workflow to send email At day 1 15 45 and 60 after OOT Case Work Order Created</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case_Work_Order__c.Resolution__c</field>
            <operation>equals</operation>
            <value>Open,Investigation Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_OOT_CWO_created_reminder_email_60_Day</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case_Work_Order__c.CreatedDate</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_OOT_CWO_created_reminder_email_day_1_and_day_15</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Email_Alert_OOT_Case_and_Case_Work_Orders_Day_1_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case_Work_Order__c.CreatedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_OOT_CWO_created_reminder_email_day_1_and_day_15</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case_Work_Order__c.CreatedDate</offsetFromField>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_OOT_CWO_created_reminder_email_45_Day</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case_Work_Order__c.CreatedDate</offsetFromField>
            <timeLength>45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>

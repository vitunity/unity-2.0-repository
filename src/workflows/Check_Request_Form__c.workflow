<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Check_Request_Form_10K_Approval_Reminder</fullName>
        <ccEmails>steve.hiller@varian.com</ccEmails>
        <description>Check Request Form 10K Approval Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Check_Request_10K_Approvers</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Check_Request_15K_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_request_form_approval_reminder</template>
    </alerts>
    <alerts>
        <fullName>Check_Request_Form_5K_Approval_Reminder</fullName>
        <description>Check Request Form 5K Approval Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Check_Request_5K_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_request_form_approval_reminder</template>
    </alerts>
    <alerts>
        <fullName>Check_Request_Form_Above_10K_Approval_Reminder</fullName>
        <description>Check Request Form 15K Approval Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Check_Request_10K_Approvers</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Check_Request_15K_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_request_form_approval_reminder</template>
    </alerts>
    <alerts>
        <fullName>Check_Request_Form_Above_15K_Approval_Reminder</fullName>
        <description>Check Request Form Above 15K Approval Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Check_Request_10K_Approvers</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Check_Request_15K_Approvers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_request_form_approval_reminder</template>
    </alerts>
    <alerts>
        <fullName>Check_Request_Form_above_15K_Approval_Level_2_Reminder</fullName>
        <ccEmails>steve.hiller@varian.com</ccEmails>
        <description>Check Request Form above 15K Approval Level 2 Reminder</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_Request_Form_Approval_Level_2</template>
    </alerts>
    <alerts>
        <fullName>HCP_10K_Check_Request_From_Approved</fullName>
        <description>HCP 10K Check Request From Approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>heidi.long@varian.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_request_approval_completion_notification</template>
    </alerts>
    <alerts>
        <fullName>HCP_15K_Check_Request_Form_Approval_Email</fullName>
        <description>HCP 15K Check Request Form Approval Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>heidi.long@varian.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_request_approval_completion_notification</template>
    </alerts>
    <alerts>
        <fullName>HCP_5K_Check_Request_From_Approved</fullName>
        <description>HCP 5K Check Request From Approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>heidi.long@varian.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_request_approval_completion_notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Check_Request_Form_Rejected</fullName>
        <description>Send Email when Check Request Form Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_request_rejection_notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Check_Request_From_Rejected</fullName>
        <description>Send Email when Check Request Form Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Check_request_rejection_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>ReSet_CheckRequestForm_Status_as_NULL</fullName>
        <field>Status__c</field>
        <name>ReSet CheckRequestForm Status as NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReSet_CheckRequestFrom_Status_as_NULL</fullName>
        <field>Status__c</field>
        <name>ReSet CheckRequestForm Status as NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReSet_Submited_Date_on_CheckRequestForm</fullName>
        <field>Submited_Date__c</field>
        <name>ReSet Submitted Date on CheckRequestForm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReSet_Submitted_Date_on_CheckRequestForm</fullName>
        <field>Submited_Date__c</field>
        <name>ReSet Submitted Date on CheckRequestForm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ReSet_Submitted_Date_on_CheckRequestFrom</fullName>
        <field>Submited_Date__c</field>
        <name>ReSet Submitted Date on CheckRequestForm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Check_Request_Form_as_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Check Request Form as Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Check_Request_Form_as_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Check Request Form as Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Check_Request_Form_as_Submited</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Set Check Request Form as Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Check_Request_Form_as_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Set Check Request Form as Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Check_Request_From_as_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Check Request Form as Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Check_Request_From_as_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Check Request Form as Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Check_Request_From_as_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Set Check Request Form as Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submited_Date_on_Check_Request_Form</fullName>
        <field>Submited_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Submitted Date on Check Request Form</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_Date_on_Check_Request_Form</fullName>
        <field>Submited_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Submitted Date on Check Request Form</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Submitted_Date_on_Check_Request_From</fullName>
        <field>Submited_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Submitted Date on Check Request Form</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>10K Check Request Form Approval Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Check_Request_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Check_Request_Form__c.Total_Amount__c</field>
            <operation>greaterThan</operation>
            <value>&quot;USD 5,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Check_Request_Form__c.Total_Amount__c</field>
            <operation>lessOrEqual</operation>
            <value>&quot;USD 10,000&quot;</value>
        </criteriaItems>
        <description>Email Reminder need to be send to Approve the Check request Form after 48 Hours and then after 24 Hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Request_Form_10K_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Check_Request_Form__c.Submited_Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Request_Form_10K_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Check_Request_Form__c.Submited_Date__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>15K Check Request Form Approval Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Check_Request_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Check_Request_Form__c.Total_Amount__c</field>
            <operation>greaterThan</operation>
            <value>&quot;USD 10,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Check_Request_Form__c.Total_Amount__c</field>
            <operation>lessOrEqual</operation>
            <value>&quot;USD 15,000&quot;</value>
        </criteriaItems>
        <description>Email Reminder need to be send to Approve the Check request Form first after 48 Hours and then after 24 Hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Request_Form_Above_10K_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Check_Request_Form__c.Submited_Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Request_Form_Above_10K_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Check_Request_Form__c.Submited_Date__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>5K Check Request Form Approval Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Check_Request_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Check_Request_Form__c.Total_Amount__c</field>
            <operation>lessOrEqual</operation>
            <value>&quot;USD 5,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Check_Request_Form__c.Total_Amount__c</field>
            <operation>greaterThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <description>Email Reminder need to be send to Approve the Check request Form first after 48 Hours and then after 24 Hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Request_Form_5K_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Check_Request_Form__c.Submited_Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Request_Form_5K_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Check_Request_Form__c.Submited_Date__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Above 15K Check Request Form Approval Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Check_Request_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Check_Request_Form__c.Total_Amount__c</field>
            <operation>greaterThan</operation>
            <value>&quot;USD 15,000&quot;</value>
        </criteriaItems>
        <description>Email Reminder need to be send to Approve the Check request Form after 48 Hours and then after 24 Hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Request_Form_Above_15K_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Check_Request_Form__c.Submited_Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Request_Form_Above_15K_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Check_Request_Form__c.Submited_Date__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>

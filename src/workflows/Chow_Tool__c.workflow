<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Chow_Tool_Escalation_Email_Alert</fullName>
        <description>Chow Tool Escalation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Assignee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ChowTool/ChowToolFINALREMINDER</template>
    </alerts>
    <alerts>
        <fullName>Chow_Tool_Reminder_Email_Alert</fullName>
        <description>Chow Tool Reminder Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Assignee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ChowTool/ChowToolREMINDER</template>
    </alerts>
    <rules>
        <fullName>Chow Tool Escalation Email Rule</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Chow_Tool__c.Request_Status__c</field>
            <operation>notEqual</operation>
            <value>Order Amendment Submitted for SAP Modification,Completed-Updated in SAP,Denied,Duplicate</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Chow_Tool_Escalation_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Chow_Tool__c.AssignmentDateTest__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Chow Tool Reminder Email Rule</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Chow_Tool__c.Request_Status__c</field>
            <operation>notEqual</operation>
            <value>Order Amendment Submitted for SAP Modification,Completed-Updated in SAP,Denied,Duplicate</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Chow_Tool_Reminder_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Chow_Tool__c.AssignmentDateTest__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>

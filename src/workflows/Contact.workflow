<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Annual Relationship History</fullName>
        <actions>
            <name>SurveyQuarterSetNone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Survey_last_modified_by</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Survey_last_modified_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>STSK0012610 - Relationship Survey</description>
        <formula>AND( 
ISCHANGED( Annual_Relationship_Survey__c ), 
(PRIORVALUE( Annual_Relationship_Survey__c )) = True 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

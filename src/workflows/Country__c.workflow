<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_when_a_new_country_is_created</fullName>
        <description>Email when a new country is created</description>
        <protected>false</protected>
        <recipients>
            <recipient>fiaz.aziz@varian.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>john.ma@varian.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>naveen.shetty@varian.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rohit.andrade@varian.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vms-rkaur@varian.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>donotreply@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/New_Country_Created</template>
    </alerts>
    <rules>
        <fullName>New Country Created</fullName>
        <actions>
            <name>Email_when_a_new_country_is_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Country__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send email when new country is created</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Country__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

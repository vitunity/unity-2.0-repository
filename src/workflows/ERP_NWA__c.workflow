<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Interface_Status</fullName>
        <description>LMS Interface status update</description>
        <field>Interface_Status__c</field>
        <formula>&quot;Processed&quot;</formula>
        <name>Interface Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>LMS Interface field update</fullName>
        <actions>
            <name>Interface_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (4 AND 5 AND 6) OR (7 AND 8 AND 9)</booleanFilter>
        <criteriaItems>
            <field>ERP_NWA__c.LMS_Status__c</field>
            <operation>equals</operation>
            <value>REL</value>
        </criteriaItems>
        <criteriaItems>
            <field>ERP_NWA__c.Is_REL__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>ERP_NWA__c.Interface_Status__c</field>
            <operation>equals</operation>
            <value>PROCESSING</value>
        </criteriaItems>
        <criteriaItems>
            <field>ERP_NWA__c.LMS_Status__c</field>
            <operation>equals</operation>
            <value>MCNF</value>
        </criteriaItems>
        <criteriaItems>
            <field>ERP_NWA__c.Is_MCNF__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>ERP_NWA__c.Interface_Status__c</field>
            <operation>equals</operation>
            <value>PROCESSING</value>
        </criteriaItems>
        <criteriaItems>
            <field>ERP_NWA__c.LMS_Status__c</field>
            <operation>equals</operation>
            <value>CNF</value>
        </criteriaItems>
        <criteriaItems>
            <field>ERP_NWA__c.Is_CNF__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>ERP_NWA__c.Interface_Status__c</field>
            <operation>equals</operation>
            <value>PROCESSING</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

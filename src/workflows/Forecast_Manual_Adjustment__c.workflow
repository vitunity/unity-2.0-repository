<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Manual_Adjustment_Key</fullName>
        <field>Manual_Adjustment_Key__c</field>
        <formula>TEXT(Region__c)&amp;TEXT(Quarter__c)&amp;TEXT(Fiscal_Year__c)</formula>
        <name>Update Manual Adjustment Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Manual Adjustment Key</fullName>
        <actions>
            <name>Update_Manual_Adjustment_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

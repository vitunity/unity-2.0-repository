<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FieldService_Survey_Sat_score_1_2_3</fullName>
        <ccEmails>customer.sat@varian.com</ccEmails>
        <description>FieldService Survey -Sat score-1,2,3</description>
        <protected>false</protected>
        <recipients>
            <recipient>penny.devlin@varian.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Action_Item_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Survey_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>customer.sat@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Survey_Email_Templates/Field_Service_Survey_Action</template>
    </alerts>
</Workflow>

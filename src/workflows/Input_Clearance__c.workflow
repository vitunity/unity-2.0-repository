<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_alert_for_clearance_expiration_date</fullName>
        <description>Email alert for clearance expiration date</description>
        <protected>false</protected>
        <recipients>
            <recipient>ali.rezaei@varian.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>saundra.mckinley@varian.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>sales_email_templates/Email_alert_for_clearance_entry_form</template>
    </alerts>
    <rules>
        <fullName>Email Alert for Clearance Expiration Date</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Input_Clearance__c.Does_Not_Expire__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Email Alert for Clearance Expiration Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_alert_for_clearance_expiration_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Input_Clearance__c.Clearance_Expiration_Date__c</offsetFromField>
            <timeLength>-180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>

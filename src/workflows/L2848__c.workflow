<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Alert_if_No_Action</fullName>
        <description>Email to Alert if No Action user</description>
        <protected>false</protected>
        <recipients>
            <field>Alert_If_No_Action__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>supportqa@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Helpdesk_Templates/L2848_No_Action_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_email_on_submission_of_L2848</fullName>
        <description>Send email on submission of L2848</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Email_Templates/SR_Email_on_submitting_L2848_record</template>
    </alerts>
    <alerts>
        <fullName>X96_Hour_Alert</fullName>
        <description>96 Hour Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Alert_If_No_Action__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Helpdesk_Templates/L2848_No_Action_Email2</template>
    </alerts>
    <alerts>
        <fullName>X96_Hour_Email_to_Alert_If_No_Action</fullName>
        <description>96 Hour Email to Alert If No Action</description>
        <protected>false</protected>
        <recipients>
            <field>Alert_If_No_Action__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Helpdesk_Templates/L2848_No_Action_Email2</template>
    </alerts>
    <rules>
        <fullName>48 Hours Not Submitted</fullName>
        <active>true</active>
        <criteriaItems>
            <field>L2848__c.Was_anyone_injured__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>L2848__c.Is_Submit__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>L2848__c.Is_Submitted__c</field>
            <operation>equals</operation>
            <value>Not yet</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_to_Alert_if_No_Action</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>L2848__c.CreatedDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>48 Hours Not Submitted_test</fullName>
        <active>false</active>
        <criteriaItems>
            <field>L2848__c.Was_anyone_injured__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>L2848__c.Is_Submit__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>L2848__c.Is_Submitted__c</field>
            <operation>equals</operation>
            <value>Not yet</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_to_Alert_if_No_Action</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>L2848__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>96 Hour Email to Alert If No Action</fullName>
        <active>true</active>
        <criteriaItems>
            <field>L2848__c.Is_Submit__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>L2848__c.Was_anyone_injured__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>L2848__c.Is_Submitted__c</field>
            <operation>equals</operation>
            <value>Not yet</value>
        </criteriaItems>
        <description>US5252 - Alert If No Action user gets notified if no injury after 96 hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X96_Hour_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>L2848__c.CreatedDate</offsetFromField>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>96 Hour Email to Alert If No Action_test</fullName>
        <active>false</active>
        <criteriaItems>
            <field>L2848__c.Is_Submit__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>L2848__c.Was_anyone_injured__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>L2848__c.Is_Submitted__c</field>
            <operation>equals</operation>
            <value>Not yet</value>
        </criteriaItems>
        <description>US5252 - Alert If No Action user gets notified if no injury after 96 hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X96_Hour_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>L2848__c.CreatedDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SR%3A Notification on submitting the L2848 Record</fullName>
        <actions>
            <name>Send_email_on_submission_of_L2848</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>L2848__c.Is_Submit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>DE 730 
It has been deactivated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

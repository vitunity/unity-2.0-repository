<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Lung_Phantom_Email</fullName>
        <description>Lung Phantom Email</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Portal_Templates/LungPhantumEmailTemplate</template>
    </alerts>
    <rules>
        <fullName>LungPhantum Record Creation</fullName>
        <actions>
            <name>Lung_Phantom_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lung_Phantom__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

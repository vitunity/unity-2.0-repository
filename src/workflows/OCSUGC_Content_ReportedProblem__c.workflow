<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>OCSUGC_Send_Email_Notification_For_Flagging_Comments</fullName>
        <description>OCSUGC Send Email Notification For Flagging Comments</description>
        <protected>false</protected>
        <recipients>
            <recipient>OCSUGC_Admins</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>oncopeer@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>OCSUGC_Email_Templates/OCSUGC_Flagging_Notification_HTML</template>
    </alerts>
    <rules>
        <fullName>OCSUGC Flagging Email Notification</fullName>
        <actions>
            <name>OCSUGC_Send_Email_Notification_For_Flagging_Comments</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(ISPICKVAL(OCSUGC_Type_of_Content__c, &apos;Comments&apos;), OCSUGC_Flagged__c == true)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

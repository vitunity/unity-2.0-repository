<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Libra_Phase_II_Notification</fullName>
        <description>Libra Phase II Notification</description>
        <protected>false</protected>
        <recipients>
            <field>District_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>donotreply@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Quote_Email_Templates/Libra_Phase_2_Email_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Libra_Phase_II_Notification1</fullName>
        <description>Libra Phase II Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>District Sales Administrator</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <field>District_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>donotreply@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Quote_Email_Templates/Libra_Phase_2_Email_Reminder_HTML</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_next_trigger_fire_date</fullName>
        <field>Libra2_Next_Trigger_Fire_date__c</field>
        <formula>TODAY()+14</formula>
        <name>Set next trigger fire date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_next_trigger_fire_date1</fullName>
        <field>Libra2_Next_Trigger_Fire_date__c</field>
        <formula>TODAY() + 14</formula>
        <name>Set next trigger fire date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Libra Phase 2 Email Reminder</fullName>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>LIBRA II</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>7 - CLOSED WON</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>8 - CLOSED LOST</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Expected_Close_Date__c</field>
            <operation>lessThan</operation>
            <value>9/27/2018</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Libra_Phase_II_Notification</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_next_trigger_fire_date</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Libra_Phase_II_Notification1</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_next_trigger_fire_date1</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Libra2_Next_Trigger_Fire_date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>

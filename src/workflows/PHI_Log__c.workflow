<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EmailOnOwnerChange</fullName>
        <description>EmailOnOwnerChange</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>donotreply@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>PHI_Logs/SR_notification_to_phi_owner</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_when_file_is_uploaded</fullName>
        <description>Email alert when file is uploaded</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PHI_Logs/when_phi_log_folder_is_create_and_a_file_is_uploaded</template>
    </alerts>
    <alerts>
        <fullName>sending_out_email_to_phi_log_owner</fullName>
        <description>sending out email to phi log owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>PHI_Logs/when_the_record_is_created</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Storage_Location</fullName>
        <field>Data_Security_Details__c</field>
        <literalValue>Varian Data Center (CRM, Varian Secure Drop)</literalValue>
        <name>Update Storage Location</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_owner_name</fullName>
        <field>Owneres_Name__c</field>
        <formula>Owner:User.FirstName+&apos; &apos;+Owner:User.LastName</formula>
        <name>Update owner name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>check_the_queue_as_Owner_field</fullName>
        <field>Queue_as_Owner__c</field>
        <literalValue>1</literalValue>
        <name>check the queue as Owner field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_the_picklist_valie</fullName>
        <field>Disposition2__c</field>
        <literalValue>Transferred - Owner Copy Destroyed/Sent to New Owner</literalValue>
        <name>update the picklist valie</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Name of the owner</fullName>
        <actions>
            <name>Update_owner_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PHI_Log__c.OwnerId</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>When owner is changed</fullName>
        <actions>
            <name>update_the_picklist_valie</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(OwnerId), PRIORVALUE(OwnerId)&lt;&gt; OwnerId,NOT(ISNULL(PRIORVALUE(OwnerId))),NOT(BEGINS(OwnerId, &quot;00G&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>When phi is to upload files to Box</fullName>
        <actions>
            <name>Update_Storage_Location</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PHI_Log__c.Storage_Location2__c</field>
            <operation>equals</operation>
            <value>Varian Secure Drop</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>email alert when storage location is available</fullName>
        <actions>
            <name>Email_alert_when_file_is_uploaded</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>BEGINS( Server_Location__c ,&quot;https://&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>when a phi log record is created</fullName>
        <actions>
            <name>sending_out_email_to_phi_log_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISNULL(OwnerId))</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>PHI_Log__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>when the owner is moved to queue</fullName>
        <actions>
            <name>check_the_queue_as_Owner_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>BEGINS(OwnerId,&quot;00G&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

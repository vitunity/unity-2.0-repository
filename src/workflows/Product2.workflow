<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>E_Notification</fullName>
        <description>E-Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Customer_Portal_Templates/Customer_Portal_Subscribed_e_Notification</template>
    </alerts>
    <alerts>
        <fullName>Part_No_Longer_Requires_PSE_Approval</fullName>
        <ccEmails>Margot.Smith=varian.com@example.com</ccEmails>
        <ccEmails>Damon.Blue=varian.com@example.com</ccEmails>
        <description>Part No Longer Requires PSE Approval</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Service_Email_Templates/Part_No_Longer_Requires_PSE_Approval</template>
    </alerts>
    <alerts>
        <fullName>Part_Requires_PSE_Approval</fullName>
        <ccEmails>Margot.Smith=varian.com@example.com</ccEmails>
        <ccEmails>Damon.Blue=varian.com@example.com</ccEmails>
        <description>Part Requires PSE Approval</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Service_Email_Templates/Part_Requires_PSE_Approval</template>
    </alerts>
    <rules>
        <fullName>Part No Longer Requires PSE approval</fullName>
        <actions>
            <name>Part_No_Longer_Requires_PSE_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>workflow used to send email to Logistics when a part no longer requires PSE Approval</description>
        <formula>AND(PRIORVALUE(PSE_Approval_Required__c) = TRUE, PSE_Approval_Required__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Part Requires PSE approval</fullName>
        <actions>
            <name>Part_Requires_PSE_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>workflow used to send email to Logistics when a part require PSE Approval</description>
        <formula>AND(PRIORVALUE(PSE_Approval_Required__c) = FALSE, ISNEW() = FALSE, PSE_Approval_Required__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

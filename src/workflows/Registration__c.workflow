<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CP_New_Event_RegistrationUSA</fullName>
        <description>Send Email to Registered event user for usa</description>
        <protected>false</protected>
        <recipients>
            <field>E_mail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Portal_Templates/CP_New_Event_Registration</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Registered_event_user</fullName>
        <description>Send Email to Registered event user</description>
        <protected>false</protected>
        <recipients>
            <field>E_mail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Portal_Templates/CP_New_Event_Registration</template>
    </alerts>
    <rules>
        <fullName>CP RegisterEvents</fullName>
        <actions>
            <name>Send_Email_to_Registered_event_user</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Registration__c.E_mail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send mail to users regestering for Events</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CpRegistraionEventUS</fullName>
        <actions>
            <name>CP_New_Event_RegistrationUSA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Registration__c.E_mail__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Registration__c.Country__c</field>
            <operation>equals</operation>
            <value>USA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Registration__c.Meal_option__c</field>
            <operation>contains</operation>
            <value>Accept Meals (Reportable),Purchase Meals (Not Reportable*),Decline Meals (Not Reportable)</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>STB_Reject_Notification</fullName>
        <description>STB Reject Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Technician_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>donotreply@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Service_Email_Templates/STB_Reject_Email</template>
    </alerts>
    <rules>
        <fullName>STB Reject Created</fullName>
        <actions>
            <name>Update_Technician_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>STB_Reject__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>STB Reject Workflow</fullName>
        <actions>
            <name>STB_Reject_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>STB Reject Workflow to send email when STB Reject Reason is that STB ModCard does not exist in PSE database.</description>
        <formula>AND(BEGINS(Reason_for_Rejection__c , &quot;Matching PSE Mod Card not found&quot;), NOT(ISNULL(Technician_Email__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_If_WorkOrder_Is_Not_Approved</fullName>
        <description>check if workorder is not approved</description>
        <field>Order_Not_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Check If WorkOrder Is Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnCheck_If_WorkOrder_Is_Not_Approved</fullName>
        <description>uncheck if workorder is not approved</description>
        <field>Order_Not_Approved__c</field>
        <literalValue>0</literalValue>
        <name>UnCheck If WorkOrder Is Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Bypass_Validation</fullName>
        <field>Bypass_Validation__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Bypass Validation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check If WorkOrder Is Not Approved</fullName>
        <actions>
            <name>Check_If_WorkOrder_Is_Not_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check If WorkOrder Is Not Approved</description>
        <formula>Order_Status__c != &apos;Approved&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UnCheck If WorkOrder Is Not Approved</fullName>
        <actions>
            <name>UnCheck_If_WorkOrder_Is_Not_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>UnCheck If WorkOrder Is Not Approved</description>
        <formula>ISPICKVAL(Work_Details__r.SVMXC__Service_Order__r.SVMXC__Order_Status__c ,&apos;Approved&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Bypass Validation</fullName>
        <actions>
            <name>Uncheck_Bypass_Validation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC_Time_Entry__c.Bypass_Validation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

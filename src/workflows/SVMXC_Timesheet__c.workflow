<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SR_Time_Sheet_Approval_Notification</fullName>
        <description>SR_Time Sheet Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Email_Templates/WD_Approved_Mail</template>
    </alerts>
    <alerts>
        <fullName>SR_Timesheet_Rejection</fullName>
        <description>SR_Timesheet_Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>donotreply@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Service_Email_Templates/SR_Time_Sheet_Rejection_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Techn_and_Manager_if_Approved_TE_date_time_is_changed</fullName>
        <description>Send Email to Techn and Manager, if Approved TE date time is changed</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_s_Email_ID__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Send_Email_to_Techn_and_Manager_if_Approved_TS_TE_date_time_is_changed</template>
    </alerts>
    <fieldUpdates>
        <fullName>SR_Time_Sheet_Approval</fullName>
        <description>US 4001</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>SR_Time_Sheet_Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SR_Time_Sheet_Pending_Approval</fullName>
        <description>US 4001</description>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>SR_Time_Sheet_Pending/Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SR_Time_Sheet_Rejection</fullName>
        <field>Status__c</field>
        <literalValue>Incomplete</literalValue>
        <name>SR_Time_Sheet_Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Comments_Available_On_Timesheet</fullName>
        <field>Comments_Available__c</field>
        <literalValue>1</literalValue>
        <name>Update Comments Available On Timesheet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_s_Email_ID</fullName>
        <description>US4459: Block time card 
Update Manager&apos;s Email ID</description>
        <field>Manager_s_Email_ID__c</field>
        <formula>Technician__r.User__r.Manager.Email</formula>
        <name>Update Manager&apos;s Email ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Technician_Name_On_TM</fullName>
        <description>Update Technician Name On Timesheet</description>
        <field>Technician_Name__c</field>
        <formula>Technician__r.Name</formula>
        <name>Update Technician Name On TM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Timesheet_Comments_Available</fullName>
        <field>Comments_Available__c</field>
        <literalValue>0</literalValue>
        <name>Update Timesheet Comments Available</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SR_Timesheet_Rejection Rule</fullName>
        <actions>
            <name>SR_Timesheet_Rejection</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Timesheet_Comments_Available</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>SVMXC_Timesheet__c.Comments_Available__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC_Timesheet__c.Technician_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to Techn and Manager if  Approved TE date time is changed</fullName>
        <actions>
            <name>Send_Email_to_Techn_and_Manager_if_Approved_TE_date_time_is_changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>US4459: Block time card 
Send Email to Techn and Manager, if Approved TE date time is changed</description>
        <formula>IF(AND(PRIORVALUE(Most_Recently_Child_Time_Entry_Updates__c)  &lt;&gt; Most_Recently_Child_Time_Entry_Updates__c,  ISPICKVAL(Status__c, &apos;Approved&apos;)), TRUE,FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Managers Email ID</fullName>
        <actions>
            <name>Update_Manager_s_Email_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>US4459: Block time card 
Send Email to Techn and Manager, if Approved TE date time is changed
: Update Manager&apos;s Email ID</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Brazil_WO_Approved_w_Parts</fullName>
        <ccEmails>nick.sauer@varian.com</ccEmails>
        <ccEmails>Daniela.Goldoni@varian.com</ccEmails>
        <ccEmails>Carmen.Palacio@varian.com</ccEmails>
        <description>Brazil_WO_Approved_w_Parts</description>
        <protected>false</protected>
        <recipients>
            <field>District_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Technician_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Email_Templates/Brazil_WO_Approved_w_Parts</template>
    </alerts>
    <rules>
        <fullName>Brazil_WO_Approved_w_Parts</fullName>
        <actions>
            <name>Brazil_WO_Approved_w_Parts</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.Sales_Org__c</field>
            <operation>equals</operation>
            <value>4400</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.SVMXC__Order_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.Parts_Quote_Status__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>SVMXC__Service_Order__c.Parts_Work_Detail_Count__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Work Order for Sales Org 4400 is Approved and has Parts Lines where Parts Quote is received.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

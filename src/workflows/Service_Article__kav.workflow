<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Article_Approved</fullName>
        <description>Article Approved</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Email_Templates/Article_Publication_Approved</template>
    </alerts>
    <alerts>
        <fullName>Article_Rejected</fullName>
        <description>Article Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Service_Email_Templates/Article_Publication_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_by_SME</fullName>
        <field>ValidationStatus</field>
        <literalValue>Approved by SME</literalValue>
        <name>Approved by SME</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_for_Internal</fullName>
        <field>ValidationStatus</field>
        <literalValue>Approved for Internal</literalValue>
        <name>Approved for Internal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_Visible_to_Customer_Channel</fullName>
        <field>IsVisibleInCsp</field>
        <literalValue>0</literalValue>
        <name>Not Visible to Customer Channel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_Visible_to_Partner_Channel</fullName>
        <field>IsVisibleInPrm</field>
        <literalValue>0</literalValue>
        <name>Not Visible to Partner Channel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not_Visible_to_Public_Channel</fullName>
        <field>IsVisibleInPkb</field>
        <literalValue>0</literalValue>
        <name>Not Visible to Public Channel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_field</fullName>
        <field>Product_and_Version_DNU__c</field>
        <formula>CreatedBy.Manager.FirstName + &apos; &apos; +CreatedBy.Manager.LastName</formula>
        <name>Update Manager field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validated_Internal</fullName>
        <field>ValidationStatus</field>
        <literalValue>Validated Internal</literalValue>
        <name>Validated Internal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validated_SME</fullName>
        <field>ValidationStatus</field>
        <literalValue>Validated SME</literalValue>
        <name>Validated SME</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Pending</fullName>
        <field>ValidationStatus</field>
        <literalValue>Validation Pending</literalValue>
        <name>Validation Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_Article</fullName>
        <action>Publish</action>
        <label>Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>Publish_Article_as_New</fullName>
        <action>PublishAsNew</action>
        <label>Publish Article as New</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>SR%3AUpdate Manager Name</fullName>
        <actions>
            <name>Update_Manager_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Service_Article__kav.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

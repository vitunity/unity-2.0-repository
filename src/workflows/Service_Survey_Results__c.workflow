<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Mail_for_approval_to_manager</fullName>
        <description>Mail for approval to manager</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Owner_Manager__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>supportqa@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Service_Email_Templates/Mail_for_ser_Approval_to_manager</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status_Pending_approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Approval Status Pending approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_status_pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Approval status pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Aproval_Status_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Aproval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_pending_approval_date_to_today</fullName>
        <description>US4130</description>
        <field>Pending_Approval_Date__c</field>
        <formula>Today()</formula>
        <name>Set pending approval date to today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>

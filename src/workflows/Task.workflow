<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Notif_To_Tech_Sales_Manager_Notif_Gp_On_Save</fullName>
        <description>Send Notif To Tech Sales Manager Notif Gp On Save</description>
        <protected>false</protected>
        <recipients>
            <recipient>Tech_Sales_Manager_Notification_Group</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>support@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>ALL_templates/ToPlanning</template>
    </alerts>
    <rules>
        <fullName>Technical Sales Manager Save</fullName>
        <actions>
            <name>Send_Notif_To_Tech_Sales_Manager_Notif_Gp_On_Save</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Service_Group__c</field>
            <operation>includes</operation>
            <value>Technical Sales Manager</value>
        </criteriaItems>
        <description>Workflow triggered on saving a Planning Request record type including Service Group: Technical Sales Manager</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

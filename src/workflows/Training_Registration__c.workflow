<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Cp_Send_Mail_for_Training_Course_Registration</fullName>
        <description>Cp Send Mail for Training Course Registration</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Portal_Templates/CpTrainingRegistrationEmail</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Varian_Admin_for_Training_and_courses</fullName>
        <ccEmails>learningcenter=varian.com@example.com</ccEmails>
        <description>Send Email to Varian Admin for Training and courses</description>
        <protected>false</protected>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Portal_Templates/Cp_SendMailToAdminTrainingCourse</template>
    </alerts>
    <rules>
        <fullName>CpTrainingRegistration</fullName>
        <actions>
            <name>Cp_Send_Mail_for_Training_Course_Registration</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Email_to_Varian_Admin_for_Training_and_courses</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training_Registration__c.Name</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <description>Send mail to users registering for Training Courses</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

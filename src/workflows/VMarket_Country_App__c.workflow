<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>vMarket_CountryApp_Reject</fullName>
        <ccEmails>marketplace@varian.com</ccEmails>
        <description>vMarket_CountryApp_Reject</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>donotreply@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>vMarket_Email_Templates/vMarket_CountryAppRejection_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>vMarket_More_Info_Parent_App</fullName>
        <field>ApprovalStatus__c</field>
        <literalValue>Info Requested</literalValue>
        <name>vMarket More Info Parent App</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>VMarket_App__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vMarket_Update_Parent_App_filed</fullName>
        <field>ApprovalStatus__c</field>
        <literalValue>Published</literalValue>
        <name>vMarket Update Parent App filed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>VMarket_App__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>VMarket Country App More Info Requested</fullName>
        <actions>
            <name>vMarket_More_Info_Parent_App</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VMarket_Country_App__c.VMarket_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Info Requested</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>vMarket Country App Rejected Mail</fullName>
        <actions>
            <name>vMarket_CountryApp_Reject</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VMarket_Country_App__c.VMarket_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>vMarket Publish Parent App</fullName>
        <actions>
            <name>vMarket_Update_Parent_App_filed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VMarket_Country_App__c.VMarket_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Published</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>vMakert_Buyer_Order_Failure_Alert</fullName>
        <description>vMakert Buyer Order Failure Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>vMarket_Email_Templates/vMarket_Order_Failure_Template</template>
    </alerts>
    <alerts>
        <fullName>vMarket_Buyer_Order_Success_Email_Alert</fullName>
        <description>vMarket Buyer Order Success Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>vMarket_Email_Templates/vMarket_Order_Success_Template</template>
    </alerts>
    <rules>
        <fullName>vMarket Buyer Order Failure Rule</fullName>
        <actions>
            <name>vMakert_Buyer_Order_Failure_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>vMarketOrderItem__c.Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vMarket Buyer Order Success Rule</fullName>
        <actions>
            <name>vMarket_Buyer_Order_Success_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>vMarketOrderItem__c.Status__c</field>
            <operation>equals</operation>
            <value>Success</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

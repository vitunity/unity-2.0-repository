<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>vMarket_App_Bug_Fix</fullName>
        <ccEmails>sunita.jagdale@varian.com</ccEmails>
        <description>vMarket App Bug Fix</description>
        <protected>false</protected>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>vMarket_Email_Templates/vMarket_Bug_Fix</template>
    </alerts>
    <alerts>
        <fullName>vMarket_App_Requested</fullName>
        <ccEmails>harshal.mulay@varian.com</ccEmails>
        <description>vMarket App Info Requested</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>myvarian@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>vMarket_Email_Templates/vMarket_InfoRequested_App_Template</template>
    </alerts>
    <rules>
        <fullName>vMarket App Bug Fix Rule</fullName>
        <actions>
            <name>vMarket_App_Bug_Fix</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>vMarket_App__c.bug_fix__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vMarket App Status Info Requested Rule</fullName>
        <actions>
            <name>vMarket_App_Requested</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>vMarket_App__c.ApprovalStatus__c</field>
            <operation>equals</operation>
            <value>Info Requested</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

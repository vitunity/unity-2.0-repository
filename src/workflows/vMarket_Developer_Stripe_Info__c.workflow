<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>vMarket_Developer_Account_Request</fullName>
        <ccEmails>marketplace@varian.com</ccEmails>
        <description>vMarket_Developer_Account_Request</description>
        <protected>false</protected>
        <senderAddress>donotreply@varian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>vMarket_Email_Templates/vMarket_App_Developer_Request</template>
    </alerts>
    <rules>
        <fullName>vMarket_Developer_Account_Account_Request</fullName>
        <actions>
            <name>vMarket_Developer_Account_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>vMarket_Developer_Stripe_Info__c.Developer_Request_Status__c</field>
            <operation>notEqual</operation>
            <value>Rejected,Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
